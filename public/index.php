<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

require_once APPLICATION_PATH . '/../library/Lib/Application.php';

// Create application, bootstrap, and run
Lib_Application::getInstance()->run();

