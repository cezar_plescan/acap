var _BaseComponents = {};

(function(){

var Storage = Class.extend(
{
    init: function()
    {
        this._storage = {};
    },

    get: function( key )
    {
        return this._storage[ key ];
    },

    has: function( key )
    {
        return typeof this._storage[ key ] != 'undefined';
    },

    store: function( key, value )
    {
        this._storage[ key ] = value;
    }
});

var Container = Class.extend(
{
    // custom constructor
    _reset: function(){},
    _init: function(){},
    _setup: function(){},

    init: function( $container, options )
    {
        this._reset();

        this.$container = $container;

        this.Parent     = null;

        if ( typeof options == 'undefined' )
        {
            options = {}
        }
        this.options = options;

        if ( typeof options.Parent != 'undefined' )
        {
            this.setParent( options.Parent );
        }

        this._setup();

        this._init();
    },

    _trigger: function( eventName, params )
    {
        if (typeof params == 'undefined' )
        {
            params = null;
        }

        var callbacks = [ this.options[eventName], this['_' + eventName] ];
        for (var key in callbacks)
        {
            var callback = callbacks[key];
            if ( $.isFunction(callback) )
            {
                return callback.call(this, params);
                break;
            }
        }

    },

    setHandler: function( eventName, callback )
    {
        if ( $.isFunction(callback) )
        {
            this.options[eventName] = callback;
        }
    },

    getContainer: function()
    {
        return this.$container;
    },

    find: function( selector )
    {
        var $element = $(selector, this.$container);

        return $element;
    },

    bind: function(event, selector, callback)
    {
        this.$container.on(event, selector, callback);
    },

    show: function()
    {
        this.$container.show();
    },

    hide: function()
    {
        this.$container.hide();
    },

    getParent: function()
    {
        return this.Parent;
    },

    setParent: function( Parent )
    {
        this.Parent = Parent;
    }

})

var Form = Container.extend(
{
    _setup: function()
    {
        this.loadingMessage = 'În curs de procesare...';

        this.$message   = $('.message', this.$container);

        this.$form = this.$container.is('form') ?
            this.$container:
            this.$container.find('form');

        this._bindSubmit();

        this.$loader = $('#ajax-loader');
        this.$loaderMessage = this.$loader.find('.message-content');

    },

    _bindSubmit: function()
    {
        var self = this;
        var $actionField = this.find(':hidden[name="operation"]');

        this.bind('click', ':submit', function( event ){
            var $input = $(event.target);
            $actionField.val($input.val());
        })

        this.$form.submit(function(event){
            event.preventDefault();

            self._clearErrors();

            if ( $.isFunction(self.options.submit) )
            {
                self.options.submit();
            }
            else
            {
                var formParams = self.getParams();
                if ( self._trigger('beforeSubmit', formParams) !== false )
                {
                    self._doSubmit();
                }
            }

            // clear action field
            $actionField.val('');
        });
    },

    _getRequestUrl: function()
    {
        var url = null;

        var priority = [
            this.options.url,
            this.url,
            this.$form.attr('action')
        ];

        foreach(priority, function(index, value){
            if ( url = value )
            {
                return false;
            }
        })

        return url;
    },

    _doSubmit: function()
    {
        var self = this;
        var params = this.getParams();
        params.token = $('#request-token').val();

        var requestUrl = this._getRequestUrl();

        // show loader
        this.$loaderMessage.text( this.loadingMessage );
        this.$loader.show();

        $.ajax({
            url: requestUrl,
            data: params,
            dataType: 'json',
            type: 'post',
            async: true,
            success: function( response )
            {
                self._clearErrors();

                if ( response && $.isPlainObject(response) )
                {
                    if ( response.success )
                    {
                        self._trigger('success', response.data );
                    }
                    else if ( response.errors && response.errors.not_authenticated )
                    {
                        alert('Sesiunea a expirat. Vă rugăm să vă autentificaţi din nou.');

                        $('body').empty();
                        window.location.reload();
                    }
                    else
                    {
                        self._showErrors( response.errors );
                        self._trigger('error', response.errors );
                    }
                }
                else
                {
                    alert( 'Ne pare rău, a apărut o problemă tehnică.' );
                }

            },
            error: function()
            {
                self._clearErrors();
                alert( 'Eroare la procesarea datelor!' );
            },
            complete: function()
            {
                self.$loader.hide();
            }

        })

    },

    getParams: function()
    {
        var params = $.serializeObject(this.$form.serializeArray());

        var customParams = this.getCustomParams();

        $.extend( params, customParams);

        return params;
    },

    getCustomParams: function()
    {
        return null;
    },

    _getField: function( name, strictName )
    {
        if (typeof strictName == 'undefined')
        {
            strictName = true;
        }

        var $field = $('[name="'+name+'"]:not(:disabled)', this.$form);

        if ( !$field.length && !strictName )
        {
            $field = $('[name^="'+name+'["]:not(:disabled)', this.$form);
        }

        return $field;
    },

    _showErrors: function( errors )
    {
        var self = this;

        var messages = this._parseErrors( errors );

        foreach(messages, function(field, message){
            self._setError( field, message );
        })
    },

    _parseErrors: function( searchArray, results, Final )
    {
        var self = this;

        if ( typeof results == 'undefined' )
        {
            results = [];
        }

        if ( typeof Final == 'undefined' )
        {
            Final = {};
        }

        foreach(searchArray, function(key,value){

            if ( !results.length )
            {
                results.push(key);
            }
            else
            {
                results.push("["+key+"]");
            }

            if( !self._isErrorMessage(value) )
            {
                self._parseErrors( value, results, Final );
            }
            else
            {
                var resultsLabel = results.join('');

                if ( !isString(value) )
                {
                    value = value.message;
                }

                Final[resultsLabel] = value;
                results.pop();
            }
        });

        results.pop();

        return Final;
    },

    _isErrorMessage: function( error )
    {
        if ( ( isIterable( error )
            && ( typeof error.code != 'undefined' && isString( error.code ) )
            && ( typeof error.message != 'undefined' && isString( error.message ) ) )
            || isString(error))
        {
            return true;
        }

        return false;
    },

    _displayGeneralError: function( errorMessage, timeout, append )
    {
        if ( typeof append == 'undefined' )
        {
            append = false;
        }

        var $error = $('.general-error', this.$container);
        
        if( !$error.length )
        {
            alert( errorMessage );
        }
        else
        {
            var $errorMessage = $('<div>').text( errorMessage );

            if ( append )
            {
                $error.append( $errorMessage );
            }
            else
            {
                $error.html( $errorMessage );
            }

            $error.show();

            if ( typeof timeout != 'undefined' && timeout )
            {
                setTimeout(function(){
                    $error.fadeOut();
                }, timeout)
            }
        }
    
        // hide any message
        this._clearMessage();
    },

    _setError: function( fieldName, value )
    {
        var $error = this.find( '.error[_name="' + fieldName + '"]' );

        if ( !$error.length )
        {
            var $field = this._getField( fieldName, false );

            if ( $field.length )
            {
                $error = $field.closest( '.field-container' ).find( '.error' );
            }
        }

        if ( $error.length )
        {
            $error.text( value ).show();
        }
        else
        {
            // show as general error
            this._displayGeneralError( value, null, true );
        }

    },

    _clearErrors: function()
    {
        $('.error, .general-error', this.$container).html('').hide();
    },

    setMessage: function( message, timeout )
    {
        var self = this;

        if (typeof timeout == 'undefined')
        {
            timeout = 4000;
        }

        clearTimeout(this.$message.data('timeout'));

        this.$message
                .text( message )
                .hide()
                .stop()
                .fadeIn(function(){
                    if (timeout)
                    {
                        self.$message.data('timeout', setTimeout(function(){
                            self.$message.fadeOut();
                        }, timeout));
                    }
                });
    },

    _clearMessage: function()
    {
        this.$message.text('').hide();
    },

    clearMessages: function()
    {
        this._clearErrors();
        this._clearMessage();
    },

    clear: function()
    {
        this.$form[0].reset();
    },

    reset: function()
    {
        this.$form[0].reset();
    },

    submit: function()
    {
        this.$form.submit();
    }

})

////////////////////////////////////////////////////////////////////////////////

var Filter = Container.extend(
{
    /*
     * options:
     *  - filterValue [function]
     *  - skipHidden [boolean]; defaults to true
     *
     */

    _setup: function()
    {
        this._defaults = {
            skipHidden:   true
        };

        $.extend( this.options, this._defaults );
    },

    filter: function( value )
    {
        var filterCallback = this.options.filterValue;
        var self = this;

        if ( typeof value == 'undefined' )
        {
            if ( $.isFunction( filterCallback ) )
            {
                value = filterCallback.call( this );
            }
            else
            {
                value = null;
            }
        }

        if ( this._isValid(value) )
        {
            var $items = this.getItems();

            $.each($items, function(){
                var $item = $(this);

                var filter = self._filter( $item, value );
                if ( filter == false )
                {
                    $item.hide();
                }
                else if (!self.options.skipHidden)
                {
                    $item.show();
                }
            })
        }
    },

    _isValid: function(value)
    {
        // default value has to be -1 because we have options with value = 0
        if( value != -1 && value.length )
        {
            return true;
        }

        return false;
    },

    _filter: function( $item, value )
    {
        throw 'Method not implemented!';
    },

    getItems: function()
    {
        if ( typeof this.options.itemsSelector == 'undefined' )
        {
            throw 'The selector is not defined';
        }

        var selector = '';

        if (this.options.skipHidden)
        {
            selector = ':not(:hidden)'; // :visible
        }

        var $items = this.find( this.options.itemsSelector + selector );

        return $items;
    }
});

////////////////////////////////////////////////////////////////////////////////

var Dialog = Container.extend(
{
    _open: function(){},
    _close: function(){},

    _reset: function()
    {
        this._open_arguments = [];
        this._close_arguments = [];
    },

    _setup: function()
    {
        this._defaults = {
            autoOpen:   false,
            modal:      true,
            resizable:  false,
            draggable:  false,
            closeOnEscape: false,
            position:   'center',
            width:      'auto',
            height:     'auto'
        };

        var self = this;

        $.extend( this._defaults, this.options );

        if ( typeof this._defaults.open == 'undefined' )
        {
            this._defaults.open = function(){
                self._open.apply( self, self._open_arguments );
            }
        }
        else
        {
            this._defaults.open = function(){
                self.options.open.apply( self, self._open_arguments );
            }
        }

        if ( typeof this._defaults.close == 'undefined' )
        {
            this._defaults.close = function(){
                self._close.apply( self, self._close_arguments );
            }
        }
        else
        {
            this._defaults.close = function(){
                self.options.close.apply( self, self._close_arguments );
            }

        }


        if ( typeof this._defaults.title != 'undefined' )
        {
            this._title = this._defaults.title;
            delete this._defaults.title;
        }

        this.$container.dialog( this._defaults );

        this.$container.on('dialogopen', function(){
            if ( !self._defaults.height || self._defaults.height == 'auto' )
            {
                self.$container.css('height', 'auto');
            }

            if ( !self._defaults.width || self._defaults.width == 'auto' )
            {
                self.$container.css('width', 'auto');
            }

        })

        if (typeof this._title != 'undefined')
        {
            this.setTitle( this._title );
        }
    },

    setOptions: function( options )
    {
        this.$container.dialog( 'option', options );
    },

    setTitle: function( title )
    {
        var $titleBar = this.$container.siblings('.ui-dialog-titlebar');
        var $title = $titleBar.find('.title');
        if (!$title.size())
        {
            $title = $('<span class="title">'+this._title+'</span>').insertAfter($titleBar.find('.ui-dialog-title'));
        }

        $title.text( title );
    },

    close: function()
    {
        this._close_arguments = arguments;

        this.$container.dialog('close');
    },

    open: function()
    {
        this._open_arguments = arguments;

        this.$container.dialog('open');

    },

    setTop: function(value)
    {
        var $parent = this.getDialogElement();
        //var extraHeight = $parent.outerHeight(true);

        $parent.css('top', value+'px');
    },

    getDialogElement: function()
    {
        var $dialog = this.$container.closest('.ui-dialog');

        return $dialog;
    }
})

var Message = Container.extend(
{
    displayInterval: 15000,

    _reset: function()
    {
        this.timeout = null;
    },

    _setup: function()
    {

    },

    display: function( message )
    {
        var self = this,
            $container = this.getContainer();

        clearTimeout( this.timeout );
        $container.stop();

        $container.html( message ).slideDown(function(){
            self.timeout = setTimeout( function(){
                $container.slideUp().fadeOut();
            }, self.displayInterval);
        });

    }

});


var Ajax = Container.extend(
{
    _setup: function()
    {
        this._setContainers();

        this.$defaultContainer = this.$container || $();

        this.$loader = $('#ajax-loader');
        this.$loaderMessage = this.$loader.find('.message-content');
    },

    _setContainers: function()
    {
        this.$errorContainer = this.find('.general-error');
    },

    _changeContainer: function( $newContainer )
    {
        if ( typeof $newContainer == 'undefined' || !$newContainer )
        {
            $newContainer = this.$defaultContainer;
        }

        if ( $newContainer == this.$container )
        {
            return;
        }

        this.$container = $newContainer;

        this._setContainers();
    },

    request: function( options )
    {
        var self = this;

        var url = this._buildUrl( options.url ),
            data = options.data;

        if ( typeof data == 'undefined' || !isPlainObject( data ))
        {
            data = {};
        }

        // add token
        data.token = $('#request-token').val();

        // change container
        if ( typeof options.container != 'undefined' )
        {
            if ( options.container instanceof $ )
            {
                var $container = options.container;
            }
            else
            {
                throw 'Invalid container for ajax call';
            }
        }
        else
        {
            $container = this.$defaultContainer;
        }

        this._changeContainer( $container );

        this._clearErrors();

        var async = ( typeof options.async != 'undefined' ) ? options.async : true;
        if( async && typeof options.message != 'undefined' )
        {
            // show loader
            this.$loaderMessage.text( options.message );
            this.$loader.show();
        }

        $.ajax({
            //timeout: 30000,
            url:    url,
            data:   data,
            dataType: ( typeof options.dataType != 'undefined' ) ? options.dataType : 'json',
            type: ( typeof options.type != 'undefined' ) ? options.type : 'post',
            async: async,
            success: function( response )
            {
                self._clearErrors();

                if ( this.dataType == 'json' )
                {
                    if ( response && $.isPlainObject(response) )
                    {
                        if ( response.success )
                        {
                            options.success.call( this, response.data );
                        }
                        else if ( response.errors && response.errors.not_authenticated )
                        {
                            alert('Sesiunea a expirat. Vă rugăm să vă autentificaţi din nou.');

                            $('body').empty();
                            window.location.reload();
                        }
                        else
                        {
                            self._showErrors( response.errors );
                            if ( isFunction( options.error ) )
                            {
                                options.error.call( this, response.errors );
                            }
                        }
                    }
                    else
                    {
                        alert( 'Ne pare rău, a apărut o problemă tehnică.' );
                    }
                }
                else
                {
                    options.success.call( this, response );
                }

            },
            error: function()
            {
                self._clearErrors();
                alert('Eroare la procesarea datelor!');
                if ( isFunction( options.abort ) )
                {
                    options.abort.call( this );
                }
            },
            complete: function()
            {
                self.$loader.hide();
            }
        })
    },

    _clearErrors: function()
    {
        this.$errorContainer.html('').hide();
    },

    _showErrors: function( errors )
    {
        var message;

        if ( isString( errors ) )
        {
            message = errors;
        }
        else
        {
            message = this._parseErrors( errors );
        }

        this._populateErrorContainer( message );
    },

    _populateErrorContainer: function( message )
    {
        if ( isIterable(message) )
        {
            var joinedMessage = '';
            foreach(message, function(key, value){
                joinedMessage += (value + '<br />');
            })
            message = joinedMessage;
        }

        this.$errorContainer.html( message ).slideDown();
    },

    _parseErrors: function( errors )
    {
        var message = {};

        if ( this._isErrorMessage( errors ) )
        {
            message[errors.code] = errors.message;
        }
        else
        {
            var self = this;

            foreach(errors, function(key, value){
                $.extend( message, self._parseErrors( value ) );
            })
        }

        return message;
    },

    _isErrorMessage: function( error )
    {
        if ( isIterable( error )
            && ( typeof error.code != 'undefined' && isString( error.code ) )
            && ( typeof error.message != 'undefined' && isString( error.message ) ) )
        {
            return true;
        }

        return false;
    },

    _displayGeneralError: function( message )
    {
        this.$errorContainer.html( message ).show();
    },

    _buildUrl: function( url )
    {
        url = $.trim( url );
        if ( url.charAt(0) == '/' )
        {
            return url;
        }

        var baseUrl = $.trim(this.options.baseUrl);

        if ( baseUrl.length )
        {
            // check if baseUrl ends with '/'
            if ( baseUrl[baseUrl.length-1] != '/' )
            {
                baseUrl += '/';
            }

            url = baseUrl + url;
        }

        return url;
    }

});

var Module = Container.extend(
{
    _reset: function()
    {
        this._super();

        this.SubModules = {};
    },

    _addSubModule: function( subModule, subModuleName )
    {
        if (!( subModule instanceof Container ))
        {
            throw 'The submodule should be a Container';
        }

        if ( typeof subModuleName == 'undefined' || ( subModuleName = $.trim(subModuleName) ).length == 0 )
        {
            throw 'The submodule should have a proper name';
        }

        this.SubModules[ subModuleName ] = subModule;

        subModule.setParent( this );
    },

    getSubModule: function( subModuleName )
    {
        var subModule = this.Submodules[ subModuleName ];

        if ( typeof subModule == 'undefined' )
        {
            throw 'The requested submodule "' + subModuleName +'" does not exist';
        }

        return subModule;
    }

});

/**
 * #options#
 * - defaultPane: 
 *      specifies what pane will be initially visible; 
 *      the value is the "pane-id" data attribute of the container element
 * - paneConstructors: 
 *      an object containing pairs of paneId:paneConstructor; 
 *      paneConstructor takes the correponding pane container element as its first argument when it is instantiated;
 *      paneConstructor can be an array in which the first value is the constructor itself and the second is a set of options passed to the contructor when iti is instantiated;
 * 
 */
var PanesConstructor = Container.extend(
{
    _reset: function()
    {
        this.$panes = $();
        this.paneIds = [];
        this.Panes = {};
    },
    
    _init: function()
    {
        this._setupPanes();
        this._preparePanes();
    },
    
    /**
     * initialize pane elements and determine which will be visible
     */
    _setupPanes: function()
    {
        var self = this;
        
        // store pane elements
        this.$panes = this.find( '>*' );
        
        // store pane ids
        this.$panes.each( function(){
            var paneId = $(this).data( 'pane-id' );
            self.paneIds.push( paneId );
        });
        
        // check for default pane; if none, show the first one
        if( typeof this.options.defaultPane !== 'undefined' )
        {
            this.showPane( this.options.defaultPane );
        }
        else
        {
            this._showFirstPane();
        }
    },
    
    /**
     * apply constructors 
     */
    _preparePanes: function()
    {
        var self = this;
        
        if( isIterable( this.options.paneConstructors ) )
        {
            foreach( this.options.paneConstructors, function( paneId, PaneConstructor ){
                self._loadPane( paneId, PaneConstructor );
            });
        }
    },
    
    _loadPane: function( paneId, PaneConstructor )
    {
        var constructorOptions = {};
        
        if( isArray( PaneConstructor ) )
        {
            constructorOptions = PaneConstructor[ 1 ];
            PaneConstructor = PaneConstructor[ 0 ];
        }
        
        var $pane = this._getPaneElement( paneId );
        if( $pane.size() )
        {
            var Pane = new PaneConstructor( $pane, constructorOptions );
            
            this.Panes[ paneId ] = Pane;
        }
        else
        {
            this._error.paneId( paneId );
        }
        
    },
    
    _getPaneElement: function( paneId )
    {
        var $pane = this.$panes.filter( '[data-pane-id="'+ paneId +'"]' );
        
        return $pane;
    },
    
    showPane: function( paneId )
    {
        var $pane = this._getPaneElement( paneId );
        
        if( $pane.size() )
        {
            this._hidePanes();
            $pane.show();
        }
        else
        {
            this._error.paneId( paneId );
        }
    },
    
    _showFirstPane: function()
    {
        this._hidePanes();
        this.$panes.eq( 0 ).show();
    },
    
    _hidePanes: function()
    {
        this.$panes.hide();
    },
    
    _error: {
        paneId: function( paneId )
        {
            throwError( 'The pane having the ID "' + paneId + '" doesn\'t exist.' );
        }
    }
    
});

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

var ApplicationConstructor = Container.extend(
{
    run: function()
    {
        this._showApplication();
    },
    
    _showApplication: function()
    {
        $( '#body-overlay' ).hide();

        $( '#body-canvas' ).show();
    }
    
});


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

_BaseComponents = {
    Application:    ApplicationConstructor,
    
    Ajax:           Ajax,
    Storage:        Storage,
    Container:      Container,
    Form:           Form,
    Filter:         Filter,
    Dialog:         Dialog,
    Message:        Message,
    Module:         Module,
    Panes:          PanesConstructor,
}

})();
