////////////////////////////////////////////////////////////////////////////////
// GLOBAL FUNCTIONS ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function escapeHtml(unsafe) {
  return unsafe
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;")
      .replace(/"/g, "&quot;")
      .replace(/'/g, "&#039;");
}

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    };
    return true;
}

function objectEquals(obj1, obj2) {
    for (var i in obj1) {
        if (obj1.hasOwnProperty(i)) {
            if (!(obj2.hasOwnProperty(i))) return false;
            if (obj1[i] != obj2[i]) return false;
        }
    }
    for (var i in obj2) {
        if (obj2.hasOwnProperty(i)) {
            if (!obj1.hasOwnProperty(i)) return false;
            if (obj1[i] != obj2[i]) return false;
        }
    }
    return true;
}

function is_string(input)
{
    return typeof(input) == 'string' && isNaN(input) && (input - 0 != input);
}

function isString( value )
{
    return typeof value == 'string';
}

function _isNumeric( value )
{

}

function isArray( value )
{
    return $.isArray( value );
}

function isPlainObject( value )
{
    return $.isPlainObject( value );
}

function isFunction( value )
{
    return $.isFunction( value );
}

function isIterable(data)
{
    var isIterable = false;
    if ( $.isPlainObject(data) || $.isArray(data) )
    {
        isIterable = true;
        $.each(data, function(content){
            if(typeof content == 'undefined')
            {
                isIterable = false;

                return false;
            }
        })

    }

    return isIterable;
}

/**
 * Performs an operation over an array or object
 *
 * @param Array|Object data
 * @param Function
 */
function foreach(data, callback)
{
    if ( typeof data != 'undefined' && data !== null && !isIterable(data) )
    {
        throw '<data> argument is not iteratable';
    }

    if ( !$.isFunction(callback) )
    {
        throw '<callback> argument is not a function';
    }


    if ( isArray( data ) )
    {
        for ( var i=0 ; i < data.length; i++ )
        {
            var value = callback( data[i], i );

            if ( value === false )
            {
                break;
            }
        }
    }
    else
    {
        for ( i in data ) {
            value = callback( data[i], i );

            if ( value === false )
            {
                break;
            }
        }
    }

    return data;
}

/**
 * Transform date from server format into user custom format
 *
 * @param {string} date
 * @return {string} date
 */
function formatDate( date )
{
    if ( typeof date == 'undefined' )
    {
        var actualDate = new Date();
    }
    else
    {
        date = date.replace(/-/g, "/");
        actualDate = new Date(date);

        if ( typeof actualDate == 'undefined' )
        {
            date = date.replace(/ /g,"T");
            actualDate = new Date(date);
        }
    }

    var customFormat = 'M d, yy';
    var h = actualDate.getHours();
    h = h<10 ? '0'+h : h;

    var m = actualDate.getMinutes();
    m = m<10 ? '0'+m : m;

    var s = actualDate.getSeconds();
    s = s<10 ? '0'+s : s;

    var convertedDate = $.datepicker.formatDate(customFormat+' '+h+':'+m+':'+s,actualDate);

    return convertedDate;
}

function count(input)
{
    var type = typeof input,
        length = 0;

    if (type == 'object')
    {
        if (input instanceof Array)
        {
            type = 'array';
        }
    }
    switch (type)
    {
        case 'array':
        case 'object':
            length = 0;
            for (var i in input)
            {
                if (typeof input[i] != 'undefined')
                {
                    length++;
                }
            }
            break;
        case 'undefined':
            length = 0;
            break;
        default:
            length = input ? (typeof input.length != 'undefined' ? input.length : 1) : 0;
    }

    return length;
}

/**
 * Sort an array of objects by a key
 */
function sortByKey( array, key, sortCallback )
{
    if (typeof sortCallback == 'undefined' || !$.isFunction(sortCallback))
    {
        sortCallback = function( a, b ){
            return a[key].toLowerCase().localeCompare(b[key].toLowerCase());
        }
    }

    array.sort(sortCallback);

    return array;
}

function getArguments( fnArgs, from )
{
    var args = [];
    for (var i=from; i < fnArgs.length; i++)
    {
        args.push(fnArgs[i]);
    }

    return args;
}

var isEventSupported = (function(){
    var TAGNAMES = {
        'select':'input',
        'change':'input',
        'submit':'form',
        'reset':'form',
        'error':'img',
        'load':'img',
        'abort':'img'
    }
    function isEventSupported(eventName) {
        var el = document.createElement(TAGNAMES[eventName] || 'div');
        eventName = 'on' + eventName;
        var isSupported = (eventName in el);
        if (!isSupported) {
            el.setAttribute(eventName, 'return;');
            isSupported = typeof el[eventName] == 'function';
        }
        el = null;
        return isSupported;
    }
    return isEventSupported;
})();

////////////////////////////////////////////////////////////////////////////////
// GLOBAL OBJECTS //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

var Cookie =
{
    get: function(c_name)
    {
        var i,x,y,ARRcookies=document.cookie.split(";");
        for (i=0;i<ARRcookies.length;i++)
        {
            x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
            y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
            x=x.replace(/^\s+|\s+$/g,"");
            if (x==c_name)
            {
                return unescape(y);
            }
        }
    },

    set: function(c_name,value,exdays)
    {
        var exdate=new Date();
        exdate.setDate(exdate.getDate() + exdays);
        var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
        document.cookie=c_name + "=" + c_value;
    }
}

////////////////////////////////////////////////////////////////////////////////

if ( typeof JSON == 'undefined' )
{
    var JSON = {};
}

JSON = $.extend( JSON, {
    useHasOwn : ({}.hasOwnProperty ? true : false),
    pad : function(n) {
        return n < 10 ? "0" + n : n;
    },
    m : {
        "\b": '\\b',
        "\t": '\\t',
        "\n": '\\n',
        "\f": '\\f',
        "\r": '\\r',
        '"' : '\\"',
        "\\": '\\\\'
    },
    encodeString : function(s){
        if (/["\\\x00-\x1f]/.test(s)) {
            return '"' + s.replace(/([\x00-\x1f\\"])/g, function(a, b) {
                var c = JSON.m[b];
                if(c){
                    return c;
                }
                c = b.charCodeAt();
                return "\\u00" +
                Math.floor(c / 16).toString(16) +
                (c % 16).toString(16);
            }) + '"';
        }
        return '"' + s + '"';
    },
    encodeArray : function(o){
        var a = ["["], b, i, l = o.length, v;
        for (i = 0; i < l; i += 1) {
            v = o[i];
            switch (typeof v) {
                case "undefined":
                case "function":
                case "unknown":
                    break;
                default:
                    if (b) {
                        a.push(',');
                    }
                    a.push(v === null ? "null" : this.encode(v));
                    b = true;
            }
        }
        a.push("]");
        return a.join("");
    },
    encodeDate : function(o){
        return '"' + o.getFullYear() + "-" +
        pad(o.getMonth() + 1) + "-" +
        pad(o.getDate()) + "T" +
        pad(o.getHours()) + ":" +
        pad(o.getMinutes()) + ":" +
        pad(o.getSeconds()) + '"';
    },
    encode : function(o){
        if(typeof o == "undefined" || o === null){
            return "null";
        }else if(o instanceof Array){
            return this.encodeArray(o);
        }else if(o instanceof Date){
            return this.encodeDate(o);
        }else if(typeof o == "string"){
            return this.encodeString(o);
        }else if(typeof o == "number"){
            return isFinite(o) ? String(o) : "null";
        }else if(typeof o == "boolean"){
            return String(o);
        }else {
            var self = this;
            var a = ["{"], b, i, v;
            for (i in o) {
                if(!this.useHasOwn || o.hasOwnProperty(i)) {
                    v = o[i];
                    switch (typeof v) {
                        case "undefined":
                        case "function":
                        case "unknown":
                            break;
                        default:
                            if(b){
                                a.push(',');
                            }
                            a.push(self.encode(i), ":",
                                v === null ? "null" : self.encode(v));
                            b = true;
                    }
                }
            }
            a.push("}");
            return a.join("");
        }
    },
    decode : function(json){
        return eval("(" + json + ')');
    }
});

////////////////////////////////////////////////////////////////////////////////

var Timer =
{
    _timers: [],

    getTimestamp: function()
    {
        return new Date().valueOf();
    },

    start: function( name )
    {
        this._timers[ name ] = this.getTimestamp();
    },

    end: function( name )
    {
        var time = this.getTimestamp();
        var interval = time - this._timers[ name ];
        var message = name + ': ' + interval + 'ms';
        if(console == undefined){
            alert(message);
        }else{
            console.log(message);
        }
    }
};

////////////////////////////////////////////////////////////////////////////////
// BASIC INHERITANCE ///////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;
  // The base Class implementation (does nothing)
  this.Class = function(){};

  // Create a new Class that inherits from this class
  Class.extend = function(prop) {
    var _super = this.prototype;

    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;

    // Copy the properties over onto the new prototype
    for (var name in prop) {
      // Check if we're overwriting an existing function
      prototype[name] = typeof prop[name] == "function" &&
        typeof _super[name] == "function" && fnTest.test(prop[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;

            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];

            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);
            this._super = tmp;

            return ret;
          };
        })(name, prop[name]) :
        prop[name];
    }

    // The dummy class constructor
    function Class() {
      // All construction is actually done in the init method
      if ( !initializing && this.init )
        this.init.apply(this, arguments);
    }

    // Populate our constructed prototype object
    Class.prototype = prototype;

    // Enforce the constructor to be what we expect
    Class.prototype.constructor = Class;

    // And make this class extendable
    Class.extend = arguments.callee;

    return Class;
  };
})();

////////////////////////////////////////////////////////////////////////////////
// XRegExp 1.5.1 ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// XRegExp 1.5.1
// (c) 2007-2012 Steven Levithan
// MIT License
// <http://xregexp.com>
// Provides an augmented, extensible, cross-browser implementation of regular expressions,
// including support for additional syntax, flags, and methods

var XRegExp;

if (XRegExp) {
    // Avoid running twice, since that would break references to native globals
    throw Error("can't load XRegExp twice in the same frame");
}

// Run within an anonymous function to protect variables and avoid new globals
(function (undefined) {

    //---------------------------------
    //  Constructor
    //---------------------------------

    // Accepts a pattern and flags; returns a new, extended `RegExp` object. Differs from a native
    // regular expression in that additional syntax and flags are supported and cross-browser
    // syntax inconsistencies are ameliorated. `XRegExp(/regex/)` clones an existing regex and
    // converts to type XRegExp
    XRegExp = function (pattern, flags) {
        var output = [],
            currScope = XRegExp.OUTSIDE_CLASS,
            pos = 0,
            context, tokenResult, match, chr, regex;

        if (XRegExp.isRegExp(pattern)) {
            if (flags !== undefined)
                throw TypeError("can't supply flags when constructing one RegExp from another");
            return clone(pattern);
        }
        // Tokens become part of the regex construction process, so protect against infinite
        // recursion when an XRegExp is constructed within a token handler or trigger
        if (isInsideConstructor)
            throw Error("can't call the XRegExp constructor within token definition functions");

        flags = flags || "";
        context = { // `this` object for custom tokens
            hasNamedCapture: false,
            captureNames: [],
            hasFlag: function (flag) {return flags.indexOf(flag) > -1;},
            setFlag: function (flag) {flags += flag;}
        };

        while (pos < pattern.length) {
            // Check for custom tokens at the current position
            tokenResult = runTokens(pattern, pos, currScope, context);

            if (tokenResult) {
                output.push(tokenResult.output);
                pos += (tokenResult.match[0].length || 1);
            } else {
                // Check for native multicharacter metasequences (excluding character classes) at
                // the current position
                if (match = nativ.exec.call(nativeTokens[currScope], pattern.slice(pos))) {
                    output.push(match[0]);
                    pos += match[0].length;
                } else {
                    chr = pattern.charAt(pos);
                    if (chr === "[")
                        currScope = XRegExp.INSIDE_CLASS;
                    else if (chr === "]")
                        currScope = XRegExp.OUTSIDE_CLASS;
                    // Advance position one character
                    output.push(chr);
                    pos++;
                }
            }
        }

        regex = RegExp(output.join(""), nativ.replace.call(flags, flagClip, ""));
        regex._xregexp = {
            source: pattern,
            captureNames: context.hasNamedCapture ? context.captureNames : null
        };
        return regex;
    };


    //---------------------------------
    //  Public properties
    //---------------------------------

    XRegExp.version = "1.5.1";

    // Token scope bitflags
    XRegExp.INSIDE_CLASS = 1;
    XRegExp.OUTSIDE_CLASS = 2;


    //---------------------------------
    //  Private variables
    //---------------------------------

    var replacementToken = /\$(?:(\d\d?|[$&`'])|{([$\w]+)})/g,
        flagClip = /[^gimy]+|([\s\S])(?=[\s\S]*\1)/g, // Nonnative and duplicate flags
        quantifier = /^(?:[?*+]|{\d+(?:,\d*)?})\??/,
        isInsideConstructor = false,
        tokens = [],
        // Copy native globals for reference ("native" is an ES3 reserved keyword)
        nativ = {
            exec: RegExp.prototype.exec,
            test: RegExp.prototype.test,
            match: String.prototype.match,
            replace: String.prototype.replace,
            split: String.prototype.split
        },
        compliantExecNpcg = nativ.exec.call(/()??/, "")[1] === undefined, // check `exec` handling of nonparticipating capturing groups
        compliantLastIndexIncrement = function () {
            var x = /^/g;
            nativ.test.call(x, "");
            return !x.lastIndex;
        }(),
        hasNativeY = RegExp.prototype.sticky !== undefined,
        nativeTokens = {};

    // `nativeTokens` match native multicharacter metasequences only (including deprecated octals,
    // excluding character classes)
    nativeTokens[XRegExp.INSIDE_CLASS] = /^(?:\\(?:[0-3][0-7]{0,2}|[4-7][0-7]?|x[\dA-Fa-f]{2}|u[\dA-Fa-f]{4}|c[A-Za-z]|[\s\S]))/;
    nativeTokens[XRegExp.OUTSIDE_CLASS] = /^(?:\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9]\d*|x[\dA-Fa-f]{2}|u[\dA-Fa-f]{4}|c[A-Za-z]|[\s\S])|\(\?[:=!]|[?*+]\?|{\d+(?:,\d*)?}\??)/;


    //---------------------------------
    //  Public methods
    //---------------------------------

    // Lets you extend or change XRegExp syntax and create custom flags. This is used internally by
    // the XRegExp library and can be used to create XRegExp plugins. This function is intended for
    // users with advanced knowledge of JavaScript's regular expression syntax and behavior. It can
    // be disabled by `XRegExp.freezeTokens`
    XRegExp.addToken = function (regex, handler, scope, trigger) {
        tokens.push({
            pattern: clone(regex, "g" + (hasNativeY ? "y" : "")),
            handler: handler,
            scope: scope || XRegExp.OUTSIDE_CLASS,
            trigger: trigger || null
        });
    };

    // Accepts a pattern and flags; returns an extended `RegExp` object. If the pattern and flag
    // combination has previously been cached, the cached copy is returned; otherwise the newly
    // created regex is cached
    XRegExp.cache = function (pattern, flags) {
        var key = pattern + "/" + (flags || "");
        return XRegExp.cache[key] || (XRegExp.cache[key] = XRegExp(pattern, flags));
    };

    // Accepts a `RegExp` instance; returns a copy with the `/g` flag set. The copy has a fresh
    // `lastIndex` (set to zero). If you want to copy a regex without forcing the `global`
    // property, use `XRegExp(regex)`. Do not use `RegExp(regex)` because it will not preserve
    // special properties required for named capture
    XRegExp.copyAsGlobal = function (regex) {
        return clone(regex, "g");
    };

    // Accepts a string; returns the string with regex metacharacters escaped. The returned string
    // can safely be used at any point within a regex to match the provided literal string. Escaped
    // characters are [ ] { } ( ) * + ? - . , \ ^ $ | # and whitespace
    XRegExp.escape = function (str) {
        return str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
    };

    // Accepts a string to search, regex to search with, position to start the search within the
    // string (default: 0), and an optional Boolean indicating whether matches must start at-or-
    // after the position or at the specified position only. This function ignores the `lastIndex`
    // of the provided regex in its own handling, but updates the property for compatibility
    XRegExp.execAt = function (str, regex, pos, anchored) {
        var r2 = clone(regex, "g" + ((anchored && hasNativeY) ? "y" : "")),
            match;
        r2.lastIndex = pos = pos || 0;
        match = r2.exec(str); // Run the altered `exec` (required for `lastIndex` fix, etc.)
        if (anchored && match && match.index !== pos)
            match = null;
        if (regex.global)
            regex.lastIndex = match ? r2.lastIndex : 0;
        return match;
    };

    // Breaks the unrestorable link to XRegExp's private list of tokens, thereby preventing
    // syntax and flag changes. Should be run after XRegExp and any plugins are loaded
    XRegExp.freezeTokens = function () {
        XRegExp.addToken = function () {
            throw Error("can't run addToken after freezeTokens");
        };
    };

    // Accepts any value; returns a Boolean indicating whether the argument is a `RegExp` object.
    // Note that this is also `true` for regex literals and regexes created by the `XRegExp`
    // constructor. This works correctly for variables created in another frame, when `instanceof`
    // and `constructor` checks would fail to work as intended
    XRegExp.isRegExp = function (o) {
        return Object.prototype.toString.call(o) === "[object RegExp]";
    };

    // Executes `callback` once per match within `str`. Provides a simpler and cleaner way to
    // iterate over regex matches compared to the traditional approaches of subverting
    // `String.prototype.replace` or repeatedly calling `exec` within a `while` loop
    XRegExp.iterate = function (str, regex, callback, context) {
        var r2 = clone(regex, "g"),
            i = -1, match;
        while (match = r2.exec(str)) { // Run the altered `exec` (required for `lastIndex` fix, etc.)
            if (regex.global)
                regex.lastIndex = r2.lastIndex; // Doing this to follow expectations if `lastIndex` is checked within `callback`
            callback.call(context, match, ++i, str, regex);
            if (r2.lastIndex === match.index)
                r2.lastIndex++;
        }
        if (regex.global)
            regex.lastIndex = 0;
    };

    // Accepts a string and an array of regexes; returns the result of using each successive regex
    // to search within the matches of the previous regex. The array of regexes can also contain
    // objects with `regex` and `backref` properties, in which case the named or numbered back-
    // references specified are passed forward to the next regex or returned. E.g.:
    // var xregexpImgFileNames = XRegExp.matchChain(html, [
    //     {regex: /<img\b([^>]+)>/i, backref: 1}, // <img> tag attributes
    //     {regex: XRegExp('(?ix) \\s src=" (?<src> [^"]+ )'), backref: "src"}, // src attribute values
    //     {regex: XRegExp("^http://xregexp\\.com(/[^#?]+)", "i"), backref: 1}, // xregexp.com paths
    //     /[^\/]+$/ // filenames (strip directory paths)
    // ]);
    XRegExp.matchChain = function (str, chain) {
        return function recurseChain (values, level) {
            var item = chain[level].regex ? chain[level] : {regex: chain[level]},
                regex = clone(item.regex, "g"),
                matches = [], i;
            for (i = 0; i < values.length; i++) {
                XRegExp.iterate(values[i], regex, function (match) {
                    matches.push(item.backref ? (match[item.backref] || "") : match[0]);
                });
            }
            return ((level === chain.length - 1) || !matches.length) ?
                matches : recurseChain(matches, level + 1);
        }([str], 0);
    };


    //---------------------------------
    //  New RegExp prototype methods
    //---------------------------------

    // Accepts a context object and arguments array; returns the result of calling `exec` with the
    // first value in the arguments array. the context is ignored but is accepted for congruity
    // with `Function.prototype.apply`
    RegExp.prototype.apply = function (context, args) {
        return this.exec(args[0]);
    };

    // Accepts a context object and string; returns the result of calling `exec` with the provided
    // string. the context is ignored but is accepted for congruity with `Function.prototype.call`
    RegExp.prototype.call = function (context, str) {
        return this.exec(str);
    };


    //---------------------------------
    //  Overriden native methods
    //---------------------------------

    // Adds named capture support (with backreferences returned as `result.name`), and fixes two
    // cross-browser issues per ES3:
    // - Captured values for nonparticipating capturing groups should be returned as `undefined`,
    //   rather than the empty string.
    // - `lastIndex` should not be incremented after zero-length matches.
    RegExp.prototype.exec = function (str) {
        var match, name, r2, origLastIndex;
        if (!this.global)
            origLastIndex = this.lastIndex;
        match = nativ.exec.apply(this, arguments);
        if (match) {
            // Fix browsers whose `exec` methods don't consistently return `undefined` for
            // nonparticipating capturing groups
            if (!compliantExecNpcg && match.length > 1 && indexOf(match, "") > -1) {
                r2 = RegExp(this.source, nativ.replace.call(getNativeFlags(this), "g", ""));
                // Using `str.slice(match.index)` rather than `match[0]` in case lookahead allowed
                // matching due to characters outside the match
                nativ.replace.call((str + "").slice(match.index), r2, function () {
                    for (var i = 1; i < arguments.length - 2; i++) {
                        if (arguments[i] === undefined)
                            match[i] = undefined;
                    }
                });
            }
            // Attach named capture properties
            if (this._xregexp && this._xregexp.captureNames) {
                for (var i = 1; i < match.length; i++) {
                    name = this._xregexp.captureNames[i - 1];
                    if (name)
                       match[name] = match[i];
                }
            }
            // Fix browsers that increment `lastIndex` after zero-length matches
            if (!compliantLastIndexIncrement && this.global && !match[0].length && (this.lastIndex > match.index))
                this.lastIndex--;
        }
        if (!this.global)
            this.lastIndex = origLastIndex; // Fix IE, Opera bug (last tested IE 9.0.5, Opera 11.61 on Windows)
        return match;
    };

    // Fix browser bugs in native method
    RegExp.prototype.test = function (str) {
        // Use the native `exec` to skip some processing overhead, even though the altered
        // `exec` would take care of the `lastIndex` fixes
        var match, origLastIndex;
        if (!this.global)
            origLastIndex = this.lastIndex;
        match = nativ.exec.call(this, str);
        // Fix browsers that increment `lastIndex` after zero-length matches
        if (match && !compliantLastIndexIncrement && this.global && !match[0].length && (this.lastIndex > match.index))
            this.lastIndex--;
        if (!this.global)
            this.lastIndex = origLastIndex; // Fix IE, Opera bug (last tested IE 9.0.5, Opera 11.61 on Windows)
        return !!match;
    };

    // Adds named capture support and fixes browser bugs in native method
    String.prototype.match = function (regex) {
        if (!XRegExp.isRegExp(regex))
            regex = RegExp(regex); // Native `RegExp`
        if (regex.global) {
            var result = nativ.match.apply(this, arguments);
            regex.lastIndex = 0; // Fix IE bug
            return result;
        }
        return regex.exec(this); // Run the altered `exec`
    };

    // Adds support for `${n}` tokens for named and numbered backreferences in replacement text,
    // and provides named backreferences to replacement functions as `arguments[0].name`. Also
    // fixes cross-browser differences in replacement text syntax when performing a replacement
    // using a nonregex search value, and the value of replacement regexes' `lastIndex` property
    // during replacement iterations. Note that this doesn't support SpiderMonkey's proprietary
    // third (`flags`) parameter
    String.prototype.replace = function (search, replacement) {
        var isRegex = XRegExp.isRegExp(search),
            captureNames, result, str, origLastIndex;

        // There are too many combinations of search/replacement types/values and browser bugs that
        // preclude passing to native `replace`, so don't try
        //if (...)
        //    return nativ.replace.apply(this, arguments);

        if (isRegex) {
            if (search._xregexp)
                captureNames = search._xregexp.captureNames; // Array or `null`
            if (!search.global)
                origLastIndex = search.lastIndex;
        } else {
            search = search + ""; // Type conversion
        }

        if (Object.prototype.toString.call(replacement) === "[object Function]") {
            result = nativ.replace.call(this + "", search, function () {
                if (captureNames) {
                    // Change the `arguments[0]` string primitive to a String object which can store properties
                    arguments[0] = new String(arguments[0]);
                    // Store named backreferences on `arguments[0]`
                    for (var i = 0; i < captureNames.length; i++) {
                        if (captureNames[i])
                            arguments[0][captureNames[i]] = arguments[i + 1];
                    }
                }
                // Update `lastIndex` before calling `replacement` (fix browsers)
                if (isRegex && search.global)
                    search.lastIndex = arguments[arguments.length - 2] + arguments[0].length;
                return replacement.apply(null, arguments);
            });
        } else {
            str = this + ""; // Type conversion, so `args[args.length - 1]` will be a string (given nonstring `this`)
            result = nativ.replace.call(str, search, function () {
                var args = arguments; // Keep this function's `arguments` available through closure
                return nativ.replace.call(replacement + "", replacementToken, function ($0, $1, $2) {
                    // Numbered backreference (without delimiters) or special variable
                    if ($1) {
                        switch ($1) {
                            case "$":return "$";
                            case "&":return args[0];
                            case "`":return args[args.length - 1].slice(0, args[args.length - 2]);
                            case "'":return args[args.length - 1].slice(args[args.length - 2] + args[0].length);
                            // Numbered backreference
                            default:
                                // What does "$10" mean?
                                // - Backreference 10, if 10 or more capturing groups exist
                                // - Backreference 1 followed by "0", if 1-9 capturing groups exist
                                // - Otherwise, it's the string "$10"
                                // Also note:
                                // - Backreferences cannot be more than two digits (enforced by `replacementToken`)
                                // - "$01" is equivalent to "$1" if a capturing group exists, otherwise it's the string "$01"
                                // - There is no "$0" token ("$&" is the entire match)
                                var literalNumbers = "";
                                $1 = +$1; // Type conversion; drop leading zero
                                if (!$1) // `$1` was "0" or "00"
                                    return $0;
                                while ($1 > args.length - 3) {
                                    literalNumbers = String.prototype.slice.call($1, -1) + literalNumbers;
                                    $1 = Math.floor($1 / 10); // Drop the last digit
                                }
                                return ($1 ? args[$1] || "" : "$") + literalNumbers;
                        }
                    // Named backreference or delimited numbered backreference
                    } else {
                        // What does "${n}" mean?
                        // - Backreference to numbered capture n. Two differences from "$n":
                        //   - n can be more than two digits
                        //   - Backreference 0 is allowed, and is the entire match
                        // - Backreference to named capture n, if it exists and is not a number overridden by numbered capture
                        // - Otherwise, it's the string "${n}"
                        var n = +$2; // Type conversion; drop leading zeros
                        if (n <= args.length - 3)
                            return args[n];
                        n = captureNames ? indexOf(captureNames, $2) : -1;
                        return n > -1 ? args[n + 1] : $0;
                    }
                });
            });
        }

        if (isRegex) {
            if (search.global)
                search.lastIndex = 0; // Fix IE, Safari bug (last tested IE 9.0.5, Safari 5.1.2 on Windows)
            else
                search.lastIndex = origLastIndex; // Fix IE, Opera bug (last tested IE 9.0.5, Opera 11.61 on Windows)
        }

        return result;
    };

    // A consistent cross-browser, ES3 compliant `split`
    String.prototype.split = function (s /* separator */, limit) {
        // If separator `s` is not a regex, use the native `split`
        if (!XRegExp.isRegExp(s))
            return nativ.split.apply(this, arguments);

        var str = this + "", // Type conversion
            output = [],
            lastLastIndex = 0,
            match, lastLength;

        // Behavior for `limit`: if it's...
        // - `undefined`: No limit
        // - `NaN` or zero: Return an empty array
        // - A positive number: Use `Math.floor(limit)`
        // - A negative number: No limit
        // - Other: Type-convert, then use the above rules
        if (limit === undefined || +limit < 0) {
            limit = Infinity;
        } else {
            limit = Math.floor(+limit);
            if (!limit)
                return [];
        }

        // This is required if not `s.global`, and it avoids needing to set `s.lastIndex` to zero
        // and restore it to its original value when we're done using the regex
        s = XRegExp.copyAsGlobal(s);

        while (match = s.exec(str)) { // Run the altered `exec` (required for `lastIndex` fix, etc.)
            if (s.lastIndex > lastLastIndex) {
                output.push(str.slice(lastLastIndex, match.index));

                if (match.length > 1 && match.index < str.length)
                    Array.prototype.push.apply(output, match.slice(1));

                lastLength = match[0].length;
                lastLastIndex = s.lastIndex;

                if (output.length >= limit)
                    break;
            }

            if (s.lastIndex === match.index)
                s.lastIndex++;
        }

        if (lastLastIndex === str.length) {
            if (!nativ.test.call(s, "") || lastLength)
                output.push("");
        } else {
            output.push(str.slice(lastLastIndex));
        }

        return output.length > limit ? output.slice(0, limit) : output;
    };


    //---------------------------------
    //  Private helper functions
    //---------------------------------

    // Supporting function for `XRegExp`, `XRegExp.copyAsGlobal`, etc. Returns a copy of a `RegExp`
    // instance with a fresh `lastIndex` (set to zero), preserving properties required for named
    // capture. Also allows adding new flags in the process of copying the regex
    function clone (regex, additionalFlags) {
        if (!XRegExp.isRegExp(regex))
            throw TypeError("type RegExp expected");
        var x = regex._xregexp;
        regex = XRegExp(regex.source, getNativeFlags(regex) + (additionalFlags || ""));
        if (x) {
            regex._xregexp = {
                source: x.source,
                captureNames: x.captureNames ? x.captureNames.slice(0) : null
            };
        }
        return regex;
    }

    function getNativeFlags (regex) {
        return (regex.global     ? "g" : "") +
               (regex.ignoreCase ? "i" : "") +
               (regex.multiline  ? "m" : "") +
               (regex.extended   ? "x" : "") + // Proposed for ES4; included in AS3
               (regex.sticky     ? "y" : "");
    }

    function runTokens (pattern, index, scope, context) {
        var i = tokens.length,
            result, match, t;
        // Protect against constructing XRegExps within token handler and trigger functions
        isInsideConstructor = true;
        // Must reset `isInsideConstructor`, even if a `trigger` or `handler` throws
        try {
            while (i--) { // Run in reverse order
                t = tokens[i];
                if ((scope & t.scope) && (!t.trigger || t.trigger.call(context))) {
                    t.pattern.lastIndex = index;
                    match = t.pattern.exec(pattern); // Running the altered `exec` here allows use of named backreferences, etc.
                    if (match && match.index === index) {
                        result = {
                            output: t.handler.call(context, match, scope),
                            match: match
                        };
                        break;
                    }
                }
            }
        } catch (err) {
            throw err;
        } finally {
            isInsideConstructor = false;
        }
        return result;
    }

    function indexOf (array, item, from) {
        if (Array.prototype.indexOf) // Use the native array method if available
            return array.indexOf(item, from);
        for (var i = from || 0; i < array.length; i++) {
            if (array[i] === item)
                return i;
        }
        return -1;
    }


    //---------------------------------
    //  Built-in tokens
    //---------------------------------

    // Augment XRegExp's regular expression syntax and flags. Note that when adding tokens, the
    // third (`scope`) argument defaults to `XRegExp.OUTSIDE_CLASS`

    // Comment pattern: (?# )
    XRegExp.addToken(
        /\(\?#[^)]*\)/,
        function (match) {
            // Keep tokens separated unless the following token is a quantifier
            return nativ.test.call(quantifier, match.input.slice(match.index + match[0].length)) ? "" : "(?:)";
        }
    );

    // Capturing group (match the opening parenthesis only).
    // Required for support of named capturing groups
    XRegExp.addToken(
        /\((?!\?)/,
        function () {
            this.captureNames.push(null);
            return "(";
        }
    );

    // Named capturing group (match the opening delimiter only): (?<name>
    XRegExp.addToken(
        /\(\?<([$\w]+)>/,
        function (match) {
            this.captureNames.push(match[1]);
            this.hasNamedCapture = true;
            return "(";
        }
    );

    // Named backreference: \k<name>
    XRegExp.addToken(
        /\\k<([\w$]+)>/,
        function (match) {
            var index = indexOf(this.captureNames, match[1]);
            // Keep backreferences separate from subsequent literal numbers. Preserve back-
            // references to named groups that are undefined at this point as literal strings
            return index > -1 ?
                "\\" + (index + 1) + (isNaN(match.input.charAt(match.index + match[0].length)) ? "" : "(?:)") :
                match[0];
        }
    );

    // Empty character class: [] or [^]
    XRegExp.addToken(
        /\[\^?]/,
        function (match) {
            // For cross-browser compatibility with ES3, convert [] to \b\B and [^] to [\s\S].
            // (?!) should work like \b\B, but is unreliable in Firefox
            return match[0] === "[]" ? "\\b\\B" : "[\\s\\S]";
        }
    );

    // Mode modifier at the start of the pattern only, with any combination of flags imsx: (?imsx)
    // Does not support x(?i), (?-i), (?i-m), (?i: ), (?i)(?m), etc.
    XRegExp.addToken(
        /^\(\?([imsx]+)\)/,
        function (match) {
            this.setFlag(match[1]);
            return "";
        }
    );

    // Whitespace and comments, in free-spacing (aka extended) mode only
    XRegExp.addToken(
        /(?:\s+|#.*)+/,
        function (match) {
            // Keep tokens separated unless the following token is a quantifier
            return nativ.test.call(quantifier, match.input.slice(match.index + match[0].length)) ? "" : "(?:)";
        },
        XRegExp.OUTSIDE_CLASS,
        function () {return this.hasFlag("x");}
    );

    // Dot, in dotall (aka singleline) mode only
    XRegExp.addToken(
        /\./,
        function () {return "[\\s\\S]";},
        XRegExp.OUTSIDE_CLASS,
        function () {return this.hasFlag("s");}
    );


    //---------------------------------
    //  Backward compatibility
    //---------------------------------

    // Uncomment the following block for compatibility with XRegExp 1.0-1.2:
    /*
    XRegExp.matchWithinChain = XRegExp.matchChain;
    RegExp.prototype.addFlags = function (s) {return clone(this, s);};
    RegExp.prototype.execAll = function (s) {var r = []; XRegExp.iterate(s, this, function (m) {r.push(m);}); return r;};
    RegExp.prototype.forEachExec = function (s, f, c) {return XRegExp.iterate(s, this, f, c);};
    RegExp.prototype.validate = function (s) {var r = RegExp("^(?:" + this.source + ")$(?!\\s)", getNativeFlags(this)); if (this.global) this.lastIndex = 0; return s.search(r) === 0;};
    */

})();

/*
XRegExp Unicode plugin base 0.6
(c) 2008-2012 Steven Levithan
MIT License
<http://xregexp.com>

Uses the Unicode 6.1 character database:
<http://unicode.org/Public/6.1.0/ucd/>

The Unicode plugin base adds support for the \p{L} token only (Unicode category
Letter). Plugin packages are available that add support for the remaining
Unicode categories, as well as Unicode scripts and blocks.

All Unicode tokens can be inverted by using an uppercase P; e.g., \P{L} matches
any character not in Unicode's Letter category. Negated Unicode tokens are not
supported within character classes.

Letter case, spaces, hyphens, and underscores are ignored when comparing
Unicode token names.
*/

var XRegExp;

if (!XRegExp) {
    throw ReferenceError("XRegExp must be loaded before the Unicode plugin");
}

(function () {

    var unicode = {}; // protected storage for package tokens

    XRegExp.addUnicodePackage = function (pack) {
        var codePoint = /\w{4}/g,
            clip = /[- _]+/g,
            name, p;
        for (p in pack) {
            if (pack.hasOwnProperty(p)) {
                name = p.replace(clip, "").toLowerCase();
                // disallow overriding properties that have already been added
                if (!unicode.hasOwnProperty(name)) {
                    unicode[name] = pack[p].replace(codePoint, "\\u$&");
                }
            }
        }
    };

    XRegExp.addToken(
        /\\([pP]){(\^?)([^}]*)}/,
        function (match, scope) {
            var negated = (match[1] === "P" || match[2]),
                item = match[3].replace(/[- _]+/g, "").toLowerCase();

            // \p{}, \P{}, and \p{^} are valid, but the double negative \P{^} isn't
            if (match[1] === "P" && match[2])
                throw SyntaxError("erroneous characters: " + match[0]);
            if (negated && scope === XRegExp.INSIDE_CLASS)
                throw SyntaxError("not supported in character classes: \\" + match[1] + "{" + match[2] + "...}");
            if (!unicode.hasOwnProperty(item))
                throw SyntaxError("invalid or unsupported Unicode item: " + match[0]);

            return scope === XRegExp.OUTSIDE_CLASS ?
                "[" + (negated ? "^" : "") + unicode[item] + "]" :
                unicode[item];
        },
        XRegExp.INSIDE_CLASS | XRegExp.OUTSIDE_CLASS
    );

    XRegExp.addUnicodePackage({
        L: "0041-005A0061-007A00AA00B500BA00C0-00D600D8-00F600F8-02C102C6-02D102E0-02E402EC02EE0370-037403760377037A-037D03860388-038A038C038E-03A103A3-03F503F7-0481048A-05270531-055605590561-058705D0-05EA05F0-05F20620-064A066E066F0671-06D306D506E506E606EE06EF06FA-06FC06FF07100712-072F074D-07A507B107CA-07EA07F407F507FA0800-0815081A082408280840-085808A008A2-08AC0904-0939093D09500958-09610971-09770979-097F0985-098C098F09900993-09A809AA-09B009B209B6-09B909BD09CE09DC09DD09DF-09E109F009F10A05-0A0A0A0F0A100A13-0A280A2A-0A300A320A330A350A360A380A390A59-0A5C0A5E0A72-0A740A85-0A8D0A8F-0A910A93-0AA80AAA-0AB00AB20AB30AB5-0AB90ABD0AD00AE00AE10B05-0B0C0B0F0B100B13-0B280B2A-0B300B320B330B35-0B390B3D0B5C0B5D0B5F-0B610B710B830B85-0B8A0B8E-0B900B92-0B950B990B9A0B9C0B9E0B9F0BA30BA40BA8-0BAA0BAE-0BB90BD00C05-0C0C0C0E-0C100C12-0C280C2A-0C330C35-0C390C3D0C580C590C600C610C85-0C8C0C8E-0C900C92-0CA80CAA-0CB30CB5-0CB90CBD0CDE0CE00CE10CF10CF20D05-0D0C0D0E-0D100D12-0D3A0D3D0D4E0D600D610D7A-0D7F0D85-0D960D9A-0DB10DB3-0DBB0DBD0DC0-0DC60E01-0E300E320E330E40-0E460E810E820E840E870E880E8A0E8D0E94-0E970E99-0E9F0EA1-0EA30EA50EA70EAA0EAB0EAD-0EB00EB20EB30EBD0EC0-0EC40EC60EDC-0EDF0F000F40-0F470F49-0F6C0F88-0F8C1000-102A103F1050-1055105A-105D106110651066106E-10701075-1081108E10A0-10C510C710CD10D0-10FA10FC-1248124A-124D1250-12561258125A-125D1260-1288128A-128D1290-12B012B2-12B512B8-12BE12C012C2-12C512C8-12D612D8-13101312-13151318-135A1380-138F13A0-13F41401-166C166F-167F1681-169A16A0-16EA1700-170C170E-17111720-17311740-17511760-176C176E-17701780-17B317D717DC1820-18771880-18A818AA18B0-18F51900-191C1950-196D1970-19741980-19AB19C1-19C71A00-1A161A20-1A541AA71B05-1B331B45-1B4B1B83-1BA01BAE1BAF1BBA-1BE51C00-1C231C4D-1C4F1C5A-1C7D1CE9-1CEC1CEE-1CF11CF51CF61D00-1DBF1E00-1F151F18-1F1D1F20-1F451F48-1F4D1F50-1F571F591F5B1F5D1F5F-1F7D1F80-1FB41FB6-1FBC1FBE1FC2-1FC41FC6-1FCC1FD0-1FD31FD6-1FDB1FE0-1FEC1FF2-1FF41FF6-1FFC2071207F2090-209C21022107210A-211321152119-211D212421262128212A-212D212F-2139213C-213F2145-2149214E218321842C00-2C2E2C30-2C5E2C60-2CE42CEB-2CEE2CF22CF32D00-2D252D272D2D2D30-2D672D6F2D80-2D962DA0-2DA62DA8-2DAE2DB0-2DB62DB8-2DBE2DC0-2DC62DC8-2DCE2DD0-2DD62DD8-2DDE2E2F300530063031-3035303B303C3041-3096309D-309F30A1-30FA30FC-30FF3105-312D3131-318E31A0-31BA31F0-31FF3400-4DB54E00-9FCCA000-A48CA4D0-A4FDA500-A60CA610-A61FA62AA62BA640-A66EA67F-A697A6A0-A6E5A717-A71FA722-A788A78B-A78EA790-A793A7A0-A7AAA7F8-A801A803-A805A807-A80AA80C-A822A840-A873A882-A8B3A8F2-A8F7A8FBA90A-A925A930-A946A960-A97CA984-A9B2A9CFAA00-AA28AA40-AA42AA44-AA4BAA60-AA76AA7AAA80-AAAFAAB1AAB5AAB6AAB9-AABDAAC0AAC2AADB-AADDAAE0-AAEAAAF2-AAF4AB01-AB06AB09-AB0EAB11-AB16AB20-AB26AB28-AB2EABC0-ABE2AC00-D7A3D7B0-D7C6D7CB-D7FBF900-FA6DFA70-FAD9FB00-FB06FB13-FB17FB1DFB1F-FB28FB2A-FB36FB38-FB3CFB3EFB40FB41FB43FB44FB46-FBB1FBD3-FD3DFD50-FD8FFD92-FDC7FDF0-FDFBFE70-FE74FE76-FEFCFF21-FF3AFF41-FF5AFF66-FFBEFFC2-FFC7FFCA-FFCFFFD2-FFD7FFDA-FFDC"
    });

})();

////////////////////////////////////////////////////////////////////////////////
// extend Date object //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

Date.prototype.createFrom = function( date )
{
    if ( date instanceof Date )
    {
        var time = date.getTime();

        this.setTime( time );
    }
    else if ( isPlainObject( date ) )
    {
        if ( typeof date.year != 'undefined' )
        {
            this.setYear( date.year );
        }

        if ( typeof date.month != 'undefined' )
        {
            this.setMonth( date.month );
        }

        if ( typeof date.day != 'undefined' )
        {
            this.setDate( date.day );
        }
    }

    return this;
};


////////////////////////////////////////////////////////////////////////////////
// iPAD SUPPORT ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

// Implement dblclick event on tablets
(function(){
    if (isEventSupported('touchstart')) {
        var interval = 500;

        $(document).on('touchend', function(event){
            var $target = $(event.target);

            var firstClick = $target.data('firstClick');
            if (typeof firstClick != 'undefined' && firstClick != null){
                // trigger 'dblclick' event
                $target.removeData('firstClick');
                $target.trigger('dblclick');

            } else {
                $target.data('firstClick', true);

                setTimeout(function(){
                    $target.removeData('firstClick');
                },
                interval);

            }

        })
    }

})();

// Implement longtap event on tablets
(function(){
    if (isEventSupported('touchstart')) {
        var interval = 750,
            timeout;

        $(document).on('touchstart touchend touchcancel touchmove', function(event){
            clearTimeout(timeout);

            if (event.type == 'touchstart')
            {
                timeout = setTimeout(function(){
                    var $target = $(event.target);
                    $target.trigger('longtap');

                }, interval);

            }

        })

    }

})();


// iScroll fix on iPad: attach the events to the body and not to the element
(function(){
    if (typeof iScroll != 'undefined')
    {
        iScroll.prototype._bind = function(type, el, bubble)
        {
            // save the original target
            var target = typeof el == 'undefined' ? this.scroller : el;

            if ( typeof el == 'undefined' || el != window )
            {
                el = document.body;
    }
            el.addEventListener(type, this, !!bubble);

            $(el).data( type+'-event-target', $(target) );
        }

        var handleEvent = iScroll.prototype.handleEvent;
        iScroll.prototype.handleEvent = function(e)
        {
            var $currentTarget = $(e.currentTarget),
                $target = $(e.target),
                eventType = e.type;
            if ( !$target.closest($currentTarget.data(eventType+'-event-target')).size() ) return;

            handleEvent.call( this, e );
        }

    }

})();


////////////////////////////////////////////////////////////////////////////////
//  ////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

function refreshWindow()
{
    window.resizeTo(
        $('body').width() + window.outerWidth - window.innerWidth,
        $('body').height() + window.outerHeight - window.innerHeight)
}

function centerWindow()
{
    moveTo((screen.width - outerWidth)/2, 0);
}

function openApplicationPopup()
{
    window.open( location.protocol + '//' + location.host + '/app', '', 'location=0,resizable=1,scrollbars=0,status=0,titlebar=0');
}


////////////////////////////////////////////////////////////////////////////////
// define/update jQuery methods/////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

(function(){
    // wordk-around for IE8 background-position property
    var oldCss = $.fn.css;
    $.fn.oldCss = oldCss;
    $.fn.css = function(){
        var arg1 = arguments[0]
        if (typeof arg1 != 'undefined' && arg1 == 'background-position')
        {
            // check if property is supported
            var bp = oldCss.call(this, arguments[0]);
            if (typeof bp == 'undefined')
            {
                // get value
                if (arguments.length == 1)
                {
                    var x = oldCss.call(this, 'background-position-x');
                    var y = oldCss.call(this, 'background-position-y');
                    bp = x + ' ' + y;

                    return bp;
                }
                // set value
                else if (arguments.length > 1)
                {
                    bp = arguments[1].split(' ');
                    oldCss.call(this, 'background-position-x', bp[0]);
                    oldCss.call(this, 'background-position-y', bp[1]);

                    return this;
                }
            }
        }

        return oldCss.apply(this, arguments);
    }

    $.fn.outerHTML = function( value ) {
        // Replaces the content
        if( typeof value === "string" ){
            var $this = $(this),
                $parent = $this.parent();

            var replaceElements = function(){

                // For some reason sometimes images are not replaced properly so we get rid of them first
                var $img = $this.find("img");
                if( $img.length > 0 ){
                    $img.remove();
                }

                var element;
                $( value ).map(function(){
                    element = $(this);
                    $this.replaceWith( element );
                })

                return element;

            }

            return replaceElements();

        // Returns the value
        }else{
            return $("<div />").append($(this).clone()).html();
        }
    }

    $.fn.makeExtended = function()
    {
        $(this).each(function(){

            var $el = $(this);
            $el.show();

            var $previous = $(this).prevAll(':visible').eq(0);

            if ($previous.length)
            {
                var top = $previous.position().top + $previous.outerHeight(true);
                $(this).css('top', top).show();
            }

            if (!$el.data('isResizeBound'))
            {
                $el.data('isResizeBound', true);
                $(window).bind('resizestop', function(){
                    if ($el.is(':visible'))
                    {
                        $el.makeExtended();
                    }

                })
            }

        })

        return $(this);

    }

    $.fn.circularShift = function( steps )
    {

    }

    $.serializeObject = function( object )
    {
        var o = {};
        $.each(object, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    /**
     * Performs an action at the specified document event if the target doesn't match any selector
     *
     * @param string event A jQuery event
     * @param array selectors An array with jQuery selectors
     * @param callback action The callback to be executed
     */
    $.documentEvent = function( event, selectors, action )
    {
        // if the event is not specified, it defaults to 'click'
        if ( !Type.isFunction(action) )
        {
            action = selectors;
            selectors = event;
            event = 'click';
        }

        // the selectors must be an array
        if ( !Type.isArray(selectors) )
        {
            selectors = [selectors];
        }

        // bind the event to the document
        $(document).bind( event, function( e ){

            // if the target doesn't match any selector, then the action is performed
            var executeAction = true,
                $target = $(e.target);

            $.each( selectors, function(key){
                // find the closest element that match the selector, starting from target
                if ( $target.closest(selectors[key]).length )
                {
                    executeAction = false;
                    return false;
                }
            })

            if (executeAction)
            {
                // the context of the callback is the target element
                action.call(e.target);
            }
        })
    };

    (function(){
        // Several of the methods in this plugin use code adapated from Prototype
        // Prototype JavaScript framework, version 1.6.0.1
        // (c) 2005-2007 Sam Stephenson
        var regs = {
            undHash: /_|-/,
            colons: /::/,
            words: /([A-Z]+)([A-Z][a-z])/g,
            lowUp: /([a-z\d])([A-Z])/g,
            dash: /([a-z\d])([A-Z])/g,
            replacer: /\{([^\}]+)\}/g,
            dot: /\./
        },
        // gets the nextPart property from current
        // add - if true and nextPart doesnt exist, create it as an empty object
        getNext = function(current, nextPart, add){
            return current[nextPart] !== undefined ? current[nextPart] : ( add && (current[nextPart] = {}) );
        },
        // returns true if the object can have properties (no nulls)
        isContainer = function(current){
            var type = typeof current;
            return current && ( type == 'function' || type == 'object' );
        },
        // a reference
        getObject,
        /**
* @class jQuery.String
* @parent jquerymx.lang
*
* A collection of useful string helpers. Available helpers are:
* <ul>
* <li>[jQuery.String.capitalize|capitalize]: Capitalizes a string (some_string &raquo; Some_string)</li>
* <li>[jQuery.String.camelize|camelize]: Capitalizes a string from something undercored
* (some_string &raquo; someString, some-string &raquo; someString)</li>
* <li>[jQuery.String.classize|classize]: Like [jQuery.String.camelize|camelize],
* but the first part is also capitalized (some_string &raquo; SomeString)</li>
* <li>[jQuery.String.niceName|niceName]: Like [jQuery.String.classize|classize], but a space separates each 'word' (some_string &raquo; Some String)</li>
* <li>[jQuery.String.underscore|underscore]: Underscores a string (SomeString &raquo; some_string)</li>
* <li>[jQuery.String.sub|sub]: Returns a string with {param} replaced values from data.
* <code><pre>
* $.String.sub("foo {bar}",{bar: "far"})
* //-> "foo far"</pre></code>
* </li>
* </ul>
*
*/
        str = $.String = $.extend( $.String || {} , {


            /**
* @function getObject
* Gets an object from a string. It can also modify objects on the
* 'object path' by removing or adding properties.
*
* Foo = {Bar: {Zar: {"Ted"}}}
* $.String.getObject("Foo.Bar.Zar") //-> "Ted"
*
* @param {String} name the name of the object to look for
* @param {Array} [roots] an array of root objects to look for the
* name. If roots is not provided, the window is used.
* @param {Boolean} [add] true to add missing objects to
* the path. false to remove found properties. undefined to
* not modify the root object
* @return {Object} The object.
*/
            getObject : getObject = function( name, roots, add ) {

                // the parts of the name we are looking up
                // ['App','Models','Recipe']
                var parts = name ? name.split(regs.dot) : [],
                length = parts.length,
                current,
                ret,
                i,
                r = 0,
                type;

                // make sure roots is an array
                roots = $.isArray(roots) ? roots : [roots || window];

                if(length == 0){
                    return roots[0];
                }
                // for each root, mark it as current
                while( current = roots[r++] ) {
                    // walk current to the 2nd to last object
                    // or until there is not a container
                    for (i =0; i < length - 1 && isContainer(current); i++ ) {
                        current = getNext(current, parts[i], add);
                    }
                    // if we can get a property from the 2nd to last object
                    if( isContainer(current) ) {

                        // get (and possibly set) the property
                        ret = getNext(current, parts[i], add);

                        // if there is a value, we exit
                        if( ret !== undefined ) {
                            // if add is false, delete the property
                            if ( add === false ) {
                                delete current[parts[i]];
                            }
                            return ret;

                        }
                    }
                }
            },
            /**
* Capitalizes a string
* @param {String} s the string.
* @return {String} a string with the first character capitalized.
*/
            capitalize: function( s, cache ) {
                return s.charAt(0).toUpperCase() + s.substr(1);
            },
            /**
* Capitalizes a string from something undercored. Examples:
* @codestart
* jQuery.String.camelize("one_two") //-> "oneTwo"
* "three-four".camelize() //-> threeFour
* @codeend
* @param {String} s
* @return {String} a the camelized string
*/
            camelize: function( s ) {
                s = str.classize(s);
                return s.charAt(0).toLowerCase() + s.substr(1);
            },
            /**
* Like [jQuery.String.camelize|camelize], but the first part is also capitalized
* @param {String} s
* @return {String} the classized string
*/
            classize: function( s , join) {
                var parts = s.split(regs.undHash),
                i = 0;
                for (; i < parts.length; i++ ) {
                    parts[i] = str.capitalize(parts[i]);
                }

                return parts.join(join || '');
            },
            /**
* Like [jQuery.String.classize|classize], but a space separates each 'word'
* @codestart
* jQuery.String.niceName("one_two") //-> "One Two"
* @codeend
* @param {String} s
* @return {String} the niceName
*/
            niceName: function( s ) {
                return str.classize(s,' ');
            },

            /**
* Underscores a string.
* @codestart
* jQuery.String.underscore("OneTwo") //-> "one_two"
* @codeend
* @param {String} s
* @return {String} the underscored string
*/
            underscore: function( s ) {
                return s.replace(regs.colons, '/').replace(regs.words, '$1_$2').replace(regs.lowUp, '$1_$2').replace(regs.dash, '_').toLowerCase();
            },
            /**
* Returns a string with {param} replaced values from data.
*
* $.String.sub("foo {bar}",{bar: "far"})
* //-> "foo far"
*
* @param {String} s The string to replace
* @param {Object} data The data to be used to look for properties. If it's an array, multiple
* objects can be used.
* @param {Boolean} [remove] if a match is found, remove the property from the object
*/
            sub: function( s, data, remove ) {
                var obs = [],
                remove = typeof remove == 'boolean' ? !remove : remove;
                obs.push(s.replace(regs.replacer, function( whole, inside ) {
                    //convert inside to type
                    var ob = getObject(inside, data, remove);

                    // if a container, push into objs (which will return objects found)
                    if( isContainer(ob) ){
                        obs.push(ob);
                        return "";
                    }else{
                        return ""+ob;
                    }
                }));

                return obs.length <= 1 ? obs[0] : obs;
            },
            _regs : regs
        });
    })();

})();

////////////////////////////////////////////////////////////////////////////////

function reloadPage()
{
    $('body').empty();
    window.location.reload();
}

function throwError( message )
{
    throw new Error( message );
}