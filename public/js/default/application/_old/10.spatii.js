(function(){
    
_Spatii.Module = _Components.Container.extend(
{
    _setup_: function()
    {
        this.viewModeConstructors = {
            normal:     ViewModeNormal,
            next_step:  ViewModeNextStep,
        };
        
        this.ViewModes = {};
        this.currentViewMode = null;
        
        this.freezed = false;
    },
    
    _setupResources_: function()
    {
        var panelOptions,
            self = this;
                
        panelOptions = {
            Module: this 
        };
        
        this.ScariListPanel = new _Spatii.ScariListPanel( $( '#spatii-scari-list-panel' ), panelOptions );
        this.ActionsPanel   = new _Spatii.ActionsPanel( $( '#spatii-actions-panel' ), panelOptions );
        this.ContentPanel   = new _Spatii.ContentPanel( $( '#spatii-content-panel' ), Object.merge( panelOptions, {
                onFreeze: function( event )
                {
                    if( self.freezed ) return;

                    self.freezed = true;
                    
                    self.ActionsPanel.disable();
                    self._trigger( 'freeze' );
                },
                onUnfreeze: function( event )
                {
                    if( !self.freezed ) return;
                    
                    self.freezed = false;
                    self.ActionsPanel.enable();
                    self._trigger( 'unfreeze' );
                }
        }));
    },
    
    _end_: function()
    {
        if( !this.currentViewMode )
        {
            this._setViewMode( 'normal' );
        }
    },
    
    displayContent: function( panelId, panelArguments )
    {
        this.ContentPanel.display( panelId, panelArguments );
    },
    
    _parameters_: function( parameters )
    {
        if( parameters.showNextStepButton !== undefined )
        {
            this._setViewMode( parameters.showNextStepButton ? 'next_step' : 'normal' );
        }
    },
    
    _setViewMode: function( mode )
    {
        if( this.ViewModes[ mode ] === undefined && _Components.Class.isExtendedFrom( this.viewModeConstructors[ mode ], _ViewModeAbstract ) )
        {
            this.ViewModes[ mode ] = new this.viewModeConstructors[ mode ]( this );
        }
        
        if( this.ViewModes[ mode ] instanceof _ViewModeAbstract )
        {
            if( this.currentViewMode )
            {
                this.currentViewMode.destroy();
            }
            
            this.currentViewMode = this.ViewModes[ mode ];
            this.currentViewMode.apply();
        }
    }
    
});

//------------------------------------------------------------------------------


var _ViewModeAbstract = _Components.Class.extend(
{
    init: function( Module )
    {
        this.Module = Module;
        this.viewMode = null;
        
        this._runHierarchy( '_setup_' );
        
        if( !this.viewMode )
        {
            throw new Error( 'viewMode is not set' );
        }
        
    },
    
    apply: function()
    {
        this._runHierarchy( '_apply_' );
    },
    
    destroy: function()
    {
        this._runHierarchy( '_destroy_' );
    }
});

////////////////////////////////////////////////////////////////////////////////

var ViewModeNormal = _ViewModeAbstract.extend(
{
    _setup_: function()
    {
        this.viewMode = 'normal';
    },
    
    _apply_: function()
    {
        var Module = this.Module,
            $topWrapper = Module.find( '.module-top-wrapper' );
            
        $topWrapper.hide();
    }
    
});

////////////////////////////////////////////////////////////////////////////////

var ViewModeNextStep = _ViewModeAbstract.extend(
{
    _setup_: function()
    {
        var Module = this.Module,
            self = this;
        
        this.viewMode = 'next_step';

        this.TopWrapper = new TopPanelConstructor( Module.find( '.module-top-wrapper' ), {
            onNextStep: function()
            {
                if( Module.currentViewMode !== self || Module.freezed ) return;

                Module._trigger( 'continue' );
            }
        });

        Module.ContentPanel.addHandler( 'freeze', function( event )
        {
            self._disableNextStep();
        });
        
        Module.ContentPanel.addHandler( 'unfreeze', function( event )
        {
            self._enableNextStep();
        });
        
        if( Module.freezed )
        {
            self._disableNextStep();
        }
        else
        {
            self._enableNextStep();
        }
    },
    
    _apply_: function()
    {
        var Module = this.Module,
            $contentWrapper = Module.find( '.module-content-wrapper' );
            
        this.TopWrapper.show();
        $contentWrapper.css( 'top', this.TopWrapper.getContainer().height() );
    },
    
    _destroy_: function()
    {
        var Module = this.Module,
            $contentWrapper = Module.find( '.module-content-wrapper' );
            
        this.TopWrapper.hide();
        $contentWrapper.css( 'top', 0 );
    },
    
    _disableNextStep: function()
    {
        this.TopWrapper.disable();
    },
    
    _enableNextStep: function()
    {
        this.TopWrapper.enable();
    }
    
});

//------------------------------------------------------------------------------

var TopPanelConstructor = _Components.Container.extend(
{
    _setupElements_: function()
    {
        this.$nextStepButton = this.find( 'input.next-step-button' );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$nextStepButton.on( 'click', function()
        {
            self._trigger( 'nextStep' );
        })
    }
});

})();
