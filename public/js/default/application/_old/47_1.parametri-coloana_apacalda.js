(function(){

/**
 * Daca nu exista nicio cheltuiala repartizata pe coloana de apa calda,
 * se afiseaza mesajul: "Daca energia termica pentru prepararea apei calde este 
 * facturata separat, se va adauga mai intai factura pentru serviciul de furnizare
 * a apei."
 * 
 * Daca exista cel putin o cheltuiala repartizata pe coloana de apa calda si care
 * nu are asociata nicio factura pentru energia termica folosita la prepararea 
 * apei calde, se va afisa intrebarea: "Acest serviciu reprezinta energia termica 
 * folosita la prepararea apei calde?". Daca DA - se afiseaza o lista cu serviciile
 * pentru furnizarea apei calde; daca exista doar un singur serviciu, lista nu se 
 * mai afiseaza. 
 * 
 */
_Cheltuieli.ParametriColoanaApaCalda = _Cheltuieli.ParametriColoanaApa.extend(
{

});

})();