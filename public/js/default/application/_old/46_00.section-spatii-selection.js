(function(){
    
_Cheltuieli.SelectSpatiiSection = _Cheltuieli.ValuesSection.extend(
{
    __abstract: [ '_createSpatiiDialogInstance' ],
    
    _setup_: function()
    {
        this.spatii = null;
        this.suma = 0;
    },
    
    _setupElements_: function()
    {
        this.$spatiiChoices = this.find( 'input.spatii-choice' );
        this.$selectionSummary = this.find( '.selection-summary' );
        this.$editSelection = this.find( '.edit-spatii-selection-button' );
    },
    
    _setupResources_: function()
    {
        this.SpatiiDialog = this._createSpatiiDialogInstance();
        
        if( !_Components.Class.isInstanceOf( this.SpatiiDialog, _Cheltuieli.SpatiiSelectionDialog ) )
        {
            System.error( 'Check "SpatiiDialog" class' );
        }
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$spatiiChoices.on( 'change', function()
        {
            var choice = $(this).val();
            
            if( choice == 'spatii' )
            {
                self._selectSpatii();
            }
            else if( choice == 'all' )
            {
                self._selectAllSpatii();
            }
        });
        
        this.$editSelection.on( 'click', function()
        {
            self._selectSpatii();
        });
        
        this.SpatiiDialog.addHandler( 'close', function( event, selection )
        {
            self._setSpatiiSelection( selection );
        });
    },
    
    _init_: function()
    {
        this._hideSummary();
        this._hideEditButton();
    },
    
    _reset_: function()
    {
        this.spatii = null;
        this._hideSummary();
        this._clearSpatiiChoice();
    },
    
    _parameters_: function( params )
    {
        this._setSuma( params.suma );
    },
    
    getSpatii: function()
    {
        return this.spatii;
    },
    
    hasSelection: function()
    {
        return Object.size( this.spatii );
    },
    
    openDialog: function()
    {
        this._selectSpatii();
    },
    
    _setSuma: function( suma )
    {
        if( suma === undefined ) return;
        
        this.suma = suma;
    },
    
    _selectSpatii: function()
    {
        this.SpatiiDialog.open( this._getDialogOptions() );
    },
    
    _getDialogOptions: function()
    {
        return {
            selection:      this.spatii,
            suma:           this.suma
        };
    },
    
    _selectAllSpatii: function()
    {
        this._hideSummary();
        this._hideEditButton();
        
        this._setSpatiiSelection( '*' );
    },
    
    _hideSummary: function()
    {
        this.$selectionSummary.hide();
    },
    
    _showSummary: function( selection )
    {
        this.$selectionSummary.show();
        this.$selectionSummary.text( this._generateSummaryText( selection ) );
    },
    
    _generateSummaryText: function( selection )
    {
        var text = '',
            count = this._getSelectionSize( selection );
        
        if( count !== null )
        {
            text = String.pluralize( count, 'apartament', 'apartamente' );
        }
        
        return text;
    },
    
    _getSelectionSize: function( selection )
    {
        var count = null;
        
        if( Object.isEnumerable( selection ) )
        {
            count = Object.size( selection );
        }
        else if( selection === '*' )
        {
            count = this._getTotalSpatiiCount();
        }
        
        return count;
    },
    
    _getTotalSpatiiCount: function()
    {
        var count = DataResource.get( 'Structura' ).getTotalSpatiiCount();
        
        return count;
    },
    
    _hideEditButton: function()
    {
        this.$editSelection.hide();
    },
    
    _showEditButton: function()
    {
        this.$editSelection.show();
    },
    
    _setSpatiiSelection: function( selection )
    {
        var oldValue = Object.clone( this.spatii );
        
        if( selection === null )
        {
            if( this.spatii === null )
            {
                this._clearSpatiiChoice();
            }
            else if( this.spatii === '*' )
            {
                this.$spatiiChoices.filter( '[value=all]' ).prop( 'checked', true );
            }
        }
        else if( selection === '*' )
        {
            this.spatii = selection;
            
            this._showSummary( selection );
            this._hideEditButton();
        }
        else if( Object.size( selection ) )
        {
            this._showSummary( selection );
            this._showEditButton();

            this.spatii = selection;
        }
        
        if( !Object.equal( oldValue, this.spatii ) )
        {
            this._trigger( 'change' );
        }
    },
    
    _clearSpatiiChoice: function()
    {
        this.$spatiiChoices.prop( 'checked', false );
        this._hideEditButton();
    }
    
});

})();