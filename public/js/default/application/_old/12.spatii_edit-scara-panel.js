(function(){

_Spatii.EditScaraPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.scaraInfo = null;
    },
    
    _setupElements_: function()
    {
        this.$instructionSelectScara = this.find( '.instructions .select-scara' );
    },
    
    _setupResources_: function()
    {
        this._setupPanes();
        this._initForm();
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ScariListPanel.addHandler( 'selectScara', function( event )
        {
            if( self.isVisible() )
            {
                var selectedScaraId = event.getArguments()[ 0 ];
                
                self._setScaraId( selectedScaraId );
                self._displayPanel();
            }
        });
    },
    
    /**
     * 
     * @param scaraInfo
     */
    _parameters_: function( params )
    {
        this._setScaraInfo( params.scaraInfo );
    },
    
    _show_: function()
    {
        this.ScariListPanel.setSelectMode( 'scara' );
        
        if( !this.scaraInfo )
        {
            // get scaraId from ScariListPanel
            var scaraId;
            
            scaraId = this.ScariListPanel.getSelectedScara();
            
            if( scaraId )
            {
                this._setScaraId( scaraId );
            }
        }
        
        if( this.scaraInfo )
        {
            this._displayPanel();
        }
        else
        {
            this._displaySelectBlocMessage();
        }
    },
        
    _setupPanes: function()
    {
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ) );
    },
    
    _initForm: function()
    {
        var self = this;
        
        var errorMessages = {
            denumire:   'Completați denumirea scării.',
            adresa:     'Completați adresa scării.',
        };
        
        this.Form = new _Components.Form( this.getContainer(), {
            action: 'edit-scara',
            onBeforeSubmit: function( event )
            {
                var formData = event.getArguments()[ 0 ],
                    errors = [];
                
                Object.each( formData, function( fieldName, fieldValue ){
                    if( fieldValue.isBlank() )
                    {
                        errors.push( errorMessages[ fieldName ] );
                    }
                });
                
                if( errors.length )
                {
                    this._showErrors( errors );
                    event.abort();
                }
            },
            onSuccess: function( event )
            {
                var responseData = event.getArguments()[ 0 ],
                    denumire = responseData.denumire;
                
                System.info( 'Scara {ds} a fost modificată.'.assign({ ds: denumire }) );
                
                this.clear();
                
                self._updateResourceData( responseData );
                self._trigger( 'done', {
                    returnToHomePanel: true
                });
            }
            
        });
        
    },
    
    _updateResourceData: function( data )
    {
        this.StructuraData.editScara( data );
    },
    
    _setScaraId: function( scaraId )
    {
        var scaraInfo;
        
        scaraInfo = this.StructuraData.getScaraInfo( scaraId );
        
        this._setScaraInfo( scaraInfo );
    },
    
    _setScaraInfo: function( info )
    {
        if( !Object.isObject( info ) )
        {
            this.scaraInfo = null;
        }
        else
        {
            this.scaraInfo = info;

            this.Form.getField( 'id' ).val( info.id );
            this.Form.getField( 'denumire' ).val( info.denumire );
            this.Form.getField( 'adresa' ).val( info.adresa );
        }
    },
    

    _displaySelectBlocMessage: function()
    {
        this.Panes.showPane( 'select-scara' );
    },
    
    _displayPanel: function()
    {
        this.Panes.showPane( 'panel' );
        
        this.Form.getField( 'denumire' ).focus().select();
        
        this.$instructionSelectScara[ this.StructuraData.getScariCount() > 1 ? 'show' : 'hide' ]();
    }
    
});

})();