(function(){

_Cheltuieli.SelectSpatiiParametruDialog = _Cheltuieli._SelectSpatiiDialog.extend(
{
    _setup_: function()
    {
        this.parametruCode = null;
        
        this.ScariListConstructor = ScariListConstructor;
        this.SpatiiListConstructor = SpatiiListConstructor;
    },
    
    _setupElements_: function()
    {
        this.$parametruLabel = this.find( '.list-header .parametru-cell' );
    },
    
    setParametruSpatiu: function( parametruCode )
    {
        this.parametruCode = parametruCode;
        
        this.SpatiiList.setParametru( parametruCode );
    },
    
    open: function()
    {
        if( this.parametruCode === null )
        {
            System.error( '"parametruCode" is not set' );
        }
        else
        {
            this._super.apply( this, arguments );
        }
    }
    
});

//==============================================================================

var SpatiiListConstructor = _Spatii._SpatiiListForSelectionConstructor.extend(
{
    _setup_: function()
    {
        this.parametruCode = null;
        
        this.PaneListConstructor = SpatiiPaneListConstructor;
    },
    
    _reset_: function()
    {
        var self = this;
        
        Object.each( this.Panes, function( paneId, Pane )
        {
            if( Pane.isEmpty() )
            {
                self.ScariLink.hideScara( paneId );
            }
            else
            {
                self.ScariLink.showScara( paneId );
                self.ScariLink.setTotalSpatii( paneId, Pane.getSpatiiCount() );
            }
        });
        
        this._checkIfScaraIsSelected();
    },
    
    setParametru: function( parametruCode )
    {
        this.parametruCode = parametruCode;
        
        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.setParametru( parametruCode );
        });
    }
        
});

//------------------------------------------------------------------------------

var SpatiiPaneListConstructor = _Spatii.SpatiiPaneList.extend(
{
    _setup_: function()
    {
        this.empty = false;
        this.parametruCode = null;
    },
    
    _setupElements_: function()
    {
        this.$parametruLabel = this.$listHeader.find( '.parametru-cell' );
    },
    
    _reset_: function()
    {
        this.empty = true;
        
        this._showParametruColumn();
        this._showAllRows();
        this._showRowsByParametru();
    },
    
    isEmpty: function()
    {
        return this.empty;
    },
    
    setParametru: function( parametruCode )
    {
        this.parametruCode = parametruCode;
    },    
    
    getSpatiiCount: function()
    {
        var count = this._getAllVisibleSpatiuRows().length;
        
        return count;
    },
    
    _showParametruColumn: function()
    {
        var self = this,
            parametri = DataResource.get( 'Structura' ).getParametriSpatiu(),
            parametruLabel;
    
        parametri.each( function( parametru )
        {
            if( self.parametruCode == parametru.code )
            {
                parametruLabel = parametru.denumire_short || parametru.denumire;
                
                return false;
            }
        });
        
        this.$parametruLabel.text( parametruLabel );
    },
    
    _showAllRows: function()
    {
//        this.getRows().show();
        this._getEtajContainers().show();
    },
    
    _showRowsByParametru: function()
    {
        var self = this;
            
        this._getEtajContainers().each( function()
        {
            var $etajContainer = $(this),
                $etajRows = self._getSpatiuRowsInsideContainer( $etajContainer ),
                visible = false;
            
            $etajRows.each( function()
            {
                var $row = $(this),
                    spatiuData = $row.data( 'data' ),
                    valoareParametru;
                    
                if( !spatiuData 
                        || !( valoareParametru = spatiuData[ self.parametruCode ] ) 
                        || !( valoareParametru = valoareParametru.toNumber() ) 
                  )
                {
                    self._hideRow( $row );
                }
                else
                {
                    visible = true;
                    self.empty = false;
                    
                    $row.find( '.parametru-cell' ).text( valoareParametru );
                }
            });
            
            if( !visible )
            {
                $etajContainer.hide();
            }
        });
        
    }    
    
});

//==============================================================================

var ScariListConstructor = _Spatii._ScariListForSelectionConstructor.extend(
{
    
});

})();