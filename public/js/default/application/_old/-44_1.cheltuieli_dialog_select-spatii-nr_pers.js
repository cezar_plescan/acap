(function(){

_Cheltuieli.SpatiiSelectionNrPersoaneDialog = _Cheltuieli._SelectSpatiiDialog.extend(
{
    _setup_: function()
    {
        this.SpatiiScariListConstructor = _Spatii.SpatiiScariListSelectionPickNrPersoane;
    },
    
    _getSelectionTitle: function()
    {
        var title = this._super(),
            persoane = Object.sum( this.getSelection() );


        title += ', ' + String.pluralize( persoane, 'persoană', 'persoane' );
        
        return title;
    }
        
});

//==============================================================================

var SpatiiPaneListConstructor = _Spatii.SpatiiPaneList.extend(
{
    _populateSpatiuRow: function( $spatiuRow, spatiuData )
    {
        this._super.apply( this, arguments );
        
        if( !spatiuData.nr_pers )
        {
            $spatiuRow.addClass( 'disabled' );
        }
    }
});

})();