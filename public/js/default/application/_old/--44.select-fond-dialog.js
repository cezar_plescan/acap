(function(){
    
_Cheltuieli.SelectFondDialog = _Components.Dialog.extend(
{
    _setup_: function()
    {
        this._options.headerVisible = true;

        this.suma = null;
        this.selectedFond = null;
    },
    
    _setupElements_: function()
    {
        this.$dialogContent = this.find( '>.ui-dialog-content' );
        this.$buttonsContainer = this.find( '>.buttons-container' );
        
        this.$doneButton = this.$buttonsContainer.find( '.done-button' );
        this.$cancelButton = this.$buttonsContainer.find( '.cancel-button' );
    },
    
    _setupResources_: function()
    {
        this.ChoicesSection = new ChoicesSectionConstructor( this.$dialogContent.find( '>.fonduri-choices' ) );
        this.FonduriPanes = new FonduriPanesConstructor( this.$dialogContent.find( '>.fonduri-panes' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ChoicesSection.addHandler( 'select', function( event, fond )
        {
            self._setFond( fond );
        });
        
        this.bind( 'scrollToMe', function( event, $element )
        {
            self.$dialogContent.scrollTo( $element );
        });
        
        this.addHandler( 'beforeOpen', function( event, options )
        {
            if( Object.isEmpty( options.suma ) || !options.suma.valoare || !options.suma.valoare.toNumber() )
            {
                event.abort();
                
                System.alert( 'Completați suma pe care o distribuiți' );
            }
        });
        
        this.$doneButton.on( 'click', function()
        {
            var parametri = self._getParametri();
            
            // @TODO: validare informatii completate/selectate
            
            self.close( parametri );
            
            console.log( parametri );
        });
        
        this.$cancelButton.on( 'click', function()
        {
            self.close();
        });
        
    },
    
    _init_: function()
    {
        this.FonduriPanes.hide();
    },
    
    _parameters_: function( params )
    {
        params.suma !== undefined && this._setSuma( params.suma );
    },
    
    _reset_: function()
    {
        this.suma = null;
        this.selectedFond = null;
        
        this.ChoicesSection.reset();
        
        this.FonduriPanes.hide();
        this.FonduriPanes.reset();
    },
    
    open: function()
    {
        this.reset();
        
        this._super.apply( this, arguments );
    },
    
    _getParametri: function()
    {
        var parametri = {
            fond_id:        this._getSelectedFond(),
            fond_parametri: this._getParametriFond()
        };
        
        return parametri;
    },
    
    _setSuma: function( suma )
    {
        this.suma = suma;
        
        this.FonduriPanes.sendParameters( { suma: suma } );
    },
    
    _getSelectedFond: function()
    {
        return this.selectedFond;
    },
    
    _getParametriFond: function()
    {
        var parametri = this.FonduriPanes.getParametri();
        
        return parametri;
    },
    
    _showFondDetails: function( fondId )
    {
        this.FonduriPanes.show();
        this.FonduriPanes.showPane( fondId );
    },
    
    _setFond: function( fond )
    {
        this.selectedFond = fond;
        this._showFondDetails( fond );        
    }
    
});

//==============================================================================

var ChoicesSectionConstructor = _Components.Container.extend(
{
    _setupElements_: function()
    {
        this.$choices = this.find( 'input[name=_ff]' );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$choices.on( 'change', function()
        {
            var fond = $(this).val();
            
            self._trigger( 'select', fond );
        });
    },
    
    _reset_: function()
    {
        this._clearChoices();
    },
    
    _clearChoices: function()
    {
        this.$choices.prop( 'checked', false );
    }
    
});

//==============================================================================

var FonduriPanesConstructor = _Components.Panes.extend(
{
    _setup_: function()
    {
        var self = this;
        
        this.suma = null;
        
        this._options.paneConstructors = {
            intretinere:    _Cheltuieli.FondIntretinereFrame,
            reparatii:      _Cheltuieli.FondReparatiiFrame,
            penalizari:     _Cheltuieli.FondPenalizariFrame
        };
        
        this.addHandler( 'showPane', function( event, paneId, Pane )
        {
            Pane.reset();
        });
        
        this.addHandler( 'loadPane', function( event, paneId, Pane )
        {
            Pane.sendParameters( { suma: self.suma } );
        });
        
    },
    
    _parameters_: function( params )
    {
        params.suma !== undefined && this._setSuma( params.suma );
    },
    
    getParametri: function()
    {
        var parametri = null,
            currentPaneId = this.getCurrentPaneId();
            
        if( currentPaneId )
        {
            parametri = this._getPaneObject( currentPaneId ).getParametri();
        }
        
        return parametri;
    },
    
    _setSuma: function( suma )
    {
        this.suma = suma;

        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.sendParameters( { suma: suma } );
        });
    }
    
});

})();