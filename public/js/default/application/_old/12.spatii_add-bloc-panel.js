(function(){

_Spatii.AddBlocPanel = _Spatii.AbstractContentPanel.extend(
{
    _setupElements_: function()
    {
        
    },
    
    _setupResources_: function()
    {
        this._initForm();
    },
    
    _initForm: function()
    {
        var self = this;
        
        this.Form = new _Components.Form( this.getContainer(), {
            action: 'add-bloc',
            onBeforeSubmit: function( event )
            {
                var formData = event.getArguments()[ 0 ];
                
                if( formData.denumire.isBlank() )
                {
                    this._showErrors( 'Completați denumirea blocului.' );
                    
                    event.abort();
                }
            },
            onSuccess: function( event )
            {
                var responseData = event.getArguments()[ 0 ],
                    denumire = responseData.denumire;
                
                System.info( 'Blocul {bloc} a fost adăugat.'.assign({ bloc: denumire }) );
                
                this.clear();
                
                self._updateResourceData( responseData );
                self.ScariListPanel.setCurrentBloc( responseData.id );
                
                self._trigger( 'done' );
            }
            
        });        
    },
    
    _updateResourceData: function( data )
    {
        this.StructuraData.addBloc( data );
    },
    
    _show_: function()
    {
        this.ScariListPanel.disableSelection();
        
        this.Form.clear();
        this.Form.getField( 'denumire' ).focus();
    }
    
    
});

})();