(function(){
    
_Cheltuieli.BenefEgalPanel = _Cheltuieli.BenefDetailsPanel.extend(
{
    _setupResources_: function()
    {
        this.SelectSpatiiSection = new SelectSpatiiSectionConstructor( this.find( '>.spatii-section' ) );
    },
    
    _reset_: function()
    {
        this.SelectSpatiiSection.reset();
    },
    
    _parameters_: function( params )
    {
        if( params.suma !== undefined )
        {
            this.SelectSpatiiSection.sendParameters( { suma: params.suma } );
        }
    }
    
});

//==============================================================================

var SelectSpatiiSectionConstructor = _Cheltuieli.SelectSpatiiSection.extend(
{
    _createSpatiiDialogInstance: function()
    {
        var Instance = new _Cheltuieli.SpatiiSelectionAnyDialog( $( '#select-spatii-egal-dialog' ), {
            height: 'auto'
        });
        
        return Instance;
    },
    
    _generateSummaryText: function( selection )
    {
        var spatiiText = '',
            spatiiCount = this._getSelectionSize( selection ),
            valuePerSpatiu,
            valuePerSpatiuText,
            summaryText;

        spatiiText = String.pluralize( spatiiCount, 'apartament', 'apartamente' );
    
        valuePerSpatiu = ( this.suma / spatiiCount ).round( 2 );
        valuePerSpatiuText = valuePerSpatiu + ' lei / apartament';

        summaryText = [ spatiiText, valuePerSpatiuText ].join( ', ' );

        return summaryText;
    }
    
});


})();