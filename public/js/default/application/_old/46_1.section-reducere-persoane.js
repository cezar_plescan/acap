(function(){
    
_Cheltuieli.ReducerePersoaneSection = _Components.Container.extend(
{
    _setup_: function()
    {
        this.spatii = null;
        this.reducere = null;
    },
    
    _setupElements_: function()
    {
        this.$reducereChoices = this.find( 'input.reducere-choice' );
        this.$parametriReducere = this.find( '.parametri-reducere' );
        this.$selectedReducere = this.find( '.selected-reducere' );
        this.$selectReducereButton = this.find( '.select-spatii-reducere-button' );
        this.$editReducereButton = this.find( '.edit-reducere-selection-button' );
        this.$reducereSummary = this.find( '.summary' );
        this.$inputValoareReducere = this.find( 'input.reducere-procentuala' );
    },
    
    _setupResources_: function()
    {
        this.ReducereDialog = new _Cheltuieli.SpatiiSelectionReducerePersoaneDialog( $( '#reducere-persoane-dialog' ), {
            height: 'auto'
        });
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$reducereChoices.on( 'change', function()
        {
            var choice = $(this).val() - 0;
            
            if( choice )
            {
                self._showParametriReducere();
            }
            else
            {
                self._hideParametriReducere();
            }
        });
        
        this.$selectReducereButton.on( 'click', function()
        {
            self._selectSpatiiReducere();
        });
        
        this.$editReducereButton.on( 'click', function()
        {
            self._selectSpatiiReducere();
        });
        
        this.ReducereDialog.addHandler( 'close', function( event, selection )
        {
            self._setReducereSelection( selection );
        });
        
    },
    
    _init_: function()
    {
        this._hideParametriReducere();
    },
    
    getReducere: function()
    {
        var reducere = {
            valoare:    this._getValoareReducere(),
            spatii:     this._getSpatiiReducere(),
        };
        
        return reducere;
    },
    
    setSpatii: function( spatii )
    {
        this.spatii = spatii;
    },
    
    _reset_: function()
    {
        this.$reducereChoices.prop( 'checked', false );
        
        this._hideParametriReducere();
    },
    
    _showParametriReducere: function()
    {
        this.$parametriReducere.show();
    },
    
    _hideParametriReducere: function()
    {
        this.$parametriReducere.hide();
        this.$selectedReducere.hide();
        this.$selectReducereButton.show();
        
        this.reducere = null;
        
        this.$inputValoareReducere.val( '' );
    },
    
    _selectSpatiiReducere: function()
    {
        this.ReducereDialog.open({
            spatii:     this.spatii,
            selection:  this.reducere
        });
    },
    
    _setReducereSelection: function( selection )
    {
        if( Object.isObject( selection ) )
        {
            this._showReducereSummary( selection );

            this.reducere = selection;
        }
    },
    
    _showReducereSummary: function( selection )
    {
        var text = this._generateReducereSummaryText( selection );
        
        this.$selectReducereButton.hide();
        this.$selectedReducere.show();
        
        this.$reducereSummary.text( text );
    },
    
    _generateReducereSummaryText: function( selection )
    {
        var text = '',
            persoaneCount = 0,
            spatiiCount = 0;
            
        Object.each( selection, function( spatiuId, persoane )
        {
            persoaneCount += persoane.toNumber();
            spatiiCount ++;
        });
        
        text = String.pluralize( persoaneCount, 'persoană', 'persoane' ) + ' (' + String.pluralize( spatiiCount, 'apartament', 'apartamente' ) + ')';
        
        return text;
    },
    
    _getValoareReducere: function()
    {
        var valoare = this.$inputValoareReducere.val().trim();
        
        return valoare;
    },
    
    _getSpatiiReducere: function()
    {
        return this.reducere;
    }
    
});

})();