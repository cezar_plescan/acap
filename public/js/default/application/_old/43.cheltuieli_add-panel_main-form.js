(function(){
    
_Cheltuieli.MainFormSection = _Cheltuieli.SectionAbstract.extend(
{
    _setupElements_: function()
    {
        this.$fields = this.find( 'input' );
    },
    
    _setupResources_: function()
    {
        this.find( 'input.data-emiterii-field' )
                .prop( 'readonly', true )
                .datepicker({
                    maxDate:    0,
                    dateFormat: 'DD, d MM yy'
                });
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$fields.on( 'keyup paste change', function()
        {
            if( !self.completed && self._isCompleted() )
            {
                self._triggerCompleted();
            }
        });
    },
    
    getSuma: function()
    {
        var suma = {
            valoare: this.$fields.filter( '[name=valoare]' ).val()
        };
        
        return suma;
    },
    
    _isCompleted: function()
    {
        var completed = true;
        
        this.$fields.each( function()
        {
            if( $(this).val().isBlank() )
            {
                completed = false;
                return false;
            }
        });
        
        return completed;
    }
    
});

})();