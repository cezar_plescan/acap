(function(){

_Spatii.RemoveScaraPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.scaraInfo = null;
        this.changeScara = true;
    },
    
    _setupElements_: function()
    {
        this.$blocLabel = this.find( '.bloc-label' );
        this.$scaraLabel = this.find( '.scara-label' );
        this.$instructionSelectScara = this.find( '.instructions .select-scara' );
    },
    
    _setupResources_: function()
    {
        this._setupPanes();
        this._initForm();
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ScariListPanel.addHandler( 'selectScara', function( event )
        {
            if( self.isVisible() )
            {
                var selectedScaraId = event.getArguments()[ 0 ];
                
                self._setScaraId( selectedScaraId );
                self._displayPanel();
            }
        });
    },
    
    /**
     * 
     * @param scaraInfo
     * @param changeScara
     */
    _parameters_: function( params )
    {
        this._setScaraInfo( params.scaraInfo );
        this._setChangeScara( params.changeScara );
    },
    
    _show_: function()
    {
        if( this.changeScara )
        {
            this.ScariListPanel.setSelectMode( 'scara' );
            this.$instructionSelectScara[ this.StructuraData.getScariCount() > 1 ? 'show' : 'hide' ]();
        }
        else
        {
            this.ScariListPanel.disableSelection( false ); // keep the selected item highlighted
            this.$instructionSelectScara.hide();
        }
        
        if( !this.scaraInfo )
        {
            // get scaraId from ScariListPanel
            var scaraId;
            
            scaraId = this.ScariListPanel.getSelectedScara();
            
            if( scaraId )
            {
                this._setScaraId( scaraId );
            }
        }
        
        if( this.scaraInfo )
        {
            this._displayPanel();
        }
        else
        {
            this._displaySelectScaraMessage();
        }
    },
    
    _setupPanes: function()
    {
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ) );
    },
    
    _initForm: function()
    {
        var self = this;
        
        this.Form = new _Components.Form( this.getContainer(), {
            action: 'remove-scara',
            onSuccess: function( event )
            {
                var responseData = event.getArguments()[ 0 ],
                    denumire = self.scaraInfo.denumire,
                    denumireBloc = self.scaraInfo.denumire_bloc;
                
                System.info( 'Scara {scara} din blocul {bloc} a fost eliminată.'.assign({ scara: denumire, bloc: denumireBloc }) );
                
                self._updateResourceData( responseData );
                self._trigger( 'done', {
                    returnToHomePanel: true
                });
            }
            
        });
        
    },
    
    _updateResourceData: function( data )
    {
        this.StructuraData.removeScara( data );
    },
    
    _setScaraId: function( scaraId )
    {
        var scaraInfo;
        
        scaraInfo = this.StructuraData.getScaraInfo( scaraId );
        
        this._setScaraInfo( scaraInfo );
    },
    
    _setScaraInfo: function( info )
    {
        if( !Object.isObject( info ) )
        {
            this.scaraInfo = null;
        }
        else
        {
            this.scaraInfo = info;

            this.$scaraLabel.text( info.denumire );
            this.$blocLabel.text( info.denumire_bloc );

            this.Form.getField( 'id' ).val( info.id );
        }
    },

    _disableQuit: function(){},
    _enableQuit: function(){},
    
    _displaySelectScaraMessage: function()
    {
        this.Panes.showPane( 'select-scara' );
    },
    
    _displayPanel: function()
    {
        this.Panes.showPane( 'panel' );
    },
    
    _setChangeScara: function( changeScara )
    {
        this.changeScara = ( changeScara === undefined ) ?
                true :
                Object.toBoolean( changeScara );
    }
        
});

})();