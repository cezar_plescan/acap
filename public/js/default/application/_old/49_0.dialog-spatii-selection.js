(function(){
    
_Cheltuieli.SpatiiSelectionDialog = _Components.Dialog.extend(
{
    _setup_: function()
    {
        this._options.headerVisible = true;        
        
        this.selection = null;
        this.suma = 0;
        
        this.SpatiiScariConstructorBase = _Spatii.SpatiiScariSelection;
        this.SpatiiScariConstructor = null;
    },
    
    _setupElements_: function()
    {
        this.$doneButton = this.find( '.done-button' );
        this.$cancelButton = this.find( '.cancel-button' );
    },
    
    _setupResources_: function()
    {
        if( !_Components.Class.isExtendedFrom( this.SpatiiScariConstructor, this.SpatiiScariConstructorBase ) )
        {
            System.error( 'Check "SpatiiScariConstructor" class' );
        }
        
        this.SpatiiScari = new this.SpatiiScariConstructor( this.find( '>.spatii-scari-component' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$doneButton.on( 'click', function()
        {
            var selection = self.SpatiiScari.getSelection();
            
            if( Object.size( selection ) )
            {
                self.selection = selection;

                self.close({
                    selection: selection
                });
            }
            else
            {
                self.selection = null;
                self.close();
            }
        });
        
        this.$cancelButton.on( 'click', function()
        {
            self.close();
        });
        
        this.SpatiiScari.addHandler( 'change', function( event, selection )
        {
            self._setSelection( selection );
        });
        
        this.addHandler( 'close', function()
        {
            self.SpatiiScari.hangup();
        });
    },
    
    _init_: function()
    {
        this.initialTitle = this._getTitle();
        
        this._disableDoneButton();
    },
    
    _reset_: function()
    {
        this._setSelection( null );
        
        this.SpatiiScari.reset();
        
        this._disableDoneButton();
    },
    
    _parameters_: function( params )
    {
        this._setSuma( params.suma );
        this._showSpatii( params.spatii );
        this._restoreSelection( params.selection );
    },
    
    getSelection: function()
    {
        return this.selection;
    },
    
    /**
     * @option suma
     * @option spatii
     * @option selection
     */
    open: function( options )
    {
        this.reset();
        
        this._super.apply( this, arguments );
    },

    _restoreSelection: function( selection )
    {
        if( selection === undefined ) return;
        
        this.SpatiiScari.setSelection( selection );
        
        this._setSelection( selection );
    },
    
    _setSuma: function( suma )
    {
        if( suma === undefined ) return;
        
        this.suma = suma.toNumber() || 0;
    },
    
    _disableDoneButton: function()
    {
        this.$doneButton.prop( 'disabled', true );
    },
    
    _enableDoneButton: function()
    {
        this.$doneButton.prop( 'disabled', false );
    },
    
    _setSelection: function( selection )
    {
        this._storeSelection( selection );
        this._updateTitle();
        this._computeDoneButtonStatus();
    },
    
    _storeSelection: function( selection )
    {
        this.selection = selection;
    },
    
    _hasSelection: function()
    {
        return !!Object.size( this.selection );
    },
    
    _updateTitle: function()
    {
        var title = '';
        
        if( this._hasSelection() )
        {
            title = this._getSelectionTitle();
        }
        else
        {
            title = this._getInitialTitle();
        }
        
        this.setTitle( title );
    },
    
    _getInitialTitle: function()
    {
        return this.initialTitle;
    },
    
    _getSelectionTitle: function()
    {
        var title,
            spatiiCount = Object.size( this.selection );
        
        title = String.pluralize( spatiiCount, 'apartament', 'apartamente' );

        return title;
    },
    
    _computeDoneButtonStatus: function()
    {
        if( this._isDone() )
        {
            this._enableDoneButton();
        }
        else
        {
            this._disableDoneButton();
        }
    },
    
    _isDone: function()
    {
        return this._hasSelection();
    },
    
    _showSpatii: function( spatii )
    {
        if( spatii === undefined ) return;
        
        this.SpatiiScari.showSpatii( spatii );
    }
    
});

})();