(function(){

_Spatii._SpatiuForm = _Components.RequestForm.extend(
{
    errorMessages: {
        etaj:       'Completați etajul apartamentului.',
        numar:      'Completați numărul apartamentului.',
        proprietar: 'Completați numele proprietarului apartamentului.',
        nr_pers:    'Completați numărul de persoane care locuiesc în apartament.',
        suprafata:  'Completați suprafața apartamentului.'
    },

    _setup_: function()
    {
        if( !this._options.Panel )
        {
            throw new Error( '"Panel" option is not set.' );
        }
        
        this.Panel = this._options.Panel;
    },
    
    _init_: function()
    {
        this._createAdditionalFields();
    },
    
    _createAdditionalFields: function()
    {
        var self = this,
            predefinedParametri = DataResource.get( 'Structura' ).getPredefinedParametriSpatiu(),
            $parametriContainer = this.find( '.parametri-container' );
    
        predefinedParametri.each( function( parametru )
        {
            var $parametruRow = self._getTemplate( 'parametru-item' );
            
            $parametruRow.attr( 'title', parametru.descriere );
            
            $parametruRow.find( '.parametru-label' )
                    .text( parametru.denumire );
                    
            $parametruRow.find( 'input.parametru-field' )
                    .attr( 'name', parametru.code );
            
            $parametruRow.appendTo( $parametriContainer );
            
            $parametruRow.tooltip({
                tooltipClass:   'descriere-parametru-spatiu-tooltip',
                position:       { my: 'left top+2' },
                show:           false,
                hide:           false
            });
        });
    }    
    
});

})();