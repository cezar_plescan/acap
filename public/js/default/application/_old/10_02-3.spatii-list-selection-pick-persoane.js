(function(){
        
_Spatii.SpatiiListSelectionPickPersoane = _Spatii.SpatiiListSelectionPick.extend(
{
    _getSelectionValue: function( $row )
    {
        var spatiuData = $row.data( 'data' ),
            persoane = spatiuData.nr_pers;
    
        return persoane;
    },
    
    _populateSpatiuRow: function( $spatiuRow, spatiuData )
    {
        this._super.apply( this, arguments );
        
        if( spatiuData.nr_pers === undefined )
        {
            this._hideRow( $spatiuRow );
        }
        else
        {
            $spatiuRow.find( '>.persoane-cell' ).text( spatiuData.nr_pers );
        }
    }
});

})();