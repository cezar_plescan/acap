(function(){

_Spatii.SpatiiScariSelectionPickParametru = _Spatii.SpatiiScariSelectionPick.extend(
{
    _setup_: function()
    {
        this.SpatiiPanelConstructor = _Spatii.SpatiiPanelSelectionPickParametru;
    },
    
    setParametru: function( parametruCode )
    {
        this.SpatiiPanel.setParametru( parametruCode );
    }
    
});

})();
