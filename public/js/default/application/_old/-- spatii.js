(function(){

var BlocStorage =
{
    _storage: [],
    
    addBloc: function( blocData )
    {
        var denumire = blocData.denumire;
        
        // adaugare pastrand ordinea alfabetica
        if( count( this._storage ) )
        {
            foreach( this._storage, function( index, data ){
                if( denumire.localeCompare( data.denumire ) > 0 )
                {
                    this._blocuri.splice( index + 1, 0, blocData );

                    return false;
                }
            });
        }
        else
        {
            this._storage.push( blocData );
        }
        
        this.notifyObservers( blocData );
    },
    
    _notifyObservers: function( blocData )
    {
        //...
    }
};

var EditSpatiiForm = _Components.Form.extend(
{
    url: '/api/update-spatii',

    _init_: function()
    {
        var self = this;
        
        this.$fields = this.find('input.valoare');

        window.onbeforeunload = function( event ){
            if( count( self.getParams() ) )
            {
                var message = "Există valori care nu sunt salvate. Doriţi să le salvaţi sau renunţaţi la modificări?";
                event = event || window.event;
                // For IE and Firefox
                if ( event )
                {
                    event.returnValue = message;
                }

                // For Safari
                return message;
            }
        };

    },

    getParams: function()
    {
        var params = {};

        this.$fields.each(function(){
            var $field = $(this),
                currentValue = $.trim( $field.val() ),
                originalValue = $.trim( $field.data( 'value' ) );

            if ( currentValue !== originalValue )
            {
                var fieldName = $field.attr( 'name' );
                params[ fieldName ] = currentValue;
            }

        });

        return params;
    },

    _saveFieldValues: function()
    {
        this.$fields.each(function(){
            var $field = $(this),
                currentValue = $.trim( $field.val() );

            $field.data( 'value', currentValue );
            $field.val( currentValue );
        });
    },

    _beforeSubmit: function( params )
    {
        if( count( params ) == 0 )
        {
            return false;
        }
    },

    _success: function( response )
    {
        this._saveFieldValues();

        alert('Valorile au fost salvate');
    },

    _error: function()
    {
        alert('Verificaţi valorile completate!');
    }


});

////////////////////////////////////////////////////////////////////////////////

//-- adaugare spatii --//

var AddSpatiuConstructor = _Components.Container.extend(
{
    _init_: function()
    {

    }
});

//-- adaugare blocuri --//

var AddBlocConstructor = (function(){

    var Constructor = _Components.Form.extend(
    {
        url: '/api/add-bloc',
        
        _init_: function()
        {

        },
        
        _success: function( response )
        {
            alert( 'Blocul a fost adăugat.' );
            
            this.reset();
            
            this._trigger( 'onDone' );
            
            // adaugare bloc in memorie
            BlocStorage.addBloc({
                id: response.id,
                denumire: response.denumire,
            });
        },

    });

    ////
    return Constructor;

})();
        
////////////////////////////////////////////////////////////////////////////////

var EditDialogConstructor = (function(){

    var Constructor = _Components.Dialog.extend(
    {
        _init_: function()
        {
            this.getContainer().dialog('option', {
                minHeight:  600,
                width:      600
            });

            this._setupPanes();
        },
        
        _setupPanes: function()
        {
            this.Panes = new PanesConstructor( this.find('> .panes-container') );
        },
        
        _open: function()
        {
            this.Panes.showIndexPane();
        }
        
    });
    
    var PanesConstructor = (function()
    {
        var Constructor = _Components.Panes.extend(
        {
            _init_: function()
            {
                var self = this;

                var IndexPaneConfig = [
                    IndexPaneConstructor,
                    {
                        onSelect: function( operationId )
                        {
                            self.showOperationPane( operationId );
                        }
                    }
                ];

                var OperationsPaneConfig = [
                    OperationsPaneConstructor,
                    {
                        onBack: function()
                        {
                            self.showIndexPane();
                        }
                    }
                ];

                $.extend( this.options, {
                    defaultPane: 'index',
                    paneConstructors: {
                        index: IndexPaneConfig,
                        operations: OperationsPaneConfig,

                    }
                });

                this._super();
            },

            showOperationPane: function( operationId )
            {
                this.showPane( 'operations' );
                this.Panes.operations.showPane( operationId );
            },

            showIndexPane: function()
            {
                this.showPane( 'index' );
            },



        });

        var IndexPaneConstructor = _Components.Container.extend(
        {
            _init_: function()
            {
                this._bindItemAction();
            },

            _bindItemAction: function()
            {
                var self = this;

                this.bind( 'click', '.operation-item', function(){
                    var operationId = $(this).data( 'operation-id' );
                    self._trigger( 'onSelect', operationId );
                });
            }
        });

        var OperationsPaneConstructor = (function()
        {
            var Constructor = _Components.Container.extend(
            {
                _init_: function()
                {
                    this._bindBackAction();
                    this._setupPanes();
                },

                _bindBackAction: function()
                {
                    var self = this;

                    this.find( '.back-button' ).on( 'click', function(){
                        self._trigger( 'onBack' );
                    })
                },

                _setupPanes: function()
                {
                    this.Panes = new PanesConstructor( this.find('> .panes-container') );
                },
                
                showPane: function( operationId )
                {
                    this.Panes.showPane( operationId );
                }
            });

            var PanesConstructor = _Components.Panes.extend(
            {
                _init_: function()
                {
                    this._super();
                },
                
                _preparePanes: function()
                {
                    var paneConstructorsConfig = {};
                    
                    foreach( this.paneIds, function( idx, paneId )
                    {
                        var PaneConstructor = PaneFactory.getPaneConstructor( paneId );
                        
                        if( PaneConstructor )
                        {
                            paneConstructorsConfig[ paneId ] = PaneConstructor;
                        }
                    });
                    
                    this.options.paneConstructors = paneConstructorsConfig;
                    
                    this._super();
                }
                
            });            
            
            var PaneFactory = (function()
            {
                //// with class name auto-detection
                ////
//                var FactoryObject =
//                {
//                    _classSufix: 'PaneConstructor',
//            
//                    getPaneConstructor: function( paneId )
//                    {
//                        var className = this._getClassName( paneId ),
//                            PaneConstructor = null;
//                        
//                        try {
//                            PaneConstructor = eval( className );
//                        }
//                        catch( e ){};
//                        
//                        return PaneConstructor;
//                    },
//                    
//                    _getClassName: function( paneId )
//                    {
//                        var classPrefix = $.String.classize( paneId ),
//                            className = classPrefix + this._classSufix;
//                            
//                        return className;
//                    }
//                    
//                };

                var FactoryObject = 
                {
                    _constructorMap:
                    {
                        'add-spatiu':   AddSpatiuConstructor,
                        'add-bloc':     AddBlocConstructor,
                    },
                    
                    getPaneConstructor: function( paneId )
                    {
                        var Ctor = this._constructorMap[ paneId ];
                        
//                        if( !Ctor )
//                        {
//                            throw new Error( 'Constructor not defined for pane "'+ paneId +'".' );
//                        }
//                        
                        return Ctor;
                    }
                };

                ////
                return FactoryObject;

            })();
            
            ////
            return Constructor;
            
        })();
        
        ////
        return Constructor;
        
    })();


    /////
    
    return Constructor;
    
})();

var SpatiiModule = _Components.Container.extend(
{
    _init_: function()
    {
        var self = this;
        
        this.EditSpatiiForm = new EditSpatiiForm( this.find( '#edit-spatii-form' ) );
        
        this.bind( 'click', '.save-button', function(){
            self.EditSpatiiForm.submit();
        });
        
        this.EditDialog = new EditDialogConstructor( this.find( '#edit-spatii-dialog' ) );
        
        this.bind( 'click', '.open-edit-dialog-button', function(){
            self.openEditDialog();
        });
        
        
    },
    
    openEditDialog: function()
    {
        this.EditDialog.open();
    }

});
            

////////////////////////////////////////////////////////////////////////////////

Modules.Spatii = SpatiiModule;

})();
