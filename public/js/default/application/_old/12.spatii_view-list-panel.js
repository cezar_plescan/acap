(function(){

_Spatii.ViewListPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.scaraId = null;
        
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.Module.ScariListPanel.addHandler( 'selectScara', function( event ){
            if( self.isVisible() )
            {
                var selectedScaraId = event.getArguments()[ 0 ];
                
                self._setScaraId( selectedScaraId );
                self._showList();
            }
        });
        
    },
    
    _setupResources_: function()
    {
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ), {
            paneConstructors: {
                list: SpatiiListConstructor
            }
        });
    },
    
    _parameters_: function( params )
    {
        this._setScaraId( params.scaraId );
    },
    
    _show_: function()
    {
        this.ScariListPanel.setSelectMode( 'scara' );
        
        if( !this.scaraId )
        {
            // get scaraId from ScariListPanel
            var scaraId;
            
            scaraId = this.ScariListPanel.getSelectedScara();
            
            if( scaraId )
            {
                this._setScaraId( scaraId );
            }
        }
        
        if( this.scaraId )
        {
            this._showList();
        }
        else
        {
            this._showSelectScaraMessage();
        }
    },
      
    _setScaraId: function( scaraId )
    {
        this.scaraId = scaraId ? scaraId : null;
    },
        
    _showList: function()
    {
        this.Panes.showPane( 'list', { scaraId: this.scaraId } );
    },
    
    _showSelectScaraMessage: function()
    {
        this.Panes.showPane( 'no-scara' );
    }

});

var SpatiiListConstructor = _Spatii.SpatiiPanelComponent.extend(
{
    
});

})();