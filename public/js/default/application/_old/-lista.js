(function(){


var ListaPlata = _Components.Container.extend(
{
    config: {
        monthArgName: 'm'
    },

    _init_: function()
    {
        this.Ajax = new _Components.Ajax( this.getContainer(), {
            baseUrl: '/api'
        });

        this.ViewListsPopup = null;

        this.$sumarCheltuieliContainer = this.find( '.sumar-cheltuieli-container' );
        this.$closeMonthButton = this.find( '.close-month-button' );
        this.$listePlataContainer = this.find( '.liste-plata-container' );
        this.$listePlataErrorContainer = this.find( '.liste-plata-error-container' );
        this.$computeListeButton = this.find( '.compute-liste-button' );
        this.$recomputeListeButton = this.find( '.recompute-liste-button' );

        this.$listePlataContainer.hide();
        this.$closeMonthButton.hide();
        this.$recomputeListeButton.hide();

        this._initCalculeazaListele();
        this._initInchidereLuna();

        this._initListeAnterioare();
    },

    _initCalculeazaListele: function()
    {
        var self = this;

        this.$computeListeButton.add( this.$recomputeListeButton ).on('click', function(){

            self.$listePlataErrorContainer.hide();

            self.Ajax.request({
                message: 'Vă rugăm aşteptaţi procesarea datelor',
                url: 'get-lista',
                type: 'get',
                success: function( response )
                {
                    self.$sumarCheltuieliContainer.hide();
                    self.$listePlataContainer.html( response ).show();
                    self.$computeListeButton.hide();
                    self.$closeMonthButton.show();
                    self.$recomputeListeButton.show();
                },
                error: function( message )
                {
                    self.$computeListeButton.show();
                    self.$recomputeListeButton.hide();
                    self.$closeMonthButton.hide();

                    self.$listePlataErrorContainer.show()
                            .find( '.error' ).text( message );

                    self.$listePlataContainer.hide();
                },
                abort: function()
                {
                    self.$computeListeButton.show();
                    self.$recomputeListeButton.hide();
                    self.$closeMonthButton.hide();
                }
            });
        });
    },

    _initInchidereLuna: function()
    {
        var self = this;

        this.$closeMonthButton.on('click', function(){
            if ( confirm('Sunteţi sigur că doriţi să închideţi această lună?') )
            {
                var successfulRequest = false;

                self.Ajax.request({
                    url: 'close-month',
					async: false,
                    success: function()
                    {
                        successfulRequest = true;
					}
                });

                if( successfulRequest )
                {
                    self._openListe();
                    reloadPage();
                }
            }
        });
    },

    _openListe: function( month )
    {
        var h = screen.availHeight,
            w = screen.availWidth;

        var ListaWindow = window.open('/lista/view' + ( typeof month != 'undefined' ? ( '?' + this.config.monthArgName + '=' + encodeURI(month) ) : '' ), 'liste', 'fullscreen=1,height='+h+',left=0,location=0,menubar=1,resizable=1,status=0,scrollbars=1,toolbar=0,top=0,width='+w);

        ListaWindow.focus();
        //ListaWindow.print();

    },

    _initListeAnterioare: function()
    {
        var self = this;

        var $listeAnterioareContainer = this.find( '.liste-anterioare-container' ),
            $button = $listeAnterioareContainer.find( '.liste-anterioare-button' ),
            $dropDown = $listeAnterioareContainer.find( '.liste-anterioare-drop-down' ),
            slideSpeed = Math.min( $dropDown.find( '.open-month-item' ).length * 50 + 150, 500 );

        this.bind('click', '.open-month-item', function(event){
            event.preventDefault();

            var month = $(this).data( 'month' );
            self._openListe( month );

            $dropDown.slideUp( slideSpeed );
        })

        $button.on('click', function(){
            $dropDown.slideToggle( slideSpeed );
        })

        $(document).on( 'mousedown', function( event ){
            var $target = $( event.target );

            if( !$target.closest( $listeAnterioareContainer ).length )
            {
                $dropDown.slideUp( slideSpeed );
            }
        });

    }

});

////////////////////////////////////////////////////////////////////////////////

Modules.ListaPlata = ListaPlata;

})();
