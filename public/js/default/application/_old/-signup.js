(function(){

var SignupFormClass = _Components.Form.extend(
{
    url: '/api/signup',

    _init_: function()
    {

    },

    _success: function()
    {
        // do a redirect
        alert('Contul dumneavoastră a fost creat cu succes.\nPentru utilizarea aplicaţiei este nevoie sa folosiţi Codul Fiscal şi parola completate mai devreme.');

        $('body').empty();
        location.href = '/';
    }
});

////////////////////////////////////////////////////////////////////////////////

Modules.Signup = _Components.Module.extend(
{
    _init_: function()
    {
        var SignupForm = new SignupFormClass( this.find('.signup-form') );

        this._addSubModule( SignupForm, 'signup-form');
    }
});


})();
