(function(){
        
_Spatii.SpatiiPanelSelectionPickParametru = _Spatii.SpatiiPanelSelectionPick.extend(
{
    _setup_: function()
    {
        this.parametruCode = null;
        
        this.SpatiiListConstructorBase = _Spatii.SpatiiListSelectionPickParametru;
    },
    
    setParametru: function( parametruCode )
    {
        this.parametruCode = parametruCode;
        
        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.setParametru( parametruCode );
        });
    }
    
});

})();