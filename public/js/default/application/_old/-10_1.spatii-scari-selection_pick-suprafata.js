(function(){
        
_Spatii.SpatiiScariListSelection = _Spatii.SpatiiScariList.extend(
{
    _setup_: function()
    {
        
    },
    
    _setupActions_:function() // ...
    {
        var self = this;
        
        this.bind( 'selectSpatii', function( e, spatiiData )
        {
            spatiiData.each( function( spatiuData )
            {
                var scaraId = spatiuData.scara_id;
                
                self._increaseCounter( scaraId );
            });
        });
        
        this.bind( 'deselectSpatii', function( e, spatiiData )
        {
            spatiiData.each( function( spatiuData )
            {
                var scaraId = spatiuData.scara_id;
                
                self._decreaseCounter( scaraId );
            });
        });

    },
    
    getSelection: function() ///... de revizuit
    {
        var selection;
        
        if( this._isSelectable() )
        {
            selection = [];
            
            this.find( '.spatiu-row.selected' ).each( function()
            {
                var spatiuId = $(this).data( 'id' );
                
                selection.push( spatiuId );
            });
        }
        else if( this.getContainer().hasClass( 'input-value' ) )
        {
            selection = {};
            
            this.find( '.spatiu-row' ).each( function()
            {
                var $spatiuRow = $(this),
                    spatiuId = $spatiuRow.data( 'id' ),
                    spatiuValue = $spatiuRow.find( 'input[name=suma_spatiu]' ).val();
                
                if( !spatiuValue.isBlank() )
                {
                    selection[ spatiuId ] = spatiuValue;
                }
            });
        }
        
        return selection;
    },
    
    selectSpatii: function( spatii )
    {
        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.selectSpatii( spatii );
        });
    },
    
    _increaseCounter: function( scaraId )
    {
        this.ScariLink.increaseCounter( scaraId );
    },
    
    _decreaseCounter: function( scaraId )
    {
        this.ScariLink.decreaseCounter( scaraId );
    },
    
});

//------------------------------------------------------------------------------

_Spatii.SpatiiPaneListSelection = _Spatii.SpatiiPaneList.extend(
{
    _setupActions_: function()
    {
        var self = this;
        
        this.find( '.select-all-button' ).on( 'click', function()
        {
            var $rows = self._getVisibileRows( self._getSpatiuRows( ':not(.disabled,.selected)' ) );

            self._selectRow( $rows );
        });

        this.find( '.deselect-all-button' ).on( 'click', function()
        {
            var $rows = self._getVisibileRows( self._getSpatiuRows( ':not(.disabled).selected' ) );

            self._deselectRow( $rows );
        });

        this.bind( 'click', '.spatiu-row:not(.disabled)', function()
        {
            var $row = $(this);

            if( self._isRowSelected( $row ) )
            {
                self._deselectRow( $row );
            }
            else
            {
                self._selectRow( $row );
            }
        });

        this.$listContainer.on( 'mouseenter', '.etaj-cell', function()
        {
            var $etajCell = $(this),
                $etajContainer = self._getEtajContainerOfElement( $etajCell );

            $etajContainer.addClass( 'hover' );
        });

        this.$listContainer.on( 'mouseleave', '.etaj-cell', function()
        {
            var $etajCell = $(this),
                $etajContainer = self._getEtajContainerOfElement( $etajCell );

            $etajContainer.removeClass( 'hover' );
        });

        this.$listContainer.on( 'click', '.etaj-cell', function()
        {
            var $etajCell = $(this),
                $etajContainer = self._getEtajContainerOfElement( $etajCell ),
                $rows = self._getVisibileRows( $etajContainer.find( '.spatiu-row:not(.disabled)' ) ),
                allSelected = true;

            $rows.each( function()
            {
                var $row = $(this);

                if( !self._isRowSelected( $row ) )
                {
                    allSelected = false;

                    return false;
                }
            });

            if( allSelected )
            {
                $rows = $rows.filter( '.selected' );
                self._deselectRow( $rows );
            }
            else
            {
                $rows = $rows.not( '.selected' );
                self._selectRow( $rows );
            }
        });
        
    },
    
    _reset_: function()
    {
        this._deselectRow( this._getSpatiuRows(), false );
    },
    
    selectSpatii: function( spatii )
    {
        if( !Object.isArray( spatii ) || !spatii.length ) return;
        
        var self = this,
            $rows = this._getSpatiuRows( ':not(.disabled,.selected)' );

        $rows.each( function()
        {
            var $row = $(this),
                spatiuId = $row.data( 'id' );
                
            if( spatii.indexOf( spatiuId ) >= 0 )
            {
                self._selectRow( $row );
            }
        });
    },
    
    _selectRow: function( $row, triggerEvent )
    {
        $row.addClass( 'selected' );
        
        if( triggerEvent === undefined || triggerEvent )
        {
            var spatiiData = [];
            
            $row.each( function()
            {
                var spatiuData = $(this).data( 'data' );
                
                spatiiData.push( spatiuData );
            });
            
            this._triggerDOM( 'selectSpatii', spatiiData );
        }
    },
    
    _deselectRow: function( $row, triggerEvent )
    {
        $row.removeClass( 'selected' );

        if( triggerEvent === undefined || triggerEvent )
        {
            var spatiiData = [];
            
            $row.each( function()
            {
                var spatiuData = $(this).data( 'data' );
                
                spatiiData.push( spatiuData );
            });
            
            this._triggerDOM( 'deselectSpatii', spatiiData );
        }
    },
    
    _isRowSelected: function( $row )
    {
        return $row.hasClass( 'selected' );
    }
    
});

//==============================================================================

_Spatii.ScariListSelection = _Spatii.ScariListPanel.extend(
{
    _setup_: function()
    {
        this.selectedSpatiiCounters = {};
        this.totalSpatiiCounters = {};
    },
    
    _setupActions_:function()
    {
        // actualizare numar apartamente dupa stergere sau adaugare ....
    },
    
    _init_: function()
    {
        this._populateSpatiiCounters();
    },
    
    _populateSpatiiCounters: function()
    {
        var self = this,
            StructuraData = DataResource.get( 'Structura' );
        
        this._getScaraItems().each( function()
        {
            var $scaraItem = $(this),
                $totalCounter = $scaraItem.find( '.total-spatii-counter' ),
                $selectedSpatiiCounter = $scaraItem.find( '.selected-spatii-counter' ),
                scaraId = $scaraItem.data( 'id' ),
                spatiiCounter = StructuraData.getSpatiiCount( scaraId );
            
            $selectedSpatiiCounter.text( '0' );
            $selectedSpatiiCounter.addClass( 'empty' );
            $totalCounter.text( spatiiCounter );
            
            self.totalSpatiiCounters[ scaraId ] = spatiiCounter;
            self.selectedSpatiiCounters[ scaraId ] = 0;
        });
    },
    
    increaseCounter: function( scaraId )
    {
        var $scaraItem = this._getScaraItem( scaraId ),
            $counter = $scaraItem.find( '.selected-spatii-counter' ),
            $totalCounter = $scaraItem.find( '.total-spatii-counter' );
        
        if( !this.selectedSpatiiCounters[ scaraId ] )
        {
            this.selectedSpatiiCounters[ scaraId ] = 0;
            
            $counter.show();
            
            $totalCounter.text( this.totalSpatiiCounters[ scaraId ] );
        }
        
        this.selectedSpatiiCounters[ scaraId ]++;
        
        $counter.text( this.selectedSpatiiCounters[ scaraId ] );
        
        $counter.removeClass( 'empty' );
    },
    
    decreaseCounter: function( scaraId )
    {
        if( !this.selectedSpatiiCounters[ scaraId ] )
        {
            return;
        }
        
        var $scaraItem = this._getScaraItem( scaraId ),
            $counter = $scaraItem.find( '.selected-spatii-counter' );
        
        this.selectedSpatiiCounters[ scaraId ]--;
        
        $counter.text( this.selectedSpatiiCounters[ scaraId ] );
        
        if( this.selectedSpatiiCounters[ scaraId ] == 0 )
        {
            $counter.addClass( 'empty' );
        }
    },
    
    setTotalSpatii: function( scaraId, total )
    {
        var $scaraItem = this._getScaraItem( scaraId ),
            $totalCounter = $scaraItem.find( '.total-spatii-counter' );
        
        this.totalSpatiiCounters[ scaraId ] = total;
        $totalCounter.text( total );
    },
    
    _reset_: function()
    {
        var self = this;
        
        this._getScaraItems().each( function()
        {
            var $scaraItem = $(this),
                $selectedSpatiiCounter = $scaraItem.find( '.selected-spatii-counter' ),
                scaraId = $scaraItem.data( 'id' );
            
            $selectedSpatiiCounter.text( '0' );
            $selectedSpatiiCounter.addClass( 'empty' );
            
            self.selectedSpatiiCounters[ scaraId ] = 0;
        });
        
    }

});

})();