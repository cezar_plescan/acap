(function(){

_Spatii.SpatiiScari = _Components.Container.extend(
{
    _setup_: function()
    {
        this.ScariPanelConstructorBase = _Spatii.ScariListPanelComponent;
        this.SpatiiPanelConstructorBase = _Spatii.SpatiiPanelComponent;
        
        this.ScariPanelConstructor = null;
        this.SpatiiPanelConstructor = null;
        this.SpatiiListConstructor = null;
    },
    
    _setupResources_: function()
    {
        if( this.ScariPanelConstructor === null )
        {
            this.ScariPanelConstructor = this.ScariPanelConstructorBase;
        }
        else
        {
            if( !_Components.Class.isInstanceOf( this.ScariPanelConstructor, this.ScariPanelConstructorBase ) )
            {
                System.error( 'Check "ScariPanelConstructor" class' );
            }
        }
        
        if( this.SpatiiPanelConstructor === null )
        {
            this.SpatiiPanelConstructor = this.SpatiiPanelConstructorBase;
        }
        else
        {
            if( !_Components.Class.isExtendedFrom( this.SpatiiPanelConstructor, this.SpatiiPanelConstructorBase ) )
            {
                System.error( 'Check "SpatiiPanelConstructor" class' );
            }
        }
        
        this.ScariPanel = new this.ScariPanelConstructor( this.find( '.scari-list-component' ) );
        this.SpatiiPanel = new this.SpatiiPanelConstructor( this.find( '.spatii-list-component' ), {
            SpatiiListConstructor:  this.SpatiiListConstructor
        });
    },
    
    _setupActions_:function()
    {
        var self = this;
        
        this.ScariPanel.addHandler( 'selectScara', function( event, selectedScaraId )
        {
            self.SpatiiPanel.setScaraId( selectedScaraId );
        });

    },
    
    _init_: function()
    {
        this.ScariPanel.setSelectMode( 'scara' );
        
        this._checkIfScaraIsSelected();
    },
    
    _reset_: function()
    {
        this.ScariPanel.reset();
        this.SpatiiPanel.reset();
        
        this._checkIfScaraIsSelected();
    },
    
    showSpatii: function( spatii )
    {
        this.SpatiiPanel.showSpatii( spatii );
    },
    
    _checkIfScaraIsSelected: function()
    {
        var scaraId = this.ScariPanel.getSelectedScara();

        if( scaraId )
        {
            this.SpatiiPanel.setScaraId( scaraId );
        }
    }
    
});

})();
