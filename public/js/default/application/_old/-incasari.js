(function(){

var SelectSpatiuComponent = _Components.Container.extend(
{
    _init_: function()
    {
        var self = this;

        this.$blocSelect            = this.find( '.bloc-select-element' );
        this.$scaraSelectContainer  = this.find( '.scara-container' );
        this.$scaraSelect           = this.$scaraSelectContainer.find( '.scara-select-element' );
        this.$spatiuSelectContainer = this.find( '.spatiu-container' );
        this.$spatiuSelect          = this.$spatiuSelectContainer.find( '.spatiu-select-element' );

        this.reset();

        this.$blocSelect.change(function(){

            // remove default option
            self.$blocSelect.children('[value=""]').hide();

            // show corresponding SELECT element
            var blocId = self.$blocSelect.val();

            self.$scaraSelect
                    .show()
                    .children()
                        .prop('selected', false)
                        .hide()
                        .filter('[value=""], [data-bloc="'+blocId+'"]').show();

            self.$scaraSelectContainer.show();

            self.$spatiuSelectContainer.hide();
            self.$spatiuSelect
                    .children().prop('selected', false);
        });

        this.$scaraSelect.change(function(){

            // remove default option
            self.$scaraSelect.children('[value=""]').hide();

            // show corresponding SELECT element
            var scaraId = self.$scaraSelect.val();
            self.$spatiuSelect
                    .show()
                    .children()
                        .prop('selected', false)
                        .hide()
                        .filter('[value=""], [data-scara="'+scaraId+'"]').show();

            self.$spatiuSelectContainer.show();

        });

        this.$spatiuSelect.change(function(){

            // remove default option
            self.$spatiuSelect.children('[value=""]').hide();

        });

    },

    reset: function()
    {
        this.$blocSelect.children()
                .show()
                .prop('selected', false);

        this.$scaraSelectContainer.hide();
        this.$spatiuSelectContainer.hide();

    }

});

var SelectDataComponent = _Components.Container.extend(
{
    _init_: function()
    {
        var self = this;

        this.$input = this.find( '.data-select-element' );
        this.$storeDate = $('<input type="hidden" />');

        this.$storeDate
                .insertAfter( this.$input )
                .attr( 'name', this.$input.attr( 'name' ) );

        this.$input
                .removeAttr( 'name' );

        this.$input.datepicker({
            maxDate: new Date(),
            onSelect: function( date, instance )
            {
                var y = instance.selectedYear,
                    m = parseInt( instance.selectedMonth ) + 1,
                    d = instance.selectedDay;

                m = ( m.toString().length < 2 ) ? '0' + m : m;
                d = ( d.toString().length < 2 ) ? '0' + d : d;

                var date = y + '-' + m + '-' + d;
                self.$storeDate.val( date );
            }
        });
    },

    reset: function()
    {
        this.$input.val('');
        this.$storeDate.val('');
    }

});

var Add = _Components.Form.extend(
{
    url: '/api/add-incasare',

    _init_: function()
    {
        this.SelectSpatiu = new SelectSpatiuComponent( this.find( '.section.section-spatiu' ) );

        this.SelectData = new SelectDataComponent( this.find( '.section.section-data' ) );
    },

    reset: function()
    {
        this._super();

        this.SelectSpatiu.reset();
        this.SelectData.reset();

    },

    _success: function( response )
    {
        this._trigger('onAdd');

        this.reset();

        alert('Încasarea a fost înregistrată.');
    },

    _error: function()
    {
        alert('EROARE: Verificaţi informaţiile completate!');
    }

});

// LIST CHELTUIELI /////////////////////////////////////////////////////////////

var List = _Components.Container.extend(
{
    _init_: function()
    {
        this.Ajax = new _Components.Ajax( this.getContainer(), {
            baseUrl: '/api'
        })

        this.$listContainer = this.find( '.list-cheltuieli-container' );
        this.$noResults = this.find( '.no-results' );
        this.$valoareTotala = this.find( '.valoare-totala' );

        this._checkList();

        this._initDelete();

    },

    _initDelete: function()
    {
        var self = this;

        this.bind('click', '.delete-cheltuiala', function(){
            if ( !confirm('Doriţi să ştergeţi această cheltuială ?') )
            {
                return;
            }

            var $button = $(this),
                cheltuialaId = $button.data('id');

            self.Ajax.request({
                url: 'delete-cheltuiala',
                data: {
                    id: cheltuialaId
                },
                success: function()
                {
                    $button.closest('.cheltuiala-item').slideUp(function(){
                        $(this).remove();
                        self._checkList();
                        self._updateValoareTotala();
                    });
                }
            });
        })
    },

    _getCheltuialaItems: function()
    {
        return this.$listContainer.children( '.cheltuiala-item' );
    },

    _checkList: function()
    {
        if( this._getCheltuialaItems().length )
        {
            this.$noResults.hide();
            this.$listContainer.show();
        }
        else
        {
            this.$noResults.show();
            this.$listContainer.hide();
        }

    },

    _updateValoareTotala: function()
    {
        var total = 0;
        this._getCheltuialaItems().each(function(){
            var cheltuialaValue = parseFloat( $(this).find( '.valoare-cheltuiala' ).text() );

            total += cheltuialaValue;
        });

        this.$valoareTotala.text( total );
    }

});

// MAIN MODULE /////////////////////////////////////////////////////////////////

var Incasari = _Components.Container.extend(
{
    _init_: function()
    {
        var self = this;

        this.AddComponent = new Add( $('#add-incasari-module'), {
            onAdd: function()
            {
                //self._reloadList();
            }
        });

        this.ListComponent = new List( $('#list-incasari-module') );

    },

    _reloadList: function()
    {
        var self = this;

        var Ajax = new _Components.Ajax();
        Ajax.request({
            url: '/cheltuieli/list',
            dataType: 'html',
            type: 'get',
            success: function( response )
            {
                var $newContainer = $( $.trim(response) );
                self.ListComponent.getContainer().replaceWith( $newContainer );

                self.ListComponent = new List( $newContainer );
            }
        });
    }

});

////////////////////////////////////////////////////////////////////////////////

Modules.Incasari = Incasari;

})();
