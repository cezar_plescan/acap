(function(){

_Spatii.ScariListPanelComponent = _Spatii.ScariListComponent.extend(
{
    _setup_: function()
    {
        this.currentBlocId = null;
        this.currentScaraId = null;
        this.selectMode = null;
        this.canSelect = true;
    },
    
    _setupElements_: function()
    {
        this.$emptyList = this.getContainer().children( '.empty-list' );
    },
    
    _setupResources_: function()
    {

    },
    
    _setupActions_: function()
    {
        this._bindSelectScara();
        this._bindSelectBloc();
    },
    
    _init_: function()
    {
        
    },
    
    _reset_: function()
    {
        this.currentBlocId = null;
        this.currentScaraId = null;
        
        this._unhighlightItems();
    },
    
    setCurrentBloc: function( blocId )
    {
        if( !blocId ) return;
        
        this.currentBlocId = blocId;
        this._selectBloc( blocId );
    },
    
    setCurrentScara: function( scaraId )
    {
        if( !scaraId ) return;
        
        var blocId;
        
        this.currentScaraId = scaraId;
        this._selectScara( scaraId );
        
        blocId = this._getScaraBlocId( scaraId );
        this.currentBlocId = blocId;
    },
    
    getSelectedBloc: function()
    {
        return this.currentBlocId;
    },
    
    getSelectedScara: function()
    {
        return this.currentScaraId;
    },
    
    setSelectMode: function( mode )
    {
        var actions = {
            scara: function()
            {
                this.getContainer()
                        .removeClass( 'select-bloc' )
                        .addClass( 'select-scara' );
                
                this._selectCurrentScara();
                this._autoSelectScara();
            },
            bloc: function()
            {
                this.getContainer()
                        .removeClass( 'select-scara' )
                        .addClass( 'select-bloc' );
                
                this._selectCurrentBloc();
                this._autoSelectBloc();
            }
        };
        
        if( typeof actions[ mode ] !== 'undefined' )
        {
            this.selectMode = mode;
            this._enableSelection();
            this._unhighlightItems();
            
            Object.isFunction( actions[ mode ] ) && actions[ mode ].call( this );
        }
    },
    
    disableSelection: function( unhighlightItems )
    {
        if( unhighlightItems === undefined )
        {
            unhighlightItems = true;
        }
        
        this.canSelect = false;
        this.getContainer().removeClass( 'enable-selection' );
        
        if( unhighlightItems )
        {
            this._unhighlightItems();
        }
    },
    
    _enableSelection: function()
    {
        this.canSelect = true;
        this.getContainer().addClass( 'enable-selection' );
    },
    
    _generateContent: function()
    {
        this._super();
        
        this._checkForEmptyList();
    },
    
    _checkForEmptyList: function()
    {
        if( this.StructuraData.isEmpty() )
        {
            this.$emptyList.show();
        }
        else
        {
            this.$emptyList.hide();
        }
    },
    
    _addBloc: function( blocData, options )
    {
        this._super.apply( this, arguments );
        
        this.$emptyList.hide();
    },
    
    _removeBloc: function( blocData )
    {
        this._super.apply( this, arguments );
        
        if( blocData.id == this.currentBlocId )
        {
            this.currentBlocId = null;
            this.currentScaraId = null;
        }
        
        this._checkForEmptyList();
        
        this._autoSelectBloc();
    },
    
//    _editBloc: function( blocData, options )
//    {
//        this._super.apply( this, arguments );
//    },
    
    _removeScara: function( scaraData )
    {
        this._super.apply( this, arguments );
        
        if( scaraData.id == this.currentScaraId )
        {
            this.currentScaraId = null;
        }
        
        this._autoSelectScara();
    },
    
    _bindSelectScara: function()
    {
        var self = this;
        
        this.bind( 'click', '.scara-label', function()
        {
            if( !self._isSelectScaraModeAvailable() ) return;
            
            var scaraId = $(this).closest( '.scara-item' ).data( 'id' );
            
            if( scaraId == self.currentScaraId ) return;
            
            self.setCurrentScara( scaraId );
            self._trigger( 'selectScara', scaraId );
        });
    },
    
    _bindSelectBloc: function()
    {
        var self = this;
        
        this.bind( 'click', '.bloc-label', function()
        {
            if( !self._isSelectBlocModeAvailable() ) return;
            
            var blocId = $(this).closest( '.bloc-item' ).data( 'id' );
            
            if( blocId == self.currentBlocId ) return;
            
            self.setCurrentBloc( blocId );
            self._trigger( 'selectBloc', blocId );
        });
    },
    
    _selectScara: function( scaraId )
    {
        var $label;
        
        $label = this._getScaraItemLabel( scaraId );
        
        this._unhighlightItems();
        
        $label.addClass( 'selected' );
    },
    
    _selectBloc: function( blocId )
    {
        var $label;
        
        $label = this._getBlocItemLabel( blocId );
        
        this._unhighlightItems();
        
        $label.addClass( 'selected' );
    },
    
    _isSelectionEnabled: function()
    {
        return this.canSelect;
    },
    
    _isSelectScaraModeAvailable: function()
    {
        return this._isSelectionEnabled() && this.selectMode == 'scara';
    },
    
    _isSelectBlocModeAvailable: function()
    {
        return this._isSelectionEnabled() && this.selectMode == 'bloc';
    },
    
    _selectCurrentBloc: function()
    {
        this.setCurrentBloc( this.currentBlocId );
    },
    
    _selectCurrentScara: function()
    {
        this.setCurrentScara( this.currentScaraId );
    },
    
    _unhighlightItems: function()
    {
        this._getAllBlocItemLabels().removeClass( 'selected' );
        this._getAllScaraItemLabels().removeClass( 'selected' );
    },
    
    /**
     * daca a mai ramas doar un singur bloc, aceasta este selectat automat
     */
    _autoSelectBloc: function()
    {
        var $blocItems, blocId;
        
        $blocItems = this._getBlocItems();
        if( $blocItems.length == 1 )
        {
            blocId = $blocItems.eq( 0 ).data( 'id' );
            this.setCurrentBloc( blocId );
        }
    },
    
    /**
     * daca a mai ramas doar o singura scara, aceasta este selectata automat
     */
    _autoSelectScara: function()
    {
        var $scaraItems, scaraId;
        
        $scaraItems = this._getScaraItems().filter( ':visible' );
        if( $scaraItems.length == 1 )
        {
            scaraId = $scaraItems.eq( 0 ).data( 'id' );
            this.setCurrentScara( scaraId );
        }
    },
    
    _hideScara: function()
    {
        this._super.apply( this, arguments );
        
        this._autoSelectScara();
    }
    
});

})();
