(function(){
        
_Spatii.SpatiiPanelSelection = _Spatii.SpatiiPanelComponent.extend(
{
    _setup_: function()
    {
        this.paneSelections = {};
        
        this.SpatiiListConstructorBase = _Spatii.SpatiiListSelection;
    },
    
    _setupActions_:function()
    {
        var self = this;
        
        this.addHandler( 'loadPane', function( event, paneId, Pane )
        {
            Pane.addHandler( 'change', function( event, selection )
            {
                self._triggerPaneChange( paneId, selection );
            });
        });

    },
    
    _reset_: function()
    {
        this.paneSelections = {};
    },
    
    getSelection: function()
    {
        var selection = {};
        
        Object.each( this.paneSelections, function( paneId, paneSelection )
        {
            Object.extend( selection, paneSelection );
        });
        
        return selection;
    },
    
    setSelection: function( spatii )
    {
        var self = this;
        
        Object.each( this.Panes, function( paneId, Pane )
        {
            self.paneSelections[ paneId ] = Pane.setSelection( spatii );
        });
    },
    
    getSelectionsPerScara: function()
    {
        return Object.clone( this.paneSelections );
    },
    
    _triggerPaneChange: function( paneId, paneSelection )
    {
        var selection;
        
        this.paneSelections[ paneId ] = paneSelection;
        selection = this.getSelection();
        
        this._trigger( 'paneChange', paneId, paneSelection );
        this._trigger( 'change', selection );
    }
    
});

})();