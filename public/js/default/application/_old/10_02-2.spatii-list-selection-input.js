(function(){
        
_Spatii.SpatiiListSelectionInput = _Spatii.SpatiiListSelection.extend(
{
    __abstract: [ '_validateValue' ],
    
    _setup_: function()
    {
        this.errorsCount = 0;
    },
    
    _selectRow: function( $row, selectionValue )
    {
        $row.find( 'input[name=suma_spatiu]' ).val( selectionValue );
    },
    
    _getSelectionValue: function( $row )
    {
        var $input = $row.find( 'input[name=suma_spatiu]' ),
            value = $input.val().trim().toNumber();
        
        if( value && !$input.hasClass( 'has-error' ) )
        {
            return value;
        }
    },
    
    _clearSelection: function()
    {
        this._getSpatiuRows().find( 'input[name=suma_spatiu]' ).val( '' );
    },
    
    _setupActions_: function()
    {
        var self = this,
            timeout;
        
        this.bind( 'keyup paste change', 'input[name=suma_spatiu]', function( event )
        {
            var $input = $(this);
            
            var checkInput = function()
            {
                var value = $input.val().trim(),
                    oldValue = $input.data( 'old-value' ),
                    triggerErrorEvent = false,
                    triggerErrorOffEvent = false;
                    
                if( value != oldValue )
                {
                    var $spatiuRow = $input.closest( '.spatiu-row' ),
                        spatiuId = $spatiuRow.data( 'id' );

                    if( self._isValueValid( value, spatiuId, $input ) )
                    {
                        var hadError = $input.hasClass( 'has-error' );
                                
                        if( hadError )
                        {
                            $input.removeClass( 'has-error' );
                            
                            self.errorsCount --;
                            
                            triggerErrorOffEvent = ( self.errorsCount == 0 );
                        }
                        
                        self._triggerChange();
                    }
                    else
                    {
                        hadError = $input.hasClass( 'has-error' );
                        
                        if( !hadError )
                        {
                            $input.addClass( 'has-error' );
                            
                            triggerErrorEvent = ( self.errorsCount == 0 );
                            
                            self.errorsCount ++;
                            
                            self._triggerChange();
                        }
                    }
                    
                    $input.data( 'old-value', value );
                    
                    if( triggerErrorEvent )
                    {
                        self._trigger( 'error', spatiuId );
                    }
                    else if( triggerErrorOffEvent )
                    {
                        self._trigger( 'errorOff' );
                    }
                }
            };
            
            clearTimeout( timeout );
            
            if( event.type == 'change' )
            {
                checkInput();
            }
            else
            {
                timeout = setTimeout( checkInput, 300 );
            }
        });
    },
    
    _reset_: function()
    {
        this.errorsCount = 0;
    },
    
    _hangup_: function()
    {
        if( this._hasErrors() )
        {
            System.info().close();
            
            this.reset();
        }
    },
    
    _hasErrors: function()
    {
        return Object.toBoolean( this.errorsCount );
    },
    
    _isValueValid: function( value, spatiuId, $input )
    {
        var isValid = true,
            errorMessage = this._validateValue( value, spatiuId, $input );
        
        if( errorMessage !== null )
        {
            isValid = false;
            
            System.info( errorMessage, {
                autoClose: false,
                warning: true
            });
            
            $input.select().focus();
        }
        else
        {
            System.info().close();
        }
        
        return isValid;        
    },    
});

})();