(function(){

_Cheltuieli.SpatiiIndividualeDialog = _Cheltuieli._InputSpatiiDialog.extend(
{
    _setup_: function()
    {
        this.hasErrors = false;
        
        this.ScariListConstructor = ScariListConstructor;
        this.SpatiiListConstructor = SpatiiListConstructor;
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.SpatiiList.addHandler( 'error', function()
        {
            self.hasErrors = true;
            self._computeDoneButtonStatus();
        });
        
        this.SpatiiList.addHandler( 'errorOff', function()
        {
            self.hasErrors = false;
            self._computeDoneButtonStatus();
        });
        
    },
    
    _getSelectionTitle: function()
    {
        var title = this._super(),
            sumaText,
            sumaPartiala = this._getSumaPartiala(),
            rest,
            restText = '';
        
        sumaText = sumaPartiala.round( 2 ) + ' lei';

        if( !this._isSumaDistribuita() )
        {
            rest = this._getSumaRest().round( 4 );
            restText = ' (rest: ' + rest + ' lei)';

            sumaText += restText;
        }

        title += ', ' + sumaText;
            
        return title;
    },
    
    _getInitialTitle: function()
    {
        var title,
            sumaText;

        sumaText = ' (Suma: ' + this._getSuma().round( 2 ) + ' lei)';
        title = this._super() + sumaText;

        return title;
    },
    
    _getSumaPartiala: function()
    {
        return Object.sum( this.selection );
    },
    
    _getSumaRest: function()
    {
        return this._getSuma() - this._getSumaPartiala();
    },
    
    _isSumaDistribuita: function()
    {
        return this._getSumaRest().abs() < Math.pow( 10, -16 );
    },
    
    _isDone: function()
    {
        return this._super() && !this.hasErrors && this._isSumaDistribuita();
    }
    
});

//==============================================================================

var SpatiiListConstructor = _Spatii.SpatiiPanelComponent.extend(
{
    _setup_: function()
    {
        this.PaneListConstructor = SpatiiPaneListConstructor;
        
        this.errorsCount = 0;
        
        this.paneSelection = {};
    },
    
    _setupActions_:function()
    {
        var self = this;
        
        this.addHandler( 'loadPane', function( event )
        {
            var Pane = event.getArguments()[ 1 ],
                paneId = event.getArguments()[ 0 ];
            
            self.paneSelection[ paneId ] = {};
            
            Pane.addHandler( 'error', function()
            {
                self.errorsCount ++;
                
                if( self.errorsCount == 1 )
                {
                    self._trigger( 'error' );
                }
            });
            
            Pane.addHandler( 'errorOff', function()
            {
                self.errorsCount --;
                
                if( self.errorsCount == 0 )
                {
                    self._trigger( 'errorOff' );
                }
            });
            
            Pane.addHandler( 'change', function( event )
            {
                var paneSelection = event.getArguments()[ 0 ];
                
                self._updateSelection( paneId, paneSelection );
            });
        });
    },
    
    getSelection: function()
    {
        var selection = {};
        
        Object.each( this.paneSelection, function( paneId, paneSelection )
        {
            Object.extend( selection, paneSelection );
        });
        
        return selection;
    },
    
    setSelection: function( selection )
    {
        var self = this;
        
        this.paneSelection = {};
        
        Object.each( this.Panes, function( paneId, Pane )
        {
            var paneSelection = Pane.setSelection( selection );
            
            self._storeSelection( paneId, paneSelection );
        });
    },
    
    _hangup_: function()
    {
        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.hangup();
        });
        
        this.errorsCount = 0;
    },
    
    _updateSelection: function( paneId, paneSelection )
    {
        this._storeSelection( paneId, paneSelection );
        
        this._triggerSelectionChange();
    },
    
    _storeSelection: function( paneId, paneSelection )
    {
        this.paneSelection[ paneId ] = paneSelection;
        
        this.ScariLink.setSelectionCounter( paneId, Object.size( paneSelection ) );
    },
    
    _triggerSelectionChange: function()
    {
        var selection = this.getSelection();
        
        this._trigger( 'change', selection );
    }
    
});

//==============================================================================

var ScariListConstructor = _Spatii.ScariListPanel.extend(
{
    _setup_: function()
    {
        this.selectedSpatiiCounters = {};
        this.totalSpatiiCounters = {};
    },
    
    _setupActions_:function()
    {
        
    },
    
    _init_: function()
    {
        this._populateSpatiiCounters();
    },

    _reset_: function()
    {
        var self = this;
        
        this._getScaraItems().each( function()
        {
            var $scaraItem = $(this),
                $selectedSpatiiCounter = $scaraItem.find( '.selected-spatii-counter' ),
                scaraId = $scaraItem.data( 'id' );
            
            $selectedSpatiiCounter.text( '0' );
            $selectedSpatiiCounter.addClass( 'empty' );
            
            self.selectedSpatiiCounters[ scaraId ] = 0;
        });
        
    },

    setSelectionCounter: function( scaraId, counter )
    {
        var $scaraItem = this._getScaraItem( scaraId ),
            $counter = $scaraItem.find( '.selected-spatii-counter' );
        
        this.selectedSpatiiCounters[ scaraId ] = counter;
        
        $counter.text( this.selectedSpatiiCounters[ scaraId ] );
        
        counter ?
            $counter.removeClass( 'empty' ) :
            $counter.addClass( 'empty' );
    },
    
    _populateSpatiiCounters: function()
    {
        var self = this,
            StructuraData = DataResource.get( 'Structura' );
        
        this._getScaraItems().each( function()
        {
            var $scaraItem = $(this),
                $totalCounter = $scaraItem.find( '.total-spatii-counter' ),
                $selectedSpatiiCounter = $scaraItem.find( '.selected-spatii-counter' ),
                scaraId = $scaraItem.data( 'id' ),
                spatiiCounter = StructuraData.getSpatiiCount( scaraId );
            
            $selectedSpatiiCounter.text( '0' );
            $selectedSpatiiCounter.addClass( 'empty' );
            $totalCounter.text( spatiiCounter );
            
            self.totalSpatiiCounters[ scaraId ] = spatiiCounter;
            self.selectedSpatiiCounters[ scaraId ] = 0;
        });
    },
    
        
});


})();