(function(){
    
_Cheltuieli.ParametriColoanaNrPersoane = _Cheltuieli.ParametriColoanaAbstract.extend(
{
    _setupResources_: function()
    {
        this.SpatiiSection = new SpatiiSectionConstructor( this.find( '>.spatii-section' ) );
        this.ReducereSection = new ReducereSectionConstructor( this.find( '>.reducere-section' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.SpatiiSection.addHandler( 'change', function()
        {
            var spatii = this.getSpatii();
            
            self.ReducereSection.reset();
            
            if( spatii )
            {
                self.ReducereSection.show();
                self.ReducereSection.setSpatii( spatii );
            }
            else
            {
                self.ReducereSection.hide();
            }
        });
    },
    
    _init_: function()
    {
        this.ReducereSection.hide();
    },
    
    getParametri: function()
    {
        var parametri = {
            spatii:     this.SpatiiSection.getSpatii(),
            reducere:   this.ReducereSection.getReducere(),
        };
        
        return parametri;
    }
});

//==============================================================================

var SpatiiSectionConstructor = _Cheltuieli.SelectSpatiiSection.extend(
{
    _createSpatiiDialogInstance: function()
    {
        var Instance = new _Cheltuieli.SpatiiSelectionPersoaneDialog( $( '#select-spatii-nr_pers-dialog' ), {
            height: 'auto'
        });
        
        return Instance;
    }
    
});

//==============================================================================

var ReducereSectionConstructor = _Cheltuieli.ReducerePersoaneSection.extend(
{

});

})();