(function(){

_Spatii.RemoveSpatiuPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.scaraId = null;
    },
    
    _setupElements_: function()
    {
        this.$instructionSelectScara = this.find( '.instructions .select-scara' );
    },
    
    _setupResources_: function()
    {
        var constructorOptions = {
            Panel: this
        };
        
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ), {
            paneConstructors: {
                list:       [ ListPaneConstructor, constructorOptions ]
            }
        });
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.Module.ScariListPanel.addHandler( 'selectScara', function( event )
        {
            if( self.isVisible() )
            {
                var selectedScaraId = event.getArguments()[ 0 ];
                
                self._setScaraId( selectedScaraId );
                self._displayList();
            }
        });
    },
    
    _parameters_: function( params )
    {
        this._setScaraId( params.scaraId );
    },
    
    _show_: function()
    {
        if( !this.scaraId )
        {
            // get scaraId from ScariListPanel
            var scaraId;
            
            scaraId = this.ScariListPanel.getSelectedScara();
            
            if( scaraId )
            {
                this._setScaraId( scaraId );
            }
        }
        
        if( !this.scaraId )
        {
            this.Panes.showPane( 'select-scara' );
        }
        else
        {
            this._displayList();
        }
    },
    
    _setScaraId: function( scaraId )
    {
        var ListPane;
        
        this.scaraId = scaraId ? scaraId : null;
        
        if( scaraId )
        {
            ListPane = this.Panes._getPaneObject( 'list' );
            if( ListPane )
            {
                ListPane.setScaraId( scaraId );
            }
        }
    },
    
    _displayList: function()
    {
        if( this.Panes.getCurrentPaneId() == 'list' )
        {
            this.Panes.getPane( 'list' )
                    .reset()
                    .sendParameters( { scaraId: this.scaraId } );
        }
        else
        {
            this.Panes.showPane( 'list', { scaraId: this.scaraId } );
        }
    },
    
    _getScaraId: function()
    {
        return this.scaraId;
    }

});

var ListPaneConstructor = _Components.Container.extend(
{
    _setup_: function()
    {
        this.Panel = this._options.Panel;
    },
    
    _setupResources_: function()
    {
        this.SpatiiList = new SpatiiListConstructor( this.find( '.spatii-list-component' ), {
            Parent:     this,
            scaraId:    this._options.scaraId
        });
        
        this.ConfirmFrame = new ConfirmFrameConstructor( this.find( '.confirm-frame' ), {
            Parent:     this,
            Panel:      this.Panel
        });
    },
    
    _parameters_: function( params )
    {
        this.SpatiiList.sendParameters( params );
    },
    
    _show_: function()
    {
        this.reset();
    },
    
    _reset_: function()
    {
        this.Panel.ScariListPanel.setSelectMode( 'scara' );
        this.hideConfirmation();
        
        this.Panel.$instructionSelectScara[ this.Panel.StructuraData.getScariCount() > 1 ? 'show' : 'hide' ]();
    },
    
    displayConfirmation: function( spatiuData )
    {
        this.ConfirmFrame.show();
        this.ConfirmFrame.sendParameters( { spatiu: spatiuData } );
    },
    
    hideConfirmation: function()
    {
        this.ConfirmFrame.hide();
        this.SpatiiList.reset();
    },
    
    setScaraId: function( scaraId )
    {
        this.sendParameters( { scaraId: scaraId } );
    }
    
});

var SpatiiListConstructor = _Spatii.SpatiiPanelComponent.extend(
{
    _setup_: function()
    {
        this.Parent = this._options.Parent;
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.bind( 'click', '.spatiu-row', function(){
            var spatiuData, $row;
                    
            $row = $(this);
            spatiuData = $row.data( 'data' );
            
            self._selectRow( $row );
            self.Parent.displayConfirmation( spatiuData );
        });
    },
    
    _selectRow: function( $spatiuRow )
    {
        this._clearSelection();
        
        $spatiuRow.addClass( 'selected' );
    },
    
    _reset_: function()
    {
        this._clearSelection();
    },
    
    _clearSelection: function()
    {
        this.find( '.spatiu-row' ).removeClass( 'selected' );
    }
    
});

var ConfirmFrameConstructor = _Components.RequestForm.extend(
{
    action: 'remove-spatiu',
    
    _setup_: function()
    {
        this.Parent = this._options.Parent;
        this.Panel  = this._options.Panel;
        this.spatiuData = null;
    },
    
    _setupElements_: function()
    {
        this.$numar = this.find( '.spatiu-numar' );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.addHandler( 'success', function( event )
        {
            var responseData = event.getArguments()[ 0 ],
                numar = self.spatiuData.numar;
            
            System.info( 'Apartamentul {numar} a fost eliminat.'.assign({ numar: numar }) );

            DataResource.get( 'Structura' ).removeSpatiu( responseData );
            
            self.Panel._trigger( 'done', {
                returnToHomePanel: true
            });
        });
        
        this.bind( 'click', '.no-button', function( event )
        {
            self._goBack();
        });
    },
    
    _parameters_: function( params )
    {
        this._setSpatiuData( params.spatiu );
    },
    
    _show_: function()
    {
        //this.Panel.ScariListPanel.disableSelection( false );
    },
    
    _setSpatiuData: function( spatiuData )
    {
        if( !spatiuData ) return;
        
        this.spatiuData = spatiuData;
        
        this.getField( 'id' ).val( spatiuData.id );
        this.$numar.text( spatiuData.numar );
    },
    
    _goBack: function()
    {
        this.Parent.hideConfirmation();
    }
    
});

})();