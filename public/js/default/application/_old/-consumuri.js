(function(){

var AddForm = _Components.Form.extend(
{
    url: '/api/add-consum',

    _init_: function()
    {
        var self = this;

        this.$fields = this.find('input.consum');

        window.onbeforeunload = function( event ){
            if( count( self.getParams() ) )
            {
                var message = "Există consumuri care nu sunt salvate. Doriţi să le salvaţi sau renunţaţi la modificări?";
                event = event || window.event;
                // For IE and Firefox
                if ( event )
                {
                    event.returnValue = message;
                }
                
                // For Safari
                return message;
            }
        };
    },

    getParams: function()
    {
        var params = {};

        this.$fields.each(function(){
            var $field = $(this),
                currentValue = $.trim( $field.val() ),
                originalValue = $.trim( $field.data( 'value' ) );

            if ( currentValue !== originalValue )
            {
                var fieldName = $field.attr( 'name' );
                params[ fieldName ] = currentValue;
            }

        });

        return params;
    },

    _saveFieldValues: function()
    {
        this.$fields.each(function(){
            var $field = $(this),
                currentValue = $.trim( $field.val() );

            $field.data( 'value', currentValue );
            $field.val( currentValue );
        });
    },

    _beforeSubmit: function( params )
    {
        if( count( params ) == 0 )
        {
            return false;
        }
    },

    _success: function( response )
    {
        this._saveFieldValues();

        alert('Consumurile au fost salvate');
    },

    _error: function()
    {
        alert('Verificaţi valorile completate!');
    }


});

var Consumuri = _Components.Container.extend(
{
    _init_: function()
    {
        this.Form = new AddForm( this.getContainer() );

    }

});

////////////////////////////////////////////////////////////////////////////////

Modules.Consumuri = Consumuri;

})();
