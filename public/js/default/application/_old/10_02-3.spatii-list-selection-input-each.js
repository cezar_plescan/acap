(function(){
        
_Spatii.SpatiiListSelectionInputEach = _Spatii.SpatiiListSelectionInput.extend(
{
    _validateValue: function( value, spatiuId, $input )
    {
        var errorMessage = null;
    
        if( value.isBlank() )
        {
            
        }
        else
        {
            if( !Number.validateFloat( value ) )
            {
                errorMessage = 'valoarea completată nu reprezintă un număr';
            }
            else
            {
                value = value.toNumber();

                if( value < 0 )
                {
                    errorMessage = 'valoarea completată trebuie să fie un număr pozitiv';
                }
                else if( value >= 100000 )
                {
                    errorMessage = 'valoarea completată nu poate fi mai mare decât 100.000';
                }
                else
                {
                    // check digits
                    var digits = value.toString().split( '.' )[ 1 ];
                    
                    if( digits !== undefined && digits.length > 4 )
                    {
                        errorMessage = 'valoarea completată trebuie să aibă mai puțin de 4 zecimale';
                    }
                }
            }
        }
        
        return errorMessage;
    }

});

})();