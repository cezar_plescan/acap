(function(){
    
_Cheltuieli.SpatiiSelectionParametruDialog = _Cheltuieli.SpatiiSelectionPickDialog.extend(
{
    _setup_: function()
    {
        this.SpatiiScariConstructor = _Spatii.SpatiiScariSelectionPickParametru;
    },
    
    _parameters_: function( params )
    {
        this._setParametru( params.parametru );
    },
    
    _setParametru: function( parametruCode )
    {
        this.SpatiiScari.setParametru( parametruCode );
    }
    
});

})();