(function(){
    
_Cheltuieli.BenefIndividualPanel = _Cheltuieli.BenefDetailsPanel.extend(
{
    _setupResources_: function()
    {
        this.SelectSpatiiSection = new SelectSpatiiSectionConstructor( this.find( '>.spatii-section' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.SelectSpatiiSection.addHandler( 'noSelection', function( event )
        {
            self._dismiss();
        });
    },
    
    _reset_: function()
    {
        this.SelectSpatiiSection.reset();
    },
    
    _show_: function()
    {
        if( this._isSelectionEmpty() )
        {
            this._showSelectDialog();
        }
    },
    
    _isSelectionEmpty: function()
    {
        return !this.SelectSpatiiSection.hasSelection();
    },
    
    _showSelectDialog: function()
    {
        this.SelectSpatiiSection.openDialog();
    },
    
    _setSuma: function( suma )
    {
        this._super( suma );
        
        this.SelectSpatiiSection.sendParameters( { suma: suma } );
    }
    
});

//==============================================================================

var SelectSpatiiSectionConstructor = _Components.Container.extend(
{
    _setup_: function()
    {
        this.selection = null;
        this.suma = null;
    },
    
    _setupElements_: function()
    {
        this.$selectionSummary = this.find( '.selection-summary' );
        this.$editSelection = this.find( '.edit-spatii-selection-button' );
    },
    
    _setupResources_: function()
    {
        this.SpatiiDialog = new _Cheltuieli.SpatiiSelectionEachDialog( $( '#spatii-individuale-dialog' ), {
            height: 'auto'
        });
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$editSelection.on( 'click', function()
        {
            self._selectSpatii();
        });
        
        this.SpatiiDialog.addHandler( 'close', function( event, selection )
        {
            self._setSpatiiSelection( selection );
        });
    },
    
    _init_: function()
    {
        this._hideSummary();
        this._hideEditButton();
    },
    
    _reset_: function()
    {
        this.selection = null;
        this._hideSummary();
        this._hideEditButton();
    },
    
    _parameters_: function( params )
    {
        if( params.suma !== undefined )
        {
            this._setSuma( params.suma );
        }
    },
    
    getSpatii: function()
    {
        return this.selection;
    },
    
    hasSelection: function()
    {
        return Object.size( this.selection );
    },
    
    openDialog: function()
    {
        this._selectSpatii();
    },
    
    _setSuma: function( suma )
    {
        this.suma = suma;        
    },
    
    _selectSpatii: function()
    {
        this.SpatiiDialog.open({
            selection:      this.selection,
            suma:           this.suma
        });
    },
    
    _hideSummary: function()
    {
        this.$selectionSummary.hide();
    },
    
    _showSummary: function( selection )
    {
        this.$selectionSummary.show();
        this.$selectionSummary.text( this._generateSummaryText( selection ) );
    },
    
    _hideEditButton: function()
    {
        this.$editSelection.hide();
    },
    
    _showEditButton: function()
    {
        this.$editSelection.show();
    },
    
    _setSpatiiSelection: function( selection )
    {
        var oldValue = Object.clone( this.selection );
        
        if( Object.size( selection ) )
        {
            this._showSummary( selection );
            this._showEditButton();

            this.selection = selection;
        }
        else
        {
            if( Object.isEmpty( this.selection ) )
            {
                this._trigger( 'noSelection' );
            }
        }
        
        if( !Object.equal( oldValue, this.selection ) )
        {
            this._trigger( 'change' ); //?
        }
    },
    
    _generateSummaryText: function( selection )
    {
        var spatiiText,
            spatiiCount = Object.size( selection ),
            summaryText;

        spatiiText = String.pluralize( spatiiCount, 'apartament', 'apartamente' );

        summaryText = spatiiText;

        return summaryText;
    }
    
});

})();