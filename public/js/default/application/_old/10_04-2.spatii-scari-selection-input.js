(function(){

_Spatii.SpatiiScariSelectionInput = _Spatii.SpatiiScariSelection.extend(
{
    _setup_: function()
    {
        this.SpatiiPanelConstructorBase = _Spatii.SpatiiPanelSelectionInput;
    },
    
    _setupActions_:function()
    {
        var self = this;
        
        this.SpatiiPanel.addHandler( 'error', function()
        {
            self._trigger( 'error' );
        });

        this.SpatiiPanel.addHandler( 'errorOff', function()
        {
            self._trigger( 'errorOff' );
        });

    }
    
});

})();
