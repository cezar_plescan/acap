(function(){

_Spatii.SpatiiListSelectionInputPersoane = _Spatii.SpatiiListSelectionInput.extend(
{
    _validateValue: function( value, spatiuId, $input )
    {
        var errorMessage = null,
            StructuraData = DataResource.get( 'Structura' );
    
        if( value.isBlank() )
        {
            
        }
        else
        {
            var spatiuInfo = StructuraData.getSpatiuInfo( spatiuId );

            if( !Number.validateInteger( value ) )
            {
                errorMessage = 'valoarea completată nu reprezintă un număr';
            }
            else
            {
                value = value.toNumber();

                if( value < 0 )
                {
                    errorMessage = 'valoarea completată trebuie să fie un număr pozitiv';
                }
                else
                {
                    var persoaneSpatiu = spatiuInfo.nr_pers.toNumber();

                    if( value > persoaneSpatiu )
                    {
                        errorMessage = 'valoarea completată nu poate fi mai mare decât numărul curent de persoane';
                    }
                }
            }
        }
        
        return errorMessage;
    }
    
});

})();