(function(){

/**
 * Se afiseaza mesajul: "Aceasta suma trebuie sa includa si contravaloarea 
 * serviciului pentru canalizare".
 * 
 * Se afiseaza un link "vezi/modifica modul de calcul - repartizare diferenta s.a."
 * (se va deschide un dialog)
 * 
 */
_Cheltuieli.ParametriColoanaApa = _Cheltuieli.ParametriColoanaAbstract.extend(
{
    _setup_: function()
    {
        
    },
    
    _setupResources_: function()
    {
        this.ScariSection       = new ScariSectionConstructor( this.find( '>.scari-section' ) );
        this.CantitatiSection   = new CantitatiSectionConstructor( this.find( '>.cantitati-section' ) );
        
        var self = this;
        
        this.StructuraData = DataResource.get( 'Structura' );
        this.ScariList = new ScariListConstructor( this.$scariSection.find( '.scari-list' ) );
        
        this.StructuraData.registerObserver({
            addScara: function()
            {
                self._checkIfShowScariSection();
            },
            removeScara: function()
            {
                self._checkIfShowScariSection();
            },
            addBloc: function()
            {
                self._checkIfShowScariSection();
            },
            removeBloc: function()
            {
                self._checkIfShowScariSection();
            }
        });
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.bind( 'change', 'input[name=_as]', function()
        {
            var choice = $(this).val();
            
            if( choice == 'scari' )
            {
                self._enableScariList();
            }
            else
            {
                self._disableScariList();
            }
        });
        
    },
    
    _init_: function()
    {
        this._checkIfShowScariSection();
    },
    
    _checkIfShowScariSection: function()
    {
        // daca exista doar o singura scara:
        // - se selecteaza automat varianta "toate scarile"
        // - nu se afiseaza sectiunea 'scari'
        if( this.StructuraData.getScariCount() == 1 )
        {
            this.$scariSection.hide();
            this.$scariSection.find( 'input[name=_as][value=all]' ).prop( 'checked', true );
        }
        else
        {
            this.$scariSection.show();
            this.$scariSection.find( 'input[name=_as]' ).prop( 'checked', false );
            this._disableScariList();
        }
        
    },
    
    _disableScariList: function()
    {
        this.ScariList.hide();
        this.ScariList.clearSelection();
    },
    
    _enableScariList: function()
    {
        this.ScariList.show();
        
        // scroll
        this._triggerDOM( 'scrollToMe', this.ScariList.getContainer() );
    }

});

//==============================================================================

var ScariSectionConstructor = _Cheltuieli.ValuesSection.extend(
{
    
});

//==============================================================================


var ScariListConstructor = _Spatii.ScariListComponent.extend(
{
    _setup_: function()
    {
        
    },
    
    _init_: function()
    {
        this._checkIfOnlyOneBloc();
    },
    
    clearSelection: function()
    {
        this.find( 'input[name=_scari_apa]' ).prop( 'checked', false );
    },
    
    _checkIfOnlyOneBloc: function()
    {
        var $blocLabel = this._getBlocItems().find( '>label' );
                
        // daca exista doar un singur bloc:
        // - nu se afiseaza denumirea acestuia
        if( this.StructuraData.getBlocuriCount() == 1 )
        {
            $blocLabel.hide();
        }
        else
        {
            $blocLabel.show();
        }
    },
    
    _addBloc: function()
    {
        this._super.apply( this, arguments );
        
        this._checkIfOnlyOneBloc();
    },
    
    _removeBloc: function()
    {
        this._super.apply( this, arguments );
        
        this._checkIfOnlyOneBloc();
    }
    
});

})();