(function(){
    
_Cheltuieli.ReducerePersoaneDialog = _Cheltuieli._InputSpatiiDialog.extend(
{
    _setup_: function()
    {
        this._options.headerVisible = true;
        
        this.outputSelection = null;
    },
    
    _setupElements_: function()
    {
        this.$doneButton = this.find( '.done-button' );
        this.$cancelButton = this.find( '.cancel-button' );
    },
    
    _setupResources_: function()
    {
        this.ScariList = new ScariListConstructor( this.find( '.scari-list-component' ) );
        this.SpatiiList = new SpatiiListConstructor( this.find( '.spatii-list-component' ), {
            ScariLink:  this.ScariList
        });
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$doneButton.on( 'click', function()
        {
            var selection = self.SpatiiList.getSelection();
            
            if( Object.size( selection ) )
            {
                self.outputSelection = selection;

                self.close();
            }
            else
            {
                self.outputSelection = null;
                self.close();
            }
        });
        
        this.$cancelButton.on( 'click', function()
        {
            self.close();
        });
        
        this.SpatiiList.addHandler( 'error', function()
        {
            self._disableDoneButton();
        });
        
        this.SpatiiList.addHandler( 'errorOff', function()
        {
            self._enableDoneButton();
        });
        
        this.SpatiiList.addHandler( 'change', function( event )
        {
            var persoane = event.getArguments()[ 0 ];
            
            self._setPersoane( persoane );
        });
        
        this.addHandler( 'close', function()
        {
            self.SpatiiList.hangup();
        });
    },
    
    _init_: function()
    {
        this.initialTitle = this._getTitle();
    },
    
    _reset_: function()
    {
        this.outputSelection = null;
    },
    
    open: function( options )
    {
        this._super();
        
        this._generateContent( options.spatii );
        this._populateInput( options.reducere );
        
        this._enableDoneButton();
        
        this._updateTitle();
    },

    getSelection: function()
    {
        return this.outputSelection;
    },
            
    _generateContent: function( spatii )
    {
        var scari = this._getScariOfSpatii( spatii );
        
        this.reset();
        
        this.ScariList.redraw( scari );
        this.SpatiiList.redraw( spatii );
    },
    
    _populateInput: function( values )
    {
        this.SpatiiList.populateInput( values );
    },
    
    _getScariOfSpatii: function( spatii )
    {
        var scari = [],
            StructuraData = DataResource.get( 'Structura' );
        
        if( spatii === '*' )
        {
            scari = '*';
        }
        else
        {
            spatii.each( function( spatiuId )
            {
                var spatiuInfo = StructuraData.getSpatiuInfo( spatiuId ),
                    scaraId = spatiuInfo.scara_id;

                if( scari.indexOf( scaraId ) < 0 )
                {
                    scari.push( scaraId );
                }
            });
        }
        
        return scari;
    },
    
    _disableDoneButton: function()
    {
        this.$doneButton.prop( 'disabled', true );
    },
    
    _enableDoneButton: function()
    {
        this.$doneButton.prop( 'disabled', false );
    },
    
    _setPersoane: function( persoane )
    {
        var title, persoaneText;
                
        if( persoane.toNumber() )
        {
            title = '{persoane} beneficiază de tarif redus';
            
            persoaneText = String.pluralize( persoane, 'persoană', 'persoane' );
            
            title = title.assign( { persoane: persoaneText } );
        }
        else
        {
            title = this.initialTitle;
        }
        
        this.setTitle( title );
    },
    
    _updateTitle: function()
    {
        var persoane = this.SpatiiList.getPersoaneCount();
        
        this._setPersoane( persoane );
    }
    
});

//==============================================================================

var SpatiiListConstructor = _Spatii.SpatiiPanelComponent.extend(
{
//    _setup_: function()
//    {
//        this.PaneListConstructor = SpatiiPaneListConstructor;
//        
//        this.errorsCount = 0;
//        
//        this.persoaneCount = {};
//    },
//    
//    _setupActions_:function()
//    {
//        var self = this;
//        
//        this.addHandler( 'loadPane', function( event )
//        {
//            var Pane = event.getArguments()[ 1 ],
//                paneId = event.getArguments()[ 0 ];
//            
//            Pane.addHandler( 'error', function()
//            {
//                self.errorsCount ++;
//                
//                if( self.errorsCount == 1 )
//                {
//                    self._trigger( 'error' );
//                }
//            });
//            
//            Pane.addHandler( 'errorOff', function()
//            {
//                self.errorsCount --;
//                
//                if( self.errorsCount == 0 )
//                {
//                    self._trigger( 'errorOff' );
//                }
//            });
//            
//            Pane.addHandler( 'change', function( event )
//            {
//                var persoane = event.getArguments()[ 0 ];
//                
//                self._updatePersoaneCount( paneId, persoane );
//            });
//        });
//    },
    
//    redraw: function( spatii )
//    {
//        var self = this;
//        
//        this._loadAllPanes();
//        
//        Object.each( this.Panes, function( paneId, Pane )
//        {
//            Pane.showSpatii( spatii );
//            
//            self.ScariLink.setTotalPersoane( paneId, Pane.getTotalPersoane() );
//        });
//        
//        this.reset();
//    },
//    
//    getSelection: function()
//    {
//        var selection = {};
//        
//        Object.each( this.Panes, function( paneId, Pane )
//        {
//            Object.extend( selection, Pane.getSelection() );
//        });
//        
//        return selection;
//    },
//    
//    populateInput: function( values )
//    {
//        var self = this;
//        
//        this.persoaneCount = {};
//        
//        Object.each( this.Panes, function( paneId, Pane )
//        {
//            Pane.populateInput( values );
//            
//            self._updatePersoaneCount( paneId, Pane.getPersoane() );
//        });
//        
//    },
//    
//    getPersoaneCount: function()
//    {
//        return Object.sum( this.persoaneCount );
//    },
//    
//    _hangup_: function()
//    {
//        Object.each( this.Panes, function( paneId, Pane )
//        {
//            Pane.hangup();
//        });
//        
//        this.errorsCount = 0;
//    },
//    
//    _updatePersoaneCount: function( scaraId, persoane )
//    {
//        var currentPersoane = this.persoaneCount[ scaraId ];
//                
//        if( currentPersoane != persoane )
//        {
//            this.persoaneCount[ scaraId ] = persoane;
//            
//            this.ScariLink.setPersoaneCount( scaraId, persoane );
//            
//            this._trigger( 'change', this.getPersoaneCount() );
//        }
//    }
    
});

//------------------------------------------------------------------------------

var SpatiiPaneListConstructor = _Spatii.SpatiiPaneList.extend(
{
    _setup_: function()
    {
//        this.errorsCount = 0;
//        this.totalPersoane = 0;
//        this.persoane = 0;
    },
    
    _setupActions_: function()
    {
//        var self = this,
//            timeout;
//        
//        this.bind( 'keyup paste change', 'input[name=suma_spatiu]', function( event )
//        {
//            var $input = $(this);
//            
//            var checkInput = function()
//            {
//                var value = $input.val().trim(),
//                    oldValue = $input.data( 'old-value' ),
//                    triggerErrorEvent = false,
//                    triggerErrorOffEvent = false,
//                    deltaValue,
//                    numericValue;
//                    
//                if( value != oldValue )
//                {
//                    var $spatiuRow = $input.closest( '.spatiu-row' ),
//                        spatiuId = $spatiuRow.data( 'id' );
//
//                    if( self._isValueValid( value, spatiuId, $input ) )
//                    {
//                        var hadError = $input.hasClass( 'has-error' );
//                                
//                        if( hadError )
//                        {
//                            $input.removeClass( 'has-error' );
//                            
//                            self.errorsCount --;
//                            
//                            triggerErrorOffEvent = ( self.errorsCount == 0 );
//                        }
//                        
//                        numericValue = value.toNumber() || 0;
//                        deltaValue = numericValue - ( $input.data( 'last-value' ) || 0 );
//                        $input.data( 'last-value', numericValue );
//                        
//                        self._triggerChange( deltaValue );
//                    }
//                    else
//                    {
//                        hadError = $input.hasClass( 'has-error' );
//                        
//                        if( !hadError )
//                        {
//                            $input.addClass( 'has-error' );
//                            
//                            triggerErrorEvent = ( self.errorsCount == 0 );
//                            
//                            self.errorsCount ++;
//                            
//                            deltaValue = - ( $input.data( 'last-value' ) || 0 );
//                            $input.data( 'last-value', 0 );
//
//                            self._triggerChange( deltaValue );
//                        }
//                    }
//                    
//                    $input.data( 'old-value', value );
//                    
//                    if( triggerErrorEvent )
//                    {
//                        self._trigger( 'error', spatiuId );
//                    }
//                    else if( triggerErrorOffEvent )
//                    {
//                        self._trigger( 'errorOff' );
//                    }
//                }
//            };
//            
//            clearTimeout( timeout );
//            
//            if( event.type == 'change' )
//            {
//                checkInput();
//            }
//            else
//            {
//                timeout = setTimeout( checkInput, 300 );
//            }
//        });
    },
    
//    _init_: function()
//    {
//        
//    },
//    
//    showSpatii: function( spatii )
//    {
//        var self = this,
//            $rows = this._getSpatiuRows(),
//            spatiiCounter = 0;
//        
//        this.totalPersoane = 0;
//        
//        $rows.each( function()
//        {
//            var $row = $(this),
//                show = true,
//                nrPers = $row.data( 'data' ).nr_pers;
//            
//            if( spatii === '*' )
//            {
//                if( nrPers === undefined )
//                {
//                    show = false;
//                }
//            }
//            else
//            {
//                var spatiuId = $row.data( 'id' );
//                
//                if( spatii.indexOf( spatiuId ) < 0 )
//                {
//                    show = false;
//                }
//            }
//
//            if( show )
//            {
//                $row.show();
//                spatiiCounter ++;
//                
//                self.totalPersoane += ( nrPers.toNumber() || 0 );
//            }
//            else
//            {
//                $row.hide();
//            }
//        });
//        
//        this._getEtajContainers().each( function()
//        {
//            var $visibleRows,
//                $etajContainer = $(this);
//                
//            $visibleRows = self._getSpatiuRowsInsideContainer( $etajContainer ).filter( function()
//            {
//                return $(this).css( 'display' ) !== 'none';
//            });
//            
//            if( $visibleRows.length )
//            {
//                $etajContainer.show();
//            }
//            else
//            {
//                $etajContainer.hide();
//            }
//        });
//        
//        return spatiiCounter;
//    },
//    
//    getTotalPersoane: function()
//    {
//        return this.totalPersoane;
//    },
//    
//    getPersoane: function()
//    {
//        return this.persoane;
//    },
//    
//    getSelection: function()
//    {
//        var selection = {};
//        
//        this.getRows().each( function()
//        {
//            var $row = $(this),
//                spatiuId = $row.data( 'id' ),
//                $input = $row.find( 'input[name=suma_spatiu]' ),
//                value = $input.val().trim();
//                
//            if( value.length && value != '0')
//            {
//                selection[ spatiuId ] = value;
//            }
//        });
//        
//        return selection;
//    },
//    
//    populateInput: function( values )
//    {
//        var self = this,
//            $rows = this.getRows();
//        
//        this.persoane = 0;
//        
//        $rows.find( 'input[name=suma_spatiu]' )
//                .val( '' )
//                .removeData( 'last-value' );
//        
//        if( Object.size( values ) )
//        {
//            $rows.each( function()
//            {
//                var $spatiuRow = $(this),
//                    spatiuId = $spatiuRow.data( 'id' ),
//                    $input = $spatiuRow.find( 'input[name=suma_spatiu]' ),
//                    inputValue;
//
//                if( values[ spatiuId ] !== undefined )
//                {
//                    inputValue = values[ spatiuId ];
//                    $input.val( inputValue );
//
//                    self.persoane += ( inputValue.toNumber() || 0 );
//                }
//            });
//        }
//    },
//    
//    _hangup_: function()
//    {
//        if( this._hasErrors() )
//        {
//            System.info().close();
//            
//            this.reset();
//        }
//    },
//    
//    _reset_: function()
//    {
//        this.errorsCount = 0;
//        
//        this.getRows().each( function()
//        {
//            $(this).find( 'input[name=suma_spatiu]').removeClass( 'has-error' );
//        });
//    },
//    
//    _hasErrors: function()
//    {
//        return Object.toBoolean( this.errorsCount );
//    },
//    
//    _triggerChange: function( delta )
//    {
//        if( delta )
//        {
//            this.persoane += delta;
//            
//            this._trigger( 'change', this.persoane );
//        }
//    }
    
});

//==============================================================================

var ScariListConstructor = _Spatii.ScariListPanel.extend(
{
//    _setup_: function()
//    {
//        this.dataProvider = [];
//    },
//    
//    _setupActions_:function()
//    {
//        
//    },
//    
//    _init_: function()
//    {
//        
//    },
//    
    redraw: function( scari ) // - e posibil sa fie utila
    {
        var self = this,
            scariCounter = 0,
            $scaraItems = this._getScaraItems(),
            $blocItems = this._getBlocItems();
        
        this.reset();
        
        if( scari === '*' )
        {
            $blocItems
                    .show()
                    .data( 'used', true );
            
            $scaraItems
                    .show()
                    .data( 'used', true );
            
            scariCounter = this._getScaraItems().length;
        }
        else
        {
            $scaraItems.each( function()
            {
                var $scaraItem = $(this),
                    scaraId = $scaraItem.data( 'id' );
                
                if( scari.indexOf( scaraId ) >= 0 )
                {
                    $scaraItem
                            .show()
                            .data( 'used', true );
                    
                    scariCounter ++;
                }
                else
                {
                    $scaraItem
                            .hide()
                            .data( 'used', false );
                }
            });
            
            $blocItems.each( function()
            {
                var $blocItem = $(this),
                    $scariItems = self._getScaraItems( $blocItem ),
                    allUnused = true;
                
                $scariItems.each( function()
                {
                    if( $(this).data( 'used' ) )
                    {
                        allUnused = false;
                        
                        return false;
                    }
                });
                
                if( allUnused )
                {
                    $blocItem
                            .hide()
                            .data( 'used', false );
                }
                else
                {
                    $blocItem
                            .show()
                            .data( 'used', true );
                }
            });
        }
        
        if( scariCounter == 1 )
        {
            var $scaraItem, scaraId;
                    
            $scaraItem = $scaraItems.filter( function()
            {
                return $(this).data( 'used' );
            });
            
            scaraId = $scaraItem.data( 'id' );
            
            this.setCurrentScara( scaraId );
            this._trigger( 'selectScara', scaraId );
        }
    },
    
//    setTotalPersoane: function( scaraId, total )
//    {
//        var $scaraItem = this._getScaraItem( scaraId ),
//            $totalPersoane = $scaraItem.find( '.total-persoane' );
//    
//        $totalPersoane.text( total );
//    },
//    
//    setPersoaneCount: function( scaraId, persoane )
//    {
//        var $scaraItem = this._getScaraItem( scaraId ),
//            $persoaneCount = $scaraItem.find( '.persoane-count' );
//    
//        $persoaneCount.text( persoane );
//        
//        if( persoane.toString().trim() === '0' )
//        {
//            $persoaneCount.addClass( 'empty' );
//        }
//        else
//        {
//            $persoaneCount.removeClass( 'empty' );
//        }
//    }
    
});


})();