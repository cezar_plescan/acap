(function(){

_Spatii.AddSpatiuPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.scaraInfo = null;
        this.changeScara = true;
    },
    
    _setupElements_: function()
    {
        this.$instructionSelectScara = this.find( '.instructions .select-scara' );
    },
    
    _setupResources_: function()
    {
        var self = this;
        
        this.Form = new FormConstructor( this.find( 'form.spatiu-form' ), {
            Panel: self
        });
        
        this.List = new ListConstructor( this.find( '.spatii-list-component' ) );
        
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ) );
        
        this.RemoveScara = new RemoveScaraSectionConstructor( this.find( '.remove-scara-section' ), {
            DisplayManager: this.DisplayManager
        });
        
    },
        
    _setupActions_: function()
    {
        var self = this;
        
        this.Module.ScariListPanel.addHandler( 'selectScara', function( event )
        {
            if( self.isVisible() )
            {
                var selectedScaraId = event.getArguments()[ 0 ];
                
                self._setScaraId( selectedScaraId );
                self._displayPanel();
            }
        });
    },
    
    _parameters_: function( params )
    {
        this._setScaraInfo( params.scaraInfo );
    },
    
    _show_: function()
    {
        if( this.changeScara )
        {
            this.ScariListPanel.setSelectMode( 'scara' );
            this.RemoveScara.hide();
            this.$instructionSelectScara[ this.StructuraData.getScariCount() > 1 ? 'show' : 'hide' ]();
        }
        else
        {
            this.ScariListPanel.disableSelection( false ); // keep the selected item highlighted
            this.RemoveScara.show();
            this.$instructionSelectScara.hide();
        }

        if( !this.scaraInfo )
        {
            // get scaraId from ScariListPanel
            var scaraId;
            
            scaraId = this.ScariListPanel.getSelectedScara();
            
            if( scaraId )
            {
                this._setScaraId( scaraId );
            }
        }
        
        if( !this.scaraInfo )
        {
            this._displaySelectScaraMessage();
        }
        else
        {
            this._displayPanel();
        }
    },
    
    _setScaraId: function( scaraId )
    {
        var scaraInfo;
        
        scaraInfo = this.StructuraData.getScaraInfo( scaraId );
        
        this._setScaraInfo( scaraInfo );
    },
    
    _setScaraInfo: function( scaraInfo )
    {
        if( !Object.isObject( scaraInfo ) )
        {
            this.scaraInfo = null;
        }
        else
        {
            this.scaraInfo = scaraInfo;

            this.Form.getField( 'scara_id' ).val( scaraInfo.id );
            this.Form.clear();
            
            this.List.setScaraId( scaraInfo.id ); 
        }
        
        this.RemoveScara.setScaraInfo( scaraInfo );
    },
    
    _displaySelectScaraMessage: function()
    {
        this.Panes.showPane( 'select-scara' );
    },
    
    _displayPanel: function()
    {
        this.Panes.showPane( 'panel' );
        this.Form.getField( 'etaj' ).focus();
    },
    
    _disableQuit: function()
    {
        this._super();
        
        this.changeScara = false;
    },
    
    _enableQuit: function()
    {
        this._super();
        
        this.changeScara = true;
    },
    
    enableQuit: function()
    {
        if( !this.canQuit )
        {
            this._enableQuit();
        }
    },
    
    hideRemoveScaraSection: function()
    {
        this.RemoveScara.hide();
    }
    
});

var FormConstructor = _Spatii._SpatiuForm.extend(
{
    action: 'add-spatiu',
    
    _setupActions_: function()
    {
        var self = this;
        
        this.addHandler( 'beforeSubmit', function( event )
        {
            var formData = event.getArguments()[ 0 ],
                errors = [];

            Object.each( formData, function( fieldName, fieldValue ){
                if( fieldValue.isBlank() && self.errorMessages[ fieldName ] !== undefined )
                {
                    errors.push( self.errorMessages[ fieldName ] );
                }
            });

            if( errors.length )
            {
                this._showErrors( errors );
                event.abort();
            }
        });
        
        this.addHandler( 'success', function( event )
        {
            var responseData, continueAdding;
            
            responseData = event.getArguments()[ 0 ];
            continueAdding = self._checkIfContinue();
            
            self.clear();
            
            System.info( 'Apartamentul cu numărul {numar} a fost adăugat la scara {scara} din blocul {bloc}.'.assign({
                numar:  responseData.numar,
                scara:  self.Panel.scaraInfo.denumire,
                bloc:   self.Panel.scaraInfo.denumire_bloc,
            }) );

            self._updateResourceData( responseData );

            self.Panel.enableQuit();
            self.Panel.hideRemoveScaraSection();
                
            if( continueAdding )
            {
                self._autocompleteForm( responseData );
            }
                
            self.Panel._trigger( 'done', {
                returnToHomePanel:  !continueAdding,
                scaraId:            responseData.scara_id
            });
        });
    },
    
    _checkIfContinue: function()
    {
        var _continue = this.find( 'input.continue' ).prop( 'checked' );
        
        return _continue;
    },
    
    _updateResourceData: function( spatiuData )
    {
        this.Panel.StructuraData.addSpatiu( spatiuData );
    },
    
    _autocompleteForm: function( spatiuData )
    {
        this.getField( 'etaj' ).val( spatiuData.etaj );
        
        if( spatiuData.numar - 0 == spatiuData.numar )
        {
            this.getField( 'numar' ).val( spatiuData.numar.toNumber() + 1 );
            this.getField( 'proprietar' ).focus();
        }
        else
        {
            this.getField( 'numar' ).focus();
        }
    }
    
});

/*
 * @parameter scaraId
 */
var ListConstructor = _Spatii.SpatiiPanelComponent.extend(
{
    
});

var RemoveScaraSectionConstructor = _Components.Container.extend(
{
    _setup_: function()
    {
        if( !this._options.DisplayManager )
        {
            throw new Error( '"DisplayManager" option is not defined.' );
        }
        
        this.DisplayManager = this._options.DisplayManager;
        this.scaraInfo = null;
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.bind( 'click', '.remove-scara-button', function(){
            self._removeScara();
        });
    },
    
    _parameters_: function( params )
    {
        this.setScaraInfo( params.scaraInfo );
    },
    
    setScaraInfo: function( info )
    {
        this.scaraInfo = info;
    },
    
    _removeScara: function()
    {
        if( !this.scaraInfo )
        {
            throw new Error( '"scaraInfo" is not set.' );
        }
        
        this.DisplayManager.display( _Spatii.ContentPanelCodes.REMOVE_SCARA, { 
            scaraInfo:   this.scaraInfo,
            changeScara: false
        }, {
            force: true
        });
    }
});


})();