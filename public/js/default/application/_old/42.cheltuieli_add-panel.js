(function(){
    
_Cheltuieli.AddCheltuiala = _Components.Container.extend(
{
    _setup_: function()
    {
        if( !this._options.Module )
        {
            throw new Error( '"Module" option is not set.' );
        }
        
        this.Module = this._options.Module;
    },
    
    _setupElements_: function()
    {
        this.$sections = this.getContainer().children().filter( '.section' );
    },
    
    _setupResources_: function()
    {
        var sectionArguments = {
            Page: this
        };
        
        this.MainFormSection = new _Cheltuieli.MainFormSection( this._getSectionElement( 'main-form' ), sectionArguments );
        this.DistribuireSection = new _Cheltuieli.DistribuireSection( this._getSectionElement( 'distribuire' ), sectionArguments );
        this.FacturaSection = new _Cheltuieli.FacturaSection( this._getSectionElement( 'factura' ), sectionArguments );
        this.InventarSection = new _Cheltuieli.InventarSection( this._getSectionElement( 'inventar' ), sectionArguments );
        
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.MainFormSection.addHandler( 'completed', function()
        {
            self.DistribuireSection.enable();
        });
        
        this.DistribuireSection.addHandler( 'completed', function()
        {
            self.FacturaSection.enable();
        });
        
        this.FacturaSection.addHandler( 'completed', function()
        {
            self.InventarSection.enable();
        });
        
        this.InventarSection.addHandler( 'completed', function()
        {
            self._enableSubmit();
        });
        
        
    },
    
    _init_: function()
    {
        //this.DistribuireSection.disable();
        this.FacturaSection.disable();
    },
    
    getSuma: function()
    {
        return this.MainFormSection.getSuma();
    },
    
    _getSectionElement: function( sectionName )
    {
        var $section = this.$sections.filter( '.' + sectionName + '-section' );
        
        return $section;
    },
    
    _enableSubmit: function()
    {
        //.....
    }
    
});

//==============================================================================

_Cheltuieli.SectionAbstract = _Components.Container.extend(
{
    _setup_: function()
    {
        this.completed = false;
        this.Page = this._options.Page;
    },
    
    _triggerCompleted: function()
    {
        this.completed = true;
        this._trigger( 'completed' );
    },
    
    _isCompleted: function()
    {
        return true;
    }
    
    
});

})();