(function(){

_Cheltuieli.SpatiiSelectionPersoaneDialog = _Cheltuieli.SpatiiSelectionPickDialog.extend(
{
    _setup_: function()
    {
        this.SpatiiScariConstructor = _Spatii.SpatiiScariSelectionPickPersoane;
    },
    
    _getSelectionTitle: function()
    {
        var title = this._super(),
            persoane = Object.sum( this.getSelection() );

        title += ', ' + String.pluralize( persoane, 'persoană', 'persoane' );
        
        return title;
    }
    
});

})();