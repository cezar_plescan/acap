(function(){
    
_Cheltuieli.ListCheltuieliNeachitate = _Components.Container.extend(
{
    _setup_: function()
    {
        if( !this._options.Module )
        {
            throw new Error( '"Module" option is not set.' );
        }
        
        this.Module = this._options.Module;
    },
    
    _setupResources_: function()
    {
        
    },
    
    _setupActions_: function()
    {
        
    },
    
    _init_: function()
    {
        
    }
    
});

})();