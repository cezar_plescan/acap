(function(){

_Spatii.ContentPanelCodes = {
    ADD_BLOC:       'add-bloc',
    ADD_SCARA:      'add-scara',
    ADD_SPATIU:     'add-spatiu',
    VIEW_LIST:      'view-list',
    REMOVE_BLOC:    'remove-bloc',
    REMOVE_SCARA:   'remove-scara',
    REMOVE_SPATIU:  'remove-spatiu',
    EDIT_BLOC:      'edit-bloc',
    EDIT_SCARA:     'edit-scara',
    EDIT_SPATIU:    'edit-spatiu'
};

_Spatii.ContentPanel = _Components.Container.extend(
{
    _setup_: function()
    {
        if( !this._options.Module )
        {
            throw new Error( '"Module" option is not set.' );
        }
    },
    
    _setupResources_: function()
    {
        this.StructuraData  = DataResource.get( 'Structura' );
        this.Module         = this._options.Module;
        
        this._setupPanes();
    },
    
    _init_: function()
    {
        this._refreshModulePage();
    },
    
    display: function( panelId, panelArguments, options )
    {
        var defaultPanelConfig;
        
        options = Object.makeObject( options );
        
        if( options.force )
        {
            
        }
        else
        {
            defaultPanelConfig = this._getDefaultPanel();

            if( defaultPanelConfig )
            {
                panelId         = defaultPanelConfig.id;
                panelArguments  = defaultPanelConfig.args;

                this._trigger( 'freeze' );
            }
            else
            {
                if( !this._getCurrentPanelId() )
                {
                    var panelConfig;

                    panelConfig = this._getHomePanelConfig();

                    panelId         = panelConfig.id;
                    panelArguments  = panelConfig.args;
                }

                this._trigger( 'unfreeze' );
            }
        }
        
        if( panelId )
        {
            this._display( panelId, panelArguments );
        }
    },
    
    _display: function( panelId, panelArguments )
    {
        this.Panes.showPane( panelId, panelArguments );
    },
    
    _getCurrentPanelId: function()
    {
        return this.Panes.getCurrentPaneId();
    },
    
    _getHomePanelConfig: function()
    {
        var panelConfig;
        
        panelConfig = {
            id:     _Spatii.ContentPanelCodes.VIEW_LIST,
            args:   { scaraId: this.Module.ScariListPanel.getSelectedScara() }
        };

        return panelConfig;
    },
    
    _setupPanes: function()
    {
        var self = this;
        
        var panelConstructorOptions = {
            DisplayManager: self,
            Module: self.Module,
            onDone: function( event )
            {
                var options = event.getArguments()[ 0 ];
                
                self._refreshModulePage( options );
            },
            onQuit: function( event )
            {
                self.displayHomePanel();
            }
        };
        
        var paneConstructors = {};
        
        Object.each( _Spatii.ContentPanelCodes, function( key, value ){
            // check if panel class is defined
            var panelClassName = value.camelize() + 'Panel';
            if( _Components.Class.isExtendedFrom( _Spatii[ panelClassName ], _Spatii.AbstractContentPanel ) )
            {
                paneConstructors[ value ] = [ _Spatii[ panelClassName ], panelConstructorOptions ];
            }
        });
        
        this.Panes = new _Components.Panes( this.find( '>.content-panes-container' ), {
            defaultPane:        false,
            paneConstructors:   paneConstructors
        });
    },
    
    /**
     * 
     * @param returnToHomePanel
     */
    _refreshModulePage: function( options )
    {
        options = Object.makeObject( options );
        
        if( options.returnToHomePanel )
        {
            this.displayHomePanel();
        }
        else
        {
            this.display();
        }
    },
    
    displayHomePanel: function()
    {
        var homePanelConfig;
        
        homePanelConfig = this._getHomePanelConfig();
        
        this.display( homePanelConfig.id, homePanelConfig.args );
    },
    
    _getDefaultPanel: function()
    {
        var StructuraResource = this.StructuraData,
            panelId, 
            panelArguments = {};
        
        if( this._hasBlocuri() )
        {
            var blocId = this._getEmptyBlocId();
            if( blocId )
            {
                panelId = _Spatii.ContentPanelCodes.ADD_SCARA;
                
                this.Module.ScariListPanel.setCurrentBloc( blocId );
                
                Object.extend( panelArguments, {
                    blocInfo:       StructuraResource.getBlocInfo( blocId ),
                    disableQuit:    true
                });
            }
            else
            {
                var scaraId = this._getEmptyScaraId();
                if( scaraId )
                {
                    panelId = _Spatii.ContentPanelCodes.ADD_SPATIU;
                    
                    this.Module.ScariListPanel.setCurrentScara( scaraId );
                    
                    Object.extend( panelArguments, {
                        scaraInfo:      StructuraResource.getScaraInfo( scaraId ),
                        disableQuit:    true
                    });
                }
            }
        }
        else
        {
            panelId = _Spatii.ContentPanelCodes.ADD_BLOC;
            
            panelArguments.disableQuit = true;
        }
        
        var config = panelId ?
            {
                id:     panelId,
                args:   panelArguments
            } :
            null;
    
        return config;
    },
    
    _hasBlocuri: function()
    {
        var has = !this.StructuraData.isEmpty();
        
        return has;
    },
    
    _getEmptyBlocId: function()
    {
        var id = null;
        
        this.StructuraData.getBlocuri().each(function( blocData ){
            if( blocData.scari.isEmpty() )
            {
                id = blocData.id;
                return false;
            }
        });
        
        return id;
    },
    
    _getEmptyScaraId: function()
    {
        var id = null;
        
        this.StructuraData.getBlocuri().each(function( blocData ){
            blocData.scari.each(function( scaraData ){
                if( scaraData.spatii.isEmpty() )
                {
                    id = scaraData.id;
                    return false;
                }
            });
            
            if( id )
            {
                return false;
            }
        });
        
        return id;
    }

});

_Spatii.AbstractContentPanel = _Components.Container.extend(
{
    _setup_: function()
    {
        if( !this._options.Module )
        {
            throw new Error( '"Module" option is not defined.' );
        }
        
        if( !this._options.DisplayManager )
        {
            throw new Error( '"DisplayManager" option is not defined.' );
        }
        
        this.Module         = this._options.Module;
        this.ScariListPanel = this.Module.ScariListPanel;
        this.DisplayManager = this._options.DisplayManager;
        
        this.canQuit = true;
    },
    
    _setupElements_: function()
    {
        this.$cancel = this.find( '.cancel-button' );
    },
    
    _setupResources_: function()
    {
        this.StructuraData  = DataResource.get( 'Structura' );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$cancel.on( 'click', function(){
            self._quit();
        });
    },
    
    _parameters_: function( params )
    {
        if( params.disableQuit )
        {
            this._disableQuit();
        }
        else
        {
            this._enableQuit();
        }
    },

    _quit: function()
    {
        if( this.canQuit )
        {
            this._trigger( 'quit' );
        }
    },
    
    _disableQuit: function()
    {
        this.canQuit = false;
        this.$cancel.hide();
    },
    
    _enableQuit: function()
    {
        this.canQuit = true;
        this.$cancel.show();
    },
    
    _getSelectedScara: function()
    {
        var sId = this.Module.ScariListPanel.getSelectedScara();
        
        return sId;
    }
    
});

})();