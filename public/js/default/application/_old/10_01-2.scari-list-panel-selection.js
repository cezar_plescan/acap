(function(){

_Spatii.ScariListPanelSelection = _Spatii.ScariListPanelComponent.extend(
{
    _setup_: function()
    {
        
    },
    
    _setupActions_:function()
    {
        // actualizare numar apartamente dupa stergere sau adaugare ....
    },
    
    _init_: function()
    {
        this._populateSpatiiCounters();
    },
    
    _populateSpatiiCounters: function()
    {
        var self = this;
        
        this._getScaraItems().each( function()
        {
            var $scaraItem = $(this),
                $totalCounter = $scaraItem.find( '.total-spatii-counter' ),
                $selectedSpatiiCounter = $scaraItem.find( '.selected-spatii-counter' );
            
            $selectedSpatiiCounter.text( '0' );
            $selectedSpatiiCounter.addClass( 'empty' );
            $totalCounter.text( '0' );
        });
    },
    
    setCounter: function( scaraId, counter )
    {
        var $scaraItem = this._getScaraItem( scaraId ),
            $counter = $scaraItem.find( '.selected-spatii-counter' );
        
        $counter.text( counter );
        
        counter ?
            $counter.removeClass( 'empty' ) :
            $counter.addClass( 'empty' );
    },

    setTotals: function( counters )
    {
        var self = this;
        
        Object.each( counters, function( scaraId, total )
        {
            if( total )
            {
            var $scaraItem = this._getScaraItem( scaraId ),
                $totalCounter = $scaraItem.find( '.total-spatii-counter' );
                
                self._showScara( scaraId );
                $totalCounter.text( total );
            }
            else
            {
                self._hideScara( scaraId );
            }
        });
        
    },
    
    _reset_: function()
    {
        var self = this;
        
        this._getScaraItems().each( function()
        {
            var $scaraItem = $(this),
                $selectedSpatiiCounter = $scaraItem.find( '.selected-spatii-counter' );
            
            $selectedSpatiiCounter.text( '0' );
            $selectedSpatiiCounter.addClass( 'empty' );
        });
    }

});

})();