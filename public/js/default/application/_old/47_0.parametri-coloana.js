(function(){
    
_Cheltuieli.ParametriColoanaAbstract = _Components.Container.extend(
{
    __abstract: [ 'getParametri' ],
    
    _setup_: function()
    {
        this.suma = null;
    },
    
    _parameters_: function( params )
    {
        params.suma !== undefined && this._setSuma( params.suma );
    },
    
    _setSuma: function( suma )
    {
        this.suma = suma;
    }
    
});

})();