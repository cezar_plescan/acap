(function(){
        
_Spatii.SpatiiListSelectionPickSuprafata = _Spatii.SpatiiListSelectionPick.extend(
{
    _getSelectionValue: function( $row )
    {
        var spatiuData = $row.data( 'data' ),
            suprafata = spatiuData.suprafata;
    
        return suprafata;
    },
    
    _populateSpatiuRow: function( $spatiuRow, spatiuData )
    {
        this._super.apply( this, arguments );
        
        $spatiuRow.find( '>.suprafata-cell' ).text( spatiuData.suprafata );
    }
});

})();