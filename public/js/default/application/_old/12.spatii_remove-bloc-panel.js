(function(){

_Spatii.RemoveBlocPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.blocInfo = null;
        this.changeBloc = true;
    },
        
    _setupElements_: function()
    {
        this.$blocLabel = this.find( '.bloc-label' );
        this.$instructionSelectBloc = this.find( '.instructions .select-bloc' );
    },

    _setupResources_: function()
    {
        this._setupPanes();
        this._initForm();
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ScariListPanel.addHandler( 'selectBloc', function( event )
        {
            if( self.isVisible() )
            {
                var selectedBlocId = event.getArguments()[ 0 ];
                
                self._setBlocId( selectedBlocId );
                self._displayPanel();
            }
        });
    },
    
    /**
     * 
     * @param blocInfo
     * @param changeBloc
     */
    _parameters_: function( params )
    {
        this._setBlocInfo( params.blocInfo );
        this._setChangeBloc( params.changeBloc );
    },
    
    _show_: function()
    {
        if( this.changeBloc )
        {
            this.ScariListPanel.setSelectMode( 'bloc' );
            this.$instructionSelectBloc[ this.StructuraData.getBlocuriCount() > 1 ? 'show' : 'hide' ]();
        }
        else
        {
            this.ScariListPanel.disableSelection( false ); // keep the selected item highlighted
            this.$instructionSelectBloc.hide();
        }
        
        if( !this.blocInfo )
        {
            // get blocId from ScariListPanel
            var blocId;
            
            blocId = this.ScariListPanel.getSelectedBloc();
            
            if( blocId )
            {
                this._setBlocId( blocId );
            }
        }
        
        if( this.blocInfo )
        {
            this._displayPanel();
        }
        else
        {
            this._displaySelectBlocMessage();
        }
    },
    
    _setupPanes: function()
    {
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ) );
    },
    
    _initForm: function()
    {
        var self = this;
        
        this.Form = new _Components.Form( this.getContainer(), {
            action: 'remove-bloc',
            onSuccess: function( event )
            {
                var responseData = event.getArguments()[ 0 ],
                    denumire = self.blocInfo.denumire;
                
                System.info( 'Blocul {bloc} a fost eliminat.'.assign({ bloc: denumire }) );
                
                self._updateResourceData( responseData );
                self._trigger( 'done', {
                    returnToHomePanel: true
                });
            }
            
        });
        
    },
    
    _updateResourceData: function( data )
    {
        this.StructuraData.removeBloc( data );
    },
    
    _setBlocId: function( blocId )
    {
        var blocInfo;
        
        blocInfo = this.StructuraData.getBlocInfo( blocId );
        
        this._setBlocInfo( blocInfo );
    },
    
    _setBlocInfo: function( info )
    {
        if( !Object.isObject( info ) )
        {
            this.blocInfo = null;
        }
        else
        {
            this.blocInfo = info;
            this.$blocLabel.text( info.denumire );

            this.Form.getField( 'id' ).val( info.id );
        }
    },
    
    _disableQuit: function(){},
    _enableQuit: function(){},
    
    _displaySelectBlocMessage: function()
    {
        this.Panes.showPane( 'select-bloc' );
    },
    
    _displayPanel: function()
    {
        this.Panes.showPane( 'panel' );
    },
    
    _setChangeBloc: function( changeBloc )
    {
        this.changeBloc = ( changeBloc === undefined ) ?
                true :
                Object.toBoolean( changeBloc );
    }
        
});

})();