(function(){
        
_Spatii.SpatiiListSelection = _Spatii.SpatiiListComponent.extend(
{
    __abstract: [ '_selectRow', '_getSelectionValue', '_clearSelection' ],
    
    setSelection: function( selection )
    {
        this._clearSelection();
        
        if( !Object.size( selection ) ) return;
        
        var self = this,
            $rows = this._getAllVisibleSpatiuRows(),
            spatiiSelection = {};

        $rows.each( function()
        {
            var $row = $(this),
                spatiuId = $row.data( 'id' ),
                selectionValue = selection[ spatiuId ];
                
            if( selectionValue !== undefined )
            {
                self._selectRow( $row, selectionValue );
                spatiiSelection[ spatiuId ] = selectionValue;
            }
        });
        
        return spatiiSelection;
    },
    
    getSelection: function()
    {
        var self = this,
            selection = {},
            $rows = this._getAllVisibleSpatiuRows();
        
        $rows.each( function()
        {
            var $row = $(this),
                spatiuId = $row.data( 'id' ),
                selectionValue = self._getSelectionValue( $row );
                
            if( selectionValue !== undefined )
            {
                selection[ spatiuId ] = selectionValue;
            }
        });
        
        return selection;
    },
    
    _triggerChange: function()
    {
        this._trigger( 'change', this.getSelection() );
    },
    
    _reset_: function()
    {
        this._clearSelection();
    }
    
});

})();