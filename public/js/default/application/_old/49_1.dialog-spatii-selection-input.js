(function(){
    
_Cheltuieli.SpatiiSelectionInputDialog = _Cheltuieli.SpatiiSelectionDialog.extend(
{
    _setup_: function()
    {
        this.hasErrors = false;
        
        this.SpatiiScariConstructorBase = _Spatii.SpatiiScariSelectionInput;
    },
        
    _setupActions_: function()
    {
        var self = this;
        
        this.SpatiiScari.addHandler( 'error', function()
        {
            self.hasErrors = true;
            self._disableDoneButton();
        });
        
        this.SpatiiScari.addHandler( 'errorOff', function()
        {
            self.hasErrors = false;
            self._computeDoneButtonStatus();
        });
    },
    
    _isDone: function()
    {
        return this._super() && !this.hasErrors;
    }
    
});

})();