(function(){
    
_Cheltuieli.SpatiiSelectionReducerePersoaneDialog = _Cheltuieli.SpatiiSelectionInputDialog.extend(
{
    _setup_: function()
    {
        this.SpatiiScariConstructor = _Spatii.SpatiiScariSelectionInputPersoane;
    },
    
    _getSelectionTitle: function()
    {
        var originalTitle = this._super(),
            title = '{persoane} beneficiază de tarif redus',
            persoane = Object.sum( this.getSelection() ),
            persoaneText = String.pluralize( persoane, 'persoană', 'persoane' );
            
        title = title.assign( { persoane: persoaneText } );
        
        return title;
    }
    
});

//==============================================================================

var __ScariListConstructor = 
{
    redraw: function( scari ) // - e posibil sa fie utila
    {
        var self = this,
            scariCounter = 0,
            $scaraItems = this._getScaraItems(),
            $blocItems = this._getBlocItems();
        
        this.reset();
        
        if( scari === '*' )
        {
            $blocItems
                    .show()
                    .data( 'used', true );
            
            $scaraItems
                    .show()
                    .data( 'used', true );
            
            scariCounter = this._getScaraItems().length;
        }
        else
        {
            $scaraItems.each( function()
            {
                var $scaraItem = $(this),
                    scaraId = $scaraItem.data( 'id' );
                
                if( scari.indexOf( scaraId ) >= 0 )
                {
                    $scaraItem
                            .show()
                            .data( 'used', true );
                    
                    scariCounter ++;
                }
                else
                {
                    $scaraItem
                            .hide()
                            .data( 'used', false );
                }
            });
            
            $blocItems.each( function()
            {
                var $blocItem = $(this),
                    $scariItems = self._getScaraItems( $blocItem ),
                    allUnused = true;
                
                $scariItems.each( function()
                {
                    if( $(this).data( 'used' ) )
                    {
                        allUnused = false;
                        
                        return false;
                    }
                });
                
                if( allUnused )
                {
                    $blocItem
                            .hide()
                            .data( 'used', false );
                }
                else
                {
                    $blocItem
                            .show()
                            .data( 'used', true );
                }
            });
        }
        
        if( scariCounter == 1 )
        {
            var $scaraItem, scaraId;
                    
            $scaraItem = $scaraItems.filter( function()
            {
                return $(this).data( 'used' );
            });
            
            scaraId = $scaraItem.data( 'id' );
            
            this.setCurrentScara( scaraId );
            this._trigger( 'selectScara', scaraId );
        }
    }

};


})();