(function(){
    
_Cheltuieli.BenefParametruPanel = _Cheltuieli.BenefDetailsPanel.extend(
{
    _setup_: function()
    {
        this.parametruId = null;
    },
    
    _setupResources_: function()
    {
        this.SelectParametruSection = new SelectParametruSectionConstructor( this.find( '>.benef-parametru-section' ) );
        this.SelectSpatiiSection = new SelectSpatiiSectionConstructor( this.find( '>.spatii-section' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.SelectParametruSection.addHandler( 'change', function( event, choice )
        {
            self._setParametru( choice );
        });
        
        DataResource.get( 'Structura' ).registerObserver({
            'newParametru discardParametru': function()
            {
                self._checkParametriLength();
            }
        });
        
    },
    
    _init_: function()
    {
        this._checkParametriLength();
        
    },
    
    _reset_: function()
    {
        this.parametruId = null;
        
        this._checkParametriLength();
        this.SelectParametruSection.reset();
    },
    
    _hideSpatiiSection: function()
    {
        this.SelectSpatiiSection.hide();
        this.SelectSpatiiSection.reset();
    },
    
    _showSpatiiSection: function( parametruId )
    {
        this.SelectSpatiiSection.reset();
        this.SelectSpatiiSection.show();
        this.SelectSpatiiSection.setParametru( parametruId );
    },
    
    _hideParametruSection: function()
    {
        this.SelectParametruSection.hide();
    },
    
    _showParametruSection: function()
    {
        this.SelectParametruSection.show();
    },
    
    _setParametru: function( parametruId )
    {
        this.parametruId = parametruId;
        
        if( parametruId !== null )
        {
            this._showSpatiiSection( parametruId );
        }
        else
        {
            this._hideSpatiiSection();
        }
    },
    
    _checkParametriLength: function()
    {
        var StructuraData = DataResource.get( 'Structura' ),
            parametri = StructuraData.getParametriSpatiu();
    
        if( parametri.length == 1 )
        {
            this._hideParametruSection();
            this._setParametru( parametri[ 0 ].code );
        }
        else
        {
            this._showParametruSection();
            this._hideSpatiiSection();
        }
    },
    
    _parameters_: function( params )
    {
        if( params.suma !== undefined )
        {
            this.SelectSpatiiSection.sendParameters( { suma: params.suma } );
        }
    }
    
});

//==============================================================================

var SelectParametruSectionConstructor = _Components.Container.extend(
{
    _setupElements_: function()
    {
        this.$parametriContainer = this.find( '.parametri-container' );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.bind( 'change', ':radio', function()
        {
            var choice = $(this).val();
            
            self._trigger( 'change', choice );
        });
        
        DataResource.get( 'Structura' ).registerObserver({
            'newParametru discardParametru': function()
            {
                self._populateParametriList();
            }
        });
    },
    
    _init_: function()
    {
        this._populateParametriList();
    },
    
    _reset_: function()
    {
        this._clearSelection();
    },
    
    _populateParametriList: function()
    {
        var self = this,
            parametri = DataResource.get( 'Structura' ).getParametriSpatiu();
    
        this.$parametriContainer.empty();
        
        parametri.each( function( parametru )
        {
            var $choice = self._getTemplate( 'parametru-choice' );
            
            self._populateChoiceTemplate( $choice, parametru );
            
            $choice.appendTo( self.$parametriContainer );
        });
        
    },
    
    _populateChoiceTemplate: function( $choice, parametru )
    {
        $choice.find( ':radio' ).val( parametru.code );
        $choice.find( '.denumire-parametru' ).text( parametru.denumire );
    },
    
    _clearSelection: function()
    {
        this.find( ':radio' ).prop( 'checked', false );
    }
    
});

//==============================================================================

var SelectSpatiiSectionConstructor = _Cheltuieli.SelectSpatiiSection.extend(
{
    _createSpatiiDialogInstance: function()
    {
        var Instance = new _Cheltuieli.SpatiiSelectionParametruDialog( $( '#select-spatii-parametru-dialog' ), {
            height: 'auto'
        });
        
        return Instance;
    },
    
    _setup_: function()
    {
        this.parametruCode = null;
        this.suma = null;
    },
    
    _parameters_: function( params )
    {
        if( params.suma !== undefined )
        {
            this.suma = params.suma;
        }
    },
    
    setParametru: function( parametruCode )
    {
        this.parametruCode = parametruCode;
    },
    
    _getDialogOptions: function()
    {
        var options = Object.extend( this._super(), {
            parametru: this.parametruCode
        });
        
        return options;
    },
    
    _generateSummaryText: function( selection )
    {
        var self = this,
            spatiiText = '',
            spatiiCount,
            StructuraData = DataResource.get( 'Structura' ),
            parametruLabel = StructuraData.getParametruInfo( this.parametruCode ).denumire,
            parametruText = '"'+parametruLabel+'"',
            parametriText,
            valuePerSpatiu,
            valuePerSpatiuText,
            valoareParametri = 0, 
            summaryText;
        
        if( selection === '*' )
        {
            var spatii = this._getTotalSpatiiForParametru();
            
            spatiiCount = Object.size( spatii );
            valoareParametri = Object.sum( spatii );
        }
        else
        {
            spatiiCount = selection.length;
            
            selection.each( function( spatiuId )
            {
                var parametruValue = StructuraData.getSpatiuInfo( spatiuId )[ self.parametruCode ].toNumber();

                valoareParametri += parametruValue;
            });
        }
    
        spatiiText = String.pluralize( spatiiCount, 'apartament', 'apartamente' );
    
        parametriText = String.pluralize( valoareParametri, parametruText, parametruText );

        valuePerSpatiu = ( this.suma / valoareParametri ).round( 2 );
        valuePerSpatiuText = valuePerSpatiu + ' lei / unitate';

        summaryText = [ spatiiText, parametriText, valuePerSpatiuText ].join( ', ' );

        return summaryText;
    },
    
    _getTotalSpatiiForParametru: function()
    {
        var self = this,
            spatii = {};
        
        DataResource.get( 'Structura' ).getAllSpatii().each( function( spatiuData )
        {
            var parametruValue = spatiuData[ self.parametruCode ];
            
            if( parametruValue !== undefined && ( parametruValue.toNumber() ) )
            {
                spatii[ spatiuData.id ] = parametruValue.toNumber();
            }
        });
        
        return spatii;
    }
    
});



})();