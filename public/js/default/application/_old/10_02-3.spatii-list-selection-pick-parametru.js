(function(){
        
_Spatii.SpatiiListSelectionPickParametru = _Spatii.SpatiiListSelectionPick.extend(
{
    _setup_: function()
    {
        this.parametruCode = null;
    },
    
    _setupElements_: function()
    {
        this.$parametruLabel = this.$listHeader.find( '.parametru-cell' );
    },
    
    _getSelectionValue: function( $row )
    {
        var spatiuData = $row.data( 'data' ),
            parametruValue = spatiuData[ this.parametruCode ];
    
        return parametruValue;
    },
    
    setParametru: function( parametruCode )
    {
        this.parametruCode = parametruCode;
        
        this._showParametruLabel();
        this._showRowsByParametru();
    },
    
    _showParametruLabel: function()
    {
        var self = this,
            parametri = DataResource.get( 'Structura' ).getParametriSpatiu(),
            parametruLabel;
    
        parametri.each( function( parametru )
        {
            if( self.parametruCode == parametru.code )
            {
                parametruLabel = parametru.denumire_short || parametru.denumire;
                
                return false;
            }
        });
        
        this.$parametruLabel.text( parametruLabel );
    },
    
    _showRowsByParametru: function()
    {
        var self = this;
        
        this._getAllVisibleSpatiuRows().each( function()
        {
            var $row = $(this),
                valoareParametru = self._getSelectionValue( $row );
            
            if( !valoareParametru || !valoareParametru.toNumber() )
            {
                self._hideRow( $row );
            }
            else
            {
                $row.find( '.parametru-cell' ).text( valoareParametru );
            }
        });
    }
    
});

})();