(function(){
    
_Cheltuieli.BenefDetailsPanel = _Components.Container.extend(
{
    _setup_: function()
    {
        this.suma = null;
    },
    
    _parameters_: function( params )
    {
        if( params.suma !== undefined )
        {
            this._setSuma( params.suma );
        }
    },
    
    _show_: function()
    {
        this._triggerDOM( 'scrollToMe', this.getContainer() );        
    },
    
    _setSuma: function( suma )
    {
        this.suma = suma;
    },
    
    _dismiss: function()
    {
//        this.reset();
        this._trigger( 'dismiss' );
    }
    
});
    
})();