(function(){

_Spatii.SpatiiScariSelection = _Spatii.SpatiiScari.extend(
{
    _setup_: function()
    {
        this.ScariPanelConstructorBase = _Spatii.ScariListPanelSelection;
        this.SpatiiPanelConstructorBase = _Spatii.SpatiiPanelSelection;
        this.SpatiiListConstructorBase = _Spatii.SpatiiListSelection;
    },
    
    _setupActions_:function()
    {
        var self = this;
        
        this.SpatiiPanel.addHandler( 'change', function( event, selection )
        {
            self._trigger( 'change', selection );
        });

        this.SpatiiPanel.addHandler( 'paneChange', function( event, paneId, paneSelection )
        {
            self._setSelectedSpatiiCounter( paneId, paneSelection );
        });

    },
    
    _init_: function()
    {
        this._setScariTotals();
    },
    
    getSelection: function()
    {
        return this.SpatiiPanel.getSelection();
    },
    
    setSelection: function( selection )
    {
        var scariSelections,
            self = this;
    
        this.SpatiiPanel.setSelection( selection );
        scariSelections = this.SpatiiPanel.getSelectionsPerScara();
        
        Object.each( scariSelections, function( scaraId, scaraSelection )
        {
            self._setSelectedSpatiiCounter( scaraId, scaraSelection );
        });
    },
    
    _setScariTotals: function()
    {
        var totals = this.SpatiiPanel.getSpatiiTotals();
        
        this.ScariPanel.setTotals( totals );
    },
    
    _setSelectedSpatiiCounter: function( scaraId, counter )
    {
        if( Object.isEnumerable( counter ) )
        {
            counter = Object.size( counter );
        }
        
        this.ScariPanel.setCounter( scaraId, counter );
    }
    
});

})();
