(function(){
    
_Cheltuieli.ParametriColoanaBeneficiar = _Cheltuieli.ParametriColoanaAbstract.extend(
{
    _setupResources_: function()
    {
        this.ChoicesSection = new ChoicesSectionConstructor( this.find( '>.moduri-distribuire-choices-section' ) );
        this.ChoiceDetailsPanes = new ChoiceDetailsPanesConstructor( this.find( '>.moduri-distribuire-details-section' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ChoicesSection.addHandler( 'change', function( event, choice )
        {
            if( choice !== null )
            {
                self._showDetails( choice );
            }
            else
            {
//                self.ChoiceDetailsPanes.hide();
                self.reset();
            }
        });
        
        this.ChoiceDetailsPanes.addHandler( 'dismissPanel', function( event, choice )
        {
            self._clearSelection();
        });
    },
    
    _setSuma: function( suma )
    {
        this._super( suma );
        this.ChoiceDetailsPanes.sendParameters( { suma: suma } );
    },
    
    _reset_: function()
    {
        this._clearSelection();
    },
    
    _clearSelection: function()
    {
        this.ChoicesSection.reset();
        
        this.ChoiceDetailsPanes.hide();
        this.ChoiceDetailsPanes.hidePanes();
    },
    
    _showDetails: function( choice )
    {
        this.ChoiceDetailsPanes.reset();
        this.ChoiceDetailsPanes.show();
        this.ChoiceDetailsPanes.showPane( choice );
        
        this._triggerDOM( 'scrollToMe', this.getContainer() );
    }
    
});

//==============================================================================

var ChoicesSectionConstructor = _Components.Container.extend(
{
    _setupElements_: function()
    {
        this.$choices = this.find( ':radio' );
        this.$parametruChoiceContainer = this.find( '.parametru-choice' );
        this.$parametriListContainer = this.find( '.parametri-list-container' );
    },
    
    _setupActions_: function()
    {
        var self = this,
            StructuraData = DataResource.get( 'Structura' );
        
        this.$choices.on( 'change', function()
        {
            var choice = $(this).val();
            
            self._trigger( 'change', choice );
        });
        
        StructuraData.registerObserver({
            'newParametru discardParametru': function()
            {
                self._populateParametriList();
            }
        });
    },
    
    _init_: function()
    {
        this._populateParametriList();
    },
    
    _reset_: function()
    {
        this._clearSelection();
        
    },
    
    _clearSelection: function()
    {
        this.$choices.prop( 'checked', false );
    },
    
    /**
     * Parametrii sunt returnati de catre StructuraData.getParametriSpatiu();
     * - valori posibile:
     *      - un array fara niciun element -> nu se afiseaza optiunea
     *      - un array cu mai mult de un element -> se afiseaza optiunea
     * 
     * Parametrii depind de:
     * - folosirea unui nou parametru: StructuraData:newParametru
     * - renuntarea la un parametru: StructuraData:discardParametru
     * In toate cazurile se regenereaza lista
     * 
     */
    _populateParametriList: function()
    {
        var StructuraData = DataResource.get( 'Structura' ),
            parametri = StructuraData.getParametriSpatiu(),
            parametriText = '', parametriList = [];
    
        if( Object.size( parametri ) )
        {
            parametri.each( function( parametru )
            {
                parametriList.push( parametru.denumire );
            });
            
            parametriText = parametriList.join( ', ' );
            
            this.$parametriListContainer.text( parametriText );
            this.$parametruChoiceContainer.show();
        }
        else
        {
            this.$parametruChoiceContainer.hide();
            
            if( this._getChoice() == 'parametru' )
            {
                this.reset();
                this._trigger( 'change', null );
            }
        }
    },
    
    _getChoice: function()
    {
        var choice = this.$choices.filter( ':checked' ).val() || null;
        
        return choice;
    }
    
});

//==============================================================================

var ChoiceDetailsPanesConstructor = _Components.Panes.extend(
{
    _setup_: function()
    {
        var self = this;
        
        this.suma = null;
        
        var panelArgs = {
            onDismiss: function( event )
            {
                var panelId = event.getArguments()[ 0 ];
                
                self._trigger( 'dismissPanel', panelId );
            }
        };
        
        this._options.paneConstructors = {
            parametru:  [ _Cheltuieli.BenefParametruPanel, panelArgs ],
            egal:       [ _Cheltuieli.BenefEgalPanel, panelArgs ],
            individual: [ _Cheltuieli.BenefIndividualPanel, panelArgs ]
        };
        
        this.addHandler( 'loadPane', function( event, paneId, Pane )
        {
            Pane.sendParameters( { suma: self.suma } );
        });
        
    },
    
    _reset_: function()
    {
        var Pane = this._getPaneObject( this.getCurrentPaneId() );
        
        if( Pane )
        {
            Pane.reset();
        }
    },
    
    _parameters_: function( params )
    {
        params.suma !== undefined && this._setSuma( params.suma );
    },
    
    _setSuma: function( suma )
    {
        this.suma = suma;
        
        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.sendParameters( { suma: suma } );
        });
    }
    
});

})();