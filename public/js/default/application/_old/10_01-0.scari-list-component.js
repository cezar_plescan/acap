(function(){

_Spatii.ScariListComponent = _Components.Container.extend(
{
    _setup_: function()
    {
    },
    
    _setupElements_: function()
    {
    },
    
    _setupResources_: function()
    {
        var self = this;
        
        this.StructuraData = DataResource.get( 'Structura' );
        
        this.StructuraData.registerObserver({
            addBloc: function( blocData, options )
            {
                self._addBloc( blocData, options );
            },
            removeBloc: function( blocData )
            {
                self._removeBloc( blocData );
            },
            editBloc: function( blocData, options )
            {
                self._editBloc( blocData, options );
            },
            addScara: function( scaraData, options )
            {
                self._addScara( scaraData, options );
            },
            removeScara: function( scaraData )
            {
                self._removeScara( scaraData );
            },
            editScara: function( scaraData, options )
            {
                self._editScara( scaraData, options );
            }
        });

    },
    
    _setupActions_: function()
    {
    },
    
    _init_: function()
    {
        var self = this;
        
        this._generateContent();
        
    },
    
    hideScara: function( scaraId )
    {
        this._hideScara( scaraId );
    },
    
    showScara: function( scaraId )
    {
        this._showScara( scaraId );
    },

    _reset_: function()
    {
        this._showAllItems();
    },
    
    _generateContent: function()
    {
        var self = this;
    
        var data = this._getDataProvider();
        
        data.each( function( blocData ){
            self._addBloc( blocData );
        });
    },
    
    _getDataProvider: function()
    {
        return this.StructuraData.getBlocuri();
    },
    
    _addBloc: function( blocData, options )
    {
        var self = this,
            $blocItem = this._getTemplate( 'bloc-item' ),
            $scariContainer = this._getScariContainer( $blocItem );

        options = Object.makeObject( options );
        
        this._populateBlocItem( $blocItem, blocData );
        
        // adaugare scari
        blocData.scari.each( function( scaraData ){
            self._addScara( scaraData, {
                $container: $scariContainer
            });
        });
        
        this._insertBlocItemAt( $blocItem, options.position );
    },
    
    _removeBloc: function( blocData )
    {
        var $blocItem;
                
        $blocItem = this._getBlocItem( blocData.id );
        $blocItem.remove();
        
    },
    
    _editBloc: function( blocData, options )
    {
        var $blocItem;
        
        options = Object.makeObject( options );
        
        $blocItem = this._getBlocItem( blocData.id );
        this._populateBlocItem( $blocItem, blocData );
        
        if( options.position !== undefined )
        {
            $blocItem.detach();
            
            this._insertBlocItemAt( $blocItem, options.position );
        }
    },
    
    _populateBlocItem: function( $blocItem, blocData )
    {
        $blocItem.data( 'id', blocData.id );
        $blocItem.find( '.bloc-label' ).text( blocData.denumire );
    },
    
    _insertBlocItemAt: function( $blocItem, position )
    {
        var inserted = false;
        
        if( position !== undefined )
        {
            var $target = this._getBlocItems().eq( position );
            if( $target.length )
            {
                $blocItem.insertBefore( $target );
                
                inserted = true;
            }
        }
        
        if( !inserted )
        {
            $blocItem.appendTo( this.getContainer() );
        }
    },
    
    _getBlocItems: function()
    {
        var $items = this.getContainer().children( '.bloc-item' );
        
        return $items;
    },

    _getBlocItem: function( blocId )
    {
        var $item;
        
        $item = this._getBlocItems().filter( function(){
            return $(this).data( 'id' ) == blocId;
        });

        return $item;
    },
    
    _getParentBlocItem: function( $element )
    {
        return $element.closest( '.bloc-item' );
    },
    
    _getBlocItemLabel: function( blocId )
    {
        var $label;
        
        $label = this._getBlocItem( blocId ).find( '.bloc-label' );
        
        return $label;
    },
    
    _getAllBlocItemLabels: function()
    {
        var $labels;
        
        $labels = this._getBlocItems().find( '.bloc-label' );
        
        return $labels;
    },
    
    _getScariContainer: function( $blocItem )
    {
        if( Object.isScalar( $blocItem ) )
        {
            $blocItem = this._getBlocItems().filter( function(){
                return $( this ).data( 'id' ) == $blocItem;
            });
        }
        
        var $scariContainer = $blocItem.children( '.scari-container' );
        
        return $scariContainer;
    },

    _addScara: function( scaraData, options )
    {
        var $scariContainer, $scaraItem;
        
        options = Object.makeObject( options );
        
        $scariContainer = options.$container || this._getScariContainer( scaraData.bloc_id );
        $scaraItem = this._getTemplate( 'scara-item' );
    
        this._populateScaraItem( $scaraItem, scaraData );
        
        this._insertScaraItemAt( $scaraItem, $scariContainer, options.position );
    },
    
    _removeScara: function( scaraData )
    {
        this._getScaraItem( scaraData.id ).remove();
        
    },
    
    _editScara: function( scaraData, options )
    {
        var $scaraItem, $scariContainer;
        
        options = Object.makeObject( options );
        
        $scaraItem = this._getScaraItem( scaraData.id );
        this._populateScaraItem( $scaraItem, scaraData );
        
        if( options.position !== undefined )
        {
            $scaraItem.detach();
            
            $scariContainer = this._getScariContainer( this.StructuraData.getScaraInfo( scaraData.id ).bloc_id );
            this._insertScaraItemAt( $scaraItem, $scariContainer, options.position );
        }
    },
    
    _insertScaraItemAt: function( $scaraItem, $scariContainer, position )
    {
        var inserted = false;
        
        if( position !== undefined )
        {
            var $target = $scariContainer.children().eq( position );
            if( $target.length )
            {
                $scaraItem.insertBefore( $target );
                
                inserted = true;
            }
        }
        
        if( !inserted )
        {
            $scaraItem.appendTo( $scariContainer );
        }
    },
    
    _populateScaraItem: function( $scaraItem, scaraData )
    {
        $scaraItem.data( 'id', scaraData.id );
        $scaraItem.find( '.scara-label' ).text( scaraData.denumire );
    },
    
    _getScaraItems: function( $blocItem )
    {
        var $items;
        
        $items = ( $blocItem === undefined ? this._getBlocItems() : $blocItem )
                .find( '.scara-item' );
        
        return $items;
    },
    
    _getScaraItem: function( scaraId )
    {
        var $item;
        
        $item = this._getScaraItems().filter( function(){
            return $(this).data( 'id' ) == scaraId;
        });

        return $item;
    },
    
    _getScaraItemLabel: function( scaraId )
    {
        var $label;
        
        $label = this._getScaraItem( scaraId ).find( '.scara-label' );
        
        return $label;
    },
    
    _getAllScaraItemLabels: function()
    {
        var $labels;
        
        $labels = this._getScaraItems().find( '.scara-label' );
        
        return $labels;
    },
    
    _getScaraBlocId: function( scaraId )
    {
        var scaraInfo, blocId;
        
        scaraInfo = this.StructuraData.getScaraInfo( scaraId );
        blocId = scaraInfo.bloc_id;
        
        return blocId;
    },
    
    _hideScara: function( scaraId )
    {
        var $scaraItem  = this._getScaraItem( scaraId ),
            $blocItem  = this._getParentBlocItem( $scaraItem ),
            $siblings   = this._getScaraItems( $blocItem );
                
        $scaraItem.hide();
        
        if( !$siblings.is( ':visible' ) )
        {
            $blocItem.hide();
        }
    },
    
    _showScara: function( scaraId )
    {
        var $scaraItem  = this._getScaraItem( scaraId ),
            $blocItem  = this._getParentBlocItem( $scaraItem ),
            $siblings   = this._getScaraItems( $blocItem );
                
        $scaraItem.show();
        
        if( $siblings.is( ':visible' ) )
        {
            $blocItem.show();
        }
    },
    
    _showAllItems: function()
    {
        this._getBlocItems().show();
        this._getScaraItems().show();
    },
    
    
});

})();
