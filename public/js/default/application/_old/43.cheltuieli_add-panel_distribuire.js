(function(){
    
_Cheltuieli.DistribuireSection = _Cheltuieli.SectionAbstract.extend(
{
    _setupResources_: function()
    {
        var choicesArguments = {
            Section: this
        };
        
        this.ChoicesPanes = new ChoicePanesConstructor( this.find( '.choices-panes' ), {
            paneConstructors: {
                integral:   [ IntegralPaneConstructor, choicesArguments ],
                fragmentat: [ FragmentatPaneConstructor, choicesArguments ],
            }
        });
        
        this.SelectFondDialog = new _Cheltuieli.SelectFondDialog( $( '#select-fond-dialog' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.find( 'input[name=_d]' ).on( 'change', function()
        {
            var value = $(this).val() - 0;
            
            if( value )
            {
                self._showIntegralChoice();
            }
            else
            {
                self._showFragmentatChoice();
            }
        });
    },
    
    _init_: function()
    {
        
    },
    
    getSuma: function()
    {
        return this.Page.getSuma();
    },
    
    _showIntegralChoice: function()
    {
        this.ChoicesPanes.showPane( 'integral' );
    },
    
    _showFragmentatChoice: function()
    {
        this.ChoicesPanes.showPane( 'fragmentat' );
    }    
    
});

//==============================================================================

var ChoicePanesConstructor = _Components.Panes.extend(
{
    
});

//==============================================================================

var IntegralPaneConstructor = _Components.Container.extend(
{
    _setup_: function()
    {
        this.Section = this._options.Section;
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.find( '.select-fond-button' ).on( 'click', function()
        {
            self._showSelectFondDialog(); // preluare parametri ...
        });
    },

    _getSelectFondDialog: function()
    {
        return this.Section.SelectFondDialog;
    },
        
    _showSelectFondDialog: function()
    {
        this._getSelectFondDialog().open({
            suma: this._getSuma()
        });
    },
    
    _getSuma: function()
    {
        return this.Section.getSuma();
    }
    
});

//==============================================================================

var FragmentatPaneConstructor = _Components.Container.extend(
{

});


})();