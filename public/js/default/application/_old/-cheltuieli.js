(function(){

var SelectSpatiiPopupConstructor = _Components.Dialog.extend(
{
    _init_: function()
    {
        var self = this;

        this.$checkboxes = this.find(':checkbox');

        this.find('input.cancel').on('click', function(){
            self.close();
        });

        this.find('input.save').on('click', function(){
            self._save();
            self.close();
        });

        this.find('.select-all').on('click', function(){
            self.$checkboxes.prop('checked', true);
        });

        this.find('.deselect-all').on('click', function(){
            self.$checkboxes.prop('checked', false);
        });

    },

    _open: function()
    {
        this._restore();
    },

    _restore: function()
    {
        this.$checkboxes
            .prop('checked', false)
            .filter('.selected')
                .prop('checked', true);

    },

    _save: function()
    {
        var selection = [];

        this.$checkboxes
            .removeClass('selected')
            .filter(':checked').each(function(){
                var $input = $(this);

                $input.addClass('selected');
                selection.push( $input.val() );
            });

        this._trigger('selectEnd', selection);

    },

    reset: function()
    {
        this.$checkboxes
                .prop('checked', false)
                .removeClass('selected');
    }

});

var SelectSpatiiConstructor = _Components.Container.extend(
{
    _init_: function()
    {
        var self = this;

        this.SpatiiPopup = {};

        this.find('.spatii-popup').each(function(){
            var $element = $(this),
                scaraId = $element.data('scara_id');

            self.SpatiiPopup[ scaraId ] = new SelectSpatiiPopupConstructor( $element, {
                selectEnd: function( spatii )
                {
                    self._saveSpatii( spatii, scaraId );
                }
            });
        });

        this.$buttons = this.find('.select-spatii-button');

        this.$buttons.each(function(){
            var $button = $(this),
                scaraId = $button.data('scara_id');

            $button.data('default_label', $button.text());

            $button.on('click', function(){
                self.SpatiiPopup[ scaraId ].open();
            })

        });

        this.$spatiiSelectionContainers = this.find( '.spatii-selection' );
        this.$spatiuTemplate = this.$spatiiSelectionContainers.find(':hidden').remove().eq(0);

    },

    _saveSpatii: function( spatii, scaraId )
    {
        var self = this;
        var $container = this.$spatiiSelectionContainers.filter( '[data-scara_id="' + scaraId + '"]' );

        $container.empty();
        foreach( spatii, function( index, spatiuId ){
            var $clone = self.$spatiuTemplate.clone().val( spatiuId );
            $container.append( $clone );
        });

        var $button = this.$buttons.filter( '[data-scara_id="' + scaraId + '"]' );

        var countSpatii = count( spatii );
        if ( countSpatii )
        {
            var text =  countSpatii + ' apartament' + ( countSpatii > 1 ? 'e' : '' );
        }
        else
        {
            text = $button.data('default_label');
        }

        $button.text( text );
    },

    reset: function()
    {
        foreach( this.SpatiiPopup, function( index, Popup ){
            Popup.reset();
        });

        this.$spatiiSelectionContainers.empty();

        this.$buttons.each(function(){
            var $button = $(this);
            $button.text( $button.data('default_label') );
        });

    }


});


var SelectSpatiiBeneficiarPopupConstructor = _Components.Dialog.extend(
{
    _init_: function()
    {
        var self = this;

        this.$fields = this.find('.input-field');

        this.find('input.cancel').on('click', function(){
            self.close();
        });

        this.find('input.save').on('click', function(){
            self._save();
            self.close();
        });

    },

    _open: function()
    {
        this._restore();
    },

    _restore: function()
    {
        this.$fields.each(function(){
            var $input = $(this);

            $input.val( $input.data('value') || '' );

        });

    },

    _save: function()
    {
        var values = {};

        this.$fields.each(function(){
            var $input = $(this),
                value = $.trim( $input.val() );

            $input.data( 'value', value );

            if ( value.length )
            {
                values[ $input.attr('name') ] = value;
            }
        });

        this._trigger('selectEnd', values);
    },

    reset: function()
    {
        this.$fields
                .val('')
                .data('value', '');
    }


});

var SpatiiBeneficiarConstructor = _Components.Container.extend(
{
    _init_: function()
    {
        var self = this;

        this.SpatiiPopup = {};

        this.find('.spatii-beneficiar-popup').each(function(){
            var $element = $(this),
                scaraId = $element.data('scara_id');

            self.SpatiiPopup[ scaraId ] = new SelectSpatiiBeneficiarPopupConstructor( $element, {
                selectEnd: function( spatiiValues )
                {
                    self._saveSpatii( spatiiValues, scaraId );
                }
            });
        });

        this.$buttons = this.find('.select-spatii-button');

        this.$buttons.each(function(){
            var $button = $(this),
                scaraId = $button.data('scara_id');

            $button.data('default_label', $button.text());

            $button.on('click', function(){
                self.SpatiiPopup[ scaraId ].open();
            })

        });

        this.$spatiiSelectionContainers = this.find( '.spatii-selection' );
        this.$spatiuTemplate = this.$spatiiSelectionContainers.find(':hidden').remove().eq(0);

    },

    _saveSpatii: function( spatiiValues, scaraId )
    {
        var self = this;
        var $container = this.$spatiiSelectionContainers.filter( '[data-scara_id="' + scaraId + '"]' );

        $container.empty();
        foreach( spatiiValues, function( name, value ){
            var $clone = self.$spatiuTemplate.clone();

            $clone
                .attr( 'name', name )
                .val( value );

            $container.append( $clone );
        });

        var $button = this.$buttons.filter( '[data-scara_id="' + scaraId + '"]' );

        var countSpatii = count( spatiiValues );
        if ( countSpatii )
        {
            var text =  countSpatii + ' apartament' + ( countSpatii > 1 ? 'e' : '' );
        }
        else
        {
            text = $button.data('default_label');
        }

        $button.text( text );
    },

    reset: function()
    {
        foreach( this.SpatiiPopup, function( index, Popup ){
            Popup.reset();
        });

        this.$spatiiSelectionContainers.empty();

        this.$buttons.each(function(){
            var $button = $(this);
            $button.text( $button.data('default_label') );
        });
    }


});

var Add = _Components.Form.extend(
{
    url: '/api/add-cheltuiala',

    _init_: function()
    {
        this._initSelectareLuna();

        this._initSelectareColoana();

        this._initSelectareSpatiiRepartizare();

        this._initSelectareSpatiiBeneficiar();
    },

    reset: function()
    {
        var $selectedMonth = this._getField( 'month' ).filter( ':checked' );

        this._super();

        this.$coloaneSections.hide();
        this.$submitContainer.hide();
        this.SelectSpatii.reset();
        this.SelectSpatiiBeneficiar.reset();
        this.$monthSection.hide();
        $selectedMonth.prop( 'checked', true );

    },

    _success: function( response )
    {
        this._trigger('onAdd');

        this.reset();

        Application.setWorkingMonth( response.month );

        alert('Cheltuiala a fost adaugata.');
    },

    _error: function()
    {
        alert('Verificaţi informaţiile completate!');
    },

    _initSelectareLuna: function()
    {
        var self = this;

        this.$monthSection = this.find('.section.section-month');

        if ( this.$monthSection.is(':visible') )
        {
            this.$sectionColoane = this.find( '.section.section-coloane' ).hide();

            this.$monthSection.find('.select-month').one('change', function(){
                self.$sectionColoane.show();
            });
        }

    },

    _initSelectareColoana: function()
    {
        var self = this;

        this.$monthSection = this.find('.section.section-month');

        this.$coloaneSections = this.find('.section.coloana-section').hide();

        this.$submitContainer = this.find('.submit-container').hide();

        if ( this.$monthSection )

        this.bind('change', 'input.coloana-choice', function(){
            var $input = $(this),
                coloanaCode = $input.data('code');

            // exista sectiuni specifice anumitor coloane
            self._showColoanaSection( coloanaCode );

            self.$submitContainer.show();

            self._clearErrors();
        });
    },

    _showColoanaSection: function( coloanaCode )
    {
        this.$coloaneSections.hide();

        this._disableFields( this.$coloaneSections );

        var $visibleSections = this.$coloaneSections.filter(function(){
            var $section = $(this),
                sectionColoanaCode = $section.data('code');

            if ( typeof sectionColoanaCode == 'undefined' || sectionColoanaCode === null )
            {
                return true;
            }

            var sectionCodes = sectionColoanaCode.split(' ');
            if ( sectionCodes.indexOf( coloanaCode ) >= 0 )
            {
                return true;
            }

            return false;
        });

        $visibleSections.show();
        this._enableFields( $visibleSections );

    },

    _disableFields: function( $container )
    {
        $container.find( 'select, input, textarea' ).attr( 'disabled', 'disabled' );
    },

    _enableFields: function( $container )
    {
        $container.find( 'select, input, textarea' ).removeAttr( 'disabled' );
    },

    _initSelectareSpatiiRepartizare: function()
    {
        this.SelectSpatii = new SelectSpatiiConstructor( this.$coloaneSections.filter('.section-spatii-repartizare') );
    },

    _initSelectareSpatiiBeneficiar: function()
    {
        this.SelectSpatiiBeneficiar = new SpatiiBeneficiarConstructor( this.$coloaneSections.filter('.section-spatii-beneficiar') );
    }

});

// LIST CHELTUIELI /////////////////////////////////////////////////////////////

var List = _Components.Container.extend(
{
    _init_: function()
    {
        this.Ajax = new _Components.Ajax( this.getContainer(), {
            baseUrl: '/api'
        })

        this.$listContainer = this.find( '.list-cheltuieli-container' );
        this.$noResults = this.find( '.no-results' );
        this.$valoareTotala = this.find( '.valoare-totala' );

        this._checkList();

        this._initDelete();

    },

    _initDelete: function()
    {
        var self = this;

        this.bind('click', '.delete-cheltuiala', function(){
            if ( !confirm('Doriţi să ştergeţi această cheltuială ?') )
            {
                return;
            }

            var $button = $(this),
                cheltuialaId = $button.data('id');

            self.Ajax.request({
                url: 'delete-cheltuiala',
                data: {
                    id: cheltuialaId
                },
                success: function()
                {
                    $button.closest('.cheltuiala-item').slideUp(function(){
                        $(this).remove();
                        self._checkList();
                        self._updateValoareTotala();
                    });
                }
            });
        })
    },

    _getCheltuialaItems: function()
    {
        return this.$listContainer.children( '.cheltuiala-item' );
    },

    _checkList: function()
    {
        if( this._getCheltuialaItems().length )
        {
            this.$noResults.hide();
            this.$listContainer.show();
        }
        else
        {
            this.$noResults.show();
            this.$listContainer.hide();
        }
            
            

    },
            
    _updateValoareTotala: function()
    {
        var total = 0;
        this._getCheltuialaItems().each(function(){
            var cheltuialaValue = parseFloat( $(this).find( '.valoare-cheltuiala' ).text() );
            
            total += cheltuialaValue;
        });
        
        this.$valoareTotala.text( total );
    }

});

// MAIN MODULE /////////////////////////////////////////////////////////////////

var Cheltuieli = _Components.Container.extend(
{
    _init_: function()
    {
        var self = this;

        this.AddComponent = new Add( $('#add-cheltuiala-module'), {
            onAdd: function()
            {
                self._reloadList();
            }
        });

        this.ListComponent = new List( $('#list-cheltuieli-module') );

    },

    _reloadList: function()
    {
        var self = this;

        var Ajax = new _Components.Ajax();
        Ajax.request({
            url: '/cheltuieli/list',
            dataType: 'html',
            type: 'get',
            success: function( response )
            {
                var $newContainer = $( $.trim(response) );
                self.ListComponent.getContainer().replaceWith( $newContainer );

                self.ListComponent = new List( $newContainer );
            }
        });
    }

});

////////////////////////////////////////////////////////////////////////////////

var _Cheltuieli = _Components.Container.extend(
{
    _defaultOptions: {
        showLoadingMask:        true
    },
    
    _init_: function()
    {
        var self = this;
        
        setTimeout( function(){
            self._trigger( 'contentLoaded' );
        }, 8000 );
    }
    
});

Modules.Cheltuieli = _Cheltuieli;

})();
