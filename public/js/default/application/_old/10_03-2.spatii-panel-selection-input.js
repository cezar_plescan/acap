(function(){
        
_Spatii.SpatiiPanelSelectionInput = _Spatii.SpatiiPanelSelection.extend(
{
    _setup_: function()
    {
        this.errorsCount = 0;
        
        this.SpatiiListConstructorBase = _Spatii.SpatiiListSelectionInput;
    },
    
    _setupActions_:function()
    {
        var self = this;
        
        this.addHandler( 'loadPane', function( event, paneId, Pane )
        {
            Pane.addHandler( 'error', function()
            {
                self.errorsCount ++;
                
                if( self.errorsCount == 1 )
                {
                    self._trigger( 'error' );
                }
            });
            
            Pane.addHandler( 'errorOff', function()
            {
                self.errorsCount --;
                
                if( self.errorsCount == 0 )
                {
                    self._trigger( 'errorOff' );
                }
            });
            
        });

    },
    
    _init_: function()
    {
        this.getContainer().addClass( 'input-values' );
    },
    
    _reset_: function()
    {
        this.errorsCount = 0;
    }
    
});

})();