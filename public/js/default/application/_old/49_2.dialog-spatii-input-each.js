(function(){
    
_Cheltuieli.SpatiiSelectionEachDialog = _Cheltuieli.SpatiiSelectionInputDialog.extend(
{
    _setup_: function()
    {
        this.SpatiiScariConstructor = _Spatii.SpatiiScariSelectionInputEach;
    },
    
    _getSelectionTitle: function()
    {
        var title = this._super(),
            sumaText,
            sumaPartiala = this._getSumaPartiala(),
            rest,
            restText = '';
        
        sumaText = sumaPartiala.round( 2 ) + ' lei';

        if( !this._isSumaDistribuita() )
        {
            rest = this._getSumaRest().round( 4 );
            restText = ' (rest: ' + rest + ' lei)';

            sumaText += restText;
        }

        title += ', ' + sumaText;
            
        return title;
    },
    
    _getInitialTitle: function()
    {
        var title,
            sumaText;

        sumaText = ' (Suma: ' + this._getSuma().round( 2 ) + ' lei)';
        title = this._super() + sumaText;

        return title;
    },
    
    _getSumaPartiala: function()
    {
        return Object.sum( this.getSelection() );
    },
    
    _getSumaRest: function()
    {
        return this._getSuma() - this._getSumaPartiala();
    },
    
    _isSumaDistribuita: function()
    {
        return this._getSumaRest().abs() < Math.pow( 10, -16 );
    },
    
    _isDone: function()
    {
        return this._super() && this._isSumaDistribuita();
    }
    
    
});

})();