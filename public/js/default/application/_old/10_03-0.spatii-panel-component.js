(function(){

_Spatii.SpatiiPanelComponent = _Components.Panes.extend(
{
    _setup_: function()
    {
        this.scaraId = null;
        
        this.SpatiiListConstructorBase = _Spatii.SpatiiListComponent;
        this.SpatiiListConstructor = null;
    },
    
    _setupResources_: function()
    {
        if( this._options.SpatiiListConstructor )
        {
            this.SpatiiListConstructor = this._options.SpatiiListConstructor;
        }
        
        if( this.SpatiiListConstructor === null )
        {
            this.SpatiiListConstructor = this.SpatiiListConstructorBase;
        }
        else
        {
            if( !_Components.Class.isExtendedFrom( this.SpatiiListConstructor, this.SpatiiListConstructorBase ) )
            {
                System.error( 'Check "SpatiiScaraListConstructor" class' );
            }
        }
        
        this.StructuraData = DataResource.get( 'Structura' );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.StructuraData.registerObserver({
            removeScara: function( scaraData )
            {
                self._removePane( scaraData.id );
            }
        });
    },
    
    _init_: function()
    {
        this._loadAllPanes();
    },
    
//    _parameters_: function( params )
//    {
//        this._setScaraId( params.scaraId );
//    },
//    
    _end_: function()
    {
        this._checkIfScaraIsSelected();
    },
    
    _reset_: function()
    {
        this.scaraId = null;
        
        this._checkIfScaraIsSelected();
    },
    
    getSpatiiTotals: function()
    {
        return this._getSpatiiTotals();
    },
    
    showSpatii: function( spatii )
    {
        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.showSpatii( spatii );
        });
    },
    
    setScaraId: function( scaraId )
    {
        var $paneTemplate;

        this.scaraId = scaraId;
        
        if( !scaraId ) return;
        
        if( !this._isPaneLoaded( scaraId ) )
        {
            $paneTemplate = this._getTemplate( 'spatii-list-pane' );
            this._addPane( $paneTemplate, scaraId, [ this.SpatiiListConstructor, { Parent: this, scaraId: scaraId } ] );
        }
        
        if( this.getCurrentPaneId() == scaraId )
        {
            this.getPane( scaraId ).reset();
        }
        else
        {
            this.showPane( scaraId );
        }
    },
    
    _getScaraId: function()
    {
        return this.scaraId;
    },
    
    _checkIfScaraIsSelected: function()
    {
        if( !this.scaraId )
        {
            this._showSelectScaraMessage();
        }
    },
    
    _showSelectScaraMessage: function()
    {
        this.showPane( 'select-scara' );
    },
    
    _loadAllPanes: function()
    {
        var self = this,
            scari = this.StructuraData.getScari();
        
        scari.each( function( scaraData )
        {
            var scaraId = scaraData.id;
            
            if( !self._isPaneLoaded( scaraId ) )
            {
                var $paneTemplate = self._getTemplate( 'spatii-list-pane' );
                self._addAndLoadPane( $paneTemplate, scaraId, [ self.SpatiiListConstructor, { Parent: self, scaraId: scaraId } ] );
            }
        });
    },
    
    _getSpatiiTotals: function()
    {
        var totals = {};
        
        Object.each( this.Panes, function( paneId, Pane )
        {
            totals[ paneId ] = Pane.getTotalSpatii();
        });
        
        return totals;
    }

});

})();