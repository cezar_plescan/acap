(function(){

_Spatii.AddScaraPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.blocInfo = null;
        this.changeBloc = true;
    },
    
    _setupElements_: function()
    {
        this.$instructionSelectBloc = this.find( '.instructions .select-bloc' );
    },
    
    _setupResources_: function()
    {
        this._setupPanes();
        this._initForm();
        
        this.RemoveBloc = new RemoveBlocSectionConstructor( this.find( '.remove-bloc-section' ), {
            DisplayManager: this.DisplayManager
        });
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ScariListPanel.addHandler( 'selectBloc', function( event )
        {
            if( self.isVisible() )
            {
                var selectedBlocId = event.getArguments()[ 0 ];
                
                self._setBlocId( selectedBlocId );
                self._displayPanel();
            }
        });
    },
    
    /**
     * 
     * @param blocInfo
     * @param changeBloc
     */
    _parameters_: function( params )
    {
        this._setBlocInfo( params.blocInfo );
    },
    
    _show_: function()
    {
        if( this.changeBloc )
        {
            this.ScariListPanel.setSelectMode( 'bloc' );
            this.$instructionSelectBloc[ this.StructuraData.getBlocuriCount() > 1 ? 'show' : 'hide' ]();
            this.RemoveBloc.hide();
        }
        else
        {
            this.ScariListPanel.disableSelection( false ); // keep the selected item highlighted
            this.$instructionSelectBloc.hide();
            this.RemoveBloc.show();
        }
        
        if( !this.blocInfo )
        {
            // get blocId from ScariListPanel
            var blocId;
            
            blocId = this.ScariListPanel.getSelectedBloc();
            
            if( blocId )
            {
                this._setBlocId( blocId );
            }
        }
        
        if( this.blocInfo )
        {
            this._displayPanel();
        }
        else
        {
            this._displaySelectBlocMessage();
        }
    },
        
    _setupPanes: function()
    {
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ) );
    },
    
    _initForm: function()
    {
        var self = this;
        
        var errorMessages = {
            denumire:   'Completați denumirea scării.',
            adresa:     'Completați adresa scării.',
        };
        
        this.Form = new _Components.Form( this.getContainer(), {
            action: 'add-scara',
            onBeforeSubmit: function( event )
            {
                var formData = event.getArguments()[ 0 ],
                    errors = [];
                
                Object.each( formData, function( fieldName, fieldValue ){
                    if( fieldValue.isBlank() )
                    {
                        errors.push( errorMessages[ fieldName ] );
                    }
                });
                
                if( errors.length )
                {
                    this._showErrors( errors );
                    event.abort();
                }
            },
            onSuccess: function( event )
            {
                var responseData = event.getArguments()[ 0 ],
                    denumire = responseData.denumire,
                    denumireBloc = self.blocInfo.denumire;
                
                System.info( 'Scara {ds} a fost adăugată la blocul {db}.'.assign({ ds: denumire, db: denumireBloc }) );
                
                this.clear();
                
                self._updateResourceData( responseData );
                self.ScariListPanel.setCurrentScara( responseData.id );

                self._trigger( 'done' );
            }
            
        });
        
    },
    
    _updateResourceData: function( data )
    {
        this.StructuraData.addScara( data );
    },
    
    _setBlocId: function( blocId )
    {
        var blocInfo;
        
        blocInfo = this.StructuraData.getBlocInfo( blocId );
        
        this._setBlocInfo( blocInfo );
    },
    
    _setBlocInfo: function( info )
    {
        if( !Object.isObject( info ) )
        {
            this.blocInfo = null;
        }
        else
        {
            this.blocInfo = info;

            this.Form.getField( 'bloc_id' ).val( info.id );
            this.Form.clear();
        }
        
        this.RemoveBloc.setBlocInfo( info );
    },
    

    _displaySelectBlocMessage: function()
    {
        this.Panes.showPane( 'select-bloc' );
    },
    
    _displayPanel: function()
    {
        this.Panes.showPane( 'panel' );
        this.Form.getField( 'denumire' ).focus();
    },
    
    _setChangeBloc: function( changeBloc )
    {
        this.changeBloc = ( changeBloc === undefined ) ?
                true :
                Object.toBoolean( changeBloc );
    },
    
    _disableQuit: function()
    {
        this._super();
        
        this.changeBloc = false;
    },
    
    _enableQuit: function()
    {
        this._super();
        
        this.changeBloc = true;
    }
    
});

var RemoveBlocSectionConstructor = _Components.Container.extend(
{
    _setup_: function()
    {
        if( !this._options.DisplayManager )
        {
            throw new Error( '"DisplayManager" option is not defined.' );
        }
        
        this.DisplayManager = this._options.DisplayManager;
        this.blocInfo = null;
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.bind( 'click', '.remove-bloc-button', function(){
            self._removeBloc();
        });
    },
    
    _parameters_: function( params )
    {
        this.setBlocInfo( params.blocInfo );
    },
    
    setBlocInfo: function( info )
    {
        this.blocInfo = info;
    },
    
    _removeBloc: function()
    {
        if( !this.blocInfo )
        {
            throw new Error( '"blocInfo" is not set.' );
        }
        
        this.DisplayManager.display( _Spatii.ContentPanelCodes.REMOVE_BLOC, { 
            blocInfo:   this.blocInfo,
            changeBloc: false
        }, {
            force: true
        });
    }
});

})();