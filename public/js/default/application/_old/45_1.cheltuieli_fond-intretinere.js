(function(){

_Cheltuieli.FondIntretinereFrame = _Cheltuieli.FondDetailsPanel.extend(
{
    _setup_: function()
    {
        this.suma = null;
        this.selectedColoana = null;
    },
    
    _setupResources_: function()
    {
        this.ChoicesSection         = new ChoicesSectionConstructor( this.find( '.coloane-choices' ) );
        this.ParametriColoanaPanes  = new ParametriColoanaPanesConstructor( this.find( '.parametri-coloana-panes' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ChoicesSection.addHandler( 'select', function( event, coloana )
        {
            self._setColoana( coloana );
        });
    },
    
    _init_: function()
    {
        this.ParametriColoanaPanes.hide();
    },
    
    _reset_: function()
    {
        this.suma = null;
        this.selectedColoana = null;
        
        this.ChoicesSection.reset();
        
        this.ParametriColoanaPanes.hide();
        this.ParametriColoanaPanes.reset();
    },
    
    getParametri: function()
    {
        var parametri = {
            coloana_id:         this._getSelectedColoana(),
            parametri_coloana:  this._getColoanaParametri(),
        };
        
        return parametri;
    },
    
    _setSuma: function( suma )
    {
        this._super( suma );
        
        this.ParametriColoanaPanes.sendParameters( { suma: suma } );
    },
    
    _setColoana: function( coloana )
    {
        this.selectedColoana = coloana;
        this._showColoanaParametri( coloana );
    },
    
    _showColoanaParametri: function( coloana )
    {
        this.ParametriColoanaPanes.show();
        this.ParametriColoanaPanes.showPane( coloana );
    },
    
    _getSelectedColoana: function()
    {
        return this.selectedColoana;
    },
    
    _getColoanaParametri: function()
    {
        return this.ParametriColoanaPanes.getParametri();
    }
    
});

//==============================================================================

var ChoicesSectionConstructor = _Components.Container.extend(
{
    _setupElements_: function()
    {
        this.$choices = this.find( 'input[name=_c]' );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$choices.on( 'change', function()
        {
            var coloana = $(this).val();
            
            self._trigger( 'select', coloana );
        });
    },
    
    _reset_: function()
    {
        this._clearChoices();
    },
    
    _clearChoices: function()
    {
        this.$choices.prop( 'checked', false );
    }
    
});

//==============================================================================

var ParametriColoanaPanesConstructor = _Components.Panes.extend(
{
    _setup_: function()
    {
        var self = this;
        
        this.suma = null;
        
        var panelArgs = {};
        
        this._options.paneConstructors = {
            nr_pers:    [ _Cheltuieli.ParametriColoanaNrPersoane, panelArgs ],
            supr:       [ _Cheltuieli.ParametriColoanaSuprafata, panelArgs ],
            incalzire:  [ _Cheltuieli.ParametriColoanaIncalzire, panelArgs ],
            benef:      [ _Cheltuieli.ParametriColoanaBeneficiar, panelArgs ],
            apa_rece:   [ _Cheltuieli.ParametriColoanaApaRece, panelArgs ],
            apa_calda:  [ _Cheltuieli.ParametriColoanaApaCalda, panelArgs ]
        };
        
        this.addHandler( 'loadPane', function( event, paneId, Pane )
        {
            Pane.sendParameters( { suma: self.suma } );
        });
        
    },
    
    _parameters_: function( params )
    {
        params.suma !== undefined && this._setSuma( params.suma );
    },
    
    getParametri: function()
    {
        var parametri = null,
            currentPaneId = this.getCurrentPaneId();
            
        if( currentPaneId )
        {
            parametri = this._getPaneObject( currentPaneId ).getParametri();
        }
        
        return parametri;
    },
    
    _setSuma: function( suma )
    {
        this.suma = suma;

        Object.each( this.Panes, function( paneId, Pane )
        {
            Pane.sendParameters( { suma: suma } );
        });
    }
    
});

})();