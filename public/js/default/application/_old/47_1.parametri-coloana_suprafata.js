(function(){
    
_Cheltuieli.ParametriColoanaSuprafata = _Cheltuieli.ParametriColoanaAbstract.extend(
{
    _setupResources_: function()
    {
        this.SpatiiSection = new SpatiiSectionConstructor( this.find( '>.spatii-section' ) );
    },
    
    getParametri: function()
    {
        var parametri = {
            spatii:     this.SpatiiSection.getSpatii()
        };
        
        return parametri;
    }
    
});

//==============================================================================

var SpatiiSectionConstructor = _Cheltuieli.SelectSpatiiSection.extend(
{
    _createSpatiiDialogInstance: function()
    {
        var Instance = new _Cheltuieli.SpatiiSelectionSuprafataDialog( $( '#select-spatii-suprafata-dialog' ), {
            height: 'auto'
        });
        
        return Instance;
    }
    
});


})();