(function(){

_Spatii.EditBlocPanel = _Spatii.AbstractContentPanel.extend(
{
    _setup_: function()
    {
        this.blocInfo = null;
    },
    
    _setupElements_: function()
    {
        this.$instructionSelectBloc = this.find( '.instructions .select-bloc' );
    },
    
    _setupResources_: function()
    {
        this._setupPanes();
        this._initForm();
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.ScariListPanel.addHandler( 'selectBloc', function( event )
        {
            if( self.isVisible() )
            {
                var selectedBlocId = event.getArguments()[ 0 ];
                
                self._setBlocId( selectedBlocId );
                self._displayPanel();
            }
        });
    },
    
    _parameters_: function( params )
    {
        this._setBlocInfo( params.blocInfo );
    },
    
    _show_: function()
    {
        this.ScariListPanel.setSelectMode( 'bloc' );
        
        if( !this.blocInfo )
        {
            // get blocId from ScariListPanel
            var blocId;
            
            blocId = this.ScariListPanel.getSelectedBloc();
            
            if( blocId )
            {
                this._setBlocId( blocId );
            }
        }
        
        if( this.blocInfo )
        {
            this._displayPanel();
        }
        else
        {
            this._displaySelectBlocMessage();
        }
    },
    
    _setupPanes: function()
    {
        this.Panes = new _Components.Panes( this.find( '>.panes-container' ) );
    },
    
    _initForm: function()
    {
        var self = this;
        
        this.Form = new _Components.Form( this.getContainer(), {
            action: 'edit-bloc',
            onBeforeSubmit: function( event )
            {
                var formData = event.getArguments()[ 0 ];
                
                if( formData.denumire.isBlank() )
                {
                    this._showErrors( 'Completați denumirea blocului.' );
                    
                    event.abort();
                }
            },
            onSuccess: function( event )
            {
                var responseData = event.getArguments()[ 0 ],
                    denumire = self.blocInfo.denumire;
                
                System.info( 'Blocul {bloc} a fost modificat.'.assign({ bloc: denumire }) );
                
                this.clear();
                
                self._updateResourceData( responseData );
                self._trigger( 'done', {
                    returnToHomePanel: true
                });
            }
            
        });        
    },
    
    _updateResourceData: function( data )
    {
        this.StructuraData.editBloc( data );
    },
    
    _setBlocId: function( blocId )
    {
        var blocInfo;
        
        blocInfo = this.StructuraData.getBlocInfo( blocId );
        
        this._setBlocInfo( blocInfo );
    },
    
    _setBlocInfo: function( info )
    {
        if( !Object.isObject( info ) )
        {
            this.blocInfo = null;
        }
        else
        {
            this.blocInfo = info;

            this.Form.getField( 'id' ).val( info.id );
            this.Form.getField( 'denumire' ).val( info.denumire );
        }
    },
    
    _displaySelectBlocMessage: function()
    {
        this.Panes.showPane( 'select-bloc' );
    },
    
    _displayPanel: function()
    {
        this.Panes.showPane( 'panel' );
        
        this.Form.getField( 'denumire' ).focus().select();
        
        this.$instructionSelectBloc[ this.StructuraData.getBlocuriCount() > 1 ? 'show' : 'hide' ]();
    }
    
    
});

})();