(function(){
    
_Cheltuieli.FacturaSection = _Cheltuieli.SectionAbstract.extend(
{
    _setupElements_: function()
    {
        this.$extraContent = this.find( '>.scadenta, >.chitante' );
    },
    
    _setupResources_: function()
    {
        this.find( 'input.scadenta-field' )
                .prop( 'readonly', true )
                .datepicker({
                    dateFormat: 'DD, d MM yy'
                });
        
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.find( 'input[name=_f]' ).on( 'change', function()
        {
            var selectedValue = $(this).val() - 0;
            
            if( selectedValue )
            {
                self._showExtraContent();
            }
            else
            {
                self._hideExtraContent();
            }
            
            // self._triggerCompleted();
        });
        
    },
    
    _init_: function()
    {
        this._hideExtraContent();
    },
    
    _showExtraContent: function()
    {
        this.$extraContent.appendTo( this.getContainer() );
    },
    
    _hideExtraContent: function()
    {
        this.$extraContent.detach();
    }
    
});


})();