(function(){

_Cheltuieli.SpatiiIndividualeSection = _Components.Container.extend(
{
    _setup_: function()
    {
        this.values = null;
    },
    
    _setupElements_: function()
    {
        this.$instruction = this.find( '>.instructions' );
        this.$valuesSummary = this.find( '>.section-content' );
        this.$summaryText = this.$valuesSummary.find( '.summary' );
        
        this.$distribuireButtons = this.find( '.distribuire-spatii-button' );
    },
    
    _setupResources_: function()
    {
        this.SpatiiDialog = new _Cheltuieli.SpatiiSelectionEachDialog( $( '#spatii-individuale-dialog' ) );
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.$distribuireButtons.on( 'click', function()
        {
            self._openDialog();
        });

        this.SpatiiDialog.addHandler( 'close', function( event, selection )
        {
            self._setValues( selection );
        });
    },
    
    _init_: function()
    {
        this._hideSummary();
    },
    
    getValues: function()
    {
        return this.values;
    },
    
    fillInValues: function()
    {
        this._openDialog();
    },
    
    _openDialog: function()
    {
        this.SpatiiDialog.open({
            values: this.values
        });
    },
    
    _hideSummary: function()
    {
        this.$valuesSummary.hide();
        this.$instruction.show();
    },
    
    _showSummary: function( values )
    {
        this.$valuesSummary.show();
        this.$instruction.hide();
        
        if( values !== null && Object.size( values ) )
        {
            var summaryText = this._generateSummaryText( values );
            
            this.$summaryText.text( summaryText );
        }
    },
    
    _setValues: function( values )
    {
        if( values === null )
        {
            if( this.values === null )
            {
                this._hideSummary();
            }
            else
            {
                this._showSummary();
            }
        }
        else if( Object.size( values ) )
        {
            this._showSummary( values );

            this.values = values;
        }
    },
    
    _generateSummaryText: function( values )
    {
        var text = 'Suma este distribuita la {spatii}.'.assign({
            spatii: String.pluralize( Object.size( values ), 'apartament', 'apartamente' )
        });
        
        return text;
    }
    
});

})();