(function(){
    
_Spatii.ActionsPanel = _Components.Container.extend(
{
    _setup_: function()
    {
        if( !this._options.Module )
        {
            throw new Error( '"Module" option is not set.' );
        }
    },
    
    _setupResources_: function()
    {
        this.Module = this._options.Module;
    },
    
    _setupActions_: function()
    {
        var self = this;
        
        this.bind( 'click', '.action-item', function(){
            var $item = $(this),
                contentPanelId = $item.data( 'action' );
            
            self.Module.displayContent( contentPanelId );
        });
    },
    
    _init_: function()
    {
        
    }
    
});

})();