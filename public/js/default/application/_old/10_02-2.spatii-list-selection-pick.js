(function(){
        
_Spatii.SpatiiListSelectionPick = _Spatii.SpatiiListSelection.extend(
{
    _setupActions_: function()
    {
        var self = this;
        
        this.find( '.select-all-button' ).on( 'click', function()
        {
            var $rows = self._filterVisibleRows( self._getSpatiuRows() );

            self._selectRow( $rows );
            self._triggerChange();
        });

        this.find( '.deselect-all-button' ).on( 'click', function()
        {
            var $rows = self._filterVisibleRows( self._getSpatiuRows() );

            self._deselectRow( $rows );
            self._triggerChange();
        });

        this.bind( 'click', '.spatiu-row', function()
        {
            var $row = $(this);

            if( self._isRowSelected( $row ) )
            {
                self._deselectRow( $row );
            }
            else
            {
                self._selectRow( $row );
            }
            
            self._triggerChange();
        });

        this.$listContainer.on( 'mouseenter', '.etaj-cell', function()
        {
            var $etajCell = $(this),
                $etajContainer = self._getEtajContainerOfElement( $etajCell );

            $etajContainer.addClass( 'hover' );
        });

        this.$listContainer.on( 'mouseleave', '.etaj-cell', function()
        {
            var $etajCell = $(this),
                $etajContainer = self._getEtajContainerOfElement( $etajCell );

            $etajContainer.removeClass( 'hover' );
        });

        this.$listContainer.on( 'click', '.etaj-cell', function()
        {
            var $etajCell = $(this),
                $etajContainer = self._getEtajContainerOfElement( $etajCell ),
                $rows = self._filterVisibileRows( self._getSpatiuRowsInsideContainer( $etajContainer ) ),
                allSelected = true;

            $rows.each( function()
            {
                var $row = $(this);

                if( !self._isRowSelected( $row ) )
                {
                    allSelected = false;

                    return false;
                }
            });

            if( allSelected )
            {
                $rows = $rows.filter( '.selected' );
                self._deselectRow( $rows );
            }
            else
            {
                $rows = $rows.not( '.selected' );
                self._selectRow( $rows );
            }
            
            self._triggerChange();
        });
        
    },
    
    _clearSelection: function()
    {
        this._deselectAllRows();
    },
    
    _selectRow: function( $row )
    {
        $row.addClass( 'selected' );
    },
    
    _deselectRow: function( $row )
    {
        $row.removeClass( 'selected' );
    },
    
    _isRowSelected: function( $row )
    {
        return $row.hasClass( 'selected' );
    },
    
    _deselectAllRows: function()
    {
        this._deselectRow( this._getSpatiuRows() );
    }
    
});

})();