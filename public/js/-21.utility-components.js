var UtilityComponents;

(function(){

var GlobalMessage = BaseComponents.Container.extend(
{
    _defaultTimeout: 3000,
    timer: null,

    show: function( message, hideTimeout )
    {
        var self = this;

        this._setText( message );
        this.$container.show();

        if ( typeof timeout == 'undefined')
        {
            hideTimeout = this._defaultTimeout;
        }
        else
        {
            hideTimeout = parseInt( hideTimeout );
        }

        if ( hideTimeout > 0 )
        {
            this.timer = setTimeout(function(){
                self.hide();
            }, hideTimeout);
        }

    },

    hide: function()
    {
        this.$container.hide();
        clearTimeout( this.timer );
    },

    _setText: function( text )
    {
        this.$container.find('.content').text( text );
    }

})

var WarningDialog = BaseComponents.Dialog.extend(
{
    _title: 'WARNING',

    _setup: function()
    {
        var self = this;
        this.$container.on('click', '.close', function(){
            self.close();
        });

        this._super();

    },
    open: function(content)
    {
        this.$container.find('.content').html(content);

        this._super();
    }

})

var CustomPopup = BaseComponents.Dialog.extend(
{
    $buttonsContainer: $(),
    buttonTemplate: '<input class="button" type="button" value="%label%" />',
    $sourcePopup: $(),

    _setup: function()
    {
        this.$container = this._clonePopup();

        this.$content = this.$container.find('.content');
        this.$buttonsContainer = this.$container.find('.buttons');

        // call parent constructor
        this._super( options );

    },

    _clonePopup: function()
    {
        var $popup = $('#custom-popup').clone(true).removeAttr('id');

        return $popup;
    },

    setContent: function( content )
    {
        this.$content.text( content );
    },

    /**
     * Add buttons and define their actions
     */

    buttons: function( map )
    {
        var self = this;

        self._removeButtons();

        $.each(map, function( key ){
            self._addButton( key, this );
        })
    },

    _removeButtons: function()
    {
        this.$buttonsContainer.empty();
    },

    _addButton: function( label, action )
    {
        var self = this;
        var buttonHtml = this.buttonTemplate.replace( '%label%', label );
        var $button = $( buttonHtml );

        $button.appendTo( this.$buttonsContainer );

        $button.click(function(){
            action.call(self);
            self.close();
        })
    }

})

var Selection = Class.extend(
{
    _highlightClass:    'highlight',
    _collectedClass:    'collected',
    _wordItemSelector:  '.word-item>.text',
    _collectedItemsSelector:	'#collected-items>div.col>.word-item>.text',

    init: function()
    {
        this.$highlighted   = $();
        this._selected      = null;
        var self = this;

        // prevent default action
//            $(document).on('click', self._wordItemSelector, function(event){
//                event.preventDefault();
//            })
//
//            // highlight a word/phrase
//            $(document).on('mousedown touchstart', self._wordItemSelector, function(event){
//                event.preventDefault();
//                var $this = $(this);
//                // right click
//                if (event.which == 3)
//                {
//                // open the 'copy' menu
//                //App.WordRightClickMenu.open($this);
//                }
//
//                // highlight the item
//                self.$highlighted.removeClass(self._highlightClass);
//                self.$highlighted = $this.addClass(self._highlightClass);
//            })

        // disable right click menu
        //            $(document).delegate(self._wordItemSelector, 'contextmenu', function(event){
        //                event.preventDefault();
        //            })

//            var collectEvent = App.getCollectEvent();
//
//            // collect an item
//            $(document).on(collectEvent, self._wordItemSelector+':not(.'+self._collectedClass+')', function(event){
//                if ($(this).closest('.non-collectable').size())
//                {
//                    return;
//                }
//                var $item = self.getHighlighted();
//                if (!$item.parents('#open-project').size())
//                {
//                    var collectParams = self._getCollectParams($item);
//                    $.extend(collectParams, {
//                        item_id:        $item.parent().attr('_wid'),
//                        text:           $item.text()
//                    })
//                    App.Project.collect(collectParams);
//                }
//
//                event.preventDefault();
//            })

    },

    // return the selected text
    get: function()
    {
        var text = '';

        if(window.getSelection)
        {
            this._selected = window.getSelection();
            text = this._selected + '';
        }
        else if(document.getSelection)
        {
            this._selected = document.getSelection();
            text = this._selected + '';
        }
        else if(document.selection)
        {
            text = document.selection.createRange().text;
            this._selected = document.selection.createRange().parentElement();
        }

        return text;
    },

    getAnchor: function()
    {
        return this._selected ? (this._selected.anchorNode ? $(this._selected.anchorNode) : $(this._selected)) : $([]);
    },

    // checks if the selected text belongs to the specified parent
    belongsTo: function($parent)
    {
        if (!this._selected)
        {
            this.get();
        }

        return this.getAnchor().parents().is($parent);
    },

    _getCollectParams: function($item)
    {
        var $parent = $item.parents('.page-content:first');
        var colectParams = {
            ref_item_id:    $parent.find('input[name=ref_item_id_collected]:first').val(),
            type_id:        $parent.find('input[name=type_id_collected]:first').val(),
            display_id:     $parent.find('input[name=display_id_collected]:first').val()
        }

        return colectParams;
    },

    getHighlighted: function()
    {
        var $parents = this.$highlighted.parents();
        // a word is clicked and highlighted
        if ($parents.is('body') && $parents.is('.current-words:visible'))
        {
            var word = $.trim(this.$highlighted.text());
            if (word.length)
            {
                return this.$highlighted;
            }
        }

        return $();
    },

    getWord: function()
    {
        var $word = this.getHighlighted();
        // a word is clicked and highlighted
        if ($word.size())
        {
            return $.trim($word.text());
        }
        // a piece of text is selected
        else
        {
            var selected = this.get();
            if (this.belongsTo('.page-content') && !this.belongsTo('.current-words') && !this.belongsTo('.ignore'))
            {
                return selected;
            }
        }
        return false;
    },


    getSelection: function()
    {
        var Selection = rangy.getSelection();

        var startNode = Selection.anchorNode;
        var endNode = Selection.focusNode;
        var selectionStart = Selection.anchorOffset;
        var selectionEnd = Selection.focusOffset;

        var data = {
            startNode:  startNode,
            endNode:    endNode,
            start:      selectionStart,
            end:        selectionEnd
        };

        return data;
    },

    setSelection: function(startNode, endNode, selectionStart, selectionEnd)
    {
        var Range = rangy.createRange();
        var Selection = rangy.getSelection();
        Range.setStart(startNode, selectionStart);
        Range.setEnd(endNode, selectionEnd);
        Selection.addRange(Range);
    },

    setCollected: function()
    {
        var $item = this.getHighlighted();

        this.highlight($item);
    },

    highlight: function($item)
    {
        //////////////////////////////

        // change style
        $item.add($item.find('.text')).addClass(this._collectedClass);

        // mark the original word item as collected
        $item.parents('.current-words:first').siblings('.word-list').find('[_wid='+$item.parent().attr('_wid')+']>.text').addClass(this._collectedClass);

    },

    unhighlight: function($item)
    {
        // change style
        $item.add($item.find('.text')).removeClass(this._collectedClass);

        // unmark the original word item as collected
        $item.parents('.current-words:first').siblings('.word-list').find('[_wid='+$item.parent().attr('_wid')+']>.text').removeClass(this._collectedClass);

    },

    clearCollected: function()
    {
        var $words = $('.word-item');
        this.unhighlight($words);
    },

    /**
     * Remove any text selection
     */
    clear: function()
    {
        rangy.getSelection().removeAllRanges();
    }

});

////////////////////////////////////////////////////////////////////////////////
$(function(){

    UtilityComponents = {
        GlobalMessage:  new GlobalMessage( $('#global-message') ),
        WarningDialog:  new WarningDialog( $('#warning-dialog'), {
            zIndex:     9991,
            width:      434,
            minHeight:  125
    	})
    }
})

})();
