-- creare cont Asociatie M1B Pascani - Ionita

BEGIN;

-- asociatie
INSERT INTO `asociatie` (`id`, `username`, `working_month`, `status`, `cod_fiscal`, `email`, `denumire`, `adresa`, `localitate`, `judet`, `telefon`, `password`, `salt`, `timestamp`, `activation_code`, `token`)
VALUES (NULL, '25478354', NULL, 1, '25478354', 'm1b@mailinator.com', 'Asociatia de Proprietari M1B', 'Str. Moldovei nr. 12, Bl. M1, Sc. E, parter', 'Pascani', 'Iasi', '0723856695', '59aa7440649359377300a5c031350b518ac28a05', 'eabf724d7201b71a326319ca2521eb09cd5e26f4', '2013-07-14 14:18:43', '783733b46c29314a128175d9a08b6c08303a884a', '080130e46fe8be4cb1832d3aad687a9c5a177a2c');

SET @asociatie_id = LAST_INSERT_ID();

-- bloc
INSERT INTO `bloc` (`id`, `asociatie_id`, `denumire`, `adding_month`, `status`, `created_at`, `updated_at`) VALUES
(NULL, @asociatie_id, 'M1', NULL, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

SET @bloc_id = LAST_INSERT_ID();

-- scara
INSERT INTO `scara` (`id`, `_asociatie_id`, `bloc_id`, `denumire`, `adresa`, `adding_month`, `status`, `timestamp`) VALUES
(NULL, @asociatie_id, @bloc_id, 'B', NULL, NULL, 0, '2013-07-14 14:21:11');

SET @scara_id = LAST_INSERT_ID();

-- spatiu
INSERT INTO `spatiu` (`id`, `_asociatie_id`, `scara_id`, `numar`, `etaj`, `adding_month`, `status`, `timestamp`)
VALUES (NULL, @asociatie_id, @scara_id, '1', 1, NULL, 0, '2013-07-14 14:21:50');

SET @spatiu_id = LAST_INSERT_ID();

INSERT INTO `spatiu` (`id`, `_asociatie_id`, `scara_id`, `numar`, `etaj`, `adding_month`, `status`, `timestamp`) VALUES
(NULL, @asociatie_id, @scara_id, '2', 1, NULL, 0, '2013-07-14 14:23:15'),
(NULL, @asociatie_id, @scara_id, '3', 1, NULL, 0, '2013-07-14 14:23:15'),
(NULL, @asociatie_id, @scara_id, '4', 1, NULL, 0, '2013-07-14 14:24:01'),
(NULL, @asociatie_id, @scara_id, '5', 2, NULL, 0, '2013-07-14 14:24:19'),
(NULL, @asociatie_id, @scara_id, '6', 2, NULL, 0, '2013-07-14 14:24:35'),
(NULL, @asociatie_id, @scara_id, '7', 2, NULL, 0, '2013-07-14 14:24:47'),
(NULL, @asociatie_id, @scara_id, '8', 2, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '9', 3, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '10', 3, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '11', 3, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '12', 3, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '13', 4, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '14', 4, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '15', 4, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '16', 4, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '17', 5, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '18', 5, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '19', 5, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '20', 5, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '21', 6, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '22', 6, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '23', 6, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '24', 6, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '25', 7, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '26', 7, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '27', 7, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '28', 7, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '29', 8, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '30', 8, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '31', 8, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '32', 8, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '33', 9, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '34', 9, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '35', 9, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '36', 9, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '37', 10, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '38', 10, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '39', 10, NULL, 0, '2013-07-14 14:30:57'),
(NULL, @asociatie_id, @scara_id, '40', 10, NULL, 0, '2013-07-14 14:30:57');

-- parametru_spatiu

INSERT INTO `parametru_spatiu` (`id`, `_asociatie_id`, `ref_id`, `parameter`, `value`, `timestamp`) VALUES
(NULL, @asociatie_id, @spatiu_id+(144-144), 'proprietar', 'Loghin Gheorghe', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(144-144), 'nr_pers', '2', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(145-144), 'proprietar', 'Apopei Anica', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(145-144), 'nr_pers', '3', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(146-144), 'proprietar', 'Apostol Gheorghe', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(146-144), 'nr_pers', '2', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(147-144), 'proprietar', 'Iorgu Jan', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(147-144), 'nr_pers', '2', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(148-144), 'proprietar', 'Cricu Romica', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(148-144), 'nr_pers', '1', '2013-07-14 14:33:06'),
(NULL, @asociatie_id, @spatiu_id+(149-144), 'proprietar', 'Romaniuc M.', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(149-144), 'nr_pers', '2', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(150-144), 'proprietar', 'Ionita Neculai', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(150-144), 'nr_pers', '2', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(151-144), 'proprietar', 'Poputoaia Iacob', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(151-144), 'nr_pers', '2', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(152-144), 'proprietar', 'Axinie Armando', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(152-144), 'nr_pers', '4', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(153-144), 'proprietar', 'Iovu Mircea', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(153-144), 'nr_pers', '2', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(154-144), 'proprietar', 'Sandu Vasile', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(154-144), 'nr_pers', '4', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(155-144), 'proprietar', 'Bota Petronela', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(155-144), 'nr_pers', '0', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(156-144), 'proprietar', 'Hutanu Alexandru', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(156-144), 'nr_pers', '3', '2013-07-14 14:35:08'),
(NULL, @asociatie_id, @spatiu_id+(157-144), 'proprietar', 'Dodoaia Constantin', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(157-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(158-144), 'proprietar', 'Olaru Gheorghe', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(158-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(159-144), 'proprietar', 'Hârțan Aurel', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(159-144), 'nr_pers', '0', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(160-144), 'proprietar', 'Botea Viorel', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(160-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(161-144), 'proprietar', 'Rusu Dan', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(161-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(162-144), 'proprietar', 'Ghiojan Rodica', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(162-144), 'nr_pers', '0', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(163-144), 'proprietar', 'Anghelina Ilie', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(163-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(164-144), 'proprietar', 'Costea Marin', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(164-144), 'nr_pers', '1', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(165-144), 'proprietar', 'Anton Ana', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(165-144), 'nr_pers', '0', '2013-07-14 15:00:24'),
(NULL, @asociatie_id, @spatiu_id+(166-144), 'proprietar', 'Măslincă Maria', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(166-144), 'nr_pers', '1', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(167-144), 'proprietar', 'Panainte Alexandru', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(167-144), 'nr_pers', '5', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(168-144), 'proprietar', 'Coman Cornelia', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(168-144), 'nr_pers', '1', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(169-144), 'proprietar', 'Dumitrașcu Maria', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(169-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(170-144), 'proprietar', 'Axinte Nelu', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(170-144), 'nr_pers', '3', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(171-144), 'proprietar', 'Ciopragra Mariana', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(171-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(172-144), 'proprietar', 'Pintilie Alexandru', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(172-144), 'nr_pers', '2', '2013-07-14 14:37:57'),
(NULL, @asociatie_id, @spatiu_id+(173-144), 'proprietar', 'Jeder Cheorghe', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(173-144), 'nr_pers', '2', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(174-144), 'proprietar', 'Vasile Liviu', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(174-144), 'nr_pers', '3', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(175-144), 'proprietar', 'Buburuzan Pavel', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(175-144), 'nr_pers', '2', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(176-144), 'proprietar', 'Boboc Mihai', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(176-144), 'nr_pers', '4', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(177-144), 'proprietar', 'Dutcă Dan Vasile', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(177-144), 'nr_pers', '3', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(178-144), 'proprietar', 'Simion Ioan', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(178-144), 'nr_pers', '2', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(179-144), 'proprietar', 'Jardă Maria', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(179-144), 'nr_pers', '2', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(180-144), 'proprietar', 'Păuleț Dumitru', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(180-144), 'nr_pers', '3', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(181-144), 'proprietar', 'Tansanu Magdalena', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(181-144), 'nr_pers', '0', '2013-07-14 15:00:24'),
(NULL, @asociatie_id, @spatiu_id+(182-144), 'proprietar', 'Turcu Constantin', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(182-144), 'nr_pers', '0', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(183-144), 'proprietar', 'Rotaru Constantin', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(183-144), 'nr_pers', '4', '2013-07-14 14:39:46'),
(NULL, @asociatie_id, @spatiu_id+(144-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(145-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(146-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(147-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(148-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(149-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(150-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(151-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(152-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(153-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(154-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(155-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(156-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(157-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(158-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(159-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(160-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(161-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(162-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(163-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(164-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(165-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(166-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(167-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(168-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(169-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(170-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(171-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(172-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(173-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(174-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(175-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(176-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(177-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(178-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(179-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(180-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(181-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(182-144), 'suprafata', '52.67', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(183-144), 'suprafata', '68.22', '2013-07-14 14:42:30'),
(NULL, @asociatie_id, @spatiu_id+(151-144), 'restante', '30', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(155-144), 'restante', '112.5', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(157-144), 'restante', '1', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(159-144), 'restante', '180', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(167-144), 'restante', '1643.5', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(170-144), 'restante', '54.5', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(172-144), 'restante', '68', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(174-144), 'restante', '24', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(176-144), 'restante', '174.5', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(180-144), 'restante', '91.5', '2013-07-14 14:59:01'),
(NULL, @asociatie_id, @spatiu_id+(181-144), 'restante', '254', '2013-07-14 14:59:01');

-- reducere energie electrica (lift)
INSERT INTO `reducere_repartizare` (`id`, `denumire`, `coloana_id`, `asociatie_id`, `status`, `timestamp`) VALUES
(NULL, 'Reducere energie electrica lift etaj 1', 3, @asociatie_id, 0, '0000-00-00 00:00:00');

SET @reducere_id = LAST_INSERT_ID();

INSERT INTO `parametru_reducere_repartizare` (`id`, `_asociatie_id`, `ref_id`, `parameter`, `value`) VALUES
(NULL, @asociatie_id, @reducere_id, 'reducere', '50'),
(NULL, @asociatie_id, @reducere_id, 'spatii', CONCAT('{"', (@spatiu_id+0), '":"*","', (@spatiu_id+1), '":"*","', (@spatiu_id+2), '":"*","', (@spatiu_id+3), '":"*"}'));

COMMIT;