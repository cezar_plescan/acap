CREATE TABLE IF NOT EXISTS `incasari` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_asociatie_id` int(11) NOT NULL,
  `spatiu_id` int(11) NOT NULL,
  `valoare` decimal(10,4) NOT NULL,
  `coloana_id` smallint(6) NOT NULL,
  `data` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `_asociatie_id` (`_asociatie_id`)
) ENGINE = InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

ALTER TABLE `coloana` ADD `alt_label` VARCHAR( 30 ) NOT NULL AFTER `label` ,
ADD `for_incasari` TINYINT NOT NULL AFTER `alt_label` ;

ALTER TABLE `coloana` ADD INDEX ( `for_incasari` ) ;

UPDATE `coloana` SET `alt_label` = 'Întreţinere' WHERE `coloana`.`id` =8 ;

UPDATE `coloana` SET `for_incasari` = '1' WHERE `coloana`.`id` = 8;
UPDATE `coloana` SET `for_incasari` = '1' WHERE `coloana`.`id` = 11;
UPDATE `coloana` SET `for_incasari` = '1' WHERE `coloana`.`id` = 12;
UPDATE `coloana` SET `for_incasari` = '1' WHERE `coloana`.`id` = 13;

ALTER TABLE `incasari` ADD `month` MEDIUMINT NOT NULL AFTER `_asociatie_id`;

ALTER TABLE `acap`.`incasari` ADD INDEX `is_asociatie_id` ( `id` , `_asociatie_id` );
ALTER TABLE `incasari` ADD INDEX `status` ( `_asociatie_id` , `status` );

CREATE TABLE IF NOT EXISTS `contributie_spatiu` (
`id` int( 11 ) NOT NULL AUTO_INCREMENT ,
`_asociatie_id` int( 11 ) NOT NULL ,
`month` mediumint( 9 ) NOT NULL,
`spatiu_id` int( 11 ) NOT NULL ,
`coloana_id` smallint( 6 ) NOT NULL ,
`value` DECIMAL( 10, 4 ) NOT NULL ,
`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
PRIMARY KEY ( `id` )
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

