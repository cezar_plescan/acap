<?php

$jsPath = realpath( '../public/js' );
$jsContent = ' ';

$resources = scandir( $jsPath );
if ($resources)
{
    // keep only children files
    foreach ($resources as $i => $resource)
    {
        //if (!$this->isValid($resource, $baseDirPath))
        if (is_file($jsPath. DIRECTORY_SEPARATOR . $resource) && $resource[0] != '-')
        {
            $jsContent .= "\n";

            $jsContent .= file_get_contents( $jsPath .DIRECTORY_SEPARATOR . $resource ) ;
        }
    }

    $moduleDir = 'default';

    $moduleDirPath = realpath($jsPath . DIRECTORY_SEPARATOR . $moduleDir);
    $mResources = scandir($moduleDirPath);
    if ($mResources)
    {
        foreach ($mResources as $resource)
        {
            if (is_file($moduleDirPath. DIRECTORY_SEPARATOR . $resource) && $resource[0] != '-')
            {
                $jsContent .= "\n";

                $jsContent .= file_get_contents( $moduleDirPath .DIRECTORY_SEPARATOR . $resource ) ;
            }
        }
    }
}

file_put_contents( $jsPath . DIRECTORY_SEPARATOR . '-resources.js', $jsContent );