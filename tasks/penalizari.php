<?php

require 'penalizari_valori_2.php';

//-----

$interval_legal = $interval_gratie + $interval_scadenta;

$penalizari_debite = [];

echo "Penalizarile se aplica pentru fiecare debit, daca acesta a fost achitat cu intarziere sau nu a fost achitat inca.\n";
echo "Se ordoneaza cronologic debitele si creditele.\n";
echo "Penalizarile se calculeaza pana la data $data_curenta\n";

$avans_credit = false;

foreach( $debit as $data_debit => $valoare_debit )
{
    echo "\n";
    echo "(+) Debit din $data_debit: $valoare_debit\n";
    
    $penalizari_debite[ $data_debit ] = 0;
    $plafonare_penalizare = false;
    $penalizari_plafonate = 0;
    
    foreach( $credit as $data_credit => $valoare_credit )
    {
        echo "(-) Chitanta din $data_credit: ";
        echo ( $avans_credit ? "rest de " : "" ) . "$valoare_credit\n";
        
        // verific daca debitul a fost acoperit prin credit
        $rest = $valoare_debit - $valoare_credit;

        // daca debitul a fost achitat integral:
        if( abs( $rest ) < 1e-10 )
        {
            $avans_credit = false;
            
            // calculez eventualele penalizari pentru debitul curent
            $penalizari = calculeaza_penalizari( $valoare_debit, $data_debit, $data_credit );
            $penalizari_debite[ $data_debit ] += $penalizari;
            
            // se scoate din calcul creditul curent
            unset( $credit[ $data_credit ] );
            
            echo "> debit achitat integral\n";
            
            if( $penalizari )
            {
                $intarziere = diferenta_zile( $data_credit, $data_debit ) - $interval_scadenta;
                
                echo "> debitul este achitat cu intarziere de $intarziere zile de la data scadenta\n";
                echo "> se calculeaza penalizari in valoare de $penalizari";
                
            }
            else
            {
                echo "> pentru suma achitata nu se aplica penalizari deoarece plata se incadreaza in termenul legal de $interval_legal zile\n";
            }
            
            // se continua cu urmatorul debit
            break;
        }
        // daca debitul a fost achitat partial
        elseif( $rest > 0 )
        {
            $avans_credit = false;
            
            // se calculeaza penalizarile doar pentru suma achitata
            $penalizari = calculeaza_penalizari( $valoare_credit, $data_debit, $data_credit );
            $penalizari_debite[ $data_debit ] += $penalizari;
            
            $vb = $valoare_debit;
            
            // se scade din debit suma achitata
            $valoare_debit = $valoare_debit - $valoare_credit;

            // se scoate din calcul creditul curent
            unset( $credit[ $data_credit ] );

            echo "> debit achitat partial\n";
            
            if( $penalizari )
            {
                $intarziere = diferenta_zile( $data_credit, $data_debit ) - $interval_scadenta;
                
                echo "> debitul este achitat cu intarziere de $intarziere zile de la data scadenta\n";
                echo "> pentru suma achitata se calculeaza penalizari in valoare de $penalizari; pentru restul de plata penalizarile se calculeaza ulterior\n";
                
            }
            else
            {
                echo "> pentru suma achitata nu se aplica penalizari deoarece plata se incadreaza in termenul legal de $interval_legal zile\n";
            }
            
            echo "> rest de plata: $vb - $valoare_credit = $valoare_debit\n";
            
            // se continua cu urmatorul credit, pentru a acoperi debitul curent
            continue;
        }
        // daca debitul a fost achitat integral si s-a achitat in avans
        else
        {
            // se scade din creditul curent valoarea debitului
            $credit[ $data_credit ] = $valoare_credit - $valoare_debit;
            
            $avans_credit = true;
            
            echo "> debit achitat integral\n";
            echo "> mai raman $valoare_credit - $valoare_debit = {$credit[ $data_credit ]} cu care se vor achita debitele urmatoare\n";
            
            // calculez eventualele penalizari pentru debitul curent
            $penalizari = calculeaza_penalizari( $valoare_debit, $data_debit, $data_credit );
            $penalizari_debite[ $data_debit ] += $penalizari;
            
            if( $penalizari )
            {
                $intarziere = diferenta_zile( $data_credit, $data_debit ) - $interval_scadenta;
                
                echo "> debitul este achitat cu intarziere de $intarziere zile de la data scadenta\n";
                echo "> se calculeaza penalizari in valoare de $valoare_debit (suma restanta) * $procent_penalizare% (procent penalizare pe zi) * $intarziere (zile de intarziere) = $penalizari\n";
            }
            else
            {
                echo "> pentru suma achitata nu se aplica penalizari deoarece plata se incadreaza in termenul legal de $interval_legal zile\n";
            }
            
            // se continua cu urmatorul debit
            break;
        }
    }
    
    if( count( $credit ) == 0 )
    {
        $penalizari = calculeaza_penalizari( $valoare_debit, $data_debit, $data_curenta );
        @$penalizari_debite[ $data_debit ] += $penalizari;
        
        if( $penalizari )
        {
            $intarziere = diferenta_zile( $data_curenta, $data_debit ) - $interval_scadenta;

            echo "> debitul nu este achitat\n";
            echo "> se calculeaza penalizari in valoare de $valoare_debit (suma restanta) * $procent_penalizare% (procent penalizare pe zi) * $intarziere (zile de intarziere) = $penalizari\n";
        }
        
    }
    
    echo "\n> penalizari pentru debitul curent: {$penalizari_debite[ $data_debit ]}\n";
    
    // verificare daca suma penalizarilor nu depaseste suma la care s-au aplicat
    if( $penalizari_debite[ $data_debit ] > $debit[ $data_debit ] )
    {
        $penalizari_debite[ $data_debit ] = $debit[ $data_debit ];
        
        echo "> din cauza ca suma penalizarilor nu poate depasi suma cotei restante la care s-au aplicat, penalizarile se plafoneaza la {$penalizari_debite[ $data_debit ]}\n";
    }
    
    $total_penalizari = array_sum( $penalizari_debite );
    echo ">> total penalizari: $total_penalizari\n";
}

$total_penalizari = array_sum( $penalizari_debite );

echo "\nTotal penalizari [$data_curenta]: $total_penalizari";

//------------------------------------------------------------------------------
echo "\n\n";
//------------------------------------------------------------------------------

function calculeaza_penalizari( $debit, $data_debit, $data_curenta )
{
    global $interval_gratie, $interval_scadenta, $procent_penalizare;
    
    $penalizari = 0;
    
    $intarziere = diferenta_zile( $data_curenta, $data_debit );
    
    if( $intarziere > ( $interval_gratie + $interval_scadenta ) )
    {
        $penalizari = $debit * $procent_penalizare / 100 * ( $intarziere - $interval_scadenta );
    }
    
    return $penalizari;
}

function diferenta_zile( $date_1, $date_2 )
{
    $date_1 = strtotime( $date_1 );
    $date_2 = strtotime( $date_2 );

    $diff = round( ($date_1 - $date_2) / 86400 );

    return $diff;
}


