<?php

$cssPath = realpath( '../public/css' );
$cssContent = ' ';

$resources = scandir( $cssPath );
if ($resources)
{
    // keep only children files
    foreach ($resources as $i => $resource)
    {
        //if (!$this->isValid($resource, $baseDirPath))
        if (is_file($cssPath. DIRECTORY_SEPARATOR . $resource) && $resource[0] != '-')
        {
            $cssContent .= "\n";

            $cssContent .= file_get_contents( $cssPath .DIRECTORY_SEPARATOR . $resource ) ;
        }
    }

    $moduleDir = 'default';

    $moduleDirPath = realpath($cssPath . DIRECTORY_SEPARATOR . $moduleDir);
    $mResources = scandir($moduleDirPath);
    if ($mResources)
    {
        foreach ($mResources as $resource)
        {
            if (is_file($moduleDirPath. DIRECTORY_SEPARATOR . $resource) && $resource[0] != '-')
            {
                $cssContent .= "\n";

                $cssContent .= file_get_contents( $moduleDirPath .DIRECTORY_SEPARATOR . $resource ) ;
            }
        }
    }
}

file_put_contents( $cssPath . DIRECTORY_SEPARATOR . '-resources.css', $cssContent );