<?php

class App_Component_Configuration_Validator_FirstMonth extends Lib_Validator_Abstract
{
    const ERR_INVALID   = 'invalid';
    const ERR_MONTH     = 'month';
    const ERR_YEAR      = 'year';
    const ERR_FUTURE    = 'future';
    
    protected function init()
    {
        $this->addMessageTemplates([
            self::ERR_INVALID   => 'Format incorect al parametrilor.',
            self::ERR_MONTH     => 'Luna nu este specificată sau este incorectă.',
            self::ERR_YEAR      => 'Anul nu este specificat sau este incorect.',
            self::ERR_FUTURE    => 'Nu este permisă alegerea unei luni din viitor.',
        ]);
    }
    
    protected function validate( $value )
    {
        if( !is_array( $value ) )
        {
            $this->setError( self::ERR_INVALID );
        }
        
        $month = intval( @$value[ App_Component_Configuratie_Object::ARG_MONTH ] );
        if( $month < 1 || $month > 12 )
        {
            $this->setError( self::ERR_MONTH );
        }
        
        $year = intval( @$value[ App_Component_Configuratie_Object::ARG_YEAR ] );
        $currentYear = date( 'Y' );
        
        if( $year < $currentYear - 3 || $year > $currentYear )
        {
            $this->setError( self::ERR_YEAR );
        }
        
        $currentMonth = date( 'n' );
        if( $year == $currentYear && $month > $currentMonth )
        {
            $this->setError( self::ERR_FUTURE );
        }
        
        $value = [
            App_Component_Configuratie_Object::ARG_MONTH   => $month,
            App_Component_Configuratie_Object::ARG_YEAR    => $year,
        ];
        
        return $value;
    }
    
}