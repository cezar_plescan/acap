<?php

class App_Component_Configuratie_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'configuratie';
    
    static protected $instance;
    
    public function isUsedByCheltuieli( array $parameters )
    {
        $categorie      = Lib_Tools::checkInputDataKeys( $parameters, 'categorie' );
        $month          = Lib_Tools::checkInputDataKeys( $parameters, 'month' );
        $ascociatieId   = Lib_Tools::checkInputDataKeys( $parameters, 'asociatie_id' );
        $parametru      = Lib_Tools::checkInputDataKeys( $parameters, 'parametru' );
        $configuratieId = Lib_Tools::checkInputDataKeys( $parameters, 'configuratie_id' );
        
        $cheltuialaTable            = App_Component_Cheltuiala_Db::getInstance()->quoteTableName();
        $cheltuialaTipTable         = App_Component_CheltuialaTip_Db::getInstance()->quoteTableName();
        $cheltuialaParametruTable   = App_Component_CheltuialaParametru_Db::getInstance()->quoteTableName();
        
        $parameterValueColumn       = App_Component_CheltuialaParametru_Db::getInstance()->quoteColumns( App_Component_CheltuialaParametru_Object::PROP_VALUE );
        
        $cheltuiala_tipIdColumn     = App_Component_Cheltuiala_Db::getInstance()->quoteColumns( App_Component_Cheltuiala_Object::PROP_TIP );
        $cheltuialaTip_idColumn     = App_Component_CheltuialaTip_Db::getInstance()->quoteColumns( App_Component_CheltuialaTip_Object::PROP_ID );
        $cheltuiala_idColumn        = App_Component_Cheltuiala_Db::getInstance()->quoteColumns( App_Component_Cheltuiala_Object::PROP_ID );
        $cheltuialaParametru_refIdColumn = App_Component_CheltuialaParametru_Db::getInstance()->quoteColumns( App_Component_CheltuialaParametru_Object::PROP_REF_ID );
        
        $conditions = $this->whereAnd([
            $this->whereEqual( $categorie, App_Component_CheltuialaTip_Object::PROP_TIP, App_Component_CheltuialaTip_Db::TABLE ),
            $this->whereEqual( $month, App_Component_Cheltuiala_Object::PROP_MONTH, App_Component_Cheltuiala_Db::TABLE ),
            $this->whereEqual( $ascociatieId, App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID, App_Component_Cheltuiala_Db::TABLE ),
            $this->whereEqual( App_Component_Cheltuiala_Object::VAL_STATUS_ACTIVE, App_Component_Cheltuiala_Object::PROP_STATUS, App_Component_Cheltuiala_Db::TABLE ),
            $this->whereEqual( $parametru, App_Component_CheltuialaParametru_Object::PROP_PARAMETRU, App_Component_CheltuialaParametru_Db::TABLE ),
        ]);
        
        $sql = "
            SELECT $parameterValueColumn from $cheltuialaParametruTable
            LEFT JOIN $cheltuialaTable ON $cheltuiala_idColumn = $cheltuialaParametru_refIdColumn
            LEFT JOIN $cheltuialaTipTable ON $cheltuiala_tipIdColumn = $cheltuialaTip_idColumn
            WHERE $conditions";
        
        $results = $this->queryAndFetchAll( $sql );
        $isUsed = FALSE;
            
        foreach( $results as $row )
        {
            $rawValue = $row[ App_Component_CheltuialaParametru_Object::PROP_VALUE ];
            $parameterValue = json_decode( $rawValue, TRUE );
            
            if( $parameterValue == $configuratieId )
            {
                $isUsed = TRUE;
                break;
            }
        }
        
        return $isUsed;
    }
        
}
