<?php

class App_Component_Configuratie_Object extends Lib_Component_ORM_Object
{
    const PROP_ASOCIATIE_ID     = 'asociatie_id';
    const PROP_PARAMETRU_ID     = 'parametru_id';
    const PROP_VALUE            = 'value';
    const PROP_STATUS           = 'status';
    
    const PARAMETRU             = 'parametru';
    const VALUES                = 'values';
    
    const VAL_STATUS_ACTIVE     = 0;
    const VAL_STATUS_DELETED    = 1;
    
    public static function getActiveStatusCriteria() 
    {
        $criteria = [
            self::PROP_STATUS => self::VAL_STATUS_ACTIVE,
        ];
        
        return $criteria;
    }
    
    
    // -------------------------------------------------------------------------
    
//    const ARG_MONTH     = 'month';
//    const ARG_YEAR      = 'year';
//    
//    const INIT_STEP     = 'initialization_step';
//    const FREEZED_PAGE  = 'freezed_page';
//    const FIRST_MONTH   = 'first_month';
//    
//    protected $Asociatie;
//    
//    public function __construct( App_Component_Asociatie_Object $Asociatie )
//    {
//        $this->Asociatie = $Asociatie;
//    }
//    
//    public function getData()
//    {
//        $config = $this->Asociatie->getParametri();
//        
//        return $config;
//    }
//    
//    public function nextInitStep()
//    {
//        $currentStep = $this->Asociatie->getValoareParametru( self::INIT_STEP );
//        
//        if( $currentStep === NULL )
//        {
//            $nextStep = 0;
//        }
//        else
//        {
//            $nextStep = $currentStep + 1;
//        }
//        
//        App_Component_AsociatieParametru_Factory::getInstance()->updateParameter( $this->Asociatie->getId(), self::INIT_STEP, $nextStep );
//        
//        return $nextStep;
//    }
//    
//    public function setFreezedPage( $page )
//    {
//        App_Component_AsociatieParametru_Factory::getInstance()->updateParameter( $this->Asociatie->getId(), self::FREEZED_PAGE, $page );
//    }
//    
//    public function clearFreezedPage()
//    {
//        App_Component_AsociatieParametru_Factory::getInstance()->updateParameter( $this->Asociatie->getId(), self::FREEZED_PAGE, NULL );
//    }
//    
//    public function setFirstMonth( array $monthYear )
//    {
//        $monthId = $monthYear[ self::ARG_YEAR ] . str_pad( $monthYear[ self::ARG_MONTH ], 2, '0', STR_PAD_LEFT );
//                
//        App_Component_AsociatieParametru_Factory::getInstance()->updateParameter( $this->Asociatie->getId(), self::FIRST_MONTH, $monthId );
//    }
    
}
