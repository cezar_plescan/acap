<?php

class App_Component_Configuratie_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    public function getData( $asociatieId, $categorie = NULL, $parametru = NULL, $configuratieId = NULL )
    {
        $cacheId = 'data-' . $asociatieId;
        
        if( $this->notCache( $cacheId ) )
        {
            $data = [];

            $columns = [
                App_Component_Configuratie_Object::PROP_ID,
                App_Component_Configuratie_Object::PROP_PARAMETRU_ID,
                App_Component_Configuratie_Object::PROP_VALUE,
            ];

            $conditions = [
                App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                App_Component_Configuratie_Object::PROP_STATUS          => App_Component_Configuratie_Object::VAL_STATUS_ACTIVE,
            ];

            $rawData = $this->retrieveData( $columns, $conditions );
            
            $parametri  = App_Component_ConfiguratieParametru_Factory::getInstance()->getParametri();

            foreach( $rawData as $row )
            {
                $parametruData = $parametri[ $row[ App_Component_Configuratie_Object::PROP_PARAMETRU_ID ] ];
                
                $categ = $parametruData[ App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID ];
                $param = $parametruData[ App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE ];

                $rawValue = $row[ App_Component_Configuratie_Object::PROP_VALUE ];
                $value = json_decode( $rawValue, TRUE );
                
                $parametruValues = [
                    App_Component_Configuratie_Object::PROP_ID => $row[ App_Component_Configuratie_Object::PROP_ID ],
                ];
                
                if( is_array( $value ) )
                {
                    $parametruValues += $value;
                }
                else
                {
                    $parametruValues[ App_Component_Configuratie_Object::PROP_VALUE ] = $value;
                }
                
                if( App_Component_ConfiguratieParametru_Factory::getInstance()->isParameterAList( $categ, $param ) )
                {
                    $data[ $categ ][ $param ][] = $parametruValues;
                }
                else
                {
                    $data[ $categ ][ $param ] = $parametruValues;
                }
            }
            
            $this->writeCache( $cacheId, $data );
        }
        
        $configuratie = $this->readCache( $cacheId );
        
        $returnData = NULL;
        
        if( $categorie === NULL )
        {
            $returnData = $configuratie;
        }
        else
        {
            $configuratieCategorie = isset( $configuratie[ $categorie ] ) ?
                $configuratie[ $categorie ] :
                [];
            
            if( $parametru === NULL )
            {
                $returnData = $configuratieCategorie;
            }
            else
            {
                if( $configuratieId === NULL )
                {
                    $returnData = @$configuratieCategorie[ $parametru ];
                }
                else if( is_array( $configuratieCategorie[ $parametru ] ) )
                {
                    foreach( $configuratieCategorie[ $parametru ] as $parametruData )
                    {
                        if( !is_array( $parametruData ) )
                        {
                            break;
                        }
                        else if( $parametruData[ App_Component_Configuratie_Object::PROP_ID ] == $configuratieId )
                        {
                            $returnData = $parametruData;
                            
                            break;
                        }
                    }
                }
            }
        }
        
        return $returnData;
    }
    
    public function getParameterData( $asociatieId, $parametruId )
    {
        $data = [];
        
        $columns = [
            App_Component_Configuratie_Object::PROP_ID,
            App_Component_Configuratie_Object::PROP_PARAMETRU_ID,
            App_Component_Configuratie_Object::PROP_VALUE,
        ];

        $conditions = [
            App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $asociatieId,
            App_Component_Configuratie_Object::PROP_PARAMETRU_ID    => $parametruId,
            App_Component_Configuratie_Object::PROP_STATUS          => App_Component_Configuratie_Object::VAL_STATUS_ACTIVE,
        ];

        $rawData = $this->retrieveData( $columns, $conditions );

        foreach( $rawData as $row )
        {
            $rawValue = $row[ App_Component_Configuratie_Object::PROP_VALUE ];
            $value = json_decode( $rawValue, TRUE );

            $parametruValues = [
                App_Component_Configuratie_Object::PROP_ID => $row[ App_Component_Configuratie_Object::PROP_ID ],
            ];

            if( is_array( $value ) )
            {
                $parametruValues += $value;
            }
            else
            {
                $parametruValues[ App_Component_Configuratie_Object::PROP_VALUE ] = $value;
            }

            if( App_Component_ConfiguratieParametru_Factory::getInstance()->isParameterAList( $parametruId ) )
            {
                $data[] = $parametruValues;
            }
            else
            {
                $data = $parametruValues;
                
                break;
            }
        }

        return $data;
    }
    
    public function getAllEntriesCount( $asociatieId, $categorie, $parametru )
    {
        $categorieId = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieId( $categorie );
        $parametruId = App_Component_ConfiguratieParametru_Factory::getInstance()->getParametruId( $parametru, $categorieId );
        
        $conditions = [
            App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $asociatieId,
            App_Component_Configuratie_Object::PROP_PARAMETRU_ID    => $parametruId,
        ];

        $count = $this->count( $conditions );
        
        return $count;
    }
    
    /**
     * 
     * @param array $inputData [ asociatie_id, parametru_id, value ]
     * @return array
     */
    public function add( array $inputData )
    {
        Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::PROP_ASOCIATIE_ID );
        Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::PROP_PARAMETRU_ID );
        Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::PROP_VALUE );
        
        $configuratieData = Lib_Tools::filterArray( $inputData, [
            App_Component_Configuratie_Object::PROP_ASOCIATIE_ID,
            App_Component_Configuratie_Object::PROP_PARAMETRU_ID,
            App_Component_Configuratie_Object::PROP_VALUE,
        ]);
        
        $configuratieValue = $inputData[ App_Component_Configuratie_Object::PROP_VALUE ];
        
        $configuratieData[ App_Component_Configuratie_Object::PROP_VALUE ] = json_encode( $configuratieValue );
        
        $configuratieId = $this->getDb()->insert( $configuratieData );
        
        $returnData = [
            App_Component_Configuratie_Object::PROP_ID => $configuratieId,
        ];
        
        if( is_array( $configuratieValue ) )
        {
            $returnData += $configuratieValue;
        }
        else
        {
            $returnData[ App_Component_Configuratie_Object::PROP_VALUE ] = $configuratieValue;
        }
        
        return $returnData;
    }
    
    /**
     * 
     * @param array $inputData [ id, value ]
     * @return array
     */
    public function edit( array $inputData )
    {
        $id         = Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::PROP_ID );
        $newValue   = Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::PROP_VALUE );
        
        $newValueEncoded = json_encode( $newValue );
        
        $this->getDb()->update([
            App_Component_Configuratie_Object::PROP_VALUE => $newValueEncoded,
        ], [
            App_Component_Configuratie_Object::PROP_ID => $id
        ]);
        
        $returnData = [
            App_Component_Configuratie_Object::PROP_ID => $id,
        ];
        
        if( is_array( $newValue ) )
        {
            $returnData += $newValue;
        }
        else
        {
            $returnData[ App_Component_Configuratie_Object::PROP_VALUE ] = $newValue;
        }
        
        return $returnData;
    }
    
    public function remove( array $configuratieData )
    {
        $configuratieId = Lib_Tools::checkInputDataKeys( $configuratieData, App_Component_Configuratie_Object::PROP_ID );
        
        $this->getDb()->update([
            App_Component_Configuratie_Object::PROP_STATUS => App_Component_Configuratie_Object::VAL_STATUS_DELETED,
        ], [
            App_Component_Configuratie_Object::PROP_ID => $configuratieId,
        ]);
    }
    
    public function update( array $inputData )
    {
        $asociatieId    = Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::PROP_ASOCIATIE_ID );
        $parametruId    = Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::PROP_PARAMETRU_ID );
        $values         = Lib_Tools::checkInputDataKeys( $inputData, App_Component_Configuratie_Object::VALUES );
        
        if( !is_array( $values ) )
        {
            throw new InvalidArgumentException( '"values" parameter is not an array' );
        }
        
        $addValues = isset( $values[ 'add' ] ) ? $values[ 'add' ] : [] ;
        $removeValues = isset( $values[ 'remove' ] ) ? $values[ 'remove' ] : [] ;
        
        if( !is_array( $addValues ) )
        {
            throw new InvalidArgumentException( '"addValues" parameter is not an array' );
        }
        
        if( !is_array( $removeValues ) )
        {
            throw new InvalidArgumentException( '"removeValues" parameter is not an array' );
        }
        
        // remove entries
        
        foreach( $removeValues as $configuratieId )
        {
            $configuratieData = [
                App_Component_Configuratie_Object::PROP_ID => $configuratieId,
            ];
            
            $this->remove( $configuratieData );
        }
        
        // add new entries
        
        foreach( $addValues as $parameterValue )
        {
            $configuratieData = [
                App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                App_Component_Configuratie_Object::PROP_PARAMETRU_ID    => $parametruId,
                App_Component_Configuratie_Object::PROP_VALUE           => $parameterValue,
            ];
            
            $this->add( $configuratieData );
        }
        
        $returnData = $this->getParameterData( $asociatieId, $parametruId );
        
        return $returnData;
    }
    
    public function getParametruId( $configuratieId )
    {
        $column     = [ App_Component_Configuratie_Object::PROP_PARAMETRU_ID ];
        $condition = [ App_Component_Configuratie_Object::PROP_ID => $configuratieId ];
        
        $configuratieRow = $this->retrieveData( $column, $condition );
        
        $parametruId = Lib_Tools::checkInputDataKeys( $configuratieRow, [ 0, App_Component_Configuratie_Object::PROP_PARAMETRU_ID ] );
        
        return $parametruId;
    }
    
    public function getConfiguratieValue( $configuratieId )
    {
        $column     = [ App_Component_Configuratie_Object::PROP_VALUE ];
        $condition = [ App_Component_Configuratie_Object::PROP_ID => $configuratieId ];
        
        $configuratieRow = $this->retrieveData( $column, $condition );
        
        $value = json_decode( Lib_Tools::checkInputDataKeys( $configuratieRow, [ 0, App_Component_Configuratie_Object::PROP_VALUE ] ), TRUE );
        
        return $value;
    }
    
    public function isUsedByCheltuieli( array $parameters )
    {
        return $this->getDb()->isUsedByCheltuieli( $parameters );
    }
    
}
