<?php

class App_Component_Subcoloana_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    /** (v)
     *
     * @return type
     */
    public function getAll()
    {
        $data = $this->getDb()->getAll();
        $subcoloaneData = [];

        foreach ($data as $row)
        {
            $id = $row[ App_Component_Subcoloana_Object::PROP_ID ];

            $subcoloaneData[ $id ] = $row;
        }

        return $subcoloaneData;
    }

}
