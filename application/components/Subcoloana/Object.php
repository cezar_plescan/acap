<?php

class App_Component_Subcoloana_Object extends Lib_Component_ORM_Object
{
    const COLOANA_ID            = 'coloana_id';
    const CODE                  = 'code';
    const LABEL                 = 'label';
    const ORDER                 = 'order';
    const IS_NUMERIC            = 'is_numeric';
    const ALIGN_RIGHT           = 'align_right';
    const SHOW_ZEROS            = 'show_zeros';
    const STATUS                = 'status';

    const STATUS_DISPLAYABLE    = 0;

    protected $_properties = [
        self::PROP_ID                    => NULL,
        self::COLOANA_ID            => NULL,
        self::CODE                  => NULL,
        self::LABEL                 => NULL,
        self::ORDER                 => NULL,
        self::IS_NUMERIC            => NULL,
        self::ALIGN_RIGHT           => NULL,
        self::SHOW_ZEROS            => NULL,
        self::STATUS                => self::STATUS_DISPLAYABLE,
    ];

}