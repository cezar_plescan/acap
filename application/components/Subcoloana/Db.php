<?php

class App_Component_Subcoloana_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'subcoloana';

    /** (v)
     *
     */
    public function getAll()
    {
        $columns = $this->quoteColumns([
            App_Component_Subcoloana_Object::PROP_ID,
            App_Component_Subcoloana_Object::COLOANA_ID,
            App_Component_Subcoloana_Object::CODE,
            App_Component_Subcoloana_Object::LABEL,
            App_Component_Subcoloana_Object::IS_NUMERIC,
            App_Component_Subcoloana_Object::ALIGN_RIGHT,
            App_Component_Subcoloana_Object::SHOW_ZEROS,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                    $this->whereEqual( App_Component_Subcoloana_Object::STATUS_DISPLAYABLE, App_Component_Subcoloana_Object::STATUS ),
                ])) .

                $this->orderBy(
                        [ App_Component_Subcoloana_Object::ORDER ]
                );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

}
