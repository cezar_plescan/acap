<?php

class App_Component_CheltuialaParametru_Db extends App_Component_ParametruEntity_Db
{
    const TABLE = 'cheltuiala_parametru';

    static protected $instance;

    /** (v)
     *
     * @param int $asociatieId
     *
     * @return type
     */
    public function getAll( $asociatieId )
    {
        $columns = $this->quoteColumns([
                App_Component_CheltuialaParametru_Object::PROP_REF_ID,
                App_Component_CheltuialaParametru_Object::PROP_PARAMETRU,
                App_Component_CheltuialaParametru_Object::PROP_VALUE,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_CheltuialaParametru_Object::PROP_ASOCIATIE_ID ),
                ]));

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

}
