<?php

class App_Component_CheltuialaParametru_ValuesFilter
{
    public function filter( array &$parametri )
    {
        foreach( $parametri as $parametru => $value )
        {
            $methodName = '_filter' . Zend_Filter::filterStatic( $parametru, 'Word_UnderscoreToCamelCase' );
            if( method_exists( $this, $methodName ) )
            {
                $parametri[ $parametru ] = $this->$methodName( $value );
            }
        }
    }
    
    protected function _filterData( $data )
    {
        if( $data instanceof DateTime )
        {
            $data = $data->format( Lib_Application::getInstance()->getConfig( [ 'settings', 'db_date_format' ] ) );
        }
        
        return $data;
    }
    
    protected function _filterScadenta( $scadenta )
    {
        if( $scadenta instanceof DateTime )
        {
            $scadenta = $scadenta->format( Lib_Application::getInstance()->getConfig( [ 'settings', 'db_date_format' ] ) );
        }
        
        return $scadenta;
    }
    
}
