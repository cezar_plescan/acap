<?php

class App_Component_CheltuialaParametru_Factory extends App_Component_ParametruEntity_Factory
{
    static protected $instance;

    const PARAMETRU_DESCRIERE   = 'descriere';
    const PARAMETRU_FURNIZOR    = 'furnizor';
    const PARAMETRU_BENEFICIAR  = 'beneficiar';
    const PARAMETRU_DOCUMENT    = 'act';
    const PARAMETRU_VALOARE     = 'valoare';
    const PARAMETRU_TAXE        = 'taxe';
    const PARAMETRU_DATA        = 'data';
    const PARAMETRU_SCADENTA    = 'scadenta';
    const PARAMETRU_FOND        = 'fond';
    const PARAMETRU_REPARTIZARE = 'repartizare';
    const PARAMETRU_REDUCERE    = 'reducere';
    const PARAMETRU_CONTOR      = 'contor';
    const PARAMETRU_ASCENSOARE  = 'ascensoare';
    const PARAMETRU_BRANSAMENT  = 'bransament';
    
    const PARAMETRU_REPARTIZARE_BLOCURI = 'blocuri';
    const PARAMETRU_REPARTIZARE_SCARI   = 'scari';
    const PARAMETRU_REPARTIZARE_SPATII  = 'spatii';
    
    const PARAMETRU_REDUCERE_SPATII     = 'spatii';
    const PARAMETRU_REDUCERE_PROCENT    = 'procent';
    
    protected $ValuesFilter = null;
    
    protected function __construct()
    {
        parent::__construct();
        
        $this->ValuesFilter = new App_Component_CheltuialaParametru_ValuesFilter();
    }
    
    public function setParametriCheltuiala( $cheltuialaId, array $parametri )
    {
        // stergere eventuali parametri existenti
        $this->deleteParametri( $cheltuialaId );
        
        $this->filterParametriValues( $parametri );
        
        // inserare parametri
        $this->addParametri( $cheltuialaId, $parametri );
        
        return $parametri;
    }
    
    protected function filterParametriValues( array &$parametri )
    {
        $this->ValuesFilter->filter( $parametri );
    }
    
}
