<?php

class App_Component_Scara_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    /** (v)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAll( $asociatieId, $month )
    {
        $cacheId = "scari-$asociatieId-$month";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $data = $this->getDb()->getAll( $asociatieId, $month );

            $consumuri = App_Component_Consum_Scara_Factory::getInstance()->getAllByScara( $asociatieId, $month );
            $spatii = App_Component_Spatiu_Factory::getInstance()->getAll( $asociatieId, $month );

            $scariData = [];

            foreach( $data as $rowData )
            {
                $scaraId = $rowData[ App_Component_Scara_Object::PROP_ID ];

                $rowData[ App_Component_Scara_Object::CONSUMURI ] = isset( $consumuri[ $scaraId ] ) ? $consumuri[ $scaraId ] : [];
                $rowData[ App_Component_Scara_Object::PROP_SPATII ] = [];

                $scariData[ $scaraId ] = $rowData;
            }

            foreach( $spatii as $spatiuId => $spatiu )
            {
                $scaraId = $spatiu[ App_Component_Spatiu_Object::PROP_SCARA_ID ];
                $scariData[ $scaraId ][ App_Component_Scara_Object::PROP_SPATII ][] = $spatiuId;
            }

            $this->_cache[ $cacheId ] = $scariData;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Returneaza lista cu toate scarile din Asociatie, ordonate dupa bloc si denumire.
     * 
     * @param int $asociatieId
     * @return array
     */
    public function getList( $asociatieId )
    {
        $cacheId = "list-$asociatieId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $data = $this->getDb()->getAll( $asociatieId );

            $this->_cache[ $cacheId ] = $data;
        }

        return $this->_cache[ $cacheId ];
    }
    
    /**
     * Returneaza lista cu ID-urile tuturor scarilor din Asociatie
     * 
     * @param int $asociatieId
     * @return array
     */
    public function getIDList( $asociatieId )
    {
        $cacheId = "id-list-$asociatieId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $data = $this->getDb()->getIDList( $asociatieId );

            $this->_cache[ $cacheId ] = $data;
        }

        return $this->_cache[ $cacheId ];
    }
    
    /** (v)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAllByAsociatie( $asociatieId, $month )
    {
        $cacheId = "scari-asociatie-$asociatieId-$month";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $data = $this->getDb()->getAllByAsociatie( $asociatieId, $month );

            $this->_cache[ $cacheId ] = $data;
        }

        return $this->_cache[ $cacheId ];
    }

    public function getScariByBlocId( $blocId )
    {
        $cacheId = "scari-bloc-$blocId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $scari = $this->fetchAll([
                App_Component_Scara_Object::PROP_BLOC_ID => $blocId
            ]);

            $this->_cache[ $cacheId ] = $scari;
        }

        return $this->_cache[ $cacheId ];
    }
    
    public function getScariIdsByBlocId( $blocId )
    {
        $cacheId = "scari_ids-bloc-$blocId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $scariData = $this->getDb()->fetchAll([
                App_Component_Scara_Object::PROP_BLOC_ID => $blocId
            ]);
            
            $ids = array_column( $scariData, App_Component_Scara_Object::PROP_ID );
            
            $this->_cache[ $cacheId ] = $ids;
        }

        return $this->_cache[ $cacheId ];
    }
    
    public function getDenumireCompleta( array $scaraData )
    {
        $denumireCompleta = 'Scara ' . $scaraData[ App_Component_Scara_Object::PROP_DENUMIRE ] . ', Bloc ' . $scaraData[ App_Component_Scara_Object::DENUMIRE_BLOC ];

        return $denumireCompleta;
    }
    
    public function getDenumireScara( $scaraId, array $options = [] )
    {
        $denumire = '';
        
        $scaraData = $this->retrieveData([
            App_Component_Scara_Object::PROP_DENUMIRE,
            App_Component_Scara_Object::PROP_BLOC_ID,
        ], [
            App_Component_Scara_Object::PROP_ID => $scaraId,
        ] + App_Component_Scara_Object::getActiveStatusCriteria() );
        
        $blocId = Lib_Tools::checkInputDataKeys( $scaraData, [ 0, App_Component_Scara_Object::PROP_BLOC_ID ] );
        $denumireScara = Lib_Tools::checkInputDataKeys( $scaraData, [ 0, App_Component_Scara_Object::PROP_DENUMIRE ] );
        
        $blocData = App_Component_Bloc_Factory::getInstance()->getBlocData( $blocId );
        $denumireBloc = Lib_Tools::checkInputDataKeys( $blocData, App_Component_Bloc_Object::PROP_DENUMIRE );
        
        if( !empty( $options[ 'abbr' ] ) )
        {
            $denumire = 'Bl. ' . $denumireBloc . ', Sc. ' . $denumireScara;
        }
        else
        {
            $denumire = 'Bloc ' . $denumireBloc . ', Scara ' . $denumireScara;
        }
        
        return $denumire;
    }
    
    /**
     * 
     * @param array $scaraValues 
     * { App_Component_Scara_Object::ASOCIATIE_ID, 
     * { App_Component_Scara_Object::BLOC_ID, 
     *   App_Component_Scara_Object::DENUMIRE, 
     *   App_Component_Scara_Object::ADRESA, 
     *   App_Component_Scara_Object::ADDING_MONTH 
     * }
     * 
     * @return App_Component_Scara_Object
     */
    public function add( array $scaraValues )
    {
        // filter the input
        $data = Lib_Tools::filterArray( $scaraValues, [ 
            App_Component_Scara_Object::PROP_ASOCIATIE_ID, 
            App_Component_Scara_Object::PROP_BLOC_ID, 
            App_Component_Scara_Object::PROP_ADRESA, 
            App_Component_Scara_Object::PROP_DENUMIRE, 
            App_Component_Scara_Object::PROP_ADDING_MONTH 
        ]);
        
        // create an object and save it
        $Scara = $this->createAndSave( [ $data ] );
        
        return $Scara;
    }
    
    /**
     * 
     * @param array $scaraData 
     * { App_Component_Scara_Object::ID, 
     *   App_Component_Scara_Object::DENUMIRE, 
     *   App_Component_Scara_Object::ADRESA
     * }
     * 
     * @return App_Component_Scara_Object
     */
    public function edit( array $scaraData )
    {
        $Scara = $this->find( $scaraData[ App_Component_Scara_Object::PROP_ID ] );
        
        if( $Scara )
        {
            $Scara->setPropertyIfKeyExists( App_Component_Scara_Object::PROP_DENUMIRE, $scaraData );
            $Scara->setPropertyIfKeyExists( App_Component_Scara_Object::PROP_ADRESA, $scaraData );
            
            $Scara->save();
        }
        
        return $Scara;
    }
    
    /**
     * 
     * @param array $scaraData { App_Component_Scara_Object::ID }
     * 
     * @return App_Component_Scara_Object
     */
    public function remove( array $scaraData )
    {
        $Scara = $this->find( $scaraData[ App_Component_Scara_Object::PROP_ID ] );
        
        if( $Scara )
        {
            $Scara->remove();
        }
        
        return $Scara;
    }
    
    
}
