<?php

class App_Component_Scara_Object extends Lib_Component_ORM_Object // implements App_Component_Cheltuiala_Interface_Target
{
    // properties
    const PROP_ASOCIATIE_ID = '_asociatie_id';
    const PROP_BLOC_ID       = 'bloc_id';
    const PROP_DENUMIRE      = 'denumire';
    const PROP_ADRESA        = 'adresa';
    const PROP_ADDING_MONTH  = 'adding_month';
    const PROP_STATUS        = 'status';

    // other properties
    const MONTH         = 'month';
    const CONSUMURI     = 'consumuri';
    const PROP_SPATII   = 'spatii';
    const SPATII_DATA   = 'spatii_data';
    const DENUMIRE_BLOC = 'denumire_bloc';

    // property values
    const VAL_STATUS_ACTIVE     = 0;
    const VAL_STATUS_DELETED    = 1;

    // criterii de repartizare a unor valori
    const REPARTIZARE_SUPRAFATA     = App_Component_Spatiu_Object::REPARTIZARE_SUPRAFATA;
    const REPARTIZARE_PERSOANE      = App_Component_Spatiu_Object::REPARTIZARE_PERSOANE;
    const REPARTIZARE_APA_RECE      = App_Component_Spatiu_Object::REPARTIZARE_APA_RECE;
    const REPARTIZARE_EGAL          = App_Component_Spatiu_Object::REPARTIZARE_EGAL;

    const ATTR_DENUMIRE_MAX_LENGTH  = 20;
    const ATTR_ADRESA_MAX_LENGTH    = 200;
    
    protected $_properties = [
        self::PROP_ID            => NULL,
        self::PROP_ASOCIATIE_ID  => NULL,
        self::PROP_BLOC_ID       => NULL,
        self::PROP_DENUMIRE      => NULL,
        self::PROP_ADRESA        => NULL,
        self::PROP_ADDING_MONTH  => NULL,
        self::PROP_STATUS        => self::VAL_STATUS_ACTIVE,
        self::PROP_CREATED_AT    => NULL,
        self::PROP_UPDATED_AT    => NULL,
    ];

    protected
            $_cache = [];

    /**
     * @return array
     */
    public static function getActiveStatusCriteria()
    {
        $criteria = [
            self::PROP_STATUS    => self::VAL_STATUS_ACTIVE,
        ];
        
        return $criteria;
    }
    
    public function getBlocId()
    {
        return $this->getProperty( self::PROP_BLOC_ID );
    }

    public function getDenumire()
    {
        return $this->getProperty( self::PROP_DENUMIRE );
    }

    public function getAdresa()
    {
        return $this->getProperty( self::PROP_ADRESA );
    }

    /**
     * 
     * @return App_Component_Scara_Object
     */
    public function remove()
    {
        $this->setProperty( self::PROP_STATUS, self::VAL_STATUS_DELETED );
        $this->save();
        
        // stergere spatii
        $this->removeSpatii();
        
        return $this;
    }
    
    public function belongsTo($blocId)
    {
        if ( $blocId instanceof App_Component_Bloc_Object )
        {
            $blocId = $blocId->getId();
        }

        if ( $this->{self::PROP_BLOC_ID} == $blocId )
        {
            return true;
        }

        return false;
    }

//    /**
//     * @return App_Component_Bloc_Object
//     */
//    public function getBloc()
//    {
//        if (!isset($this->_cache['bloc']))
//        {
//            $Bloc = App_Component_Bloc_Factory::getInstance()->getAll()[ $this->{self::BLOC_ID} ];
//
//            $this->_cache['bloc'] = $Bloc;
//        }
//
//        return $this->_cache['bloc'];
//    }

    /**
     *
     * @return App_Component_Spatiu_Object[]
     */
    public function getSpatii()
    {
        if (!isset($this->_cache['spatii']))
        {
            $spatiuFactory = App_Component_Spatiu_Factory::getInstance();
            $Spatii = $spatiuFactory->getByScara( $this->{self::PROP_ID}, $this->{self::MONTH} );

            $this->_cache['spatii'] = $Spatii;
        }

        return $this->_cache['spatii'];
    }

    public function getSpatiiGroupedByEtaj()
    {
        if (!isset($this->_cache['spatii-etaj']))
        {
            $SpatiiGroupedByEtaj = [];
            $Spatii = $this->getSpatii();

            foreach($Spatii as $Spatiu)
            {
                $etaj = $Spatiu->getEtaj();
                $SpatiiGroupedByEtaj[ $etaj ][ $Spatiu->getId() ] = $Spatiu;
            }

            $this->_cache['spatii-etaj'] = $SpatiiGroupedByEtaj;
        }

        return $this->_cache['spatii-etaj'];
    }

    /**
     *
     * @return App_Component_Consum_Scara_Object[]
     */
    public function getConsumuri()
    {
        if (!isset($this->_cache['consumuri']))
        {
            $ConsumScaraFactory = App_Component_Consum_Scara_Factory::getInstance();
            $Consumuri = $ConsumScaraFactory->getAll($this->{self::PROP_ID}, $this->{self::MONTH});

            $this->_cache['consumuri'] = $Consumuri;
        }

        return $this->_cache['consumuri'];
    }

    /**
     * Returneaza un consum
     *
     * @param string $codConsum
     * @return App_Component_Consum_Scara_Object
     */
    public function getConsum($codConsum)
    {
        $Consumuri = $this->getConsumuri();

        if ( isset($Consumuri[ $codConsum ]) )
        {
            /* @var $ConsumScara App_Component_Consum_Scara_Object */
            $ConsumScara = $Consumuri[ $codConsum ];
        }
        else
        {
            $ConsumScara = null;
        }

        return $ConsumScara;
    }

    /**
     * Returneaza valoarea unui consum
     *
     * @param string $codConsum
     * @return string|null
     */
    public function getValoareConsum($codConsum)
    {
        $cacheId = 'valoare-consum-' . $codConsum;

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $ConsumScara = $this->getConsum( $codConsum );

            if ( $ConsumScara )
            {
                $valoareConsum = $ConsumScara->getValue();
            }
            else
            {
                $valoareConsum = null;
            }

            $this->_cache[ $cacheId ] = $valoareConsum;
        }

        return $this->_cache[ $cacheId ];
    }

    public function setValoareConsum($codConsum, $valoare)
    {
        $this->_cache['valoare-consum-' . $codConsum] = $valoare;
    }

    /**
     * Calculeaza valoarea corespunzatoare unui criteriu
     *
     * @param string $criteriu
     *
     * @return mixed
     * @throws App_Component_Scara_Exception_CriteriuInvalid
     */
    public function getValoareCriteriu($criteriu)
    {
        $methodMap = [
            self::REPARTIZARE_SUPRAFATA     => '_getSuprafata',
            self::REPARTIZARE_PERSOANE      => '_getPersoane',
            self::REPARTIZARE_APA_RECE      => ['getValoareConsum', App_Component_Consum_Scara_Factory::APA_RECE],
            self::REPARTIZARE_EGAL          => function(){ return 1; },
        ];

        if (isset( $methodMap[$criteriu] ))
        {
            $methodDefinition = (array)$methodMap[ $criteriu ];

            $methodName = $methodDefinition[0];
            $methodArgs = array_slice($methodDefinition, 1);

            if (is_callable($methodName))
            {
                $value = call_user_func_array($methodName, $methodArgs);

                return $value;
            }
            elseif (method_exists($this, $methodName))
            {
                $value = call_user_func_array([$this, $methodName], $methodArgs);

                return $value;
            }
        }

        throw new App_Component_Scara_Exception_CriteriuInvalid( $criteriu );
    }

    /**
     * Calculeaza suprafata intregii Scari.
     *
     * @return float
     */
    protected function _getSuprafata()
    {
        if (!isset($this->_cache['suprafata']))
        {
            // Suprafata Scarii = Suma suprafetelor spatiilor din Scara
            $suprafata = 0;

            foreach ($this->getSpatii() as $Spatiu)
            {
                $suprafataSpatiu = $Spatiu->getValoareParametru( App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA );
                $suprafata += $suprafataSpatiu;
            }

            $this->_cache['suprafata'] = $suprafata;
        }

        return $this->_cache['suprafata'];
    }

    /**
     * Calculeaza nr de persoane din intreaga Scara.
     *
     * @return int
     */
    protected function _getPersoane()
    {
        if (!isset($this->_cache['persoane']))
        {
            // Numarul total de pers. al Scarii = Suma pers. spatiilor din Scara
            $persoane = 0;

            foreach ($this->getSpatii() as $Spatiu)
            {
                $persoaneSpatiu = $Spatiu->getValoareParametru( App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE );
                $persoane += $persoaneSpatiu;
            }

            $this->_cache['persoane'] = $persoane;
        }

        return $this->_cache['persoane'];
    }

    public function getDenumireCompleta()
    {
        $denumireScara = $this->getDenumire();
        $denumireBloc = $this->getBloc()->getDenumire();

        $denumireCompleta = 'Bloc ' . $denumireBloc . ', ' . 'Scara ' . $denumireScara;

        return $denumireCompleta;
    }

    public function getCheltuieli( $codColoana )
    {
        $cacheId = 'cheltuieli-' . $codColoana;

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $CheltuieliColoana = App_Component_Cheltuiala_Factory::getInstance()->getByColoanaCode( $codColoana );
            $Cheltuieli = [];

            foreach ( $CheltuieliColoana as $C )
            {
                // verific daca cheltuiala este repartizata la aceasta scara
                foreach( $C->getTargetObjects() as $Target )
                {
                    if ( $Target->isLinkedToScara( $this ) )
                    {
                        $Cheltuieli[ $C->getId() ] = $C;
                        break;
                    }
                }
            }

            $this->_cache[ $cacheId ] = $Cheltuieli;
        }

        return $this->_cache[ $cacheId ];
    }

    public function isLinkedToScara($Scara)
    {
        if ($Scara instanceof App_Component_Scara_Object)
        {
            $scaraId = $Scara->getId();
        }
        else
        {
            $scaraId = $Scara;
        }

        return ( $scaraId == $this->getId() );
    }

    private function removeSpatii()
    {
        $spatii = $this->getSpatiiObjects();
        foreach( $spatii as $Spatiu )
        {
            $Spatiu->remove();
        }
    }
    
    private function getSpatiiObjects()
    {
        $spatii = App_Component_Spatiu_Factory::getInstance()->getSpatiiByScaraId( $this->getId() );
        
        return $spatii;
    }
    
}