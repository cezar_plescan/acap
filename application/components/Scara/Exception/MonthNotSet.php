<?php

class App_Component_Scara_Exception_MonthNotSet extends UnexpectedValueException
{
    public function __construct($message, $code, $previous)
    {
        $message = 'Luna nu este setata';
        
        parent::__construct($message, $code, $previous);
    }
}
