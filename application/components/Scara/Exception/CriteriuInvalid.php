<?php

class App_Component_Scara_Exception_CriteriuInvalid extends UnexpectedValueException
{
    public function __construct($criteriu)
    {
        $message = 'Criteriul este invalid: "' . $criteriu . '"';

        parent::__construct($message, $code, $previous);
    }
}
