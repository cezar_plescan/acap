<?php

/**
 * Conditions:
 * - value is required
 * - have a maximum length
 * - should be unique per asociatie
 * - limit for active entries
 * - limit for all entries
 */
class App_Component_Scara_Validator_Add extends App_Component_Scara_Validator_EntryAbstract
{
    const MAX_ENTRIES           = 20;
    const MAX_OPERATIONS        = 100;
    
    const ERR_MAX_ENTRIES           = 'max_entries';
    const ERR_MAX_OPERATIONS        = 'max_operations';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_MAX_ENTRIES   => 'Ați atins limita numărului de scări ce pot face parte dintr-un bloc.',
            self::ERR_MAX_OPERATIONS => 'Ați depășit numărul admis de operații.',
        ]);
    }
    
    /**
     * 
     * @param array $value { App_Component_Scara_Object::PROP_DENUMIRE, App_Component_Scara_Object::PROP_ADRESA, App_Component_Scara_Object::PROP_BLOC_ID }
     * @return array
     */
    protected function validate( $value )
    {
        // verificare bloc
        $blocId = $this->validateBlocId( @$value[ App_Component_Scara_Object::PROP_BLOC_ID ] );

        // verificare numar de scari din bloc
        $this->checkMaxEntriesPerBloc( $blocId );
        
        // verificare numar de operatiuni per bloc
        $this->checkMaxOperationsPerBloc( $blocId );
        
        $denumire = $this->validateDenumire( @$value[ App_Component_Scara_Object::PROP_DENUMIRE ], $blocId );
        
        $adresa = $this->validateAdresa( @$value[ App_Component_Scara_Object::PROP_ADRESA ] );
        
        $filteredValue = [
            App_Component_Scara_Object::PROP_DENUMIRE   => $denumire,
            App_Component_Scara_Object::PROP_ADRESA     => $adresa,
            App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
        ];
        
        return $filteredValue;
    }
    
    protected function checkMaxEntriesPerBloc( $blocId )
    {
        $criteria = [
            App_Component_Scara_Object::PROP_BLOC_ID => $blocId,
        ];
        
        $entriesCount = App_Component_Scara_Factory::getInstance()->countActive( $criteria );
        
        if( $entriesCount >= self::MAX_ENTRIES )
        {
            $this->setError( self::ERR_MAX_ENTRIES );
        }
    }
    
    protected function checkMaxOperationsPerBloc( $blocId )
    {
        $criteria = [
            App_Component_Scara_Object::PROP_BLOC_ID => $blocId,
        ];
        
        $operationsCount = App_Component_Scara_Factory::getInstance()->count( $criteria );
        
        if( $operationsCount >= self::MAX_OPERATIONS )
        {
            $this->setError( self::ERR_MAX_OPERATIONS );
        }
    }

}