<?php

class App_Component_Scara_Validator_Ids extends Lib_Validator_Abstract
{
    const OPT_ASOCIATIE_ID  = 'a';
    const OPT_MONTH         = 'm';

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID   => 'Scara invalida: "%id%"',
        ));
    }

    protected function _isValid( $scaraIds )
    {
        $scari = App_Component_Scara_Factory::getInstance()->getAllByAsociatie( $this->getOption( self::OPT_ASOCIATIE_ID ), $this->getOption( self::OPT_MONTH ) );

        // argumentul poate fi un singur ID sau o lista de ID-uri
        if ( !is_array( $scaraIds ) )
        {
            $scaraIds = [ $scaraIds ];
        }

        $scaraIds = array_unique( $scaraIds );

        foreach( $scaraIds as $scaraId )
        {
            if( !isset( $scari[ $scaraId ] ) )
            {
                $this->_setError( self::ERR_INVALID, [ 'id' => $scaraId ] );
            }
        }

        return $scaraIds;
    }

}
