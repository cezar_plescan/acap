<?php

/**
 * 
 * 
 */
abstract class App_Component_Scara_Validator_EntryAbstract extends App_Component_Asociatie_Validator_BaseAbstract
{
    const ERR_DENUMIRE_REQUIRED     = 'denumire_required';
    const ERR_DENUMIRE_MAX_LENGTH   = 'denumire_max_length';
    const ERR_DENUMIRE_DUPLICATE    = 'denumire_duplicate';
    const ERR_INVALID_BLOC_ID       = 'invalid_bloc_id';
    const ERR_INVALID_SCARA_ID      = 'invalid_scara_id';
    const ERR_ADRESA_MAX_LENGTH     = 'adresa_max_length';
    const ERR_ADRESA_REQUIRED       = 'adresa_required';
    
    const DENUMIRE_MAX_LENGTH       = App_Component_Scara_Object::ATTR_DENUMIRE_MAX_LENGTH;
    const ADRESA_MAX_LENGTH         = App_Component_Scara_Object::ATTR_ADRESA_MAX_LENGTH;
    
    protected function init()
    {
        $this->addMessageTemplates([
            self::ERR_DENUMIRE_REQUIRED     => 'Completați denumirea scării.',
            self::ERR_ADRESA_REQUIRED       => 'Completați adresa scării.',
            self::ERR_DENUMIRE_MAX_LENGTH   => 'Denumirea scării nu poate depăși ' . Lib_Grammar::pluralize( self::DENUMIRE_MAX_LENGTH, 'caracter', 'e'),
            self::ERR_ADRESA_MAX_LENGTH     => 'Adresa scării nu poate depăși ' . Lib_Grammar::pluralize( self::ADRESA_MAX_LENGTH, 'caracter', 'e'),
            self::ERR_DENUMIRE_DUPLICATE    => 'Acestă denumire aparține unei alte scări.',
            self::ERR_INVALID_BLOC_ID       => 'Blocul specificat nu este valid.',
            self::ERR_INVALID_SCARA_ID      => 'Scara specificată nu este validă.',
        ]);
    }
    
    /**
     * Conditii:
     * - sa existe
     * - sa fie unica in bloc
     * - sa nu depaseasca o anumita lungime
     * 
     * @param type $denumire
     * @param type $blocId
     * @return string
     */
    protected function validateDenumire( $denumire, $blocId, $excludeScaraId = NULL )
    {
        // verificarea existentei denumirii si a dimensiunii acesteia
        $denumireFiltered = trim( $denumire );
        
        $length = strlen( $denumireFiltered );
        
        if( $length == 0 )
        {
            $this->addError( self::ERR_DENUMIRE_REQUIRED );
        }
        elseif ( $length > self::DENUMIRE_MAX_LENGTH )
        {
            $this->addError( self::ERR_DENUMIRE_MAX_LENGTH );
        }
        else
        {
            // verificare unicitate
            $this->validateUniqueness( $denumireFiltered, $blocId, $excludeScaraId );
        }
        
        return $denumireFiltered;
    }

    /**
     * Conditii:
     * - sa existe
     * - sa nu depaseasca o anumita lungime
     * 
     */
    protected function validateAdresa( $adresa )
    {
        $adresaFiltered = trim( $adresa );
        
        $length = strlen( $adresaFiltered );
        
        if( $length == 0 )
        {
            $this->addError( self::ERR_ADRESA_REQUIRED );
        }
        elseif ( $length > self::ADRESA_MAX_LENGTH )
        {
            $this->addError( self::ERR_ADRESA_MAX_LENGTH );
        }
        
        return $adresaFiltered;
    }

    private function validateUniqueness( $denumire, $blocId, $excludeScaraId = NULL )
    {
        $criteria = [
            App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
            App_Component_Scara_Object::PROP_DENUMIRE   => $denumire,
        ];
        
        if( !empty( $excludeScaraId ) )
        {
            $criteria[ App_Component_Scara_Object::PROP_ID ] = new Lib_Component_ORM_Db_Operator_NotEqual( $excludeScaraId );
        }
        
        $duplicates = App_Component_Scara_Factory::getInstance()->countActive( $criteria );
        
        if( $duplicates )
        {
            $this->addError( self::ERR_DENUMIRE_DUPLICATE );
        }
    }

    protected function validateBlocId( $blocId )
    {
        $blocIdFiltered = (int)$blocId;
        
        // verific ID-ul blocului: sa existe si sa apartina Asociatiei
        $Bloc = App_Component_Bloc_Factory::getInstance()->findActive( $blocIdFiltered );
        
        if( !$Bloc
            || $Bloc->getProperty( App_Component_Bloc_Object::PROP_ASOCIATIE_ID ) != $this->getAsociatieId() )
        {
            $this->setError( self::ERR_INVALID_BLOC_ID );
        }
        
        return $blocIdFiltered;
    }
    
    protected function validateScaraId( $scaraId )
    {
        $scaraIdFiltered = (int)$scaraId;
        
        // verific ID-ul scarii: sa existe si sa apartina Asociatiei
        $Scara = App_Component_Scara_Factory::getInstance()->findActive( $scaraIdFiltered );
        
        if( !$Scara
            || $Scara->getProperty( App_Component_Scara_Object::PROP_ASOCIATIE_ID ) != $this->getAsociatieId() )
        {
            $this->setError( self::ERR_INVALID_SCARA_ID );
        }
        
        return $scaraIdFiltered;
    }
    
    
}