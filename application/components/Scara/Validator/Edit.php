<?php

class App_Component_Scara_Validator_Edit extends App_Component_Scara_Validator_EntryAbstract
{
    const ERR_NO_VALUE  = 'no_value';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_NO_VALUE  => 'Specificați câmpul pe care doriți să-l modificați.',
        ]);
    }
    
    /**
     * 
     * @param array $value { App_Component_Scara_Object::PROP_DENUMIRE, App_Component_Scara_Object::PROP_ADRESA, App_Component_Scara_Object::PROP_ID }
     * @return array
     */
    protected function validate( $value )
    {
        // verificare scara
        $scaraId = $this->validateScaraId( @$value[ App_Component_Scara_Object::PROP_ID ] );

        $filteredValue = [
            App_Component_Scara_Object::PROP_ID   => $scaraId,
        ];
        
        // trebuie sa existe cel putin una dintre valorile DENUMIRE sau ADRESA
        $denumireExists = array_key_exists( App_Component_Scara_Object::PROP_DENUMIRE, $value );
        $adresaExists   = array_key_exists( App_Component_Scara_Object::PROP_ADRESA, $value );
        
        if( !$denumireExists && !$adresaExists )
        {
            $this->setError( self::ERR_NO_VALUE );
        }
        
        if( $denumireExists )
        {
            $blocId = $this->getBlocId( $scaraId );
            
            $denumire = $this->validateDenumire( @$value[ App_Component_Scara_Object::PROP_DENUMIRE ], $blocId, $scaraId );
            
            $filteredValue[ App_Component_Scara_Object::PROP_DENUMIRE ] = $denumire;
        }
        
        if( $adresaExists )
        {
            $adresa = $this->validateAdresa( @$value[ App_Component_Scara_Object::PROP_ADRESA ] );
            
            $filteredValue[ App_Component_Scara_Object::PROP_ADRESA ] = $adresa;
        }
        
        return $filteredValue;
    }
    
    protected function getBlocId( $scaraId )
    {
        $Bloc = App_Component_Scara_Factory::getInstance()->find( $scaraId );
        $blocId = $Bloc ? $Bloc->getProperty( App_Component_Scara_Object::PROP_BLOC_ID ) : NULL;
        
        return $blocId;
    }

}