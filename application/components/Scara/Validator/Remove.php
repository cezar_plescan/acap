<?php

class App_Component_Scara_Validator_Remove extends App_Component_Scara_Validator_EntryAbstract
{
    /**
     * 
     * @param array $value { App_Component_Scara_Object::PROP_ID }
     * @return array
     */
    protected function validate( $value )
    {
        // verificare scara
        $scaraId = $this->validateScaraId( @$value[ App_Component_Scara_Object::PROP_ID ] );

        $filteredValue = [
            App_Component_Scara_Object::PROP_ID   => $scaraId,
        ];

        return $filteredValue;
    }

}