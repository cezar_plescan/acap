<?php

class App_Component_Scara_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'scara';
    
    static protected $instance;

    /** (v)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAll( $asociatieId, $month = NULL )
    {
        $columns = $this->quoteColumns([
                App_Component_Scara_Object::PROP_ID,
                App_Component_Scara_Object::PROP_BLOC_ID,
                App_Component_Scara_Object::PROP_DENUMIRE,
                App_Component_Scara_Object::PROP_ADRESA,
        ]);

        $sql =
                $this->selectStmt( $columns ) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_Scara_Object::PROP_ASOCIATIE_ID ),
                        $this->getActiveStatusQuery(),
                ]));

                // nu se face ordonare; de aceasta operatie se ocupa frontend-ul
                // rezultatele sunt returnate in ordinea in care au fost adaugate in DB
//                $this->orderBy(
//                        [App_Component_Scara_Object::PROP_BLOC_ID],
//                        [App_Component_Scara_Object::PROP_DENUMIRE]
//                );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

    /** (v)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAllByAsociatie( $asociatieId, $month )
    {
        $columns = $this->quoteColumns([
                App_Component_Scara_Object::PROP_ID,
        ]);

        $sql =
                $this->selectStmt( $columns ) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_Scara_Object::PROP_ASOCIATIE_ID ),
                        $this->getActiveStatusQuery(),
                ]));

        $results = $this->queryAndFetchAll($sql);

        $data = [];
        foreach( $results as $row )
        {
            $id = $row[ App_Component_Scara_Object::PROP_ID ];
            $data[ $id ] = $id;
        }

        return $data;
    }

    /**
     *
     * @param int $month
     * @return string
     */
    protected function getActiveStatusQuery()
    {
        $sql = $this->whereEqual(App_Component_Scara_Object::VAL_STATUS_ACTIVE, App_Component_Scara_Object::PROP_STATUS);

        return $sql;
    }

    public function getIDList( $asociatieId )
    {
        $columns = $this->quoteColumns([
            App_Component_Scara_Object::PROP_ID,
        ]);

        $sql =  $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_Scara_Object::PROP_ASOCIATIE_ID ),
                        $this->getActiveStatusQuery(),
                ]));

        $results = $this->queryAndFetchAll( $sql );
        $list = array_column( $results, App_Component_Scara_Object::PROP_ID );

        return $list;
    }
    
}
