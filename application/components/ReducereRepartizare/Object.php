<?php

abstract class App_Component_ReducereRepartizare_Object extends Lib_Component_ORM_Object
{
    // fields
    const DENUMIRE      = 'denumire';
    const COLOANA_ID    = 'coloana_id';
    const ASOCIATIE_ID  = 'asociatie_id';
    const STATUS        = 'status';

    // other properties
    const PARAMETRI     = 'params';

    // field values
    const STATUS_ACTIVE     = 0;
    const STATUS_DELETED    = 1;

    protected $_properties = [
        self::PROP_ID            => NULL,
        self::DENUMIRE      => NULL,
        self::COLOANA_ID    => NULL,
        self::ASOCIATIE_ID  => NULL,
        self::STATUS        => self::STATUS_ACTIVE,
        self::PROP_TIMESTAMP     => NULL,
    ];

    protected $_cache = [];

    static public function getActiveStatusCriteria()
    {
        $criteria = [
            self::STATUS        => self::STATUS_ACTIVE,
            self::ASOCIATIE_ID  => Lib_Application::getInstance()->getUserId(),
        ];

        return $criteria;
    }

    abstract public function getPondere($spatiuId);

    /**
     *
     * @return array
     */
    protected function getParametri()
    {
        return $this->{self::PARAMETRI};
    }

    protected function getSpatii()
    {
        return App_Component_Spatiu_Factory::getInstance()->getAll();
    }

    public function getDenumire()
    {
        return $this->getProperty( self::DENUMIRE );
    }



}
