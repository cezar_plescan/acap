<?php

class App_Component_ReducereRepartizare_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'reducere_repartizare';

//    public function _getByCheltuiala( $cheltuialaId )
//    {
//        $asociatieId = Lib_Application::getInstance()->getUserId();
//
//        $columns = $this->quoteColumns('*');
//
//        $sql =
//                $this->selectStmt($columns) .
//
//                $this->innerJoin(
//                        App_Component_ReducereRepartizareCheltuiala_Db::TABLE,
//                        App_Component_ReducereRepartizareCheltuiala_Object::REDUCERE_REPARTIZARE_ID,
//                        App_Component_ReducereRepartizare_Object::ID,
//                        null,
//                        $this->whereAnd([
//                            $this->whereEqual($cheltuialaId, App_Component_ReducereRepartizareCheltuiala_Object::CHELTUIALA_ID, App_Component_ReducereRepartizareCheltuiala_Db::TABLE),
//                        ])) .
//
//                $this->where($this->whereAnd([
//                    $this->whereEqual($asociatieId, App_Component_ReducereRepartizare_Object::ASOCIATIE_ID),
//                    $this->getActiveStatusQuery(),
//                ])) .
//
//                $this->limit(1);
//
//        $exceptiiData = $this->queryAndFetchAll($sql);
//
//        // extrag parametrii exceptiei
//
//        $exceptiiIds = [];
//        foreach($exceptiiData as $exceptieId => $exceptieData)
//        {
//            $exceptiiIds[] = $exceptieData[ App_Component_ReducereRepartizare_Object::ID ];
//        }
//
//        $parametruDb = new App_Component_Parametru_ReducereRepartizare_Db();
//        $params = $parametruDb->getByRefId( $exceptiiIds );
//
//        foreach($exceptiiData as $index => $exceptieData)
//        {
//            $exceptiiData[ $index ][ App_Component_ReducereRepartizare_Object::PARAMETRI ] = [];
//
//            foreach($params as $j => $paramData)
//            {
//                $exceptieId = $paramData[App_Component_Parametru_ReducereRepartizare_Object::REF_ID ];
//                $parametru = $paramData[App_Component_Parametru_ReducereRepartizare_Object::PARAMETRU ];
//                $valoareParametru = $paramData[App_Component_Parametru_ReducereRepartizare_Object::VALUE ];
//
//                if ( $exceptieId == $exceptieData[ App_Component_ReducereRepartizare_Object::ID ] )
//                {
//                    $exceptiiData[ $index ][ App_Component_ReducereRepartizare_Object::PARAMETRI ][ $parametru ] = $valoareParametru;
//
//                    unset($params[$j]);
//                }
//            }
//
//        }
//
//        return $exceptiiData;
//    }

    /** (v)
     *
     * @param int $asociatieId
     *
     * @return array
     */
    public function getAll( $asociatieId )
    {
        $columns = $this->quoteColumns([
            App_Component_ReducereRepartizare_Object::PROP_ID,
            App_Component_ReducereRepartizare_Object::DENUMIRE,
        ]);

        $sql =
                $this->selectStmt( $columns ) .

                $this->where($this->whereAnd([
                    $this->whereEqual( $asociatieId, App_Component_ReducereRepartizare_Object::ASOCIATIE_ID ),
                    $this->getActiveStatusQuery(),
                ]));

        $exceptiiData = $this->queryAndFetchAll($sql);

        return $exceptiiData;
    }

    /** (v)
     *
     * @return string
     */
    protected function getActiveStatusQuery()
    {
        $sql = $this->whereEqual(App_Component_ReducereRepartizare_Object::STATUS_ACTIVE, App_Component_ReducereRepartizare_Object::STATUS);

        return $sql;
    }

}
