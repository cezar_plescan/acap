<?php

abstract class App_Component_ReducereRepartizare_Algorithm extends Lib_Component_Object
{
    protected
            $config = [],
            $spatii = [],
            $denumire,
            $Lista,
            $reducere,
            $spatiiReducere = [];

    public function __construct( array $config, App_Component_Lista_Object $Lista )
    {
        $this->config = $config;
        $this->denumire = $config[ App_Component_ReducereRepartizare_Object::DENUMIRE ];

        $this->spatii = $Lista->getSpatii();

        $reducere = @$this->config[ App_Component_ReducereRepartizare_Object::PARAMETRI ][ App_Component_Parametru_ReducereRepartizare_Factory::PARAM_REDUCERE ];
        if ( !isset( $reducere ) )
        {
            throw new App_Component_Lista_Exception_Compute_ExceptieRepartizare_ConfigUndefined( $this->Lista, $this, App_Component_Parametru_ReducereRepartizare_Factory::PARAM_REDUCERE );
        }
        $this->reducere = $reducere;

        $spatii = @$this->config[ App_Component_ReducereRepartizare_Object::PARAMETRI ][ App_Component_Parametru_ReducereRepartizare_Factory::PARAM_SPATII ];
        if ( empty( $spatii ) || !is_array( $spatii ) )
        {
            throw new App_Component_Lista_Exception_Compute_ExceptieRepartizare_ConfigUndefined( $this->Lista, $this, App_Component_Parametru_ReducereRepartizare_Factory::PARAM_SPATII );
        }
        $this->spatiiReducere = $spatii;

    }

    abstract public function getPonderi( array $spatiiIds );

    public function getReducere()
    {
        return $this->reducere;
    }

    public function getDenumire()
    {
        return $this->denumire;
    }
}
