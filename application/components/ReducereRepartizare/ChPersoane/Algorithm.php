<?php

class App_Component_ReducereRepartizare_ChPersoane_Algorithm extends App_Component_ReducereRepartizare_Algorithm
{
    const ALL = '*';

    public function getPonderi(array $spatiiIds)
    {
        $ponderi = [];

        $parametruSpatiu = App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE;
        foreach( $spatiiIds as $spatiuId )
        {
            $pondere = @$this->spatii[ $spatiuId ][ App_Component_Spatiu_Object::PARAMETRI ][ $parametruSpatiu ];

            if ( isset( $pondere ) && isset( $this->spatiiReducere[ $spatiuId ] ) )
            {
                $nrPersReducere = $this->spatiiReducere[ $spatiuId ];

                if( $nrPersReducere == self::ALL )
                {
                    $nrPersReducere = $pondere;
                }
                else if ( $nrPersReducere > $pondere )
                {
                    throw new App_Component_Lista_Exception_Compute_ExceptieRepartizare_ConfigNrPersInvalid( $this->Lista, $this, $spatiuId, $nrPersReducere, $pondere );
                }

                $pondere = ( $pondere - $nrPersReducere ) + ( $nrPersReducere * $this->reducere ) / 100;
            }

            $ponderi[ $spatiuId ] = $pondere;
        }

        return $ponderi;
    }

}
