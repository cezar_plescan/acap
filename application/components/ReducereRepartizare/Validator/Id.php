<?php

class App_Component_ReducereRepartizare_Validator_Id extends Lib_Validator_Abstract
{
    const OPT_ASOCIATIE_ID  = 'a';
    const OPT_COLOANA_ID    = 'c';

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID   => 'Reducere invalidă: "%id%"',
        ));

        $this->setRequired( false );
    }

    protected function _isValid( $reducereId )
    {
        $reducereId = (int)$reducereId;

        $factory = App_Component_ReducereRepartizare_Factory::getInstance();

        $valid = $factory->count(
            [
                App_Component_ReducereRepartizare_Object::PROP_ID            => $reducereId,
                App_Component_ReducereRepartizare_Object::COLOANA_ID    => $this->getOption( self::OPT_COLOANA_ID ),
                App_Component_ReducereRepartizare_Object::ASOCIATIE_ID  => $this->getOption( self::OPT_ASOCIATIE_ID ),
            ] +

            App_Component_ReducereRepartizare_Object::getActiveStatusCriteria()
        );

        if ( !$valid )
        {
            $this->_setError( self::ERR_INVALID, ['id' => $reducereId] );
        }

        return $reducereId;
    }

}
