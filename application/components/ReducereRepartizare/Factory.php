<?php

class App_Component_ReducereRepartizare_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    /**
     *
     * @param type $algorithmType
     * @param type $config
     *
     * @return App_Component_ReducereRepartizare_Algorithm
     */
    public function getAlgorithm( $algorithmType, array $config, App_Component_Lista_Object $Lista )
    {
        $algorithm = $this->create( [ $config, $Lista ], $algorithmType, 'Algorithm' );

        return $algorithm;
    }

    protected function getObjectType(array $objectProperties)
    {
        if ( isset( $objectProperties[ App_Component_ReducereRepartizare_Object::COLOANA_ID ] ) )
        {
            $coloanaId = $objectProperties[ App_Component_ReducereRepartizare_Object::COLOANA_ID ];

            $Coloane = (new App_Component_Coloana_Factory)->getAll();

            if ( isset($Coloane[ $coloanaId ]) )
            {
                /* @var $Coloana App_Component_Coloana_Object */
                $Coloana = $Coloane[ $coloanaId ];
                $objectType = $Coloana->getCode();

                return $objectType;
            }
        }

        return null;
    }

    /** (o)
     *
     * @return array
     */
    public function getAll( $asociatieId )
    {
        $reduceriData = [];

        $data = $this->getDb()->getAll( $asociatieId );
        foreach ($data as $rowData)
        {
            $id = $rowData[App_Component_ReducereRepartizare_Object::PROP_ID ];

            $rowData[ App_Component_ReducereRepartizare_Object::PARAMETRI ] = [];

            $reduceriData[ $id ] = $rowData;
        }

        $parametriData = App_Component_Parametru_ReducereRepartizare_Factory::getInstance()->getAllByReducere( $asociatieId );
        foreach( $parametriData as $reducereId => $parametri )
        {
            if ( !isset( $reduceriData[ $reducereId ] ) ) continue;

            foreach($parametri as $parametru => $value)
            {
                $reduceriData[ $reducereId ][ App_Component_ReducereRepartizare_Object::PARAMETRI ][ $parametru ] = $value;
            }
        }

        return $reduceriData;
    }



}

