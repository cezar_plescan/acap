<?php

class App_Component_ConfiguratieCategorie_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    const CATEGORIE_ENERGIE_ELECTRICA   = 'energie_electrica';
    const CATEGORIE_GAZE_NATURALE       = 'gaze_naturale';
    const CATEGORIE_ASCENSOARE          = 'ascensoare';
    
    public function getCategorii()
    {
        $cacheId = 'categorii';
        
        if( $this->notCache( $cacheId ) )
        {
            $categorii = [];

            $columns = [
                App_Component_ConfiguratieCategorie_Object::PROP_ID,
                App_Component_ConfiguratieCategorie_Object::PROP_NAME,
            ];

            $rawData = $this->retrieveData( $columns );

            foreach( $rawData as $row )
            {
                $id     = $row[ App_Component_ConfiguratieCategorie_Object::PROP_ID ];
                $name   = $row[ App_Component_ConfiguratieCategorie_Object::PROP_NAME ];

                $categorii[ $id ] = $name;
            }
            
            $this->writeCache( $cacheId, $categorii );
        }
        
        return $this->readCache( $cacheId );
    }
    
    public function getCategorieName( $categorieId )
    {
        $categorii  = $this->getCategorii();
        
        $denumireCategorie = isset( $categorii[ $categorieId ] ) ?
                $categorii[ $categorieId ] :
                NULL;
        
        return $denumireCategorie;
    }
    
    public function getCategorieId( $categorie )
    {
        $categorieId = NULL;
        $categorieFiltered = strtolower( trim( $categorie ) );
        $categorii  = $this->getCategorii();
        
        foreach( $categorii as $cId => $categorieName )
        {
            if( $categorieName === $categorieFiltered )
            {
                $categorieId = $cId;
                
                break;
            }
        }
        
        return $categorieId;
    }
    
}
