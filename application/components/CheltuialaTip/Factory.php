<?php

class App_Component_CheltuialaTip_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    const TIP_ADMINISTRATIV     = 'administrativ';
    const TIP_ENERGIE_ELECTRICA = 'energie_electrica';
    const TIP_INTRETINERE_ASCENSOR = 'intretinere_ascensor';
    const TIP_AUTORIZATIE_ASCENSOR = 'autorizatie_ascensor';
    const TIP_LUCRARI_ASCENSOR  = 'lucrari_ascensor';
    const TIP_SALUBRIZARE       = 'salubrizare';
    const TIP_GAZE_NATURALE     = 'gaze_naturale';

    public function getTipuri()
    {
        $cacheId = 'tipuri';
        
        if( $this->notCache( $cacheId ) )
        {
            $columns = [
                App_Component_CheltuialaTip_Object::PROP_ID,
                App_Component_CheltuialaTip_Object::PROP_DENUMIRE,
                App_Component_CheltuialaTip_Object::PROP_TIP,
                App_Component_CheltuialaTip_Object::PROP_CATEGORIE,
            ];

            $conditions = [
                App_Component_CheltuialaTip_Object::PROP_STATUS => App_Component_CheltuialaTip_Object::VAL_STATUS_ACTIVE,
            ];
            
            $tipuri = $this->retrieveData( $columns, $conditions );
            
            $this->writeCache( $cacheId, $tipuri );
        }
        
        return $this->readCache( $cacheId );
    }
    
    public function getTipName( $tipId )
    {
        $tipuri = $this->getTipuri();
        $name = NULL;
        
        foreach( $tipuri as $tipData )
        {
            if( $tipData[ App_Component_CheltuialaTip_Object::PROP_ID ] == $tipId )
            {
                $name = $tipData[ App_Component_CheltuialaTip_Object::PROP_TIP ];
                
                break;
            }
        }
        
        return $name;
    }
    
    public function getDenumireTip( $tip )
    {
        $tipuri = $this->getTipuri();
        $denumire = NULL;
        
        foreach( $tipuri as $tipData )
        {
            if( $tipData[ App_Component_CheltuialaTip_Object::PROP_ID ] == $tip
                || $tipData[ App_Component_CheltuialaTip_Object::PROP_TIP ] == $tip
            )
            {
                $denumire = $tipData[ App_Component_CheltuialaTip_Object::PROP_DENUMIRE ];
                
                break;
            }
        }
        
        return $denumire;
    }
    
    public function getTipId( $tip )
    {
        $tipuri = $this->getTipuri();
        $id = NULL;
        $tip = strtolower( trim( $tip ) );
        
        foreach( $tipuri as $tipData )
        {
            if( $tipData[ App_Component_CheltuialaTip_Object::PROP_TIP ] === $tip )
            {
                $id = $tipData[ App_Component_CheltuialaTip_Object::PROP_ID ];
                
                break;
            }
        }
        
        return $id;
    }
    
    public function getTipuriByCategorie( $categorie )
    {
        $cacheId = 'categorie-' . $categorie;
        
        if( $this->notCache( $cacheId ) )
        {
            $tipuriCategorie = [];

            foreach( $this->getTipuri() as $tipData )
            {
                if( $tipData[ App_Component_CheltuialaTip_Object::PROP_CATEGORIE ] == $categorie )
                {
                    $tipuriCategorie[] = $tipData;
                }
            }
            
            $this->writeCache( $cacheId, $tipuriCategorie );
        }
        
        return $this->readCache( $cacheId );
    }
    
}
