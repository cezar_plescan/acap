<?php

class App_Component_CheltuialaTip_Object extends Lib_Component_ORM_Object
{
    // properties
    const PROP_TIP          = 'tip';
    const PROP_CATEGORIE    = 'categorie';
    const PROP_DENUMIRE     = 'denumire';
    const PROP_STATUS       = 'status';

    // values
    const VAL_STATUS_ACTIVE     = 0;
    const VAL_STATUS_INACTIVE   = 1;

    protected $_properties = [
        self::PROP_ID           => NULL,
        self::PROP_CATEGORIE    => NULL,
        self::PROP_DENUMIRE     => NULL,
        self::PROP_TIP          => NULL,
    ];

}