<?php

class App_Component_Authentication_Worker extends Lib_Authentication_Abstract
{
    const TOKEN = App_Component_Asociatie_Object::PROP_TOKEN;
    
    protected $Identity;
    
    public function __construct()
    {
        parent::__construct();
        
        $this->readIdentity();
    }
    
    /**
     * 
     * @param int $asociatieId
     * @param int $rememberMe
     */
    public function storeIdentity( $asociatieId, $rememberMe = true )
    {
        parent::storeIdentity( $asociatieId, $rememberMe );
        
        $this->setIdentity( $asociatieId );
        
        $this->generateToken();
    }
    
    protected function setIdentity( $asociatieId )
    {
        $this->Identity = ( $asociatieId instanceof App_Component_Asociatie_Object ) ?
            $asociatieId :
            $this->getAsociatie( $asociatieId );
        
    }
    
    /**
     * @return App_Component_Asociatie_Object | NULL
     */
    public function getIdentity( $checkIdentity = false )
    {
        if( $checkIdentity
            && !$this->Identity )
        {
            throw new App_Component_Authentication_Exception_IdentityNotSet();
        }
        
        return $this->Identity;
    }
    
    protected function readIdentity()
    {
        $asociatieId = parent::getIdentity();
        if( $asociatieId === null )
        {
            $this->clearIdentity();
        }
        else
        {
            $Asociatie = $this->getAsociatie( $asociatieId );
            if( $Asociatie
                && $Asociatie->isAccountActive() )
            {
                $this->setIdentity( $Asociatie );
            }
            else
            {
                $this->clearIdentity();
            }
        }
        
    }
    
    public function clearIdentity()
    {
        parent::clearIdentity();
        
        $this->Identity = NULL;
    }
    
    protected function getAsociatie( $asociatieId )
    {
        $Asociatie = App_Component_Asociatie_Factory::getInstance()->find( $asociatieId );
        
        return $Asociatie;
    }
    
    protected function generateToken()
    {
        $Asociatie = $this->getIdentity( true );
        
        $Asociatie->generateToken();
    }
    
    public function checkToken( $token )
    {
        $Asociatie = $this->getIdentity( true );
        
        $isValid = $Asociatie && $Asociatie->checkToken( $token );
        
        return $isValid;
    }
    
    public function getToken()
    {
        $Asociatie = $this->getIdentity( true );
        
        $token = $Asociatie->getToken();
        
        return $token;
    }
    
    public function getIdentityId()
    {
        $Asociatie = $this->getIdentity( true );
        
        $token = $Asociatie->getId();
        
        return $token;
    }
    
}
