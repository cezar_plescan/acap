<?php

class App_Component_Authentication_Validator_Token extends Lib_Validator_Abstract
{
    const ERR_NOT_AUTHENTICATED = App_Component_Authentication_Validator_Identity::ERR_INVALID;
    const ERR_INVALID           = 'invalid_token';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_INVALID           => 'Token invalid.',
        ]);
    }

    /**
     * 
     * @param array { App_Component_Asociatie_Object::PROP_TOKEN }
     */
    protected function validate( $token )
    {
        $IdentityValidator = new App_Component_Authentication_Validator_Identity();
        
        if( !$IdentityValidator->isValid() )
        {
            $this->setError( $IdentityValidator );
        }
        else
        {
            if( empty( $token ) || !Lib_Authentication_Manager::getWorker()->checkToken( $token ) )
            {
                $this->setError( self::ERR_INVALID );
            }
        }
    }
    
}
