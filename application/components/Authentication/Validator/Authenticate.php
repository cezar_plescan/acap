<?php

/**
 * 
 */
class App_Component_Authentication_Validator_Authenticate extends Lib_Validator_Abstract
{
    const ERR_INVALID   = 'invalid_credentials';
    
    protected function init()
    {
        $this->addMessageTemplates([
            self::ERR_INVALID           => 'Datele introduse sunt incorecte',
        ]);
    }

    /**
     *
     * @param array $credentials 
     *      { App_Component_Asociatie_Object::PROP_USERNAME, 
     *        App_Component_Asociatie_Object::PROP_PASSWORD }
     */
    protected function validate( $credentials )
    {
        // check for the existance of both values
        if( !isset( $credentials[ App_Component_Asociatie_Object::PROP_USERNAME ] )
            || !isset( $credentials[ App_Component_Asociatie_Object::PROP_PASSWORD ] ) )
        {
            $this->setError( self::ERR_INVALID );
        }

        $username = trim( $credentials[ App_Component_Asociatie_Object::PROP_USERNAME ] );
        $password = $credentials[ App_Component_Asociatie_Object::PROP_PASSWORD ];

        // extract the Asociatie object
        $Asociatie = App_Component_Asociatie_Factory::getInstance()->findByUsername( $username );

        if( $Asociatie === NULL
            || !$Asociatie->checkPassword( $password ) )
        {
            $this->setError( self::ERR_INVALID );
        }

        // prepare return data
        unset( $credentials[ App_Component_Asociatie_Object::PROP_PASSWORD ] );
        $credentials[ App_Component_Asociatie_Object::PROP_USERNAME ] = $username;
        $credentials[ App_Component_Asociatie_Object::PROP_ID ] = $Asociatie->getId();
        
        return $credentials;
    }
    
}
