<?php

/**
 * @todo Erorile invalid_username si invalid_password se vor elimina in etapa de
 * productie; ele sunt folosite momentat doar pentru debugging.
 *
 */
class App_Component_Authentication_Validator_Identity extends Lib_Validator_Abstract
{
    const ERR_INVALID = 'not_authenticated';
    
    protected function init()
    {
        $this->addMessageTemplates([
            self::ERR_INVALID           => 'Nu sunteți autentificat.',
        ]);
    }

    /**
     *
     * @no-params
     */
    protected function validate( $params )
    {
        if( !Lib_Authentication_Manager::getWorker()->hasIdentity() )
        {
            $this->setError( self::ERR_INVALID );
        }
    }
    
}
