<?php

class App_Component_Asociatie_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    /**
     * Create a user account
     *
     * @param array $data
     * @return App_Component_Asociatie_Object The user instance, or OPERATION_ERROR if errors occured.
     */
    public function createAccount(array $data)
    {
        // filter input data
        $data = Lib_Tools::filterArray( $data, [
            App_Component_Asociatie_Object::PROP_ADRESA,
            App_Component_Asociatie_Object::PROP_COD_FISCAL,
            App_Component_Asociatie_Object::PROP_DENUMIRE,
            App_Component_Asociatie_Object::PROP_EMAIL,
            App_Component_Asociatie_Object::PROP_JUDET,
            App_Component_Asociatie_Object::PROP_LOCALITATE,
            App_Component_Asociatie_Object::PROP_PASSWORD,
            App_Component_Asociatie_Object::PROP_TELEFON,
        ]);

        // set default values
        $data[App_Component_Asociatie_Object::PROP_STATUS]    = App_Component_Asociatie_Object::VAL_STATUS_ACTIVE;
        $data[App_Component_Asociatie_Object::PROP_USERNAME]  = $data[App_Component_Asociatie_Object::PROP_COD_FISCAL];

        $self = $this;

        // create and save the user instance

        /* @var $Asociatie App_Component_Asociatie_Object */
        $Asociatie = $self->create( [ $data ] );
        $Asociatie->generateActivationCode();
        $Asociatie->save();

        // send a confirmation email
        $Asociatie->sendAccountCreatedEmail();

        return $Asociatie;
    }

    /**
     * Finds an active User by his username
     *
     * @param string $username
     * @return App_Component_Asociatie_Object Return the found User or NULL if it is not found
     */
    public function findByUsername( $username )
    {
        $User = $this->find([
            App_Component_Asociatie_Object::PROP_USERNAME     => trim( $username ),
            App_Component_Asociatie_Object::PROP_STATUS       => App_Component_Asociatie_Object::VAL_STATUS_ACTIVE,
        ]);

        return $User;
    }
}
