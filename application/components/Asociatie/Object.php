<?php

class App_Component_Asociatie_Object extends Lib_Component_ORM_Object
{
    const PROP_USERNAME      = 'username';
    const PROP_STATUS        = 'status';
    const PROP_COD_FISCAL    = 'cod_fiscal';
    const PROP_DENUMIRE      = 'denumire';
    const PROP_ADRESA        = 'adresa';
    const PROP_LOCALITATE    = 'localitate';
    const PROP_JUDET         = 'judet';
    const PROP_EMAIL         = 'email';
    const PROP_TELEFON       = 'telefon';
    const PROP_PASSWORD      = 'password';
    const PROP_SALT          = 'salt';
    const PROP_ACTIVATION_CODE = 'activation_code';
    const PROP_TOKEN         = 'token';

    // values for STATUS field
    const VAL_STATUS_INACTIVE   = 0; // new account
    const VAL_STATUS_ACTIVE     = 1;
    const VAL_STATUS_BLOCKED    = 2;
    const VAL_STATUS_DELETED    = 3;

    protected $_properties = array(
        self::PROP_ID            => null,
        self::PROP_USERNAME      => null,
        self::PROP_STATUS        => self::VAL_STATUS_INACTIVE,
        self::PROP_COD_FISCAL    => null,
        self::PROP_DENUMIRE      => null,
        self::PROP_ADRESA        => null,
        self::PROP_LOCALITATE    => null,
        self::PROP_JUDET         => null,
        self::PROP_EMAIL         => null,
        self::PROP_TELEFON       => null,
        self::PROP_PASSWORD      => null,
        self::PROP_SALT          => null,
        self::PROP_ACTIVATION_CODE => null,
        self::PROP_TOKEN         => null,
        self::PROP_TIMESTAMP     => null,
    );

    protected
            $_cache = [];

    protected function _preInsert()
    {
        $this->setProperty( self::PROP_PASSWORD, $this->encryptPassword($this->getProperty( self::PROP_PASSWORD )) );
    }

    protected function _preUpdate()
    {

    }

    /**
     * Encrypt a password
     *
     * @param string $password The plain password
     *
     * @return string The encrypted password
     */
    protected function encryptPassword($password)
    {
        $salt = $this->getProperty( self::PROP_SALT );
        if (null === $salt)
        {
            $salt = $this->generateSalt();
        }

        $encryptedPassword = sha1(sha1($password) . $salt);

        return $encryptedPassword;
    }

    public function checkPassword($plainPassword)
    {
        if ( 0 == strcmp($this->encryptPassword($plainPassword), $this->getProperty( self::PROP_PASSWORD )) )
        {
            return true;
        }

        return false;
    }

    public function generateToken()
    {
        $token = Lib_Tools::generateCode();
        $this->setProperty( self::PROP_TOKEN, $token );

        $this->save();

        return $this;
    }

    public function checkToken($token)
    {
        $check = ( strcmp( $token, $this->getProperty( self::PROP_TOKEN ) ) === 0 );

        return $check;
    }

    public function getToken()
    {
        return $this->getProperty( self::PROP_TOKEN );
    }

    /**
     * Generates a salt and saves it in the Object
     *
     * @return sttring The generated salt
     */
    protected function generateSalt()
    {
        $salt = Lib_Tools::generateCode();
        $this->setProperty( self::PROP_SALT, $salt );

        return $salt;
    }

    /**
     * Check if the account is active.
     *
     * @return bool
     */
    public function isAccountActive()
    {
        $active = ($this->getProperty( self::PROP_STATUS ) == self::VAL_STATUS_ACTIVE);

        return $active;
    }

    /**
     * Generates an activation code and saves it in the Object
     *
     * @return string The generated code
     */
    public function generateActivationCode()
    {
        $code = Lib_Tools::generateCode();

        $this->setProperty( self::PROP_ACTIVATION_CODE, $code );

        return $code;
    }

    public function sendAccountCreatedEmail()
    {
        // to do ...
    }

    /** !! se foloseste getLunaCurenta !!
     * Returneaza:
     * Daca exista o luna inchisa - urmatoarea luna
     * Altfel - luna in care s-a inregistrat prima cheltuiala
     *
     * @return int
     */
    public function _getLunaDeLucru_()
    {
        $cacheId = 'luna-de-lucru';

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $monthId = NULL;

            $ultimaLunaInchisa = App_Component_OutputLista_Factory::getInstance()->getLastClosedMonthId( $this->getId() );
            if ( $ultimaLunaInchisa )
            {
                $monthId = Lib_Date::getInstance()->addMonths( $ultimaLunaInchisa, 1 );
            }
            else
            {
                $monthId = App_Component_Cheltuiala_Factory::getInstance()->getFirstMonth( $this->getId() );
            }

            $this->_cache[ $cacheId ] = $monthId;
        }

        return $this->_cache[ $cacheId ];
    }

    public function getDenumire()
    {
        return $this->getProperty( self::PROP_DENUMIRE );
    }

    public function getIdentityQuery()
    {
        $sql = $this->getDb()->getUserQuery($this->getProperty( self::PROP_ID ));

        return $sql;
    }

    /**
     * Returneaza o lista cu obiecte de tip ConsumComun
     */
    public function getConsumuri()
    {
        if (!isset($this->_cache['consumuri_comune']))
        {
            $ConsumAsociatieFactory = App_Component_Consum_Asociatie_Factory::getInstance();
            $Consumuri = $ConsumAsociatieFactory->getAll();

            $this->_cache['consumuri_comune'] = $Consumuri;
        }

        return $this->_cache['consumuri_comune'];
    }

    /**
     * Returneaza un consum comun (pe intreaga Asociatie)
     *
     * @param string $codConsum
     *
     * @return App_Component_Consum_Asociatie_Object
     */
    public function getConsum($codConsum)
    {
        $Consumuri = $this->getConsumuri();

        if ( isset($Consumuri[ $codConsum ]) )
        {
            $Consum = $Consumuri[ $codConsum ];
        }
        else
        {
            $Consum = null;
        }

        return $Consum;
    }

    /**
     * Returneaza valoarea unui consum comun
     *
     * @param string $codConsum
     * @return string|null
     */
    public function getValoareConsum($codConsum)
    {
        $Consum = $this->getConsum( $codConsum );

        if ( null !== $Consum )
        {
            $valoareConsum = $Consum->getValue();
        }
        else
        {
            $valoareConsum = null;
        }

        return $valoareConsum;
    }

    public function getParametri()
    {
        if (!isset($this->_cache['parametri']))
        {
            $ParametruAsociatieFactory = App_Component_AsociatieParametru_Factory::getInstance();
            $Parametri = $ParametruAsociatieFactory->getAll( $this->getId(), $this->getLunaCurenta() );

            $this->_cache['parametri'] = $Parametri;
        }

        return $this->_cache['parametri'];
    }

    public function getValoareParametru($parametruName)
    {
        $parametri = $this->getParametri();
        if (isset($parametri[ $parametruName ]))
        {
            $valoareParametru = $parametri[ $parametruName ];
        }
        else
        {
            try
            {
                $valoareParametru = Lib_Application::getInstance()->getConfig( [ 'asociatie', $parametruName ] );
            }
            catch ( Lib_Application_Exception_InvalidConfigKey $e )
            {
                $valoareParametru = NULL;
            }
        }

        return $valoareParametru;
    }

    public function getAdresaCompleta()
    {
        $adresa = $this->getProperty( self::PROP_ADRESA );
        $localitate = $this->getProperty( self::PROP_LOCALITATE );

        $adresaCompleta = $localitate . ', ' . $adresa;

        return $adresaCompleta;
    }

    public function getCodFiscal()
    {
        return $this->getProperty( self::PROP_COD_FISCAL );
    }

    public function getUsername()
    {
        return $this->getProperty( self::PROP_USERNAME );
    }

    public function getTelefon()
    {
        return $this->getProperty( self::PROP_TELEFON );
    }

    public function getMonthName( $monthId )
    {
        $parts = str_split( $monthId, 4 );

        $year = $parts[0];
        $month = (int)$parts[1];

        $monthText = Lib_Date::getMonthLabel($month) . ' ' . $year;

        return $monthText;
    }

    public function getConfiguration()
    {
        $cacheId = 'config';
        
        if( !isset( $this->_cache[ $cacheId ] ) )
        {
            $Config = new App_Component_Configuratie_Object( $this );
            
            $this->_cache[ $cacheId ] = $Config;
        }
        
        return $this->_cache[ $cacheId ];
    }
    
    public function getLunaCurenta()
    {
        $lunaCurenta = App_Component_AsociatieParametru_Factory::getInstance()->getValoare( App_Component_AsociatieParametru_Factory::LUNA_CURENTA , $this->getId() );
        
        return $lunaCurenta;
    }
    
}