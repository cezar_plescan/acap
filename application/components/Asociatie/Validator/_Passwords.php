<?php

class App_Model_Asociatie_Validator_Passwords extends App_Validator_Abstract
{
    const OPT_OLD        = 'old';
    const OPT_CONFIRM    = 'confirm';
    const OPT_USER_ID    = 'id';

    const ERR_INCORRECT  = 'invalid';
    const ERR_MIN        = 'err_min';
    const ERR_MAX        = 'err_max';
    const ERR_CONFIRM    = 'err_confirm';

    const MIN_LENGTH     = App_Component_Asociatie_Object::PASSWORD_MIN_LENGTH;
    const MAX_LENGTH     = App_Component_Asociatie_Object::PASSWORD_MAX_LENGTH;

    const VAL_OLD_PASSWORD      = 'val_old_pass';
    const VAL_CONFIRM_PASSWORD  = 'val_confirm_pass';

    protected $errorTemplates = array(
        self::ERR_INCORRECT => 'The current password is incorrect',
        self::ERR_MIN => 'The new password must have at least %value% characters.',
        self::ERR_MAX => 'The new password must have at most %value% characters.',
        self::ERR_CONFIRM => 'The confirmation password is incorrect',
    );

    protected $requiredValues = array(
        self::VAL_OLD_PASSWORD,
        self::VAL_CONFIRM_PASSWORD,
    );

    /**
    *
    *
    */
    public function _isValid($password)
    {
        $old_password = $this->validateOldPassword();
        $password = $this->validatePassword();
        $confirm_password = $this->validateConfirmPassword();

        return $password;
    }

    protected function isEmpty($value)
    {

        $check =
            strlen($this->getAdditionalValue(self::VAL_OLD_PASSWORD)) ||
            strlen($value) ||
            strlen($this->getAdditionalValue(self::VAL_CONFIRM_PASSWORD));

        return !$check;
    }

    protected function validateOldPassword()
    {
        $oldPassword = $this->getAdditionalValue(self::VAL_OLD_PASSWORD);

        $user_id = $this->getOption(self::OPT_USER_ID);
        $userModel = new App_Component_Asociatie_Object;
        $userModel->find($user_id);
        $userPassword = $userModel->{App_Component_Asociatie_Object::PROP_PASSWORD};

        if (!$userModel->checkPassword($oldPassword))
        {
            $this->_setAdditionalError(self::VAL_OLD_PASSWORD, self::ERR_INCORRECT);
        }

        return $oldPassword;
    }

    protected function validatePassword()
    {
        $password = $this->getValue();

        $pass_length = strlen($password);

        if ($pass_length > self::MAX_LENGTH)
        {
            $this->_setError(self::ERR_MAX, array('value' => self::MAX_LENGTH));
        }
        else if ($pass_length < self::MIN_LENGTH)
        {
            $this->_setError(self::ERR_MIN, array('value' => self::MIN_LENGTH));
        }

        return $password;
    }

    protected function validateConfirmPassword()
    {
        $password = $this->getValue();

        $confirmPassword = $this->getAdditionalValue(self::VAL_CONFIRM_PASSWORD);

        if(strcmp($password, $confirmPassword) != 0)
        {
            $this->_setAdditionalError(self::VAL_CONFIRM_PASSWORD, self::ERR_CONFIRM);
        }

        return $confirmPassword;
    }
}
