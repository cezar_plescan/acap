<?php

class App_Component_Asociatie_Validator_Id extends Lib_Validator_Abstract
{
    const ERR_INACTIVE     = 'inactiv';

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID  => 'Cont invalid',
            self::ERR_INACTIVE => 'Cont inactiv',
        ));

    }

    protected function _isValid( $id )
    {
        $id = (int)$id;

        $Account = App_Component_Asociatie_Factory::getInstance()->find( $id );

        if( !$Account )
        {
            $this->_setError( self::ERR_INVALID );
        }

        if( !$Account->isAccountActive() )
        {
            $this->_setError( self::ERR_INACTIVE );
        }

        return $id;
    }

}

