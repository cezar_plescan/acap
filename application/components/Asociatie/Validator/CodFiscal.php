<?php

class App_Component_Asociatie_Validator_CodFiscal extends Lib_Validator_Abstract
{
    const ERR_DUPLICATE     = 'duplicate';

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_DUPLICATE     => 'Codul fiscal este deja înregistrat',
        ));

    }

    protected function _isValid ($value)
    {
        $valid = $this->checkFormat($value);
        if (!$valid)
        {
            $this->_setError(self::ERR_INVALID);
        }

        $duplicate = $this->checkDuplicate($value);
        if ($duplicate)
        {
            $this->_setError(self::ERR_DUPLICATE);
        }

        return $value;
    }

    protected function checkFormat($value)
    {
        // read parameter values from the application configuration
        $Application = Lib_Application::getInstance();

        $validateFormat = $Application->getConfig(['validators', 'cod_fiscal', 'validate_format'], true);

        // skip validation
        if (!$validateFormat)
        {
            return true;
        }

        $maxLength      = $Application->getConfig(['validators', 'cod_fiscal', 'max_length']);
        $validationKey  = $Application->getConfig(['validators', 'cod_fiscal', 'validation_key']);

        // Verifică dacă e întreg
        if (!is_numeric($value))
        {
            return false;
        }

        $intValue = (int)$value;

        if (strcmp($value, $intValue))
        {
            return false;
        }

        if ($intValue < 0)
        {
            return false;
        }

        // se elimina eventualele '0'-uri
        $value = (string)$value;

        // verific lungimea codului
        if (strlen($value) > $maxLength)
        {
            return false;
        }

        // Extrage cifra de control
        $cifraControl = substr($value, -1);
        $value = substr($value, 0, -1);

        // Adaugă zerouri în prefix pâna când CIF are 9 cifre
        while (strlen($value) < ($maxLength - 1) )
        {
            $value = '0' . $value;
        }

        // Calculează suma de control;
        // nu mai este nevoie de inversare (avem zerouri în primele poziții)
        $suma = 0;
        for($i = 0; $i < $maxLength -1 ; $i++)
        {
            $suma += $value[$i] * $validationKey[$i];
        }

        $rest = $suma*10 % 11;

        if ($rest == 10)
        {
            $rest=0;
        }

        return ($rest == $cifraControl);
    }

    protected function checkDuplicate($value)
    {
        $userFactory = App_Component_Asociatie_Factory::getInstance();
        $count = $userFactory->count(array(
            App_Component_Asociatie_Object::PROP_COD_FISCAL   => $value,
            App_Component_Asociatie_Object::PROP_STATUS       => new Lib_Component_ORM_Db_Operator_NotEqual(App_Component_Asociatie_Object::VAL_STATUS_DELETED),
        ));

        return (bool)$count;
    }
}

