<?php

abstract class App_Component_Asociatie_Validator_BaseAbstract extends Lib_Validator_Abstract
{
    const OPT_ASOCIATIE_ID          = 'asociatie_id';
    const OPT_CLIENT_TIME           = 'client_time';
    
    protected function init()
    {
        parent::init();
        
        $asociatieId = $this->getOption( self::OPT_ASOCIATIE_ID );
        if( $asociatieId === NULL )
        {
            throw new App_Component_Asociatie_Validator_Exception_AsociatieIdOptionNotSet();
        }
    }

    protected function getAsociatieId()
    {
        $aid = $this->getOption( self::OPT_ASOCIATIE_ID );
        
        return $aid;
    }
    
}