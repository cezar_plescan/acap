<?php

class App_Component_Asociatie_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'asociatie';
    
    static protected $instance;
    
    public function getUserQuery( $userId )
    {
        $sql = $this->whereEqual($userId, App_Component_Asociatie_Object::PROP_ID);

        return $sql;
    }
}
