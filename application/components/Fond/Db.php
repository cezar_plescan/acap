<?php

class App_Component_Fond_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'fond';
    
    static protected $instance;
    
    public function getRepartizare( $fondId )
    {
        $sql = $this->selectStmt( [ App_Component_FondTip_Object::PROP_REPARTIZARE ] ) .
               $this->leftJoin( App_Component_FondTip_Db::TABLE, App_Component_FondTip_Object::PROP_ID, App_Component_Fond_Object::PROP_TIP_ID ) .
               $this->where( [ App_Component_Fond_Object::PROP_ID => $fondId ] );
        
        $repartizare = $this->queryAndFetchColumn( $sql );
        
        return $repartizare;
    }
    
    public function getTip( $fondId )
    {
        $sql = $this->selectStmt( [ App_Component_FondTip_Object::PROP_TIP ] ) .
               $this->leftJoin( App_Component_FondTip_Db::TABLE, App_Component_FondTip_Object::PROP_ID, App_Component_Fond_Object::PROP_TIP_ID ) .
               $this->where( [ App_Component_Fond_Object::PROP_ID => $fondId ] );
        
        $tip = $this->queryAndFetchColumn( $sql );
        
        return $tip;
    }
    
}
