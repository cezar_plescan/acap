<?php

class App_Component_Fond_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    /**
     * @todo implementare metodă
     * 
     * @return array
     */
    public function getFonduri( $asociatieId, $month = NULL )
    {
        $cacheId = "fonduri-$asociatieId";
        
        if( $this->notCache( $cacheId ) )
        {
            $columns = [
                App_Component_Fond_Object::PROP_ID,
                App_Component_Fond_Object::PROP_TIP_ID,
                App_Component_Fond_Object::PROP_PARAMETRI,
            ];
            
            $conditions = [
                App_Component_Fond_Object::PROP_ASOCIATIE_ID    => $asociatieId,
            ];
            
            $conditions += App_Component_Fond_Object::getActiveStatusCriteria();
            
            $fonduriData = $this->retrieveData( $columns, $conditions );
            foreach( $fonduriData as $index => $fondRow )
            {
                $fondRow = $this->parseFondTip( $fondRow );
                $fondRow = $this->parseFondParametri( $fondRow );
                
                $fonduriData[ $index ] = $fondRow;
            }
            
            $this->writeCache( $cacheId, $fonduriData );
        }
        
        return $this->readCache( $cacheId );
    }
    
    public function getFonduriIds( $asociatieId )
    {
        $cacheId = "fonduri-ids-$asociatieId";
        
        if( $this->notCache( $cacheId ) )
        {
            $fonduriIds = [];
            
            $fonduriCacheId = "fonduri-$asociatieId";
            if( $this->hasCache( $fonduriCacheId ) )
            {
                $fonduri = $this->readCache( $fonduriCacheId );
                foreach( $fonduri as $fond )
                {
                    $fondId = $fond[ App_Component_Fond_Object::PROP_ID ];
                    
                    $fonduriIds[] = $fondId;
                }
            }
            else
            {
                $columns = [
                    App_Component_Fond_Object::PROP_ID,
                ];

                $conditions = [
                    App_Component_Fond_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                ];

                $conditions += App_Component_Fond_Object::getActiveStatusCriteria();

                $fonduriData = $this->retrieveData( $columns, $conditions );
                foreach( $fonduriData as $fondRow )
                {
                    $fondId = $fondRow[ App_Component_Fond_Object::PROP_ID ];
                    
                    $fonduriIds[] = $fondId;
                }
            }
            
            $this->writeCache( $cacheId, $fonduriIds );
        }
        
        return $this->readCache( $cacheId );
    }
    
    public function isRepartizareMandatory( $fondId )
    {
        $repartizare = $this->getRepartizareFond( $fondId );
        
        $mandatory = ( $repartizare == App_Component_FondTip_Object::VAL_REPARTIZARE_MANDATORY );
        
        return $mandatory;
    }
    
    public function isRepartizareOptional( $fondId )
    {
        $repartizare = $this->getRepartizareFond( $fondId );
        
        $optional = ( $repartizare == App_Component_FondTip_Object::VAL_REPARTIZARE_OPTIONAL );
        
        return $optional;
    }
    
    public function isRepartizareNone( $fondId )
    {
        $repartizare = $this->getRepartizareFond( $fondId );
        
        $none = ( $repartizare == App_Component_FondTip_Object::VAL_REPARTIZARE_NONE );
        
        return $none;
    }
    
    public function isTipIntretinere( $fondId )
    {
        $tip = $this->getTipFond( $fondId );
        
        return ( $tip == App_Component_FondTip_Factory::TIP_INTRETINERE );
    }
    
    protected function parseFondTip( array $fond )
    {
        $tipId = $fond[ App_Component_Fond_Object::PROP_TIP_ID ];
        
        $fondTipData = App_Component_FondTip_Factory::getInstance()->getTipData( $tipId );
        
        $fond[ App_Component_Fond_Object::DENUMIRE ]    = $fondTipData[ App_Component_FondTip_Object::PROP_DENUMIRE ];
        $fond[ App_Component_Fond_Object::REPARTIZARE ] = $fondTipData[ App_Component_FondTip_Object::PROP_REPARTIZARE ];
        
        return $fond;
    }
    
    protected function parseFondParametri( array $fond )
    {
        if( isset( $fond[ App_Component_Fond_Object::PROP_PARAMETRI ] ) )
        {
            $parametri = json_decode( $fond[ App_Component_Fond_Object::PROP_PARAMETRI ], TRUE );

            if( !is_array( $parametri ) )
            {
                throw new DomainException( '"parametri" is not an array for fond ' . $fond[ App_Component_Fond_Object::PROP_ID ] );
            }

            $fond = array_merge( $fond, $parametri );
        }

        unset( $fond[ App_Component_Fond_Object::PROP_PARAMETRI ] );
        
        return $fond;
    }
    
    protected function getRepartizareFond( $fondId )
    {
        $DB = $this->getDb();
        
        $repartizare = $DB->getRepartizare( $fondId );
        
        return $repartizare;
    }
    
    protected function getTipFond( $fondId )
    {
        $DB = $this->getDb();
        
        $tip = $DB->getTip( $fondId );
        
        return $tip;
    }
    
}
