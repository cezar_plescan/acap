<?php

class App_Component_Fond_Object extends Lib_Component_ORM_Object
{
    const PROP_ASOCIATIE_ID     = 'asociatie_id';
    const PROP_TIP_ID           = 'tip_id';
    const PROP_PARAMETRI        = 'parametri';
    const PROP_STATUS           = 'status';
    
    const DENUMIRE              = 'denumire';
    const REPARTIZARE           = 'repartizare';
    
    const VAL_STATUS_ACTIVE     = 0;
    const VAL_STATUS_DELETED    = 1;
    
    public static function getActiveStatusCriteria()
    {
        $criteria = [
            self::PROP_STATUS => self::VAL_STATUS_ACTIVE,
        ];
        
        return $criteria;
    }
    
}
