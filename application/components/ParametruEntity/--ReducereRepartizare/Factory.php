<?php

class App_Component_Parametru_ReducereRepartizare_Factory extends App_Component_ParametruEntity_Factory
{
    static protected $instance;

    // parametri disponibili
    const PARAM_REDUCERE    = 'reducere';
    const PARAM_SPATII      = 'spatii';

    static protected $denumiriNearticulate = [
        self::PARAM_REDUCERE    => 'Reducere',
        self::PARAM_SPATII      => 'Apartamente',
    ];

    static public function getDenumireNearticulata( $parametru )
    {
        return self::$denumiriNearticulate[ $parametru ];
    }

    /** (v)
     *
     * Returneaza parametrii reducerilor
     *
     * @param int $asociatieId
     *
     * @return array
     */
    public function getAllByReducere( $asociatieId )
    {
        $data = $this->getDb()->getAll( $asociatieId );
        $parametriData = [];

        foreach( $data as $rowData )
        {
            $reducereId = $rowData[ App_Component_Parametru_ReducereRepartizare_Object::PROP_REF_ID ];
            if ( !isset( $parametriData[ $reducereId ] ) )
            {
                $parametriData[ $reducereId ] = [];
            }

            $value = $rowData[ App_Component_Parametru_ReducereRepartizare_Object::PROP_VALUE ];
            $decodedValue = json_decode( $value, true );
            if( null !== $decodedValue )
            {
                $value = $decodedValue;
            }

            $parametriData[ $reducereId ][ $rowData[ App_Component_Parametru_ReducereRepartizare_Object::PROP_PARAMETRU ] ] = $value;
        }

        return $parametriData;
    }

}
