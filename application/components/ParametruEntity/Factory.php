<?php

abstract class App_Component_ParametruEntity_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

//    public function updateParameter( $refId, $parametru, $valoare )
//    {
//        $ParametruObject = $this->find([
//            App_Component_Parametru_Object::PROP_REF_ID     => $refId,
//            App_Component_Parametru_Object::PROP_PARAMETRU  => $parametru,
//        ]);
//        
//        if( $ParametruObject === NULL )
//        {
//            $ParametruObject = $this->createAndSave([[
//                App_Component_Parametru_Object::PROP_REF_ID     => $refId,
//                App_Component_Parametru_Object::PROP_PARAMETRU  => $parametru,
//                App_Component_Parametru_Object::PROP_VALUE      => $valoare,
//            ]]);
//        }
//        elseif( $valoare === NULL )
//        {
//            $ParametruObject->delete();
//        }
//        else
//        {
//            $ParametruObject->updateAndSave([
//                App_Component_Parametru_Object::PROP_VALUE      => $valoare
//            ]);
//        }
//    }
//    
//    
    //==========================================================================
    
    public function addParametri( $refId, array $parametri )
    {
        $DB = $this->getDb();
        
        foreach( $parametri as $parametru => $value )
        {
            $valueEncoded = $this->encodeValue( $value );
            
            $data = [
                App_Component_ParametruEntity_Object::PROP_REF_ID     => $refId,
                App_Component_ParametruEntity_Object::PROP_PARAMETRU  => $parametru,
                App_Component_ParametruEntity_Object::PROP_VALUE      => $valueEncoded,
            ];
            
            $DB->insert( $data );
        }
    }
    
    public function deleteParametri( $refId )
    {
        $this->getDb()->delete( [ App_Component_ParametruEntity_Object::PROP_REF_ID => $refId ] );
    }
    
    protected function encodeValue( $value )
    {
        return json_encode( $value );
    }
    
    protected function decodeValue( $value )
    {
        return json_decode( $value, true );
    }
    
    public function getValoare( $parametru, $refId )
    {
        $rows = $this->retrieveData([
            App_Component_ParametruEntity_Object::PROP_VALUE,
        ], [
            App_Component_ParametruEntity_Object::PROP_REF_ID     => $refId,
            App_Component_ParametruEntity_Object::PROP_PARAMETRU  => $parametru,
        ]);
        
        $valoare = isset( $rows[ 0 ] ) ?
                $this->decodeValue( $rows[ 0 ][ App_Component_ParametruEntity_Object::PROP_VALUE ] ) :
                NULL;
        
        return $valoare;
    }
    
    /**
     * 
     * @param type $parametru
     * @param array $references
     * @return type
     * 
     * @todo stocare in _cache
     */
    public function getParametruValueList( $parametru, array $references )
    {
        $list = [];
        
        $rows = $this->retrieveData([
            App_Component_ParametruEntity_Object::PROP_VALUE,
            App_Component_ParametruEntity_Object::PROP_REF_ID,
        ], [
            App_Component_ParametruEntity_Object::PROP_PARAMETRU  => $parametru,
            App_Component_ParametruEntity_Object::PROP_REF_ID     => $references,
        ]);
        
        foreach( $rows as $row )
        {
            $list[ $row[ App_Component_ParametruEntity_Object::PROP_REF_ID ] ] = $this->decodeValue( $row[ App_Component_ParametruEntity_Object::PROP_VALUE ] );
        }
        
        return $list;
    }
    
    /**
     * Returnează parametrii grupaţi după referinţă.
     * 
     * @param array $references Lista de referinţe pentru care se retunează parametrii
     * @return array
     */
    public function getParametriByReferences( array $references )
    {
        $parametri = $this->getParametriByCondition([
                App_Component_ParametruEntity_Object::PROP_REF_ID => $references, 
        ]);
        
        return $parametri;
    }
    
    /**
     * Returnează parametrii grupaţi după referinţă.
     * 
     * @param int $asociatieId
     * @return array
     */
    public function getParametriByAsociatie( $asociatieId )
    {
        $parametri = $this->getParametriByCondition([
                App_Component_ParametruEntity_Object::PROP_ASOCIATIE_ID => $asociatieId, 
        ]);
        
        return $parametri;
    }
    
    protected function getParametriByCondition( array $condition )
    {
        $parametri = [];
        
        $columns = [
                App_Component_ParametruEntity_Object::PROP_REF_ID,
                App_Component_ParametruEntity_Object::PROP_PARAMETRU,
                App_Component_ParametruEntity_Object::PROP_VALUE,
        ];
        
        $rows = $this->retrieveData( $columns, $condition );
        
        foreach( $rows as $row )
        {
            $refId = $row[ App_Component_ParametruEntity_Object::PROP_REF_ID ];
            
            if( !isset( $parametri[ $refId ] ) )
            {
                $parametri[ $refId ] = [];
            }
            
            $parameter = $row[ App_Component_ParametruEntity_Object::PROP_PARAMETRU ];
            $value = $this->decodeValue( $row[ App_Component_ParametruEntity_Object::PROP_VALUE ] );
            
            $parametri[ $refId ][ $parameter ] = $value;
        }
        
        return $parametri;
    }
    
}
