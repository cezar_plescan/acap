<?php

abstract class App_Component_ParametruEntity_Object extends Lib_Component_ORM_Object
{
    // properties
    const PROP_ASOCIATIE_ID      = '_asociatie_id';
    const PROP_REF_ID            = 'ref_id';
    const PROP_PARAMETRU         = 'parameter';
    const PROP_VALUE             = 'value';

    protected $_properties = [
        self::PROP_ID            => NULL,
        self::PROP_REF_ID        => NULL,
        self::PROP_PARAMETRU     => NULL,
        self::PROP_VALUE         => NULL,
        self::PROP_CREATED_AT    => NULL,
        self::PROP_UPDATED_AT    => NULL,
    ];

    public function getValoare()
    {
        $value = $this->getProperty( self::PROP_VALUE );

        return $value;
    }

    public function getParametru()
    {
        $parametru = $this->getProperty( self::PROP_PARAMETRU );

        return $parametru;
    }


}