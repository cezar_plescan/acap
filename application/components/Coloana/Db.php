<?php

class App_Component_Coloana_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'coloana';

    /** (o)
     *
     * @param int $month
     * @return array
     */
    public function getAll()
    {
        $columns = $this->quoteColumns([
            App_Component_Coloana_Object::PROP_ID,
            App_Component_Coloana_Object::CODE,
            App_Component_Coloana_Object::LABEL,
            App_Component_Coloana_Object::IS_LABEL_DISPLAYABLE,
            App_Component_Coloana_Object::VALUE_LABEL,
            App_Component_Coloana_Object::TYPE,
            App_Component_Coloana_Object::PRIORITY,
            App_Component_Coloana_Object::USED_BY,
            App_Component_Coloana_Object::TYPE,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->getActiveStatusQuery(),
                ])) .

                $this->orderBy(
                        [App_Component_Coloana_Object::ORDER]
                );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

    /** (o)
     *
     * @param int $month
     * @return array
     */
    public function getForCheltuieli()
    {
        $columns = $this->quoteColumns([
            App_Component_Coloana_Object::PROP_ID,
            App_Component_Coloana_Object::CODE,
            App_Component_Coloana_Object::LABEL,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereEqual( App_Component_Coloana_Object::TYPE_CHELTUIALA, App_Component_Coloana_Object::TYPE ),
                        $this->getActiveStatusQuery(),
                ])) .

                $this->orderBy(
                        [ App_Component_Coloana_Object::ORDER ]
                );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

    /** (o)
     *
     * @param int $month
     * @return array
     */
    public function getForIncasari()
    {
        $columns = $this->quoteColumns([
            App_Component_Coloana_Object::PROP_ID,
            App_Component_Coloana_Object::CODE,
            App_Component_Coloana_Object::LABEL,
            App_Component_Coloana_Object::ALT_LABEL,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereNotEqual( 0, App_Component_Coloana_Object::FOR_INCASARI ),
                        $this->getActiveStatusQuery(),
                ])) .

                $this->orderBy(
                        [ App_Component_Coloana_Object::ORDER ]
                );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

    /** (v)
     *
     * @param int $month
     * @return string
     */
    public function getActiveStatusQuery()
    {
        $sql = $this->whereEqual(App_Component_Coloana_Object::STATUS_ACTIVE, App_Component_Coloana_Object::STATUS);

        return $sql;
    }

}
