<?php

class App_Component_Coloana_Validator_ForCheltuiala extends Lib_Validator_Abstract
{

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID   => 'Coloana aleasă este incorectă',
            self::ERR_REQUIRED  => 'Alegeţi o coloană',
        ));
    }

    /** (v)
     *
     * @param type $coloanaId
     * @return type
     */
    protected function _isValid( $coloanaId )
    {
        $coloanaId = (int)$coloanaId;

        $coloane = App_Component_Coloana_Factory::getInstance()->getForCheltuieli();

        if( isset( $coloane[ $coloanaId ] ) )
        {
            return $coloanaId;
        }
        else
        {
            $this->_setError( self::ERR_INVALID );
        }

    }

}
