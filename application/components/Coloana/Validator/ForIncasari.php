<?php

class App_Component_Coloana_Validator_ForIncasari extends Lib_Validator_Abstract
{

    protected function init()
    {
        $this->addErrorTemplates([
            self::ERR_INVALID   => 'Specificaţi tipul încasării',
            self::ERR_REQUIRED  => 'Specificaţi tipul încasării',
        ]);
    }

    /** (v)
     *
     * @param type $coloanaId
     * @return type
     */
    protected function _isValid( $coloanaId )
    {
        $coloanaId = (int)$coloanaId;

        $coloane = App_Component_Coloana_Factory::getInstance()->getForIncasari();

        if( isset( $coloane[ $coloanaId ] ) )
        {
            return $coloanaId;
        }
        else
        {
            $this->_setError( self::ERR_INVALID );
        }

    }

}
