<?php

class App_Component_Coloana_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    // coduri coloane (acestea sunt valorile codurilor obiectelor de tip Coloana)
    const COLOANA_APA_RECE                  = 'apa_rece';
    const COLOANA_APA_CALDA                 = 'apa_calda';
    const COLOANA_CHELTUIELI_PERSOANA       = 'ch_persoane';
    const COLOANA_CHELTUIELI_SUPRAFATA      = 'ch_suprafata';
    const COLOANA_INCALZIRE                 = 'incalzire';
    const COLOANA_BENEFICIAR                = 'beneficiar';

    const COLOANA_RESTANTE                  = 'restante';
    const COLOANA_FOND_REPARATII            = 'fond_reparatii';
    const COLOANA_FOND_RULMENT              = 'fond_rulment';
    const COLOANA_PENALIZARI                = 'penalizari';

    const COLOANA_INTRETINERE               = 'total_luna';

    /** (o)
     * Returneaza toate coloanele
     *
     * @param int $month
     * @return array
     */
    public function getAll()
    {
        $coloaneData = $this->getDb()->getAll();
        $subcoloaneData = App_Component_Subcoloana_Factory::getInstance()->getAll();

        $data = [];

        foreach($coloaneData as $row)
        {
            $coloanaId = $row[ App_Component_Coloana_Object::PROP_ID ];

            $row[ App_Component_Coloana_Object::SUBCOLOANE ] = [];

            $data[ $coloanaId ] = $row;
        }

        // adaug subcoloanele
        foreach ($subcoloaneData as $subcoloanaId => $row)
        {
            $coloanaId = $row[ App_Component_Subcoloana_Object::COLOANA_ID ];
            unset( $row[ App_Component_Subcoloana_Object::COLOANA_ID ] );

            if ( isset( $data[ $coloanaId ] ) )
            {
                $data[ $coloanaId ][ App_Component_Coloana_Object::SUBCOLOANE ][ $subcoloanaId ] = $row;
            }
        }

        return $data;
    }

    /** (v)
     *
     * @return bool
     */
    public function coloanaHasValue( array $coloanaData )
    {
        $has = (NULL !== $coloanaData[ App_Component_Coloana_Object::VALUE_LABEL ]) || (0 == count( $coloanaData[ App_Component_Coloana_Object::SUBCOLOANE ] ));

        return $has;
    }

    /** (v)
     *
     * @return bool
     */
    public function getColoanaWidth( array $coloanaData )
    {
        $subcoloaneCount = count( $coloanaData[ App_Component_Coloana_Object::SUBCOLOANE ] );

        if( 0 == $subcoloaneCount )
        {
            $width = 1;
        }
        else
        {
            $width = $subcoloaneCount + ( $this->coloanaHasValue( $coloanaData ) ? 1 : 0 );
        }

        return $width;
    }

    /**
     *
     * @return array
     */
    public function getForCheltuieli()
    {
        $cacheId = 'cheltuieli';

        if( !isset( $this->_cache[ $cacheId ] ) )
        {
            $coloaneData = $this->getDb()->getForCheltuieli();

            $data = [];

            foreach( $coloaneData as $row )
            {
                $coloanaId = $row[ App_Component_Coloana_Object::PROP_ID ];

                $data[ $coloanaId ] = $row;
            }

            $this->_cache[ $cacheId ] = $data;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     *
     * @return array
     */
    public function getForIncasari()
    {
        $cacheId = 'incasari';

        if( !isset( $this->_cache[ $cacheId ] ) )
        {
            $coloaneData = $this->getDb()->getForIncasari();

            $data = [];

            foreach( $coloaneData as $row )
            {
                $coloanaId = $row[ App_Component_Coloana_Object::PROP_ID ];

                if( !empty( $row[ App_Component_Coloana_Object::ALT_LABEL ] ) )
                {
                    $row[ App_Component_Coloana_Object::LABEL ] = $row[ App_Component_Coloana_Object::ALT_LABEL ];
                }
                unset( $row[ App_Component_Coloana_Object::ALT_LABEL ] );

                $data[ $coloanaId ] = $row;
            }

            $this->_cache[ $cacheId ] = $data;
        }

        return $this->_cache[ $cacheId ];
    }


}
