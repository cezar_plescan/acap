<?php

class App_Component_Coloana_Object extends Lib_Component_ORM_Object
{
    // fields
    const CODE                  = 'code';
    const TYPE                  = 'type';
    const LABEL                 = 'label';
    const ALT_LABEL             = 'alt_label';
    const FOR_INCASARI          = 'for_incasari';
    const IS_LABEL_DISPLAYABLE  = 'is_displayable';
    const VALUE_LABEL           = 'value_label';
    const ORDER                 = 'order';
    const PRIORITY              = 'priority';
    const USED_BY               = 'used_by';
    const STATUS                = 'status';

    // field values
    const STATUS_ACTIVE     = 0;
    const STATUS_DELETED    = 1;

    const TYPE_CHELTUIALA       = 1;
    const TYPE_ALTE_CONTRIBUTII = 2;
    const TYPE_TOTAL            = 3;

    // other properties
    const SUBCOLOANE            = 'subcoloane';

    // other consts
    const DEFAULT_VALUE_LABEL   = 'valoare';
    const PARAMS                = 'params';

    protected $_properties = [
        self::PROP_ID                    => NULL,
        self::CODE                  => NULL,
        self::TYPE                  => NULL,
        self::LABEL                 => NULL,
        self::IS_LABEL_DISPLAYABLE  => 1,
        self::VALUE_LABEL           => self::DEFAULT_VALUE_LABEL,
        self::STATUS                => self::STATUS_ACTIVE,
        self::ORDER                 => NULL,
        self::PRIORITY              => NULL,
        self::USED_BY               => NULL,
    ];

    protected
            $_cache = [];

    public function getSubcoloane()
    {
        if (!isset($this->_cache['subcoloane']))
        {
            $SubcoloanaFactory = App_Component_Subcoloana_Factory::getInstance();
            $Subcoloane = $SubcoloanaFactory->getByColoana( $this->getId() );

            $this->_cache['subcoloane'] = $Subcoloane;
        }

        return $this->_cache['subcoloane'];
    }

    public function getLabel()
    {
        return $this->getProperty( self::LABEL );
    }

    public function getCode()
    {
        return $this->getProperty( self::CODE );
    }

}