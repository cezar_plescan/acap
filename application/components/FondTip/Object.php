<?php

class App_Component_FondTip_Object extends Lib_Component_ORM_Object
{
    const PROP_TIP              = 'tip';
    const PROP_DENUMIRE         = 'denumire';
    const PROP_REPARTIZARE      = 'repartizare';
    
    const VAL_REPARTIZARE_NONE      = 0;
    const VAL_REPARTIZARE_OPTIONAL  = 1;
    const VAL_REPARTIZARE_MANDATORY = 2;
    
}
