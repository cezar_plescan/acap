<?php

class App_Component_FondTip_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    const TIP_INTRETINERE   = 'intretinere';
    const TIP_REPARATII     = 'reparatii';
    const TIP_EXTRA         = 'extra';
    const TIP_PENALIZARI    = 'penalizari';
    
    public function getTipuri()
    {
        $cacheId = 'tipuri';
        
        if( $this->notCache( $cacheId ) )
        {
            $columns = [
                App_Component_FondTip_Object::PROP_ID,
                App_Component_FondTip_Object::PROP_TIP,
                App_Component_FondTip_Object::PROP_DENUMIRE,
                App_Component_FondTip_Object::PROP_REPARTIZARE,
            ];

            $tipuri = $this->retrieveData( $columns );
            
            $this->writeCache( $cacheId, $tipuri );
        }
        
        return $this->readCache( $cacheId );
    }
    
    public function getTipData( $tipId )
    {
        $tipuri = $this->getTipuri();
        
        foreach( $tipuri as $tipData )
        {
            if( $tipData[ App_Component_FondTip_Object::PROP_ID ] == $tipId )
            {
                return $tipData;
            }
        }
        
        throw new UnexpectedValueException( 'Invalid tip_id: ' . $tipId );
    }
    
}
