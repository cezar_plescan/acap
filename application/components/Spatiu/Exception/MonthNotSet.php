<?php

class App_Component_Spatiu_Exception_MonthNotSet extends UnexpectedValueException
{
    public function __construct($message, $code, $previous)
    {
        $message = 'Luna nu este setata';

        parent::__construct($message, $code, $previous);
    }
}
