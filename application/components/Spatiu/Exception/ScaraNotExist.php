<?php

class App_Component_Spatiu_Exception_ScaraNotExist extends UnexpectedValueException
{
    public function __construct( $scaraId, $code, $previous )
    {
        parent::__construct( $scaraId, $code, $previous );
    }
}
