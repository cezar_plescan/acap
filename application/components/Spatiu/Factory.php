<?php

class App_Component_Spatiu_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    /**
     * Returneaza lista cu ID-urile tuturor spatiilor din Asociatie
     * 
     * @param int $asociatieId
     * @return array
     */
    public function getIDList( $asociatieId )
    {
        $cacheId = "id-list-$asociatieId";

        if( $this->notCache( $cacheId ) )
        {
            $columns = [ App_Component_Spatiu_Object::PROP_ID ];

            $conditions = 
                [ App_Component_Spatiu_Object::PROP_ASOCIATIE_ID => $asociatieId ] + 
                App_Component_Spatiu_Object::getActiveStatusCriteria();
            
            $results = $this->retrieveData( $columns, $conditions );
            
            $data = array_column( $results, App_Component_Spatiu_Object::PROP_ID );

            $this->writeCache( $cacheId, $data );
        }

        return $this->readCache( $cacheId );
    }
    
    public function getIDListByRepartizare( $asociatieId, $repartizare )
    {
        $list = [];
        
        if( $repartizare === '*' )
        {
            $list = $this->getIDList( $asociatieId );
        }
        elseif( is_array( $repartizare ) )
        {
            $repartizareConditions = [];
                    
            $blocuri    = @$repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_BLOCURI ];
            $scari      = @$repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI ];
            $spatii     = @$repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SPATII ];
            
            if( $blocuri )
            {
                $repartizareConditions[] = $this->getDb()->whereIn( $blocuri, App_Component_Spatiu_Object::PROP_BLOC_ID );
            }
            
            if( $scari )
            {
                $repartizareConditions[] = $this->getDb()->whereIn( $scari, App_Component_Spatiu_Object::PROP_SCARA_ID );
            }
            
            if( $spatii )
            {
                $repartizareConditions[] = $this->getDb()->whereIn( $spatii, App_Component_Spatiu_Object::PROP_ID );
            }
            
            $repartizareConditionsSql = $this->getDb()->whereOr( $repartizareConditions );
            
            $columns = [ App_Component_Spatiu_Object::PROP_ID ];
            
            $conditions = [
                $repartizareConditionsSql,
                App_Component_Spatiu_Object::PROP_ASOCIATIE_ID => $asociatieId,
            ] +
                App_Component_Spatiu_Object::getActiveStatusCriteria();
            
            $results = $this->retrieveData( $columns, $conditions );
            
            $list = array_column( $results, App_Component_Spatiu_Object::PROP_ID );
        }
        
        $list = array_unique( $list );
        
        return $list;
    }
    
    //==========================================================================
    
    /** (v)
     *
     * Extrage Spatiile dintr-o Asociatie, cu toate informatiile aferente (numar, etaj, parametri, consumuri, ...)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAll( $asociatieId, $month )
    {
        $cacheId = "spatii-$asociatieId-$month";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $spatii = $this->getDb()->getAll( $asociatieId, $month );

            $parametri  = App_Component_SpatiuParametru_Factory::getInstance()->getAllBySpatiu( $asociatieId, $month );
            $consumuri  = App_Component_Consum_Spatiu_Factory::getInstance()->getAllBySpatiu( $asociatieId, $month );
            $incasari   = App_Component_Incasare_Factory::getInstance()->getNotCountedBySpatiu( $asociatieId );

            $spatiiData = [];

            foreach( $spatii as $rowData )
            {
                $spatiuId = $rowData[ App_Component_Spatiu_Object::PROP_ID ];

                $rowData[ App_Component_Spatiu_Object::PARAMETRI ]  = isset( $parametri[ $spatiuId ] ) ? $parametri[ $spatiuId ] : [];
                $rowData[ App_Component_Spatiu_Object::CONSUMURI ]  = isset( $consumuri[ $spatiuId ] ) ? $consumuri[ $spatiuId ] : [];
                $rowData[ App_Component_Spatiu_Object::INCASARI ]   = isset( $incasari[ $spatiuId ] ) ? $incasari[ $spatiuId ] : [];

                $spatiiData[ $spatiuId ] = $rowData;
            }

            $this->_cache[ $cacheId ] = $spatiiData;
        }

        return $this->_cache[ $cacheId ];
    }

    /** (v)
     *
     * Extrage ID-urile spatiilor dintr-o Asociatie
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAllByAsociatie( $asociatieId, $month )
    {
        $cacheId = "spatii-asociatie-$asociatieId-$month";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $spatii = $this->getDb()->getAllByAsociatie( $asociatieId, $month );

            $this->_cache[ $cacheId ] = $spatii;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Returnează lista cu toate spaţiile din asociaţie.
     * Lista este neordonată. 
     * 
     * @param int $asociatieId
     * @return array
     */
    public function getList( $asociatieId )
    {
        $cacheId = "list-$asociatieId";

        if( $this->notCache( $cacheId ) )
        {
            $data = $this->getDb()->getAll( $asociatieId );
            $parametri = App_Component_SpatiuParametru_Factory::getInstance()->getAllBySpatiu( $asociatieId );
            
            foreach( $data as $index => $spatiuData )
            {
                $data[ $index ][ App_Component_Spatiu_Object::PROP_ETAJ_ORDER ] = $spatiuData[ App_Component_Spatiu_Object::PROP_ETAJ ];
                $data[ $index ][ App_Component_Spatiu_Object::PROP_ETAJ ] = App_Component_Spatiu_Property_Etaj::getLabel( $spatiuData[ App_Component_Spatiu_Object::PROP_ETAJ ] );
                
                // adaugare parametri
                $spatiuId = $spatiuData[ App_Component_Spatiu_Object::PROP_ID ];
                $parametriSpatiu = $parametri[ $spatiuId ];
                
                $data[ $index ] += $parametriSpatiu;
            }
            
            $this->writeCache( $cacheId, $data );
        }

        return $this->readCache( $cacheId );
    }
    
    public function getDenumireCompleta( $spatiuId )
    {
        $spatiuLinkInfo = $this->getDb()->getSpatiuLinkInfo( $spatiuId );

        $spatiuInfo = $spatiuLinkInfo[ App_Component_Spatiu_Object::PROP_NUMAR ] . ', Sc. ' . $spatiuLinkInfo[ App_Component_Spatiu_Object::SCARA ] . ', Bl. ' . $spatiuLinkInfo[ App_Component_Spatiu_Object::BLOC ];

        return $spatiuInfo;
    }

    public function getValoareIncasari( array $spatiu, $type = null )
    {
        $incasari = $spatiu[ App_Component_Spatiu_Object::INCASARI ];
        $valoareIncasari = 0;

        if( $incasari )
        {
            if( null !== $type )
            {
                $coloaneIncasari = App_Component_Coloana_Factory::getInstance()->getForIncasari();
            }

            foreach( $incasari as $incasare )
            {
                if( $incasare )
                {
                    if( null === $type || $coloaneIncasari[ $incasare[ App_Component_Incasare_Object::COLOANA_ID ] ][ App_Component_Coloana_Object::CODE ] == $type )
                    {
                        $valoareIncasari += $incasare[ App_Component_Incasare_Object::VALUE ];
                    }
                }
            }
        }

        return $valoareIncasari;
    }

    public function saveContributiiCurente( $asociatieId, $month )
    {
        $contributii = App_Component_ContributieSpatiu_Factory::getInstance()->getContributiiBySpatiu( $asociatieId, $month );

        // parse $contributii
        $contributiiData = [];
        foreach( $contributii as $spatiuId => $contributiiPerSpatiu )
        {
            foreach( $contributiiPerSpatiu as $contributieId => $contributieSpatiu )
            {
                $contributiiData[] = [
                    App_Component_SpatiuParametru_Object::PROP_REF_ID           => $spatiuId,
                    App_Component_SpatiuParametru_Object::PROP_PARAMETRU        => $contributieSpatiu[ App_Component_ContributieSpatiu_Object::COLOANA_CODE ],
                    App_Component_SpatiuParametru_Object::PROP_VALUE            => $contributieSpatiu[ App_Component_ContributieSpatiu_Object::VALUE ],
                    App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                ];
            }
        }

        App_Component_SpatiuParametru_Factory::getInstance()->update( $contributiiData );

    }

    public function add( array $spatiuValues )
    {
        $spatiuProperties = [
            App_Component_Spatiu_Object::PROP_ASOCIATIE_ID,
            App_Component_Spatiu_Object::PROP_ADDING_MONTH,
            App_Component_Spatiu_Object::PROP_SCARA_ID,
            App_Component_Spatiu_Object::PROP_NUMAR,
            App_Component_Spatiu_Object::PROP_ETAJ,
        ];
        
        $spatiuData = Lib_Tools::filterArray( $spatiuValues, $spatiuProperties );
        
        // generare proprietare redundanta `bloc_id`
        $scaraId = $spatiuData[ App_Component_Spatiu_Object::PROP_SCARA_ID ];
        $Scara = App_Component_Scara_Factory::getInstance()->find( $scaraId );
        if( !$Scara )
        {
            throw new App_Component_Spatiu_Exception_ScaraNotExist( $scaraId );
        }
        $blocId = $Scara->getProperty( App_Component_Scara_Object::PROP_BLOC_ID );
        $spatiuData[ App_Component_Spatiu_Object::PROP_BLOC_ID ] = $blocId;
        
        // adaugare in DB
        /* @var $Spatiu App_Component_Spatiu_Object */
        $Spatiu = $this->createAndSave( [ $spatiuData ] );
        
        // adaug parametrii spatiului
        $Spatiu->updateParametri( $spatiuValues );
        
        return $Spatiu;
    }

    public function edit( array $spatiuValues )
    {
        /* @var $Spatiu App_Component_Spatiu_Object */
        $Spatiu = $this->find( $spatiuValues[ App_Component_Spatiu_Object::PROP_ID ] );
        
        if( $Spatiu )
        {
            $Spatiu->setPropertyIfKeyExists( App_Component_Spatiu_Object::PROP_ETAJ, $spatiuValues );
            $Spatiu->setPropertyIfKeyExists( App_Component_Spatiu_Object::PROP_NUMAR, $spatiuValues );
            
            $Spatiu->updateParametri( $spatiuValues );
            
            $Spatiu->save();
        }
        
        return $Spatiu;
    }
    
    /**
     * 
     * @param array $spatiuData { App_Component_Spatiu_Object::ID }
     * 
     * @return App_Component_Spatiu_Object
     */
    public function remove( array $spatiuData )
    {
        $Spatiu = $this->find( $spatiuData[ App_Component_Spatiu_Object::PROP_ID ] );
        
        if( $Spatiu )
        {
            $Spatiu->remove();
        }
        
        return $Spatiu;
    }
    
    public function getSpatiiByScaraId( $scaraId )
    {
        $cacheId = "spatii-scara-$scaraId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $spatii = $this->fetchAll([
                App_Component_Spatiu_Object::PROP_SCARA_ID => $scaraId
            ]);

            $this->_cache[ $cacheId ] = $spatii;
        }

        return $this->_cache[ $cacheId ];
    }
    
    public function getSpatiiIdsByScaraId( $scaraId )
    {
        $cacheId = "spatii_ids-scara-$scaraId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $spatiiData = $this->getDb()->fetchAll([
                App_Component_Spatiu_Object::PROP_SCARA_ID => $scaraId
            ]);
            
            $ids = array_column( $spatiiData, App_Component_Spatiu_Object::PROP_ID );
            
            $this->_cache[ $cacheId ] = $ids;
        }

        return $this->_cache[ $cacheId ];
    }
    
    public function getSpatiiIdsByBlocId( $blocId )
    {
        $cacheId = "spatii_ids-bloc-$blocId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $spatiiData = $this->getDb()->fetchAll([
                App_Component_Spatiu_Object::PROP_BLOC_ID => $blocId
            ]);
            
            $ids = array_column( $spatiiData, App_Component_Spatiu_Object::PROP_ID );
            
            $this->_cache[ $cacheId ] = $ids;
        }

        return $this->_cache[ $cacheId ];
    }
    
}
