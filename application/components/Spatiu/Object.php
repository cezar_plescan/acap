<?php

class App_Component_Spatiu_Object extends Lib_Component_ORM_Object // implements App_Component_Cheltuiala_Interface_Target
{
    // properties
    const PROP_ASOCIATIE_ID = '_asociatie_id';
    const PROP_BLOC_ID      = 'bloc_id';
    const PROP_SCARA_ID     = 'scara_id';
    const PROP_NUMAR        = 'numar';
    const PROP_ETAJ         = 'etaj';
    const PROP_ADDING_MONTH = 'adding_month';
    const PROP_STATUS       = 'status';

    // other properties
    const PROP_ETAJ_ORDER   = 'etaj_order';
    
    const PROP_PROPRIETAR   = App_Component_SpatiuParametru_Factory::PARAMETRU_PROPRIETAR;
    const PROP_NR_PERSOANE  = App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE;
    const PROP_SUPRAFATA    = App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA;
    
    const MONTH         = 'month';
    const PARAMETRI     = 'parametri';
    const CONSUMURI     = 'consumuri';
    const INCASARI      = 'incasari';
    const SCARA         = 'scara';
    const BLOC          = 'bloc';

    // property values
    const VAL_STATUS_ACTIVE     = 0;
    const VAL_STATUS_DELETED    = 1;

    // criterii de repartizare a unor valori
    const REPARTIZARE_SUPRAFATA     = 's';
    const REPARTIZARE_PERSOANE      = 'p';
    const REPARTIZARE_APA_RECE      = 'ar';
    const REPARTIZARE_EGAL          = 'e';

    const ATTR_NUMAR_MAX_LENGTH         = 6;
    const ATTR_PROPRIETAR_MAX_LENGTH    = 60;
    const ATTR_NR_PERS_MAX_VALUE        = 20;
    const ATTR_NR_PERS_NULL_VALUE       = '-';
    const ATTR_SUPRAFATA_MAX_VALUE      = 1000;
    const ATTR_PARAMETRU_MAX_VALUE      = 999;
    
    protected $_properties = [
        self::PROP_ID            => NULL,
        self::PROP_ASOCIATIE_ID  => NULL,
        self::PROP_BLOC_ID       => NULL,
        self::PROP_SCARA_ID      => NULL,
        self::PROP_NUMAR         => NULL,
        self::PROP_ETAJ          => NULL,
        self::PROP_ADDING_MONTH  => NULL,
        self::PROP_STATUS        => self::VAL_STATUS_ACTIVE,
        self::PROP_CREATED_AT    => NULL,
        self::PROP_UPDATED_AT    => NULL,
    ];

    protected $_additionalProperties = [
        
    ];

    /**
     * @return array
     */
    static public function getActiveStatusCriteria()
    {
        $criteria = [
            self::PROP_STATUS    => self::VAL_STATUS_ACTIVE,
        ];
        
        return $criteria;
    }
    
    static public function getMainParametriList()
    {
        return [
            self::PROP_PROPRIETAR,
            self::PROP_NR_PERSOANE,
            self::PROP_SUPRAFATA,
        ];
    }

    static public function getAllPredefinedParametriList()
    {
        $parametriSuplimentariList = App_Component_DenumireParametruSpatiu_Factory::getInstance()->getParametriList();
        $mainParametriList = self::getMainParametriList();
        
        $allParametri = array_merge( $parametriSuplimentariList, $mainParametriList );
        
        return $allParametri;
    }
    
    public function getNumar()
    {
        return $this->getProperty( self::PROP_NUMAR );
    }

    public function getEtajLabel()
    {
        $label = App_Component_Spatiu_Property_Etaj::getLabel( $this->getEtaj() );
        
        return $label;
    }

    public function getEtaj()
    {
        return $this->getProperty( self::PROP_ETAJ );
    }

    public function getProprietar()
    {
        $p = $this->getValoareParametru( App_Component_SpatiuParametru_Factory::PARAMETRU_PROPRIETAR );
        
        return $p;
    }

    public function getPersoane()
    {
        return $this->getValoareParametru( App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE );
    }

    public function getSuprafata()
    {
        return $this->getValoareParametru( App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA );
    }

    public function getLocatie()
    {
        $Scara = $this->getScara();
        $Bloc = $Scara->getBloc();

        $locatie = 'Ap. ' . $this->getNumar() . ', Sc. ' . $Scara->getDenumire() . ', Bl. ' . $Bloc->getDenumire();

        return $locatie;
    }

    public function belongsTo($Scara)
    {
        if ($Scara instanceof App_Component_Scara_Object)
        {
            $scaraId = $Scara->getId();
        }
        else
        {
            $scaraId = $Scara;
        }

        $return = ($this->{self::PROP_SCARA_ID} == $scaraId);

        return $return;
    }

    public function isLinkedToScara( $Scara )
    {
        return $this->belongsTo( $Scara );
    }

    /**
     * @return App_Component_Scara_Object
     */
    public function getScara()
    {
        $Scari = App_Component_Scara_Factory::getInstance()->getAll();
        $Scara = $Scari[ $this->{self::PROP_SCARA_ID}];

        return $Scara;
    }

    public function getParametri()
    {
        if (!isset($this->_cache['parametri']))
        {
            $ParametruSpatiuFactory = App_Component_SpatiuParametru_Factory::getInstance();
            $parametriSpatiu = $ParametruSpatiuFactory->getAllForSpatiu( $this->getId() );

            $this->_cache['parametri'] = $parametriSpatiu;
        }

        return $this->_cache['parametri'];
    }

    public function getMainParametri()
    {
        $parametri = $this->getParametri();
        $mainParametri = Lib_Tools::filterArray( $parametri, self::getMainParametriList(), Lib_Tools::FILTER_COMMON_KEYS );
        
        return $mainParametri;
    }
    
    public function getValoareParametru( $denumireParametru )
    {
        $parametri = $this->getParametri();
        $valoareParametru = @$parametri[ $denumireParametru ];

        return $valoareParametru;
    }

    /**
     *
     * @return App_Component_Consum_Spatiu_Object[]
     */
    public function getConsumuri()
    {
        if (!isset($this->_cache['consumuri']))
        {
            $ConsumSpatiuFactory = App_Component_Consum_Spatiu_Factory::getInstance();
            $ConsumScara = $ConsumSpatiuFactory->getAll($this->{self::PROP_ID}, $this->{self::MONTH});

            $this->_cache['consumuri'] = $ConsumScara;
        }

        return $this->_cache['consumuri'];
    }

    /**
     * Returneaza un consum
     *
     * @param string $consumType
     * @return App_Component_Consum_Spatiu_Object
     */
    public function getConsum($consumType)
    {
        $Consumuri = $this->getConsumuri();

        if ( isset($Consumuri[ $consumType ]) )
        {
            /* @var $ConsumSpatiu App_Component_Consum_Spatiu_Object */
            $ConsumSpatiu = $Consumuri[ $consumType ];
        }
        else
        {
            $ConsumSpatiu = null;
        }

        return $ConsumSpatiu;
    }

    /**
     * Returneaza valoarea unui consum
     *
     * @param string $consumType
     * @return string|null
     */
    public function getValoareConsum($consumType)
    {
        $ConsumSpatiu = $this->getConsum( $consumType );

        if ( $ConsumSpatiu )
        {
            $valoareConsum = $ConsumSpatiu->getValue();
        }
        else
        {
            $valoareConsum = null;
        }

        return $valoareConsum;
    }

    /**
     * Calculeaza valoarea corespunzatoare unui criteriu
     *
     * @param string $criteriu
     *
     * @return mixed
     * @throws App_Component_Spatiu_Exception_CriteriuInvalid
     */
    public function getValoareCriteriu($criteriu)
    {
        $methodMap = [
            self::REPARTIZARE_SUPRAFATA     => 'getSuprafata',
            self::REPARTIZARE_PERSOANE      => 'getPersoane',
            self::REPARTIZARE_APA_RECE      => ['getValoareConsum', App_Component_Consum_Spatiu_Factory::APA_RECE],
            self::REPARTIZARE_EGAL          => function(){ return 1; },
        ];

        if (isset( $methodMap[$criteriu] ))
        {
            $methodDefinition = (array)$methodMap[ $criteriu ];

            $methodName = $methodDefinition[0];
            $methodArgs = array_slice($methodDefinition, 1);

            if (is_callable($methodName))
            {
                $value = call_user_func_array($methodName, $methodArgs);

                return $value;
            }
            elseif (method_exists($this, $methodName))
            {
                $value = call_user_func_array([$this, $methodName], $methodArgs);

                return $value;
            }
        }

        throw new App_Component_Spatiu_Exception_CriteriuInvalid( $criteriu );
    }

    /**
     * 
     * @return App_Component_Spatiu_Object
     */
    public function remove()
    {
        $this->setProperty( self::PROP_STATUS, self::VAL_STATUS_DELETED );
        $this->save();
        
        return $this;
    }
    
    public function updateParametri( array $parametri )
    {
        $parametri = Lib_Tools::filterArray( $parametri, self::getAllPredefinedParametriList(), Lib_Tools::FILTER_COMMON_KEYS );
        
        $parametriSpatiu = [];
        
        foreach( $parametri as $denumireParametru => $valoareParametru)
        {
            $parametruSpatiu = [
                App_Component_SpatiuParametru_Object::PROP_REF_ID       => $this->getId(),
                App_Component_SpatiuParametru_Object::PROP_PARAMETRU    => $denumireParametru,
                App_Component_SpatiuParametru_Object::PROP_VALUE        => $valoareParametru,
                App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID => $this->getProperty( self::PROP_ASOCIATIE_ID ),
            ];
            
            $parametriSpatiu[] = $parametruSpatiu;
        }
        
        App_Component_SpatiuParametru_Factory::getInstance()->update( $parametriSpatiu );
        
        return $this;
    }
    
}