<?php

class App_Component_Spatiu_Validator_Add extends App_Component_Spatiu_Validator_EntryAbstract
{
    const MAX_ENTRIES_PER_SCARA     = 300;
    const MAX_OPERATIONS            = 500;

    const ERR_MAX_ENTRIES_PER_SCARA = 'max_entries';
    const ERR_MAX_OPERATIONS        = 'max_operations';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_MAX_ENTRIES_PER_SCARA => 'Ați atins limita numărului de apartamente ce pot face parte dintr-o scară.',
            self::ERR_MAX_OPERATIONS        => 'Ați depășit numărul admis de operații.',
        ]);
    }
    
    /**
     * @see App_Api_Spatiu_Add
     * @param array $value { 
     *      App_Component_Spatiu_Object::PROP_SCARA_ID,
     *      App_Component_Spatiu_Object::PROP_ETAJ,
     *      App_Component_Spatiu_Object::PROP_NUMAR,
     *      App_Component_Parametru_Spatiu_Factory::PARAMETRU_PROPRIETAR,
     *      App_Component_Parametru_Spatiu_Factory::PARAMETRU_NR_PERSOANE,
     *      App_Component_Parametru_Spatiu_Factory::PARAMETRU_SUPRAFATA,
     * }
     */
    protected function validate( $value )
    {
        // validare input value - should be an array
        $this->validateInputFormat( $value );
        
        // verificare scara
        $scaraId = $this->validateScaraId( @$value[ App_Component_Spatiu_Object::PROP_SCARA_ID ] );
        
        // verificare numar de apartamente din scara
        $this->checkMaxEntriesPerScara( $scaraId );
        
        // verificare numar de operatiuni per scara
        $this->checkMaxOperationsPerScara( $scaraId );
        
        // etaj
        $etaj = $this->validateEtaj( @$value[ App_Component_Spatiu_Object::PROP_ETAJ ] );
        
        // numar
        $numar = $this->validateNumar( @$value[ App_Component_Spatiu_Object::PROP_NUMAR ], $scaraId );
        
        // proprietar
        $proprietar = $this->validateProprietar( @$value[ App_Component_SpatiuParametru_Factory::PARAMETRU_PROPRIETAR ] );
        
        // numar persoane
        $nrPersoane = $this->validateNrPersoane( @$value[ App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE ] );
        
        // suprafata
        $suprafata = $this->validateSuprafata( @$value[ App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA ] );
        
        $parametriSuplimentari = $this->validateParametriSuplimentari( $value );
        
        $filteredValue = [
            App_Component_Spatiu_Object::PROP_SCARA_ID      => $scaraId,
            App_Component_Spatiu_Object::PROP_ETAJ          => $etaj,
            App_Component_Spatiu_Object::PROP_NUMAR         => $numar,
            App_Component_Spatiu_Object::PROP_PROPRIETAR    => $proprietar,
            App_Component_Spatiu_Object::PROP_NR_PERSOANE   => $nrPersoane,
            App_Component_Spatiu_Object::PROP_SUPRAFATA     => $suprafata,
        ] + $parametriSuplimentari;
        
        return $filteredValue;
    }

    protected function checkMaxEntriesPerScara( $scaraId )
    {
        $criteria = [
            App_Component_Spatiu_Object::PROP_SCARA_ID => $scaraId,
        ];
        
        $entriesCount = App_Component_Spatiu_Factory::getInstance()->countActive( $criteria );
        
        if( $entriesCount >= self::MAX_ENTRIES_PER_SCARA )
        {
            $this->setError( self::ERR_MAX_ENTRIES_PER_SCARA );
        }
    }
    
    protected function checkMaxOperationsPerScara( $scaraId )
    {
        $criteria = [
            App_Component_Spatiu_Object::PROP_SCARA_ID => $scaraId,
        ];
        
        $operationsCount = App_Component_Spatiu_Factory::getInstance()->count( $criteria );
        
        if( $operationsCount >= self::MAX_OPERATIONS )
        {
            $this->setError( self::ERR_MAX_OPERATIONS );
        }
    }
    
    
}
