<?php

class App_Component_Spatiu_Validator_Remove extends App_Component_Spatiu_Validator_EntryAbstract
{
    /**
     * 
     * @param array $value { App_Component_Spatiu_Object::PROP_ID }
     * @return array
     */
    protected function validate( $value )
    {
        // verificare ID spatiu
        $spatiuId = $this->validateSpatiuId( @$value[ App_Component_Spatiu_Object::PROP_ID ] );

        $filteredValue = [
            App_Component_Spatiu_Object::PROP_ID   => $spatiuId,
        ];

        return $filteredValue;
    }

}