<?php

class App_Component_Spatiu_Validator_Edit extends App_Component_Spatiu_Validator_EntryAbstract
{
    const ERR_NO_VALUE  = 'no_value';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_NO_VALUE  => 'Specificați câmpul pe care doriți să-l modificați.',
        ]);
    }
    
    /**
     * 
     * @param array $value
     * @return array
     */
    protected function validate( $value )
    {
        $this->validateInputFormat( $value );
        
        // verificare ID spatiu
        $spatiuId = $this->validateSpatiuId( @$value[ App_Component_Spatiu_Object::PROP_ID ] );

        $filteredValue = [
            App_Component_Spatiu_Object::PROP_ID   => $spatiuId,
        ];
        
        // trebuie sa existe cel putin una dintre valorili
//        $etajExists         = array_key_exists( App_Component_Spatiu_Object::PROP_ETAJ, $value );
//        $numarExists        = array_key_exists( App_Component_Spatiu_Object::PROP_NUMAR, $value );
        $proprietarExists   = array_key_exists( App_Component_Spatiu_Object::PROP_PROPRIETAR, $value );
        $persoaneExists     = array_key_exists( App_Component_Spatiu_Object::PROP_NR_PERSOANE, $value );
        $suprafataExists    = array_key_exists( App_Component_Spatiu_Object::PROP_SUPRAFATA, $value );
        
        if( !( $proprietarExists || $persoaneExists || $suprafataExists ) )
        {
            $this->setError( self::ERR_NO_VALUE );
        }
        
//        if( $etajExists )
//        {
//            $etaj = $this->validateEtaj( @$value[ App_Component_Spatiu_Object::PROP_ETAJ ] );
//            $filteredValue[ App_Component_Spatiu_Object::PROP_ETAJ ] = $etaj;
//        }
//        
//        if( $numarExists )
//        {
//            $numar = $this->validateNumar( @$value[ App_Component_Spatiu_Object::PROP_NUMAR ], $this->getScaraId( $spatiuId ), $spatiuId );
//            $filteredValue[ App_Component_Spatiu_Object::PROP_NUMAR ] = $numar;
//        }
//        
        if( $proprietarExists )
        {
            $proprietar = $this->validateProprietar( @$value[ App_Component_Spatiu_Object::PROP_PROPRIETAR ] );
            $filteredValue[ App_Component_Spatiu_Object::PROP_PROPRIETAR ] = $proprietar;
        }
        
        if( $persoaneExists )
        {
            $persoane = $this->validateNrPersoane( @$value[ App_Component_Spatiu_Object::PROP_NR_PERSOANE ] );
            $filteredValue[ App_Component_Spatiu_Object::PROP_NR_PERSOANE ] = $persoane;
        }
        
        if( $suprafataExists )
        {
            $suprafata = $this->validateSuprafata( @$value[ App_Component_Spatiu_Object::PROP_SUPRAFATA ] );
            $filteredValue[ App_Component_Spatiu_Object::PROP_SUPRAFATA ] = $suprafata;
        }
        
        $parametriSuplimentari = $this->validateParametriSuplimentari( $value );
        
        $filteredValue += $parametriSuplimentari;
        
        return $filteredValue;
    }

    protected function getScaraId( $spatiuId )
    {
        $Scara = App_Component_Spatiu_Factory::getInstance()->find( $spatiuId );
        $scaraId = $Scara ? $Scara->getProperty( App_Component_Spatiu_Object::PROP_SCARA_ID ) : NULL;
        
        return $scaraId;
    }
    
}