<?php

class App_Component_Spatiu_Validator_Id extends Lib_Validator_Abstract
{
    const OPT_ASOCIATIE_ID  = 'a';
    const OPT_MONTH         = 'm';
    const OPT_MULTIPLE_ENTRIES = 'me';

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID   => 'Spaţiu invalid: "%id%"',
            self::ERR_REQUIRED  => 'Alegeţi apartamentele',
        ));

        $this->setOption( self::OPT_MULTIPLE_ENTRIES, true );
    }

    protected function beforeValidation()
    {
        if( $this->getOption( self::OPT_MULTIPLE_ENTRIES ) )
        {
        }
        else
        {
            $this->addErrorTemplates([self::ERR_REQUIRED  => 'Specificaţi apartamentul']);
        }

    }

    protected function _isValid( $spatiuId )
    {
        $spatii = App_Component_Spatiu_Factory::getInstance()->getAllByAsociatie( $this->getOption( self::OPT_ASOCIATIE_ID ), $this->getOption( self::OPT_MONTH ) );

        if( $this->getOption( self::OPT_MULTIPLE_ENTRIES ) )
        {
            if( !is_array( $spatiuId ) )
            {
                $this->_setError( self::ERR_REQUIRED );
            }
            else
            {
                $ids = array_unique( $spatiuId );
            }
        }
        else
        {
            $ids = array( $spatiuId );
        }

        foreach( $ids as $sId )
        {
            if( !isset( $spatii[ $sId ] ) )
            {
                $this->_setError( self::ERR_INVALID, [ 'id' => $sId ] );
            }
        }


        return $spatiuId;
    }

}
