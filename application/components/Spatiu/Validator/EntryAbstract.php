<?php

abstract class App_Component_Spatiu_Validator_EntryAbstract extends App_Component_Asociatie_Validator_BaseAbstract
{
    const ERR_SPATIU_ID_INVALID     = 'spatiu_invalid';
    const ERR_SCARA_ID_INVALID      = 'scara_invalid';
    const ERR_ETAJ_REQUIRED         = 'etaj_required';
    const ERR_ETAJ_INVALID          = 'etaj_invalid';
    const ERR_NUMAR_REQUIRED        = 'numar_required';
    const ERR_NUMAR_MAX_LENGTH      = 'numar_max_length';
    const ERR_NUMAR_DUPLICATE       = 'numar_duplicate';
    const ERR_PROPRIETAR_REQUIRED   = 'proprietar_required';
    const ERR_PROPRIETAR_MAX_LENGTH = 'proprietar_max_length';
    const ERR_NR_PERS_REQUIRED      = 'pers_required';
    const ERR_NR_PERS_INVALID       = 'pers_invalid';
    const ERR_NR_PERS_MAX_VALUE     = 'pers_max';
    const ERR_SUPRAFATA_MAX_VALUE   = 'supr_max';
    const ERR_SUPRAFATA_REQUIRED    = 'supr_required';
    const ERR_SUPRAFATA_INVALID     = 'supr_invalid';
    const ERR_PARAMETRU_INVALID     = 'parametru_invalid';
    
    const NR_PERS_NULL_VALUE        = App_Component_Spatiu_Object::ATTR_NR_PERS_NULL_VALUE;
    const NUMAR_MAX_LENGTH          = App_Component_Spatiu_Object::ATTR_NUMAR_MAX_LENGTH;
    const PROPRIETAR_MAX_LENGTH     = App_Component_Spatiu_Object::ATTR_PROPRIETAR_MAX_LENGTH;
    const NR_PERS_MAX_VALUE         = App_Component_Spatiu_Object::ATTR_NR_PERS_MAX_VALUE;
    const SUPRAFATA_MAX_VALUE       = App_Component_Spatiu_Object::ATTR_SUPRAFATA_MAX_VALUE;
    const PARAMETRU_MAX_VALUE       = App_Component_Spatiu_Object::ATTR_PARAMETRU_MAX_VALUE;
    
    protected function init()
    {
        $this->addMessageTemplates([
            self::ERR_SPATIU_ID_INVALID     => 'Spatiul specificat nu este valid.',
            self::ERR_SCARA_ID_INVALID      => 'Scara specificată nu este validă.',
            self::ERR_ETAJ_REQUIRED         => 'Specificați etajul la care se află apartamentul.',
            self::ERR_ETAJ_INVALID          => 'Etajul specificat nu este valid.',
            self::ERR_NUMAR_REQUIRED        => 'Specificați numărul apartamentului.',
            self::ERR_NUMAR_DUPLICATE       => 'Acest număr aparține unui alt apartament.',
            self::ERR_NUMAR_MAX_LENGTH      => 'Numărul apartamentului trebuie să conțină cel mult '. Lib_Grammar::pluralize( self::NUMAR_MAX_LENGTH, 'caracter', 'e'),
            self::ERR_PROPRIETAR_REQUIRED   => 'Specificați numele complet al proprietarului apartamentului.',
            self::ERR_PROPRIETAR_MAX_LENGTH => 'Numele proprietarului trebuie să conțină cel mult '. Lib_Grammar::pluralize( self::PROPRIETAR_MAX_LENGTH, 'caracter', 'e'),
            self::ERR_NR_PERS_REQUIRED      => 'Specificați numărul de persoane care locuiesc în apartament.',
            self::ERR_NR_PERS_INVALID       => 'Numărul de persoane nu este valid.',
            self::ERR_NR_PERS_MAX_VALUE     => 'Numărul de persoane trebuie să fie mai mic decât '. self::NR_PERS_MAX_VALUE .'.' ,
            self::ERR_SUPRAFATA_REQUIRED    => 'Specificați suprafața apartamentului.' ,
            self::ERR_SUPRAFATA_INVALID     => 'Suprafața apartamentului nu este validă.',
            self::ERR_SUPRAFATA_MAX_VALUE   => 'Suprafața apartamentului trebuie să fie mai mică decât '. self::SUPRAFATA_MAX_VALUE .'.',
            self::ERR_PARAMETRU_INVALID     => 'Valoarea parametrului "%denumire%" trebuie să fie între 1 și ' . self::PARAMETRU_MAX_VALUE .'.',
        ]);
        
    }
    
    protected function validateScaraId( $scaraId )
    {
        $scaraIdFiltered = (int)$scaraId;
        
        // verific ID-ul scarii: sa existe si sa apartina Asociatiei
        $Scara = App_Component_Scara_Factory::getInstance()->findActive( $scaraIdFiltered );
        
        if( !$Scara
            || $Scara->getProperty( App_Component_Scara_Object::PROP_ASOCIATIE_ID ) != $this->getAsociatieId() )
        {
            $this->setError( self::ERR_SCARA_ID_INVALID );
        }
        
        return $scaraIdFiltered;
    }
    
    protected function validateSpatiuId( $spatiuId )
    {
        $spatiuIdFiltered = (int)$spatiuId;
        
        // verific ID-ul: sa existe si sa apartina Asociatiei
        $Spatiu = App_Component_Spatiu_Factory::getInstance()->findActive( $spatiuIdFiltered );
        
        if( !$Spatiu
            || $Spatiu->getProperty( App_Component_Spatiu_Object::PROP_ASOCIATIE_ID ) != $this->getAsociatieId() )
        {
            $this->setError( self::ERR_SPATIU_ID_INVALID );
        }
        
        return $spatiuIdFiltered;
    }
    
    protected function validateEtaj( $etaj )
    {
        if( $this->ifEmpty( $etaj, self::ERR_ETAJ_REQUIRED ) )
        {
            $etaj = App_Component_Spatiu_Property_Etaj::getValue( $etaj );
            
            if( $etaj === NULL )
            {
                $this->addError( self::ERR_ETAJ_INVALID );
            }
        }
        
        return $etaj;
    }
    
    protected function validateNumar( $numar, $scaraId, $excludeSpatiuId = NULL )
    {
        // verificarea existentei numarului si a dimensiunii acestuia
        $numar = trim( $numar );
        $length = strlen( $numar );
        
        if( !$this->ifEmpty( $numar, self::ERR_NUMAR_REQUIRED ) )
        {
            if( !$this->ifLongerThan( $numar, self::NUMAR_MAX_LENGTH, self::ERR_NUMAR_MAX_LENGTH ) )
            {
                $this->validateNumarUniqueness( $numar, $scaraId, $excludeSpatiuId );
            }
        }
        
        return $numar;
    }
    
    private function validateNumarUniqueness( $numar, $scaraId, $excludeSpatiuId = NULL )
    {
        $criteria = [
            App_Component_Spatiu_Object::PROP_SCARA_ID  => $scaraId,
            App_Component_Spatiu_Object::PROP_NUMAR     => $numar,
        ];
        
        if( !empty( $excludeSpatiuId ) )
        {
            $criteria[ App_Component_Spatiu_Object::PROP_ID ] = new Lib_Component_ORM_Db_Operator_NotEqual( $excludeSpatiuId );
        }
        
        $duplicates = App_Component_Spatiu_Factory::getInstance()->countActive( $criteria );
        
        if( $duplicates )
        {
            $this->addError( self::ERR_NUMAR_DUPLICATE );
        }
    }

    protected function validateProprietar( $proprietar )
    {
        if( !$this->ifEmpty( $proprietar, self::ERR_PROPRIETAR_REQUIRED ) )
        {
            $this->ifLongerThan( $proprietar, self::PROPRIETAR_MAX_LENGTH, self::ERR_PROPRIETAR_MAX_LENGTH );
        }
        
        return $proprietar;
    }
    
    protected function validateNrPersoane( $nrPersoane )
    {
        if( !$this->ifEmpty( $nrPersoane, self::ERR_NR_PERS_REQUIRED ) )
        {
            if( $nrPersoane == self::NR_PERS_NULL_VALUE )
            {
                $nrPersoane = NULL;
            }
            elseif( $this->ifNotInteger( $nrPersoane ) || $this->ifLessThan( $nrPersoane, 0 ) )
            {
                $this->addError( self::ERR_NR_PERS_INVALID );
            }
            elseif( $this->ifGreaterThan( $nrPersoane, self::NR_PERS_MAX_VALUE ) )
            {
                $this->addError( self::ERR_NR_PERS_MAX_VALUE );
            }
        }
        
        return $nrPersoane;
    }
    
    protected function validateSuprafata( $suprafata )
    {
        if( !$this->ifEmpty( $suprafata, self::ERR_SUPRAFATA_REQUIRED ) )
        {
            if( $this->ifNotFloat( $suprafata ) || $this->ifLessThan( $suprafata, 0 ) )
            {
                $this->addError( self::ERR_SUPRAFATA_INVALID );
            }
            elseif( $this->ifGreaterThan( $suprafata, self::SUPRAFATA_MAX_VALUE ) )
            {
                $this->addError( self::ERR_SUPRAFATA_MAX_VALUE );
            }
            else
            {
                $suprafata = round( $suprafata, 2 );
            }
        }
        
        return $suprafata;
    }
    
    protected function validateParametriSuplimentari( array $inputValue )
    {
        $parametri = [];
        
        $predefinedParametri = App_Component_DenumireParametruSpatiu_Factory::getInstance()->getParametri();
        
        foreach( $predefinedParametri as $parametru )
        {
            $parametruCode = $parametru[ App_Component_DenumireParametruSpatiu_Object::PROP_CODE ];
            
            if( isset( $inputValue[ $parametruCode ] ) )
            {
                $parametruValue = $this->validateParametruValue( $inputValue[ $parametruCode ], $parametru );
                
                $parametri[ $parametruCode ] = $parametruValue;
            }
        }
        
        return $parametri;
    }
    
    protected function validateParametruValue( $value, $parametruData )
    {
        $value = trim( $value );
        
        if( strlen( $value ) == 0 )
        {
            $value = NULL;
        }
        else
        {
            if( $this->ifNotInteger( $value ) || $this->ifLessThan( $value < 0 ) || $this->ifGreaterThan( $value > self::PARAMETRU_MAX_VALUE ) )
            {
                $this->addError( 
                        self::ERR_PARAMETRU_INVALID, 
                        [ 'denumire' => $parametruData[ App_Component_DenumireParametruSpatiu_Object::PROP_DENUMIRE ] ],
                        $parametruData[ App_Component_DenumireParametruSpatiu_Object::PROP_CODE ]
                );
            }
            else
            {
                if( $value == 0 )
                {
                    $value = NULL;
                }
            }
        }
        
        return $value;
    }
    
}