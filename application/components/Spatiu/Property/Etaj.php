<?php

class App_Component_Spatiu_Property_Etaj
{
    const MAX_VALUE     = 40;
    
    const VAL_SUBSOL    = 's';
    const VAL_DEMISOL   = 'd';
    const VAL_PARTER    = 'p';
    const VAL_MEZANIN   = 'm';
    const VAL_MANSARDA  = 'md';
    
    static protected $_etajLabels = [
        '-5'    => self::VAL_SUBSOL,
        '-4'    => self::VAL_DEMISOL,
        '-3'    => self::VAL_PARTER,
        '-2'    => self::VAL_MEZANIN,
        '100'   => self::VAL_MANSARDA,
    ];

    /**
     * Take value from DB and convert to readable value
     * 
     * @param mixed $etaj
     * @return string
     */
    static public function getLabel( $etaj )
    {
        $label = NULL;
        
        $stringValue = trim( $etaj );
        
        if( array_key_exists( $stringValue, self::$_etajLabels ) )
        {
            $label = strtoupper( self::$_etajLabels[ $stringValue ] );
        }
        elseif( self::isValidNumber( $etaj ) )
        {
            $label = $stringValue;
        }
            
        return $label;
    }
    
    /**
     * Take an input value and convert to DB format
     * 
     * @param mixed $etaj
     * @return mixed
     */
    static public function getValue( $etaj )
    {
        $value = NULL;
        
        $stringValue = trim( strtolower( $etaj ) );
        
        if( ( $key = array_search( $stringValue, self::$_etajLabels ) ) !== FALSE )
        {
            $value = intval( $key );
        }
        elseif( self::isValidNumber( $etaj ) )
        {
            $value = intval( $etaj );
        }
            
        return $value;
        
    }
    
    static private function isValidNumber( $etaj )
    {
        $stringValue    = trim( $etaj );
        $intValue       = intval( $etaj );

        $isValid = ( $stringValue === (string)$intValue ) && ( $intValue >= 1 ) && ( $intValue <= self::MAX_VALUE );
        
        return $isValid;
    }
}
