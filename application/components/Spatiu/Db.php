<?php

class App_Component_Spatiu_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'spatiu';
    
    static protected $instance;

    /** (v)
     * Extrage toate apartamentele unei Asociatii
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAll( $asociatieId, $month = NULL )
    {
        $columns = $this->quoteColumns([
                App_Component_Spatiu_Object::PROP_ID,
                App_Component_Spatiu_Object::PROP_BLOC_ID,
                App_Component_Spatiu_Object::PROP_SCARA_ID,
                App_Component_Spatiu_Object::PROP_NUMAR,
                App_Component_Spatiu_Object::PROP_ETAJ,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where( $this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_Spatiu_Object::PROP_ASOCIATIE_ID ),
                        $this->whereConditions( App_Component_Spatiu_Object::getActiveStatusCriteria() ),
                ]));
                

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

}
