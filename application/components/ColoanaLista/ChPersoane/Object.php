<?php

class App_Component_ColoanaLista_ChPersoane_Object extends App_Component_ColoanaLista_Object
{
    
    public function compute()
    {
        $parametru = App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE;

        $data = $this->computeUsingWeight( $parametru );

        return $data;
    }

}