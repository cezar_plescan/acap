<?php

/**
 * Totalul cheltuielilor lunare
 */
abstract class App_Component_ColoanaLista_Total_Object extends App_Component_ColoanaLista_Object
{
    /**
     * Returneaza un array de forma
     *      [ <spatiu_id> => [
     *          value => <value>
     *      ]],
     *
     * @return array
     */
    protected function _compute()
    {
        $valoriColoaneLista = func_get_args()[0];
        if (!is_array($valoriColoaneLista))
        {
            $valoriColoaneLista = [];
        }

        $data = [];

        /* @var $LinieColoanaLista App_Component_LinieColoanaLista_Object */
        foreach ($valoriColoaneLista as $valoriColoanaLista)
        {
            foreach ($valoriColoanaLista as $spatiuId => $LinieColoanaLista)
            {
                if (!isset( $data[ $spatiuId ]))
                {
                    $data[ $spatiuId ] = [
                            self::VALUE => 0,
                    ];
                }

                $data[ $spatiuId ][ self::VALUE ] += $LinieColoanaLista->getValue();
            }
        }

        return $data;
    }

}