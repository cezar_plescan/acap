<?php

class App_Component_ColoanaLista_Exception_CheltuialaNotFound extends UnexpectedValueException
{
    public function __construct(App_Component_ColoanaLista_Object $ColoanaLista)
    {
        $message = 'Nici o factura nu a fost repartizata pe coloana "' . $ColoanaLista->getLabel() . '"';

        parent::__construct($message);
    }
}
