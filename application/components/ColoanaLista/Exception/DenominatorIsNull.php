<?php

class App_Component_ColoanaLista_Exception_DenominatorIsNull extends UnexpectedValueException
{
    public function __construct()
    {
        $message = 'Baza de repartizare este nula.';

        parent::__construct($message);
    }
}
