<?php

class App_Component_ColoanaLista_Beneficiar_Object extends App_Component_ColoanaLista_Object
{
    public function compute()
    {
        $data = [];

        $cheltuieli = $this->getCheltuieli();

        foreach ($cheltuieli as $cheltuiala)
        {
            $parametriCheltuiala = $cheltuiala[ App_Component_Cheltuiala_Object::PARAMETRI ];
            foreach( $parametriCheltuiala as $spatiuId => $valoare )
            {
                if ( !isset( $data[ $spatiuId ][ 'value' ] ) )
                {
                    $data[ $spatiuId ][ 'value' ] = 0;
                }

                $data[ $spatiuId ][ 'value' ] += $valoare;
            }
        }

        return $data;
    }

}