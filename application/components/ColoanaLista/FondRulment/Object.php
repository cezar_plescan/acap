<?php

/**
 * Repartizeaza cheltuielile repartizare pe beneficiar
 * Numele clasei trebuie sa coincida cu codul Coloanei
 */
class App_Component_ColoanaLista_FondRulment_Object extends App_Component_ColoanaLista_Object
{
    public function compute()
    {
        $data = [];

        $spatii = $this->getSpatii();

        foreach( $spatii as $spatiuId => $spatiu )
        {
            $data[ $spatiuId ][ App_Component_Lista_Object::VALUE ] = @$spatiu[ App_Component_Spatiu_Object::PARAMETRI ][ App_Component_Coloana_Factory::COLOANA_FOND_RULMENT ];
        }

        return $data;
    }

}
