<?php

abstract class App_Component_ColoanaLista_Subcoloana_Object extends Lib_Component_Object
{
    protected
            /**
             * @var App_Component_ColoanaLista_Object
             */
            $ColoanaLista;

    /**
     * Calculeaza valorile pentru o subcoloana
     *
     * @return array [spatiu_id => {display_value, value}]
     */
    abstract public function compute();

    public function __construct(App_Component_ColoanaLista_Object $ColoanaLista)
    {
        $this->ColoanaLista = $ColoanaLista;
    }

    /** (v)
     *
     * @return type
     */
    protected function getSpatii()
    {
        return $this->ColoanaLista->getSpatii();
    }
}