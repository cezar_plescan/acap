<?php

/**
 * Repartizeaza cheltuielile repartizare pe beneficiar
 * Numele clasei trebuie sa coincida cu codul Coloanei
 */
class App_Component_ColoanaLista_Restante_Object extends App_Component_ColoanaLista_Object
{
    public function compute()
    {
        $data = [];

        $spatii = $this->getSpatii();

        foreach( $spatii as $spatiuId => $spatiu )
        {
            // restante = Restante_luna_precedenta + Intretinere_luna_precedenta + Incasare

            $restanteLunaPrecedenta = @$spatiu[ App_Component_Spatiu_Object::PARAMETRI ][ App_Component_SpatiuParametru_Factory::PARAMETRU_RESTANTE ];
            $intretinereLunaPrecedenta = @$spatiu[ App_Component_Spatiu_Object::PARAMETRI ][ App_Component_SpatiuParametru_Factory::PARAMETRU_INTRETINERE ];

            $incasari = App_Component_Spatiu_Factory::getInstance()->getValoareIncasari( $spatiu, App_Component_Incasare_Factory::INCASARE_INTRETINERE );

            $data[ $spatiuId ][ App_Component_Lista_Object::VALUE ] = $restanteLunaPrecedenta + $intretinereLunaPrecedenta - $incasari;
        }

        return $data;
    }

}
