<?php

/**
 *
 */
class App_Component_ColoanaLista_Spatiu_Object extends App_Component_ColoanaLista_Object
{

    public function compute()
    {
        // calculez valorile subcoloanelor
        $data = $this->computeSubcoloaneValues();

        return $data;
    }

}