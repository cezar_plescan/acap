<?php

/**
 *
 */
class App_Component_ColoanaLista_Spatiu_Subcoloana_Proprietar extends App_Component_ColoanaLista_Subcoloana_Object
{

    public function compute()
    {
        $data = [];

        foreach ($this->getSpatii() as $spatiuId => $spatiuData)
        {
            $data[ $spatiuId ][ 'value' ]  = @$spatiuData[ 'parametri' ][ App_Component_SpatiuParametru_Factory::PARAMETRU_PROPRIETAR ];
        }

        return $data;
    }

}