<?php

/**
 *
 */
class App_Component_ColoanaLista_Spatiu_Subcoloana_NrSpatiu extends App_Component_ColoanaLista_Subcoloana_Object
{

    public function compute()
    {
        $data = [];

        foreach( $this->getSpatii() as $spatiuId => $spatiuData )
        {
            $data[ $spatiuId ][ 'value' ]  = $spatiuData[ App_Component_Spatiu_Object::PROP_NUMAR ];
        }

        return $data;
    }

}