<?php

/**
 *
 */
class App_Component_ColoanaLista_ApaCalda_Object extends App_Component_ColoanaLista_Apa_Object
{
    const CHELTUIALA_LABEL                      = 'apă caldă';

    const SCARA_CONSUM_REPARTIZAT               = App_Component_Consum_Scara_Factory::APA_CALDA_REPARTIZAT;
    const SCARA_DIF_GEN_PAS                     = App_Component_Consum_Scara_Factory::APA_CALDA_DIFERENTA_GENERAL_PASANTI;

    const CONSUM_SCARA                          = App_Component_Consum_Scara_Factory::APA_CALDA;
    const CONSUM_SPATIU                         = App_Component_Consum_Spatiu_Factory::APA_CALDA;
    const CONSUM_COMUN_SCARA                    = App_Component_Consum_Scara_Factory::APA_CALDA_COMUN;
    const REPARTIZARE_DIFERENTA_SCARA_GENERAL   = App_Component_AsociatieParametru_Factory::APA_REP_DIF_SCARA_GENERAL;
    const REPARTIZARE_DIFERENTA_GENERAL_PASANTI = App_Component_AsociatieParametru_Factory::APA_REP_DIF_GENERAL_PASANTI;
    const REPARTIZARE_DIFERENTA_SPATIU_SCARA    = App_Component_AsociatieParametru_Factory::APA_REP_DIF_SPATIU_SCARA;
    const REPARTIZARE_CONSUM_COMUN_SCARA        = App_Component_AsociatieParametru_Factory::APA_REP_CONSUM_COMUN_SCARA;
    const REPARTIZARE_CONSUM_COMUN_GENERAL      = App_Component_AsociatieParametru_Factory::APA_REP_CONSUM_COMUN_GENERAL;
    const REPARTIZARE_CONSUM_SCARI_NECONTORIZATE = App_Component_AsociatieParametru_Factory::APA_REP_CONSUM_SCARI_NECONTORIZATE;

}
