<?php

/**
 *
 */
abstract class App_Component_ColoanaLista_Apa_Object extends App_Component_ColoanaLista_Object
{
    // consumuri corespunzatoare spatiilor
    const SPATIU_CONSUM_REPARTIZAT             = 'consum_repartizat_spatiu';
    const SPATIU_CONSUM_INREGISTRAT            = 'consum_inregistrat_spatiu';
    const SPATIU_CONSUM_COMUN_GENERAL          = 'consum_comun_general_spatiu';
    const SPATIU_CONSUM_COMUN_SCARA            = 'consum_comun_scara_spatiu';
    const SPATIU_DIFERENTA_SCARA               = 'diferente_scara_spatiu';
    const SPATIU_CONSUM_PAUSAL                 = 'consum_pausal_spatiu';

    const SUBCOLOANA_CANTITATE          = 'cantitate';
    const SUBCOLOANA_DIFERENTE          = 'diferente';
    const SUBCOLOANA_CONSUM_COMUN       = 'consum_comun';

    protected
            $_cache = [];

    /**
     *
     * Metoda efectiva de calcul a valorilor.
     *
     * Returneaza un array de forma
     *      [spatiu_id => [
     *          value => <value>,
     *          params => [<param_value>]
     *      ]],
     *
     * @return array
     */
    public function _compute()
    {
        $Cheltuieli = $this->getCheltuieli();
        if ( !$Cheltuieli )
        {
            return [];
        }

        $data = [];

        foreach($Cheltuieli as $Cheltuiala)
        {
            /* @var $Cheltuiala App_Component_Cheltuiala_Object */
            $spatiiValues = $this->computeValues( $Cheltuiala );

            //$this->_checkData( $spatiiValues, false );

            $dataCheltuiala = $this->transformValues( $spatiiValues, $Cheltuiala );

            $data += $dataCheltuiala;
        }

        return $data;
    }

    /**
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return App_Component_Scara_Object[]
     */
    protected function getScariCheltuiala( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $idCheltuiala = $Cheltuiala->id;
        $cacheId = 'scari-cheltuiala-'.$idCheltuiala;

        if ( !isset($this->_cache[ $cacheId ]) )
        {
            $scariIds = $Cheltuiala->getTarget();
            $Scari = App_Component_Scara_Factory::getInstance()->getAll();

            $ScariCheltuiala = [];

            foreach ($scariIds as $scaraId)
            {
                $ScariCheltuiala[ $scaraId ] = $Scari[ $scaraId ];
            }

            $this->_cache[ $cacheId ] = $ScariCheltuiala;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return App_Component_Scara_Object[]
     */
    protected function getSpatiiCheltuiala( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $cacheId = 'spatii-cheltuiala-' . $Cheltuiala->id;

        if ( !isset($this->_cache[ $cacheId ]) )
        {
            $Spatii = [];
            $Scari = $this->getScariCheltuiala( $Cheltuiala );

            foreach($Scari as $Scara)
            {
                $Spatii += $Scara->getSpatii();
            }

            $this->_cache[ $cacheId ] = $Spatii;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * @see pg. 591 pentru regulile de calcul
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return array
     */
    protected function computeValues( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $data = [];

        $ScariContorizate = $ScariNecontorizate = [];

        // determin Scarile contorizate/necontorizate
        $ScariCheltuiala = $this->getScariCheltuiala( $Cheltuiala );

        foreach ($ScariCheltuiala as $Scara)
        {
            if ($this->esteScaraContorizata($Scara))
            {
                $ScariContorizate[ $Scara->id ] = $Scara;
            }
            else
            {
                $ScariNecontorizate[ $Scara->id ] = $Scara;
            }
        }

        //// determin consumul pentru Scarile contorizate

        $sumaConsumScariContorizate = 0;
        $diferentaGeneralPasantiRepartizata = 0;

        foreach ($ScariContorizate as $Scara)
        {
            $consumInregistratScara         = $Scara->getValoareConsum( static::CONSUM_SCARA );
            $diferentaGeneralPasantiPeScara = $this->repartizeazaDiferentaGeneralPasantiPeScara( $Scara, $Cheltuiala );

            $consumRepartizatScara = $consumInregistratScara + $diferentaGeneralPasantiPeScara;

            if (!$ScariNecontorizate)
            {
                $diferentaScariGeneralPeScara = $this->repartizeazaDiferentaScariGeneralPeScara( $Scara, $Cheltuiala );
                $consumRepartizatScara += $diferentaScariGeneralPeScara;
            }

            $dataScara = $this->computeValuesForScari( $Scara, $consumRepartizatScara, $Cheltuiala );

            $data += $dataScara;

            $sumaConsumScariContorizate += $consumInregistratScara;
            $diferentaGeneralPasantiRepartizata += $diferentaGeneralPasantiPeScara;
        }

        //// determin consumul pentru Scarile necontorizate

        if ($ScariNecontorizate)
        {
            $consumTotalInregistrat = $this->getConsumTotalInregistrat( $Cheltuiala );
            $consumComunGeneral     = $this->getConsumComunGeneral( $Cheltuiala );
            $restDiferentaGeneralPasanti = $this->getDiferentaGeneralPasanti( $Cheltuiala ) - $diferentaGeneralPasantiRepartizata;

            $consumScariNecontorizate = $consumTotalInregistrat - ( $sumaConsumScariContorizate + $consumComunGeneral ) + $restDiferentaGeneralPasanti;

            $dataScariNecontorizate = $this->computeValuesForScari( $ScariNecontorizate, $consumScariNecontorizate, $Cheltuiala );

            $data+= $dataScariNecontorizate;
        }

        return $data;
    }

    protected function _checkData(array $data, $die = true)
    {
        $sumaConsumComun = 0;
        $sumaConsum = 0;

        foreach($data as $spatiuId => $values)
        {
            $sumaConsumComun += $values[ self::SPATIU_CONSUM_COMUN_GENERAL ];
            $sumaConsum += $values[ self::SPATIU_CONSUM_REPARTIZAT ];
        }

        if ($die)
        {
            echo 'Verificare:<pre>';
            print_r($data);
            echo '</pre>';
        }

        echo '<hr>Suma consum comun = ' . $sumaConsumComun;
        echo '<br>Consum comun = ' . $this->getConsumComunGeneral();

        echo '<hr>Suma consumuri repartizate = ' . $sumaConsum;
        echo '<br>Consum total = ' . $this->getConsumGeneralTotal();

        $die && die();
    }

    /**
     * Preia valorile subcoloanelor si genereaza un rezultat formatat pentru a fi
     * folosit la afisarea in Lista de Plata
     *
     * @param array $spatiuValues
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     * @return type
     */
    protected function transformValues(array $spatiuValues, App_Component_Cheltuiala_Object $Cheltuiala)
    {
        $data = [];

        $pretUnitar = $this->getPretUnitar( $Cheltuiala );

        foreach($spatiuValues as $spatiuId => $values)
        {
            $value = $values[ self::SPATIU_CONSUM_REPARTIZAT ] * $pretUnitar;

            $data[ $spatiuId ] = [
                    self::VALUE     => $value,
                    self::SUBCOLOANE_VALUES    => [
                            self::SUBCOLOANA_CANTITATE      => $values[ self::SPATIU_CONSUM_INREGISTRAT ],
                            self::SUBCOLOANA_DIFERENTE      => @$values[ self::SPATIU_DIFERENTA_SCARA ],
                            self::SUBCOLOANA_CONSUM_COMUN   => $values[ self::SPATIU_CONSUM_COMUN_GENERAL ] + $values[ self::SPATIU_CONSUM_COMUN_SCARA ],
                    ],
            ];

            // verific daca spatiul are consum pausal
            $Spatiu = $this->getSpatii()[ $spatiuId ];
            if ( !$this->esteSpatiulContorizat($Spatiu) )
            {
                $data[ $spatiuId ][ self::SUBCOLOANE_DISPLAY_VALUES ] = [
                        self::SUBCOLOANA_CANTITATE      => $values[ self::SPATIU_CONSUM_PAUSAL ] . ' *',
                ];
            }
        }

        return $data;
    }

    /**
     * Determina daca Scara este contorizata sau nu.
     * O Scara este contorizata doar daca exista un consum inregistrat
     *
     * @param App_Component_Scara_Object $Scara
     *
     * @return type
     */
    protected function esteScaraContorizata(App_Component_Scara_Object $Scara)
    {
        $ConsumContorizatScara = $Scara->getConsum( static::CONSUM_SCARA );

        return (bool)$ConsumContorizatScara;
    }

    /**
     * Calculeaza partea din diferenta dintre suma consumurilor inregistrate la fiecare scara
     * si consumul inregistrat de contorul de bransament, ce revine unei scari
     *
     * @param App_Component_Scara_Object $Scara
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function repartizeazaDiferentaScariGeneralPeScara(App_Component_Scara_Object $Scara, App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $cacheId = 'valori-ponderate-diferente-scari-' . $Cheltuiala->id;

        if ( !isset($this->_cache[ $cacheId ]) )
        {
            // calculul repartizarii se face la toate scarile odata

            $diferentaScariGeneral = $this->getDiferentaScariGeneral( $Cheltuiala );

            // citesc modul de repartizare a diferentelor
            $criteriuDiferente = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_DIFERENTA_SCARA_GENERAL );
            if ( null === $criteriuDiferente )
            {
                $criteriuDiferente = Lib_Application::getInstance()->getConfig(['repartizare', 'apa',  'diferenta']);
            }

            $ponderi = [];
            foreach($this->getScariCheltuiala( $Cheltuiala ) as $scaraId => $S)
            {
                $ponderi[ $scaraId ] = $S->getValoareCriteriu( $criteriuDiferente );
            }

            $valoriPonderate = Lib_Tools::distributeWeighted($diferentaScariGeneral, $ponderi);

            $this->_cache[ $cacheId ] = $valoriPonderate;
        }

        $diferentaScara = @$this->_cache[ $cacheId ][ $Scara->id ];

        return $diferentaScara;
    }

    /**
     * Calculeaza diferenta dintre suma consumurilor inregistrate la fiecare scara
     * si consumul inregistrat de contorul de bransament
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function getDiferentaScariGeneral( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        /* informatii necesare:
         * - consum inregistrat de contorul general (de bransament)
         * - suma consumurilor inregistrate pe fiecare scara
         * - modul de repartizare a diferentelor
         */

        $cacheId = 'diferenta-scari-general-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $consumTotalInregistrat = $this->getConsumTotalInregistrat( $Cheltuiala );
            $sumaConsumuriScari     = $this->getSumaConsumuriInregistrateScari( $Cheltuiala );
            $consumComunGeneral     = $this->getConsumComunGeneral( $Cheltuiala );

            $diferenta = $consumTotalInregistrat - ($sumaConsumuriScari + $consumComunGeneral);

            $this->_cache[ $cacheId ] = $diferenta;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Consumul Total Inregistrat de contorul de bransament (general)
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function getConsumTotalInregistrat( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $cacheId = 'consum-total-inregistrat-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $consumGeneral = $Cheltuiala->getValoareParametru( App_Component_Cheltuiala_Apa_Object::PARAM_CONSUM_TOTAL_INREGISTRAT );

            $this->_cache[ $cacheId ] = $consumGeneral;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Consumul total facturat
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function getConsumGeneralTotal( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $cacheId = 'consum-general-total-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $consumGeneral = $this->getConsumTotalInregistrat( $Cheltuiala );
            $diferentaGeneralPasanti = $this->getDiferentaGeneralPasanti( $Cheltuiala );

            $consumGeneralTotal = $consumGeneral + $diferentaGeneralPasanti;

            $this->_cache[ $cacheId ] = $consumGeneralTotal;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Returneaza valoarea facturata
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function getValoareFacturata( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $cacheId = 'valoare-facturata-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $valoare = $Cheltuiala->getValoare();

            $this->_cache[ $cacheId ] = $valoare;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Returneaza pretul unui mc de apa rece
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     * @throws UnexpectedValueException
     */
    protected function getPretUnitar( App_Component_Cheltuiala_Object $Cheltuiala )
    {
        $cantitate = $this->getConsumGeneralTotal( $Cheltuiala );
        $valoare = $this->getValoareFacturata( $Cheltuiala );

        if (!$cantitate)
        {
            throw new UnexpectedValueException('Cantitatea facturata este nulă');
        }

        $pretUnitar = $valoare / $cantitate;

        return $pretUnitar;
    }

    /**
     * Suma consumurilor inregistrate pe fiecare Scara
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function getSumaConsumuriInregistrateScari(App_Component_Cheltuiala_Object $Cheltuiala)
    {
        $cacheId = 'total-consumuri-scari-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $total = 0;
            foreach($this->getScariCheltuiala($Cheltuiala) as $Scara)
            {
                $consumScara = $Scara->getValoareConsum( static::CONSUM_SCARA );
                $total += $consumScara;
            }

            $this->_cache[ $cacheId ] = $total;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Consumul comun pt Factura inregistrata
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function getConsumComunGeneral(App_Component_Cheltuiala_Object $Cheltuiala)
    {
        $cacheId = 'consum-comun-general-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $consumComun = $Cheltuiala->getValoareParametru( App_Component_Cheltuiala_Apa_Object::PARAM_CONSUM_COMUN );

            $this->_cache[ $cacheId ] = $consumComun;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Repartizarea diferentei General-Pasanti pe o Scara.
     * Conform Legii, aceasta valoare se repartizeaza in mod egal la fiecare scara.
     *
     * @param App_Component_Scara_Object $Scara
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function repartizeazaDiferentaGeneralPasantiPeScara(App_Component_Scara_Object $Scara, App_Component_Cheltuiala_Object $Cheltuiala)
    {
        $cacheId = 'valori-ponderate-diferenta-general-pasanti-' . $Cheltuiala->id;

        if ( !isset($this->_cache[ $cacheId ]) )
        {
            // calculul repartizarii se face la toate scarile odata

            $diferenta = $this->getDiferentaGeneralPasanti( $Cheltuiala );

            // citesc modul de repartizare a diferentelor
            $criteriuDiferente = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_DIFERENTA_GENERAL_PASANTI );
            if ( null === $criteriuDiferente )
            {
                $criteriuDiferente = Lib_Application::getInstance()->getConfig(['repartizare', 'apa',  'diferenta_general_pasanti']);
            }

            $ponderi = [];
            foreach($this->getScariCheltuiala() as $scaraId => $S)
            {
                $ponderi[ $scaraId ] = $S->getValoareCriteriu( $criteriuDiferente );
            }

            $valoriPonderate = Lib_Tools::distributeWeighted($diferenta, $ponderi);

            $this->_cache[ $cacheId ] = $valoriPonderate;
        }

        $diferentaScara = @$this->_cache[ $cacheId ][ $Scara->id ];

        return $diferentaScara;
    }

    /**
     * Returneaza diferenta dintre Contorul General si Pasanti, valoare ce se
     * regaseste in Factura
     *
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function getDiferentaGeneralPasanti(App_Component_Cheltuiala_Object $Cheltuiala)
    {
        $cacheId = 'diferenta-general-pasanti-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $diferenta = $Cheltuiala->getValoareParametru( App_Component_Cheltuiala_Apa_Object::PARAM_DIFERENTA_GENERAL_PASANTI );

            $this->_cache[ $cacheId ] = $diferenta;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Determina daca Spatiul este contorizat.
     *
     * @param App_Component_Spatiu_Object $Spatiu
     * @return type
     */
    protected function esteSpatiulContorizat(App_Component_Spatiu_Object $Spatiu)
    {
        $ConsumContorizatSpatiu = $Spatiu->getConsum( static::CONSUM_SPATIU );

        return (bool)$ConsumContorizatSpatiu;
    }

    /**
     * Determina daca Spatiul este contorizat in sistem pausal.
     *
     * @param App_Component_Spatiu_Object $Spatiu
     * @return type
     */
    protected function esteSpatiulPausal(App_Component_Spatiu_Object $Spatiu)
    {
        $consumSpatiu = $Spatiu->getValoareConsum( static::CONSUM_SPATIU );
        $pausal = ( $consumSpatiu === App_Component_Consum_Spatiu_Factory::VALUE_PAUSAL );

        return $pausal;
    }

    /**
     * Calculeaza partea din diferenta dintre suma consumurilor inregistrate la fiecare spatiu
     * si consumul inregistrat de contorul de scara, ce revine unui spatiu
     *
     * @param App_Component_Spatiu_Object $Scara
     * @return float
     */
    protected function repartizeazaDiferentaSpatiiScaraPeSpatiu(array $Scari, $consumRepartizatScara, App_Component_Spatiu_Object $Spatiu)
    {
        $scariIds = [];
        foreach($Scari as $Scara)
        {
            $scariIds[] = $Scara->id;
        }
        sort($scariIds);
        $hashId = 'ponderi-diferente-spatii-' . implode('-', $scariIds);

        if (!isset($this->_cache[ $hashId ]))
        {
            $diferenta = $this->getDiferentaSpatiiScara( $Scari, $consumRepartizatScara );

            // citesc modul de repartizare a diferentelor
            $criteriuDiferente = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_DIFERENTA_SPATIU_SCARA );
            if ( null === $criteriuDiferente )
            {
                $criteriuDiferente = Lib_Application::getInstance()->getConfig(['repartizare', 'apa',  'diferenta']);
            }

            // extrag Spatiile pt Scarile specificate
            $Spatii = [];
            foreach ($Scari as $Scara)
            {
                $Spatii += $Scara->getSpatii();
            }

            $ponderi = [];
            foreach ($Spatii as $spatiuId => $S)
            {
                $ponderi[ $spatiuId ] = $S->getValoareCriteriu( $criteriuDiferente );
            }

            $valoriPonderate = Lib_Tools::distributeWeighted( $diferenta, $ponderi );

            $this->_cache[ $hashId ] = $valoriPonderate;
        }

        $valoare = @$this->_cache[ $hashId ][ $Spatiu->id ];

        return $valoare;
    }

    /**
     * Calculeaza diferenta dintre suma consumurilor inregistrate la fiecare spatiu
     * si consumul inregistrat de contorul de scara / consumul repartizat la un grup de scari
     *
     * @return float
     */
    protected function getDiferentaSpatiiScara(array $Scari, $consumRepartizatScara)
    {
        /* informatii necesare:
         * - consum inregistrat de contorul de scara
         * - suma consumurilor inregistrate pe fiecare spatiu
         * - modul de repartizare a diferentelor
         */

        $scariIds = [];
        foreach($Scari as $Scara)
        {
            $scariIds[] = $Scara->id;
        }
        sort($scariIds);
        $hashId = 'diferenta-spatii-scara-' . implode('-', $scariIds);

        if ( !isset($this->_cache[ $hashId ]) )
        {
            $sumaConsumuriSpatii = $consumuriComuneScari = 0;

            foreach($Scari as $Scara)
            {
                $sumaConsumuriSpatii  += $this->getSumaConsumuriSpatii( $Scara );
                $consumuriComuneScari += $this->getConsumComunScara( $Scara );
            }

            $diferenta = $consumRepartizatScara - ($sumaConsumuriSpatii + $consumuriComuneScari);

            $this->_cache[ $hashId ] = $diferenta;
        }

        return $this->_cache[ $hashId ];
    }

    /**
     * Suma consumurilor inregistrate pe fiecare Spatiu dintr-o anumita Scara
     *
     * @return float
     */
    protected function getSumaConsumuriSpatii(App_Component_Scara_Object $Scara)
    {
        $cacheId = 'suma-consumuri-spatii-' . $Scara->id;

        if ( !isset($this->_cache[ $cacheId ]) )
        {
            $suma = 0;

            foreach($Scara->getSpatii() as $Spatiu)
            {
                $consumSpatiu = $Spatiu->getValoareConsum( static::CONSUM_SPATIU );
                $suma += $consumSpatiu;
            }

            $this->_cache[ $cacheId ] = $suma;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Consumul comun inregistrat intr-o anumita Scara
     *
     * @return float
     */
    protected function getConsumComunScara(App_Component_Scara_Object $Scara)
    {
        $cacheId = 'consum-comun-scara-' . $Scara->id;

        if ( !isset($this->_cache[ $cacheId ]) )
        {
            $consumComunScara = $Scara->getValoareConsum( static::CONSUM_COMUN_SCARA );

            $this->_cache[ $cacheId ] = $consumComunScara;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Repartizeaza consumul comun dintr-o scara la un anumit apartament din acea scara
     *
     * @param App_Component_Scara_Object $Scara
     * @param App_Component_Spatiu_Object $Spatiu
     * @return type
     */
    protected function repartizeazaConsumComunScaraPeSpatiu(App_Component_Scara_Object $Scara, App_Component_Spatiu_Object $Spatiu)
    {
        $cacheId = 'valori-ponderate-consum-comun-scara-spatiu-' . $Scara->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $consumComunScara = $this->getConsumComunScara( $Scara );
            $valoriPonderate = [];

            if ($consumComunScara)
            {
                $criteriuConsumComunScara = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_CONSUM_COMUN_SCARA );
                if ( null === $criteriuConsumComunScara )
                {
                    $criteriuConsumComunScara = Lib_Application::getInstance()->getConfig(['repartizare', 'apa',  'consum_comun']);
                }

                $ponderi = [];
                foreach($Scara->getSpatii() as $spatiuId => $S)
                {
                    $ponderi[ $spatiuId ] = $S->getValoareCriteriu( $criteriuConsumComunScara );
                }

                $valoriPonderate = Lib_Tools::distributeWeighted( $consumComunScara, $ponderi );
            }

            $this->_cache[ $cacheId ] = $valoriPonderate;
        }

        $valoare = @$this->_cache[ $cacheId ][ $Spatiu->id ];

        return $valoare;
    }

    /**
     * Repartizeaza consumul comun general la un apartament
     *
     * @param App_Component_Spatiu_Object $Spatiu
     * @param App_Component_Cheltuiala_Object $Cheltuiala
     *
     * @return float
     */
    protected function repartizeazaConsumComunGeneralPeSpatiu(App_Component_Spatiu_Object $Spatiu, App_Component_Cheltuiala_Object $Cheltuiala)
    {
        $cacheId = 'valori-ponderate-consum-comun-general-spatiu-' . $Cheltuiala->id;

        if (!isset($this->_cache[ $cacheId ]))
        {
            $consumComunGeneral = $this->getConsumComunGeneral( $Cheltuiala );
            $valoriPonderate = [];

            if ($consumComunGeneral)
            {
                $criteriuConsumComunGeneral = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_CONSUM_COMUN_GENERAL );
                if ( null === $criteriuConsumComunGeneral )
                {
                    $criteriuConsumComunGeneral = Lib_Application::getInstance()->getConfig(['repartizare', 'apa',  'consum_comun']);
                }

                $ponderi = [];
                foreach($this->getSpatiiCheltuiala( $Cheltuiala ) as $spatiuId => $S)
                {
                    $ponderi[ $spatiuId ] = $S->getValoareCriteriu( $criteriuConsumComunGeneral );
                }

                $valoriPonderate = Lib_Tools::distributeWeighted( $consumComunGeneral, $ponderi );
            }

            $this->_cache[ $cacheId ] = $valoriPonderate;
        }

        $valoare = @$this->_cache[ $cacheId ][ $Spatiu->id ];

        return $valoare;
    }

    /**
    * @see pg. 591 pentru metoda de calcul
    *
    * @param $Scari
    * @param $consumRepartizatScara
    * @param App_Component_Cheltuiala_Object $Cheltuiala
    *
    * @return array [ spatiu_id => array( consumuri corespunzatoare spatiului ) ]
    */
    protected function computeValuesForScari($Scari, $consumRepartizatScara, App_Component_Cheltuiala_Object $Cheltuiala)
    {
        // argumentul $Scari poate reprezenta una sau mai multe scari
        if (!is_array($Scari))
        {
            $Scari = $Scari ?
                [ $Scari ] :
                [];
        }

        // extrag Spatiile pt Scarile specificate
        $Spatii = [];
        foreach ($Scari as $Scara)
        {
            $Spatii += $Scara->getSpatii();
        }

        $data = [];

        $SpatiiContorizate = $SpatiiPausal = $SpatiiDebransate = [];

        // determin Spatiile contorizate / necontorizate(pausal)
        foreach ($Spatii as $Spatiu)
        {
            if ($this->esteSpatiulContorizat( $Spatiu ))
            {
                $SpatiiContorizate[ $Spatiu->id ] = $Spatiu;
            }
            elseif ($this->esteSpatiulPausal( $Spatiu ))
            {
                $SpatiiPausal[ $Spatiu->id ] = $Spatiu;
            }
            else
            {
                $SpatiiDebransate[ $Spatiu->id ] = $Spatiu;
            }
        }

        $existaSpatiiPausal = (bool)$SpatiiPausal;

        //// se repatizeaza consumul pe fiecare Spatiu contorizat

        $sumaConsumuriSpatiiContorizate = 0;

        foreach ($SpatiiContorizate as $spatiuId => $Spatiu)
        {
            $ScaraSpatiu = $this->getScariCheltuiala( $Cheltuiala )[ $Spatiu->{$Spatiu::SCARA_ID} ];

            $consumInregistratSpatiu        = $Spatiu->getValoareConsum( static::CONSUM_SPATIU );
            $consumComunScaraPeSpatiu       = $this->repartizeazaConsumComunScaraPeSpatiu( $ScaraSpatiu, $Spatiu );
            $consumComunGeneralPeSpatiu     = $this->repartizeazaConsumComunGeneralPeSpatiu( $Spatiu, $Cheltuiala );

            $consumRepartizatSpatiu = $consumInregistratSpatiu + $consumComunScaraPeSpatiu + $consumComunGeneralPeSpatiu;

            if ( !$existaSpatiiPausal )
            {
                $diferentaSpatiiScaraPeSpatiu = $this->repartizeazaDiferentaSpatiiScaraPeSpatiu( $Scari, $consumRepartizatScara, $Spatiu );

                $consumRepartizatSpatiu += $diferentaSpatiiScaraPeSpatiu;
            }

            $sumaConsumuriSpatiiContorizate += $consumInregistratSpatiu;

            $data[ $spatiuId ] = [
                    self::SPATIU_CONSUM_INREGISTRAT    => $consumInregistratSpatiu,
                    self::SPATIU_CONSUM_REPARTIZAT     => $consumRepartizatSpatiu,
                    self::SPATIU_CONSUM_COMUN_GENERAL  => $consumComunGeneralPeSpatiu,
                    self::SPATIU_CONSUM_COMUN_SCARA    => $consumComunScaraPeSpatiu,
                    self::SPATIU_DIFERENTA_SCARA       => @$diferentaSpatiiScaraPeSpatiu,
            ];
        }

        //// se repatizeaza consumul pe fiecare Spatiu necontorizat

        // calculez consumul pausal pe scara/grupul de scari
        $consumComunScara = $this->getConsumComunScara( $Scara );
        $consumPausalScara = $consumRepartizatScara - ( $sumaConsumuriSpatiiContorizate + $consumComunScara );

        foreach ($SpatiiPausal as $spatiuId => $Spatiu)
        {
            $ScaraSpatiu = $this->getScari()[ $Spatiu->{$Spatiu::SCARA_ID} ];

            $consumPausalSpatiu             = $this->repartizeazaConsumPausal( $SpatiiPausal, $Spatiu, $consumPausalScara );
            $consumComunScaraPeSpatiu       = $this->repartizeazaConsumComunScaraPeSpatiu( $ScaraSpatiu, $Spatiu );
            $consumComunGeneralPeSpatiu     = $this->repartizeazaConsumComunGeneralPeSpatiu( $Spatiu, $Cheltuiala );

            $consumRepartizatSpatiu = $consumPausalSpatiu + $consumComunScaraPeSpatiu + $consumComunGeneralPeSpatiu;

            $data[ $spatiuId ] = [
                    self::SPATIU_CONSUM_INREGISTRAT    => null,
                    self::SPATIU_CONSUM_PAUSAL         => $consumPausalSpatiu,
                    self::SPATIU_CONSUM_REPARTIZAT     => $consumRepartizatSpatiu,
                    self::SPATIU_CONSUM_COMUN_GENERAL  => $consumComunGeneralPeSpatiu,
                    self::SPATIU_CONSUM_COMUN_SCARA    => $consumComunScaraPeSpatiu,
            ];
        }

        foreach($SpatiiDebransate as $spatiuId => $Spatiu)
        {
            $ScaraSpatiu = $this->getScari()[ $Spatiu->{$Spatiu::SCARA_ID} ];

            $consumComunScaraPeSpatiu       = $this->repartizeazaConsumComunScaraPeSpatiu( $ScaraSpatiu, $Spatiu );
            $consumComunGeneralPeSpatiu     = $this->repartizeazaConsumComunGeneralPeSpatiu( $Spatiu, $Cheltuiala );

            $consumRepartizatSpatiu = $consumComunScaraPeSpatiu + $consumComunGeneralPeSpatiu;

            $data[ $spatiuId ] = [
                    self::SPATIU_CONSUM_INREGISTRAT    => null,
                    self::SPATIU_CONSUM_PAUSAL         => null,
                    self::SPATIU_CONSUM_REPARTIZAT     => $consumRepartizatSpatiu,
                    self::SPATIU_CONSUM_COMUN_GENERAL  => $consumComunGeneralPeSpatiu,
                    self::SPATIU_CONSUM_COMUN_SCARA    => $consumComunScaraPeSpatiu,
            ];
        }

        return $data;
    }

    /**
     * Repartizare consum pausal la un Spatiu
     *
     * @param App_Component_Spatiu_Object $Spatiu
     * @param float $consumNecontorizatScara
     *
     * @return float
     */
    protected function repartizeazaConsumPausal(array $SpatiiPausal, App_Component_Spatiu_Object $Spatiu, $consumNecontorizatScara)
    {
        $cacheId = 'valori-ponderate-consum-pausal-spatiu-' . $Spatiu->{$Spatiu::SCARA_ID};

        if (!isset($this->_cache[ $cacheId ]))
        {
            $criteriuConsumPausal = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_CONSUM_PAUSAL );
            if ( null === $criteriuConsumPausal)
            {
                $criteriuConsumPausal = Lib_Application::getInstance()->getConfig(['repartizare', 'apa',  'consum_pausal']);
            }

            $ponderi = [];
            foreach($SpatiiPausal as $spatiuId => $S)
            {
                $ponderi[ $spatiuId ] = $S->getValoareCriteriu( $criteriuConsumPausal );
            }

            $valoriPonderate = Lib_Tools::distributeWeighted( $consumNecontorizatScara, $ponderi );

            $this->_cache[ $cacheId ] = $valoriPonderate;
        }

        $valoare = @$this->_cache[ $cacheId ][ $Spatiu->id ];

        return $valoare;
    }

}

