<?php

class App_Component_ColoanaLista_Apa_Exception_FacturaNotFound extends UnexpectedValueException
{
    public function __construct($message, $code, $previous)
    {
        $message = 'Nu exista nici o factura inregistrata';

        parent::__construct($message, $code, $previous);
    }
}
