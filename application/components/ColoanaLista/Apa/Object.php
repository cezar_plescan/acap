<?php

/**
 *
 */
abstract class App_Component_ColoanaLista_Apa_Object extends App_Component_ColoanaLista_Object
{
    // consumuri corespunzatoare spatiilor
    const SPATIU_CONSUM_REPARTIZAT             = 'consum_repartizat_spatiu';
    const SPATIU_CONSUM_INREGISTRAT            = 'consum_inregistrat_spatiu';
    const SPATIU_CONSUM_COMUN_GENERAL          = 'consum_comun_general_spatiu';
    const SPATIU_CONSUM_COMUN_SCARA            = 'consum_comun_scara_spatiu';
    const SPATIU_DIFERENTA_SCARA               = 'diferente_scara_spatiu';

    const SUBCOLOANA_CANTITATE          = 'cantitate';
    const SUBCOLOANA_DIFERENTE          = 'diferente';
    const SUBCOLOANA_CONSUM_COMUN       = 'consum_comun';

    const CHELTUIALA_PRET_UNITAR            = 'cpu';
    const CHELTUIALA_CANTITATE              = 'cc';
    const CHELTUIALA_CANTITATE_REPARTIZATA  = 'ccr';
    const CHELTUIALA_CANTITATE_SCARA        = 'ccs';
    const CHELTUIALA_CANTITATE_ESTIMATA_SCARA = 'cces';
    const CHELTUIALA_DIFERENTA_MC           = 'dmc';


    protected
            $scari = [];

    public function compute()
    {
        $cheltuieli = $this->getCheltuieli();

        $this->scari = $this->Lista->getScari();

        $data = [];

        foreach( $cheltuieli as $cheltuiala )
        {
            $spatiiValues = $this->_compute( $cheltuiala );

            // verific ca spatiile sa nu se suprapuna
            foreach( $spatiiValues as $spatiuId => $values )
            {
                if ( isset( $data[ $spatiuId ] ) )
                {
                    throw new App_Component_Lista_Exception_Compute_Cheltuiala_SpatiuUsed( $this->Lista, $spatiuId, static::CHELTUIALA_LABEL );
                }

                $data[ $spatiuId ] = $values;
            }
        }

        return $data;
    }

    /**
     * @see pg. 591 pentru regulile de calcul
     *
     * @param array $cheltuiala
     *
     * @return array
     */
    protected function _compute( array $cheltuiala )
    {
        $data = [];

        $cheltuialaId = $cheltuiala[ App_Component_Cheltuiala_Object::PROP_ID ];

        // verific daca consumul este specificat
        $consumGeneral = @$cheltuiala[ App_Component_Cheltuiala_Object::PARAMETRI ][ App_Component_Cheltuiala_Apa_Object::PARAM_CONSUM_TOTAL_INREGISTRAT ];
        if ( !$consumGeneral )
        {
            throw new App_Component_Lista_Exception_Compute_Cheltuiala_ConsumGeneralUndefined( $cheltuiala[ App_Component_Cheltuiala_Object::PROP_DESCRIERE ] );
        }

        // verific valoare cheltuielii
        $valoareCheltuiala = @$cheltuiala[ App_Component_Cheltuiala_Object::PROP_VALUE ];
        if ( !$valoareCheltuiala )
        {
            throw new App_Component_Lista_Exception_Compute_Cheltuiala_ValoareUndefined( $cheltuiala[ App_Component_Cheltuiala_Object::PROP_DESCRIERE ] );
        }

        $diferenteGeneralPasanti = @$cheltuiala[ App_Component_Cheltuiala_Object::PARAMETRI ][ App_Component_Cheltuiala_Apa_Object::PARAM_DIFERENTA_GENERAL_PASANTI ];

        $cantitateFacturata = $consumGeneral + $diferenteGeneralPasanti;
        if ( !$cantitateFacturata )
        {
            throw new App_Component_Lista_Exception_Compute_Cheltuiala_CantitateUndefined( $cheltuiala[ App_Component_Cheltuiala_Object::PROP_DESCRIERE ] );
        }

        $pretUnitar = $valoareCheltuiala / $cantitateFacturata;

        //// determin Scarile contorizate/necontorizate si calculez totalul consumurilor inregistrate de scarile contorizate

        $scariContorizate = $scariNecontorizate = [];

        $scariCheltuiala = $cheltuiala[ App_Component_Cheltuiala_Object::PROP_TARGET ];
        $totalConsumScariContorizate = 0;

        foreach ($scariCheltuiala as $scaraId)
        {
            // scara contorizata
            if ( isset( $this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::CONSUM_SCARA ] ) )
            {
                $scariContorizate[] = $scaraId;

                $consumInregistratScara = $this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::CONSUM_SCARA ];
                $totalConsumScariContorizate += $consumInregistratScara;

                $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ][ self::CHELTUIALA_CANTITATE_SCARA ] = $consumInregistratScara;
            }
            // scara necontorizata
            else
            {
                $scariNecontorizate[] = $scaraId;
            }
        }

        if ( count( $scariNecontorizate ) )
        {
            // determin consumul pentru scarile necontorizate
            $criteriuRepartizareScariNecontorizate = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_CONSUM_SCARI_NECONTORIZATE );

            switch ($criteriuRepartizareScariNecontorizate)
            {
                case App_Component_Consum_Scara_Factory::REPARTIZARE_EGAL:

                    $consumScariNecontorizate = $consumGeneral - $totalConsumScariContorizate;
                    $consumEstimatScaraNecontorizata = $consumScariNecontorizate / count( $scariNecontorizate );

                    $totalConsumScariContorizate += $consumGeneral;

                    foreach( $scariNecontorizate as $scaraId )
                    {
                        $this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::CONSUM_SCARA ] = $consumEstimatScaraNecontorizata;

                        $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ][ self::CHELTUIALA_CANTITATE_ESTIMATA_SCARA ] = $consumEstimatScaraNecontorizata;
                    }

                    $scariContorizate = $scariCheltuiala;
                    $scariNecontorizate = [];

                break;

                case App_Component_Consum_Scara_Factory::REPARTIZARE_COMUN:
                    // scarile necontorizate se considera ca fiind una singura
                break;

                default:
                    throw new App_Component_Lista_Exception_Compute( 'Criteriul ales pentru repartizarea consumurilor la scările necontorizate este incorect.' );
            }
        }

        //// determin consumul pentru Scarile contorizate

        // repartizez diferentele general-pasanti

        if ( $diferenteGeneralPasanti )
        {
            $repartizareDiferente = Lib_Application::getInstance()->getUser()->getValoareParametru( static::REPARTIZARE_DIFERENTA_GENERAL_PASANTI );

            switch ( $repartizareDiferente )
            {
                case App_Component_Consum_Scara_Factory::REPARTIZARE_EGAL:
                    $countScari = count( $this->scari );
                    $cotaRepartizare = $diferenteGeneralPasanti / $countScari;

                    foreach( $this->scari as $scaraId => $scara )
                    {
                        $this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::SCARA_DIF_GEN_PAS ] = $cotaRepartizare;
                    }

                break;

                default:
                    throw new App_Component_Lista_Exception_Compute( 'Criteriul ales pentru repartizarea diferentelor general - pasanţi este incorect.' );
            }

        }

        foreach( $scariContorizate as $scaraId )
        {
            $consumInregistratScara         = @$this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::CONSUM_SCARA ];
            $diferentaGeneralPasantiPeScara = @$this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::SCARA_DIF_GEN_PAS ];

            // @todo: determinare si repartizare diferenta dintre totalul consumurilor scarilor contorizate si consumul general

            $consumRepartizatScara = $consumInregistratScara + $diferentaGeneralPasantiPeScara;

            $this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::SCARA_CONSUM_REPARTIZAT ] = $consumRepartizatScara;

            if( !isset( $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ] ) )
            {
                $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ] = [];
            }

            $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ] += [
                self::CHELTUIALA_COLOANA_ID             => $this->getColoanaId(),
                self::CHELTUIALA_COLOANA_CODE           => $this->getCode(),
                self::CHELTUIALA_DESCRIERE              => $cheltuiala[ App_Component_Cheltuiala_Object::PROP_DESCRIERE ],
                self::CHELTUIALA_VALOARE                => $valoareCheltuiala,
                self::CHELTUIALA_VALOARE_REPARTIZATA    => $consumRepartizatScara * $pretUnitar,
                self::CHELTUIALA_PRET_UNITAR            => $pretUnitar,
                self::CHELTUIALA_CANTITATE              => $cantitateFacturata,
                self::CHELTUIALA_CANTITATE_REPARTIZATA  => $consumRepartizatScara,
            ];

            if( Lib_Application::getInstance()->getUser()->getValoareParametru( App_Component_AsociatieParametru_Factory::APA_REP_DIF_SPATIU_SCARA ) == App_Component_Consum_Spatiu_Factory::REPARTIZARE_CONSUM )
            {

                $totalConsumuriSpatii = 0;

                $spatiiScara =  $this->scari[ $scaraId ][ App_Component_Scara_Object::SPATII_DATA ];

                foreach( $spatiiScara as $spatiuId => $spatiu )
                {
                    $consumInregistratSpatiu = @$spatiu[ App_Component_Spatiu_Object::CONSUMURI ][ static::CONSUM_SPATIU ];
                    $totalConsumuriSpatii += $consumInregistratSpatiu;
                }

                if( $totalConsumuriSpatii )
                {
                    $diferentaMc = ( $consumRepartizatScara - $totalConsumuriSpatii ) / $totalConsumuriSpatii * $pretUnitar;

                    if( $diferentaMc > 1e-12 || $diferentaMc < -1e-12)
                    {
                        $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ] += [
                            self::CHELTUIALA_DIFERENTA_MC => $diferentaMc
                        ];
                    }
                }
            }
        }

        foreach( $scariContorizate as $scaraId )
        {
            $dataScara = $this->computeValuesForScara( $scaraId, $cheltuiala );

            $data += $dataScara;
        }

        // @todo: efectuare calcule pentru scari necontorizate (se considera ca reprezentand o singura scara)


        return $data;
    }

    /**
    * @see pg. 591 pentru metoda de calcul
    *
    * @param $scaraId
    * @param $consumRepartizatScara
    * @param App_Component_Cheltuiala_Object $cheltuiala
    *
    * @return array [ spatiu_id => array( consumuri corespunzatoare spatiului ) ]
    */
    protected function computeValuesForScara( $scaraId, array $cheltuiala )
    {
        $data = [];

        $consumRepartizatScara = $this->scari[ $scaraId ][ App_Component_Scara_Object::CONSUMURI ][ static::SCARA_CONSUM_REPARTIZAT ];

        $spatiiScara =  $this->scari[ $scaraId ][ App_Component_Scara_Object::SPATII_DATA ];

        // salvez consumurile inregistrate

        $totalConsumuriSpatii = 0;
        $totalPersoaneSpatii = 0;
        $totalSuprafataSpatii = 0;

        foreach( $spatiiScara as $spatiuId => $spatiu )
        {
            $consumInregistratSpatiu = @$spatiu[ App_Component_Spatiu_Object::CONSUMURI ][ static::CONSUM_SPATIU ];
            if ( null === $consumInregistratSpatiu )
            {
                throw new App_Component_Lista_Exception_Compute( $this->Lista->getDenumireSpatiu( $spatiuId ) . ' nu are consumul înregistrat pentru factura de ' . static::CHELTUIALA_LABEL );
            }
            if ( 0 > $consumInregistratSpatiu )
            {
                throw new App_Component_Lista_Exception_Compute( 'Consumul de ' .  static::CHELTUIALA_LABEL . ' pentru ' . $this->Lista->getDenumireSpatiu( $spatiuId ) . ' nu poate fi negativ.' );
            }

            $data[ $spatiuId ][ self::SPATIU_CONSUM_INREGISTRAT ] = $consumInregistratSpatiu;

            $totalConsumuriSpatii += $consumInregistratSpatiu;
            $totalPersoaneSpatii += @$spatiu[ App_Component_Spatiu_Object::PARAMETRI ][ App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE ];
            $totalSuprafataSpatii += @$spatiu[ App_Component_Spatiu_Object::PARAMETRI ][ App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA ];
        }

        // diferente total consumuri inregistrate / consum repartizat scara
        $diferenteScaraSpatii = $consumRepartizatScara - $totalConsumuriSpatii;
        if ( $diferenteScaraSpatii )
        {
            $repartizareDiferente = Lib_Application::getInstance()->getUser()->getValoareParametru( App_Component_AsociatieParametru_Factory::APA_REP_DIF_SPATIU_SCARA );

            switch ( $repartizareDiferente )
            {
                case App_Component_Consum_Spatiu_Factory::REPARTIZARE_EGAL:
                    $countSpatii = count( $spatiiScara );
                    $cotaRepartizare = $diferenteScaraSpatii / $countSpatii;

                    foreach( $spatiiScara as $spatiuId => $spatiu )
                    {
                        $data[ $spatiuId ][ self::SPATIU_DIFERENTA_SCARA ] = $cotaRepartizare;
                    }

                break;

                case App_Component_Consum_Spatiu_Factory::REPARTIZARE_CONSUM:

                    if ( 0 == $totalConsumuriSpatii )
                    {
                        $message = 'Consumul total de ' . static::CHELTUIALA_LABEL .  ' declarat pe ' . $this->Lista->getDenumireScara( $scaraId ) . ' este zero.';
                        $message .= ' Diferenţa dintre consumul repartizat pe scara şi totalul consumurilor declarate pe apartamente nu poate fi repartizată în funcţie de criteriul ales (proporţional cu consumurile declarate pe apartamente).';

                        throw new App_Component_Lista_Exception_Compute( $message );
                    }

                    foreach( $spatiiScara as $spatiuId => $spatiu )
                    {
                        $consumInregistratSpatiu = $spatiu[ App_Component_Spatiu_Object::CONSUMURI ][ static::CONSUM_SPATIU ];
                        $cotaRepartizare = $diferenteScaraSpatii * $consumInregistratSpatiu / $totalConsumuriSpatii;

                        $data[ $spatiuId ][ self::SPATIU_DIFERENTA_SCARA ] = $cotaRepartizare;
                    }

                break;

                case App_Component_Consum_Spatiu_Factory::REPARTIZARE_PERSOANE:

                    if ( 0 == $totalPersoaneSpatii )
                    {
                        $message = 'Numărul de persoane din ' . $this->Lista->getDenumireScara( $scaraId ) . ' este zero.';
                        $message .= ' Diferenţa dintre consumul repartizat pe scara şi totalul consumurilor declarate pe apartamente nu poate fi repartizată în funcţie de criteriul ales (proporţional cu numărul de persoane).';

                        throw new App_Component_Lista_Exception_Compute( $message );
                    }

                    foreach( $spatiiScara as $spatiuId => $spatiu )
                    {
                        $nrPersoane = $spatiu[ App_Component_Spatiu_Object::PARAMETRI ][ App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE ];
                        $cotaRepartizare = $diferenteScaraSpatii * $nrPersoane / $totalPersoaneSpatii;

                        $data[ $spatiuId ][ self::SPATIU_DIFERENTA_SCARA ] = $cotaRepartizare;
                    }

                break;

                case App_Component_Consum_Spatiu_Factory::REPARTIZARE_SUPRAFATA:

                    if ( 0 == $totalSuprafataSpatii )
                    {
                        $message = 'Suma suprafeţelor apartamentelor din ' . $this->Lista->getDenumireScara( $scaraId ) . ' este zero.';
                        $message .= ' Diferenţa dintre consumul repartizat pe scara şi totalul consumurilor declarate pe apartamente nu poate fi repartizată în funcţie de criteriul ales (proporţional cu suprafaţa apartamentelor).';

                        throw new App_Component_Lista_Exception_Compute( $message );
                    }

                    foreach( $spatiiScara as $spatiuId => $spatiu )
                    {
                        $suprafata = $spatiu[ App_Component_Spatiu_Object::PARAMETRI ][ App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA ];
                        $cotaRepartizare = $diferenteScaraSpatii * $suprafata / $totalSuprafataSpatii;

                        $data[ $spatiuId ][ self::SPATIU_DIFERENTA_SCARA ] = $cotaRepartizare;
                    }

                break;

                default:
                    throw new App_Component_Lista_Exception_Compute( 'Criteriul ales pentru repartizarea diferentei dintre consumul repartizat pe scara si totalul consumurilor declarate pe apartamente este incorect.' );
            }

        }

        // @todo: repartizare consumuri comune

        $cantitateFacturata =
                @$cheltuiala[ App_Component_Cheltuiala_Object::PARAMETRI ][ App_Component_Cheltuiala_Apa_Object::PARAM_DIFERENTA_GENERAL_PASANTI ] +
                $cheltuiala[ App_Component_Cheltuiala_Object::PARAMETRI ][ App_Component_Cheltuiala_Apa_Object::PARAM_CONSUM_TOTAL_INREGISTRAT ];

        $pretUnitar = $cheltuiala[ App_Component_Cheltuiala_Object::PROP_VALUE ] / $cantitateFacturata;

        foreach( $data as $spatiuId => $spatiuValues )
        {
            $consumRepartizat = $spatiuValues[ self::SPATIU_CONSUM_INREGISTRAT ] + @$spatiuValues[ self::SPATIU_DIFERENTA_SCARA ];

            $data[ $spatiuId ][ self::SPATIU_CONSUM_REPARTIZAT ] = $consumRepartizat;

            $data[ $spatiuId ][ App_Component_Lista_Object::SUBCOLOANE ] = [
                $this->getSubcoloanaId( self::SUBCOLOANA_CANTITATE )    => [
                    App_Component_Lista_Object::VALUE   => $spatiuValues[ self::SPATIU_CONSUM_INREGISTRAT ],
                ],
            ];

            $subcoloanaDiferente = @$this->subcoloaneCodeIdMap[ self::SUBCOLOANA_DIFERENTE ];
            if ( $subcoloanaDiferente )
            {
                $data[ $spatiuId ][ App_Component_Lista_Object::SUBCOLOANE ][ $subcoloanaDiferente ] = [
                    App_Component_Lista_Object::VALUE   => $spatiuValues[ self::SPATIU_DIFERENTA_SCARA ],
                ];
            }

            $data[ $spatiuId ][ App_Component_Lista_Object::VALUE ] = $pretUnitar * $consumRepartizat;
        }

        return $data;
    }

}

