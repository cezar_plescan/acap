<?php

/**
 * @incomplete
 *
 */
abstract class App_Component_ColoanaLista_Object extends Lib_Component_Object
{
    const VALUE                = 'value';
    const SUBCOLOANE_VALUES    = 'valori_subcoloane';
    const DISPLAY_VALUE        = 'display_value';
    const SUBCOLOANE_DISPLAY_VALUES = 'valori_display_subcoloane';

    const CHELTUIALA_COLOANA_ID             = 'ccid';
    const CHELTUIALA_COLOANA_CODE           = 'ccc';
    const CHELTUIALA_DESCRIERE              = 'cd';
    const CHELTUIALA_VALOARE                = 'cv';
    const CHELTUIALA_VALOARE_REPARTIZATA    = 'cvr';
    const CHELTUIALA_VALOARE_PE_UNITATE     = 'cvu';
    const CHELTUIALA_VALOARE_PE_UNITATE_REDUCERE = 'cvur';
    const CHELTUIALA_DENUMIRE_REDUCERE      = 'cdr';

    protected
            $coloanaData = [],

            /**
             * @var App_Component_Lista_Object
             */
            $Lista,

            $cheltuieliScari = [],

            $subcoloaneCodeIdMap = [],

            /**
             * @var int Luna de lucru
             */
            $month,
            /**
             * @var App_Component_Coloana_Object
             */
            $Coloana,

            /**
             * @var App_Component_Spatiu_Object[]
             */
            $Spatii,

            /**
             * @var App_Component_Scara_Object[]
             */
            $Scari,


            /**
             * @var App_Component_Cheltuiala_Object[]
             */
            $Cheltuieli;

    /** (v)
     *
     */
    public function __construct(array $coloanaData, App_Component_Lista_Object $Lista)
    {
        $this->coloanaData  = $coloanaData;
        $this->Lista        = $Lista;

        // subcoloane
        $this->subcoloane = $coloanaData[ App_Component_Coloana_Object::SUBCOLOANE ];
        foreach( $this->subcoloane as $subcoloanaId => $subcoloana )
        {
            $this->subcoloaneCodeIdMap[ $subcoloana[ App_Component_Subcoloana_Object::CODE ] ] = $subcoloanaId;
        }
    }

    protected function getSubcoloanaId( $subcoloanaCode )
    {
        return $this->subcoloaneCodeIdMap[ $subcoloanaCode ];
    }

    /** (v)
     *
     * Calculeaza contributiile pentru o Coloana la toate spatiile
     * @see pg. 605 pentru structura rezultatelor
     *
     * @return array [spatiu_id: {subcoloane, value, display_value}]
     */
    abstract public function compute();

    public function getCode()
    {
        return $this->coloanaData[ App_Component_Coloana_Object::CODE ];
    }

    public function getCheltuieliScari()
    {
        return $this->cheltuieliScari;
    }

    /**
     *
     * @return App_Component_Coloana_Object
     */
    protected function getColoana()
    {
        return $this->Coloana;
    }

    /** (v)
     *
     * @return array
     */
    public function getSpatii()
    {
        return $this->Lista->getSpatii();
    }

    /**
     *
     * @return App_Component_Scara_Object[]
     */
    protected function getScari()
    {
        return $this->Scari;
    }

    /**
     *
     * @return int
     */
    protected function getMonth()
    {
        return $this->month;
    }

    /** (v)
     * Returneaza ID-ul obiectului ORM Coloana
     *
     * @return int
     */
    public function getColoanaId()
    {
        $id = $this->coloanaData[ App_Component_Coloana_Object::PROP_ID ];

        return $id;
    }

    /** (v)
     * Returneaza codul Coloanei
     *
     * @return string
     */
    public function getColoanaCode()
    {
        $code = $this->coloanaData[ App_Component_Coloana_Object::CODE ];

        return $code;
    }

    /**
     * Returneaza daca denumirea Coloanei va fi sau nu afisata in Lista de Plata
     *
     * @return bool
     */
    public function isLabelDisplayable()
    {
        return $this->getColoana()->{App_Component_Coloana_Object::IS_LABEL_DISPLAYABLE};
    }

    /**
     * Returneaza denumirea Coloanei
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->getColoana()->{App_Component_Coloana_Object::LABEL};
    }

    /**
     * Returneaza numarul de celule orizontale ocupat pentru afisarea intregii Coloane in Lista de Plata
     *
     * @return int
     */
    public function getWidth()
    {
        $paramsCount = count($this->getSubcoloane());

        if (0 == $paramsCount)
        {
            $width = 1;
        }
        else
        {
            $width = $paramsCount + ($this->hasValue() ? 1 : 0);
        }

        return $width;
    }

    /**
     * Returneaza Subcoloanele
     *
     * @return App_Component_Subcoloana_Object[]
     */
    public function getSubcoloane()
    {
        return $this->getColoana()->getSubcoloane();
    }

    /**
     *
     * @return bool
     */
    public function hasValue()
    {
        $has = (NULL !== $this->getColoana()->{App_Component_Coloana_Object::VALUE_LABEL}) || (0 == count($this->getSubcoloane()));

        return $has;
    }

    /**
     *
     * @return string
     */
    public function getValueLabel()
    {
        $label = $this->getColoana()->{App_Component_Coloana_Object::VALUE_LABEL};

        return $label;
    }

    /**
     *
     * @return int
     */
    public function getPrioritate()
    {
        $prioritate = $this->getColoana()->{App_Component_Coloana_Object::PRIORITY};

        return $prioritate;
    }

    /**
     *
     * @return int
     */
    public function isUsedBy(self $ColoanaLista)
    {
        $usedBy = $this->getColoana()->{App_Component_Coloana_Object::USED_BY};

        if ( $usedBy === $ColoanaLista->getColoanaCode())
        {
            return true;
        }

        return false;
    }

    /** (v)
     *
     * Returneaza cheltuielile repartizate pe coloana curenta
     *
     * @return array
     */
    protected function getCheltuieli()
    {
        $cheltuieliLista = $this->Lista->getCheltuieliColoane();
        $coloanaId = $this->coloanaData[ App_Component_Coloana_Object::PROP_ID ];

        $cheltuieli = isset( $cheltuieliLista[ $coloanaId ] ) ? $cheltuieliLista[ $coloanaId ] : [];

        return $cheltuieli;
    }

    /** (v)
     *
     * @param string $parametru
     *
     * @return array
     *
     * @throws App_Component_Lista_Exception_Compute_Pondere_Negativa
     * @throws App_Component_Lista_Exception_Compute_TotalPonderiNul
     */
    protected function computeUsingWeight( $parametru )
    {
        $data = [];

        // extrag toate cheltuielile corespunzatoare coloanei curente

        $cheltuieli = $this->getCheltuieli();
        $spatii = $this->getSpatii();

        /* @var $Cheltuiala App_Component_Cheltuiala_Object */
        foreach( $cheltuieli as $cheltuialaId => $cheltuiala )
        {
            $valoareCheltuiala = $cheltuiala[ App_Component_Cheltuiala_Object::PROP_VALUE ];
            if ( !$valoareCheltuiala ) continue;

            $spatiiCheltuiala = $cheltuiala[ App_Component_Cheltuiala_Object::PROP_TARGET ];

            // calculez suma ponderilor
            $ponderiSpatii = [];

            // verific daca exista exceptii de repartizare
            $exceptieData = $cheltuiala[ App_Component_Cheltuiala_Object::REDUCERE_REPARTIZARE ];
            if ( $exceptieData )
            {
                $ExceptieRepartizare = App_Component_ReducereRepartizare_Factory::getInstance()->getAlgorithm( $this->getCode(), $exceptieData, $this->Lista );
                $ponderiSpatii = $ExceptieRepartizare->getPonderi( $spatiiCheltuiala );
            }
            else
            {
                foreach( $spatiiCheltuiala as $spatiuId )
                {
                    $ponderiSpatii[ $spatiuId ] = @$spatii[ $spatiuId ][ App_Component_Spatiu_Object::PARAMETRI ][ $parametru ];
                }
            }

            $totalPonderi = 0;
            foreach( $ponderiSpatii as $pondere )
            {
                if ( $pondere < 0 )
                {
                    throw new App_Component_Lista_Exception_Compute_Pondere_Negativa( $this->Lista, $spatiuId, $parametru );
                }

                $totalPonderi += $pondere;
            }

            if ( !$totalPonderi )
            {
                throw new App_Component_Lista_Exception_Compute_TotalPonderiNul( $this->Lista, $spatiiCheltuiala, $parametru );
            }

            // calculare valori repartizate spatiilor

            foreach ($spatiiCheltuiala as $spatiuId)
            {
                $pondere = $ponderiSpatii[ $spatiuId ];

                // calculez contributia pt Spatiul curent
                $spatiuValue = $valoareCheltuiala / $totalPonderi * $pondere;

                // adun valoarea pe coloana
                if ( !isset( $data[ $spatiuId ][ 'value' ] ) )
                {
                    $data[ $spatiuId ][ 'value' ] = 0;
                }

                $data[ $spatiuId ][ 'value' ] += $spatiuValue;

                //// sumar cheltuieli

                $scaraId = $spatii[ $spatiuId ][ App_Component_Spatiu_Object::PROP_SCARA_ID ];
                if ( !isset( $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ] ) )
                {
                    $valoareUnitara = $valoareCheltuiala / $totalPonderi;

                    $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ] =
                    [
                        self::CHELTUIALA_COLOANA_ID             => $this->getColoanaId(),
                        self::CHELTUIALA_COLOANA_CODE           => $this->getCode(),
                        self::CHELTUIALA_DESCRIERE              => $cheltuiala[ App_Component_Cheltuiala_Object::PROP_DESCRIERE ],
                        self::CHELTUIALA_VALOARE                => $valoareCheltuiala,
                        self::CHELTUIALA_VALOARE_PE_UNITATE     => $valoareUnitara,
                    ];

                    if( $exceptieData )
                    {
                        $valoareUnitaraReducere = $ExceptieRepartizare->getReducere() * $valoareUnitara / 100;

                        $this->cheltuieliScari[ $scaraId ][ $cheltuialaId ] += [
                            self::CHELTUIALA_VALOARE_PE_UNITATE_REDUCERE    => $valoareUnitaraReducere,
                            self::CHELTUIALA_DENUMIRE_REDUCERE              => $ExceptieRepartizare->getDenumire(),
                        ];
                    }
                }

                @$this->cheltuieliScari[ $scaraId ][ $cheltuialaId ][ self::CHELTUIALA_VALOARE_REPARTIZATA ] += $spatiuValue;
            }

        }

        return $data;
    }

    /** (v)
     *
     * @param type $subcoloanaCode
     * @return type
     */
    protected function getSubcoloanaObject( $subcoloanaCode )
    {
        $paramClassName = Zend_Filter::filterStatic( $subcoloanaCode, 'Word_UnderscoreToCamelCase' );
        $resourceName   = 'Subcoloana_' . $paramClassName;

        /* @var $SubcoloanaLista App_Component_ColoanaLista_Subcoloana_Object */
        $SubcoloanaLista = $this->getResource( $resourceName, [$this] );

        return $SubcoloanaLista;
    }



    /** (v)
     * Metoda se aplica in cazul cand subcoloanele sunt independente unele de altele
     *
     * @return array
     */
    protected function computeSubcoloaneValues()
    {
        $data = [];

        /* @var $Subcoloana App_Component_Subcoloana_Object */
        foreach( $this->coloanaData[ 'subcoloane' ] as $subcoloanaId => $subcoloana )
        {
            $subcoloanaCode = $subcoloana[ 'code' ];

            $SubcoloanaObject = $this->getSubcoloanaObject( $subcoloanaCode );

            $spatiiSubcoloanaValues = $SubcoloanaObject->compute();

            foreach( $spatiiSubcoloanaValues as $spatiuId => $subcoloanaValues )
            {
                $data[ $spatiuId ][ 'subcoloane' ][ $subcoloanaId ] = $subcoloanaValues;
            }
        }

        return $data;
    }

}