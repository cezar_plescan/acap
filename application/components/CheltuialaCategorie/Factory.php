<?php

class App_Component_CheltuialaCategorie_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    const CATEG_UTILITATE_PUBLICA   = 'utilitate_publica';
    const CATEG_ASCENSOARE          = 'ascensoare';
    const CATEG_ADMINISTRATIV       = 'administrativ';
    
    public function getCategorii()
    {
        $cacheId = 'categorii';
        
        if( $this->notCache( $cacheId ) )
        {
            $columns = [
                App_Component_CheltuialaCategorie_Object::PROP_DENUMIRE,
                App_Component_CheltuialaCategorie_Object::PROP_CATEGORIE,
            ];

            $conditions = [
                App_Component_CheltuialaCategorie_Object::PROP_STATUS => App_Component_CheltuialaCategorie_Object::VAL_STATUS_ACTIVE,
            ];
            
            $categorii = $this->retrieveData( $columns, $conditions );

            $this->writeCache( $cacheId, $categorii );
        }
        
        return $this->readCache( $cacheId );
    }
    
    public function getCategorieName( $categorieId )
    {
        $categorii = $this->getCategorii();
        $name = $categorii[ $categorieId ][ App_Component_CheltuialaCategorie_Object::PROP_CATEGORIE ];
        
        return $name;
    }
    
}
