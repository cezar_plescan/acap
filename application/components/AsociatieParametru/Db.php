<?php

class App_Component_AsociatieParametru_Db extends App_Component_ParametruEntity_Db
{
    const TABLE = 'asociatie_parametru';

    static protected $instance;
    
    /** (v)
     *
     * @param int $month
     * @param int $asociatieId
     *
     * @return array
     */
    public function getAll( $asociatieId, $month )
    {
        $columns = $this->quoteColumns([
            App_Component_AsociatieParametru_Object::PROP_ID,
            App_Component_AsociatieParametru_Object::PROP_PARAMETRU,
            App_Component_AsociatieParametru_Object::PROP_VALUE,
        ]);

        $sql =
                $this->selectStmt( $columns ) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_AsociatieParametru_Object::PROP_REF_ID ),
                ]));

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }


}
