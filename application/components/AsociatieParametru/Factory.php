<?php

class App_Component_AsociatieParametru_Factory extends App_Component_ParametruEntity_Factory
{
    static protected $instance;

    // parametri disponibili

    /**
     * Modul de repartizare al diferentei dintre suma consumurilor inregistrate
     * pe fiecare scara si consumul inregistrat de contorul de bransament,
     * pentru apa rece.
     */
    const APA_REP_DIF_SCARA_GENERAL = 'apa_dif_sc_gen';

    /**
     * Modul de repartizare al diferentelor general-pasanti, pentru apa rece.
     */
    const APA_REP_DIF_GENERAL_PASANTI = 'apa_dif_gen_pas';

    /**
     * Modul de repartizare al diferentei dintre suma consumurilor inregistrate
     * pe fiecare spatiu dintr-o anumita scara si consumul inregistrat de contorul
     * din acea scara, pentru apa rece.
     */
    const APA_REP_DIF_SPATIU_SCARA = 'apa_dif_sp_sc';

    /**
     * Modul de repartizare al consumului comun dintr-o scara pe fiecare spatiu.
     * @see App_Component_Spatiu_Object - constantele {REPARTIZARE_...}
     */
    const APA_REP_CONSUM_COMUN_SCARA = 'apa_cc_sc';

    /**
     * Modul de repartizare al consumului comun general pe fiecare spatiu.
     * @see App_Component_Spatiu_Object - constantele {REPARTIZARE_...}
     */
    const APA_REP_CONSUM_COMUN_GENERAL = 'apa_cc_gen';

    const APA_REP_CONSUM_SCARI_NECONTORIZATE = 'apa_sc_necont';

    const INTERVAL_SCADENTA = 'interval_scadenta';

    const PRESEDINTE        = 'presedinte';
    const ADMINISTRATOR     = 'administrator';
    const CENZOR            = 'cenzor';
    const LUNA_CURENTA      = '_luna_curenta';
    
    static protected $toArray = [
        
    ];
    
    /** (v)
     *
     * @return array
     */
    public function getAll( $asociatieId, $month )
    {
        $data = $this->getDb()->getAll( $asociatieId, $month );
        $parametriData = [];

        foreach( $data as $row )
        {
            self::addValue( $parametriData, $row );
        }

        return $parametriData;
    }

    static protected function addValue( array &$data, array $row )
    {
        $parameter  = $row[ App_Component_ParametruEntity_Object::PROP_PARAMETRU ];
        $value      = $row[ App_Component_ParametruEntity_Object::PROP_VALUE ];
        
        if( in_array( $parameter, self::$toArray ) )
        {
            if( empty( $data[ $parameter ] ) )
            {
                $data[ $parameter ] = [];
            }
            
            $data[ $parameter ][] = [ $value, $row[ App_Component_AsociatieParametru_Object::PROP_ID ] ];
        }
        else
        {
            $data[ $parameter ] = $value;
        }
    }
    
}
