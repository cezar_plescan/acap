<?php

class App_Component_CalculatorContributii_TipCheltuiala_GazeNaturale extends App_Component_CalculatorContributii_TipCheltuiala_Abstract
{
    protected $Repartizare = null;
    
    protected function init()
    {
        $this->Repartizare = new App_Component_CalculatorContributii_Repartizare_Persoane();
    }
    
    public function compute( array $cheltuialaData )
    {
        $asociatieId    = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID );
        $parametri      = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PARAMETRI );
        
        $valoare        = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE );
        $repartizare    = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE );
        
        $spatiiRepartizare = $this->getSpatiiList( $repartizare, $asociatieId );
        
        $contributii = $this->Repartizare->compute( $valoare, $spatiiRepartizare );
        
        return $contributii;
    }
    
}
