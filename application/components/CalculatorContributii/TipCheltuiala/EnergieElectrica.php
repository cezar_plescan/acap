<?php

class App_Component_CalculatorContributii_TipCheltuiala_EnergieElectrica extends App_Component_CalculatorContributii_TipCheltuiala_Abstract
{
    protected $Repartizare = null;
    
    protected function init()
    {
        $this->Repartizare = new App_Component_CalculatorContributii_Repartizare_Persoane();
    }

    public function compute( array $cheltuialaData )
    {
        $asociatieId    = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID );
        $parametri      = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PARAMETRI );
        
        $contorId       = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_CONTOR );
        $repartizare    = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE );
        $valoare        = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE );
        
        $spatiiRepartizare = $this->getSpatiiList( $repartizare, $asociatieId );

        $contorData = App_Component_Configuratie_Factory::getInstance()->getConfiguratieValue( $contorId );
        $scutiriContor = Lib_Tools::checkInputDataKeys( $contorData, App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI, FALSE );
        
        $contributii = [];
        $rest = 100;
            
        if( $scutiriContor )
        {
            foreach( $scutiriContor as $instalatie )
            {
                $spatiiScutite = Lib_Tools::checkInputDataKeys( $instalatie, App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_SPATII );
                
                $spatiiPlata = array_diff( $spatiiRepartizare, $spatiiScutite );
                
                if( $spatiiPlata )
                {
                    $procent = Lib_Tools::checkInputDataKeys( $instalatie, App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_PROCENT );

                    $contributiiPartiale = $this->Repartizare->compute( $valoare * $procent / 100, $spatiiPlata );
                    $this->addContibutii( $contributii, $contributiiPartiale );

                    $rest -= $procent;
                }
            }
        }
        
        if( $rest < 0 )
        {
            throw new RangeException( 'Suma procentelor instalaţiilor depăşeşte 100' );
        }
        
        $contributiiRest = $this->Repartizare->compute( $valoare * $rest / 100, $spatiiRepartizare );
        $this->addContibutii( $contributii, $contributiiRest );
        
        return $contributii;
    }            
    
}
