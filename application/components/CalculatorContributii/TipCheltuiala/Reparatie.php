<?php

class App_Component_CalculatorContributii_TipCheltuiala_Reparatie extends App_Component_CalculatorContributii_TipCheltuiala_Abstract
{
    protected $Repartizare = null;
    
    protected function init()
    {
        $this->Repartizare = new App_Component_CalculatorContributii_Repartizare_Suprafata();
    }
    
    /**
     * Contributiile se calculeaza in functie de fondul de plata
     */
    public function compute( array $cheltuialaData )
    {
        $contributii = NULL;
        
        $parametri      = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PARAMETRI );
        $fondId         = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_FOND );
        
        if( App_Component_Fond_Factory::getInstance()->isRepartizareMandatory( $fondId )
            || ( App_Component_Fond_Factory::getInstance()->isRepartizareOptional( $fondId )
                 && isset( $parametri[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE ] ) )
        )
        {
            $asociatieId    = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID );
            $valoare        = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE );
            $repartizare    = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE );
            $spatii         = $this->getSpatiiList( $repartizare, $asociatieId );

            $contributii = $this->Repartizare->compute( $valoare, $spatii );
        }
        
        return $contributii;
    }
    
}
