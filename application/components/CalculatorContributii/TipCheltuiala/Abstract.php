<?php

abstract class App_Component_CalculatorContributii_TipCheltuiala_Abstract
{
    abstract public function compute( array $cheltuialaData );
    
    public function __construct()
    {
        $this->init();
    }
    
    protected function init() {}
    
    protected function getSpatiiList( $repartizare, $asociatieId )
    {
        return App_Component_Spatiu_Factory::getInstance()->getIDListByRepartizare( $asociatieId, $repartizare );
    }
    
    protected function addContibutii( array &$contributiiTotal, array $contributii )
    {
        foreach( $contributii as $spatiuId => $value )
        {
            if( !isset( $contributiiTotal[ $spatiuId ] ) )
            {
                $contributiiTotal[ $spatiuId ] = 0;
            }
            
            $contributiiTotal[ $spatiuId ] += $value;
        }
    }
    
}
