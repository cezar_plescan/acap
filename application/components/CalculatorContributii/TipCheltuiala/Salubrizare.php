<?php

class App_Component_CalculatorContributii_TipCheltuiala_Salubrizare extends App_Component_CalculatorContributii_TipCheltuiala_Abstract
{
    protected $Repartizare = null;
    
    protected function init()
    {
        $this->Repartizare = new App_Component_CalculatorContributii_Repartizare_Persoane();
    }
    
    public function compute( array $cheltuialaData )
    {
        $parametri      = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PARAMETRI );
        $asociatieId    = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID );
        
        $valoare        = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE );
        $repartizare    = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE );
        $reducere       = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE, FALSE );
        
        $spatiiRepartizare = $this->getSpatiiList( $repartizare, $asociatieId );
        
        if( $reducere )
        {
            $spatiiReducere     = Lib_Tools::checkInputDataKeys( $reducere, App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_SPATII );
            $procentReducere    = Lib_Tools::checkInputDataKeys( $reducere, App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_PROCENT );
            
            $contributii = $this->Repartizare->compute(
                    $valoare, 
                    $spatiiRepartizare, 
                    function( $spatiuId, $persoaneSpatiu ) use ( $spatiiReducere, $procentReducere )
                    {
                        if( isset( $spatiiReducere[ $spatiuId ] ) )
                        {
                            $persoaneReducere = $spatiiReducere[ $spatiuId ];
                            
                            $pondere = $persoaneSpatiu - $persoaneReducere + ( $persoaneReducere * $procentReducere / 100 );
                            
                            return $pondere;
                        }
                    }
            );
        }
        else
        {
            $contributii = $this->Repartizare->compute( $valoare, $spatiiRepartizare );
        }
        
        return $contributii;
    }
    
}
