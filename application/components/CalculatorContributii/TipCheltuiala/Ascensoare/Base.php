<?php

class App_Component_CalculatorContributii_TipCheltuiala_Ascensoare_Base extends App_Component_CalculatorContributii_TipCheltuiala_Abstract
{
    protected $Repartizare = null;
    
    protected function init()
    {
        $this->Repartizare = new App_Component_CalculatorContributii_Repartizare_Suprafata();
    }
    
    public function compute( array $cheltuialaData )
    {
        $asociatieId        = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID );
        $parametri          = Lib_Tools::checkInputDataKeys( $cheltuialaData, App_Component_Cheltuiala_Object::PARAMETRI );
        
        $ascensoareValues   = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_ASCENSOARE );
        $fond               = Lib_Tools::checkInputDataKeys( $parametri, App_Component_CheltuialaParametru_Factory::PARAMETRU_FOND );
        
        if( !is_array( $ascensoareValues ) )
        {
            throw new UnexpectedValueException( 'Invalid parameter "parametri/ascensoare"' );
        }
        
        $contributii = [];
        
        $repartizareMandatory = App_Component_Fond_Factory::getInstance()->isRepartizareMandatory( $fond );
        $repartizareOptional = App_Component_Fond_Factory::getInstance()->isRepartizareOptional( $fond );
        
        foreach( $ascensoareValues as $ascensorValues )
        {
            $valoare = Lib_Tools::checkInputDataKeys( $ascensorValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE );

            if( $repartizareMandatory ||
                ( $repartizareOptional && isset( $ascensorValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE ] ) )
            )
            {
                $repartizare = Lib_Tools::checkInputDataKeys( $ascensorValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE );
                $spatii = $this->getSpatiiList( $repartizare, $asociatieId );

                $contributiiAscensor = $this->Repartizare->compute( $valoare, $spatii );

                $this->addContibutii( $contributii, $contributiiAscensor );
            }
        }
        
        return $contributii;
    }
    
}
