<?php

class App_Component_CalculatorContributii_Repartizare_Persoane // extends App_Component_CalculatorContributii_Repartizare_Abstract
{
    protected function init()
    {
        
    }
    
    public function compute( $valoare, array $spatii, $pondereModifier = NULL )
    {
        $contributii = [];
        
        if( !$spatii )
        {
            return $contributii;
        }
        
        $ponderi = [];
        
        $persoaneSpatii = App_Component_SpatiuParametru_Factory::getInstance()->getParametruValueList( App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE, $spatii );
        
        foreach( $persoaneSpatii as $spatiuId => $persoane )
        {
            $pondere = $persoane;
            
            if( is_callable( $pondereModifier ) )
            {
                $result = $pondereModifier( $spatiuId, $persoane );
                
                if( $result !== NULL )
                {
                    $pondere = $result;
                }
            }
            
            $ponderi[ $spatiuId ] = $pondere;
        }
        
        $total = array_sum( $ponderi );

        if( $total == 0 )
        {
            throw new RangeException( 'Numărul persoanelor din următoarele spații este nul: ' . implode( ',', $spatii ) );
        }
        
        $unit = $valoare / $total;
        
        foreach( $ponderi as $spatiuId => $pondere )
        {
            $contributii[ $spatiuId ] = $unit * $pondere;
        }
        
        return $contributii;
    }
    
    public function getTotalPonderi( array $spatii )
    {
        $persoaneSpatii = App_Component_SpatiuParametru_Factory::getInstance()->getParametruValueList( App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE, $spatii );
        $total = array_sum( $persoaneSpatii );
        
        return $total;
    }
    
}
