<?php

class App_Component_CalculatorContributii_Repartizare_Suprafata // extends App_Component_CalculatorContributii_Repartizare_Abstract
{
    protected function init()
    {
        
    }
    
    public function compute( $valoare, array $spatii )
    {
        $contributii = [];
        
        if( !$spatii )
        {
            return $contributii;
        }
        
        $suprafataSpatii = App_Component_SpatiuParametru_Factory::getInstance()->getParametruValueList( App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA, $spatii );
        $total = array_sum( $suprafataSpatii );

        if( $total == 0 )
        {
            throw new RangeException( 'Suma suprafețelor următoarelor spații este nulă: ' . implode( ',', $spatii ) );
        }
        
        $unit = $valoare / $total;
        
        foreach( $suprafataSpatii as $spatiuId => $suprafata )
        {
            $contributii[ $spatiuId ] = $unit * $suprafata;
        }
        
        return $contributii;
    }
    
}
