<?php

class App_Component_CalculatorContributii_Worker extends Lib_Component_Abstract
{
    /**
     * 
     * @param array $cheltuialaData [ tip ]
     * @return type
     */
    public function compute( array $cheltuialaData )
    {
        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_TIP ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"id\"" );
        }
        
        $tipCheltuialaId = $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_TIP ];
        
        $Calculator = $this->getCalculator( $tipCheltuialaId );
        $contributii = $Calculator->compute( $cheltuialaData );
        
        return $contributii;
    }
    
    /**
     * 
     * @param int $tipCheltuialaId
     * @return App_Component_CalculatorContributii_TipCheltuiala_Abstract
     */
    protected function getCalculator( $tipCheltuialaId )
    {
        $tipName = App_Component_CheltuialaTip_Factory::getInstance()->getTipName( $tipCheltuialaId );
        $objectName = 'TipCheltuiala_' . Zend_Filter::filterStatic( $tipName, 'Word_UnderscoreToCamelCase' );
        
        $Calculator = $this->getResource( $objectName );
        
        return $Calculator;
    }
    
}
