<?php

abstract class App_Component_ConfiguratieParametru_Parametru_Abstract
{
    protected $isList = FALSE;
    
    public function isList()
    {
        return $this->isList;
    }
    
    static public function getData( $asociatieId, $configuratieId = NULL )
    {
        $data = App_Component_Configuratie_Factory::getInstance()->getData( $asociatieId, static::CATEGORIE, static::DENUMIRE, $configuratieId );
        
        return $data;
    }
    
    static public function getAllEntriesCount( $asociatieId )
    {
        $count = App_Component_Configuratie_Factory::getInstance()->getAllEntriesCount( $asociatieId, static::CATEGORIE, static::DENUMIRE );
        
        return $count;
    }
    
    static public function getParameterId()
    {
        $categorie = static::CATEGORIE;
        $parametru = static::DENUMIRE;
        
        $categorieId = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieId( $categorie );
        $parametruId = App_Component_ConfiguratieParametru_Factory::getInstance()->getParametruId( $parametru, $categorieId );
        
        return $parametruId;
    }
    
}
