<?php

class App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Validator_Update extends App_Component_ConfiguratieParametru_Validator_UpdateAbstract
{
    const ERR_ASCENSOR_INVALID      = 'ascensor_invalid';
    const ERR_ASCENSOR_USED         = 'ascensor_used';
    const ERR_SCARA_EMPTY           = 'scara_empty';
    const ERR_SCARA_DUPLICATE       = 'scara_duplicate';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_ASCENSOR_INVALID      => 'Ascensor inexistent.',
            self::ERR_ASCENSOR_USED         => 'Ascensorul de la %scara% nu poate fi eliminat deoarece există cheltuieli '
                                             . 'înregistrate în luna curentă care sunt repartizate la această scară.',
            self::ERR_SCARA_EMPTY           => 'Specificaţi scara la care se află ascensorul.',
            self::ERR_SCARA_DUPLICATE       => 'Există o scară specificată de două ori.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        $filteredValue = [ 
            'remove'    => [],
            'add'       => [],
        ];
        
        $this->makeArray( $inputValues );
        
        $asociatieId    = $this->getAsociatieId();
        
        $ascensoareActuale = App_Component_Configuratie_Factory::getInstance()->getData( 
                $asociatieId, 
                App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::CATEGORIE,
                App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::DENUMIRE );
        
        if( !$ascensoareActuale )
        {
            $ascensoareActuale = [];
        }
        
        $existingScariIds   = array_column( $ascensoareActuale, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
        $newScariIds        = array_column( $inputValues, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
        
        foreach( $ascensoareActuale as $ascensorValue )
        {
            $scaraIdAscensor    = Lib_Tools::checkInputDataKeys( $ascensorValue, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
            $ascensorId         = Lib_Tools::checkInputDataKeys( $ascensorValue, App_Component_Configuratie_Object::PROP_ID );
            
            if( !in_array( $scaraIdAscensor, $newScariIds ) )
            {
                $filteredParametruValue = $this->validateForRemove( $ascensorId );
                
                $filteredValue[ 'remove' ][] = $ascensorId;
            }
        }
        
        foreach( $inputValues as $ascensorValue )
        {
            $scaraIdAscensor = $this->getValueAtKey( $ascensorValue, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
            
            if( !in_array( $scaraIdAscensor, $existingScariIds ) )
            {
                $filteredParametruValue = $this->validateForAdd( $ascensorValue );

                $this->validateScaraDuplicate( $filteredParametruValue, $filteredValue[ 'add' ] );
                
                $filteredValue[ 'add' ][] = $filteredParametruValue;
            }
        }

        return $filteredValue;
    }

    protected function validateForRemove( $ascensorId )
    {
        $this->validateAscensorId( $ascensorId );
        $this->checkIfAscensorIsUsed( $ascensorId );
    }
        
    protected function validateForAdd( $ascensorValue )
    {
        $this->makeArray( $ascensorValue );
        
        $scara = $this->validateScaraId( $ascensorValue );
        
        $filteredValue = [
            App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA => $scara,
        ];
        
        return $filteredValue;
    }
        
    private function validateAscensorId( $ascensorId )
    {
        $asociatieId    = $this->getAsociatieId();
        
        $parametru      = App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::DENUMIRE;
        $categorie      = App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::CATEGORIE;
        
        $categorieId    = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieId( $categorie );
        $parametruId    = App_Component_ConfiguratieParametru_Factory::getInstance()->getParametruId( $parametru, $categorieId );
        
        $exists = App_Component_Configuratie_Factory::getInstance()->countActive([
                App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                App_Component_Configuratie_Object::PROP_ID              => $ascensorId,
                App_Component_Configuratie_Object::PROP_PARAMETRU_ID    => $parametruId,
        ]);
        
        if( !$exists )
        {
            $this->setError( self::ERR_ASCENSOR_INVALID );
        }
    }
    
    private function checkIfAscensorIsUsed( $ascensorId )
    {
        // extrag cheltuielile din luna curenta
        $asociatieId    = $this->getAsociatieId();
        $month          = $this->getMonth();
        
        $cheltuieliAscensoare = App_Component_Cheltuiala_Factory::getInstance()->getCheltuieliCurente( 
                $asociatieId, 
                $month, 
                [ App_Component_CheltuialaTip_Factory::TIP_INTRETINERE_ASCENSOR, 
                  App_Component_CheltuialaTip_Factory::TIP_AUTORIZATIE_ASCENSOR, 
                  App_Component_CheltuialaTip_Factory::TIP_LUCRARI_ASCENSOR ] );

        foreach( $cheltuieliAscensoare as $cheltuialaData )
        {
            $ascensoareCheltuiala = Lib_Tools::checkInputDataKeys( $cheltuialaData, [ App_Component_Cheltuiala_Object::PARAMETRI, App_Component_CheltuialaParametru_Factory::PARAMETRU_ASCENSOARE ] );
            
            if( isset( $ascensoareCheltuiala[ $ascensorId ] ) )
            {
                $ascensorData = App_Component_Configuratie_Factory::getInstance()->getConfiguratieValue( $ascensorId );
                $scaraId = Lib_Tools::checkInputDataKeys( $ascensorData, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
                $denumireScara = App_Component_Scara_Factory::getInstance()->getDenumireScara( $scaraId, [ 'abbr' => true ] );

                $this->setError( self::ERR_ASCENSOR_USED, [ 'scara' => $denumireScara ] );
            }
        }
    }
    
    private function validateScaraId( array $ascensorInputValue )
    {
        if( !isset( $ascensorInputValue[ App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA ] ) )
        {
            $this->setError( self::ERR_SCARA_EMPTY );
        }
        
        $scaraId = $ascensorInputValue[ App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA ];
        $allScari = App_Component_Scara_Factory::getInstance()->getIDList( $this->getAsociatieId() );

        if( !$this->ifNotInArray( $scaraId, $allScari, self::ERR_SCARA_EMPTY, true ) )
        {
            return $scaraId;
        }
    }
    
    private function validateScaraDuplicate( array $ascensorInputValue, array $addValues )
    {
        $scari = array_column( $addValues, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
        $scaraIdAscensor = Lib_Tools::checkInputDataKeys( $ascensorInputValue, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
        
        $this->ifInArray( $scaraIdAscensor, $scari, self::ERR_SCARA_DUPLICATE, true );
    }
    
}
