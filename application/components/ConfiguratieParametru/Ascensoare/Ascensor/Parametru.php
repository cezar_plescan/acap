<?php

class App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru extends App_Component_ConfiguratieParametru_Parametru_Abstract
{
    const DENUMIRE      = 'ascensor';
    const CATEGORIE     = App_Component_ConfiguratieCategorie_Factory::CATEGORIE_ASCENSOARE;
    
    const PARAM_SCARA   = 'scara_id';
    
    protected $isList = TRUE;

}
