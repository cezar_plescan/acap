<?php

class App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru extends App_Component_ConfiguratieParametru_Parametru_Abstract
{
    const DENUMIRE          = 'contor';
    const CATEGORIE         = App_Component_ConfiguratieCategorie_Factory::CATEGORIE_ENERGIE_ELECTRICA;
    
    const PARAM_SCARI       = 'scari';
    const PARAM_DENUMIRE    = 'denumire';
    const PARAM_SCUTIRI     = 'scutiri';
    
    const PARAM_SCUTIRI_DENUMIRE    = 'denumire';
    const PARAM_SCUTIRI_PROCENT     = 'procent';
    const PARAM_SCUTIRI_SPATII      = 'spatii';
    
    protected $isList = TRUE;
}
