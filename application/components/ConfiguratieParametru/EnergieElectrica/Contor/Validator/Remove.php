<?php

class App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Validator_Remove extends App_Component_ConfiguratieParametru_Validator_RemoveAbstract
{
    const ERR_CONFIG_ID_MISSING     = 'config_id_missing';
    const ERR_CONFIG_ID_USED        = 'config_used';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_CONFIG_ID_MISSING     => 'Specificaţi ID-ul contorului.',
            self::ERR_CONFIG_ID_USED        => 'Contorul nu poate fi eliminat deoarece este folosit de cheltuieli înregistrate în luna curentă.',
        ]);
    }
    
    protected function validate( $configuratieId )
    {
        $this->checkIfUsed( $configuratieId );
        
        if( !$this->hasErrors() )
        {
            $filteredValue = [
                App_Component_Configuratie_Object::PROP_ID => $configuratieId,
            ];

            return $filteredValue;
        }
    }

    protected function checkIfUsed( $configId )
    {
        $isUsed = App_Component_Configuratie_Db::getInstance()->isUsedByCheltuieli([
            'categorie'     => App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::CATEGORIE,
            'parametru'     => App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::DENUMIRE,
            'configuratie_id' => $configId,
            'asociatie_id'  => $this->getAsociatieId(),
            'month'         => $this->getMonth(),
        ]);
        
        $this->ifTrue( $isUsed, self::ERR_CONFIG_ID_USED, true );
    }

}
