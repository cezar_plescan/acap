<?php

class App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Validator_Add extends App_Component_ConfiguratieParametru_Validator_AddAbstract
{
    const ERR_DENUMIRE_MISSING      = 'denumire_missing';
    const ERR_DENUMIRE_EMPTY        = 'denumire_empty';
    const ERR_DENUMIRE_LENGTH       = 'denumire_length';
    const ERR_DENUMIRE_DUPLICATE    = 'denumire_duplicate';
    const ERR_SCARI_MISSING         = 'scari_missing';
    const ERR_SCARI_INVALID         = 'scari_invalid';
    const ERR_SCARI_EMPTY           = 'scari_empty';
    const ERR_INSTALATIE_DENUMIRE_MISSING   = 'instalatie_denumire_missing';
    const ERR_INSTALATIE_DENUMIRE_EMPTY     = 'instalatie_denumire_empty';
    const ERR_INSTALATIE_DENUMIRE_DUPLICATE = 'instalatie_denumire_duplicate';
    const ERR_INSTALATIE_DENUMIRE_LENGTH    = 'instalatie_denumire_length';
    const ERR_INSTALATIE_PROCENT_MISSING    = 'instalatie_procent_missing';
    const ERR_INSTALATIE_PROCENT_EMPTY      = 'instalatie_procent_empty';
    const ERR_INSTALATIE_PROCENT_INTERVAL   = 'instalatie_procent_interval';
    const ERR_SCUTIRI_SUMA_PROCENTE         = 'instalatie_suma_procente';
    const ERR_INSTALATIE_SPATII_MISSING     = 'instalatie_spatii_missing';
    const ERR_INSTALATIE_SPATII_EMPTY       = 'instalatie_spatii_empty';
    const ERR_INSTALATIE_SPATII_INVALID     = 'instalatie_spatii_invalid';
    
    const DENUMIRE_LENGTH               = 30;
    const INSTALATIE_DENUMIRE_LENGTH    = 20;
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_DENUMIRE_MISSING      => 'Specificaţi denumirea contorului',
            self::ERR_DENUMIRE_EMPTY        => 'Specificaţi denumirea contorului',
            self::ERR_DENUMIRE_LENGTH       => 'Denumirea contorului nu poate depăşi ' . Lib_Grammar::pluralize( self::DENUMIRE_LENGTH, 'caracter' ) . '.',
            self::ERR_DENUMIRE_DUPLICATE    => 'Denumirea folosită este atribuită deja unui alt contor.',
            self::ERR_SCARI_MISSING         => 'Specificaţi scările deservite.',
            self::ERR_SCARI_EMPTY           => 'Specificaţi scările deservite.',
            self::ERR_SCARI_INVALID         => 'Specificaţi scările deservite.',
            self::ERR_INSTALATIE_DENUMIRE_MISSING   => 'Specificaţi denumirea pentru %numeral% instalaţie.',
            self::ERR_INSTALATIE_DENUMIRE_EMPTY     => 'Specificaţi denumirea pentru %numeral% instalaţie.',
            self::ERR_INSTALATIE_DENUMIRE_DUPLICATE => 'Denumirea pentru %numeral% instalaţie este deja folosită.',
            self::ERR_INSTALATIE_DENUMIRE_LENGTH    => 'Denumirea pentru %numeral% instalaţie nu poate depăşi ' . Lib_Grammar::pluralize( self::INSTALATIE_DENUMIRE_LENGTH, 'caracter' ) . '.',
            self::ERR_INSTALATIE_PROCENT_MISSING    => 'Specificaţi valoarea procentuală estimată pentru %numeral% instalaţie.',
            self::ERR_INSTALATIE_PROCENT_EMPTY      => 'Specificaţi valoarea procentuală estimată pentru %numeral% instalaţie.',
            self::ERR_INSTALATIE_PROCENT_INTERVAL   => 'Valoarea procentuală estimată pentru %numeral% instalaţie trebuie să fie un număr întreg între 1 şi 99.',
            self::ERR_SCUTIRI_SUMA_PROCENTE         => 'Suma valorilor procentuale estimate pentru consumurile instalaţiilor nu poate depăşi 100.',
            self::ERR_INSTALATIE_SPATII_MISSING     => 'Selectaţi apartamentele în care locuiesc persoanele scutite de la plata consumului pentru %numeral% instalaţie.',
            self::ERR_INSTALATIE_SPATII_INVALID     => 'Selectaţi apartamentele în care locuiesc persoanele scutite de la plata consumului pentru %numeral% instalaţie.',
            self::ERR_INSTALATIE_SPATII_EMPTY       => 'Selectaţi apartamentele în care locuiesc persoanele scutite de la plata consumului pentru %numeral% instalaţie.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        $this->validateInputFormat( $inputValues );
        
        $denumireContor = $this->validateDenumireContor( $inputValues );
        $scariContor    = $this->validateScariContor( $inputValues );
        $scutiri        = $this->validateScutiri( $inputValues, $scariContor );
        
        if( !$this->hasErrors() )
        {
            $filteredValue = [
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_DENUMIRE   => $denumireContor,
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCARI      => $scariContor,
            ];
            
            if( $scutiri )
            {
                $filteredValue[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI ] = $scutiri;
            }

            return $filteredValue;
        }
    }

    protected function validateDenumireContor( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_DENUMIRE, self::ERR_DENUMIRE_MISSING, true );
        
        $denumire = $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_DENUMIRE ];
        
        $this->ifEmpty( $denumire, self::ERR_DENUMIRE_EMPTY, true );
        $this->ifLongerThan( $denumire, self::DENUMIRE_LENGTH, self::ERR_DENUMIRE_LENGTH, true );
        
        $this->checkDenumireDuplicate( $denumire );
        
        return $denumire;
    }
    
    protected function validateScariContor( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCARI, self::ERR_SCARI_MISSING, true );
        
        $scari = $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCARI ];
        
        $this->ifNotArray( $scari, self::ERR_SCARI_MISSING, true );
        $this->ifEmpty( $scari, self::ERR_SCARI_EMPTY, true );
        
        $this->validateScariIds( $scari );
        
        return $scari;
    }
    
    protected function validateScutiri( array $inputValues, array $scariContor )
    {
        if( !isset( $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI ] )
            || !is_array( $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI ] ) )
        {
            return NULL;
        }
        
        $filteredValue = [];
        $scutiri = $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI ];
        
        $denumiriInstalatii = [];
        $sumaProcente = 0;
        
        foreach( $scutiri as $index => $instalatie )
        {
            if( $this->ifNotArray( $instalatie ) )
            {
                continue;
            }
            
            $denumire = $this->validateInstalatieDenumire( $instalatie, $index + 1 );
            
            $this->ifInArray( 
                    $denumire, 
                    $denumiriInstalatii, 
                    [ self::ERR_INSTALATIE_DENUMIRE_DUPLICATE, [ 'numeral' => Lib_Grammar::numeralOrdinal( $index + 1, true ) ] ], 
                    TRUE );
            
            $denumiriInstalatii[] = $denumire;
            
            $procent = $this->validateInstalatieProcent( $instalatie, $index + 1 );
            
            $sumaProcente += $procent;
            $this->ifGreaterThan( $sumaProcente, 101, self::ERR_SCUTIRI_SUMA_PROCENTE, true );
            
            $spatii = $this->validateInstalatieSpatii( $instalatie, $scariContor, $index + 1 );
            
            $filteredValue[] = [
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_DENUMIRE   => $denumire,
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_PROCENT    => $procent,
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_SPATII     => $spatii,
            ];
        }
        
        return $filteredValue;
    }
    
    private function checkDenumireDuplicate( $denumire )
    {
        $contoare = App_Component_Configuratie_Factory::getInstance()->getParameterData( $this->getAsociatieId(), App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::getParameterId() );
        
        foreach( $contoare as $contor )
        {
            $denumireContor = Lib_Tools::checkInputDataKeys( $contor, App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_DENUMIRE );
            
            $this->ifEqual( $denumire, $denumireContor, self::ERR_DENUMIRE_DUPLICATE, TRUE );
        }
    }
    
    private function validateScariIds( array $scari )
    {
        $availableScari = App_Component_Scara_Factory::getInstance()->getIDList( $this->getAsociatieId() );
        
        $this->ifConditionThenError( array_diff( $scari, $availableScari ), self::ERR_SCARI_INVALID, true );
    }
    
    private function validateInstalatieDenumire( array $inputValues, $index )
    {
        $errorParams = [ 'numeral' => Lib_Grammar::numeralOrdinal( $index, true ) ];
        
        $this->ifKeyNotSet( 
                $inputValues, 
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_DENUMIRE,
                [ self::ERR_INSTALATIE_DENUMIRE_MISSING, $errorParams ],
                true );
        
        $denumire = $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_DENUMIRE ];
        
        $this->ifEmpty( $denumire, [ self::ERR_INSTALATIE_DENUMIRE_EMPTY, $errorParams ], true );
        $this->ifLongerThan( $denumire, self::INSTALATIE_DENUMIRE_LENGTH, [ self::ERR_INSTALATIE_DENUMIRE_LENGTH, $errorParams ], true );
        
        return $denumire;
    }
    
    private function validateInstalatieProcent( array $inputValues, $index )
    {
        $errorParams = [ 'numeral' => Lib_Grammar::numeralOrdinal( $index, true ) ];
        
        $this->ifKeyNotSet( 
                $inputValues, 
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_PROCENT,
                [ self::ERR_INSTALATIE_PROCENT_MISSING, $errorParams ],
                true );
        
        $procent = $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_PROCENT ];
        
        $this->ifEmpty( $procent, [ self::ERR_INSTALATIE_PROCENT_EMPTY, $errorParams ], true );
        $this->ifNotInteger( $procent, [ self::ERR_INSTALATIE_PROCENT_INTERVAL, $errorParams ], true );
        $this->ifGreaterThan( $procent, 99, [ self::ERR_INSTALATIE_PROCENT_INTERVAL, $errorParams ], true );
        $this->ifLessThan( $procent, 1, [ self::ERR_INSTALATIE_PROCENT_INTERVAL, $errorParams ], true );
        
        return $procent;
    }
    
    private function validateInstalatieSpatii( array $inputValues, array $scariContor, $index )
    {
        $errorParams = [ 'numeral' => Lib_Grammar::numeralOrdinal( $index, true ) ];
        
        $this->ifKeyNotSet( 
                $inputValues, 
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_SPATII,
                [ self::ERR_INSTALATIE_SPATII_MISSING, $errorParams ],
                true );
        
        $spatii = $inputValues[ App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCUTIRI_SPATII ];
        
        $this->ifEmpty( $spatii, [ self::ERR_INSTALATIE_SPATII_EMPTY, $errorParams ], true );
        
        $spatiiScari = App_Component_Spatiu_Factory::getInstance()->getIDListByRepartizare( 
                $this->getAsociatieId(), 
                [ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI => $scariContor ] );
        
        $this->ifConditionThenError( array_diff( $spatii, $spatiiScari ), [ self::ERR_INSTALATIE_SPATII_INVALID, $errorParams ], true );
        
        return $spatii;
    }
    
}
