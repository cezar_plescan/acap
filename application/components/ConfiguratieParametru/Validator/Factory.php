<?php

class App_Component_ConfiguratieParametru_Validator_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    public function getAddValidator( $parametruId, array $arguments = [] )
    {
        $Validator = $this->getValidatorObject( 'add', $parametruId, $arguments );
        
        return $Validator;
    }
    
    public function getEditValidator( $parametruId, array $arguments = [] )
    {
        $Validator = $this->getValidatorObject( 'edit', $parametruId, $arguments );
        
        return $Validator;
    }
    
    public function getRemoveValidator( $parametruId, array $arguments = [] )
    {
        $Validator = $this->getValidatorObject( 'remove', $parametruId, $arguments );
        
        return $Validator;
    }
    
    public function getUpdateValidator( $parametruId, array $arguments = [] )
    {
        $Validator = $this->getValidatorObject( 'update', $parametruId, $arguments );
        
        return $Validator;
    }
    
    private function getValidatorObject( $validatorType, $parametruId, array $instaceArgs = [] )
    {
        $conditions = [ 
            App_Component_ConfiguratieParametru_Object::PROP_ID => $parametruId
        ];

        $columns = [
            App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID,
            App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE,
        ];

        $parametruRow = App_Component_ConfiguratieParametru_Factory::getInstance()->retrieveData( $columns, $conditions );
        
        if( empty( $parametruRow ) )
        {
            throw new UnexpectedValueException( '$parametruId is invalid: ' . var_export( $parametruId, true ) );
        }
        
        $categorieId    = $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID ];
        
        $denumire       = $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE ];
        $categorie      = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieName( $categorieId );
        
        $Validator = $this->createObject( [ $validatorType, 'Validator', $denumire, $categorie ], [
            self::OPT_CTOR_ARGS => [ $instaceArgs ],
            self::OPT_LEVELS_UP => 1,
        ]);
        
        return $Validator;
    }
    
}
