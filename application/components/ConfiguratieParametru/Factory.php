<?php

class App_Component_ConfiguratieParametru_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    public function getParametri()
    {
        $cacheId = 'parametri';
        
        if( $this->notCache( $cacheId ) )
        {
            $columns = [
                App_Component_ConfiguratieParametru_Object::PROP_ID,
                App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE,
                App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID
            ];

            $rawData = $this->retrieveData( $columns );
            $categorii  = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorii();
            $parametri = [];

            foreach( $rawData as $row )
            {
                $pId = $row[ App_Component_ConfiguratieParametru_Object::PROP_ID ];
                $categorieId = $row[ App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID ];
                
                $categorie = $categorii[ $categorieId ];

                $parametri[ $pId ] = [
                    App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE   => $row[ App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE ],
                    App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID  => $categorie,
                ];
            }
            
            $this->writeCache( $cacheId, $parametri );
        }
        
        return $this->readCache( $cacheId );
    }
    
    public function isParameterAList( $categorie, $parametru = NULL )
    {
        if( $parametru === NULL )
        {
            $parametruId = $categorie;
            
            $parametru = $this->getParametruName( $parametruId );
            $categorie = $this->getCategorieName( $parametruId );
        }
        
        /* @var $Parametru App_Component_ConfiguratieParametru_Parametru_Abstract  */
        $Parametru = $this->createObject( [ 'parametru', $parametru, $categorie ] );
        
        $isList = $Parametru->isList();
        
        return $isList;
    }
    
    public function getParametruId( $parametru, $categorieId )
    {
        $parametruId = NULL;
        $columns = [ App_Component_ConfiguratieParametru_Object::PROP_ID ];

        $parametru = strtolower( trim( $parametru ) );
        
        $conditions = [
            App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID   => $categorieId,
            App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE       => $parametru,
        ];

        $parametruRow = $this->retrieveData( $columns, $conditions );
        
        if( isset( $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_ID ] ) )
        {
            $parametruId = $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_ID ];
        }
        
        return $parametruId;
    }
    
    public function getParametruName( $parametruId )
    {
        $parametruName = NULL;
        $columns = [ App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE ];

        $conditions = [
            App_Component_ConfiguratieParametru_Object::PROP_ID => $parametruId,
        ];

        $parametruRow = $this->retrieveData( $columns, $conditions );
        
        if( isset( $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE ] ) )
        {
            $parametruName = $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_DENUMIRE ];
        }
        
        return $parametruName;
    }
    
    public function getCategorieName( $parametruId )
    {
        $categorie = NULL;
        $columns = [ App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID ];

        $conditions = [
            App_Component_ConfiguratieParametru_Object::PROP_ID => $parametruId,
        ];

        $parametruRow = $this->retrieveData( $columns, $conditions );
        
        if( isset( $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID ] ) )
        {
            $categorieId = $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_CATEGORIE_ID ];
            
            $categorie = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieName( $categorieId );
        }
        
        return $categorie;
    }
    
    public function isOperationAllowed( $parametruId, $operation )
    {
        $allowed = FALSE;
        
        $columns = [
            App_Component_ConfiguratieParametru_Object::PROP_OPERATIONS,
        ];
        
        $conditions = [
            App_Component_ConfiguratieParametru_Object::PROP_ID => $parametruId,
        ];
        
        $parametruRow = $this->retrieveData( $columns, $conditions );
        
        if( isset( $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_OPERATIONS ] ) )
        {
            $operations = $parametruRow[ 0 ][ App_Component_ConfiguratieParametru_Object::PROP_OPERATIONS ];
            
            $allowed = (bool)( $operations & $operation );
        }
        
        return $allowed;
    }
    
}
