<?php

class App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Validator_Update extends App_Component_ConfiguratieParametru_Validator_UpdateAbstract
{
    const ERR_BRANSAMENT_INVALID    = 'bransament_invalid';
    const ERR_BRANSAMENT_USED       = 'bransament_used';
    const ERR_SCARA_EMPTY           = 'scara_empty';
    const ERR_SCARA_DUPLICATE       = 'scara_duplicate';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_BRANSAMENT_INVALID    => 'Branşament inexistent.',
            self::ERR_BRANSAMENT_USED       => 'Branşamentul de la %scara% nu poate fi eliminat deoarece este folosit de cheltuieli înregistrate în luna curentă.',
            self::ERR_SCARA_EMPTY           => 'Specificați scara aferentă branşamentului.',
            self::ERR_SCARA_DUPLICATE       => 'Există o scară specificată de două ori.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        $filteredValue = [ 
            'remove'    => [],
            'add'       => [],
        ];
        
        $this->makeArray( $inputValues );
        
        $asociatieId    = $this->getAsociatieId();
        
        $bransamenteActuale = App_Component_Configuratie_Factory::getInstance()->getData( 
                $asociatieId, 
                App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::CATEGORIE,
                App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::DENUMIRE );
        
        if( !$bransamenteActuale )
        {
            $bransamenteActuale = [];
        }
        
        $existingScariIds   = array_column( $bransamenteActuale, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
        $newScariIds        = array_column( $inputValues, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
        
        foreach( $bransamenteActuale as $bransamentValue )
        {
            $scaraIdBransament  = Lib_Tools::checkInputDataKeys( $bransamentValue, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
            $bransamentId       = Lib_Tools::checkInputDataKeys( $bransamentValue, App_Component_Configuratie_Object::PROP_ID );
            
            if( !in_array( $scaraIdBransament, $newScariIds ) )
            {
                $filteredParametruValue = $this->validateForRemove( $bransamentId );
                
                $filteredValue[ 'remove' ][] = $bransamentId;
            }
        }
        
        foreach( $inputValues as $bransamentValue )
        {
            $scaraIdBransament = $this->getValueAtKey( $bransamentValue, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
            
            if( !in_array( $scaraIdBransament, $existingScariIds ) )
            {
                $filteredParametruValue = $this->validateForAdd( $bransamentValue );

                $this->validateScaraDuplicate( $filteredParametruValue, $filteredValue[ 'add' ] );
                
                $filteredValue[ 'add' ][] = $filteredParametruValue;
            }
        }

        return $filteredValue;
    }
    
    protected function validateForRemove( $bransamentId )
    {
        $this->validateBransamentId( $bransamentId );
        $this->checkIfBransamentIsUsed( $bransamentId );
    }
        
    protected function validateForAdd( $bransamentValue )
    {
        $this->makeArray( $bransamentValue );
        
        $scara = $this->validateScaraId( $bransamentValue );
        
        $filteredValue = [
            App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA => $scara,
        ];
        
        return $filteredValue;
    }
        
    private function validateBransamentId( $bransamentId )
    {
        $asociatieId    = $this->getAsociatieId();
        
        $parametru      = App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::DENUMIRE;
        $categorie      = App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::CATEGORIE;
        
        $categorieId    = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieId( $categorie );
        $parametruId    = App_Component_ConfiguratieParametru_Factory::getInstance()->getParametruId( $parametru, $categorieId );
        
        $exists = App_Component_Configuratie_Factory::getInstance()->countActive([
                App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                App_Component_Configuratie_Object::PROP_ID              => $bransamentId,
                App_Component_Configuratie_Object::PROP_PARAMETRU_ID    => $parametruId,
        ]);
        
        if( !$exists )
        {
            $this->setError( self::ERR_BRANSAMENT_INVALID );
        }
    }
    
    private function checkIfBransamentIsUsed( $bransamentId )
    {
        $isUsed = App_Component_Configuratie_Factory::getInstance()->isUsedByCheltuieli([
            'categorie'     => App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::CATEGORIE,
            'parametru'     => App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::DENUMIRE,
            'configuratie_id' => $bransamentId,
            'asociatie_id'  => $this->getAsociatieId(),
            'month'         => $this->getMonth(),
        ]);
        
        if( $isUsed )
        {
            $bransamentData = App_Component_Configuratie_Factory::getInstance()->getConfiguratieValue( $bransamentId );
            $scaraId = Lib_Tools::checkInputDataKeys( $bransamentData, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
            $denumireScara = App_Component_Scara_Factory::getInstance()->getDenumireScara( $scaraId, [ 'abbr' => true ] );
            
            $this->setError( self::ERR_BRANSAMENT_USED, [ 'scara' => $denumireScara ] );
        }
    }
    
    private function validateScaraId( array $bransamentValue )
    {
        if( !isset( $bransamentValue[ App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA ] ) )
        {
            $this->setError( self::ERR_SCARA_EMPTY );
        }
        
        $scaraId = $bransamentValue[ App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA ];
        $allScari = App_Component_Scara_Factory::getInstance()->getIDList( $this->getAsociatieId() );

        if( !$this->ifNotInArray( $scaraId, $allScari, self::ERR_SCARA_EMPTY, true ) )
        {
            return $scaraId;
        }
    }
    
    private function validateScaraDuplicate( array $bransamentValue, array $addValues )
    {
        $scari = array_column( $addValues, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
        $scaraBransament = Lib_Tools::checkInputDataKeys( $bransamentValue, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
        
        $this->ifInArray( $scaraBransament, $scari, self::ERR_SCARA_DUPLICATE, true );
    }
    
}
