<?php

class App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru extends App_Component_ConfiguratieParametru_Parametru_Abstract
{
    const DENUMIRE      = 'bransament';
    const CATEGORIE     = App_Component_ConfiguratieCategorie_Factory::CATEGORIE_GAZE_NATURALE;
    
    const PARAM_SCARA   = 'scara_id';
    
    protected $isList = TRUE;
    
}
