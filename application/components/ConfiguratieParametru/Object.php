<?php

class App_Component_ConfiguratieParametru_Object extends Lib_Component_ORM_Object
{
    const PROP_DENUMIRE     = 'denumire';
    const PROP_CATEGORIE_ID = 'categorie_id';
    const PROP_OPERATIONS   = 'operations';
    
    const CATEGORIE         = 'categorie';
    
    const VAL_OPERATION_ADD         = 1;
    const VAL_OPERATION_EDIT        = 2;
    const VAL_OPERATION_DELETE      = 4;
    const VAL_OPERATION_UPDATE_ALL  = 8;
    
}
