<?php

class App_Component_Bloc_Object extends Lib_Component_ORM_Object
{
    // properties
    const PROP_ASOCIATIE_ID  = 'asociatie_id';
    const PROP_DENUMIRE      = 'denumire';
    const PROP_ADDING_MONTH  = 'adding_month';
    const PROP_STATUS        = 'status';

    // property values
    const VAL_STATUS_ACTIVE     = 0;
    const VAL_STATUS_DELETED    = 1;

    // other properties
    const MONTH = 'month';
    const PROP_SCARI = 'scari';

    const ATTR_DENUMIRE_MAX_LENGTH = 20;
    
    protected $_properties = [
        self::PROP_ID            => NULL,
        self::PROP_ASOCIATIE_ID  => NULL,
        self::PROP_DENUMIRE      => NULL,
        self::PROP_ADDING_MONTH  => NULL,
        self::PROP_STATUS        => self::VAL_STATUS_ACTIVE,
        self::PROP_CREATED_AT    => NULL,
        self::PROP_UPDATED_AT    => NULL,
    ];

    /**
     * @return array
     */
    public static function getActiveStatusCriteria()
    {
        $criteria = [
            self::PROP_STATUS    => self::VAL_STATUS_ACTIVE,
        ];
        
        return $criteria;
    }
    
    /**
     * 
     * @return App_Component_Bloc_Object
     */
    public function remove()
    {
        $this->setProperty( self::PROP_STATUS, self::VAL_STATUS_DELETED );
        $this->save();
        
        // stergere scari
        $this->removeScari();
        
        return $this;
    }
    
    public function getDenumire()
    {
        return $this->getProperty( self::PROP_DENUMIRE );
    }

//    public function getScari()
//    {
//        if ( !isset($this->_cache['scari']) )
//        {
//            $scariBloc = [];
//            $Scari = App_Component_Scara_Factory::getInstance()->getAll();
//
//            foreach ($Scari as $Scara)
//            {
//                /* @var $Scara App_Component_Scara_Object */
//                if ( $Scara->belongsTo( $this ) )
//                {
//                    $scariBloc[ $Scara->getId() ] = $Scara;
//                }
//            }
//
//            $this->_cache['scari'] = $scariBloc;
//        }
//
//        return $this->_cache['scari'];
//    }

    private function removeScari()
    {
        $scari = $this->getScariObjects();
        foreach( $scari as $Scara )
        {
            $Scara->remove();
        }
    }
    
    private function getScariObjects()
    {
        $scari = App_Component_Scara_Factory::getInstance()->getScariByBlocId( $this->getId() );
        
        return $scari;
    }
    
}
