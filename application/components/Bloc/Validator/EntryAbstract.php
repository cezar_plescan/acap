<?php

/**
 * Conditions:
 * - value is required
 * - have a maximum length
 * - should be unique per asociatie
 * - limit for active entries
 * - limit for all entries
 */
abstract class App_Component_Bloc_Validator_EntryAbstract extends App_Component_Asociatie_Validator_BaseAbstract
{
    const DENUMIRE_MAX_LENGTH       = App_Component_Bloc_Object::ATTR_DENUMIRE_MAX_LENGTH;
    
    const ERR_DENUMIRE_REQUIRED     = 'required';
    const ERR_DENUMIRE_MAX_LENGTH   = 'max_length';
    const ERR_DENUMIRE_DUPLICATE    = 'duplicate';
    const ERR_INVALID_BLOC_ID       = 'invalid_bloc_id';
    
    protected function init()
    {
        $this->addMessageTemplates([
            self::ERR_DENUMIRE_REQUIRED     => 'Completați denumirea blocului.',
            self::ERR_DENUMIRE_MAX_LENGTH   => 'Denumirea blocului nu poate depăși ' . self::DENUMIRE_MAX_LENGTH . ' caractere.',
            self::ERR_DENUMIRE_DUPLICATE    => 'Acestă denumire aparține unui alt bloc.',
            self::ERR_INVALID_BLOC_ID       => 'Blocul specificat nu este valid.',            
        ]);
    }
    
    protected function validateDenumire( $denumire, $blocId = NULL )
    {
        // verificarea existentei denumirii si a dimensiunii acesteia
        $denumire = trim( $denumire );
        
        $length = strlen( $denumire );
        
        if( $length == 0 )
        {
            $this->setError( self::ERR_DENUMIRE_REQUIRED );
        }
        elseif ( $length > self::DENUMIRE_MAX_LENGTH )
        {
            $this->setError( self::ERR_DENUMIRE_MAX_LENGTH );
        }
        
        // verificare unicitate
        $this->validateUniqueness( $denumire, $blocId );
        
        return $denumire;
    }

    protected function validateUniqueness( $denumire, $excludeBlocId = NULL )
    {
        $criteria = [
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $this->getAsociatieId(),
            App_Component_Bloc_Object::PROP_DENUMIRE        => $denumire,
        ];
        
        if( !empty( $excludeBlocId ) )
        {
            $criteria[ App_Component_Bloc_Object::PROP_ID ] = new Lib_Component_ORM_Db_Operator_NotEqual( $excludeBlocId );
        }
        
        $duplicates = App_Component_Bloc_Factory::getInstance()->countActive( $criteria );
        
        if( $duplicates )
        {
            $this->setError( self::ERR_DENUMIRE_DUPLICATE );
        }
    }

    protected function validateBlocId( $blocId )
    {
        $blocId = (int)$blocId;
        
        // verific ID-ul blocului: sa existe si sa apartina Asociatiei
        $Bloc = App_Component_Bloc_Factory::getInstance()->findActive( $blocId );
        
        if( !$Bloc
            || $Bloc->getProperty( App_Component_Bloc_Object::PROP_ASOCIATIE_ID ) != $this->getAsociatieId() )
        {
            $this->setError( self::ERR_INVALID_BLOC_ID );
        }
        
        return $blocId;
    }
    
}