<?php

/**
* Verificari:
* - ID-ul blocului sa apartina Asociatiei
* - blocul sa fie activ
* - denumirea sa existe
* - denumirea sa nu fie duplicata
* - denumirea sa contina un numar maxim de caractere
*/
class App_Component_Bloc_Validator_Edit extends App_Component_Bloc_Validator_EntryAbstract
{
    /**
     * 
     * @param array $value { App_Component_Bloc_Object::PROP_DENUMIRE, App_Component_Bloc_Object::PROP_ID }
     * @return type
     */
    protected function validate( $value )
    {
        $blocId = $this->validateBlocId( @$value[ App_Component_Bloc_Object::PROP_ID ] );
        
        $denumire = $this->validateDenumire( @$value[ App_Component_Bloc_Object::PROP_DENUMIRE ], $blocId );
        
        $filteredValue = [
            App_Component_Bloc_Object::PROP_DENUMIRE    => $denumire,
            App_Component_Bloc_Object::PROP_ID          => $blocId,
        ];
        
        return $filteredValue;
    }
   
}