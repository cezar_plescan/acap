<?php

/**
 * Date de intrare:
 * - App_Component_Bloc_Object::PROP_ID
 * 
 * Valori returnate:
 * - App_Component_Bloc_Object::PROP_ID
 * 
 * Conditii:
 * - ID-ul blocului sa fie specificat
 * - sa apartina de Asociatie
 * - blocul sa fie activ
 */
class App_Component_Bloc_Validator_Remove extends App_Component_Bloc_Validator_EntryAbstract
{
    /**
     * 
     * @param array $value { App_Component_Bloc_Object::PROP_ID }
     * @return type
     */
    protected function validate( $value )
    {
        $blocId = $this->validateBlocId( @$value[ App_Component_Bloc_Object::PROP_ID ] );
        
        $filteredValue = [
            App_Component_Bloc_Object::PROP_ID  => $blocId,
        ];
        
        return $filteredValue;
    }
    
}