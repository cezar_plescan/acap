<?php

/**
 * Conditions:
 * - value is required
 * - have a maximum length
 * - should be unique per asociatie
 * - limit for active entries
 * - limit for all entries
 */
class App_Component_Bloc_Validator_Add extends App_Component_Bloc_Validator_EntryAbstract
{
    const MAX_ENTRIES           = 20;
    const MAX_OPERATIONS        = 100;
    
    const ERR_MAX_ENTRIES           = 'max_entries';
    const ERR_MAX_OPERATIONS        = 'max_operations';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_MAX_ENTRIES   => 'Ați atins limita numărului de blocuri.',
            self::ERR_MAX_OPERATIONS => 'Ați depășit numărul admis de operații.',
        ]);
    }
    
    /**
     * 
     * @param array $value { App_Component_Bloc_Object::PROP_DENUMIRE }
     * @return type
     */
    protected function validate( $value )
    {
        // verificare numar de blocuri
        $this->checkMaxEntries();
        
        // verificare numar de operatiuni
        $this->checkMaxOperations();
        
        $denumire = $this->validateDenumire( @$value[ App_Component_Bloc_Object::PROP_DENUMIRE ] );
        
        $filteredValue = [
            App_Component_Bloc_Object::PROP_DENUMIRE  => $denumire,
        ];
        
        return $filteredValue;
    }
    
    protected function checkMaxEntries()
    {
        $criteria = [
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID     => $this->getOption( self::OPT_ASOCIATIE_ID ),
        ];
        
        $count = App_Component_Bloc_Factory::getInstance()->countActive( $criteria );
        
        if( $count >= self::MAX_ENTRIES )
        {
            $this->setError( self::ERR_MAX_ENTRIES );
        }
    }
    
    protected function checkMaxOperations()
    {
        $criteria = [
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID     => $this->getOption( self::OPT_ASOCIATIE_ID ),
        ];
        
        $count = App_Component_Bloc_Factory::getInstance()->count( $criteria );
        
        if( $count >= self::MAX_OPERATIONS )
        {
            $this->setError( self::ERR_MAX_OPERATIONS );
        }
    }

}