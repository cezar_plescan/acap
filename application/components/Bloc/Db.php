<?php

class App_Component_Bloc_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'bloc';
    
    static protected $instance;
    
    /** (v)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAll( $asociatieId, $month = NULL )
    {
        $columns = $this->quoteColumns([
            App_Component_Bloc_Object::PROP_ID,
            App_Component_Bloc_Object::PROP_DENUMIRE,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_Bloc_Object::PROP_ASOCIATIE_ID ),
                        $this->getActiveStatusQuery(),
                ]));

                // nu se face ordonare; de aceasta operatie se ocupa frontend-ul
                // rezultatele sunt returnate in ordinea in care au fost adaugate in DB
//                $this->orderBy(
//                        [App_Component_Bloc_Object::PROP_DENUMIRE]
//                );

        $results = $this->queryAndFetchAll( $sql );

        return $results;
    }

    protected function getActiveStatusQuery()
    {
        $sqlPart = $this->whereConditions( App_Component_Bloc_Object::getActiveStatusCriteria() );

        return $sqlPart;
    }

    public function getIDList( $asociatieId )
    {
        $columns = $this->quoteColumns([
            App_Component_Bloc_Object::PROP_ID,
        ]);

        $sql =  $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_Bloc_Object::PROP_ASOCIATIE_ID ),
                        $this->getActiveStatusQuery(),
                ]));

        $results = $this->queryAndFetchAll( $sql );
        $list = array_column( $results, App_Component_Bloc_Object::PROP_ID );

        return $list;
    }
    
}
