<?php

class App_Component_Bloc_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    /** (v)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return type
     */
    public function getAll( $asociatieId, $month )
    {
        $cacheId = "all-$asociatieId-$month";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $data = $this->getDb()->getAll( $asociatieId, $month );

            $blocuriData = [];

            foreach( $data as $rowData )
            {
                $id = $rowData[ App_Component_Bloc_Object::PROP_ID ];
                $rowData[ App_Component_Bloc_Object::PROP_SCARI ] = [];

                $blocuriData[ $id ] = $rowData;
            }

            $scari = App_Component_Scara_Factory::getInstance()->getAll( $asociatieId, $month );
            foreach( $scari as $scaraId => $scara )
            {
                $blocId = $scara[ App_Component_Scara_Object::PROP_BLOC_ID ];
                $blocuriData[ $blocId ][ App_Component_Bloc_Object::PROP_SCARI ][] = $scaraId;
            }

            $this->_cache[ $cacheId ] = $blocuriData;
        }

        return $this->_cache[ $cacheId ];
    }

    /**
     * Returneaza lista cu toate blocurile din Asociatie
     * 
     * @param int $asociatieId
     * @return array
     */
    public function getList( $asociatieId )
    {
        $cacheId = "list-$asociatieId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $data = $this->getDb()->getAll( $asociatieId );

            $this->_cache[ $cacheId ] = $data;
        }

        return $this->_cache[ $cacheId ];
    }
    
    /**
     * Returneaza lista cu ID-urile tuturor blocurilor din Asociatie
     * 
     * @param int $asociatieId
     * @return array
     */
    public function getIDList( $asociatieId )
    {
        $cacheId = "id-list-$asociatieId";

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $data = $this->getDb()->getIDList( $asociatieId );

            $this->_cache[ $cacheId ] = $data;
        }

        return $this->_cache[ $cacheId ];
    }
    
    public function getBlocData( $blocId )
    {
        $rawData = $this->retrieveData([
            App_Component_Bloc_Object::PROP_DENUMIRE,
            App_Component_Bloc_Object::PROP_ID,
        ], [
            App_Component_Bloc_Object::PROP_ID => $blocId,
        ] + App_Component_Bloc_Object::getActiveStatusCriteria() );
        
        $blocData = Lib_Tools::checkInputDataKeys( $rawData, 0 );
        
        return $blocData;
    }
    
    public function getStructura( $asociatieId )
    {
        $blocuri    = $this->getList( $asociatieId );
        $scari      = App_Component_Scara_Factory::getInstance()->getList( $asociatieId );
        $spatii     = App_Component_Spatiu_Factory::getInstance()->getList( $asociatieId );

        $structura = $blocuri;
        foreach( $structura as $indexBloc => $blocData )
        {
            //// pentru fiecare bloc se adauga scarile componente
            
            $scariDinBloc = [];
            $blocId = $blocData[ App_Component_Bloc_Object::PROP_ID ];
            
            foreach ( $scari as $indexScara => $scaraData )
            {
                $spatiiDinScara = [];
                $scaraId = $scaraData[ App_Component_Scara_Object::PROP_ID ];
                
                foreach( $spatii as $indexSpatiu => $spatiuData )
                {
                    // grupare spatii din scara curenta
                    if( $spatiuData[ App_Component_Spatiu_Object::PROP_SCARA_ID ] == $scaraId )
                    {
                        $spatiiDinScara[] = $spatiuData;
                    }
                }
                
                // adaugare spatii la scara corespunzatoare
                $scaraData[ App_Component_Scara_Object::PROP_SPATII ] = $spatiiDinScara;
                
                // grupare scari din blocul curent
                if( $scaraData[ App_Component_Scara_Object::PROP_BLOC_ID ] == $blocId )
                {
                    $scariDinBloc[] = $scaraData;
                }
            }
            
            // adaugare scari la blocul corespunzator
            $structura[ $indexBloc ][ App_Component_Bloc_Object::PROP_SCARI ] = $scariDinBloc;
        }
        
        return $structura;
    }
    
    /**
     * 
     * @param array $blocValues 
     * { App_Component_Bloc_Object::ASOCIATIE_ID, 
     *   App_Component_Bloc_Object::DENUMIRE, 
     *   App_Component_Bloc_Object::ADDING_MONTH 
     * }
     * 
     * @return App_Component_Bloc_Object
     */
    public function add( array $blocValues )
    {
        // filter the input
        $data = Lib_Tools::filterArray( $blocValues, [ 
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID, 
            App_Component_Bloc_Object::PROP_DENUMIRE, 
            App_Component_Bloc_Object::PROP_ADDING_MONTH 
        ]);
        
        // create an object and save it
        $Bloc = $this->createAndSave( [ $data ] );
        
        return $Bloc;
    }
    
    /**
     * 
     * @param array $blocData { App_Component_Bloc_Object::ID }
     * 
     */
    public function remove( array $blocData )
    {
        $Bloc = $this->find( $blocData[ App_Component_Bloc_Object::PROP_ID ] );
        
        if( $Bloc )
        {
            $Bloc->remove();
        }
        
        return $Bloc;
    }
    
    /**
     * 
     * @param array $blocData 
     * { App_Component_Bloc_Object::ID, 
     *   App_Component_Bloc_Object::DENUMIRE
     * }
     * 
     * @return App_Component_Bloc_Object
     */
    public function edit( array $blocData )
    {
        $Bloc = $this->find( $blocData[ App_Component_Bloc_Object::PROP_ID ] );
        
        if( $Bloc )
        {
            $Bloc->setProperty( App_Component_Bloc_Object::PROP_DENUMIRE, $blocData[ App_Component_Bloc_Object::PROP_DENUMIRE ] );
            $Bloc->save();
        }
        
        return $Bloc;
    }
    
}
