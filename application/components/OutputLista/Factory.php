<?php

class App_Component_OutputLista_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    protected
            $_cache = [];

    public function getContent( $asociatieId, $month )
    {
        return $this->getDb()->getContent( $asociatieId, $month );
    }

    public function getPdfContent( $asociatieId, $month )
    {
        return $this->getDb()->getPdfContent( $asociatieId, $month );
    }

    public function saveContent( $asociatieId, $month, $content )
    {
        // eliminare spatii multiple
        $content = preg_replace(['/'.PHP_EOL.'/', '/>\s*/', '/\s*</', '/\s+/'], ['', '>', '<',' '], $content);

        return $this->getDb()->saveContent( $asociatieId, $month, $content );
    }

    protected function savePdfContent( $asociatieId, $month, $content )
    {
        return $this->getDb()->savePdfContent( $asociatieId, $month, $content );
    }

    public function closeMonth( $asociatieId, $month )
    {
        // create PDF content
        try
        {
            $pdfConverter = new Lib_PdfConverter_Converter();
            $pdfConverter->convert( $this->getContent( $asociatieId, $month ) );
            $this->savePdfContent( $asociatieId, $month, $pdfConverter->getOutput() );
        }
        catch( PdfcrowdException $why )
        {
            Lib_Logger_Exception::getInstance()->log( $why );
        }

        return $this->getDb()->closeMonth( $asociatieId, $month );
    }

    /** (v)
     *
     * Returneaza codurile lunilor inchise
     */
    public function getClosedMonthIds( $asociatieId )
    {
        $cacheId = 'months';

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $months = $this->getDb()->getClosedMonthIds( $asociatieId );
            $this->_cache[ $cacheId ] = $months;
        }

        return $this->_cache[ $cacheId ];
    }

    public function getLastClosedMonthId( $asociatieId )
    {
        $cacheId = 'last-month';

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $months = $this->getClosedMonthIds( $asociatieId );
            $firstMonth = reset( $months );

            $this->_cache[ $cacheId ] = $firstMonth;
        }

        return $this->_cache[ $cacheId ];
    }


}
