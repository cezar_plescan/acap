<?php

class App_Component_OutputLista_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'output_lista';

    public function getContent( $asociatieId, $month )
    {
        $columns = $this->quoteColumns( App_Component_OutputLista_Object::CONTENT );

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereEqual($month, App_Component_OutputLista_Object::MONTH),
                        $this->whereEqual($asociatieId, App_Component_OutputLista_Object::ASOCIATIE_ID),
                ]));

        $content = trim( $this->queryAndFetchColumn($sql) );

        return $content;
    }

    public function saveContent( $asociatieId, $month, $content )
    {
        $columns = $this->quoteColumns([
            App_Component_OutputLista_Object::ASOCIATIE_ID,
            App_Component_OutputLista_Object::MONTH,
            App_Component_OutputLista_Object::CONTENT,
        ]);

        $sql =
            'INSERT into ' . $this->quoteTableName() . '(' . $columns . ')' . ' VALUES (? , ? , ?)'
            . ' ON DUPLICATE KEY UPDATE ' . $this->quoteColumns( App_Component_OutputLista_Object::CONTENT ) . ' = ?';


        $stmt = new Zend_Db_Statement_Pdo($this->getAdapter(), $sql);
        $driverStmt = $stmt->getDriverStatement();

        $null_1 = $content;
        $null_2 = $content;

        $driverStmt->bind_param('iiss', $asociatieId, $month, $null_1, $null_2);

        $chunks = str_split($content, 102400);
        foreach($chunks as $data)
        {
            $driverStmt->send_long_data(3, $data);
            $driverStmt->send_long_data(4, $data);
        }

        $stmt->execute();
    }

    public function getPdfContent( $asociatieId, $month )
    {
        $columns = $this->quoteColumns( App_Component_OutputLista_Object::PDF_CONTENT );

        $sql =
                $this->selectStmt($columns) .

                $this->where($this->whereAnd([
                        $this->whereEqual($month, App_Component_OutputLista_Object::MONTH),
                        $this->whereEqual($asociatieId, App_Component_OutputLista_Object::ASOCIATIE_ID),
                ]));

        $content = trim( $this->queryAndFetchColumn($sql) );

        return $content;
    }

    public function savePdfContent( $asociatieId, $month, $content )
    {
        $sql =
            'UPDATE ' . $this->quoteTableName() . 'SET ' . $this->quoteColumns( App_Component_OutputLista_Object::PDF_CONTENT ) . ' = ? '
            . $this->where( $this->whereAnd([
                $this->whereEqual( $asociatieId, App_Component_OutputLista_Object::ASOCIATIE_ID ),
                $this->whereEqual( $month, App_Component_OutputLista_Object::MONTH ),
            ]));


        $stmt = new Zend_Db_Statement_Pdo($this->getAdapter(), $sql);
        $driverStmt = $stmt->getDriverStatement();

        $null_1 = $content;

        $driverStmt->bind_param('s', $null_1);

        $chunks = str_split($content, 102400);
        foreach($chunks as $data)
        {
            $driverStmt->send_long_data(1, $data);
        }

        $stmt->execute();
    }

    public function closeMonth( $asociatieId, $month )
    {
        $asociatieId = Lib_Application::getInstance()->getUserId();

        $sql =
                'UPDATE ' . $this->quoteTableName() . ' SET ' . $this->quoteColumns( App_Component_OutputLista_Object::CLOSED ) . ' = ' . $this->quoteValue( App_Component_OutputLista_Object::CLOSED_TRUE ) .

                $this->where( $this->whereAnd([
                        $this->whereEqual($month, App_Component_OutputLista_Object::MONTH),
                        $this->whereEqual($asociatieId, App_Component_OutputLista_Object::ASOCIATIE_ID),
                ]));

        $this->query($sql);

        return true;
    }

    /** (o)
     *
     * @param type $asociatieId
     * @return type
     */
    public function getClosedMonthIds( $asociatieId )
    {
        $columns = $this->quoteColumns( App_Component_OutputLista_Object::MONTH );

        $sql =
                $this->selectStmt( $columns ) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_OutputLista_Object::ASOCIATIE_ID ),
                        $this->whereEqual( App_Component_OutputLista_Object::CLOSED_TRUE, App_Component_OutputLista_Object::CLOSED ),
                ])) .

                $this->orderBy(
                        [ App_Component_OutputLista_Object::MONTH, 'DESC' ]
                );

        $months = [];

        $result = $this->queryAndFetchAll($sql);
        foreach ($result as $row)
        {
            $months[] = $row[ App_Component_OutputLista_Object::MONTH ];
        }

        return $months;
    }


}
