<?php

class App_Component_OutputLista_Object extends Lib_Component_ORM_Object
{
    // fields
    const ASOCIATIE_ID  = 'asociatie_id';
    const CONTENT       = 'content';
    const PDF_CONTENT   = 'pdf_content';
    const MONTH         = 'month';
    const CLOSED        = 'closed';

    // field values
    const CLOSED_TRUE   = 1;
    const CLOSED_FALSE  = 0;

    protected $_properties = [
        self::PROP_ID            => NULL,
        self::ASOCIATIE_ID  => NULL,
        self::MONTH         => NULL,
        self::CLOSED        => NULL,
        self::CONTENT       => NULL,
        self::PROP_TIMESTAMP     => NULL,
    ];

    public function getContent()
    {
        return $this->getProperty( self::CONTENT );
    }

}
