<?php

class App_Component_OutputLista_Validator_CloseMonth extends Lib_Validator_Abstract
{
    const OPT_ASOCIATIE = 'a';

    const ERR_DUPLICATE = 'duplicate';

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID   => 'Luna nu poate fi închisă',
            self::ERR_DUPLICATE => 'Luna este deja închisă',
        ));

        $this->setRequired( false );
        $this->setSkipValidation( false );
    }

    /**
     * Doar luna de lucru poate fi inchisa
     *
     * @param type $monthId (unused)
     * @return type
     * @throws UnexpectedValueException
     */
    protected function _isValid( $monthId )
    {
        $Asociatie = $this->getOption( self::OPT_ASOCIATIE );
        if ( !( $Asociatie instanceof App_Component_Asociatie_Object ) )
        {
            throw new UnexpectedValueException( 'ARG_ASOCIATIE should be of App_Component_Asociatie_Object class type' );
        }

        $workingMonthId = $Asociatie->getLunaCurenta();

        if ( !$workingMonthId )
        {
            $this->_setError( self::ERR_INVALID );
        }

        $isClosed = App_Component_OutputLista_Factory::getInstance()->count([
            App_Component_OutputLista_Object::ASOCIATIE_ID      => $Asociatie->getId(),
            App_Component_OutputLista_Object::MONTH             => $workingMonthId,
            App_Component_OutputLista_Object::CLOSED            => App_Component_OutputLista_Object::CLOSED_TRUE,
        ]);

        if ( $isClosed )
        {
            $this->_setError( self::ERR_DUPLICATE );
        }

        return $workingMonthId;
    }

}
