<?php

class App_Component_Incasare_Validator_Date extends Lib_Validator_Date
{
    const ERR_FUTURE = 'f';

    protected function init()
    {
        parent::init();

        $this->addErrorTemplates(array(
            self::ERR_INVALID           => 'Data nu este corectă',
            self::ERR_FUTURE            => 'Data nu poate fi în viitor',
            self::ERR_REQUIRED          => 'Completaţi data încasării',
        ));

    }

    protected function _isValid($value)
    {
        $value = parent::_isValid($value);

        $DateUtility = Lib_Date::getInstance();

        if( $DateUtility->compareDays($value, $DateUtility->now()) > 0 )
        {
            $this->_setError( self::ERR_FUTURE );
        }

        return $value;
    }

}
