<?php

class App_Component_Incasare_Validator_Value extends Lib_Validator_Number_Decimal
{
    const ERR_NULL      = 'null';
    const ERR_NEGATIVE  = 'negative';

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID           => 'Valoarea nu este corectă',
            self::ERR_NULL              => 'Valoarea nu poate fi zero',
            self::ERR_NEGATIVE          => 'Valoarea nu poate fi negativă',
            self::ERR_REQUIRED          => 'Completaţi valoarea încasării',
        ));

    }

    protected function _isValid($value)
    {
        $value = parent::_isValid($value);

        if ( !$value )
        {
            $this->_setError( self::ERR_NULL );
        }

        if( $value < 0 )
        {
            $this->_setError( self::ERR_NEGATIVE );
        }

        return $value;
    }

}
