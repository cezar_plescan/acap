<?php

class App_Component_Incasare_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'incasari';

    /** (v)
     *
     * @param array $criteriaArguments
     * @return string
     */
    protected function getActiveStatusQuery()
    {
        $query = $this->whereEqual( App_Component_Incasare_Object::STATUS_ACTIVE, App_Component_Incasare_Object::STATUS );

        return $query;
    }

    /**
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getNotCounted( $asociatieId )
    {
        $columns = $this->quoteColumns([
                App_Component_Incasare_Object::PROP_ID,
                App_Component_Incasare_Object::SPATIU_ID,
                App_Component_Incasare_Object::VALUE,
                App_Component_Incasare_Object::COLOANA_ID,
        ]);

        $sql =
                $this->selectStmt($columns) .

                $this->where( $this->whereAnd([
                    $this->getActiveStatusQuery(),
                    $this->whereEqual( $asociatieId, App_Component_Incasare_Object::ASOCIATIE_ID ),
                ]) ) .
                $this->orderBy( [ App_Component_Incasare_Object::PROP_TIMESTAMP ] );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

    public function deleteIncasare( $asociatieId, $incasareId )
    {
        $data = [
            App_Component_Incasare_Object::STATUS => App_Component_Incasare_Object::STATUS_DELETED,
        ];

        $where = $this->whereAnd([
            $this->whereEqual( $incasareId, App_Component_Incasare_Object::PROP_ID ),
            $this->whereEqual( $asociatieId, App_Component_Incasare_Object::ASOCIATIE_ID ),
        ]);

        $this->update($data, $where);
    }

}
