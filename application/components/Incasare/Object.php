<?php

class App_Component_Incasare_Object extends Lib_Component_ORM_Object
{
    // properties
    const ASOCIATIE_ID          = '_asociatie_id';
    const MONTH                 = 'month';
    const SPATIU_ID             = 'spatiu_id';
    const VALUE                 = 'valoare';
    const COLOANA_ID            = 'coloana_id';
    const DATE                  = 'data';
    const STATUS                = 'status';

    // property values
    const STATUS_DELETED    = 0;
    const STATUS_ACTIVE     = 1;
    const STATUS_COUNTED    = 2;

    protected $_properties = [
        self::PROP_ID                    => NULL,
        self::ASOCIATIE_ID          => NULL,
        self::MONTH                 => NULL,
        self::SPATIU_ID             => NULL,
        self::VALUE                 => NULL,
        self::COLOANA_ID            => NULL,
        self::DATE                  => NULL,
        self::STATUS                => self::STATUS_ACTIVE,
        self::PROP_TIMESTAMP             => NULL,
    ];

    static public function getActiveStatusCriteria()
    {
        $criteria = [
            self::STATUS => self::STATUS_ACTIVE,
        ];

        return $criteria;
    }

}