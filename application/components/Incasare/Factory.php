<?php

class App_Component_Incasare_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    const INCASARE_INTRETINERE = App_Component_Coloana_Factory::COLOANA_INTRETINERE;
//    const INCASARE_INTRETINERE = App_Component_Coloana_Factory::COLOANA_INTRETINERE;
//    const INCASARE_INTRETINERE = App_Component_Coloana_Factory::COLOANA_INTRETINERE;
//    const INCASARE_INTRETINERE = App_Component_Coloana_Factory::COLOANA_INTRETINERE;


    /**
     * Incasarile care nu au fost procesate
     *
     * @param type $asociatieId
     * @return type
     */
    public function getNotCountedBySpatiu( $asociatieId )
    {
        $data = $this->getDb()->getNotCounted( $asociatieId );
        $incasariData = [];

        foreach ($data as $rowData)
        {
            $spatiuId = $rowData[ App_Component_Incasare_Object::SPATIU_ID ];
            if ( !isset( $incasariData[ $spatiuId ] ) )
            {
                $incasariData[ $spatiuId ] = [];
            }

            $incasareId = $rowData[ App_Component_Incasare_Object::PROP_ID ];
            $incasariData[ $spatiuId ][ $incasareId ] = $rowData;
        }

        return $incasariData;
    }


    /** (v)
     *
     * Adauga o incasare
     *
     * @param array $values
     *      The keys of the array are:
     *          App_Component_Incasare_Object::ASOCIATIE_ID,
     *          App_Component_Incasare_Object::MONTH,
     *          App_Component_Incasare_Object::SPATIU_ID,
     *          App_Component_Incasare_Object::VALUE,
     *          App_Component_Incasare_Object::COLOANA_ID
     *
     * @return App_Component_Incasare_Object
     */
    public function add( array $values )
    {
        $values = Lib_Tools::filterArray( $values, [
            App_Component_Incasare_Object::ASOCIATIE_ID,
            App_Component_Incasare_Object::MONTH,
            App_Component_Incasare_Object::SPATIU_ID,
            App_Component_Incasare_Object::VALUE,
            App_Component_Incasare_Object::COLOANA_ID,
            App_Component_Incasare_Object::DATE,
        ]);

        $values[ App_Component_Incasare_Object::STATUS ] = App_Component_Incasare_Object::STATUS_ACTIVE;

        // salvare cheltuiala
        $Incasare = $this->createAndSave( [$values] );

        return $Incasare;
    }

    /**
     *
     * @param int $incasareId
     */
    public function deleteIncasare( $incasareId )
    {
        $this->getDb()->deleteIncasare( $incasareId );
    }

}
