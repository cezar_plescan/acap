<?php

class App_Component_Cheltuiala_Object extends Lib_Component_ORM_Object
{
    // properties
    const PROP_ASOCIATIE_ID = 'asociatie_id';
    const PROP_MONTH        = 'month';
    const PROP_STATUS       = 'status';
    const PROP_TIP          = 'tip_id';
    
    const PARAMETRI         = 'parametri';
    const CONTRIBUTII       = 'contributii';
    
    // property values
    const VAL_STATUS_ACTIVE     = 0;
    const VAL_STATUS_DELETED    = 1;
    const VAL_STATUS_PENDING    = 2;

    protected $_properties = [
        self::PROP_ID                    => NULL,
        self::PROP_ASOCIATIE_ID          => NULL,
        self::PROP_MONTH                 => NULL,
        self::PROP_STATUS                => self::VAL_STATUS_ACTIVE,
        self::PROP_CREATED_AT            => NULL,
        self::PROP_UPDATED_AT            => NULL,
    ];

    static public function getActiveStatusCriteria()
    {
        $criteria = [
            self::PROP_STATUS => self::VAL_STATUS_ACTIVE,
        ];

        return $criteria;
    }

    /**
     * Stergere logica din DB
     */
    public function disable()
    {
        $this->setProperty( self::PROP_STATUS, self::VAL_STATUS_DELETED );
        $this->save();

        return $this;
    }

//    public function getTarget()
//    {
//        if ( !isset($this->_cache['target']) )
//        {
//            $this->_cache['target'] = $this->decodeTarget($this->getProperty( self::TARGET ));
//        }
//
//        return $this->_cache['target'];
//    }

//    public function getTargetObjects()
//    {
//        $cacheId = 'target_objects';
//
//        if ( !isset($this->_cache[ $cacheId ]) )
//        {
//            $TargetObjects = [];
//
//            $target = $this->getTarget();
//            foreach ( $target as $id )
//            {
//                $object = $this->_getTargetObject( $id );
//                if ( $object )
//                {
//                    $TargetObjects[ $id ] = $object;
//                }
//            }
//
//            $this->_cache[ $cacheId ] = $TargetObjects;
//        }
//
//        return $this->_cache[ $cacheId ];
//
//    }

//    protected function _getTargetObject( $id )
//    {
//        // implicit, se considera ca ID-urile apartin spatiilor
//        $Spatii = App_Component_Spatiu_Factory::getInstance()->getAll();
//
//        return @$Spatii[ $id ];
//    }

    protected function decodeTarget($target)
    {
        $target = explode(self::TARGET_DELIMITER, $target);
        if ( $target === false )
        {
            $target = [];
        }

        return $target;
    }

    protected function _preSave()
    {
        $target = $this->getProperty( self::PROP_TARGET );
        if ( is_array( $target ) )
        {
            $this->setProperty( self::PROP_TARGET, $this->encodeTarget($target) );
        }
    }

    protected function encodeTarget(array $target)
    {
        $target = implode(self::TARGET_DELIMITER, $target);

        return $target;
    }

//    /**
//     * Returneaza parametrii unei Cheltuieli
//     *
//     * @return App_Component_Parametru_Cheltuiala_Object[]
//     */
//    public function getParametri()
//    {
//        if (null == $this->parametri)
//        {
//            $ParametruCheltuialaFactory = App_Component_Parametru_Cheltuiala_Factory::getInstance();
//            $ParametriCheltuiala = $ParametruCheltuialaFactory->getByCheltuiala($this->{self::ID});
//
//            $this->parametri = $ParametriCheltuiala;
//        }
//
//        return $this->parametri;
//    }

//    public function getValoareParametru($parametruName)
//    {
//        $parametri = $this->getParametri();
//        if (isset($parametri[ $parametruName ]))
//        {
//            /* @var $ParametruSpatiu App_Component_Parametru_Cheltuiala_Object */
//            $ParametruSpatiu = $parametri[ $parametruName ];
//
//            $valoareParametru = $ParametruSpatiu->getValoare();
//        }
//        else
//        {
//            // throw new UnexpectedValueException('Parametrul "' . $parametruName . '" nu exista.');
//            $valoareParametru = null;
//        }
//
//        return $valoareParametru;
//    }

//    public function getValoare()
//    {
//        return $this->getProperty( self::VALUE );
//    }

//    /**
//     * @return App_Component_ExceptieCheltuiala_Object
//     */
//    public function getExceptieRepartizare()
//    {
//        if ( !isset($this->_cache['exceptie']) )
//        {
//            $exceptie = App_Component_ReducereRepartizare_Factory::getInstance()->getByCheltuiala( $this->getId() );
//
//            $this->_cache['exceptie'] = $exceptie;
//        }
//
//        return $this->_cache['exceptie'];
//    }

//    public function getDescriere()
//    {
//        return $this->getProperty( self::DESCRIERE );
//    }

//    public function getColoanaId()
//    {
//        return $this->getProperty( self::COLOANA_ID );
//    }

//    public function getCreatedAt()
//    {
//        return $this->getProperty( self::CREATED_AT );
//    }

}