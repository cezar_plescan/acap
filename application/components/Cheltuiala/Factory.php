<?php

class App_Component_Cheltuiala_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;
    
    protected $CalculatorContributii = null;
    
    protected function __construct()
    {
        parent::__construct();
        
        $this->CalculatorContributii = new App_Component_CalculatorContributii_Worker();
    }

    /**
     *
     * Adauga o cheltuiala si parametrii aferenti
     *
     * @return array
     */
    public function add( array $cheltuialaData )
    {
        // salvare cheltuiala
        $cheltuialaId = $this->insert( $cheltuialaData );
        
        $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] = $cheltuialaId;
        
        $returnData = $this->setExtraData( $cheltuialaData );
        
        return $returnData;
    }

    /**
     * Se pot edita doar parametrii cheltuielii
     * 
     * @param array $cheltuialaInputData [ id, parametri ]
     */
    public function edit( array $cheltuialaInputData )
    {
        if( !isset( $cheltuialaInputData[ App_Component_Cheltuiala_Object::PROP_ID ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"id\"" );
        }
        
        if( !isset( $cheltuialaInputData[ App_Component_Cheltuiala_Object::PARAMETRI ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"parametri\"" );
        }
        
        $cheltuialaInputData = Lib_Tools::filterArray( $cheltuialaInputData, [
                App_Component_Cheltuiala_Object::PROP_ID,
                App_Component_Cheltuiala_Object::PARAMETRI,
        ]);
        
        $cheltuialaId = $cheltuialaInputData[ App_Component_Cheltuiala_Object::PROP_ID ];
        
        $cheltuialaMainData = $this->getCheltuialaMainData( $cheltuialaId );
        $cheltuialaData = $cheltuialaMainData + $cheltuialaInputData;
        
        $returnData = $this->setExtraData( $cheltuialaData );
        
        return $returnData;
    }
    
    /**
     * @param array $cheltuialaData [ id ]
     * @throws UnexpectedValueException
     */
    public function confirm( array $cheltuialaData )
    {
        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"id\"" );
        }
        
        $DB = $this->getDb();
        
        $newData = [
            App_Component_Cheltuiala_Object::PROP_STATUS => App_Component_Cheltuiala_Object::VAL_STATUS_ACTIVE,
        ];
        
        $condition = [ App_Component_Cheltuiala_Object::PROP_ID => $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] ];
        $DB->update( $newData, $condition );
    }
    
    /**
     * @param array $cheltuialaData [ id ]
     * @throws UnexpectedValueException
     */
    public function remove( array $cheltuialaData )
    {
        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"id\"" );
        }
        
        $DB = $this->getDb();
        
        $newData = [
            App_Component_Cheltuiala_Object::PROP_STATUS => App_Component_Cheltuiala_Object::VAL_STATUS_DELETED,
        ];
        
        $condition = [ App_Component_Cheltuiala_Object::PROP_ID => $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] ];
        $DB->update( $newData, $condition );
    }
    
    public function discardPending()
    {
        $DB = $this->getDb();
        
        $DB->delete([
            App_Component_Cheltuiala_Object::PROP_STATUS => App_Component_Cheltuiala_Object::VAL_STATUS_PENDING,
        ]);
    }
    
    public function getCheltuieliCurente( $asociatieId, $month, $tipCheltuiala = NULL )
    {
        $tipCheltuiala = (array)$tipCheltuiala;
        sort( $tipCheltuiala );
        $tipCheltuialaKey = implode( '-', $tipCheltuiala );
        
        $cacheId = "cheltuieli-curente-$asociatieId-$month-$tipCheltuialaKey";
        
        if( $this->notCache( $cacheId ) )
        {
            $cheltuieliData = [];

            $cheltuieliList = $this->getCheltuieliCurenteList( $asociatieId, $month, $tipCheltuiala );
            $cheltuieliIds = array_column( $cheltuieliList, App_Component_Cheltuiala_Object::PROP_ID );
            $parametriList = App_Component_CheltuialaParametru_Factory::getInstance()->getParametriByReferences( $cheltuieliIds );
            $contributiiList = App_Component_ContributieSpatiu_Factory::getInstance()->getContributiiByCheltuieli( $cheltuieliIds );

            foreach( $cheltuieliList as $cheltuiala )
            {
                $cheltuialaId = $cheltuiala[ App_Component_Cheltuiala_Object::PROP_ID ];

                $parametriCheltuiala = $parametriList[ $cheltuialaId ];

                $cheltuialaData = $cheltuiala;
                $cheltuialaData[ App_Component_Cheltuiala_Object::PARAMETRI ] = $parametriCheltuiala;

                if( isset( $contributiiList[ $cheltuialaId ] ) )
                {
                    $cheltuialaData[ App_Component_Cheltuiala_Object::CONTRIBUTII ] = $contributiiList[ $cheltuialaId ];
                }

                $cheltuieliData[] = $cheltuialaData;
            }
            
            $this->writeCache( $cacheId, $cheltuieliData );
        }
        
        return $this->readCache( $cacheId );
    }
    
    protected function getCheltuieliCurenteList( $asociatieId, $month, $tipCheltuiala = NULL )
    {
        $tipCheltuiala = (array)$tipCheltuiala;
        sort( $tipCheltuiala );
        $tipCheltuialaKey = implode( '-', $tipCheltuiala );
        
        $cacheAllId = "ccl-$asociatieId-$month";
        $cacheId = $cacheAllId . ( $tipCheltuiala ? "-$tipCheltuialaKey" : "" );
        
        if( $this->notCache( $cacheId ) )
        {
            if( $tipCheltuiala && $this->hasCache( $cacheAllId ) )
            {
                $cheltuieli = [];

                $cheltuieliAll = $this->readCache( $cacheAllId );
                foreach( $cheltuieliAll as $cheltuialaData )
                {
                    if( in_array( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_TIP ], $tipCheltuiala ) )
                    {
                        $cheltuieli[] = $cheltuialaData;
                    }
                }
            }
            else
            {
                $cheltuieli = $this->getList( $asociatieId, $month, $tipCheltuiala );
            }
            
            $this->writeCache( $cacheId, $cheltuieli );
        }
        
        return $this->readCache( $cacheId );
    }

    private function getList( $asociatieId, $month, $tipCheltuiala = NULL )
    {
        $columns = [ 
            App_Component_Cheltuiala_Object::PROP_ID, 
            App_Component_Cheltuiala_Object::PROP_TIP, 
            App_Component_Cheltuiala_Object::PROP_CREATED_AT 
        ];

        $conditions = [
            App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID  => $asociatieId,
            App_Component_Cheltuiala_Object::PROP_MONTH         => $month,
            App_Component_Cheltuiala_Object::PROP_STATUS        => App_Component_Cheltuiala_Object::VAL_STATUS_ACTIVE,
        ];

        if( $tipCheltuiala )
        {
            $tipCheltuiala = (array)$tipCheltuiala;
            $conditions[ App_Component_Cheltuiala_Object::PROP_TIP ] = array_map( 
                    function( $tc )
                    {
                        return App_Component_CheltuialaTip_Factory::getInstance()->getTipId( $tc );
                    },
                    $tipCheltuiala );
        }
        
        $cheltuieli = $this->retrieveData( $columns, $conditions );

        foreach( $cheltuieli as $index => $cheltuialaData )
        {
            $cheltuieli[ $index ][ App_Component_Cheltuiala_Object::PROP_TIP ] = 
                    App_Component_CheltuialaTip_Factory::getInstance()->getTipName( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_TIP ] );
        }
        
        return $cheltuieli;
    }

    public function getTipCheltuiala( $cheltuialaId )
    {
        $columns = [ App_Component_Cheltuiala_Object::PROP_TIP ];
        
        $conditions = [
            App_Component_Cheltuiala_Object::PROP_ID  => $cheltuialaId,
        ];
        
        $results = $this->retrieveData( $columns, $conditions );
        $tipCheltuiala = @$results[ 0 ][ App_Component_Cheltuiala_Object::PROP_TIP ];
        
        return $tipCheltuiala;
    }
    
    protected function getCheltuialaMainData( $cheltuialaId )
    {
        $columns = [ 
            App_Component_Cheltuiala_Object::PROP_ID, 
            App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID, 
            App_Component_Cheltuiala_Object::PROP_MONTH, 
            App_Component_Cheltuiala_Object::PROP_TIP, 
            App_Component_Cheltuiala_Object::PROP_CREATED_AT,
        ];
        
        $conditions = [
            App_Component_Cheltuiala_Object::PROP_ID  => $cheltuialaId,
        ];
        
        $results = $this->retrieveData( $columns, $conditions );
        $mainData = $results ? $results[ 0 ] : [];
        
        return $mainData;
    }
    
    /**
     * 
     * @param array $data [ asociatie_id, month, tip ]
     * @return int
     * @throws UnexpectedValueException
     */
    protected function insert( array $data )
    {
        if( !isset( $data[ App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"asociatie_id\"" );
        }
        
        if( !isset( $data[ App_Component_Cheltuiala_Object::PROP_MONTH ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"month\"" );
        }
        
        if( !isset( $data[ App_Component_Cheltuiala_Object::PROP_TIP ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"tip\"" );
        }
        
        $filteredData = Lib_Tools::filterArray( $data, [
            App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID, 
            App_Component_Cheltuiala_Object::PROP_MONTH, 
            App_Component_Cheltuiala_Object::PROP_TIP, 
        ]);
        
        $filteredData[ App_Component_Cheltuiala_Object::PROP_STATUS ] = App_Component_Cheltuiala_Object::VAL_STATUS_PENDING;
        
        $cheltuialaId = $this->getDb()->insert( $filteredData );
        
        return $cheltuialaId;
    }
    
    /**
     * 
     * @param array $cheltuialaData [ id, parametri ]
     * @return array
     * @throws UnexpectedValueException
     */
    protected function setParametri( array $cheltuialaData )
    {
        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"id\"" );
        }
        
        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PARAMETRI ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"parametri\"" );
        }
        
        $parametriCheltuiala = $cheltuialaData[ App_Component_Cheltuiala_Object::PARAMETRI ];
        
        $parametriCheltuialaFiltered = App_Component_CheltuialaParametru_Factory::getInstance()->setParametriCheltuiala( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ], $parametriCheltuiala );
        
        return $parametriCheltuialaFiltered;
    }
    
    /**
     * 
     * @param array $cheltuialaData [ id ]
     * @return array
     * @throws UnexpectedValueException
     */
    protected function setContributii( array $cheltuialaData )
    {
        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] ) )
        {
            throw new UnexpectedValueException( "Missing parameter \"id\"" );
        }
        
        // calculare contributii
        $contributii = $this->CalculatorContributii->compute( $cheltuialaData );
        
        // salvare contributii
        App_Component_ContributieSpatiu_Factory::getInstance()->setContributii( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ], $contributii );
            
        return $contributii;
    }
    
    /**
     * 
     * @param array $cheltuialaData [ id, tip ]
     * @return array
     */
    protected function setExtraData( array $cheltuialaData )
    {
        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ] ) )
        {
            throw new UnexpectedValueException( 'Missing parameter "id"' );
        }

        if( !isset( $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_TIP ] ) )
        {
            throw new UnexpectedValueException( 'Missing parameter "tip"' );
        }
        
        // salvare parametri
        $parametri = $this->setParametri( $cheltuialaData );
        
        // calculare si salvare contributii (dupa caz)
        $contributii = $this->setContributii( $cheltuialaData );

        $cheltuialaId   = $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_ID ];
        $cheltuialaTip  = $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_TIP ];
        
        // generare set de valori returnate
        $returnData = [
            App_Component_Cheltuiala_Object::PROP_ID        => $cheltuialaId,
            App_Component_Cheltuiala_Object::PROP_TIP       => App_Component_CheltuialaTip_Factory::getInstance()->getTipName( $cheltuialaTip ),
            App_Component_Cheltuiala_Object::PARAMETRI      => $parametri,
        ];
        
        if( $contributii !== NULL )
        {
            $returnData[ App_Component_Cheltuiala_Object::CONTRIBUTII ] = $contributii;
        }
        
        return $returnData;
    }
    
    //==========================================================================
    
//    protected function getObjectType(array $objectProperties)
//    {
//        $coloanaId = @$objectProperties[ App_Component_Cheltuiala_Object::PROP_COLOANA_ID ];
//        if ( null !== $coloanaId )
//        {
//            $coloana = App_Component_Coloana_Factory::getInstance()->getAll( @$objectProperties[ App_Component_Cheltuiala_Object::PROP_MONTH ] )[ $coloanaId ];
//            if ( $coloana )
//            {
//                $coloanaCode = $coloana[ App_Component_Coloana_Object::CODE ];
//
//                return $coloanaCode;
//            }
//        }
//
//        return null;
//    }
//
//    /** (v)
//     *
//     * @param type $asociatieId
//     * @param type $month
//     *
//     * @return int
//     */
//    public function getCheltuieliCount( $asociatieId, $month )
//    {
//        $count = $this->getDb()->getCount([
//            App_Component_Cheltuiala_Db::ARG_MONTH      => $month,
//            App_Component_Cheltuiala_Db::ARG_ASOCIATIE  => $asociatieId,
//        ]);
//
//        return $count;
//    }
//
//    /** (v)
//     *
//     * @param int $asociatieId
//     * @param int $month
//     *
//     * @return array
//     */
//    public function getAll( $asociatieId, $month )
//    {
//        $cacheId = "all-$asociatieId-$month";
//
//        if( !isset( $this->_cache[ $cacheId ] ) )
//        {
//            $data = $this->getDb()->getAll([
//                App_Component_Cheltuiala_Db::ARG_MONTH      => $month,
//                App_Component_Cheltuiala_Db::ARG_ASOCIATIE  => $asociatieId,
//            ]);
//
//            $reduceri = App_Component_ReducereRepartizare_Factory::getInstance()->getAll( $asociatieId );
//            $parametriCheltuiala = App_Component_Parametru_Cheltuiala_Factory::getInstance()->getAllByCheltuiala( $asociatieId );
//
//            $cheltuieliData = [];
//
//            foreach( $data as $rowData )
//            {
//                $cheltuialaId = $rowData[ App_Component_Cheltuiala_Object::PROP_ID ];
//
//                $target = $rowData[ App_Component_Cheltuiala_Object::PROP_TARGET ];
//                $rowData[ App_Component_Cheltuiala_Object::PROP_TARGET ] = strlen($target) ? explode(',', $target) : [];
//
//                // exceptii repartizare
//                $exceptieId = $rowData[ App_Component_Cheltuiala_Object::REDUCERE_REPARTIZARE ];
//                if ( isset( $reduceri[ $exceptieId ] ) )
//                {
//                    $exceptieData = $reduceri[ $exceptieId ];
//                    $exceptieData[ App_Component_ReducereRepartizare_Object::PROP_ID ] = $exceptieId;
//
//                    $rowData[ App_Component_Cheltuiala_Object::REDUCERE_REPARTIZARE ] = $exceptieData;
//                }
//
//                // parametrii
//                if ( isset( $parametriCheltuiala[ $cheltuialaId ] ) )
//                {
//                    $parametriValues = $parametriCheltuiala[ $cheltuialaId ];
//                }
//                else
//                {
//                    $parametriValues = [];
//                }
//
//                $rowData[ App_Component_Cheltuiala_Object::PARAMETRI ] = $parametriValues;
//
//                $rowData = $this->parseCheltuiala( $rowData );
//                
//                $cheltuieliData[ $cheltuialaId ] = $rowData;
//            }
//
//            $this->_cache[ $cacheId ] = $cheltuieliData;
//        }
//
//        return $this->_cache[ $cacheId ];
//    }
//
//    protected function parseCheltuiala( array $cheltuialaData )
//    {
//        $coloaneCheltuieli = App_Component_Coloana_Factory::getInstance()->getForCheltuieli();
//        
//        $coloanaCode = $coloaneCheltuieli[ $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_COLOANA_ID ] ][ App_Component_Coloana_Object::CODE ];
//        switch( $coloanaCode )
//        {
//            case App_Component_Coloana_Factory::COLOANA_BENEFICIAR:
//                
//                $total = 0;
//                
//                foreach( $cheltuialaData[ App_Component_Cheltuiala_Object::PARAMETRI ] as $spatiuId => $valoareSpatiu )
//                {
//                    $total += $valoareSpatiu;
//                    $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_TARGET ][] = $spatiuId;
//                }
//                
//                $cheltuialaData[ App_Component_Cheltuiala_Object::PROP_VALUE ] = $total;
//                
//            break;
//            
//        }
//        
//        return $cheltuialaData;
//    }
//    
//    /** (v)
//     *
//     * @param int $asociatieId
//     * @param int $month
//     *
//     * @return array
//     */
//    public function getAllByColoane( $asociatieId, $month )
//    {
//        $cheltuieli = $this->getAll( $asociatieId, $month );
//
//        $cheltuieliColoane = [];
//
//        foreach( $cheltuieli as $cheltuialaId => $cheltuiala )
//        {
//            $coloanaId = $cheltuiala[ App_Component_Cheltuiala_Object::PROP_COLOANA_ID ];
//
//            $cheltuieliColoane[ $coloanaId ][ $cheltuialaId ] = $cheltuiala;
//        }
//
//        return $cheltuieliColoane;
//    }
//
//    /** (v)
//     *
//     * @param type $asociatieId
//     * @return type
//     */
//    public function getFirstMonth( $asociatieId )
//    {
//        $month = $this->getDb()->getFirstMonth( $asociatieId );
//
//        return $month;
//    }
//
//    public function deleteCheltuiala( $cheltuialaId )
//    {
//        $this->getDb()->deleteCheltuiala( $cheltuialaId );
//    }

    /**************************************************************************/
    
}
