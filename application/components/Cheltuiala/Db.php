<?php

class App_Component_Cheltuiala_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'cheltuiala';

    static protected $instance;
    
    const ARG_ASOCIATIE     = 'a';
    const ARG_MONTH         = 'm';

    /** (v)
     *
     * @param array $criteriaArguments
     * @return string
     */
    protected function getSearchCriteriaQuery( array $criteriaArguments )
    {
        $criteria = [
            $this->whereConditions( App_Component_Cheltuiala_Object::getActiveStatusCriteria() ),
        ];

        if ( isset( $criteriaArguments[ self::ARG_ASOCIATIE ] ) )
        {
            $criteria[] = $this->whereEqual( $criteriaArguments[ self::ARG_ASOCIATIE ], App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID );
        }

        if ( isset( $criteriaArguments[ self::ARG_MONTH ] ) )
        {
            $criteria[] = $this->whereEqual( $criteriaArguments[ self::ARG_MONTH ], App_Component_Cheltuiala_Object::PROP_MONTH );
        }

        $query = $this->whereAnd( $criteria );

        return $query;
    }

    /** (v)
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getCount( array $criteria )
    {
        $sql =
                $this->selectStmt( 'COUNT(*)' ) .

                $this->where( $this->getSearchCriteriaQuery( $criteria ) );

        $results = $this->queryAndFetchColumn($sql);

        return $results;
    }

    /** (o)
     *
     * @param array $criteria
     *
     * @return array
     */
    public function getAll( array $criteria )
    {
        $columns = [
            $this->quoteColumns([
                App_Component_Cheltuiala_Object::PROP_ID,
                App_Component_Cheltuiala_Object::PROP_COLOANA_ID,
                App_Component_Cheltuiala_Object::PROP_DESCRIERE,
                App_Component_Cheltuiala_Object::PROP_VALUE,
                App_Component_Cheltuiala_Object::PROP_TARGET,
                App_Component_Cheltuiala_Object::PROP_TIMESTAMP,
            ]),
            $this->quoteColumns([
                App_Component_ReducereRepartizareCheltuiala_Object::REDUCERE_REPARTIZARE_ID => App_Component_Cheltuiala_Object::REDUCERE_REPARTIZARE
            ], App_Component_ReducereRepartizareCheltuiala_Db::TABLE),
        ];

        $sql =
                $this->selectStmt($columns) .

                $this->leftJoin( App_Component_ReducereRepartizareCheltuiala_Db::TABLE, App_Component_ReducereRepartizareCheltuiala_Object::CHELTUIALA_ID, App_Component_Cheltuiala_Object::PROP_ID) .

                $this->where( $this->getSearchCriteriaQuery( $criteria ) ) .

                $this->orderBy(
                        [ App_Component_Cheltuiala_Object::PROP_TIMESTAMP, 'DESC' ]
                );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

    /** (v)
     *
     * @return type
     */
    public function getFirstMonth( $asociatieId )
    {
        $columns = $this->quoteColumns( App_Component_Cheltuiala_Object::PROP_MONTH );

        $sql =
                $this->selectStmt( $columns ) .

                $this->where( $this->getSearchCriteriaQuery([ self::ARG_ASOCIATIE => $asociatieId ]) ) .

                $this->orderBy(
                        [ App_Component_Cheltuiala_Object::PROP_TIMESTAMP ]
                ) .

                $this->limit(1);

        $monthId = $this->queryAndFetchColumn($sql);

        return $monthId;
    }

    public function deleteCheltuiala( $cheltuialaId )
    {
        $data = [
            App_Component_Cheltuiala_Object::PROP_STATUS => App_Component_Cheltuiala_Object::VAL_STATUS_DELETED,
        ];

        $where = $this->whereEqual( $cheltuialaId, App_Component_Cheltuiala_Object::PROP_ID );

        $this->update($data, $where);
    }

}
