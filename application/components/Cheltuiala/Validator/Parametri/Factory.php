<?php

class App_Component_Cheltuiala_Validator_Parametri_Factory extends Lib_Component_Factory
{
    public function getValidator( $tipCheltuialaId, array $ctorArgs = [] )
    {
        $validatorName = App_Component_CheltuialaTip_Factory::getInstance()->getTipName( $tipCheltuialaId );
        
        $Validator = $this->createObject( $validatorName, [ 
                self::OPT_PATH      => 'TipCheltuiala/',
                self::OPT_CTOR_ARGS => [ $ctorArgs ],
        ]);
        
        return $Validator;
    }
    
}
