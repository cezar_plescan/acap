<?php

abstract class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_FacturaAbstract extends App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Abstract
{
    const SCADENTA_INTERVAL             = '2M';
    
    const ERR_SCADENTA_REQUIRED         = 'scadenta_required';
    const ERR_SCADENTA_INVALID          = 'scadenta_invalid';
    const ERR_SCADENTA_LIMIT            = 'scadenta_limit';
    const ERR_SCADENTA_EMITERE          = 'scadenta_old';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates(
        [
            self::ERR_REPARTIZARE_REQUIRED  => 'Specificaţi apartamentele la care se repartizează factura.',
            self::ERR_REPARTIZARE_INVALID   => 'Specificaţi apartamentele la care se repartizează factura.',
            
            self::ERR_DATA_REQUIRED         => 'Completaţi data emiterii facturii.',
            self::ERR_DATA_INVALID          => 'Data emiterii facturii nu este corectă.',
            self::ERR_DATA_LIMIT            => 'Data emiterii facturii nu poate fi ulterioară zilei curente.',
            self::ERR_DATA_TOO_OLD          => 'Data emiterii facturii este prea îndepărtată.',
            
            self::ERR_SCADENTA_REQUIRED     => 'Completaţi scadenţa facturii.',
            self::ERR_SCADENTA_INVALID      => 'Scadenţa facturii nu este corectă.',
            self::ERR_SCADENTA_EMITERE      => 'Scadenţa facturii nu poate fi anterioară datei emiterii.',
            self::ERR_SCADENTA_LIMIT        => 'Scadenţa facturii este prea îndepărtată.',
        ]);
    }
    
    /**
     * 
     * @return array[ DateTime, DateTime ]
     */
    protected function validateDataScadenta( array $inputValues )
    {
        $dataEmiterii = $this->validateData( $inputValues );
        $scadenta = $this->validateScadenta( $inputValues, $dataEmiterii );
        
        $filteredValue = [ $dataEmiterii, $scadenta ];
        
        return $filteredValue;
    }
    
    /**
     * 
     * @return DateTime
     */
    protected function validateScadenta( array $inputValues, DateTime $dataEmiterii )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_SCADENTA, self::ERR_SCADENTA_REQUIRED );
        $scadenta = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_SCADENTA ];
        
        $this->ifEmpty( $scadenta, self::ERR_SCADENTA_REQUIRED );
        $this->ifNotDate( $scadenta, self::ERR_SCADENTA_INVALID );
        
        $clientTime = $this->getOption( self::OPT_CLIENT_TIME );

        $scadentaTimestamp = $scadenta->getTimestamp();
        $dataTimestamp = $dataEmiterii->getTimestamp();

        $this->ifLessThan( $scadentaTimestamp, $dataTimestamp, self::ERR_SCADENTA_EMITERE );

        $upperLimit = clone $clientTime;
        $upperLimit->add( new DateInterval( $this->getParametruScadentaInterval() ) );
        $upperLimit->setTime( 0, 0, 0 );

        $upperLimitTimestamp = $upperLimit->getTimestamp();

        $this->ifGreaterThan( $scadentaTimestamp, $upperLimitTimestamp, self::ERR_SCADENTA_LIMIT );
        
        return $scadenta;
    }

    private function getParametruScadentaInterval()
    {
        $interval = 'P' . Lib_Application::getInstance()->getConfig( [ 'validators', 'cheltuiala', 'interval_scadenta' ], self::SCADENTA_INTERVAL );
        
        return $interval;
    }
    
}
