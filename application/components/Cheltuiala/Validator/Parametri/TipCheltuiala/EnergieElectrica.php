<?php

class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_EnergieElectrica extends App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_FacturaAbstract
{
    const ERR_CONTOR_REQUIRED               = 'contor_required';
    const ERR_CONTOR_INVALID                = 'contor_invalid';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates(
        [
            self::ERR_CONTOR_REQUIRED           => 'Specificaţi contorul de energie electrică.',
            self::ERR_CONTOR_INVALID            => 'Specificaţi contorul de energie electrică.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        $this->validateInputFormat( $inputValues );
        
        $descriere      = $this->validateDescriere( $inputValues );
        $furnizor       = $this->validateFurnizor( $inputValues );
        $document       = $this->validateDocument( $inputValues );
        $valoare        = $this->validateValoare( $inputValues );
        $dataScadenta   = $this->validateDataScadenta( $inputValues );
        $contorId       = $this->validateContor( $inputValues );
        $repartizare    = $this->validateRepartizareContor( $inputValues, $contorId );
        
        list( $data, $scadenta ) = $dataScadenta;

        $filteredValue = [
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE      => $descriere,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_FURNIZOR       => $furnizor,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DOCUMENT       => $document,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE        => $valoare,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DATA           => $data,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_SCADENTA       => $scadenta,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_CONTOR         => $contorId,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE    => $repartizare,
        ];

        return $filteredValue;
    }

    protected function validateContor( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_CONTOR, self::ERR_CONTOR_REQUIRED );
        $contorId = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_CONTOR ];
        
        $this->ifEmpty( $contorId, self::ERR_CONTOR_REQUIRED );
            
        $contoare = App_Component_Configuratie_Factory::getInstance()->getData( 
                $this->getAsociatieId(),
                App_Component_ConfiguratieCategorie_Factory::CATEGORIE_ENERGIE_ELECTRICA,
                App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::DENUMIRE 
        );

        if( !is_array( $contoare ) ||
            !in_array( $contorId, array_column( $contoare, App_Component_Configuratie_Object::PROP_ID ) ) )
        {
            $this->setError( self::ERR_CONTOR_INVALID );
        }
        
        return $contorId;
    }
    
    protected function validateRepartizareContor( array $inputValues, $contorId )
    {
        // verificare dacă repartizarea selectată aparţine scărilor deservite de acest contor
        $contorData = App_Component_Configuratie_Factory::getInstance()->getConfiguratieValue( $contorId );
        $scariContor = Lib_Tools::checkInputDataKeys( $contorData, App_Component_ConfiguratieParametru_EnergieElectrica_Contor_Parametru::PARAM_SCARI );

        $repartizare = $this->validateRepartizare(
                $inputValues,
                [ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI => $scariContor ]
        );
        
        return $repartizare;
    }
    
}
