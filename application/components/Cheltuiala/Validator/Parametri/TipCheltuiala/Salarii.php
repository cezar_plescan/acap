<?php

class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Salarii extends App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Abstract
{
    const ERR_TAXE_INVALID          = 'taxe_invalid';
    const ERR_TAXE_LIMIT_UP         = 'taxe_limit_up';
    const ERR_TAXE_LIMIT_DOWN       = 'taxe_limit_down';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_TAXE_INVALID          => 'Valoarea taxelor nu este corectă. Folosiţi un număr cu cel mult ' . self::FLOAT_PRECISION . ' zecimale.'
                                                . '<br/>Atenţie: pentru separatorul de zecimale folosiţi virgula.',
            self::ERR_TAXE_LIMIT_UP         => 'Valoarea taxelor nu poate depăşi valoarea cheltuielii.',
            self::ERR_TAXE_LIMIT_DOWN       => 'Valoarea taxelor nu poate fi negativă.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        // validare input value - should be an array
        $this->validateInputFormat( $inputValues );

        $descriere  = $this->validateDescriere( $inputValues );
        $beneficiar = $this->validateBeneficiar( $inputValues );
        $valoare    = $this->validateValoare( $inputValues );
        $taxe       = $this->validateTaxe( $inputValues, $valoare );
        $fond       = $this->validateFond( $inputValues );
        
        $filteredValue = [
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE      => $descriere,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_BENEFICIAR     => $beneficiar,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE        => $valoare,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_TAXE           => $taxe,
        ] + 
            $fond;

        return $filteredValue;
    }

    protected function validateTaxe( array $inputValues, $valoare )
    {
        $taxe = $this->getValueAtKey( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_TAXE );
        
        if( $this->ifEmpty( $taxe) || $this->ifZero( $taxe ) )
        {
            $taxe = NULL;
        }
        else
        {
            $this->ifNotFloat( $taxe, self::ERR_TAXE_INVALID );
            $this->ifGreaterThan( $taxe, $valoare, self::ERR_TAXE_LIMIT_UP );
            $this->ifLessThan( $taxe, 0, self::ERR_TAXE_LIMIT_DOWN );
        }
        
        return $taxe;
    }
    
}
