<?php

class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Ascensoare_Base extends App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_FacturaAbstract
{
    const ERR_ASCENSOARE_EMPTY      = 'ascensoare_empty';
    const ERR_ASCENSOARE_INVALID    = 'ascensoare_invalid';
    
    const ERR_ASCENSOR_INVALID      = 'ascensor_invalid';

    protected $ascensoareIds = [];
    
    protected function init()
    {
        parent::init();
        
        $this->retrieveAscensoareIds();
        
        $oneAscensorMessage = 'Completaţi valoarea facturată.';
        $multipleAscensoareMessage = 'Completaţi valoarea serviciului pentru cel puţin un ascensor.';
        
        $emptyAscensoareMessage = count( $this->ascensoareIds ) >= 2 ? $multipleAscensoareMessage : $oneAscensorMessage;
        
        $this->addMessageTemplates([
            self::ERR_ASCENSOARE_EMPTY          => $emptyAscensoareMessage,
            self::ERR_ASCENSOARE_INVALID        => $emptyAscensoareMessage,
            self::ERR_ASCENSOR_INVALID          => 'Ascensor incorect',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        $this->validateInputFormat( $inputValues );
        
        $descriere      = $this->validateDescriere( $inputValues );
        $furnizor       = $this->validateFurnizor( $inputValues );
        $document       = $this->validateDocument( $inputValues );
        $dataScadenta   = $this->validateDataScadenta( $inputValues );
        $fondId         = $this->validateFondId( $inputValues );
        $valoriAscensoare = $this->validateValoriAscensoare( $inputValues, $fondId );
        
        list( $data, $scadenta ) = $dataScadenta;

        $valoare = $this->computeValoareTotala( $valoriAscensoare );

        $filteredValue = [
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE      => $descriere,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_FURNIZOR       => $furnizor,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DOCUMENT       => $document,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DATA           => $data,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_SCADENTA       => $scadenta,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE        => $valoare,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_FOND           => $fondId,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_ASCENSOARE     => $valoriAscensoare,
        ];

        return $filteredValue;
    }

    protected function validateValoriAscensoare( array $inputValues, $fondId )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_ASCENSOARE, self::ERR_ASCENSOARE_EMPTY );
        
        $filteredValue = [];
        $noValues = TRUE;

        $ascensoare = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_ASCENSOARE ];

        $this->ifNotArray( $ascensoare, self::ERR_ASCENSOARE_EMPTY );

        $repartizareMandatory = App_Component_Fond_Factory::getInstance()->isRepartizareMandatory( $fondId );
        $repartizareOptional = App_Component_Fond_Factory::getInstance()->isRepartizareOptional( $fondId );
        
        foreach( $ascensoare as $ascensorId => $ascensorValues )
        {
            $this->ifNotArray( $ascensorValues, self::ERR_ASCENSOARE_INVALID );
            $this->checkAscensorId( $ascensorId );
            
            if( isset( $ascensorValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE ] ) )
            {
                $valoare = $ascensorValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE ];

                if( !$this->ifEmpty( $valoare ) )
                {
                    $noValues = FALSE;

                    $valoare = $this->validateValoare( $ascensorValues );
                    
                    $filteredValue[ $ascensorId ] = [ App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE => $valoare ];
                    
                    if( $repartizareMandatory ||
                        ( $repartizareOptional && isset( $ascensorValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE ] ) ) )
                    {
                        $repartizareAscensor = $this->validateRepartizareAscensor( $ascensorValues, $ascensorId );
                        
                        $filteredValue[ $ascensorId ][ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE ] = $repartizareAscensor;
                    }
                }
            }
        }

        if( $noValues )
        {
            $this->setError( self::ERR_ASCENSOARE_EMPTY );
        }
        
        return $filteredValue;
    }
    
    protected function validateRepartizareAscensor( array $inputValues, $ascensorId )
    {
        $ascensorData = App_Component_Configuratie_Factory::getInstance()->getData( 
                $this->getAsociatieId(),
                App_Component_ConfiguratieCategorie_Factory::CATEGORIE_ASCENSOARE,
                App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::DENUMIRE,
                $ascensorId );

        $scaraId = Lib_Tools::checkInputDataKeys( $ascensorData, App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::PARAM_SCARA );
        $repartizareScara = [ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI => [ $scaraId ] ];
        
        $repartizare = $this->validateRepartizare( $inputValues, $repartizareScara, FALSE );
        
        return $repartizare;
    }
    
    protected function computeValoareTotala( array $ascensoareValues )
    {
        $valoare = array_sum( array_column( $ascensoareValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE ) );
        
        return $valoare;
    }
    
    private function retrieveAscensoareIds()
    {
        $ascensoare = App_Component_Configuratie_Factory::getInstance()->getData( $this->getAsociatieId(),
                App_Component_ConfiguratieCategorie_Factory::CATEGORIE_ASCENSOARE,
                App_Component_ConfiguratieParametru_Ascensoare_Ascensor_Parametru::DENUMIRE );
        
        if( !is_array( $ascensoare ) )
        {
            $ascensoare = [];
        }
        
        $this->ascensoareIds = array_column( $ascensoare, App_Component_Configuratie_Object::PROP_ID );
    }
    
    private function checkAscensorId( $ascensorId )
    {
        $this->ifNotInArray( $ascensorId, $this->ascensoareIds, self::ERR_ASCENSOR_INVALID );
    }
    
}
