<?php
//... to refactor
class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Salubrizare extends App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_FacturaAbstract
{
    const ERR_REDUCERE_PROCENT_REQUIRED     = 'reducere_procent_required';
    const ERR_REDUCERE_PROCENT_INVALID      = 'reducere_procent_invalid';
    
    const ERR_REDUCERE_PERSOANE_REQUIRED    = 'reducere_persoane_required';
    const ERR_REDUCERE_PERSOANE_INVALID     = 'reducere_persoane_invalid';
    
    const ERR_NR_PERSOANE_NOT_NUMBER        = 'persoane_not_number';
    const ERR_NR_PERSOANE_NEGATIVE          = 'persoane_negative';
    const ERR_NR_PERSOANE_TOO_MANY          = 'persoane_too_many';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates(
        [
            self::ERR_REDUCERE_PROCENT_REQUIRED     => 'Completaţi reducerea procentuală.',
            self::ERR_REDUCERE_PROCENT_INVALID      => 'Reducerea procentuală trebuie să fie un număr întreg între 1 şi 99.',
            
            self::ERR_REDUCERE_PERSOANE_REQUIRED    => 'Specificaţi persoanele care beneficiază de reducere.',
            self::ERR_REDUCERE_PERSOANE_INVALID     => 'Specificaţi persoanele care beneficiază de reducere.',
            
            self::ERR_NR_PERSOANE_NOT_NUMBER        => 'Aţi completat o valoare pentru numărul de persoane care nu reprezintă un număr întreg.',
            self::ERR_NR_PERSOANE_NEGATIVE          => 'Aţi completat o valoare negativă pentru numărul de persoane.',
            self::ERR_NR_PERSOANE_TOO_MANY          => 'Aţi completat o valoare care depăşeşte numărul de persoane din spaţiul respectiv.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        $this->validateInputFormat( $inputValues );
        
        $descriere      = $this->validateDescriere( $inputValues );
        $furnizor       = $this->validateFurnizor( $inputValues );
        $document       = $this->validateDocument( $inputValues );
        $valoare        = $this->validateValoare( $inputValues );
        $dataScadenta   = $this->validateDataScadenta( $inputValues );
        $repartizare    = $this->validateRepartizare( $inputValues );
        $reducere       = $this->validateReducere( $inputValues, $repartizare );
        
        list( $data, $scadenta ) = $dataScadenta;

        $filteredValue = [
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE      => $descriere,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_FURNIZOR       => $furnizor,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DOCUMENT       => $document,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE        => $valoare,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DATA           => $data,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_SCADENTA       => $scadenta,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE       => $reducere,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE    => $repartizare,
        ];

        return $filteredValue;
    }

    protected function validateReducere( array $inputValues, $repartizare )
    {
        $reducere = $this->getValueAtKey( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE );
        
        if( is_array( $reducere ) )
        {
            $procent    = $this->validateReducereProcent( $reducere );
            $spatii     = $this->validateReducereSpatii( $reducere, $repartizare );
            
            $reducere = [
                App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_SPATII    => $spatii,
                App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_PROCENT   => $procent,
            ];
        }
        else
        {
            $reducere = NULL;
        }
        
        return $reducere;
    }
    
    private function validateReducereProcent( array $reducere )
    {
        $this->ifKeyNotSet( $reducere, App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_PROCENT, self::ERR_REDUCERE_PROCENT_REQUIRED );
        
        $procent = $reducere[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_PROCENT ];
        
        $this->ifEmpty( $procent, self::ERR_REDUCERE_PROCENT_REQUIRED );
        $this->ifNotInteger( $procent, self::ERR_REDUCERE_PROCENT_INVALID );
        $this->ifLessThan( $procent, 1, self::ERR_REDUCERE_PROCENT_INVALID );
        $this->ifGreaterThan( $procent, 99, self::ERR_REDUCERE_PROCENT_INVALID );
        
        return $procent;
    }
    
    private function validateReducereSpatii( array $reducere, $repartizare )
    {
        $this->ifKeyNotSet( $reducere, App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_SPATII, self::ERR_REDUCERE_PERSOANE_REQUIRED );
        
        $spatii = $reducere[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REDUCERE_SPATII ];
        
        $this->ifEmpty( $spatii, self::ERR_REDUCERE_PERSOANE_REQUIRED );
        $this->ifNotArray( $spatii, self::ERR_REDUCERE_PERSOANE_REQUIRED );
        
        $availableSpatii = App_Component_Spatiu_Factory::getInstance()->getIDListByRepartizare( $this->getAsociatieId(), $repartizare );
        $persoaneSpatii = App_Component_SpatiuParametru_Factory::getInstance()->getParametruValueList( App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE, array_keys( $spatii ) );
            
        foreach( $spatii as $spatiuId => $persoaneReducere )
        {
            // verificare apartenenta spatiu la domeniul de repartizare
            $this->ifNotInArray( $spatiuId, $availableSpatii, self::ERR_REDUCERE_PERSOANE_INVALID );

            // verificare existenta numar de persoane pentru spatiul curent
            $this->ifKeyNotSet( $persoaneSpatii, $spatiuId, self::ERR_REDUCERE_PERSOANE_INVALID );

            // verificare numar de persoane pentru care se aplica reducerea
            $this->ifNotInteger( $persoaneReducere, self::ERR_NR_PERSOANE_NOT_NUMBER );
            $this->ifLessThan( $persoaneReducere, 0, self::ERR_NR_PERSOANE_NEGATIVE );

            $persoaneSpatiu = $persoaneSpatii[ $spatiuId ];
            $this->ifGreaterThan( $persoaneReducere, $persoaneSpatiu, self::ERR_NR_PERSOANE_TOO_MANY );
        }
        
        return $spatii;
    }
    
}
