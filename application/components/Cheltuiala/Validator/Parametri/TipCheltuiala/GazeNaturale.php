<?php

class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_GazeNaturale extends App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_FacturaAbstract
{
    const ERR_BRANSAMENT_ID_REQUIRED    = 'bransament_required';
    const ERR_BRANSAMENT_ID_INVALID     = 'bransament_invalid';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates(
        [
            self::ERR_BRANSAMENT_ID_REQUIRED    => 'Specificaţi branşamentul.',
            self::ERR_BRANSAMENT_ID_INVALID     => 'Specificaţi branşamentul.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        $this->validateInputFormat( $inputValues );
        
        $descriere      = $this->validateDescriere( $inputValues );
        $furnizor       = $this->validateFurnizor( $inputValues );
        $document       = $this->validateDocument( $inputValues );
        $valoare        = $this->validateValoare( $inputValues );
        $dataScadenta   = $this->validateDataScadenta( $inputValues );
        $bransamentId   = $this->validateBransamentId( $inputValues );
        $repartizare    = $this->validateBransamentRepartizare( $inputValues, $bransamentId );
        
        list( $data, $scadenta ) = $dataScadenta;

        $filteredValue = [
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE      => $descriere,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_FURNIZOR       => $furnizor,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DOCUMENT       => $document,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE        => $valoare,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DATA           => $data,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_SCADENTA       => $scadenta,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_BRANSAMENT     => $bransamentId,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE    => $repartizare,
        ];

        return $filteredValue;
    }

    protected function validateBransamentId( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_BRANSAMENT, self::ERR_BRANSAMENT_ID_REQUIRED );
        
        $bransamentId = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_BRANSAMENT ];

        $this->ifEmpty( $bransamentId, self::ERR_BRANSAMENT_ID_REQUIRED );
        
        $bransamente = App_Component_Configuratie_Factory::getInstance()->getData( 
                $this->getAsociatieId(),
                App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::CATEGORIE,
                App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::DENUMIRE );

        if( !is_array( $bransamente ) ||
            !in_array( $bransamentId, array_column( $bransamente, App_Component_Configuratie_Object::PROP_ID ) ) )
        {
            $this->setError( self::ERR_BRANSAMENT_ID_INVALID );
        }
        
        return $bransamentId;
    }

    protected function validateBransamentRepartizare( array $inputValues, $bransamentId )
    {
        $bransamentData = App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::getData( $this->getAsociatieId(), $bransamentId );

        $bransamentScaraId = Lib_Tools::checkInputDataKeys( $bransamentData, App_Component_ConfiguratieParametru_GazeNaturale_Bransament_Parametru::PARAM_SCARA );
        $repartizareBransament = [ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI => [ $bransamentScaraId ] ];
        
        $repartizare = $this->validateRepartizare( $inputValues, $repartizareBransament, FALSE );
        
        return $repartizare;
    }
    
}
