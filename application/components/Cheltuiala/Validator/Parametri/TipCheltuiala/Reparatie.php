<?php

class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Reparatie extends App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Abstract
{
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            
        ]);
    }
    
    protected function validate( $inputValues )
    {
        // validare input value - should be an array
        $this->validateInputFormat( $inputValues );

        $descriere  = $this->validateDescriere( $inputValues );
        $furnizor   = $this->validateFurnizor( $inputValues );
        $document   = $this->validateDocument( $inputValues );
        $valoare    = $this->validateValoare( $inputValues );
        $data       = $this->validateData( $inputValues );
        $fond       = $this->validateFond( $inputValues );
        
        $filteredValue = [
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE     => $descriere,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_FURNIZOR      => $furnizor,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DOCUMENT      => $document,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE       => $valoare,
            App_Component_CheltuialaParametru_Factory::PARAMETRU_DATA          => $data,
        ] + 
            $fond;

        return $filteredValue;
    }

    protected function validateFondId( array $inputValues )
    {
        $fondId = parent::validateFondId( $inputValues );
        
        $isIntretinere = App_Component_Fond_Factory::getInstance()->isTipIntretinere( $fondId );
        
        $this->ifTrue( $isIntretinere, self::ERR_FOND_INVALID );
        
        return $fondId;
    }
}
