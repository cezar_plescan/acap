<?php

abstract class App_Component_Cheltuiala_Validator_Parametri_TipCheltuiala_Abstract extends App_Component_Asociatie_Validator_BaseAbstract
{
    const DESCRIERE_MAX_LENGTH      = 100;
    const FURNIZOR_MAX_LENGTH       = 60;
    const BENEFICIAR_MAX_LENGTH     = self::FURNIZOR_MAX_LENGTH;
    const DOCUMENT_MAX_LENGTH       = 20;
    const VALOARE_MAX_LIMIT         = 100000;
    const VALOARE_MIN_LIMIT         = -100000;
    const DATA_INTERVAL             = '1Y';
    
    const ERR_FOND_REQUIRED         = 'fond_required';
    const ERR_FOND_INVALID          = 'fond_invalid';
    
    const ERR_DESCRIERE_REQUIRED    = 'descriere_required';
    const ERR_DESCRIERE_TOO_LONG    = 'descriere_too_long';
    
    const ERR_FURNIZOR_REQUIRED     = 'furnizor_required';
    const ERR_FURNIZOR_TOO_LONG     = 'furnizor_too_long';
    
    const ERR_BENEFICIAR_REQUIRED   = 'beneficiar_required';
    const ERR_BENEFICIAR_TOO_LONG   = 'beneficiar_too_long';
    
    const ERR_DOCUMENT_REQUIRED     = 'document_required';
    const ERR_DOCUMENT_TOO_LONG     = 'document_too_long';
    
    const ERR_VALOARE_REQUIRED      = 'valoare_required';
    const ERR_VALOARE_NULL          = 'valoare_null';
    const ERR_VALOARE_INVALID       = 'valoare_invalid';
    const ERR_VALOARE_LIMIT_UP      = 'valoare_limit_up';
    const ERR_VALOARE_LIMIT_DOWN    = 'valoare_limit_down';
    
    const ERR_DATA_REQUIRED         = 'data_required';
    const ERR_DATA_INVALID          = 'data_invalid';
    const ERR_DATA_LIMIT            = 'data_limit';
    const ERR_DATA_TOO_OLD          = 'data_old';
    
    const ERR_REPARTIZARE_REQUIRED  = 'repartizare_required';
    const ERR_REPARTIZARE_INVALID   = 'repartizare_invalid';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates(
        [
            self::ERR_DESCRIERE_REQUIRED    => 'Completaţi descrierea cheltuielii',
            self::ERR_DESCRIERE_TOO_LONG    => 'Descrierea cheltuielii nu poate depăşi ' . self::DESCRIERE_MAX_LENGTH . ' de caractere.',
            
            self::ERR_FURNIZOR_REQUIRED     => 'Completaţi denumirea furnizorului.',
            self::ERR_FURNIZOR_TOO_LONG     => 'Denumirea furnizorului nu poate depăşi ' . self::FURNIZOR_MAX_LENGTH . ' de caractere.',
            
            self::ERR_BENEFICIAR_REQUIRED   => 'Completaţi numele beneficiarului.',
            self::ERR_BENEFICIAR_TOO_LONG   => 'Numele beneficiarului nu poate depăşi ' . self::BENEFICIAR_MAX_LENGTH . ' de caractere.',
            
            self::ERR_DOCUMENT_REQUIRED     => 'Completaţi documentul justificativ.',
            self::ERR_DOCUMENT_TOO_LONG     => 'Documentul justificativ nu poate depăşi ' . self::DOCUMENT_MAX_LENGTH . ' de caractere.',
            
            self::ERR_VALOARE_REQUIRED      => 'Completaţi valoarea cheltuielii.',
            self::ERR_VALOARE_NULL          => 'Valoarea cheltuielii nu poate fi nulă.',
            self::ERR_VALOARE_INVALID       => 'Valoarea cheltuielii nu este corectă. Folosiţi un număr cu cel mult ' . self::FLOAT_PRECISION . ' zecimale.'
                                                . '<br/>Atenţie: pentru separatorul de zecimale folosiţi virgula.',
            self::ERR_VALOARE_LIMIT_UP      => 'Valoarea cheltuielii nu poate depăşi ' . Lib_Tools::formatNumber( self::VALOARE_MAX_LIMIT, 0 ) . ' lei.',
            self::ERR_VALOARE_LIMIT_DOWN    => 'Valoarea cheltuielii nu poate depăşi ' . Lib_Tools::formatNumber( self::VALOARE_MIN_LIMIT, 0 ) . ' lei.',
            
            self::ERR_DATA_REQUIRED         => 'Completaţi data cheltuielii.',
            self::ERR_DATA_INVALID          => 'Data cheltuielii nu este corectă.',
            self::ERR_DATA_LIMIT            => 'Data cheltuielii nu poate fi ulterioară zilei curente.',
            self::ERR_DATA_TOO_OLD          => 'Data cheltuielii este prea îndepărtată.',
            
            self::ERR_FOND_REQUIRED         => 'Specificaţi fondul din care se achită cheltuiala.',
            self::ERR_FOND_INVALID          => 'Specificaţi fondul din care se achită cheltuiala.',
            
            self::ERR_REPARTIZARE_INVALID   => 'Specificaţi apartamentele la care se repartizează cheltuiala.',
            self::ERR_REPARTIZARE_REQUIRED  => 'Specificaţi apartamentele la care se repartizează cheltuiala.',
            
        ]);
    }
    
    protected function validateDescriere( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE, self::ERR_DESCRIERE_REQUIRED );
        $descriere = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_DESCRIERE ];

        $this->ifLongerThan( $descriere, self::DESCRIERE_MAX_LENGTH, self::ERR_DESCRIERE_TOO_LONG );
        
        return $descriere;
    }
    
    protected function validateFurnizor( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_FURNIZOR, self::ERR_FURNIZOR_REQUIRED );
        $furnizor = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_FURNIZOR ];

        $this->ifLongerThan( $furnizor, self::FURNIZOR_MAX_LENGTH, self::ERR_FURNIZOR_TOO_LONG );
        
        return $furnizor;
    }
    
    protected function validateBeneficiar( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_BENEFICIAR, self::ERR_BENEFICIAR_REQUIRED );
        $furnizor = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_BENEFICIAR ];

        $this->ifLongerThan( $furnizor, self::BENEFICIAR_MAX_LENGTH, self::ERR_BENEFICIAR_TOO_LONG );
        
        return $furnizor;
    }
    
    protected function validateDocument( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_DOCUMENT, self::ERR_DOCUMENT_REQUIRED );
        $document = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_DOCUMENT ];

        $this->ifLongerThan( $document, self::DOCUMENT_MAX_LENGTH, self::ERR_DOCUMENT_TOO_LONG );
        
        return $document;
    }
    
    protected function validateValoare( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE, self::ERR_VALOARE_REQUIRED );
        $valoare = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_VALOARE ];
            
        $this->ifEmpty( $valoare, self::ERR_VALOARE_REQUIRED );
        $this->ifNotFloat( $valoare, self::ERR_VALOARE_INVALID );
        $this->ifZero( $valoare, self::ERR_VALOARE_NULL );
        $this->ifGreaterThan( $valoare, self::VALOARE_MAX_LIMIT, self::ERR_VALOARE_LIMIT_UP );
        $this->ifLessThan( $valoare, self::VALOARE_MIN_LIMIT, self::ERR_VALOARE_LIMIT_DOWN );
        
        return $valoare;
    }
    
    /**
     * - sa existe
     * - sa fie valida
     * - sa nu depaseasca data curenta (data curenta a clientului)
     * - sa nu fie mai veche de un an
     */
    protected function validateData( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_DATA, self::ERR_DATA_REQUIRED );
        $data = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_DATA ];
        
        $this->ifEmpty( $data, self::ERR_DATA_REQUIRED );
        $this->ifNotDate( $data, self::ERR_DATA_INVALID );
        
        $clientTime = $this->getOption( self::OPT_CLIENT_TIME );

        $dataTimestamp = $data->getTimestamp();
        $clientTimestamp = $clientTime->getTimestamp();

        $this->ifGreaterThan( $dataTimestamp, $clientTimestamp, self::ERR_DATA_LIMIT );
        
        $lowerLimit = clone $clientTime;
        $lowerLimit->sub( new DateInterval( $this->getParametruDataInterval() ) );
        $lowerLimit->setTime( 0, 0, 0 );

        $lowerLimitTimestamp = $lowerLimit->getTimestamp();

        $this->ifLessThan( $dataTimestamp, $lowerLimitTimestamp, self::ERR_DATA_TOO_OLD );
        
        return $data;
    }
    
    protected function validateRepartizare( array $inputValues, $relativeTo = '*', $repartizareAsObject = TRUE )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE, self::ERR_REPARTIZARE_REQUIRED );
        $repartizare = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE ];
        
        if( is_array( $repartizare ) )
        {
            $repartizare = $this->validateRepartizareArray( $repartizare, $repartizareAsObject );
            
            $spatiiRelativeTo = App_Component_Spatiu_Factory::getInstance()->getIDListByRepartizare( $this->getAsociatieId(), $relativeTo );
            $spatiiRepartizare = App_Component_Spatiu_Factory::getInstance()->getIDListByRepartizare( $this->getAsociatieId(), $repartizare );

            $this->ifNotIncludedIn( $spatiiRepartizare, $spatiiRelativeTo, self::ERR_REPARTIZARE_INVALID );
        }
        else
        {
            $repartizare = trim( $repartizare );

            $this->ifConditionThenError( $repartizare !== '*', self::ERR_REPARTIZARE_INVALID );
            
            if( $relativeTo !== NULL )
            {
                $repartizare = $relativeTo;
            }
        }

        return $repartizare;
    }
    
    protected function validateRepartizareArray( array $repartizare, $repartizareAsObject = TRUE )
    {
        if( !$repartizareAsObject )
        {
            // $repartizare is an array of spatiu_id
            
            $repartizare = [
                App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SPATII => $repartizare
            ];
        }
        
        $blocuri    = $this->validateRepartizareBlocuri( $repartizare );
        $scari      = $this->validateRepartizareScari( $repartizare );
        $spatii     = $this->validateRepartizareSpatii( $repartizare );

        $repartizareFiltered = [];

        if( $blocuri )
        {
            $repartizareFiltered[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_BLOCURI ] = $blocuri;
        }

        if( $scari ) 
        {
            $repartizareFiltered[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI ] = $scari;
        }

        if( $spatii ) 
        {
            $repartizareFiltered[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SPATII ] = $spatii;
        }
        
        $this->ifEmpty( $repartizareFiltered, self::ERR_REPARTIZARE_REQUIRED );

        return $repartizareFiltered;
    }
    
    protected function validateRepartizareBlocuri( array $repartizare )
    {
        $blocuri = NULL;
        
        if( isset( $repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_BLOCURI ] ) )
        {
            $blocuri = $repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_BLOCURI ];
            
            $this->ifNotArray( $blocuri, self::ERR_REPARTIZARE_INVALID );
            
            $allBlocuri = App_Component_Bloc_Factory::getInstance()->getIDList( $this->getAsociatieId() );
            $this->ifNotIncludedIn( $blocuri, $allBlocuri, self::ERR_REPARTIZARE_INVALID );
        }
        
        return $blocuri;
    }
    
    protected function validateRepartizareScari( array $repartizare )
    {   
        $scari = NULL;
        
        if( isset( $repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI ] ) )
        {
            $scari = $repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI ];
            
            $this->ifNotArray( $scari, self::ERR_REPARTIZARE_INVALID );
            
            $allScari = App_Component_Scara_Factory::getInstance()->getIDList( $this->getAsociatieId() );
            $this->ifNotIncludedIn( $scari, $allScari, self::ERR_REPARTIZARE_INVALID );
        }
        
        return $scari;
    }
        
    protected function validateRepartizareSpatii( array $repartizare )
    {
        $spatii = NULL;
        
        if( isset( $repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SPATII ] ) )
        {
            $spatii = $repartizare[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SPATII ];
            
            $this->ifNotArray( $spatii, self::ERR_REPARTIZARE_INVALID );
            
            $allSpatii = App_Component_Spatiu_Factory::getInstance()->getIDList( $this->getAsociatieId() );
            $this->ifNotIncludedIn( $spatii, $allSpatii, self::ERR_REPARTIZARE_INVALID );
        }
        
        return $spatii;
    }
    
    protected function validateRepartizareSpatiiScara( $spatiiScara, $scaraId )
    {
        $filteredValue = FALSE;
        $repartizareScara = [ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SCARI => [ $scaraId ] ];

        if( $this->ifEmpty( $spatiiScara ) || $this->ifNotArray( $spatiiScara ) )
        {
            $filteredValue = $repartizareScara;
        }
        else
        {
            // keep valid spatiu_ids
            $allSpatiiScara = App_Component_Spatiu_Factory::getInstance()->getIDListByRepartizare( $this->getAsociatieId(), $repartizareScara );

            $spatiiScara = array_map( 
                    function( $id ){
                        return (int)$id;
                    }, 
                    $spatiiScara 
            );

            $validSpatii = array_unique( array_intersect( $spatiiScara, $allSpatiiScara ) );
            
            if( $this->ifEmpty( $validSpatii ) )
            {
                $filteredValue = $repartizareScara;
            }
            else
            {
                $filteredValue = [ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE_SPATII => $validSpatii ];
            }
        }
        
        return $filteredValue;
    }
    
    protected function validateFond( array $inputValues, $relativeTo = '*', $repartizareAsObject = TRUE )
    {
        $fond = $this->validateFondId( $inputValues );
        
        $filteredValue = [
            App_Component_CheltuialaParametru_Factory::PARAMETRU_FOND => $fond,
        ];
        
        if( App_Component_Fond_Factory::getInstance()->isRepartizareMandatory( $fond ) ||
            ( App_Component_Fond_Factory::getInstance()->isRepartizareOptional( $fond ) 
              && isset( $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE ] ) 
            ) )
        {
            $repartizare = $this->validateRepartizare( $inputValues, $relativeTo, $repartizareAsObject );
            
            $filteredValue[ App_Component_CheltuialaParametru_Factory::PARAMETRU_REPARTIZARE ] = $repartizare;
        }
        
        return $filteredValue;
    }
    
    protected function validateFondId( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Component_CheltuialaParametru_Factory::PARAMETRU_FOND, self::ERR_FOND_REQUIRED );
        
        $fond = $inputValues[ App_Component_CheltuialaParametru_Factory::PARAMETRU_FOND ];
        $fonduri = App_Component_Fond_Factory::getInstance()->getFonduriIds( $this->getAsociatieId() );

        $this->ifNotInArray( $fond, $fonduri, self::ERR_FOND_INVALID );
        
        return $fond;
    }
    
    private function getParametruDataInterval()
    {
        $interval = 'P' . Lib_Application::getInstance()->getConfig( [ 'validators', 'cheltuiala', 'interval_data' ], self::DATA_INTERVAL );
        
        return $interval;
    }
    
}
