<?php

abstract class App_Component_Cheltuiala_Validator_ParametriAbstract extends App_Component_Asociatie_Validator_WorkingMonth
{
    const ERR_PARAMETRI_MISSING     = 'parametri_missing';
    const ERR_CHELTUIALA_ID_MISSING = 'cheltuiala_id_missing';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_PARAMETRI_MISSING         => 'Specificaţi parametrii cheltuielii.',
            self::ERR_CHELTUIALA_ID_MISSING     => 'Specificaţi ID-ul cheltuielii.',
        ]);
    }
    
    protected function validateParametriCheltuiala( array $inputValues, $tipCheltuialaId )
    {
        if( !isset( $inputValues[ App_Component_Cheltuiala_Object::PARAMETRI ] ) )
        {
            $this->setError( self::ERR_PARAMETRI_MISSING );
        }
        else
        {
            $parametri = $inputValues[ App_Component_Cheltuiala_Object::PARAMETRI ];
            
            $this->validateInputFormat( $parametri );

            $parametriValidator = $this->getParametriValidator( $tipCheltuialaId );
            $parametriFiltered = $this->attachValidator( $parametriValidator, $parametri );

            return $parametriFiltered;
        }
    }
    
    protected function getParametriValidator( $tipCheltuialaId )
    {
        $Validator = App_Component_Cheltuiala_Validator_Parametri_Factory::getInstance()->getValidator( $tipCheltuialaId, [
                App_Component_Asociatie_Validator_BaseAbstract::ARG_OPTIONS => [
                        App_Component_Asociatie_Validator_BaseAbstract::OPT_ASOCIATIE_ID    => $this->getAsociatieId(),
                        App_Component_Asociatie_Validator_BaseAbstract::OPT_CLIENT_TIME     => $this->getOption( self::OPT_CLIENT_TIME ),
                ]
        ]);
        
        return $Validator;
    }
    
}
