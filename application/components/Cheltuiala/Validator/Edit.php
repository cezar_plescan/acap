<?php

class App_Component_Cheltuiala_Validator_Edit extends App_Component_Cheltuiala_Validator_ParametriAbstract
{
    protected function validate( $inputValues )
    {
        // validare input value - should be an array
        $this->validateInputFormat( $inputValues );
        
        $idCheltuiala = $this->validateIdCheltuiala( $inputValues );
        
        $tipCheltuiala = App_Component_Cheltuiala_Factory::getInstance()->getTipCheltuiala( $idCheltuiala );
        $parametriCheltuiala = $this->validateParametriCheltuiala( $inputValues, $tipCheltuiala );
        
        $filteredValue = [
            App_Component_Cheltuiala_Object::PROP_ID    => $idCheltuiala,
            App_Component_Cheltuiala_Object::PARAMETRI  => $parametriCheltuiala,
        ];
        
        return $filteredValue;
    }

    
    protected function validateIdCheltuiala( array $inputValues )
    {
        if( !isset( $inputValues[ App_Component_Cheltuiala_Object::PROP_ID ] ) )
        {
            $this->setError( self::ERR_PARAMETRI_MISSING );
        }
        else
        {
            $idCheltuiala = $inputValues[ App_Component_Cheltuiala_Object::PROP_ID ];
            
            $Validator = new App_Component_Cheltuiala_Validator_Id([
                    App_Component_Cheltuiala_Validator_Id::ARG_OPTIONS => [
                            App_Component_Cheltuiala_Validator_Id::OPT_ASOCIATIE_ID => $this->getAsociatieId(),
                            App_Component_Cheltuiala_Validator_Id::OPT_MONTH        => $this->getMonth(),
                    ]
            ]);

            $idCheltuialaFiltered = $this->attachValidator( $Validator, $idCheltuiala );

            return $idCheltuialaFiltered;
        }
    }
    
}
