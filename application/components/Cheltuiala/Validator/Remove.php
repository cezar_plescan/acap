<?php

class App_Component_Cheltuiala_Validator_Remove extends App_Component_Asociatie_Validator_WorkingMonth
{
    protected function validate( $value )
    {
        // validare input value - should be an array
        $this->validateInputFormat( $value );

        $idCheltuiala = $this->validateIdCheltuiala( $value[ App_Component_Cheltuiala_Object::PROP_ID ] );
        
        $filteredValue = [
            App_Component_Cheltuiala_Object::PROP_ID    => $idCheltuiala,
        ];
        
        return $filteredValue;
    }

    protected function validateIdCheltuiala( $idCheltuiala )
    {
        $Validator = new App_Component_Cheltuiala_Validator_Id([
                App_Component_Cheltuiala_Validator_Id::ARG_OPTIONS => [
                        App_Component_Cheltuiala_Validator_Id::OPT_ASOCIATIE_ID => $this->getAsociatieId(),
                        App_Component_Cheltuiala_Validator_Id::OPT_MONTH        => $this->getMonth(),
                ]
        ]);
        
        $idCheltuialaFiltered = $this->attachValidator( $Validator, $idCheltuiala );
        
        return $idCheltuialaFiltered;
    }
    
}
