<?php

class App_Component_Cheltuiala_Validator_Add extends App_Component_Cheltuiala_Validator_ParametriAbstract
{
    const MAX_ENTRIES           = 100;
    const MAX_OPERATIONS        = 200;
    
    const ERR_MAX_ENTRIES       = 'max_entries';
    const ERR_MAX_OPERATIONS    = 'max_operations';
    const ERR_TIP_INVALID       = 'tip_invalid';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_MAX_ENTRIES       => 'Ați atins numărul maxim de cheltuieli adăugate într-o singură lună.',
            self::ERR_MAX_OPERATIONS    => 'Ați atins numărul maxim de operaţii efectuate într-o singură lună.',
            self::ERR_TIP_INVALID       => 'Specificaţi tipul cheltuielii.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        // validare input value - should be an array
        $this->validateInputFormat( $inputValues );

        // verificare numar de cheltuieli si de operatii
        $this->checkMaxEntries();
        $this->checkMaxOperations();
        
        $tipCheltuialaId        = $this->validateTipCheltuiala( $inputValues );
        $parametriCheltuiala    = $this->validateParametriCheltuiala( $inputValues, $tipCheltuialaId );
        
        $filteredValue = [
            App_Component_Cheltuiala_Object::PROP_TIP           => $tipCheltuialaId,
            App_Component_Cheltuiala_Object::PARAMETRI          => $parametriCheltuiala,
            App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID  => $this->getAsociatieId(),
            App_Component_Cheltuiala_Object::PROP_MONTH         => $this->getMonth(),
        ];
        
        return $filteredValue;
    }

    protected function checkMaxEntries()
    {
        $criteria = [
            App_Component_Cheltuiala_Object::PROP_MONTH => $this->getMonth(),
        ];
        
        $entriesCount = App_Component_Cheltuiala_Factory::getInstance()->countActive( $criteria );
        
        if( $entriesCount >= self::MAX_ENTRIES )
        {
            $this->setError( self::ERR_MAX_ENTRIES );
        }
    }

    protected function checkMaxOperations()
    {
        $criteria = [
            App_Component_Cheltuiala_Object::PROP_MONTH => $this->getMonth(),
        ];
        
        $operationsCount = App_Component_Cheltuiala_Factory::getInstance()->count( $criteria );
        
        if( $operationsCount >= self::MAX_OPERATIONS )
        {
            $this->setError( self::ERR_MAX_OPERATIONS );
        }
    }
    
    protected function validateTipCheltuiala( array $inputValues )
    {
        $tipCheltuialaFiltered = NULL;
        
        if( !isset( $inputValues[ App_Component_Cheltuiala_Object::PROP_TIP ] ) )
        {
            $this->setError( self::ERR_TIP_INVALID );
        }
        else
        {
            $tipCheltuiala = $inputValues[ App_Component_Cheltuiala_Object::PROP_TIP ];
            
            $tipuri = App_Component_CheltuialaTip_Factory::getInstance()->getTipuri();

            if( !$this->ifEmpty( $tipCheltuiala, self::ERR_TIP_INVALID ) )
            {
                $found = false;
                $tipCheltuiala = strtolower( $tipCheltuiala );

                foreach( $tipuri as $tipData )
                {
                    if( $tipData[ App_Component_CheltuialaTip_Object::PROP_TIP ] === $tipCheltuiala )
                    {
                        $found = true;
                        $tipCheltuialaFiltered = $tipData[ App_Component_CheltuialaTip_Object::PROP_ID ];

                        break;
                    }
                }

                if( !$found )
                {
                    $this->setError( self::ERR_TIP_INVALID );
                }

            }
        }
        
        return $tipCheltuialaFiltered;
    }
    
}
