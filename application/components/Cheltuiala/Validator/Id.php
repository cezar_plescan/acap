<?php

class App_Component_Cheltuiala_Validator_Id extends App_Component_Asociatie_Validator_WorkingMonth
{
    const ERR_ID_INVALID       = 'id_invalid';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_ID_INVALID       => 'Operaţia nu este permisă.',
        ]);
    }
    
    protected function validate( $idCheltuiala )
    {
        $idCheltuiala = (int)$idCheltuiala;
        
        if( !$this->ifEmpty( $idCheltuiala, self::ERR_ID_INVALID ) )
        {
            $cheltuialaExists = App_Component_Cheltuiala_Factory::getInstance()->count([
                App_Component_Cheltuiala_Object::PROP_ID            => $idCheltuiala,
                App_Component_Cheltuiala_Object::PROP_ASOCIATIE_ID  => $this->getAsociatieId(),
                App_Component_Cheltuiala_Object::PROP_MONTH         => $this->getMonth(),
                App_Component_Cheltuiala_Object::PROP_STATUS        => App_Component_Cheltuiala_Object::VAL_STATUS_ACTIVE,
            ]);
            
            if( !$cheltuialaExists )
            {
                $this->setError( self::ERR_ID_INVALID );
            }
            
        }
        
        return $idCheltuiala;
    }

}
