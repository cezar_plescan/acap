<?php

/**
 * @incomplete
 *
 * Lista de Plata se calculeaza si se afiseaaza pentru fiecare scara.
 * Astfel, obiectul Lista va fi constituit din obiecte ScaraLista
 * care contin datele specifice fiecarei liste asociate unei scari.
 */
class App_Component_Lista_Object extends Lib_Component_Object implements IteratorAggregate
{
    const SUBCOLOANE    = 'subcoloane';
    const VALUE         = 'value';
    const DISPLAY_VALUE = 'display_value';

    protected
            /**
             * Container pentru obiectele ScaraLista
             */
            $ListaContainer = [],

            $month = null,
            $asociatieId = null,

            $blocuri = [],
            $scari = [],
            $spatii = [],
            $cheltuieliColoane = [],
            $coloane = [],
            $parametriAsociatie = [],

            $spatiiValues = [],

            $cheltuieliScari = [],

            $error;

    /**
     *
     * @param int $month
     */
    public function __construct( $asociatieId, $month )
    {
        $this->asociatieId = $asociatieId;
        $this->month = $month;

    }

    protected function loadData()
    {
        $this->_loadBlocuri();
        $this->_loadScari();
        $this->_loadSpatii();
        $this->_loadCheltuieliColoane();
        $this->_loadColoane();
        $this->_loadParametriAsociatie();

    }

    /**
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->ListaContainer);
    }

    /**
     * Returneaza luna de lucru
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Returneaza ID-ul asociatiei
     *
     * @return int
     */
    public function getAsociatieId()
    {
        return $this->asociatieId;
    }

    /** (v)
     *
     * Preia Blocurile si le salveaza intr-o variabila interna
     *
     * @return NULL
     */
    protected function _loadBlocuri()
    {
        $blocuri = App_Component_Bloc_Factory::getInstance()->getAll( $this->getAsociatieId(), $this->getMonth() );

        $this->blocuri = $blocuri;
    }

    /** (o)
     * Preia Scarile si le salveaza intr-o variabila interna
     * @return NULL
     */
    protected function _loadScari()
    {
        $scariData = App_Component_Scara_Factory::getInstance()->getAll( $this->getAsociatieId(), $this->getMonth() );

        $scari = [];

        // ordonez scarile dupa denumirile blocurilor
        foreach( $this->blocuri as $blocId => $bloc )
        {
            foreach( $scariData as $scaraId => $scara )
            {
                if ( $scara[ App_Component_Scara_Object::PROP_BLOC_ID ] == $blocId )
                {
                    // adaug denumirea blocului
                    $scara[ App_Component_Scara_Object::DENUMIRE_BLOC ] = $bloc[ App_Component_Bloc_Object::PROP_DENUMIRE ];

                    $scari[ $scaraId ] = $scara;
                }
            }
        }

        $this->scari = $scari;
    }

    /** (o)
     * Preia Spatiile si le salveaza intr-o variabila interna
     * @return NULL
     */
    protected function _loadSpatii()
    {
        $spatii = App_Component_Spatiu_Factory::getInstance()->getAll( $this->getAsociatieId(), $this->getMonth() );

        $this->spatii = $spatii;

        // grupez spatiile dupa scara
        foreach( $spatii as $spatiuId => $spatiuValues )
        {
            $this->scari[ $spatiuValues[ App_Component_Spatiu_Object::PROP_SCARA_ID ] ][ App_Component_Scara_Object::SPATII_DATA ][ $spatiuId ] = $spatiuValues;
        }

    }

    /** (o)
     *
     * Extrage cheltuielile, grupate dupa coloane
     */
    protected function _loadCheltuieliColoane()
    {
        $cheltuieliColoane = App_Component_Cheltuiala_Factory::getInstance()->getAllByColoane( $this->getAsociatieId(), $this->getMonth() );

        $this->cheltuieliColoane = $cheltuieliColoane;
    }

    /** (o)
     *
     * Preia coloanele si le salveaza intr-o variabila interna
     * @return NULL
     */
    protected function _loadColoane()
    {
        $coloane = App_Component_Coloana_Factory::getInstance()->getAll();

        $this->coloane = $coloane;
    }

    /** (o)
     *
     * Preia coloanele si le salveaza intr-o variabila interna
     * @return NULL
     */
    protected function _loadParametriAsociatie()
    {
        $parametri = App_Component_AsociatieParametru_Factory::getInstance()->getAll( $this->getAsociatieId(), $this->getMonth() );

        $this->parametriAsociatie = $parametri;
    }

    /** (v)
     *
     * @throws App_Component_Lista_Exception_Compute
     */
    protected function checkCheltuieli()
    {
        $exista = false;

        foreach($this->cheltuieliColoane as $cheltuieliColoana)
        {
            if ( count( $cheltuieliColoana ) > 0 )
            {
                $exista = true;
                break;
            }
        }

        if ( !$exista )
        {
            throw new App_Component_Lista_Exception_Compute( 'Nu există nici o cheltuială înregistrată.' );
        }

    }

    /** (v)
     *
     */
    protected function prepareSpatiiValues()
    {
        $this->spatiiValues = [];
    }

    /**
     *
     */
    public function compute()
    {
        try
        {
            $this->error = null;

            $this->loadData();

            $this->prepareSpatiiValues();

            // ------------>

            // verific daca exista cheltuieli
            $this->checkCheltuieli();

            // calculez valorile pentru coloanele cu prioritatea 0 (maxima)
            foreach ( $this->coloane as $coloanaId => $coloanaData )
            {
                if ( $coloanaData[ App_Component_Coloana_Object::PRIORITY ] > 0 ) continue;

                $coloanaCode = $coloanaData[ App_Component_Coloana_Object::CODE ];

                /* @var $ColoanaLista App_Component_ColoanaLista_Object */
                $ColoanaLista = App_Component_ColoanaLista_Factory::getInstance()->create([$coloanaData, $this], $coloanaCode);

                $spatiiColoanaValues = $ColoanaLista->compute();

                $cheltuieliScari = $ColoanaLista->getCheltuieliScari();
                foreach( $cheltuieliScari as $scaraId => $cheltuieliScara )
                {
                    foreach( $cheltuieliScara as $cheltuialaId => $cheltuialaData )
                    {
                        $coloanaCode = $cheltuialaData[ App_Component_ColoanaLista_Object::CHELTUIALA_COLOANA_CODE ];

                        if ( $cheltuialaData[ App_Component_ColoanaLista_Object::CHELTUIALA_VALOARE ] - @$cheltuialaData[ App_Component_ColoanaLista_Object::CHELTUIALA_VALOARE_REPARTIZATA ] < 1e-12 )
                        {
                            unset( $cheltuialaData[ App_Component_ColoanaLista_Object::CHELTUIALA_VALOARE_REPARTIZATA ] );
                        }

                        $this->cheltuieliScari[ $scaraId ][ $coloanaCode ][ $cheltuialaId ] = $cheltuialaData;
                    }
                }

                foreach( $spatiiColoanaValues as $spatiuId => $coloanaValues )
                {
                    $this->spatiiValues[ $spatiuId ][ $coloanaId ] = $coloanaValues;
                }

            }

            //// calculare total luna si general

            foreach( $this->coloane as $coloanaId => $coloanaData )
            {
                if( $coloanaData[ App_Component_Coloana_Object::CODE ] == 'total_luna' )
                {
                    $coloanaTotalLunaId = $coloanaId;
                }

                if( $coloanaData[ App_Component_Coloana_Object::CODE ] == 'total_general' )
                {
                    $coloanaTotalGeneralId = $coloanaId;
                }
            }

            foreach( $this->spatiiValues as $spatiuId => $spatiuValues )
            {
                $totalLuna = 0;
                $totalGeneral = 0;

                foreach( $spatiuValues as $coloanaId => $coloanaValues )
                {
                    $usedBy = $this->coloane[ $coloanaId ][ App_Component_Coloana_Object::USED_BY ];

                    if( $usedBy == 'total_luna' )
                    {
                        $totalLuna += $coloanaValues[ self::VALUE ];
                        $totalGeneral += $coloanaValues[ self::VALUE ];
                    }

                    if( $usedBy == 'total_general' )
                    {
                        $totalGeneral += $coloanaValues[ self::VALUE ];
                    }
                }

                $this->spatiiValues[ $spatiuId ][ $coloanaTotalLunaId ][ self::VALUE ] = $totalLuna;
                $this->spatiiValues[ $spatiuId ][ $coloanaTotalGeneralId ][ self::VALUE ] = $totalGeneral;
            }

            //// grupare spatii dupa scari

            foreach( $this->scari as $scaraId => $scara )
            {
                $scaraValues = [];
                foreach( $scara[ App_Component_Scara_Object::SPATII_DATA ] as $spatiuId => $spatiuData )
                {
                    $scaraValues[ $spatiuId ] = $this->spatiiValues[ $spatiuId ];
                }

                $ScaraLista = new App_Component_ScaraLista_Object( $this, $scara, $scaraValues );

                $this->ListaContainer[ $scaraId ] = $ScaraLista;
            }

            // salvez contributiile calculate
            App_Component_ContributieSpatiu_Factory::getInstance()->saveContributii( $this->spatiiValues, $this->getAsociatieId(), $this->getMonth() );

            return $this->spatiiValues;

        }
        catch (App_Component_Lista_Exception_Compute $exception)
        {
            $error = $exception->getMessage();

            $this->setError( $error );

            return OPERATION_ERROR;
        }

    }

    public function getComputeError()
    {
        return $this->error;
    }

    protected function setError($error)
    {
        $this->error = $error;
    }

    /**
     *
     * @return array
     */
    public function getColoane()
    {
        return $this->coloane;
    }

    /** (v)
     *
     * @return array
     */
    public function getSpatii()
    {
        return $this->spatii;
    }

    /** (v)
     *
     * @return array
     */
    public function getScari()
    {
        return $this->scari;
    }

    /** (v)
     *
     * @return array
     */
    public function getCheltuieliColoane()
    {
        return $this->cheltuieliColoane;
    }

    /** (v)
     *
     * @return array
     */
    public function getCheltuieliScari()
    {
        return $this->cheltuieliScari;
    }

    /** (v)
     *
     * @param type $spatiuId
     * @return type
     */
    public function getDenumireSpatiu( $spatiuId )
    {
        $numar = $this->spatii[ $spatiuId ][ App_Component_Spatiu_Object::PROP_NUMAR ];

        $scaraId = $this->spatii[ $spatiuId ][ App_Component_Spatiu_Object::PROP_SCARA_ID ];

        $denumireScara = $this->getDenumireScara( $scaraId );

        $denumire = "Ap. $numar, $denumireScara";

        return $denumire;
    }

    /** (v)
     *
     * @param type $spatiuId
     * @return type
     */
    public function getDenumireScara( $scaraId )
    {
        $scara = $this->scari[ $scaraId ][ App_Component_Scara_Object::PROP_DENUMIRE ];

        $blocId = $this->scari[ $scaraId ][ App_Component_Scara_Object::PROP_BLOC_ID ];
        $bloc = $this->blocuri[ $blocId ][ App_Component_Bloc_Object::PROP_DENUMIRE ];

        $denumire = "Sc. $scara, Bl. $bloc";

        return $denumire;
    }

    public function getDataAfisarii()
    {
        $cacheId = 'data-afisarii';

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $now = Lib_Date::getInstance()->now();
            $dataAfisarii = Lib_Date::getInstance()->addDays( $now, 0 );

            $this->_cache[ $cacheId ] = $dataAfisarii;
        }

        return $this->_cache[ $cacheId ];
    }

    public function getScadenta()
    {
        $cacheId = 'scadenta';

        if ( !isset( $this->_cache[ $cacheId ] ) )
        {
            $scadenta = Lib_Date::getInstance()->addDays( $this->getDataAfisarii(), Lib_Application::getInstance()->getUser()->getValoareParametru( App_Component_AsociatieParametru_Factory::INTERVAL_SCADENTA ) );

            $this->_cache[ $cacheId ] = $scadenta;
        }

        return $this->_cache[ $cacheId ];
    }

    public function saveOutput( $content )
    {
        App_Component_OutputLista_Factory::getInstance()->saveContent( $this->getAsociatieId(), $this->getMonth(), $content );
    }


}