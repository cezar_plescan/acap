<?php

class App_Component_Lista_Exception_Compute extends UnexpectedValueException
{
    public function __construct()
    {
        $message = func_get_arg( 0 );

        parent::__construct($message);
    }

}
