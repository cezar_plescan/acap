<?php

class App_Component_Lista_Exception_Compute_Cheltuiala_ValoareUndefined extends App_Component_Lista_Exception_Compute
{
    public function __construct()
    {
        $descriereCheltuiala = func_get_arg( 0 );

        $message = 'Valoare facturii "' . $descriereCheltuiala . '" este nulă.';

        parent::__construct( $message );
    }

}
