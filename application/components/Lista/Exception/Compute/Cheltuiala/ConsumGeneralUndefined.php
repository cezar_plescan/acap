<?php

class App_Component_Lista_Exception_Compute_Cheltuiala_ConsumGeneralUndefined extends App_Component_Lista_Exception_Compute
{
    public function __construct()
    {
        $descriereCheltuiala = func_get_arg( 0 );

        $message = 'Consumul general pentru factura "' . $descriereCheltuiala . '" nu este definit';

        parent::__construct( $message );
    }

}
