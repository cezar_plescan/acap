<?php

class App_Component_Lista_Exception_Compute_Cheltuiala_SpatiuUsed extends App_Component_Lista_Exception_Compute
{
    public function __construct()
    {
        $Lista = func_get_arg( 0 );
        $spatiuId = func_get_arg( 1 );
        $descriereCheltuiala = func_get_arg( 2 );

        $denumireSpatiu = $Lista->getDenumireSpatiu( $spatiuId );

        $message = 'Pentru ' . $denumireSpatiu . ' a mai fost repartizată o factură de ' . $descriereCheltuiala;

        parent::__construct( $message );
    }

}
