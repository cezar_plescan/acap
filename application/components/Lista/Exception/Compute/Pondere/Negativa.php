<?php

class App_Component_Lista_Exception_Compute_Pondere_Negativa extends App_Component_Lista_Exception_Compute
{
    public function __construct()
    {
        $Lista = func_get_arg( 0 );
        $spatiuId = func_get_arg( 1 );
        $parametru = func_get_arg( 2 );

        $denumireParametru = App_Component_SpatiuParametru_Factory::getDenumireNearticulata($parametru);
        $denumireSpatiu = $Lista->getDenumireSpatiu( $spatiuId );

        $message = 'Valoarea campului "' . $denumireParametru . '" pentru ' . $denumireSpatiu . ' nu poate fi mai mica decat zero.';

        parent::__construct( $message );
    }

}
