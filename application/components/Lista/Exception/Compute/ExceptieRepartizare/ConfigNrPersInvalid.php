<?php

class App_Component_Lista_Exception_Compute_ExceptieRepartizare_ConfigNrPersInvalid extends App_Component_Lista_Exception_Compute
{
    public function __construct()
    {
        $Lista = func_get_arg( 0 );
        $Exceptie = func_get_arg( 1 );
        $spatiuId = func_get_arg( 2 );
        $nrPersReducere = func_get_arg( 3 );
        $nrPers = func_get_arg( 4 );

        $denumireExceptie = $Exceptie->getDenumire();

        $message = 'Numărul de persoane (' . $nrPersReducere . ')  pentru care se aplică reducerea "' . $denumireExceptie . '" la ' . $Lista->getDenumireSpatiu( $spatiuId ) . ' este mai mic decât numărul curent de persoane declarate (' . $nrPers . ')';

        parent::__construct( $message );
    }

}
