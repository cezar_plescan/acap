<?php

class App_Component_Lista_Exception_Compute_ExceptieRepartizare_ConfigUndefined extends App_Component_Lista_Exception_Compute
{
    public function __construct()
    {
        $Lista = func_get_arg( 0 );
        $Exceptie = func_get_arg( 1 );
        $parametru = func_get_arg( 2 );

        $denumireExceptie = $Exceptie->getDenumire();

        $denumireParametru = App_Component_Parametru_ReducereRepartizare_Factory::getDenumireNearticulata( $parametru );

        $message = 'Valoarea campului "' . $denumireParametru . '" nu este definită pentru reducerea "' . $denumireExceptie .'"';

        parent::__construct( $message );
    }

}
