<?php

class App_Component_Lista_Exception_Compute_TotalPonderiNul extends App_Component_Lista_Exception_Compute
{
    public function __construct()
    {
        $Lista = func_get_arg( 0 );
        $spatiiIds = (array)func_get_arg( 1 );
        $parametru = func_get_arg( 2 );

        $denumireParametru = App_Component_SpatiuParametru_Factory::getDenumireNearticulata($parametru);

        $denumiriSpatii = [];
        foreach ($spatiiIds as $spatiuId)
        {
            $denumiriSpatii[] = $Lista->getDenumireSpatiu( $spatiuId );
        }
        $denumiriSpatii = implode('; ', $denumiriSpatii);

        $message = 'Valoarea campului "' . $denumireParametru . '" este nulă pentru următoarele apartamente: ' . $denumiriSpatii;

        parent::__construct( $message );
    }

}
