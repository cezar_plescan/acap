<?php

class App_Component_ReducereRepartizareCheltuiala_Object extends Lib_Component_ORM_Object
{
    // fields
    const CHELTUIALA_ID = 'cheltuiala_id';
    const REDUCERE_REPARTIZARE_ID = 'reducere_repartizare_id';

    const STATUS_ACTIVE     = 0;
    const STATUS_DELETED    = 1;

    protected $_properties = [
        self::PROP_ID            => NULL,
        self::CHELTUIALA_ID => NULL,
        self::REDUCERE_REPARTIZARE_ID => NULL,
    ];

    protected $_cache = [];


}
