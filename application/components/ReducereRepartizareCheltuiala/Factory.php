<?php

class App_Component_ReducereRepartizareCheltuiala_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    protected
            $_cache = [];

    public function add($cheltuialaId, $repartizareId)
    {
        $args = [
            App_Component_ReducereRepartizareCheltuiala_Object::CHELTUIALA_ID   => $cheltuialaId,
            App_Component_ReducereRepartizareCheltuiala_Object::REDUCERE_REPARTIZARE_ID => $repartizareId,
        ];

        $Object = $this->createAndSave([$args]);

        return $Object;
    }

}
