<?php

class App_Component_ContributieSpatiu_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'contributie_spatiu';

    public function saveContributii( array $contributiiData )
    {
        $columns = [
            App_Component_ContributieSpatiu_Object::ASOCIATIE_ID,
            App_Component_ContributieSpatiu_Object::MONTH,
            App_Component_ContributieSpatiu_Object::SPATIU_ID,
            App_Component_ContributieSpatiu_Object::COLOANA_ID,
            App_Component_ContributieSpatiu_Object::VALOARE,
        ];
        $quotedColumnsEnum = $this->quoteColumns( $columns );

        $valueColumn = $this->quoteColumns( App_Component_ContributieSpatiu_Object::VALOARE );

        $quotedValues = [];
        foreach( $contributiiData as $rowData )
        {
            $rowValues = Lib_Tools::filterArray($rowData, $columns);

            $quotedRowValues = [];
            foreach( $rowValues as $value )
            {
                $quotedRowValues[] = $this->quoteValue( $value );
            }

            $quotedValues[] = '(' . implode( ', ', $quotedRowValues ) . ')';
        }

        $quotedValuesQuery = implode( ', ', $quotedValues );

        $sql = 'INSERT INTO ' . $this->quoteTableName() . '(' . $quotedColumnsEnum . ')' .
               ' VALUES ' . $quotedValuesQuery .
               ' ON DUPLICATE KEY UPDATE ' . $valueColumn . ' = VALUES (' . $valueColumn . ')';

        $this->query($sql);

    }


}
