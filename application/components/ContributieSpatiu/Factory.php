<?php

class App_Component_ContributieSpatiu_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    public function setContributii( $cheltuialaId, $contributii )
    {
        $DB = $this->getDb();
        
        // stergere contributii existente pentru acesta cheltuiala
        $this->removeContributii( $cheltuialaId );
        
        if( $contributii !== NULL && is_array( $contributii ) )
        {
            foreach( $contributii as $spatiuId => $contributie )
            {
                $contributieData = [
                    App_Component_ContributieSpatiu_Object::PROP_CHELTUIALA_ID  => $cheltuialaId,
                    App_Component_ContributieSpatiu_Object::PROP_SPATIU_ID      => $spatiuId,
                    App_Component_ContributieSpatiu_Object::PROP_VALOARE        => $contributie,
                ];

                $DB->insert( $contributieData );
            }
        }
    }

    public function getContributiiByCheltuieli( array $cheltuieli )
    {
        $contributii = [];
                
        $rows = $this->retrieveData( [
                App_Component_ContributieSpatiu_Object::PROP_CHELTUIALA_ID,
                App_Component_ContributieSpatiu_Object::PROP_SPATIU_ID,
                App_Component_ContributieSpatiu_Object::PROP_VALOARE,
            ], [
                App_Component_ContributieSpatiu_Object::PROP_CHELTUIALA_ID => $cheltuieli, 
            ]);
        
        foreach( $rows as $row )
        {
            $cheltuialaId = $row[ App_Component_ContributieSpatiu_Object::PROP_CHELTUIALA_ID ];
            
            if( !isset( $contributii[ $cheltuialaId ] ) )
            {
                $contributii[ $cheltuialaId ] = [];
            }
            
            $spatiuId = $row[ App_Component_ContributieSpatiu_Object::PROP_SPATIU_ID ];
            $value = $row[ App_Component_ContributieSpatiu_Object::PROP_VALOARE ];
            
            $contributii[ $cheltuialaId ][ $spatiuId ] = $value;
        }
        
        return $contributii;
    }
    
    public function removeContributii( $cheltuialaId )
    {
        $DB = $this->getDb();
        
        // stergere contributii existente pentru acesta cheltuiala
        $DB->delete([
            App_Component_ContributieSpatiu_Object::PROP_CHELTUIALA_ID => $cheltuialaId,
        ]);
    }
    
}
