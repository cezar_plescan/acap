<?php

class App_Component_ContributieSpatiu_Object extends Lib_Component_ORM_Object
{
    const PROP_SPATIU_ID         = 'spatiu_id';
    const PROP_CHELTUIALA_ID     = 'cheltuiala_id';
    const PROP_VALOARE           = 'value';

    protected $_properties = [
        self::PROP_ID           => null,
        self::PROP_SPATIU_ID    => null,
        self::PROP_CHELTUIALA_ID => null,
        self::PROP_VALOARE      => null,
    ];


}