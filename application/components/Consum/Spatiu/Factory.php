<?php

class App_Component_Consum_Spatiu_Factory extends App_Component_Consum_Factory
{
    static protected $instance;

    // consumuri disponibile (aceste valori se slveaza ca referinte in DB)
    const APA_RECE       = 'ar1';
    const APA_CALDA      = 'ac2';

    // moduri de repartizare consum
    const REPARTIZARE_EGAL      = 'egal';
    const REPARTIZARE_CONSUM    = 'consum';
    const REPARTIZARE_PERSOANE  = 'pers';
    const REPARTIZARE_SUPRAFATA = 'supr';


    static protected
            $consumuriDisponibile = [
                self::APA_RECE  => 'Apă rece',
                self::APA_CALDA => 'Apă caldă',
            ];

    /** (v)
     *
     * Returneaza parametrii spatiilor
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAllBySpatiu( $asociatieId, $month )
    {
        $data = $this->getDb()->getAll( $asociatieId, self::REF_TYPE_SPATIU, $month );
        $consumuriData = [];

        foreach ($data as $rowData)
        {
            $spatiuId = $rowData[ App_Component_Consum_Spatiu_Object::REFERENCE_ID ];
            if ( !isset( $consumuriData[ $spatiuId ] ) )
            {
                $consumuriData[ $spatiuId ] = [];
            }

            $consumuriData[ $spatiuId ][ $rowData[ App_Component_Consum_Spatiu_Object::PARAMETRU ] ] = $rowData[ App_Component_Consum_Spatiu_Object::VALUE ];
        }

        return $consumuriData;
    }



}
