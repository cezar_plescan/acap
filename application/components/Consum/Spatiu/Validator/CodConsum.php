<?php

class App_Component_Consum_Spatiu_Validator_CodConsum extends App_Component_Consum_Validator_CodConsum
{
    /**
     * @return array
     */
    protected function getConsumuri()
    {
        $consumuri = array_keys( App_Component_Consum_Spatiu_Factory::getInstance()->getConsumuriDisponibile() );
        
        return $consumuri;
    }
            
}
