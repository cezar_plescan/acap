<?php

abstract class App_Component_Consum_Validator_CodConsum extends Lib_Validator_Abstract
{
    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID           => 'Consum invalid',
            self::ERR_REQUIRED          => 'Consum inexistent',
        ));

    }

    protected function _isValid($codConsum)
    {
        $codConsum = strtolower( $codConsum );
        
        $consumuri = (array)$this->getConsumuri();
        
        if ( !in_array( $codConsum, $consumuri ) )
        {
            $this->_setError( self::ERR_INVALID );
        }
        
        return $codConsum;
    }

    /**
     * @return array
     */
    abstract protected function getConsumuri();
            
}
