<?php

class App_Component_Consum_Validator_Value extends Lib_Validator_Abstract
{
    protected
            $decimalNumberValidator;

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID           => 'Valoare consum incorectă: "%value%"',
        ));

        $this->setRequired( false );

        $this->decimalNumberValidator = new Lib_Validator_Number_Decimal();
    }

    protected function _isValid($value)
    {
        // verific daca valoarea este numerica
        if ( $this->decimalNumberValidator->isValid($value) )
        {
            $value = $this->decimalNumberValidator->getFilteredValue();

            return $value;
        }

        $this->_setError( self::ERR_INVALID, ['value' => $value] );
    }

}
