<?php

class App_Component_Consum_Validator_Month extends Lib_Validator_Abstract
{
    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_INVALID   => 'Consumurile nu pot fi înregistrate',
        ));

        $this->setRequired( false );
        $this->setSkipValidation( false );
    }

    protected function _isValid( $monthId )
    {
        $Date = Lib_Date::getInstance();
        $currentMonthId = $Date->getCurrentMonthId();

        $lunaDeLucru = Lib_Application::getInstance()->getUser()->getLunaCurenta();

        if ( !$lunaDeLucru )
        {
            $this->_setError( self::ERR_INVALID );
        }
        else if ( $lunaDeLucru > $currentMonthId )
        {
            $this->_setError( self::ERR_INVALID );
        }
        // luna se calculeaza automat
        else
        {
            $monthId = $lunaDeLucru;
        }

        return $monthId;
    }

}
