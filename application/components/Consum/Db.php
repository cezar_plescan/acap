<?php

class App_Component_Consum_Db extends Lib_Component_ORM_Db
{
    const TABLE = 'consum';

    /** (v)
     *
     * @param int $asociatieId
     * @param int $refType
     * @param int $month
     *
     * @return array
     */
    public function getAll( $asociatieId, $refType, $month )
    {
        $columns = $this->quoteColumns([
            App_Component_Consum_Object::REFERENCE_ID,
            App_Component_Consum_Object::PARAMETRU,
            App_Component_Consum_Object::VALUE,
        ]);

        $sql =
                $this->selectStmt( $columns ) .

                $this->where($this->whereAnd([
                        $this->whereEqual( $asociatieId, App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID ),
                        $this->whereEqual( $month, App_Component_Consum_Object::MONTH ),
                        $this->whereEqual( $refType, App_Component_Consum_Object::REFERENCE_TYPE ),
                ]));

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

}
