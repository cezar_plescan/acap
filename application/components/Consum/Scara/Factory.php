<?php

class App_Component_Consum_Scara_Factory extends App_Component_Consum_Factory
{
    static protected $instance;

    // consumuri disponibile (aceste valori se slveaza ca referinte in DB)
    const APA_RECE       = 'ar_s1';
    const APA_CALDA      = 'ac_s1';
    const APA_RECE_COMUN = 'arc_s2';
    const APA_CALDA_COMUN = 'acc_s2';

    // moduri de repartizare consum
    const REPARTIZARE_EGAL  = 'e';
    const REPARTIZARE_COMUN = 'c';

    // diferenta repartizata la o scara
    const APA_RECE_DIFERENTA_GENERAL_PASANTI = 'ar_dif_gen_pas';
    const APA_CALDA_DIFERENTA_GENERAL_PASANTI = 'ac_dif_gen_pas';

    const APA_RECE_REPARTIZAT   = 'ar_rep';
    const APA_CALDA_REPARTIZAT  = 'ac_rep';

    static protected
            $consumuriDisponibile = [
                self::APA_RECE          => 'Apă rece',
                self::APA_CALDA         => 'Apă caldă',
//                self::APA_RECE_COMUN    => 'Consum comun apă rece',
//                self::APA_CALDA_COMUN   => 'Consum comun apă caldă',
            ];

    /** (v)
     *
     * Returneaza parametrii spatiilor
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return array
     */
    public function getAllByScara( $asociatieId, $month )
    {
        $data = $this->getDb()->getAll( $asociatieId, self::REF_TYPE_SCARA, $month );
        $consumuriData = [];

        foreach ($data as $rowData)
        {
            $scaraId = $rowData[ App_Component_Consum_Scara_Object::REFERENCE_ID ];
            if ( !isset( $consumuriData[ $scaraId ] ) )
            {
                $consumuriData[ $scaraId ] = [];
            }

            $consumuriData[ $scaraId ][ $rowData[ App_Component_Consum_Scara_Object::PARAMETRU ] ] = $rowData[ App_Component_Consum_Scara_Object::VALUE ];
        }

        return $consumuriData;
    }


}
