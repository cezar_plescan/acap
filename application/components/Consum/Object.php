<?php

class App_Component_Consum_Object extends Lib_Component_ORM_Object
{
    // properties
    const _ASOCIATIE_ID     = '_asociatie_id';
    const REFERENCE_ID      = 'ref_id';
    const REFERENCE_TYPE    = 'ref_type';
    const PARAMETRU         = 'parameter';
    const VALUE             = 'value';
    const MONTH             = 'month';

    protected $_properties = [
        self::PROP_ID            => NULL,
        self::_ASOCIATIE_ID => NULL,
        self::REFERENCE_ID  => NULL,
        self::REFERENCE_TYPE => NULL,
        self::PARAMETRU     => NULL,
        self::VALUE         => NULL,
        self::MONTH         => NULL,
        self::PROP_TIMESTAMP     => NULL,
    ];


}
