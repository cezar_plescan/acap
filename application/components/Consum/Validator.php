<?php

abstract class App_Component_Consum_Validator extends Lib_Validator_Abstract
{
    const ERR_REFERENCE_INVALID         = 'referinta_invalida';
    const ERR_CONSUM_INVALID            = 'consum_invalid';
    const ERR_VALOARE_CONSUM_INVALID    = 'valoare_consum_invalid';

    const OPT_ASOCIATIE_ID = 'a';
    const OPT_MONTH = 'm';

    protected function init()
    {
        $this->addErrorTemplates([
            self::ERR_INVALID   => 'Valoare invalidă',
        ]);

        $this->setRequired( false );
    }

    protected function _isValid( $values )
    {
        // $values must be an array
        if ( !is_array($values) )
        {
            $this->_setError( self::ERR_INVALID );
        }

        $asociatieId = $this->getOption( self::OPT_ASOCIATIE_ID );
        $month = $this->getOption( self::OPT_MONTH );

        $filteredValues = [];

        $referenceValidator     = $this->getResource( 'Validator_ReferenceId' );
        $referenceValidator->setOption( $referenceValidator::OPT_ASOCIATIE_ID, $asociatieId );
        $referenceValidator->setOption( $referenceValidator::OPT_MONTH, $month );

        $codConsumValidator     = $this->getResource( 'Validator_CodConsum' );

        $valoareConsumValidator = new App_Component_Consum_Validator_Value();

        // verific id-urile referintelor
        $referenceIds = array_keys( $values );

        $referenceValid = $referenceValidator->isValid( $referenceIds );
        if ( !$referenceValid )
        {
            $this->_setCustomError( self::ERR_REFERENCE_INVALID, $referenceValidator->getErrorMessage() );
        }

        /* the format of the $value must be:
         * - key = <ref_id>
         * - value = array[ <consum> => <consum_value> ]
         */
        foreach( $values as $referenceId => $consumuri )
        {
            // verific consumurile
            if ( !is_array( $consumuri ) )
            {
                $this->_setError( self::ERR_INVALID );
            }

            foreach( $consumuri as $codConsum => $valoareConsum )
            {
                // verific existenta consumului
                $consumValid = $codConsumValidator->isValid( $codConsum );
                if ( !$consumValid )
                {
                    $this->_setCustomError( self::ERR_CONSUM_INVALID, $codConsumValidator->getErrorMessage() );
                }
                $codConsum = $codConsumValidator->getFilteredValue();

                // verific valoarea consumului
                $valoareConsumValid = $valoareConsumValidator->isValid( $valoareConsum );
                if ( !$valoareConsumValid )
                {
                    $this->_setCustomError( self::ERR_VALOARE_CONSUM_INVALID, $valoareConsumValidator->getErrorMessage() );
                }

                $valoareConsum = $valoareConsumValidator->getFilteredValue();

                $filteredValues[ $referenceId ][ $codConsum ] = $valoareConsum;
            }
        }

        return $filteredValues;
    }

}
