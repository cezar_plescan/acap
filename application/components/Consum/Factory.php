<?php

class App_Component_Consum_Factory extends Lib_Component_ORM_Factory
{
    static protected $instance;

    const REF_TYPE_SPATIU       = 1;
    const REF_TYPE_SCARA        = 2;

    static protected
            $consumuriDisponibile = [];

    /** (v)
     *
     * @return type
     */
    public function getConsumuriDisponibile()
    {
        return static::$consumuriDisponibile;
    }

    /** (v)
     *
     * @param array $consumuri
     * @return boolean
     */
    public function add( array $consumuri )
    {
        foreach( $consumuri as $consumData )
        {
            $consumData = Lib_Tools::filterArray( $consumData, [
                App_Component_Consum_Object::REFERENCE_ID,
                App_Component_Consum_Object::REFERENCE_TYPE,
                App_Component_Consum_Object::PARAMETRU,
                App_Component_Consum_Object::VALUE,
                App_Component_Consum_Object::MONTH,
                App_Component_Consum_Object::_ASOCIATIE_ID,
            ]);

            $criteria = Lib_Tools::filterArray($consumData, [
                App_Component_Consum_Object::REFERENCE_ID,
                App_Component_Consum_Object::REFERENCE_TYPE,
                App_Component_Consum_Object::PARAMETRU,
            ]);

            /* @var $Consum App_Component_Consum_Object */
            $Consum = $this->find( $criteria );
            $valoareConsum = $consumData[ App_Component_Consum_Object::VALUE ];

            // consumul nu exista
            if ( null === $Consum )
            {
                // valoarea consumului este specificata
                if ( null !== $valoareConsum )
                {
                    // se salveaza consumul
                    $Consum = $this->createAndSave( [$consumData] );
                }
            }
            // consumul a fost inregistrat deja
            else
            {
                // valoarea consumului este specificata
                if ( null !== $valoareConsum )
                {
                    // se salveaza consumul
                    $Consum->update( $consumData )->save();
                }
                else
                {
                    $Consum->delete();
                }
            }
        }

        return true;
    }

}
