<?php

class App_Component_SpatiuParametru_Factory extends App_Component_ParametruEntity_Factory
{
    static protected $instance;

    // parametri disponibili
    const PARAMETRU_PROPRIETAR      = 'proprietar';
    const PARAMETRU_NR_PERSOANE     = 'nr_pers';
    const PARAMETRU_SUPRAFATA       = 'suprafata';
    const PARAMETRU_RESTANTE        = 'restante';
    const PARAMETRU_INTRETINERE     = 'intretinere';

    static protected $denumiriNearticulate = [
        self::PARAMETRU_NR_PERSOANE => 'Număr de persoane',
        self::PARAMETRU_PROPRIETAR  => 'Proprietar',
        self::PARAMETRU_SUPRAFATA   => 'Suprafaţă',
    ];

    static protected $denumiriArticulate = [
        self::PARAMETRU_NR_PERSOANE => 'Numărul de persoane',
        self::PARAMETRU_PROPRIETAR  => 'Proprietarul',
        self::PARAMETRU_SUPRAFATA   => 'Suprafaţa',
    ];

    static protected $denumiriGenitiv = [
        self::PARAMETRU_NR_PERSOANE => 'Numărului de persoane',
        self::PARAMETRU_PROPRIETAR  => 'Proprietarului',
        self::PARAMETRU_SUPRAFATA   => 'Suprafaţei',
    ];

    static public function getDenumireNearticulata( $parametru )
    {
        return self::$denumiriNearticulate[ $parametru ];
    }

    static public function getDenumireArticulata( $parametru )
    {
        return self::$denumiriArticulate[ $parametru ];
    }

    static public function getDenumireGenitiv( $parametru )
    {
        return self::$denumiriGenitiv[ $parametru ];
    }

    static public function getCheltuieliAditionale()
    {
        return [
            App_Component_Coloana_Factory::COLOANA_RESTANTE         => 'Restanţe',
            App_Component_Coloana_Factory::COLOANA_FOND_REPARATII   => 'Fond de reparaţii',
            App_Component_Coloana_Factory::COLOANA_FOND_RULMENT     => 'Fond de rulment',
            App_Component_Coloana_Factory::COLOANA_PENALIZARI       => 'Penalizări',
        ];
    }

    static public function getMainParametri()
    {
        return [
            self::PARAMETRU_PROPRIETAR      => 'Proprietar',
            self::PARAMETRU_NR_PERSOANE     => 'Nr. persoane',
            self::PARAMETRU_SUPRAFATA       => 'Suprafaţa',
        ];
    }

    /** (o)
     *
     * @param int $asociatieId
     *
     * Returneaza parametrii spatiilor
     */
    public function getAllBySpatiu( $asociatieId, $month = null )
    {
        $data = $this->getDb()->getAll( $asociatieId, $month );

        $parametriData = [];

        foreach( $data as $rowData )
        {
            $spatiuId = $rowData[ App_Component_SpatiuParametru_Object::PROP_REF_ID ];
            if( !isset( $parametriData[ $spatiuId ] ) )
            {
                $parametriData[ $spatiuId ] = [];
            }

            $parametriData[ $spatiuId ][ $rowData[ App_Component_SpatiuParametru_Object::PROP_PARAMETRU ] ] = $rowData[ App_Component_SpatiuParametru_Object::PROP_VALUE ];
        }

        return $parametriData;
    }
    
    public function getAllForSpatiu( $spatiuId, array $parametriList = [] )
    {
        $data = $this->getDb()->getAllForSpatiu( $spatiuId, $parametriList );

        $parametri = [];

        foreach( $data as $rowData )
        {
            $parametri[ $rowData[ App_Component_SpatiuParametru_Object::PROP_PARAMETRU ] ] = $rowData[ App_Component_SpatiuParametru_Object::PROP_VALUE ];
        }

        return $parametri;
    }

    /**
     * Adauga, editeaza sau sterge parametrii unui spatiu
     * 
     * @param array $parametri
     */
    public function update( array $parametri )
    {
        foreach( $parametri as $parametruData )
        {
            $parametruData = Lib_Tools::filterArray( $parametruData, [
                App_Component_SpatiuParametru_Object::PROP_REF_ID,
                App_Component_SpatiuParametru_Object::PROP_PARAMETRU,
                App_Component_SpatiuParametru_Object::PROP_VALUE,
                App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID,
            ]);

            $criteria = Lib_Tools::filterArray( $parametruData, [
                App_Component_SpatiuParametru_Object::PROP_REF_ID,
                App_Component_SpatiuParametru_Object::PROP_PARAMETRU,
                App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID,
            ]);

            /* @var $Parametru App_Component_SpatiuParametru_Object */
            $Parametru = $this->find( $criteria );
            $valoareParametru = $parametruData[ App_Component_SpatiuParametru_Object::PROP_VALUE ];

            // parametrul nu exista
            if ( null === $Parametru )
            {
                // valoarea parametrului este specificata
                if ( null !== $valoareParametru )
                {
                    // se salveaza parametrul
                    $Parametru = $this->createAndSave( [$parametruData] );
                }
            }
            // parametrul a fost setat deja
            else
            {
                // valoarea parametrului este specificata
                if ( null !== $valoareParametru )
                {
                    // se salveaza parametrul
                    $Parametru->update( $parametruData )->save();
                }
                else
                {
                    // se salveaza parametrul
                    $Parametru->delete();
                }
            }
        }
    }

    //==========================================================================
    
    
}
