<?php

class App_Component_SpatiuParametru_Db extends App_Component_ParametruEntity_Db
{
    const TABLE = 'spatiu_parametru';
    
    static protected $instance;

    /** (v)
     *
     * @param int $asociatieId
     * @param int $month
     *
     * @return type
     */
    public function getAll( $asociatieId, $month = null )
    {
        $columns = $this->quoteColumns([
                App_Component_SpatiuParametru_Object::PROP_REF_ID,
                App_Component_SpatiuParametru_Object::PROP_PARAMETRU,
                App_Component_SpatiuParametru_Object::PROP_VALUE,
        ]);

        $sql =
                $this->selectStmt( $columns ) .
                $this->where( [ App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID => $asociatieId ] );

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }

    public function getAllForSpatiu( $spatiuId )
    {
        $columns = $this->quoteColumns([
                App_Component_SpatiuParametru_Object::PROP_PARAMETRU,
                App_Component_SpatiuParametru_Object::PROP_VALUE,
        ]);

        $sql =  $this->selectStmt( $columns ) .
                $this->where( $this->whereEqual( $spatiuId, App_Component_SpatiuParametru_Object::PROP_REF_ID ) );

        $results = $this->queryAndFetchAll( $sql );

        return $results;
    }

}
