<?php

class __App_Component_Parametru_Spatiu_Validator_CheltuieliAditionale extends Lib_Validator_Abstract
{
    const ERR_REFERENCE_INVALID         = 'spatiu_invalid';
    const ERR_CHELTUIALA_INVALID        = 'cod_cheltuiala_invalid';
    const ERR_VALOARE_INVALID           = 'valoare_cheltuiala_invalid';

    const OPT_ASOCIATIE_ID = 'a';
    const OPT_MONTH = 'm';

    protected function init()
    {
        $this->addErrorTemplates([
            self::ERR_INVALID               => 'Valoare invalidă',
            self::ERR_CHELTUIALA_INVALID    => 'Cod cheltuiala invalid',
            self::ERR_VALOARE_INVALID       => 'Valoare incorectă: %value%',
        ]);

        $this->setRequired( false );
    }

    protected function _isValid( $values )
    {
        // $values must be an array
        if ( !is_array($values) )
        {
            $this->_setError( self::ERR_INVALID );
        }

        $asociatieId = $this->getOption( self::OPT_ASOCIATIE_ID );
        $month = $this->getOption( self::OPT_MONTH );

        $filteredValues = [];

        $spatiiValidator     = new App_Component_Spatiu_Validator_Id();
        $spatiiValidator->setOption( App_Component_Spatiu_Validator_Id::OPT_ASOCIATIE_ID, $asociatieId );
        $spatiiValidator->setOption( App_Component_Spatiu_Validator_Id::OPT_MONTH, $month );

        $valoareCheltuialaValidator = new Lib_Validator_Number_Decimal();
        $valoareCheltuialaValidator->setRequired( false );

        // verific id-urile referintelor
        $referenceIds = array_keys( $values );

        $referenceValid = $spatiiValidator->isValid( $referenceIds );
        if ( !$referenceValid )
        {
            $this->_setCustomError( self::ERR_REFERENCE_INVALID, $spatiiValidator->getErrorMessage() );
        }

        $coduriCheltuieli = App_Component_SpatiuParametru_Factory::getCheltuieliAditionale();

        /* the format of the $value must be:
         * - key = <ref_id>
         * - value = array[ <cod_cheltuiala> => <cheltuiala_value> ]
         */
        foreach( $values as $spatiuId => $cheltuieli )
        {
            // verific consumurile
            if ( !is_array( $cheltuieli ) )
            {
                $this->_setError( self::ERR_INVALID );
            }

            foreach( $cheltuieli as $codCheltuiala => $valoareCheltuiala )
            {
                // verific existenta consumului
                $codValid = isset( $coduriCheltuieli[ $codCheltuiala ] );
                if ( !$codValid )
                {
                    $this->_setError( self::ERR_CHELTUIALA_INVALID );
                }

                // verific valoarea consumului
                $valoareValid = $valoareCheltuialaValidator->isValid( $valoareCheltuiala );
                if ( !$valoareValid )
                {
                    $this->_setError( self::ERR_VALOARE_INVALID, ['value' => $valoareCheltuiala] );
                }

                $valoareCheltuiala = $valoareCheltuialaValidator->getFilteredValue();

                $filteredValues[ $spatiuId ][ $codCheltuiala ] = $valoareCheltuiala;
            }
        }

        return $filteredValues;
    }

}
