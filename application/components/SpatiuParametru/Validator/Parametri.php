<?php

class __App_Component_Parametru_Spatiu_Validator_Parametri extends Lib_Validator_Abstract
{
    const ERR_REFERENCE_INVALID         = 'spatiu_invalid';
    const ERR_PARAMETRU_INVALID         = 'cod_parametru_invalid';
    const ERR_VALOARE_INVALID           = 'valoare_parametru_invalid';

    const OPT_ASOCIATIE_ID = 'a';
    const OPT_MONTH = 'm';

    protected function init()
    {
        $this->addErrorTemplates([
            self::ERR_INVALID               => 'Valoare invalidă',
            self::ERR_PARAMETRU_INVALID     => 'Parametru invalid',
            self::ERR_VALOARE_INVALID       => 'Eroare la ap. %spatiu%: %error%',
        ]);

        $this->setRequired( false );
    }

    protected function _isValid( $values )
    {
        // $values must be an array
        if ( !is_array($values) )
        {
            $this->_setError( self::ERR_INVALID );
        }

        $asociatieId = $this->getOption( self::OPT_ASOCIATIE_ID );
        $month = $this->getOption( self::OPT_MONTH );

        $filteredValues = [];

        $spatiiValidator     = new App_Component_Spatiu_Validator_Id();
        $spatiiValidator->setOption( App_Component_Spatiu_Validator_Id::OPT_ASOCIATIE_ID, $asociatieId );
        $spatiiValidator->setOption( App_Component_Spatiu_Validator_Id::OPT_MONTH, $month );

        $parametriValidators = [
            App_Component_SpatiuParametru_Factory::PARAMETRU_PROPRIETAR    => new App_Component_SpatiuParametru_Validator_Proprietar(),
            App_Component_SpatiuParametru_Factory::PARAMETRU_NR_PERSOANE   => (new App_Component_SpatiuParametru_Validator_NrPers())->setRequired( false ),
            App_Component_SpatiuParametru_Factory::PARAMETRU_SUPRAFATA     => new App_Component_SpatiuParametru_Validator_Suprafata(),
        ];

        $valoareValidator = new Lib_Validator_Number_Decimal();
        $valoareValidator->setRequired( false );

        // verific id-urile referintelor
        $referenceIds = array_keys( $values );

        $referenceValid = $spatiiValidator->isValid( $referenceIds );
        if ( !$referenceValid )
        {
            $this->_setCustomError( self::ERR_REFERENCE_INVALID, $spatiiValidator->getErrorMessage() );
        }

        // !!! $coduriCheltuieli = App_Component_Parametru_Spatiu_Factory::getCheltuieliAditionale();

        /* the format of the $value must be:
         * - key = <ref_id>
         * - value = array[ <cod_parametru> => <parametru_value> ]
         */
        foreach( $values as $spatiuId => $parametri )
        {
            if ( !is_array( $parametri ) )
            {
                $this->_setError( self::ERR_INVALID );
            }

            foreach( $parametriValidators as $parametru => $parametruValidator )
            {
                if( !isset( $parametri[ $parametru ] ) )
                {
                    continue;
                }

                if( !$parametruValidator->isValid( $parametri[ $parametru ] ) )
                {
                    $spatiuInfo = App_Component_Spatiu_Factory::getInstance()->getDenumireCompleta( $spatiuId );
                    $this->_setError( self::ERR_VALOARE_INVALID, [ 'spatiu' => $spatiuInfo, 'error' => $parametruValidator->getErrorMessage() ] );
                }
                else
                {
                    $filteredValues[ $spatiuId ][ $parametru ] = $parametruValidator->getFilteredValue();
                }
            }
        }

        return $filteredValues;
    }

}
