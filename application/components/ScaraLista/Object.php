<?php

/**
 *
 * Lista de Plata se calculeaza si se afiseaaza pentru fiecare scara.
 * Obiectul Lista este constituit din obiecte ScaraLista
 * care contin datele specifice fiecarei liste asociate unei scari.
 */
class App_Component_ScaraLista_Object extends Lib_Component_Object implements IteratorAggregate
{
    protected
            /**
             * Lista de plata pentru intreaga Asociatie
             * @var App_Component_Lista_Object
             */
            $Lista,

            $scaraData = [],
            $scaraValues = [];

    /**
     *
     */
    public function __construct(App_Component_Lista_Object $Lista, array $scaraData, array $scaraValues)
    {
        $this->Lista = $Lista;
        $this->scaraData = $scaraData;
        $this->scaraValues = $scaraValues;

    }

    /**
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator( $this->scaraValues );
    }

    public function getColoane()
    {
        return $this->Lista->getColoane();
    }

    /**
     * @return App_Component_Lista_Object
     */
    public function getLista()
    {
        return $this->Lista;
    }

    public function getMonth()
    {
        return $this->Lista->getMonth();
    }

    public function getScaraData()
    {
        return $this->scaraData;
    }

    public function getColoaneVerticalSums()
    {
        $sums = [];

        foreach( $this->getColoane() as $coloanaId => $coloana )
        {
            // grupare subcoloane sumabile
            $subcoloaneSumabile = [];
            foreach( $coloana[ App_Component_Coloana_Object::SUBCOLOANE ] as $subcoloanaId => $subcoloana )
            {
                if( $subcoloana[ App_Component_Subcoloana_Object::IS_NUMERIC ] )
                {
                    $subcoloaneSumabile[ $subcoloanaId ] = $subcoloanaId;
                }
            }

            $coloanaHasValue = App_Component_Coloana_Factory::getInstance()->coloanaHasValue( $coloana );

            // insumare valoare coloana
            foreach( $this->scaraValues as $spatiuId => $spatiuValues )
            {
                if ( $coloanaHasValue )
                {
                    @$sums[ $coloanaId ][ App_Component_Lista_Object::VALUE ] += $spatiuValues[ $coloanaId ][ App_Component_Lista_Object::VALUE ];
                }

                foreach( $subcoloaneSumabile as $subcoloanaId )
                {
                    @$sums[ $coloanaId ][ App_Component_Coloana_Object::SUBCOLOANE ][ $subcoloanaId ][ App_Component_Lista_Object::VALUE ] += $spatiuValues[ $coloanaId ][ App_Component_Coloana_Object::SUBCOLOANE ][ $subcoloanaId ][ App_Component_Lista_Object::VALUE ];
                }
            }
        }

        return $sums;
    }


}