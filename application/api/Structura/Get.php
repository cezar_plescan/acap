<?php

/**
 * Input values: none
 * 
 * Output values: blocuri_data
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 */
class App_Api_Structura_Get extends App_Api_Authenticate_Abstract
{
    const ARG_BLOCURI           = 'blocuri';
    
    protected function initValidator()
    {
        return NULL;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $blocuri            = App_Component_Bloc_Factory::getInstance()->getStructura( $this->getAsociatieId() );
        
        $returnData = [
            self::ARG_BLOCURI           => $blocuri,
        ];
        
        return $returnData;
    }
    
}
