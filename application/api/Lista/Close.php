<?php

class App_Api_Lista_Close extends App_Api_Asociatie_WorkingMonth
{
    protected function _defineValidators()
    {
        return [];
    }

    public function onSuccess(array $filteredValues)
    {
        $asociatieId = $this->getAsociatieId();
        $month = $this->getWorkingMonth();

        App_Component_OutputLista_Factory::getInstance()->closeMonth( $asociatieId, $month );

        // salvez contributiile ....
        App_Component_Spatiu_Factory::getInstance()->saveContributiiCurente( $asociatieId, $month );

        // marchez incasarile ca fiind procesate
        App_Component_Incasare_Factory::getInstance()->setCounted( $asociatieId, $month );

    }

}
