<?php

class App_Api_Authenticate_Logout extends Lib_Api
{

    protected function initValidator()
    {
        $validator = new Lib_Validator_Pass();

        return $validator;
    }

    public function onSuccess( array $filteredValues = NULL )
    {
        Lib_Authentication_Manager::getWorker()->clearIdentity();
        
        // no data to return
    }

}
