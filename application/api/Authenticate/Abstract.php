<?php

/**
 * Abstract API class that checks the identity and the token.
 */
abstract class App_Api_Authenticate_Abstract extends Lib_Api
{
    // input values
    const ARG_TOKEN         = App_Component_Asociatie_Object::PROP_TOKEN;
    const ARG_CLIENT_TIME   = 'client_time';

    // errors
    const ERR_NOT_AUTHENTICATED = App_Component_Authentication_Validator_Identity::ERR_INVALID;
    const ERR_INVALID_TOKEN     = App_Component_Authentication_Validator_Token::ERR_INVALID;
    
    protected function beforeRun()
    {
        $tokenValidator = new App_Component_Authentication_Validator_Token();
        
        if( !$tokenValidator->isValid( @$this->arguments[ self::ARG_TOKEN ] ) )
        {
            $this->setErrors( $tokenValidator->getErrors() );
            
            return FALSE;
        }
        else
        {
            $clientTimeValidator = new Lib_Api_Validator_ClientTime();
            
            if( !$clientTimeValidator->isValid( @$this->arguments[ self::ARG_CLIENT_TIME ] ) )
            {
                $this->setErrors( $clientTimeValidator->getErrors() );
                
                return FALSE;
            }
            else
            {
                $this->arguments[ self::ARG_CLIENT_TIME ] = $clientTimeValidator->getFilteredValue();
            }
        }
    }

    /**
     * 
     * @return App_Component_Asociatie_Object
     */
    protected function getAsociatie()
    {
        return Lib_Authentication_Manager::getWorker()->getIdentity( true );
    }
    
    protected function getAsociatieId()
    {
        return Lib_Authentication_Manager::getWorker()->getIdentityId();
    }
    
    protected function getLunaCurenta()
    {
        return $this->getAsociatie()->getLunaCurenta();
    }
    
    protected function getClientTime()
    {
        return $this->getArgument( self::ARG_CLIENT_TIME );
    }
    
    
}
