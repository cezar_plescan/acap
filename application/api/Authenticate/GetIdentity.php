<?php

class App_Api_Authenticate_GetIdentity extends Lib_Api
{
    const ARG_ASOCIATIE_ID      = App_Component_Asociatie_Object::PROP_ID;
    const ARG_USERNAME          = App_Component_Asociatie_Object::PROP_USERNAME;

    protected function initValidator()
    {
        $validator = new App_Component_Authentication_Validator_Identity();

        return $validator;
    }

    public function onSuccess( array $filteredValues = NULL )
    {
        $Identity = Lib_Authentication_Manager::getWorker()->getIdentity();

        // prepare data for return
        $returnData = [
                self::ARG_ASOCIATIE_ID  => $Identity->getId(),
                self::ARG_USERNAME      => $Identity->getUsername(),
        ];

        return $returnData;
    }

}
