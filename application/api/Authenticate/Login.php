<?php

class App_Api_Authenticate_Login extends Lib_Api
{
    const ARG_ASOCIATIE_ID      = App_Component_Asociatie_Object::PROP_ID;
    const ARG_USERNAME          = App_Component_Asociatie_Object::PROP_USERNAME;
    const ARG_PASSWORD          = App_Component_Asociatie_Object::PROP_PASSWORD;
    const ARG_REMEMBER_ME       = 'remember_me';
    const ARG_TOKEN             = App_Component_Asociatie_Object::PROP_TOKEN;

    protected function initValidator()
    {
        $validator = new App_Component_Authentication_Validator_Authenticate();

        return $validator;
    }

    public function onSuccess( array $filteredValues = NULL )
    {
        $Authentication = Lib_Authentication_Manager::getWorker();
        $Authentication->storeIdentity( 
                $filteredValues[ self::ARG_ASOCIATIE_ID ],
                (bool)@$filteredValues[ self::ARG_REMEMBER_ME ]
        );
        
        // prepare data for return
        $returnData = [
                self::ARG_ASOCIATIE_ID  => $Authentication->getIdentityId(),
                self::ARG_TOKEN         => $Authentication->getToken(),
        ];

        return $returnData;
    }

}
