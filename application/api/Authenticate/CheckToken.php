<?php

class App_Api_Authenticate_CheckToken extends Lib_Api
{
    const ARG_TOKEN      = App_Component_Asociatie_Object::PROP_TOKEN;

    protected function initValidator()
    {
        $validator = new App_Component_Authentication_Validator_Token();

        return $validator;
    }

    public function onSuccess( array $filteredValues = NULL )
    {
        // no data to return
    }

}
