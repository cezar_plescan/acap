<?php

class App_Api_Consum_Add extends App_Api_Asociatie_WorkingMonth
{
    const ARG_SCARA     = 'scara';
    const ARG_SPATIU    = 'spatiu';

    protected function _defineValidators()
    {
        $consumuriSpatiiValidator  = new App_Component_Consum_Spatiu_Validator();
        $consumuriSpatiiValidator->setStopValidation();
        $consumuriSpatiiValidator->setOption( App_Component_Consum_Spatiu_Validator::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        $consumuriSpatiiValidator->setOption( App_Component_Consum_Spatiu_Validator::OPT_MONTH, $this->getWorkingMonth() );

        $consumuriScariValidator   = new App_Component_Consum_Scara_Validator();
        $consumuriScariValidator->setStopValidation();
        $consumuriScariValidator->setOption( App_Component_Consum_Scara_Validator::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        $consumuriScariValidator->setOption( App_Component_Consum_Scara_Validator::OPT_MONTH, $this->getWorkingMonth() );

        $validators = ([
            self::ARG_SCARA         => $consumuriScariValidator,
            self::ARG_SPATIU        => $consumuriSpatiiValidator,
        ]);

        return $validators;
    }

    public function onSuccess(array $filteredValues)
    {
        $consumuri = $this->convertValues($filteredValues);

        App_Component_Consum_Factory::getInstance()->add( $consumuri );
    }

    protected function convertValues(array $filteredValues)
    {
        $convertedValues = [];

        $map = [
            self::ARG_SCARA     => App_Component_Consum_Factory::REF_TYPE_SCARA,
            self::ARG_SPATIU    => App_Component_Consum_Factory::REF_TYPE_SPATIU,
        ];

        foreach( $map as $argument => $refType )
        {
            if ( isset( $filteredValues[ $argument ] ) && is_array( $filteredValues[ $argument ] ) )
            {
                foreach( $filteredValues[ $argument ] as $refId => $consumuri )
                {
                    foreach( $consumuri as $codConsum => $valoareConsum )
                    {
                        $convertedValues[] = [
                            App_Component_Consum_Object::REFERENCE_ID   => $refId,
                            App_Component_Consum_Object::REFERENCE_TYPE => $refType,
                            App_Component_Consum_Object::PARAMETRU      => $codConsum,
                            App_Component_Consum_Object::VALUE          => $valoareConsum,
                            App_Component_Consum_Object::MONTH          => $this->getWorkingMonth(),
                            App_Component_Consum_Object::_ASOCIATIE_ID  => $this->getAsociatieId(),
                        ];
                    }
                }
            }
        }

        return $convertedValues;
    }

}
