<?php

/**
 * Input values:
 * - ARG_DENUMIRE
 * - ARG_ADRESA
 * - ARG_BLOC_ID
 * 
 * Output values:
 * - ARG_DENUMIRE
 * - ARG_ADRESA
 * - ARG_SCARA_ID
 * - ARG_BLOC_ID
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_DENUMIRE_REQUIRED
 * - ERR_ADRESA_REQUIRED
 * - ERR_DENUMIRE_MAX_LENGTH
 * - ERR_DENUMIRE_DUPLICATE
 * - ERR_ADRESA_MAX_LENGTH
 * - ERR_MAX_ENTRIES
 * - ERR_MAX_OPERATIONS
 * 
 */
class App_Api_Scara_Add extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_DENUMIRE      = App_Component_Scara_Object::PROP_DENUMIRE;
    const ARG_ADRESA        = App_Component_Scara_Object::PROP_ADRESA;
    const ARG_BLOC_ID       = App_Component_Scara_Object::PROP_BLOC_ID;
    
    // output values
    const ARG_SCARA_ID      = App_Component_Scara_Object::PROP_ID;
    
    // errors
    const ERR_DENUMIRE_REQUIRED     = App_Component_Scara_Validator_Add::ERR_DENUMIRE_REQUIRED;
    const ERR_ADRESA_REQUIRED       = App_Component_Scara_Validator_Add::ERR_ADRESA_REQUIRED;
    const ERR_DENUMIRE_MAX_LENGTH   = App_Component_Scara_Validator_Add::ERR_DENUMIRE_MAX_LENGTH;
    const ERR_DENUMIRE_DUPLICATE    = App_Component_Scara_Validator_Add::ERR_DENUMIRE_DUPLICATE;
    const ERR_ADRESA_MAX_LENGTH     = App_Component_Scara_Validator_Add::ERR_ADRESA_MAX_LENGTH;
    const ERR_MAX_ENTRIES           = App_Component_Scara_Validator_Add::ERR_MAX_ENTRIES;
    const ERR_MAX_OPERATIONS        = App_Component_Scara_Validator_Add::ERR_MAX_OPERATIONS;
    const ERR_INVALID_BLOC          = App_Component_Scara_Validator_Add::ERR_INVALID_BLOC_ID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Scara_Validator_Add();
        $Validator->setOption( App_Component_Scara_Validator_Add::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array { ARG_SCARA_ID, ARG_DENUMIRE, ARG_ADRESA }
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $filteredValues[ App_Component_Scara_Object::PROP_ASOCIATIE_ID ] = $this->getAsociatieId();
        $filteredValues[ App_Component_Scara_Object::PROP_ADDING_MONTH ] = $this->getAsociatie()->getLunaCurenta();
        
        $Scara = App_Component_Scara_Factory::getInstance()->add( $filteredValues );
        
        $returnData = [ 
            self::ARG_BLOC_ID   => $Scara->getProperty( App_Component_Scara_Object::PROP_BLOC_ID ),
            self::ARG_SCARA_ID  => $Scara->getId(),
            self::ARG_DENUMIRE  => $Scara->getDenumire(),
            self::ARG_ADRESA    => $Scara->getAdresa(),
        ];
        
        return $returnData;
    }
    
}
