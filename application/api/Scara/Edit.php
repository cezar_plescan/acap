<?php

/**
 * Input values:
 * - ARG_DENUMIRE
 * - ARG_ADRESA
 * - ARG_SCARA_ID
 * 
 * Output values:
 * - ARG_DENUMIRE
 * - ARG_ADRESA
 * - ARG_SCARA_ID
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_DENUMIRE_REQUIRED
 * - ERR_ADRESA_REQUIRED
 * - ERR_DENUMIRE_MAX_LENGTH
 * - ERR_DENUMIRE_DUPLICATE
 * - ERR_ADRESA_MAX_LENGTH
 * - ERR_INVALID_SCARA
 * 
 */
class App_Api_Scara_Edit extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_DENUMIRE      = App_Component_Scara_Object::PROP_DENUMIRE;
    const ARG_ADRESA        = App_Component_Scara_Object::PROP_ADRESA;
    const ARG_SCARA_ID       = App_Component_Scara_Object::PROP_ID;
    
    // errors
    const ERR_DENUMIRE_REQUIRED     = App_Component_Scara_Validator_Edit::ERR_DENUMIRE_REQUIRED;
    const ERR_ADRESA_REQUIRED       = App_Component_Scara_Validator_Edit::ERR_ADRESA_REQUIRED;
    const ERR_DENUMIRE_MAX_LENGTH   = App_Component_Scara_Validator_Edit::ERR_DENUMIRE_MAX_LENGTH;
    const ERR_DENUMIRE_DUPLICATE    = App_Component_Scara_Validator_Edit::ERR_DENUMIRE_DUPLICATE;
    const ERR_ADRESA_MAX_LENGTH     = App_Component_Scara_Validator_Edit::ERR_ADRESA_MAX_LENGTH;
    const ERR_NO_ARGUMENTS          = App_Component_Scara_Validator_Edit::ERR_NO_VALUE;
    const ERR_INVALID_SCARA         = App_Component_Scara_Validator_Edit::ERR_INVALID_SCARA_ID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Scara_Validator_Edit();
        $Validator->setOption( App_Component_Scara_Validator_Edit::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array { ARG_SCARA_ID, ARG_DENUMIRE, ARG_ADRESA }
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Scara = App_Component_Scara_Factory::getInstance()->edit( $filteredValues );
        
        $returnData = [
            self::ARG_SCARA_ID  => $Scara->getId(),
        ];
        
        $this->populateDataIfKeyExists( $returnData, self::ARG_DENUMIRE, $filteredValues, function() use ( $Scara ) {
            return $Scara->getDenumire();
        });
        
        $this->populateDataIfKeyExists( $returnData, self::ARG_ADRESA, $filteredValues, function() use ( $Scara ) {
            return $Scara->getAdresa();
        });
        
        return $returnData;
    }
    
}
