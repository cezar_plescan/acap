<?php

/**
 * Input values:
 * - ARG_SCARA_ID
 * 
 * Output values:
 * - ARG_SCARA_ID
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_INVALID_SCARA
 * 
 */
class App_Api_Scara_Remove extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_SCARA_ID      = App_Component_Scara_Object::PROP_ID;
    
    // errors
    const ERR_INVALID_SCARA = App_Component_Scara_Validator_Remove::ERR_INVALID_SCARA_ID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Scara_Validator_Remove();
        $Validator->setOption( App_Component_Scara_Validator_Remove::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array { ARG_SCARA_ID }
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Scara = App_Component_Scara_Factory::getInstance()->remove( $filteredValues );
        
        $returnData = [
            self::ARG_SCARA_ID  => $Scara->getId(),
        ];
        
        return $returnData;
    }
    
}
