<?php

class App_Api_Cheltuiala_Remove extends App_Api_Authenticate_Abstract
{
    const ARG_CHELTUIALA_ID     = App_Component_Cheltuiala_Object::PROP_ID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Cheltuiala_Validator_Remove([
                App_Component_Cheltuiala_Validator_Remove::ARG_OPTIONS => [
                        App_Component_Cheltuiala_Validator_Remove::OPT_ASOCIATIE_ID => $this->getAsociatieId(),
                        App_Component_Cheltuiala_Validator_Remove::OPT_MONTH        => $this->getAsociatie()->getLunaCurenta(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        App_Component_Cheltuiala_Factory::getInstance()->remove( $filteredValues );
    }

}
