<?php

class App_Api_Cheltuiala_Add extends App_Api_Authenticate_Abstract
{
    const ARG_TIP_CHELTUIALA        = 'tip_id';
    const ARG_PARAMETRI_CHELTUIALA  = 'parametri';
    
    protected function initValidator()
    {
        $Validator = new App_Component_Cheltuiala_Validator_Add([
                App_Component_Cheltuiala_Validator_Add::ARG_OPTIONS => [
                        App_Component_Cheltuiala_Validator_Add::OPT_ASOCIATIE_ID    => $this->getAsociatieId(),
                        App_Component_Cheltuiala_Validator_Add::OPT_MONTH           => $this->getAsociatie()->getLunaCurenta(),
                        App_Component_Cheltuiala_Validator_Add::OPT_CLIENT_TIME     => $this->getClientTime(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Factory = App_Component_Cheltuiala_Factory::getInstance();

        $Factory->discardPending();
        $cheltuialaData = $Factory->add( $filteredValues );
        
        return $cheltuialaData;
    }

}
