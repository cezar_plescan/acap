<?php

class App_Api_Cheltuiala_Edit extends App_Api_Authenticate_Abstract
{
    const ARG_CHELTUIALA_ID         = 'id';
    const ARG_PARAMETRI_CHELTUIALA  = 'parametri';
    
    protected function initValidator()
    {
        $Validator = new App_Component_Cheltuiala_Validator_Edit([
                App_Component_Cheltuiala_Validator_Edit::ARG_OPTIONS => [
                        App_Component_Cheltuiala_Validator_Edit::OPT_ASOCIATIE_ID   => $this->getAsociatieId(),
                        App_Component_Cheltuiala_Validator_Edit::OPT_MONTH          => $this->getAsociatie()->getLunaCurenta(),
                        App_Component_Cheltuiala_Validator_Edit::OPT_CLIENT_TIME    => $this->getClientTime(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Factory = App_Component_Cheltuiala_Factory::getInstance();
        
        $cheltuialaData = $Factory->edit( $filteredValues );
        
        return $cheltuialaData;
    }

}
