<?php

class App_Api_Cheltuiala_Confirm extends App_Api_Authenticate_Abstract
{
    const ARG_CHELTUIALA_ID     = App_Component_Cheltuiala_Object::PROP_ID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Cheltuiala_Validator_Confirm([
                App_Component_Cheltuiala_Validator_Confirm::ARG_OPTIONS => [
                        App_Component_Cheltuiala_Validator_Confirm::OPT_ASOCIATIE_ID    => $this->getAsociatieId(),
                        App_Component_Cheltuiala_Validator_Confirm::OPT_MONTH           => $this->getAsociatie()->getLunaCurenta(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        App_Component_Cheltuiala_Factory::getInstance()->confirm( $filteredValues );
    }

}
