<?php

class App_Api_Cheltuiala_Get extends App_Api_Authenticate_Abstract
{
    const ARG_CURENTE       = 'cheltuieli_curente';
    const ARG_NEACHITATE    = 'cheltuieli_neachitate';
    const ARG_TIPURI        = 'tipuri';
    const ARG_CATEGORII     = 'categorii';
    
    protected function initValidator()
    {
        return NULL;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $asociatieId = $this->getAsociatieId();
        $currentMonth = $this->getLunaCurenta();
        
        $cheltuieliCurente = App_Component_Cheltuiala_Factory::getInstance()->getCheltuieliCurente( $asociatieId, $currentMonth );
        $categorii = App_Component_CheltuialaCategorie_Factory::getInstance()->getCategorii();
        $tipuri = App_Component_CheltuialaTip_Factory::getInstance()->getTipuri();
        
        $returnData = [
            self::ARG_CURENTE   => $cheltuieliCurente,
            self::ARG_TIPURI    => $tipuri,
            self::ARG_CATEGORII => $categorii,
        ];
        
        return $returnData;
    }
    
}
