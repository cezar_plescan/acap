<?php

/**
 * Input values:
 * - ARG_SPATIU_ID
 * - ARG_PROPRIETAR
 * - ARG_NR_PERS
 * - ARG_SUPRAFATA
 * 
 * Output values:
 * - ARG_SPATIU_ID
 * - ARG_ETAJ
 * - ARG_ETAJ_ORDER
 * - ARG_NUMAR
 * - ARG_PROPRIETAR
 * - ARG_NR_PERS
 * - ARG_SUPRAFATA
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_SPATIU_ID_INVALID
 * - ERR_NO_ARGUMENTS
 * - ERR_ETAJ_REQUIRED
 * - ERR_ETAJ_INVALID
 * - ERR_NUMAR_REQUIRED
 * - ERR_NUMAR_MAX_LENGTH
 * - ERR_NUMAR_DUPLICATE
 * - ERR_PROPRIETAR_REQUIRED
 * - ERR_PROPRIETAR_MAX_LENGTH
 * - ERR_NR_PERS_REQUIRED
 * - ERR_NR_PERS_INVALID
 * - ERR_SUPRAFATA_REQUIRED
 * - ERR_SUPRAFATA_INVALID
 * 
 */
class App_Api_Spatiu_Edit extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_SPATIU_ID     = App_Component_Spatiu_Object::PROP_ID;
    const ARG_PROPRIETAR    = App_Component_Spatiu_Object::PROP_PROPRIETAR;
    const ARG_NR_PERS       = App_Component_Spatiu_Object::PROP_NR_PERSOANE;
    const ARG_SUPRAFATA     = App_Component_Spatiu_Object::PROP_SUPRAFATA;
    
    // output values
    const ARG_ETAJ          = App_Component_Spatiu_Object::PROP_ETAJ;
    const ARG_NUMAR         = App_Component_Spatiu_Object::PROP_NUMAR;
    const ARG_ETAJ_ORDER    = App_Component_Spatiu_Object::PROP_ETAJ_ORDER;
    const ARG_BLOC_ID       = App_Component_Spatiu_Object::PROP_BLOC_ID;
    const ARG_SCARA_ID      = App_Component_Spatiu_Object::PROP_SCARA_ID;
    
    // errors
    const ERR_SPATIU_ID_INVALID     = App_Component_Spatiu_Validator_Edit::ERR_SPATIU_ID_INVALID;
    const ERR_NO_ARGUMENTS          = App_Component_Spatiu_Validator_Edit::ERR_NO_VALUE;
    const ERR_ETAJ_REQUIRED         = App_Component_Spatiu_Validator_Edit::ERR_ETAJ_REQUIRED;
    const ERR_ETAJ_INVALID          = App_Component_Spatiu_Validator_Edit::ERR_ETAJ_INVALID;
    const ERR_NUMAR_REQUIRED        = App_Component_Spatiu_Validator_Edit::ERR_NUMAR_REQUIRED;
    const ERR_NUMAR_MAX_LENGTH      = App_Component_Spatiu_Validator_Edit::ERR_NUMAR_MAX_LENGTH;
    const ERR_NUMAR_DUPLICATE       = App_Component_Spatiu_Validator_Edit::ERR_NUMAR_DUPLICATE;
    const ERR_PROPRIETAR_REQUIRED   = App_Component_Spatiu_Validator_Edit::ERR_PROPRIETAR_REQUIRED;
    const ERR_PROPRIETAR_MAX_LENGTH = App_Component_Spatiu_Validator_Edit::ERR_PROPRIETAR_MAX_LENGTH;
    const ERR_NR_PERS_REQUIRED      = App_Component_Spatiu_Validator_Edit::ERR_NR_PERS_REQUIRED;
    const ERR_NR_PERS_INVALID       = App_Component_Spatiu_Validator_Edit::ERR_NR_PERS_INVALID;
    const ERR_NR_PERS_MAX_VALUE     = App_Component_Spatiu_Validator_Edit::ERR_NR_PERS_MAX_VALUE;
    const ERR_SUPRAFATA_REQUIRED    = App_Component_Spatiu_Validator_Edit::ERR_SUPRAFATA_REQUIRED;
    const ERR_SUPRAFATA_INVALID     = App_Component_Spatiu_Validator_Edit::ERR_SUPRAFATA_INVALID;
    const ERR_SUPRAFATA_MAX_VALUE   = App_Component_Spatiu_Validator_Edit::ERR_SUPRAFATA_MAX_VALUE;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Spatiu_Validator_Edit();
        $Validator->setOption( App_Component_Spatiu_Validator_Edit::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        /* @var $Spatiu App_Component_Spatiu_Object */
        $Spatiu = App_Component_Spatiu_Factory::getInstance()->edit( $filteredValues );
        
        $returnData = [
            self::ARG_SPATIU_ID     => $Spatiu->getId(),
            self::ARG_BLOC_ID       => $Spatiu->getProperty( App_Component_Spatiu_Object::PROP_BLOC_ID ),
            self::ARG_SCARA_ID      => $Spatiu->getProperty( App_Component_Spatiu_Object::PROP_SCARA_ID ),
            self::ARG_ETAJ          => $Spatiu->getEtajLabel(),
            self::ARG_ETAJ_ORDER    => $Spatiu->getEtaj(),
            self::ARG_NUMAR         => $Spatiu->getNumar(),
        ];
        
        $returnData += $Spatiu->getParametri();
        
        return $returnData;
    }
    
}
