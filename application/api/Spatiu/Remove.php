<?php

/**
 * Input values:
 * - ARG_SPATIU_ID
 * 
 * Output values:
 * - ARG_SPATIU_ID
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_SPATIU_ID_INVALID
 * 
 */
class App_Api_Spatiu_Remove extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_SPATIU_ID     = App_Component_Spatiu_Object::PROP_ID;
    
    // errors
    const ERR_SPATIU_ID_INVALID     = App_Component_Spatiu_Validator_Remove::ERR_SPATIU_ID_INVALID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Spatiu_Validator_Remove();
        $Validator->setOption( App_Component_Spatiu_Validator_Remove::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Spatiu = App_Component_Spatiu_Factory::getInstance()->remove( $filteredValues );
        
        $returnData = [
            self::ARG_SPATIU_ID => $Spatiu->getId(),
        ];
        
        return $returnData;
    }
    
}
