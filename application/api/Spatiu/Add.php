<?php

/**
 * Input values:
 * - ARG_SCARA_ID
 * - ARG_ETAJ
 * - ARG_NUMAR
 * - ARG_PROPRIETAR
 * - ARG_NR_PERS
 * - ARG_SUPRAFATA
 * 
 * Output values:
 * - ARG_SPATIU_ID
 * - ARG_ETAJ
 * - ARG_ETAJ_ORDER
 * - ARG_NUMAR
 * - ARG_PROPRIETAR
 * - ARG_NR_PERS
 * - ARG_SUPRAFATA
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_SCARA_ID_INVALID
 * - ERR_COD_ETAJ_REQUIRED
 * - ERR_COD_ETAJ_INVALID
 * - ERR_NUMAR_REQUIRED
 * - ERR_NUMAR_MAX_LENGTH
 * - ERR_NUMAR_DUPLICATE
 * - ERR_PROPRIETAR_REQUIRED
 * - ERR_PROPRIETAR_MAX_LENGTH
 * - ERR_NR_PERS_REQUIRED
 * - ERR_NR_PERS_INVALID
 * - ERR_SUPRAFATA_REQUIRED
 * - ERR_SUPRAFATA_INVALID
 * - ERR_MAX_ENTRIES
 * - ERR_MAX_OPERATIONS
 * 
 */
class App_Api_Spatiu_Add extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_SCARA_ID      = App_Component_Spatiu_Object::PROP_SCARA_ID;
    const ARG_ETAJ          = App_Component_Spatiu_Object::PROP_ETAJ;
    const ARG_NUMAR         = App_Component_Spatiu_Object::PROP_NUMAR;
    const ARG_PROPRIETAR    = App_Component_Spatiu_Object::PROP_PROPRIETAR;
    const ARG_NR_PERS       = App_Component_Spatiu_Object::PROP_NR_PERSOANE;
    const ARG_SUPRAFATA     = App_Component_Spatiu_Object::PROP_SUPRAFATA;
    
    // output values
    const ARG_ETAJ_ORDER    = App_Component_Spatiu_Object::PROP_ETAJ_ORDER;
    const ARG_SPATIU_ID     = App_Component_Spatiu_Object::PROP_ID;
    const ARG_BLOC_ID       = App_Component_Spatiu_Object::PROP_BLOC_ID;
    
    // errors
    const ERR_SCARA_ID_INVALID      = App_Component_Spatiu_Validator_Add::ERR_SCARA_ID_INVALID;
    const ERR_ETAJ_REQUIRED         = App_Component_Spatiu_Validator_Add::ERR_ETAJ_REQUIRED;
    const ERR_ETAJ_INVALID          = App_Component_Spatiu_Validator_Add::ERR_ETAJ_INVALID;
    const ERR_NUMAR_REQUIRED        = App_Component_Spatiu_Validator_Add::ERR_NUMAR_REQUIRED;
    const ERR_NUMAR_MAX_LENGTH      = App_Component_Spatiu_Validator_Add::ERR_NUMAR_MAX_LENGTH;
    const ERR_NUMAR_DUPLICATE       = App_Component_Spatiu_Validator_Add::ERR_NUMAR_DUPLICATE;
    const ERR_PROPRIETAR_REQUIRED   = App_Component_Spatiu_Validator_Add::ERR_PROPRIETAR_REQUIRED;
    const ERR_PROPRIETAR_MAX_LENGTH = App_Component_Spatiu_Validator_Add::ERR_PROPRIETAR_MAX_LENGTH;
    const ERR_NR_PERS_REQUIRED      = App_Component_Spatiu_Validator_Add::ERR_NR_PERS_REQUIRED;
    const ERR_NR_PERS_INVALID       = App_Component_Spatiu_Validator_Add::ERR_NR_PERS_INVALID;
    const ERR_NR_PERS_MAX_VALUE     = App_Component_Spatiu_Validator_Add::ERR_NR_PERS_MAX_VALUE;
    const ERR_SUPRAFATA_REQUIRED    = App_Component_Spatiu_Validator_Add::ERR_SUPRAFATA_REQUIRED;
    const ERR_SUPRAFATA_INVALID     = App_Component_Spatiu_Validator_Add::ERR_SUPRAFATA_INVALID;
    const ERR_SUPRAFATA_MAX_VALUE   = App_Component_Spatiu_Validator_Add::ERR_SUPRAFATA_MAX_VALUE;
    const ERR_MAX_ENTRIES           = App_Component_Spatiu_Validator_Add::ERR_MAX_ENTRIES_PER_SCARA;
    const ERR_MAX_OPERATIONS        = App_Component_Spatiu_Validator_Add::ERR_MAX_OPERATIONS;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Spatiu_Validator_Add();
        $Validator->setOption( App_Component_Spatiu_Validator_Add::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $filteredValues[ App_Component_Spatiu_Object::PROP_ASOCIATIE_ID ] = $this->getAsociatieId();
        $filteredValues[ App_Component_Spatiu_Object::PROP_ADDING_MONTH ] = $this->getAsociatie()->getLunaCurenta();
        
        /* @var $Spatiu App_Component_Spatiu_Object */
        $Spatiu = App_Component_Spatiu_Factory::getInstance()->add( $filteredValues );
        
        $returnData = [
            self::ARG_SPATIU_ID     => $Spatiu->getId(),
            self::ARG_BLOC_ID       => $Spatiu->getProperty( App_Component_Spatiu_Object::PROP_BLOC_ID ),
            self::ARG_SCARA_ID      => $Spatiu->getProperty( App_Component_Spatiu_Object::PROP_SCARA_ID ),
            self::ARG_ETAJ          => $Spatiu->getEtajLabel(),
            self::ARG_ETAJ_ORDER    => $Spatiu->getEtaj(),
            self::ARG_NUMAR         => $Spatiu->getNumar(),
        ];
        
        $returnData += $Spatiu->getParametri();
        
        return $returnData;
    }
    
}
