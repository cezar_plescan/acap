<?php

class App_Api_Cheltuiala_Delete extends App_Api_Asociatie_WorkingMonth
{
    const ARG_CHELTUIALA_ID     = 'id';

    protected function _defineValidators()
    {
        $deleteValidator = new App_Component_Cheltuiala_Validator_Delete();
        $deleteValidator->setOption( App_Component_Cheltuiala_Validator_Delete::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        $deleteValidator->setOption( App_Component_Cheltuiala_Validator_Delete::OPT_WORKING_MONTH, $this->getWorkingMonth() );

        $validators = ([
            self::ARG_CHELTUIALA_ID => $deleteValidator,
        ]);

        return $validators;
    }

    public function onSuccess(array $filteredValues)
    {
        App_Component_Cheltuiala_Factory::getInstance()->deleteCheltuiala( $filteredValues[self::ARG_CHELTUIALA_ID ] );
    }

}
