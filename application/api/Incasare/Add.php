<?php

class App_Api_Incasare_Add extends App_Api_Asociatie_WorkingMonth
{
    const ARG_MONTH             = 'month';
    const ARG_COLOANA_ID        = 'coloana_id';
    const ARG_SPATIU_ID         = 'spatiu_id';
    const ARG_DATE              = 'date';
    const ARG_VALUE             = 'value';

    protected function _defineValidators()
    {
        $spatiuValidator = new App_Component_Spatiu_Validator_Id();
        $spatiuValidator
                ->setOption( App_Component_Spatiu_Validator_Id::OPT_ASOCIATIE_ID, $this->getAsociatieId() )
                ->setOption( App_Component_Spatiu_Validator_Id::OPT_MONTH, $this->getWorkingMonth() )
                ->setOption( App_Component_Spatiu_Validator_Id::OPT_MULTIPLE_ENTRIES, false );

        $valueValidator = new App_Component_Incasare_Validator_Value();

        $coloanaValidator = new App_Component_Coloana_Validator_ForIncasari();

        $dateValidator = new App_Component_Incasare_Validator_Date();

        $validators = ([
            self::ARG_SPATIU_ID     => $spatiuValidator,
            self::ARG_VALUE         => $valueValidator,
            self::ARG_COLOANA_ID    => $coloanaValidator,
            self::ARG_DATE          => $dateValidator,
        ]);

        return $validators;
    }

    public function onSuccess(array $filteredValues)
    {
        $incasareValues = [
            App_Component_Incasare_Object::ASOCIATIE_ID => $this->getAsociatieId(),
            App_Component_Incasare_Object::MONTH        => $this->getWorkingMonth(),
            App_Component_Incasare_Object::SPATIU_ID    => $filteredValues[ self::ARG_SPATIU_ID ],
            App_Component_Incasare_Object::VALUE        => $filteredValues[ self::ARG_VALUE ],
            App_Component_Incasare_Object::COLOANA_ID   => $filteredValues[ self::ARG_COLOANA_ID ],
            App_Component_Incasare_Object::DATE         => $filteredValues[ self::ARG_DATE ],
        ];

        App_Component_Incasare_Factory::getInstance()->add( $incasareValues );

    }

}
