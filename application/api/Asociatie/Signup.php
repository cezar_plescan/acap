<?php
class App_Api_Asociatie_Signup extends Lib_Api
{
    const ARG_PASSWORD          = App_Component_Asociatie_Object::PROP_PASSWORD;
    const ARG_EMAIL             = App_Component_Asociatie_Object::PROP_EMAIL;
    const ARG_COD_FISCAL        = App_Component_Asociatie_Object::PROP_COD_FISCAL;
    const ARG_DENUMIRE          = App_Component_Asociatie_Object::PROP_DENUMIRE;
    const ARG_ADRESA            = App_Component_Asociatie_Object::PROP_ADRESA;
    const ARG_LOCALITATE        = App_Component_Asociatie_Object::PROP_LOCALITATE;
    const ARG_JUDET             = App_Component_Asociatie_Object::PROP_JUDET;
    const ARG_TELEFON           = App_Component_Asociatie_Object::PROP_TELEFON;
    const ARG_CONFIRM_PASSWORD  = 'confirm_password';

    protected function _defineValidators()
    {
        $validators = [
            self::ARG_COD_FISCAL    => new App_Component_Asociatie_Validator_CodFiscal(),
            self::ARG_DENUMIRE      => new Lib_Validator_String(200),
            self::ARG_JUDET         => new Lib_Validator_String(60),
            self::ARG_LOCALITATE    => new Lib_Validator_String(60),
            self::ARG_ADRESA        => new Lib_Validator_String(200),
            self::ARG_TELEFON       => new Lib_Validator_Phone(),
            self::ARG_EMAIL         => new Lib_Validator_Email(),
            self::ARG_PASSWORD      => (new Lib_Validator_Password())->addAdditionalValue(Lib_Validator_Password::VAL_CONFIRM, $this->getArgument(self::ARG_CONFIRM_PASSWORD), self::ARG_CONFIRM_PASSWORD),
        ];

        return $validators;
    }

    public function onSuccess(array $filteredValues)
    {
        $userFactory = App_Component_Asociatie_Factory::getInstance();
        $Account = $userFactory->createAccount($filteredValues);

        return $this->checkResponse( $Account );
    }

}
