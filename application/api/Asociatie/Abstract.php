<?php

abstract class App_Api_Asociatie_Abstract extends Lib_Api
{
    const ARG_ASOCIATIE         = 'asociatie';

    protected
            /**
             * @var App_Component_Asociatie_Object
             */
            $Asociatie;

    public function __construct()
    {
        $this->setAsociatie( func_get_arg( 0 ) );
        
        parent::__construct();
    }

    protected function setAsociatie( App_Component_Asociatie_Object $Asociatie )
    {
        $this->Asociatie = $Asociatie;
    }

    protected function getAsociatie()
    {
        return $this->Asociatie;
    }

    protected function getAsociatieId()
    {
        return $this->Asociatie->getId();
    }

}
