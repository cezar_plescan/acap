<?php

abstract class App_Api_Asociatie_WorkingMonth extends App_Api_Asociatie_Abstract
{
    const ARG_MONTH = 'month';

    public function run(array $arguments)
    { // ???
        $mvs = $this->getMainValidatorSchema();
        $mvs->prependValidator( self::ARG_MONTH, new App_Component_Asociatie_Validator_WorkingMonth() );

        $arguments[ self::ARG_MONTH ] = $this->getWorkingMonth();

        return parent::run($arguments);
    }

    protected function getWorkingMonth()
    {
        return $this->getAsociatie()->getLunaCurenta();
    }

}
