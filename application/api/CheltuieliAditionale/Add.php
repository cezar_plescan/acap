<?php

class App_Api_CheltuieliAditionale_Add extends App_Api_Asociatie_WorkingMonth
{
    const ARG_SPATIU    = 'spatiu';

    protected function _defineValidators()
    {
        $validator  = new App_Component_Parametru_Spatiu_Validator_CheltuieliAditionale();
        $validator->setOption( App_Component_Parametru_Spatiu_Validator_CheltuieliAditionale::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        $validator->setOption( App_Component_Parametru_Spatiu_Validator_CheltuieliAditionale::OPT_MONTH, $this->getWorkingMonth() );

        $validators = ([
            self::ARG_SPATIU        => $validator,
        ]);

        return $validators;
    }

    public function onSuccess( array $filteredValues )
    {
        $values = $this->convertValues( $filteredValues );

        App_Component_SpatiuParametru_Factory::getInstance()->update( $values );
    }

    protected function convertValues( array $filteredValues )
    {
        $convertedValues = [];

        if( isset( $filteredValues[ self::ARG_SPATIU ] ) && is_array( $filteredValues[ self::ARG_SPATIU ] ) )
        {
            foreach( $filteredValues[ self::ARG_SPATIU ] as $spatiuId => $valori )
            {
                foreach( $valori as $codCheltuiala => $valoareCheltuiala )
                {
                    $convertedValues[] = [
                        App_Component_SpatiuParametru_Object::PROP_REF_ID         => $spatiuId,
                        App_Component_SpatiuParametru_Object::PROP_PARAMETRU      => $codCheltuiala,
                        App_Component_SpatiuParametru_Object::PROP_VALUE          => $valoareCheltuiala,
                        App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID  => $this->getAsociatieId(),
                    ];
                }
            }
        }

        return $convertedValues;
    }

}
