<?php

/**
 * Input values: none
 * 
 * Output values: next step
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 */
class App_Api_Configuration_NextInitStep extends App_Api_Authenticate_Abstract
{
    const ARG_STEP = 'step';
    
    protected function initValidator()
    {
        return NULL;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Configuration = $this->getAsociatie()->getConfiguration();
        $returnData = [
            self::ARG_STEP => $Configuration->nextInitStep(),
        ];
        
        return $returnData;
    }
    
}
