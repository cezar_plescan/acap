<?php

class App_Api_Configuratie_Validator_Add extends App_Component_Asociatie_Validator_BaseAbstract
{
    const ERR_CATEGORIE_INVALID     = 'categorie_invalid';
    const ERR_PARAMETRU_INVALID     = 'parametru_invalid';
    const ERR_VALUE_EMPTY           = 'value_empty';
    const ERR_OPERATION             = 'forbidden';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_CATEGORIE_INVALID     => 'Specificați categoria parametrului.',
            self::ERR_PARAMETRU_INVALID     => 'Specificați denumirea parametrului.',
            self::ERR_VALUE_EMPTY           => 'Specificați valoarea parametrului.',
            self::ERR_OPERATION             => 'Operaţia nu este permisă.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        // validat input value format - should be an array
        $this->validateInputFormat( $inputValues );

        $parametruId        = $this->validateCategorieParametru( $inputValues );
        $valoareParametru   = $this->validateValoare( $inputValues, $parametruId );
        
        if( !$this->hasErrors() )
        {
            $filteredValue = [
                App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $this->getAsociatieId(),
                App_Component_Configuratie_Object::PROP_PARAMETRU_ID    => $parametruId,
                App_Component_Configuratie_Object::PROP_VALUE           => $valoareParametru,
            ];

            return $filteredValue;
        }
    }
    
    protected function validateCategorieParametru( array $inputValues )
    {
        $filteredValue = FALSE;
        
        $categorieId = $this->validateCategorie( $inputValues );
        
        if( $categorieId !== FALSE )
        {
            $parametruId = $this->validateParametru( $inputValues, $categorieId );
            
            if( $parametruId !== FALSE )
            {
                $filteredValue = $parametruId;
            }
        }
        
        if( $filteredValue === FALSE )
        {
            $this->stopValidationIfErrors();
        }
        
        return $filteredValue;
    }

    protected function validateCategorie( array $inputValues )
    {
        if( !isset( $inputValues[ App_Api_Configuratie_Add::ARG_CATEGORIE ] ) )
        {
            $this->setError( self::ERR_CATEGORIE_INVALID );
        }
        
        $categorie = $inputValues[ App_Api_Configuratie_Add::ARG_CATEGORIE ];
        
        $filteredValue = FALSE;
        
        if( !$this->ifEmpty( $categorie, self::ERR_CATEGORIE_INVALID, true ) )
        {
            $categorieId = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieId( $categorie );

            if( !$this->ifEmpty( $categorieId, self::ERR_CATEGORIE_INVALID, true ) )
            {
                $filteredValue = $categorieId;
            }
        }
        
        return $filteredValue;
    }

    protected function validateParametru( array $inputValues, $categorieId )
    {
        if( !isset( $inputValues[ App_Api_Configuratie_Add::ARG_PARAMETRU ] ) )
        {
            $this->setError( self::ERR_PARAMETRU_INVALID );
        }
        
        $parametru = $inputValues[ App_Api_Configuratie_Add::ARG_PARAMETRU ];
        
        $filteredValue = FALSE;
        
        if( !$this->ifEmpty( $parametru, self::ERR_PARAMETRU_INVALID, true ) )
        {
            $parametruId = App_Component_ConfiguratieParametru_Factory::getInstance()->getParametruId( $parametru, $categorieId );
            
            if( !$this->ifEmpty( $parametruId, self::ERR_PARAMETRU_INVALID, true ) )
            {
                $isOperationAllowed = App_Component_ConfiguratieParametru_Factory::getInstance()->isOperationAllowed( $parametruId, App_Component_ConfiguratieParametru_Object::VAL_OPERATION_ADD );
                
                if( !$this->ifFalse( $isOperationAllowed, self::ERR_OPERATION, true ) )
                {
                    $filteredValue = $parametruId;
                }
            }
        }
        
        return $filteredValue;
    }
    
    protected function validateValoare( array $inputValues, $parametruId )
    {
        if( !isset( $inputValues[ App_Api_Configuratie_Add::ARG_VALOARE ] ) )
        {
            $this->setError( self::ERR_VALUE_EMPTY );
        }
        
        $value = $inputValues[ App_Api_Configuratie_Add::ARG_VALOARE ];
        
        $Validator = App_Component_ConfiguratieParametru_Validator_Factory::getInstance()->getAddValidator( $parametruId, [
            App_Component_ConfiguratieParametru_Validator_AddAbstract::ARG_OPTIONS => [
                App_Component_ConfiguratieParametru_Validator_AddAbstract::OPT_ASOCIATIE_ID => $this->getAsociatieId(),
            ]
        ]);

        $filteredValue = $this->attachValidator( $Validator, $value );
        
        return $filteredValue;
    }
    
}
