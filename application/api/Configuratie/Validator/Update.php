<?php

class App_Api_Configuratie_Validator_Update extends App_Component_Asociatie_Validator_WorkingMonth
{
    const ERR_CATEGORIE_INVALID     = 'categotie_invalid';
    const ERR_PARAMETRU_INVALID     = 'parametru_invalid';
    const ERR_VALUES_MISSING        = 'values_empty';
    const ERR_OPERATION             = 'operation_forbidden';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_CATEGORIE_INVALID     => 'Specificați categoria parametrului.',
            self::ERR_PARAMETRU_INVALID     => 'Specificați denumirea parametrului.',
            self::ERR_VALUES_MISSING        => 'Specificați valorile parametrului.',
            self::ERR_OPERATION             => 'Operaţia nu este permisă.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        // validat input value format - should be an array
        $this->validateInputFormat( $inputValues );

        $parametruId    = $this->validateCategorieParametru( $inputValues );
        $valori         = $this->validateValori( $inputValues, $parametruId );
        
        if( !$this->hasErrors() )
        {
            $filteredValue = [
                App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $this->getAsociatieId(),
                App_Component_Configuratie_Object::PROP_PARAMETRU_ID    => $parametruId,
                App_Component_Configuratie_Object::VALUES               => $valori,
            ];

            return $filteredValue;
        }
    }
    
    protected function validateCategorieParametru( array $inputValues )
    {
        $filteredValue = FALSE;
        
        $categorieId = $this->validateCategorie( $inputValues );
        
        if( $categorieId !== FALSE )
        {
            $parametruId = $this->validateParametru( $inputValues, $categorieId );
            
            if( $parametruId !== FALSE )
            {
                $filteredValue = $parametruId;
            }
        }
        
        if( $filteredValue === FALSE )
        {
            $this->stopValidationIfErrors();
        }
        
        return $filteredValue;
    }

    protected function validateCategorie( array $inputValues )
    {
        if( !isset( $inputValues[ App_Api_Configuratie_Update::ARG_CATEGORIE ] ) )
        {
            $this->setError( self::ERR_CATEGORIE_INVALID );
        }
        
        $filteredValue = FALSE;
        $categorie = $inputValues[ App_Api_Configuratie_Update::ARG_CATEGORIE ];
        
        if( !$this->ifEmpty( $categorie, self::ERR_CATEGORIE_INVALID, true ) )
        {
            $categorieId = App_Component_ConfiguratieCategorie_Factory::getInstance()->getCategorieId( $categorie );

            if( !$this->ifEmpty( $categorieId, self::ERR_CATEGORIE_INVALID, true ) )
            {
                $filteredValue = $categorieId;
            }
        }
        
        return $filteredValue;
    }

    protected function validateParametru( array $inputValues, $categorieId )
    {
        if( !isset( $inputValues[ App_Api_Configuratie_Update::ARG_PARAMETRU ] ) )
        {
            $this->setError( self::ERR_PARAMETRU_INVALID );
        }
        
        $filteredValue = FALSE;
        $parametru = $inputValues[ App_Api_Configuratie_Update::ARG_PARAMETRU ];
        
        if( !$this->ifEmpty( $parametru, self::ERR_PARAMETRU_INVALID, true ) )
        {
            $parametruId = App_Component_ConfiguratieParametru_Factory::getInstance()->getParametruId( $parametru, $categorieId );
            
            if( !$this->ifEmpty( $parametruId, self::ERR_PARAMETRU_INVALID, true ) )
            {
                $isOperationAllowed = App_Component_ConfiguratieParametru_Factory::getInstance()->isOperationAllowed( $parametruId, App_Component_ConfiguratieParametru_Object::VAL_OPERATION_UPDATE_ALL );
                
                if( !$this->ifFalse( $isOperationAllowed, self::ERR_OPERATION, true ) )
                {
                    $filteredValue = $parametruId;
                }
            }
        }
        
        return $filteredValue;
    }
    
    protected function validateValori( array $inputValues, $parametruId )
    {
        $values = isset( $inputValues[ App_Api_Configuratie_Update::ARG_VALORI ] ) ?
                $inputValues[ App_Api_Configuratie_Update::ARG_VALORI ] :
                NULL;
        
        $Validator = App_Component_ConfiguratieParametru_Validator_Factory::getInstance()->getUpdateValidator( $parametruId, [
            App_Component_ConfiguratieParametru_Validator_UpdateAbstract::ARG_OPTIONS => [
                App_Component_ConfiguratieParametru_Validator_UpdateAbstract::OPT_ASOCIATIE_ID  => $this->getAsociatieId(),
                App_Component_ConfiguratieParametru_Validator_UpdateAbstract::OPT_MONTH         => $this->getMonth(),
            ]
        ]);

        $filteredValue = $this->attachValidator( $Validator, $values );


        return $filteredValue;
    }
    
}
