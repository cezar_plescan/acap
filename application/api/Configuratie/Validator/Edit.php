<?php

class App_Api_Configuratie_Validator_Edit extends App_Component_Asociatie_Validator_WorkingMonth
{
    const ERR_CONFIG_ID_INVALID     = 'config_id_invalid';
    const ERR_VALUE_EMPTY           = 'value_empty';
    const ERR_OPERATION             = 'forbidden';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_CONFIG_ID_INVALID     => 'Specificaţi ID-ul parametrului de configurare.',
            self::ERR_VALUE_EMPTY           => 'Specificaţi valoarea parametrului de configurare.',
            self::ERR_OPERATION             => 'Operaţia nu este permisă.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        // validate input value format - should be an array
        $this->validateInputFormat( $inputValues );

        $configuratieId         = $this->validateConfiguratieId( $inputValues );
        $valoareConfiguratie    = $this->validateValoare( $inputValues, $configuratieId );
        
        if( !$this->hasErrors() )
        {
            $filteredValue = [
                App_Component_Configuratie_Object::PROP_ID      => $configuratieId,
                App_Component_Configuratie_Object::PROP_VALUE   => $valoareConfiguratie,
            ];

            return $filteredValue;
        }
    }
    
    protected function validateConfiguratieId( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Api_Configuratie_Edit::ARG_CONFIGURATIE_ID, self::ERR_CONFIG_ID_INVALID, true );
        
        $configId = $inputValues[ App_Api_Configuratie_Edit::ARG_CONFIGURATIE_ID ];
        
        $this->checkIfValid( $configId );
        $this->checkIfOperationAllowed( $configId );
        
        return $configId;
    }
    
    protected function checkIfValid( $configId )
    {
        $valid = (bool)App_Component_Configuratie_Factory::getInstance()->countActive([
            App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $this->getAsociatieId(),
            App_Component_Configuratie_Object::PROP_ID              => $configId,
        ] + 
            App_Component_Configuratie_Object::getActiveStatusCriteria() );
        
        return $valid;
    }

    protected function checkIfOperationAllowed( $configId )
    {
        $parametruId = $this->getParametruId( $configId );
        $isOperationAllowed = App_Component_ConfiguratieParametru_Factory::getInstance()->isOperationAllowed( $parametruId, App_Component_ConfiguratieParametru_Object::VAL_OPERATION_EDIT );
        
        return $isOperationAllowed;
    }

    protected function getParametruId( $configId )
    {
        $parametruId = App_Component_Configuratie_Factory::getInstance()->retrieveValue( $configId, App_Component_Configuratie_Object::PROP_PARAMETRU_ID );
        
        return $parametruId;
    }
    
    protected function validateValoare( array $inputValues, $configId )
    {
        $this->ifKeyNotSet( $inputValues, App_Api_Configuratie_Edit::ARG_VALOARE, self::ERR_VALUE_EMPTY, TRUE );
        
        $configValues = $inputValues[ App_Api_Configuratie_Add::ARG_VALOARE ];
        $configValues[ App_Component_Configuratie_Object::PROP_ID ] = $configId;
        
        $parametruId = $this->getParametruId( $configId );
        
        $Validator = App_Component_ConfiguratieParametru_Validator_Factory::getInstance()->getEditValidator( $parametruId, [
            App_Component_ConfiguratieParametru_Validator_EditAbstract::ARG_OPTIONS => [
                App_Component_ConfiguratieParametru_Validator_EditAbstract::OPT_ASOCIATIE_ID    => $this->getAsociatieId(),
                App_Component_ConfiguratieParametru_Validator_EditAbstract::OPT_MONTH           => $this->getMonth(),
            ]
        ]);

        $filteredValue = $this->attachValidator( $Validator, $configValues );
        
        return $filteredValue;
    }
    
}
