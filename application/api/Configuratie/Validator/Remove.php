<?php

class App_Api_Configuratie_Validator_Remove extends App_Component_Asociatie_Validator_WorkingMonth
{
    const ERR_CONFIGURATION_ID      = 'configuration_id_invalid';
    const ERR_OPERATION             = 'forbidden';
    
    protected function init()
    {
        parent::init();
        
        $this->addMessageTemplates([
            self::ERR_CONFIGURATION_ID      => 'Specificați ID-ul parametrului.',
            self::ERR_OPERATION             => 'Operaţia nu este permisă.',
        ]);
    }
    
    protected function validate( $inputValues )
    {
        // validat input value format - should be an array
        $this->validateInputFormat( $inputValues );

        $configuratieId = $this->validateConfiguratieId( $inputValues );
        $this->validateConfiguratieRemove( $configuratieId );
                
        if( !$this->hasErrors() )
        {
            $filteredValue = [
                App_Component_Configuratie_Object::PROP_ID => $configuratieId,
            ];

            return $filteredValue;
        }
    }
    
    protected function validateConfiguratieId( array $inputValues )
    {
        $this->ifKeyNotSet( $inputValues, App_Api_Configuratie_Remove::ARG_CONFIGURATIE_ID, self::ERR_CONFIGURATION_ID, TRUE );
        
        $configuratieId = $inputValues[ App_Api_Configuratie_Remove::ARG_CONFIGURATIE_ID ];
        
        $id = FALSE;
        
        if( !$this->ifEmpty( $configuratieId, self::ERR_CONFIGURATION_ID ) )
        {
            $conditions = [
                App_Component_Configuratie_Object::PROP_ID              => $configuratieId,
                App_Component_Configuratie_Object::PROP_ASOCIATIE_ID    => $this->getAsociatieId(),
            ];

            $configuratieExists = boolval( App_Component_Configuratie_Factory::getInstance()->countActive( $conditions ) );
            
            if( !$this->ifEmpty( $configuratieExists, self::ERR_CONFIGURATION_ID ) )
            {
                $id = $configuratieId;
            }
        }
        
        return $id;
    }
    
    protected function validateConfiguratieRemove( $configuratieId )
    {
        $parametruId = App_Component_Configuratie_Factory::getInstance()->getParametruId( $configuratieId );
        
        $isOperationAllowed = App_Component_ConfiguratieParametru_Factory::getInstance()->isOperationAllowed( $parametruId, App_Component_ConfiguratieParametru_Object::VAL_OPERATION_DELETE );
        $this->ifFalse( $isOperationAllowed, self::ERR_OPERATION, true );
        
        $Validator = App_Component_ConfiguratieParametru_Validator_Factory::getInstance()->getRemoveValidator( $parametruId, [
            Lib_Validator_Abstract::ARG_OPTIONS => [
                App_Component_ConfiguratieParametru_Validator_RemoveAbstract::OPT_ASOCIATIE_ID  => $this->getAsociatieId(),
                App_Component_ConfiguratieParametru_Validator_RemoveAbstract::OPT_MONTH         => $this->getMonth(),
            ]
        ]);
        
        $this->attachValidator( $Validator, $configuratieId );
    }

}
