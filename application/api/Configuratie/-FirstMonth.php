<?php

class App_Api_Configuration_FirstMonth extends App_Api_Authenticate_Abstract
{
    const ARG_MONTH = App_Component_Configuratie_Object::ARG_MONTH;
    const ARG_YEAR  = App_Component_Configuratie_Object::ARG_YEAR;
    
    protected function initValidator()
    {
        return new App_Component_Configuration_Validator_FirstMonth();
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Asociatie = $this->getAsociatie();
        $Asociatie->getConfiguration()->setFirstMonth( $filteredValues );
        
        return $filteredValues;
    }
    
}
