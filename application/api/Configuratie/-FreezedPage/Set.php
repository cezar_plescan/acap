<?php

class App_Api_Configuration_FreezedPage_Set extends App_Api_Authenticate_Abstract
{
    const ARG_PAGE  = 'page';
    
    protected function initValidator()
    {
        return NULL;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Asociatie = $this->getAsociatie();
        $Asociatie->getConfiguration()->setFreezedPage( trim( $filteredValues[ self::ARG_PAGE ] ) );
    }
    
}
