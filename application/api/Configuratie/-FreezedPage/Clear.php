<?php

class App_Api_Configuration_FreezedPage_Clear extends App_Api_Authenticate_Abstract
{
    protected function initValidator()
    {
        return NULL;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Asociatie = $this->getAsociatie();
        $Asociatie->getConfiguration()->clearFreezedPage();
    }
    
}
