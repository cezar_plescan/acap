<?php

class App_Api_Configuratie_Add extends App_Api_Authenticate_Abstract
{
    const ARG_CATEGORIE     = 'categorie';
    const ARG_PARAMETRU     = 'parametru';
    const ARG_VALOARE       = 'valoare';
    
    protected function initValidator()
    {
        $Validator = new App_Api_Configuratie_Validator_Add([
                App_Api_Configuratie_Validator_Add::ARG_OPTIONS => [
                        App_Api_Configuratie_Validator_Add::OPT_ASOCIATIE_ID     => $this->getAsociatieId(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Factory = App_Component_Configuratie_Factory::getInstance();
        
        $configurationParameterData = $Factory->add( $filteredValues );
        
        return $configurationParameterData;
    }

}
