<?php

class App_Api_Configuratie_Edit extends App_Api_Authenticate_Abstract
{
    const ARG_CONFIGURATIE_ID   = 'config_id';
    const ARG_VALOARE           = 'valoare';
    
    protected function initValidator()
    {
        $Validator = new App_Api_Configuratie_Validator_Edit([
                App_Api_Configuratie_Validator_Edit::ARG_OPTIONS => [
                        App_Api_Configuratie_Validator_Edit::OPT_ASOCIATIE_ID   => $this->getAsociatieId(),
                        App_Api_Configuratie_Validator_Edit::OPT_MONTH          => $this->getLunaCurenta(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Factory = App_Component_Configuratie_Factory::getInstance();
        
        $configurationParameterData = $Factory->edit( $filteredValues );
        
        return $configurationParameterData;
    }

}
