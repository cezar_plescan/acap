<?php

class App_Api_Configuratie_Update extends App_Api_Authenticate_Abstract
{
    const ARG_CATEGORIE     = 'categorie';
    const ARG_PARAMETRU     = 'parametru';
    const ARG_VALORI        = 'valori';
    
    protected function initValidator()
    {
        $Validator = new App_Api_Configuratie_Validator_Update([
                Lib_Validator_Abstract::ARG_OPTIONS => [
                        App_Api_Configuratie_Validator_Update::OPT_ASOCIATIE_ID => $this->getAsociatieId(),
                        App_Api_Configuratie_Validator_Update::OPT_MONTH        => $this->getLunaCurenta(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Factory = App_Component_Configuratie_Factory::getInstance();
        
        $configurationParameterData = $Factory->update( $filteredValues );
        
        return $configurationParameterData;
    }

}
