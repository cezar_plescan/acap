<?php

/**
 * Input values: none
 * 
 * Output values: configuration
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 */
class App_Api_Configuratie_Get extends App_Api_Authenticate_Abstract
{
    protected function initValidator()
    {
        return NULL;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $returnData = App_Component_Configuratie_Factory::getInstance()->getData( $this->getAsociatieId() );
        
        return $returnData;
    }
    
}
