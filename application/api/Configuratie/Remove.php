<?php

class App_Api_Configuratie_Remove extends App_Api_Authenticate_Abstract
{
    const ARG_CONFIGURATIE_ID  = 'config_id';
    
    protected function initValidator()
    {
        $Validator = new App_Api_Configuratie_Validator_Remove([
                App_Api_Configuratie_Validator_Remove::ARG_OPTIONS => [
                        App_Api_Configuratie_Validator_Remove::OPT_ASOCIATIE_ID   => $this->getAsociatieId(),
                        App_Api_Configuratie_Validator_Remove::OPT_MONTH          => $this->getLunaCurenta(),
                ]
        ]);
        
        return $Validator;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Factory = App_Component_Configuratie_Factory::getInstance();
        
        $Factory->remove( $filteredValues );
    }

}
