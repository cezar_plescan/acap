<?php

/**
 * Input values:
 * - ARG_BLOC_ID
 * - ARG_DENUMIRE
 * 
 * Output values
 * - ARG_BLOC_ID
 * - ARG_DENUMIRE
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_DENUMIRE_REQUIRED
 * - ERR_DENUMIRE_MAX_LENGTH
 * - ERR_DENUMIRE_DUPLICATE
 * - ERR_INVALID_BLOC_ID
 */
class App_Api_Bloc_Edit extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_DENUMIRE      = App_Component_Bloc_Object::PROP_DENUMIRE;
    const ARG_BLOC_ID       = App_Component_Bloc_Object::PROP_ID;
    
    // errors
    const ERR_DENUMIRE_REQUIRED     = App_Component_Bloc_Validator_Edit::ERR_DENUMIRE_REQUIRED;
    const ERR_DENUMIRE_MAX_LENGTH   = App_Component_Bloc_Validator_Edit::ERR_DENUMIRE_MAX_LENGTH;
    const ERR_DENUMIRE_DUPLICATE    = App_Component_Bloc_Validator_Edit::ERR_DENUMIRE_DUPLICATE;
    const ERR_INVALID_BLOC_ID       = App_Component_Bloc_Validator_Edit::ERR_INVALID_BLOC_ID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Bloc_Validator_Edit();
        $Validator->setOption( App_Component_Bloc_Validator_Edit::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array { ARG_BLOC_ID, ARG_DENUMIRE }
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Bloc = App_Component_Bloc_Factory::getInstance()->edit( $filteredValues );
        
        $returnData = [ 
            self::ARG_BLOC_ID   => $Bloc->getId(),
            self::ARG_DENUMIRE  => $Bloc->getDenumire(),
        ];
        
        return $returnData;
    }
    
}
