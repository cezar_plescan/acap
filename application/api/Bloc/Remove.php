<?php

/**
 * Input values:
 * - ARG_BLOC_ID
 * 
 * Output values:
 * - ARG_BLOC_ID
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_INVALID_BLOC_ID
 */
class App_Api_Bloc_Remove extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_BLOC_ID       = App_Component_Bloc_Object::PROP_ID;
    
    // errors
    const ERR_INVALID_BLOC_ID   = App_Component_Bloc_Validator_Remove::ERR_INVALID_BLOC_ID;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Bloc_Validator_Remove();
        $Validator->setOption( App_Component_Bloc_Validator_Remove::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array { ARG_BLOC_ID }
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        $Bloc = App_Component_Bloc_Factory::getInstance()->remove( $filteredValues );
        
        $returnData = [
            self::ARG_BLOC_ID   => $Bloc->getId(),
        ];
        
        return $returnData;
    }
    
}
