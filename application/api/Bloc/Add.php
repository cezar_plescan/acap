<?php

/**
 * Input values:
 * - ARG_DENUMIRE
 * 
 * Output values:
 * - ARG_DENUMIRE
 * - ARG_BLOC_ID
 * 
 * Errors:
 * - ERR_NOT_AUTHENTICATED
 * - ERR_INVALID_TOKEN
 * - ERR_DENUMIRE_REQUIRED
 * - ERR_DENUMIRE_MAX_LENGTH
 * - ERR_DENUMIRE_DUPLICATE
 * - ERR_MAX_ENTRIES
 * - ERR_MAX_OPERATIONS
 * 
 */
class App_Api_Bloc_Add extends App_Api_Authenticate_Abstract
{
    // input values
    const ARG_DENUMIRE      = App_Component_Bloc_Object::PROP_DENUMIRE;
    
    // output values
    const ARG_BLOC_ID       = App_Component_Bloc_Object::PROP_ID;
    
    // errors
    const ERR_DENUMIRE_REQUIRED     = App_Component_Bloc_Validator_Add::ERR_DENUMIRE_REQUIRED;
    const ERR_DENUMIRE_MAX_LENGTH   = App_Component_Bloc_Validator_Add::ERR_DENUMIRE_MAX_LENGTH;
    const ERR_DENUMIRE_DUPLICATE    = App_Component_Bloc_Validator_Add::ERR_DENUMIRE_DUPLICATE;
    const ERR_MAX_ENTRIES           = App_Component_Bloc_Validator_Add::ERR_MAX_ENTRIES;
    const ERR_MAX_OPERATIONS        = App_Component_Bloc_Validator_Add::ERR_MAX_OPERATIONS;
    
    protected function initValidator()
    {
        $Validator = new App_Component_Bloc_Validator_Add();
        $Validator->setOption( App_Component_Bloc_Validator_Add::OPT_ASOCIATIE_ID, $this->getAsociatieId() );
        
        return $Validator;
    }
    
    /**
     * 
     * @param array $filteredValues
     * @return array { ARG_BLOC_ID, ARG_DENUMIRE }
     */
    public function onSuccess( array $filteredValues = NULL )
    {
        // ensure that the values are corectly passed to the Factory method
        $blocValues = [
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID     => $this->getAsociatieId(),
            App_Component_Bloc_Object::PROP_ADDING_MONTH     => $this->getAsociatie()->getLunaCurenta(),
            App_Component_Bloc_Object::PROP_DENUMIRE         => $filteredValues[ self::ARG_DENUMIRE ],
        ];
        
        $Bloc = App_Component_Bloc_Factory::getInstance()->add( $blocValues );
        
        $returnData = [ 
            self::ARG_BLOC_ID   => $Bloc->getId(),
            self::ARG_DENUMIRE  => $Bloc->getDenumire(),
        ];

        return $returnData;
    }

    /**
     * @return explanatory text in HTML format
     */
    static public function getNotes()
    {
        $filePath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'notes.phtml';
        
        $notes = Lib_Tools::getHtmlView( $filePath );
        
        return $notes;
    }
    
}
