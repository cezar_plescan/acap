<?php

class App_Api_Fond_Get extends App_Api_Authenticate_Abstract
{
    const ARG_FONDURI       = 'fonduri';
    const ARG_TIPURI_FOND   = 'tipuri_fond';
    
    protected function initValidator()
    {
        return NULL;
    }
    
    public function onSuccess( array $filteredValues = NULL )
    {
        $Asociatie = $this->getAsociatie();
        
        $asociatieId = $Asociatie->getId();
        $currentMonth = $Asociatie->getLunaCurenta();
        
        $tipuri     = App_Component_FondTip_Factory::getInstance()->getTipuri();
        $fonduri    = App_Component_Fond_Factory::getInstance()->getFonduri( $asociatieId, $currentMonth );
        
        $returnData = [
            self::ARG_FONDURI       => $fonduri,
            self::ARG_TIPURI_FOND   => $tipuri,
        ];
        
        return $returnData;
    }
    
}
