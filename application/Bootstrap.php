<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initComponentsPath()
    {
        $loader = $this->getResourceLoader();

        $loader->addResourceTypes(array(
            'component' => [
                'namespace' => 'Component',
                'path'      => 'components',
            ],
            'api' => [
                'namespace' => 'Api',
                'path'      => 'api',
            ],
        ));
    }

   
}
