<?php

class Default_View_Helper_Cheltuieli extends Zend_View_Helper_Abstract
{
    /**
     *
     * @return array
     */
    public function cheltuieli()
    {
        /* @var $Asociatie App_Component_Asociatie_Object */
        $Asociatie = Lib_Application::getInstance()->getUser();

        $cheltuieli = App_Component_Cheltuiala_Factory::getInstance()->getAll( $Asociatie->getId(), $Asociatie->getLunaCurenta() );

        return $cheltuieli;
    }

}

