<?php

class Default_View_Helper_FormatValue extends Zend_View_Helper_Abstract
{
    public function formatValue($value, $isNumeric = true, $showEndingZeros = true, $precision = 2)
    {
        if ( $isNumeric )
        {
            $value = round($value, $precision);

            if ( $showEndingZeros )
            {
                $value = number_format($value, $precision, '.', '');
            }
        }
        else
        {
            $value = trim( $value );
        }

        return $this->view->escape( $value );
    }

}

