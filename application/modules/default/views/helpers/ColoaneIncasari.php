<?php

class Default_View_Helper_ColoaneIncasari extends Zend_View_Helper_Abstract
{
    public function coloaneIncasari()
    {
        $coloaneIncasari = App_Component_Coloana_Factory::getInstance()->getForIncasari();

        return $coloaneIncasari;
    }

}

