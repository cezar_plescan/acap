<?php

class Default_View_Helper_LunaDeLucru extends Zend_View_Helper_Abstract
{
    public function lunaDeLucru()
    {
        $month = Lib_Application::getInstance()->getUser()->getLunaCurenta();
        $text = '';

        if ( $month )
        {
            $text = $this->view->displayMonth( $month );
        }

        return $text;
    }

}

