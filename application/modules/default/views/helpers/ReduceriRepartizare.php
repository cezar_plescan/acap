<?php

class Default_View_Helper_ReduceriRepartizare extends Zend_View_Helper_Abstract
{
    /**
     *
     * @return array
     */
    public function reduceriRepartizare()
    {
        /* @var $Asociatie App_Component_Asociatie_Object */
        $Asociatie = Lib_Application::getInstance()->getUser();

        $exceptii = App_Component_ReducereRepartizare_Factory::getInstance()->getAll( $Asociatie->getId() );

        return $exceptii;
    }

}

