<?php

class Default_View_Helper_ColoaneCheltuieli extends Zend_View_Helper_Abstract
{
    public function coloaneCheltuieli()
    {
        $coloaneCheltuieli = App_Component_Coloana_Factory::getInstance()->getForCheltuieli();

        return $coloaneCheltuieli;
    }

}

