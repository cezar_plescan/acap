<?php

class Default_View_Helper_FormatDate extends Zend_View_Helper_Abstract
{

    public function formatDate($date, $includeYear = true, $includeHours = true)
    {
        $time = strtotime($date);
        $dateInfo = getdate($time);

        $formattedDate =
                Lib_Date::getDayOfWeekLabel( $dateInfo['wday'] ) . ', ' . $dateInfo['mday'] . ' ' . Lib_Date::getMonthLabel( $dateInfo['mon'] ) .
                ( $includeYear ? ' ' . $dateInfo['year'] : '' ) .
                ( $includeHours ? ', ' . str_pad($dateInfo['hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($dateInfo['minutes'], 2, '0', STR_PAD_LEFT) : '');

        return $this->view->escape($formattedDate);
    }

}

