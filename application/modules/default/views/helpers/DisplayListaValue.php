<?php

class Default_View_Helper_DisplayListaValue extends Zend_View_Helper_Abstract
{
    public function displayListaValue( $spatiuColoanaData, $isNumeric = true, $showEndingZeros = true)
    {
        $value = @$spatiuColoanaData[ App_Component_Lista_Object::VALUE ];
        $displayValue = @$spatiuColoanaData[ App_Component_Lista_Object::DISPLAY_VALUE ];

        if ( $displayValue === null )
        {
            $displayValue = $value;
        }

        $return = $this->view->formatValue( $displayValue, $isNumeric, $showEndingZeros );

        return $return;
    }

}

