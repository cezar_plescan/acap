<?php

class Default_View_Helper_Spatii extends Zend_View_Helper_Abstract
{
    /**
     *
     * @return array
     */
    public function spatii()
    {
        /* @var $Asociatie App_Component_Asociatie_Object */
        $Asociatie = Lib_Application::getInstance()->getUser();

        $spatii = App_Component_Spatiu_Factory::getInstance()->getAll( $Asociatie->getId(), $Asociatie->getLunaCurenta() );

        return $spatii;
    }

}

