<?php

class Default_View_Helper_Scari extends Zend_View_Helper_Abstract
{
    /**
     *
     * @return array
     */
    public function scari()
    {
        /* @var $Asociatie App_Component_Asociatie_Object */
        $Asociatie = Lib_Application::getInstance()->getUser();

        $scari = App_Component_Scara_Factory::getInstance()->getAll( $Asociatie->getId(), $Asociatie->getLunaCurenta() );

        return $scari;
    }

}

