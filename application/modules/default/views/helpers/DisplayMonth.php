<?php

class Default_View_Helper_DisplayMonth extends Zend_View_Helper_Abstract
{

    public function displayMonth( $monthId )
    {
        $monthText = $this->view->asociatie()->getMonthName( $monthId );

        return $this->view->escape( $monthText );
    }

}

