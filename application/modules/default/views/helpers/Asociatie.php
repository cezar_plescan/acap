<?php

class Default_View_Helper_Asociatie extends Zend_View_Helper_Abstract
{
    /**
     *
     * @return App_Component_Asociatie_Object
     */
    public function asociatie()
    {
        $Asociatie = Lib_Authentication_Manager::getWorker()->getIdentity();

        return $Asociatie;
    }

}

