<?php

class Default_View_Helper_Blocuri extends Zend_View_Helper_Abstract
{
    /** (v)
     *
     * @return array
     */
    public function blocuri()
    {
        /* @var $Asociatie App_Component_Asociatie_Object */
        $Asociatie = Lib_Application::getInstance()->getUser();

        $blocuri = App_Component_Bloc_Factory::getInstance()->getAll( $Asociatie->getId(), $Asociatie->getLunaCurenta() );

        return $blocuri;
    }

}

