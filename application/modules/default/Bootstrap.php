<?php

class Default_Bootstrap extends Lib_Module_Bootstrap_Abstract
{
    protected function _initComponentsPath()
    {
        $loader = $this->getBootstrap()->getResourceLoader();

        $loader->addResourceTypes(array(
            'controller_plugins' => [
                'namespace' => 'Controller_Plugin',
                'path'      => 'modules/default/controllers/plugins',
            ],
        ));
    }

    protected function _initActionHelperPath()
    {
        Zend_Controller_Action_HelperBroker::addPath( $this->getControllerDirectory() . '/helpers', 'Helper' );
    }

    protected function _initDispatchManager()
    {
        $plugin = new App_Controller_Plugin_DispatchManager();
        Zend_Controller_Front::getInstance()->registerPlugin( $plugin );
    }
    
}