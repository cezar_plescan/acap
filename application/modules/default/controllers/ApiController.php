<?php

class ApiController extends Lib_Controller_Api
{
    /**
     * Performs the authetication
     * 
     * @param App_Api_Authenticate::ARG_USERNAME
     * @param App_Api_Authenticate::ARG_PASSWORD
     * @param App_Api_Authenticate::ARG_REMEMBER_ME (optional)
     * 
     * @return Array A successful response containing as data an array of type { App_Api_Authenticate::ARG_ASOCIATIE_ID, App_Api_Authenticate::ARG_TOKEN }
     * 
     * @tested
     */
    public function loginAction()
    {
        $this->linkToApi( 'App_Api_Authenticate_Login' );
    }

    /**
     * Returns the user identity
     * 
     * @no-params
     * @return Array { App_Api_Asociatie_Authenticate::ARG_ASOCIATIE_ID, App_Api_Asociatie_Authenticate::ARG_USERNAME }
     * 
     * @tested
     */
    public function getIdentityAction()
    {
        $this->linkToApi( 'App_Api_Authenticate_GetIdentity' );
    }
    
    /**
     * Logout the current user
     * 
     * @no-params
     * @return Array A successful response
     * 
     * @tested
     */
    public function logoutAction()
    {
        $this->linkToApi( 'App_Api_Authenticate_Logout' );
    }

    /**
     * @param Lib_Api::ARG_TOKEN
     * @return Array If token is valid, return a succesful response; else, return error
     * 
     * @under-testing
     */
    public function checkTokenAction()
    {
        $this->linkToApi( 'App_Api_Authenticate_CheckToken' );
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @tested
     * 
     * @arguments { App_Api_Bloc_Add::ARG_DENUMIRE }
     * @return Array { App_Api_Bloc_Add::ARG_DENUMIRE, App_Api_Bloc_Add::ARG_BLOC_ID }
     */
    public function addBlocAction()
    {
        $this->linkToApi( 'App_Api_Bloc_Add' );
    }
    
    /**
     * @tested
     * 
     * @arguments { App_Api_Bloc_Edit::ARG_BLOC_ID, App_Api_Bloc_Edit::ARG_DENUMIRE }
     * @return Array { App_Api_Bloc_Edit::ARG_DENUMIRE, App_Api_Bloc_Edit::ARG_BLOC_ID }
     */
    public function editBlocAction()
    {
        $this->linkToApi( 'App_Api_Bloc_Edit' );
    }
    
    /**
     * @tested
     * 
     * @arguments { App_Api_Bloc_Remove::ARG_BLOC_ID }
     * @return Array { App_Api_Bloc_Remove::ARG_BLOC_ID }
     */
    public function removeBlocAction()
    {
        $this->linkToApi( 'App_Api_Bloc_Remove' );
    }

    //--------------------------------------------------------------------------
    
    /**
     * 
     * @arguments { App_Api_Scara_Add::ARG_DENUMIRE, App_Api_Scara_Add::ARG_ADRESA, App_Api_Scara_Add::ARG_BLOC_ID }
     * @return Array { App_Api_Scara_Add::ARG_DENUMIRE, App_Api_Scara_Add::ARG_SCARA_ID }
     */
    public function addScaraAction()
    {
        $this->linkToApi( 'App_Api_Scara_Add' );
    }
    
    /**
     * 
     * @arguments { App_Api_Scara_Edit::ARG_SCARA_ID, App_Api_Scara_Edit::ARG_DENUMIRE, App_Api_Scara_Edit::ARG_ADRESA }
     * @return Array { App_Api_Scara_Edit::ARG_DENUMIRE, App_Api_Scara_Edit::ARG_ADRESA, App_Api_Scara_Edit::ARG_SCARA_ID }
     */
    public function editScaraAction()
    {
        $this->linkToApi( 'App_Api_Scara_Edit' );
    }
    
    /**
     * 
     * @arguments { App_Api_Scara_Remove::ARG_SCARA_ID }
     * @return Array { App_Api_Scara_Remove::ARG_SCARA_ID }
     */
    public function removeScaraAction()
    {
        $this->linkToApi( 'App_Api_Scara_Remove' );
    }

    //--------------------------------------------------------------------------
    
    public function addSpatiuAction()
    {
        $this->linkToApi( 'App_Api_Spatiu_Add' );
    }
    
    public function editSpatiuAction()
    {
        $this->linkToApi( 'App_Api_Spatiu_Edit' );
    }
    
    public function removeSpatiuAction()
    {
        $this->linkToApi( 'App_Api_Spatiu_Remove' );
    }

    //--------------------------------------------------------------------------
    
    public function getStructuraAction()
    {
        $this->linkToApi( 'App_Api_Structura_Get' );
    }
    
    //--------------------------------------------------------------------------
    
    public function getConfigAction()
    {
        $this->linkToApi( 'App_Api_Configuratie_Get' );
    }
    
    public function addConfigAction()
    {
        $this->linkToApi( 'App_Api_Configuratie_Add' );
    }
    
    public function editConfigAction()
    {
        $this->linkToApi( 'App_Api_Configuratie_Edit' );
    }
    
    public function removeConfigAction()
    {
        $this->linkToApi( 'App_Api_Configuratie_Remove' );
    }
    
    public function updateConfigAction()
    {
        $this->linkToApi( 'App_Api_Configuratie_Update' );
    }
    
//    public function nextInitStepAction()
//    {
//        $this->linkToApi( 'App_Api_Configuration_NextInitStep' );
//    }
//    
//    public function setFreezedPageAction()
//    {
//        $this->linkToApi( 'App_Api_Configuration_FreezedPage_Set' );
//    }
//    
//    public function clearFreezedPageAction()
//    {
//        $this->linkToApi( 'App_Api_Configuration_FreezedPage_Clear' );
//    }
//    
//    public function setFirstMonthAction()
//    {
//        $this->linkToApi( 'App_Api_Configuration_FirstMonth' );
//    }
//    
    //--------------------------------------------------------------------------
    
    public function getCheltuieliAction()
    {
        $this->linkToApi( 'App_Api_Cheltuiala_Get' );
    }
    
    public function addCheltuialaAction()
    {
        $this->linkToApi( 'App_Api_Cheltuiala_Add' );
    }

    public function confirmCheltuialaAction()
    {
        $this->linkToApi( 'App_Api_Cheltuiala_Confirm' );
    }

    public function removeCheltuialaAction()
    {
        $this->linkToApi( 'App_Api_Cheltuiala_Remove' );
    }

    public function editCheltuialaAction()
    {
        $this->linkToApi( 'App_Api_Cheltuiala_Edit' );
    }

    //--------------------------------------------------------------------------
    
    public function getFonduriAction()
    {
        $this->linkToApi( 'App_Api_Fond_Get' );
    }

    //--------------------------------------------------------------------------
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function signupAction()
    {
        $params = $this->getRequestParams();

        $api = new App_Api_Asociatie_Signup();
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function deleteCheltuialaAction()
    {
        $this->checkToken();

        $params = $this->getRequestParams();

        $api = new App_Api_Cheltuiala_Delete( $this->getAsociatie() );
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function addConsumAction()
    {
        $this->checkToken();

        $params = $this->getRequestParams();

        $api = new App_Api_Consum_Add( $this->getAsociatie() );
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function addCheltuieliAditionaleAction()
    {
        $this->checkToken();

        $params = $this->getRequestParams();

        $api = new App_Api_CheltuieliAditionale_Add( $this->getAsociatie() );
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function updateSpatiiAction()
    {
        $this->checkToken();

        $params = $this->getRequestParams();

        $api = new App_Api_Spatii_Update( $this->getAsociatie() );
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function closeMonthAction()
    {
        $this->checkToken();

        $params = $this->getRequestParams();

        $api = new App_Api_Lista_Close( $this->getAsociatie() );
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function getListaAction()
    {
        $this->checkToken();

        $Lista = new App_Component_Lista_Object( $this->getAsociatie()->getId(), $this->getAsociatie()->getLunaCurenta() );

        /* @todo: this can be written:
         * 
         * $Lista->compute();
         * if( $Lista->hasErrors() ) { ... }
         * else { ... }
         * 
         */
        
        $operation = $Lista->compute();
        if ( $operation !== OPERATION_ERROR )
        {
            $this->view->Lista = $Lista;

            $output = $this->view->render('lista/compute.phtml');

            $Lista->saveOutput( $output );

            $this->sendSuccessResponse( $output );
        }
        else
        {
            $error = $Lista->getComputeError();

            $this->sendErrorResponse( $error );
        }

    }

    public function addIncasareAction()
    {
        $this->checkToken();

        $params = $this->getRequestParams();

        $api = new App_Api_Incasare_Add( $this->getAsociatie() );
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function deleteIncasareAction()
    {
        $this->checkToken();

        $params = $this->getRequestParams();

        $api = new App_Api_Incasare_Delete( $this->getAsociatie() );
        $api->run( $params );

        $this->sendApiResponse($api);
    }

    public function addSpatiiAction()
    {
        $this->linkToAsociatieApi( 'App_Api_Spatii_Add' );
    }
    
}

