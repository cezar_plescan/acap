<?php

class App_Controller_Plugin_DispatchManager extends Zend_Controller_Plugin_Abstract
{
    protected $unaffectedControllers = [ 'signup', 'api' ];
    
    public function dispatchLoopStartup( Zend_Controller_Request_Abstract $request )
    {
        $controllerName = $request->getControllerName();
        
        if( in_array( $controllerName, $this->unaffectedControllers ) )
        {
            // do nothing
        }
        else
        {
            // check for authentication
            if( $this->identityExists() )
            {
                $request->setControllerName( 'application' );
                $request->setActionName( 'index' );
            }
            else
            {
                $request->setControllerName( 'landing' );
                $request->setActionName( 'index' );
            }
        }
        
    }
    
    protected function identityExists()
    {
        $identityExists = Lib_Authentication_Manager::getWorker()->hasIdentity();
        
        return $identityExists;
    }
    
}
