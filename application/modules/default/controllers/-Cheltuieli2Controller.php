<?php

require_once '-Private.php';

class Cheltuieli2Controller extends PrivateBaseController
{
    public function indexAction()
    {
        if ( $this->_helper->workingMonthForbidden() )
        {
            $this->_forward('month-forbidden', 'error');
        }

    }

}