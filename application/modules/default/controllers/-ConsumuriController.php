<?php

require_once '-Private.php';

class ConsumuriController extends PrivateBaseController
{
    public function indexAction()
    {
        if ( $this->_helper->workingMonthUnavailable() )
        {
            $this->_forward('month-unavailable', 'error');
        }
        elseif ( $this->_helper->workingMonthForbidden() )
        {
            $this->_forward('month-forbidden', 'error');
        }

    }

}