<?php

class Helper_WorkingMonthUnavailable extends Zend_Controller_Action_Helper_Abstract
{
    public function direct()
    {
        $controller = $this->getActionController();

        $workingMonth = $controller->getIdentity()->getLunaCurenta();
        
        $unavailable = ( $workingMonth == null );

        return $unavailable;
    }

}
