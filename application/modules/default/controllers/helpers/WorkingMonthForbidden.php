<?php

class Helper_WorkingMonthForbidden extends Zend_Controller_Action_Helper_Abstract
{
    public function direct()
    {
    $controller = $this->getActionController();

        $month = $controller->getIdentity()->getLunaCurenta();
        $currentMonth = Lib_Date::getInstance()->getCurrentMonthId();

        $forbidden = ( $month > $currentMonth );

        return $forbidden;
    }

}
