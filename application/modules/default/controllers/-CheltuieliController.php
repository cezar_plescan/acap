<?php

require_once '-Private.php';

class CheltuieliController extends PrivateBaseController
{
    public function indexAction()
    {
        if ( $this->_helper->workingMonthForbidden() )
        {
            $this->_forward('month-forbidden', 'error');
        }

    }

    public function listAction()
    {
        $this->_helper->Layout->disableLayout();
    }

}