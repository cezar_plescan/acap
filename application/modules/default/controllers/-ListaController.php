<?php

require_once '-Private.php';

class ListaController extends PrivateBaseController
{
    public function indexAction()
    {
        if ( $this->_helper->workingMonthUnavailable() )
        {
            $this->_forward('month-unavailable', 'error');
        }
        elseif ( $this->_helper->workingMonthForbidden() )
        {
            $this->_forward('month-forbidden', 'error');
        }
        else
        {
        ////////////////////////////////////////////////////////////////////////

            $asociatieId = $this->getAsociatieId();

            $workingMonth = $this->_helper->workingMonth();

            //// determin numarul de cheltuieli

            $cheltuieliCount = App_Component_Cheltuiala_Factory::getInstance()->getCheltuieliCount( $asociatieId, $workingMonth );

            $this->view->cheltuieli_count = $cheltuieliCount;

            //// extrag listele calculate

            $closedMonths = App_Component_OutputLista_Factory::getInstance()->getClosedMonthIds( $asociatieId );

            $this->view->closed_months = $closedMonths;
        }
    }

    public function viewAction()
    {
        $requestParams = $this->getRequestParams();
        $month = @$requestParams['m'];

        $asociatieId = $this->getAsociatieId();

        if ( null === $month )
        {
            $month = App_Component_OutputLista_Factory::getInstance()->getLastClosedMonthId( $asociatieId );
        }

        // try to get PDF content
        $pdfContent = App_Component_OutputLista_Factory::getInstance()->getPdfContent( $asociatieId, $month );
        if( $pdfContent )
        {
            $this->sendPdfOutput( $pdfContent, 'lista_' + $month . '.pdf' );
        }
        else
        {
            $content = App_Component_OutputLista_Factory::getInstance()->getContent( $asociatieId, $month );
            if ( strlen( $content ) )
            {
                $this->_helper->Layout->setLayout( 'printer-canvas' );
                $this->_helper->ViewRenderer->setNoRender();

                $this->getResponse()
                    ->setHeader( 'Content-Type', 'text/html' )
                    ->setBody( $content );
            }
            else
            {
                $this->_redirect( 'index' );
            }
        }

    }

    public function sendPdfOutput( $output, $filename )
    {
        // set HTTP response headers
        header("Content-Type: application/pdf");
        header("Cache-Control: no-cache");
        header("Accept-Ranges: none");
        header("Content-Disposition: inline; filename=\"$filename\"");
//        header("Content-Disposition: attachment; filename=\"$filename\"");

        // send the generated PDF
        echo $output;
        die();
    }


}