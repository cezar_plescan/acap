<?php

class ApplicationController extends Lib_Controller_View
{

    public function indexAction()
    {
        $this->view->token = Lib_Authentication_Manager::getWorker()->getToken();
        $this->view->tipuri_cheltuieli = App_Component_CheltuialaTip_Factory::getInstance()->getTipuri();
        $this->view->categorii_cheltuieli = App_Component_CheltuialaCategorie_Factory::getInstance()->getCategorii();
    }
    
}
