<?php

defined( 'PHP_UNIT' )
    || define( 'PHP_UNIT', true );

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

defined( 'APPLICATION_ENV' )
    || define( 'APPLICATION_ENV', 'unit_testing' );

require_once APPLICATION_PATH . '/../library/Lib/Application.php';

// Ensure tests/ is on include_path
//set_include_path(implode(PATH_SEPARATOR, array(
//    realpath(APPLICATION_PATH . '/../tests'),
//    get_include_path(),
//)));

// Create application and bootstrap
Lib_Application::getInstance();


$autoloader = new Zend_Loader_Autoloader_Resource(array(
    'namespace'   => 'Test_',
    'basePath' => dirname(__FILE__),
    'resourceTypes' => [
        'application'   => [
            'namespace' => 'Application',
            'path'      => 'Application'
        ],
        'library'   => [
            'namespace' => 'Library',
            'path'      => 'Library'
        ],
    ]
));
Zend_Loader_Autoloader::getInstance()->pushAutoloader( $autoloader );