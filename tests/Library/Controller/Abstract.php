<?php

abstract class Test_Library_Controller_Abstract extends Zend_Test_PHPUnit_ControllerTestCase
{
    static protected $baseUrl = '';
    
    static private $executedTests = [];
    
    public static function setUpBeforeClass()
    {
        $className = get_called_class();
        echo "\n\033[0;34m> $className\033[0m";
        
        parent::setUpBeforeClass();
    }
    
    public function run( \PHPUnit_Framework_TestResult $result = null )
    {
        $className = get_called_class();
        
        if( empty( self::$executedTests[ $className ] ) )
        {
            $this->__runBeforeAllTests();
        }
        
        $testName = $this->getTestName();
        if( empty( self::$executedTests[ $className ][ $testName ] ) )
        {
            $displayName = substr( $testName, 5 );
            echo "\n\033[0;33m$displayName\033[0m";
        }
        
        parent::run( $result );
    }
    
    /**
     * @overridable
     * @hook-method
     */
    protected function __runBeforeAllTests() {}
    
    protected function setUp()
    {
        Lib_Authentication_Manager::getWorker()->clearIdentity();
        parent::setUp();
    }
    
    public function reset()
    {
        parent::reset();
        
        Lib_Application::close();
        
        $this->bootstrap = Lib_Application::getInstance();
    }
    
    protected function tearDown()
    {
        parent::tearDown();
        
        $className = get_called_class();
        self::$executedTests[ $className ][ $this->getTestName() ] = true;
    }
    
    protected function getTestName()
    {
        return $this->getName( false );
    }

    protected function doRequest( $action, array $params = [], $method = 'POST' )
    {
        $this->resetApplication();
        
        $request = $this->getRequest();
        
        $request
                ->setMethod( $method )
                ->setParams( $params );
        
        $this->dispatch( static::$baseUrl . $action );
        
    }
    
    /**
     * Reset state, but restore Session and Cookie
     */
    protected function resetApplication()
    {
        $storeSession   = $_SESSION;
        $storeCookie    = $_COOKIE;
        
        // clear all static instances
        $this->clearStaticInstances();
        
        $this->reset();
        
        $_SESSION   = $storeSession;
        $_COOKIE    = $storeCookie;
        
    }
    
    protected function clearStaticInstances()
    {
        $declaredClasses = get_declared_classes();
        
        foreach ( $declaredClasses as $className )
        {
            // skip PHPUnit classes
            if( strpos( $className, 'PHPUnit_' ) === 0 ) continue;
            
            // unset $instance static property, if exists
            try {
                $Property = new ReflectionProperty( $className, 'instance' );
                $Property->setAccessible( true );
                $Property->setValue( NULL );
            }
            catch ( ReflectionException $e ){};
        }
        
    }

}