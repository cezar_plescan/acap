<?php

abstract class Test_Application_Api_Controller extends Test_Library_Controller_Abstract
{
    static protected $baseUrl = '/api/';
    
    static protected $loginAction = 'login';
    static protected $logoutAction = 'logout';

    static protected $asociatieData = [
        // ID => [ username, password ]
        1   => [ '123456',     '123456' ],
        2   => [ 'test',       '123456' ],
        3   => [ '5317515',    '5317515' ],
    ];

    protected 
            $responseData,
            $_token;
    
    /**
     * 
     * @return Array [ ID => { username, password } ]
     */
    static protected function getAsociatiiCredentials()
    {
        $credentialsSet = [];
        
        foreach( self::$asociatieData as $id => $credentials )
        {
            $credentialsSet[ $id ] = [
                App_Api_Authenticate_Login::ARG_USERNAME     => $credentials[ 0 ],
                App_Api_Authenticate_Login::ARG_PASSWORD     => $credentials[ 1 ],
            ];
        }
        
        return $credentialsSet;
    }

    protected function doRequest( $action, array $params = [], $method = 'POST' )
    {
        parent::doRequest( $action, $params, $method );
        
        // check if the response is a JSON
        $this->responseData = $this->checkResponseFormat( $this->getResponse()->getBody() );
    }
    
    protected function doRequestWithToken( $token, $action, array $params = [], $method = 'POST' )
    {
        $params = array_merge( $params, [ App_Component_Asociatie_Object::PROP_TOKEN => $token ] );
        
        $this->doRequest( $action, $params, $method );
    }
    
    protected function getResponseData()
    {
        return $this->responseData;
    }
    
    protected function getToken()
    {
        return $this->_token;
    }
    
    protected function doLogin( array $credentials )
    {
        $this->doRequest( self::$loginAction, $credentials );
        
        // read token
        $responseData = $this->getResponseData();
        $this->_token = @$responseData[ Lib_Api::RESPONSE_DATA ][ App_Component_Authentication_Worker::TOKEN ];
    }
    
    protected function doLogout()
    {
        $this->doRequest( self::$logoutAction );
        $this->_token = NULL;
    }
    
    /**
     * Check if the output is valid JSON.
     * 
     * @param string $output
     * @return array The decoded value
     */
    protected function checkResponseFormat( $output )
    {
        $responseData = json_decode( $output, true );
        
        // write output to file
        //file_put_contents( realpath( APPLICATION_PATH . DIRECTORY_SEPARATOR . '..' .DIRECTORY_SEPARATOR . 'tests' . DIRECTORY_SEPARATOR . 'output' ) , $output, FILE_APPEND );
        
        $this->assertNotNull( $responseData, "The response is not a JSON: \n" . $output . "\n" );

        return $responseData;
    }

    private function assertResponseHasSuccessFlag()
    {
        $responseData = $this->getResponseData();
        
        // check for successful response
        $this->assertInternalType( 'array', $responseData, 'The response is not an array.' );
        $this->assertArrayHasKey( Lib_Api::RESPONSE_SUCCESS, $responseData, 'The response does not contain the `success` flag.' );
        
        return $responseData;
    }
    
    protected function assertResponseIsSuccessful( $message = null )
    {
        $responseData = $this->assertResponseHasSuccessFlag();
        
        $this->assertNotEmpty( $responseData[ Lib_Api::RESPONSE_SUCCESS ], 
                ( $message === null ) ? 
                    'Response is not successful' . 
                            ( isset( $responseData[ Lib_Api::RESPONSE_ERRORS ] ) ? 
                                ":\n" . print_r( $responseData[ Lib_Api::RESPONSE_ERRORS ], true ) :
                                '.'
                            ) : 
                    $message 
        );
        
        return $responseData;
    }
    
    protected function assertResponseIsNotSuccessful( $message = null )
    {
        $responseData = $this->assertResponseHasSuccessFlag();
        
        $this->assertEmpty( $responseData[ Lib_Api::RESPONSE_SUCCESS ], $message === null ? 'Response is successful.' : $message );
        
        return $responseData;
    }
    
    protected function assertResponseIsSuccessfulWithoutDataKey( $message = null )
    {
        $responseData = $this->assertResponseIsSuccessful();
        
        $this->assertArrayNotHasKey( Lib_Api::RESPONSE_DATA, $responseData, $message === null ? 'The response does contain the `data` key.' : $message );
        
        return $responseData;
    }
    
    protected function assertResponseIsSuccessfulWithDataKey( $message = null )
    {
        $responseData = $this->assertResponseIsSuccessful();
        
        $this->assertArrayHasKey( Lib_Api::RESPONSE_DATA, $responseData, $message === null ? 'The response does not contain the `data` key.' : $message );
        
        return $responseData;
    }
    
    protected function assertResponseHasErrors( $errorCodes )
    {
        $responseData = $this->getResponseData();
        
        $expectedErrorCodes = (array)$errorCodes;
        
        $this->assertInternalType( 'array', $responseData, 'The response is not an array.' );
        $this->assertArrayHasKey( Lib_Api::RESPONSE_SUCCESS, $responseData, 'The response does not contain the `success` flag.' );
        $this->assertEmpty( $responseData[ Lib_Api::RESPONSE_SUCCESS ], 'Response is successful. Expected errors are: ' . implode( ', ', $expectedErrorCodes ) . '.' );
        $this->assertArrayHasKey( Lib_Api::RESPONSE_ERRORS, $responseData, 'The response does not contain the `errors` key.' );
        $this->assertInternalType( 'array', $responseData[ Lib_Api::RESPONSE_ERRORS ], 'The `errors` value is not an array.' );
        
        // read returned errors
        $returnedErrors = $responseData[ Lib_Api::RESPONSE_ERRORS ];
        $returnedErrorCodes = [];
        foreach ( $returnedErrors as $error )
        {
            $returnedErrorCodes[] = $error[ Lib_Api::ERROR_CODE ];
        }
        
        sort( $returnedErrorCodes );
        sort( $expectedErrorCodes );
        
        $this->assertEquals( $expectedErrorCodes, $returnedErrorCodes, 'The expected error'. ( count( $expectedErrorCodes ) > 1 ? 's were' : ' was' ) .' not retuned.' );
        
        return $responseData;
    }
    
    static protected function addData( array &$dataSet, $arg1 )
    {
        if( $dataSet === null )
        {
            $dataSet = [];
        }
        
        $argumentsList = array_slice( func_get_args(), 1 );
        
        $dataSet[] = $argumentsList;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * 
     * @param string $username
     * @return App_Component_Asociatie_Object
     */
    protected function getAsociatieByUsername( $username )
    {
        $Asociatie = App_Component_Asociatie_Factory::getInstance()->findByUsername( $username );
         
        return $Asociatie;
    }
    
    /**
     * 
     * @param int $id
     * @return App_Component_Asociatie_Object
     */
    protected function getAsociatieById( $id )
    {
        $Asociatie = App_Component_Asociatie_Factory::getInstance()->find( $id );
         
        return $Asociatie;
    }
    
    /**
     * 
     * @param array $credentials
     * @return App_Component_Asociatie_Object
     */
    protected function getAsociatieFromCredentials( array $credentials )
    {
        $Asociatie = $this->getAsociatieByUsername( $credentials[ App_Component_Asociatie_Object::PROP_USERNAME ]);
        
        return $Asociatie;
    }
    
    /**
     * 
     * @param array $credentials
     * @return int
     */
    protected function getAsociatieIdFromCredentials( array $credentials )
    {
        $Asociatie = $this->getAsociatieByUsername( $credentials[ App_Component_Asociatie_Object::PROP_USERNAME ]);
        $asociatieId = $Asociatie->getId();
        
        return $asociatieId;
    }
    
}