<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::getStructuraAction
 * 
 */
class GetStructuraTest extends Test_Application_Api_Abstract
{
    protected $_action = 'get-structura';
    
    protected function cleanDb()
    {
        // truncate `bloc` table
        $blocDb = App_Component_Bloc_Db::getInstance();
        $blocDb->truncate();
        
        // truncate `scara` table
        $scaraDb = App_Component_Scara_Db::getInstance();
        $scaraDb->truncate();
        
        // truncate `spatiu` table
        $spatiuDb = App_Component_Spatiu_Db::getInstance();
        $spatiuDb->truncate();
        
        // truncate `parametru_spatiu` table
        $parametruSpatiuDb = App_Component_SpatiuParametru_Db::getInstance();
        $parametruSpatiuDb->truncate();
    }

    protected function defineDataValues()
    {
        
    }

    protected function generateInputValuesFromTestData( array $testData )
    {
    }

    protected function setupDbFixture()
    {
    }

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @before
     */
    public function authenticate()
    {
        $asociatieId = 1;
        
        // autentificare
        $this->doLogin( $this->getAsociatiiCredentials()[ $asociatieId ] );
        
    }
    
    public function test_AdaugareBlocuri_1_2()
    {
        $expectedValues = [];
        
        $this->addBloc( $expectedValues, 'B1 ' );
        $this->addBloc( $expectedValues, ' B2 ' );
        
        return $expectedValues;
    }
    
    /**
     * @depends test_AdaugareBlocuri_1_2
     */
    public function test_StergereBlocuri_1_2( $lastExpectedValues )
    {
        $this->removeBloc( $lastExpectedValues, 0 );
        $this->removeBloc( $lastExpectedValues, 0 ); // dupa ce blocul 1 a fost sters, pozitia blocului 2 este 0
        
        return $lastExpectedValues;
    }
        
    /**
     * @depends test_StergereBlocuri_1_2
     */
    public function test_AdaugareBlocuri_3_4()
    {
        $expectedValues = [];
        
        $this->addBloc( $expectedValues, ' B3' );
        $this->addBloc( $expectedValues, 'B4' );
        
        return $expectedValues;
    }
    
    /**
     * @depends test_AdaugareBlocuri_3_4
     */
    public function test_AdaugareScari_1_2_Bloc_3( $lastExpectedValues )
    {
        $this->addScara( $lastExpectedValues, 0, ' a ', 'str. Toma Cozma' );
        $this->addScara( $lastExpectedValues, 0, ' b ', 'str. Toma Cozma' );
        
        return $lastExpectedValues;
    }
    
    /**
     * @depends test_AdaugareScari_1_2_Bloc_3
     */
    public function test_StergereScari_1_2_Bloc_3( $lastExpectedValues )
    {
        $this->removeScara( $lastExpectedValues, 0, 0 );
        $this->removeScara( $lastExpectedValues, 0, 0 );

        return $lastExpectedValues;
    }
    
    /**
     * @depends test_StergereScari_1_2_Bloc_3
     */
    public function test_AdaugareScari_3_4_Bloc_3( $lastExpectedValues )
    {
        $this->addScara( $lastExpectedValues, 0, ' c', 'addrrr' );
        $this->addScara( $lastExpectedValues, 0, 'd ', 'aaa' );
        
        return $lastExpectedValues;
    }
    
    /**
     * @depends test_AdaugareScari_3_4_Bloc_3
     */
    public function test_AdaugareScari_5_6_7_Bloc_4( $lastExpectedValues )
    {
        $this->addScara( $lastExpectedValues, 1, ' a', 'addrrr' );
        $this->addScara( $lastExpectedValues, 1, 'c ', 'aaa' );
        $this->addScara( $lastExpectedValues, 1, ' b', 'aaa' );
        
        return $lastExpectedValues;
    }
    
    /**
     * @depends test_AdaugareScari_5_6_7_Bloc_4
     */
    public function test_StergereScara_6_Bloc_4( $lastExpectedValues )
    {
        $this->removeScara( $lastExpectedValues, 1, 1 );

        return $lastExpectedValues;
    }
    
    /**
     * @depends test_StergereScara_6_Bloc_4
     */
    public function test_AdaugareSpatii_1_2_Scara_3( $lastExpectedValues )
    {
        $this->addSpatiu( $lastExpectedValues, 0, 0, 3, 4, ' pp ', 3, 98.5 );
        $this->addSpatiu( $lastExpectedValues, 0, 0, 4, 4, ' pp ', 3, 98.5 );

        return $lastExpectedValues;
    }
    
    /**
     * @depends test_AdaugareSpatii_1_2_Scara_3
     */
    public function test_StergereSpatii_1_2_Scara_3( $lastExpectedValues )
    {
        $this->removeSpatiu( $lastExpectedValues, 0, 0, 0 );
        $this->removeSpatiu( $lastExpectedValues, 0, 0, 0 );

        return $lastExpectedValues;
    }
    
    /**
     * @depends test_StergereSpatii_1_2_Scara_3
     */
    public function test_AdaugareSpatii_3_4_5_6_Scara_3( $lastExpectedValues )
    {
        $this->addSpatiu( $lastExpectedValues, 0, 0, 5, 4, ' pp ', 3, 98.5 );
        $this->addSpatiu( $lastExpectedValues, 0, 0, 6, 4, 'aa ', 1, '98.50 ');
        $this->addSpatiu( $lastExpectedValues, 0, 0, 7, 4, ' BB ', 3, .5 );
        $this->addSpatiu( $lastExpectedValues, 0, 0, 8, 4, 'p p p ', 3, ' 9' );

        return $lastExpectedValues;
    }
    
    /**
     * @depends test_AdaugareSpatii_3_4_5_6_Scara_3
     */
    public function test_AdaugareSpatiu_7_Scara_4( $lastExpectedValues )
    {
        $this->addSpatiu( $lastExpectedValues, 0, 1, 1, App_Component_Spatiu_Property_Etaj::VAL_MEZANIN, ' pp ', 3, 98.5 );

        return $lastExpectedValues;
    }
    
    /**
     * @depends test_AdaugareSpatiu_7_Scara_4
     */
    public function test_AdaugareSpatii_8_9_10_Scara_7( $lastExpectedValues )
    {
        $this->addSpatiu( $lastExpectedValues, 1, 1, '10', 4, ' pp ', 3, 98.5 );
        $this->addSpatiu( $lastExpectedValues, 1, 1, '9', 4, 'aa ', 1, '98.50 ');
        $this->addSpatiu( $lastExpectedValues, 1, 1, 8, 4, ' BB ', 3, .5 );

        return $lastExpectedValues;
    }

    /**
     * @depends test_AdaugareSpatii_8_9_10_Scara_7
     */
    public function test_StergereScara_4( $lastExpectedValues )
    {
        $this->removeScara( $lastExpectedValues, 1, 1 );
        
        return $lastExpectedValues;
    }
    
    /**
     * @depends test_StergereScara_4
     */
    public function test_StergereBloc_4( $lastExpectedValues )
    {
        $this->removeBloc( $lastExpectedValues, 0 );
        
        return $lastExpectedValues;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    protected function performRequest( $expectedData )
    {
        $this->performAndTestAction([
            'postProcess' => function( $preProcessState, $inputValues, $credentials, $responseData ) use ( $expectedData )
            {
                $actualData = $responseData[ Lib_Api::RESPONSE_DATA ];
                
                $message = "\nExpected:\n" . print_r( $expectedData, true ) .
                           "\nActual:\n" . print_r( $actualData, true );
                        
                $this->assertEquals( $expectedData, $actualData, $message );
            },
        ]);
        
    }

    ////////////////////////////////////////////////////////////////////////////
    
    protected function addBloc( array &$expectedValues, $denumire )
    {
        $params = [
            App_Api_Bloc_Add::ARG_DENUMIRE => $denumire,
        ];
                
        $this->doRequestWithToken( $this->getToken(), 'add-bloc', $params );
        $this->assertResponseIsSuccessful();
        
        $returnData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        $returnData[ App_Component_Bloc_Object::PROP_SCARI ] = [];
        
        $this->addBlocToExpectedValues( $expectedValues, $returnData );

        $this->performRequest( $expectedValues );
        
        return $returnData;
    }
    
    protected function removeBloc( array &$expectedValues, $position )
    {
        $blocId = $this->getBlocIdFromResult( $expectedValues, $position );
        
        $params = [
            App_Api_Bloc_Remove::ARG_BLOC_ID => $blocId,
        ];
                
        $this->doRequestWithToken( $this->getToken(), 'remove-bloc', $params );
        $this->assertResponseIsSuccessful();
        
        $returnData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        
        $this->removeBlocFromExpectedValues( $expectedValues, $position );
        
        $this->performRequest( $expectedValues );
        
        return $returnData;
    }
    
    protected function addScara( array &$expectedValues, $blocPosition, $denumire, $adresa )
    {
        $blocId = $this->getBlocIdFromResult( $expectedValues, $blocPosition );
        
        $params = [
            App_Api_Scara_Add::ARG_BLOC_ID  => $blocId,
            App_Api_Scara_Add::ARG_DENUMIRE => $denumire,
            App_Api_Scara_Add::ARG_ADRESA   => $adresa,
        ];
                
        $this->doRequestWithToken( $this->getToken(), 'add-scara', $params );
        $this->assertResponseIsSuccessful();
        
        $returnData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        $returnData[ App_Component_Scara_Object::PROP_SPATII ] = [];
        
        $this->addScaraToExpectedValues( $expectedValues, $blocPosition, $returnData );
        
        $this->performRequest( $expectedValues );
        
        return $returnData;
    }
    
    protected function removeScara( array &$expectedValues, $blocPosition, $scaraPosition )
    {
        $scaraId = $this->getScaraIdFromResult( $expectedValues, $blocPosition, $scaraPosition );
        
        $params = [
            App_Api_Scara_Remove::ARG_SCARA_ID => $scaraId,
        ];
                
        $this->doRequestWithToken( $this->getToken(), 'remove-scara', $params );
        $this->assertResponseIsSuccessful();
        
        $returnData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        
        $this->removeScaraFromExpectedValues( $expectedValues, $blocPosition, $scaraPosition );
        
        $this->performRequest( $expectedValues );
        
        return $returnData;
    }
    
    protected function addSpatiu( array &$expectedValues, $blocPosition, $scaraPosition, $numar, $etaj, $proprietar, $persoane, $suprafata )
    {
        $scaraId = $this->getScaraIdFromResult( $expectedValues, $blocPosition, $scaraPosition );
        
        $params = [
            App_Api_Spatiu_Add::ARG_SCARA_ID    => $scaraId,
            App_Api_Spatiu_Add::ARG_NUMAR       => $numar,
            App_Api_Spatiu_Add::ARG_ETAJ        => $etaj,
            App_Api_Spatiu_Add::ARG_PROPRIETAR  => $proprietar,
            App_Api_Spatiu_Add::ARG_NR_PERS     => $persoane,
            App_Api_Spatiu_Add::ARG_SUPRAFATA   => $suprafata,
        ];
                
        $this->doRequestWithToken( $this->getToken(), 'add-spatiu', $params );
        $this->assertResponseIsSuccessful();
        
        $returnData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        
        $this->addSpatiuToExpectedValues( $expectedValues, $blocPosition, $scaraPosition, $returnData );
        
        return $returnData;
    }
    
    protected function removeSpatiu( array &$expectedValues, $blocPosition, $scaraPosition, $spatiuPosition )
    {
        $spatiuId = $this->getSpatiuIdFromResult( $expectedValues, $blocPosition, $scaraPosition, $spatiuPosition );
        
        $params = [
            App_Api_Spatiu_Remove::ARG_SPATIU_ID => $spatiuId,
        ];
                
        $this->doRequestWithToken( $this->getToken(), 'remove-spatiu', $params );
        $this->assertResponseIsSuccessful();
        
        $returnData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        
        $this->removeSpatiuFromExpectedValues( $expectedValues, $blocPosition, $scaraPosition, $spatiuPosition );
        
        return $returnData;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function getBlocIdFromResult( $result, $blocIndex )
    {
        $blocId = $result
            [ $blocIndex ]
            [ App_Component_Bloc_Object::PROP_ID ];
        
        return $blocId;
    }

    public function getScaraIdFromResult( $result, $blocIndex, $scaraIndex )
    {
        $scaraId = $result
            [ $blocIndex ]
            [ App_Component_Bloc_Object::PROP_SCARI ]
            [ $scaraIndex ]
            [ App_Component_Scara_Object::PROP_ID ];
        
        return $scaraId;
    }
    
    public function getSpatiuIdFromResult( $result, $blocIndex, $scaraIndex, $spatiuIndex )
    {
        $spatiuId = $result
            [ $blocIndex ]
            [ App_Component_Bloc_Object::PROP_SCARI ]
            [ $scaraIndex ]
            [ App_Component_Scara_Object::PROP_SPATII ]
            [ $spatiuIndex ]
            [ App_Component_Spatiu_Object::PROP_ID ];
        
        return $spatiuId;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function addBlocToExpectedValues( array &$expectedValues, array $blocValues )
    {
        $expectedValues[] = $blocValues;
    }
    
    public function removeBlocFromExpectedValues( array &$expectedValues, $blocPosition )
    {
        Lib_Array::removeElement( $expectedValues, $blocPosition );
    }
    
    public function addScaraToExpectedValues( array &$expectedValues, $blocPosition, array $scaraValues )
    {
        $expectedValues[ $blocPosition ][ App_Component_Bloc_Object::PROP_SCARI ][] = $scaraValues;
    }
    
    public function removeScaraFromExpectedValues( array &$expectedValues, $blocPosition, $scaraPosition )
    {
        Lib_Array::removeElement( $expectedValues[ $blocPosition ][ App_Component_Bloc_Object::PROP_SCARI ], $scaraPosition );
    }
    
    public function addSpatiuToExpectedValues( array &$expectedValues, $blocPosition, $scaraPosition, array $spatiuValues )
    {
        $expectedValues[ $blocPosition ][ App_Component_Bloc_Object::PROP_SCARI ]
                [ $scaraPosition ][ App_Component_Scara_Object::PROP_SPATII ][] = $spatiuValues;
    }
    
    public function removeSpatiuFromExpectedValues( array &$expectedValues, $blocPosition, $scaraPosition, $spatiuPosition )
    {
        Lib_Array::removeElement( $expectedValues[ $blocPosition ][ App_Component_Bloc_Object::PROP_SCARI ][ $scaraPosition ][ App_Component_Scara_Object::PROP_SPATII ], $spatiuPosition );
    }
    
}
