<?php

require_once '01.GetTest.php';

/**
 * @group structura
 * 
 * @covers ApiController::getStructuraAction
 * 
 * Testele se ruleaza pentru o alta Asociatie
 * 
 */
class GetStructuraTest_2 extends GetStructuraTest
{
    protected function cleanDb()
    {
        // continutul tabelelor se lasa nemodificat
    }

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @before
     */
    public function authenticate()
    {
        $asociatieId = 2;
        
        // autentificare
        $this->doLogin( $this->getAsociatiiCredentials()[ $asociatieId ] );
        
    }
}
