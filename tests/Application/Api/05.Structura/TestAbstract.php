<?php

abstract class _SpatiuTestAbstract extends Test_Application_Api_Abstract
{
    const DATA_ETAJ_MAX                 = '**etaj_max**';
    const DATA_NUMAR_MAX_LENGTH         = '**numar_max_length**';
    const DATA_NUMAR_LONG_1             = '**numar_long_1**';
    const DATA_NUMAR_LONG_N             = '**numar_long_N**';
    const DATA_PROPRIETAR_MAX_LENGTH    = '**proprietar_max_length**';
    const DATA_PERSOANE_MAX             = '**persoane_max**';
    const DATA_PERSOANE_MAX_1           = '**persoane_max+1**';
    const DATA_SUPRAFATA_MAX            = '**suprafata_max**';
    const DATA_SUPRAFATA_MAX_1          = '**suprafata_max+1**';
    const DATA_ETAJ_MAX_1               = '**etaj_max+1**';
    const DATA_PROPRIETAR_LONG_1        = '**proprietar_long_1**';
    const DATA_PROPRIETAR_LONG_N        = '**proprietar_long_N**';

    protected function cleanDb()
    {
        // truncate `bloc` table
        $blocDb = App_Component_Bloc_Db::getInstance();
        $blocDb->truncate();
        
        // truncate `scara` table
        $scaraDb = App_Component_Scara_Db::getInstance();
        $scaraDb->truncate();
        
        // truncate `spatiu` table
        $spatiuDb = App_Component_Spatiu_Db::getInstance();
        $spatiuDb->truncate();
        
        // truncate `parametru_spatiu` table
        $parametruSpatiuDb = App_Component_SpatiuParametru_Db::getInstance();
        $parametruSpatiuDb->truncate();
    }

    protected function deleteSpatiuEntries( $scaraId )
    {
        $spatiuDb = App_Component_Spatiu_Db::getInstance();
        
        $spatiuDb->delete([
            App_Component_Spatiu_Object::PROP_SCARA_ID => $scaraId,
        ]);
    }
    
    protected function addContentIntoDb( array $dbContent )
    {
        foreach ( $dbContent as $dataRow )
        {
            $credentials = $this->getCredentialsFromTestData( $dataRow );
            $this->doLogin( $credentials );
            
            //// adaugare bloc, daca nu exista
            
            $Bloc = App_Component_Bloc_Factory::getInstance()->findActive([
                App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $dataRow[ 0 ],
                App_Component_Bloc_Object::PROP_DENUMIRE        => $dataRow[ 1 ],
            ]);
            
            if( !$Bloc )
            {
                $blocValues = [
                    App_Api_Bloc_Add::ARG_DENUMIRE => $dataRow[ 1 ],
                ];
                $this->doRequestWithToken( $this->getToken(), 'add-bloc', $blocValues );
                $this->assertResponseIsSuccessful();
                
                $blocId = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Api_Bloc_Add::ARG_BLOC_ID ];
            }
            else
            {
                $blocId = $Bloc->getId();
            }
            
            //// adaugare scara, daca nu exista
            
            $Scara = App_Component_Scara_Factory::getInstance()->findActive([
                App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
                App_Component_Scara_Object::PROP_DENUMIRE   => $dataRow[ 2 ],
            ]);
            
            if( !$Scara )
            {
                $scaraValues = [
                    App_Api_Scara_Add::ARG_BLOC_ID      => $blocId,
                    App_Api_Scara_Add::ARG_DENUMIRE     => $dataRow[ 2 ],
                    App_Api_Scara_Add::ARG_ADRESA       => $dataRow[ 3 ],
                ];
                
                $this->doRequestWithToken( $this->getToken(), 'add-scara', $scaraValues );
                $this->assertResponseIsSuccessful();
                
                $scaraId = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Api_Scara_Add::ARG_SCARA_ID ];
            }
            else
            {
                $scaraId = $Scara->getId();
            }
            
            //// adaugare spatiu
            
            $spatiuValues = [
                App_Api_Spatiu_Add::ARG_SCARA_ID        => $scaraId,
                App_Api_Spatiu_Add::ARG_NUMAR           => $dataRow[ 4 ],
                App_Api_Spatiu_Add::ARG_ETAJ            => $dataRow[ 5 ],
                App_Api_Spatiu_Add::ARG_PROPRIETAR      => $dataRow[ 6 ],
                App_Api_Spatiu_Add::ARG_NR_PERS         => $dataRow[ 7 ],
                App_Api_Spatiu_Add::ARG_SUPRAFATA       => $dataRow[ 8 ],
            ];
            
            $this->doRequestWithToken( $this->getToken(), 'add-spatiu', $spatiuValues );
            $this->assertResponseIsSuccessful();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////

    protected function defineDataValues()
    {
        return [
            self::DATA_ETAJ_MAX                 => App_Component_Spatiu_Property_Etaj::MAX_VALUE,
            self::DATA_ETAJ_MAX_1               => App_Component_Spatiu_Property_Etaj::MAX_VALUE + 1,
            self::DATA_NUMAR_MAX_LENGTH         => Lib_Tools::generateRandomString( App_Component_Spatiu_Validator_EntryAbstract::NUMAR_MAX_LENGTH ),
            self::DATA_NUMAR_LONG_1             => Lib_Tools::generateRandomString( App_Component_Spatiu_Validator_EntryAbstract::NUMAR_MAX_LENGTH + 1 ),
            self::DATA_NUMAR_LONG_N             => Lib_Tools::generateRandomString( App_Component_Spatiu_Validator_EntryAbstract::NUMAR_MAX_LENGTH + 1, App_Component_Spatiu_Validator_EntryAbstract::NUMAR_MAX_LENGTH * 10 ),
            self::DATA_PROPRIETAR_MAX_LENGTH    => Lib_Tools::generateRandomString( App_Component_Spatiu_Validator_EntryAbstract::PROPRIETAR_MAX_LENGTH ),
            self::DATA_PROPRIETAR_LONG_1        => Lib_Tools::generateRandomString( App_Component_Spatiu_Validator_EntryAbstract::PROPRIETAR_MAX_LENGTH + 1 ),
            self::DATA_PROPRIETAR_LONG_N        => Lib_Tools::generateRandomString( App_Component_Spatiu_Validator_EntryAbstract::PROPRIETAR_MAX_LENGTH + 1, App_Component_Spatiu_Validator_EntryAbstract::PROPRIETAR_MAX_LENGTH * 10 ),
            self::DATA_PERSOANE_MAX             => App_Component_Spatiu_Validator_EntryAbstract::NR_PERS_MAX_VALUE,
            self::DATA_PERSOANE_MAX_1           => App_Component_Spatiu_Validator_EntryAbstract::NR_PERS_MAX_VALUE + 1,
            self::DATA_SUPRAFATA_MAX            => App_Component_Spatiu_Validator_EntryAbstract::SUPRAFATA_MAX_VALUE,
            self::DATA_SUPRAFATA_MAX_1          => App_Component_Spatiu_Validator_EntryAbstract::SUPRAFATA_MAX_VALUE + 1,
        ];
    }
    
    protected function getBlocIdFromTestData( array $testData )
    {
        $Bloc = App_Component_Bloc_Factory::getInstance()->findActive([
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $testData[ 0 ],
            App_Component_Bloc_Object::PROP_DENUMIRE        => $testData[ 1 ],
        ]);
        
        $blocId = $Bloc ? $Bloc->getId() : 0;
        
        return $blocId;
    }
    
    protected function getScaraIdFromTestData( array $testData )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        
        $Scara = App_Component_Scara_Factory::getInstance()->findActive([
            App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
            App_Component_Scara_Object::PROP_DENUMIRE   => $testData[ 2 ],
        ]);
        
        $scaraId = $Scara ? $Scara->getId() : 0;
        
        return $scaraId;
    }
    
    protected function getSpatiuIdFromTestData( array $testData )
    {
        $scaraId = $this->getScaraIdFromTestData( $testData );
        
        $Spatiu = App_Component_Spatiu_Factory::getInstance()->findActive([
            App_Component_Spatiu_Object::PROP_SCARA_ID  => $scaraId,
            App_Component_Spatiu_Object::PROP_NUMAR     => $testData[ 3 ],
        ]);
        
        $spatiuId = $Spatiu ? $Spatiu->getId() : 0;
        
        return $spatiuId;
    }
    
    protected function getExpectedSuprafataValue( $inputValue )
    {
        $expectedValue = round( floatval( $inputValue ), 2 );
        
        return $expectedValue;
    }
    
    protected function getExpectedNrPersoaneValue( $inputValue )
    {
        $expectedValue = intval( $inputValue );
        
        return $expectedValue;
    }
    
    protected function getExpectedEtajLabelValue( $inputValue )
    {
        $expectedValue = App_Component_Spatiu_Property_Etaj::getLabel( $inputValue );
        
        return $expectedValue;
    }
    
}