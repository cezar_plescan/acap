<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::removeSpatiuAction
 * 
 */
class RemoveSpatiuTest extends SpatiuTestAbstract
{
    protected $_action = 'remove-spatiu';
    
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR, ETAJ, PROPRIETAR, NR_PERSOANE, SUPRAFATA ]
        [ 1, 'A1', 'A', 'adresa SCARA A',     1,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN,   'p1 ',      '0',    '25' ],
        [ 1, 'A1', 'A', 'adresa SCARA A',     2,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN, 'p2 ',      '1',    '2' ],
        [ 1, 'A1', 'A', 'adresa SCARA A',     3,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN, 'p3 ',      '2',    ' 77' ],
        
        [ 1, 'A1', 'B', 'adresa SCARA B',     1,   '4',    'p4 ',      '2',    4.44 ],
        
        [ 2, 'B1', 'A', 'adresa SCARA A',     1,   '3',    'p1 ',      '2',    '77' ],
        [ 2, 'B1', 'A', 'adresa SCARA A',     2,   '31',   'p1 ',      '12',   '11' ],
    ];

    protected $_dataForAuthenticationErrors = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR_SPATIU ]
        [ 1, 'A1', 'B', '1' ],
        [ 2, 'B1', 'A', '1' ],
    ];
    
    protected $_goodData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR_SPATIU ]
        [ 1, 'A1', 'A', 2 ],
        [ 1, 'A1', 'A', 3 ],
        [ 1, 'A1', 'B', '1' ],
        [ 2, 'B1', 'A', ' 2 ' ],
    ];

    protected $_dataForErrors = [
        /* [ [ expected_errors ], 
         *      [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR_SPATIU ],
         *          asociatie_account_id ]
         */
        // spatiu inexistent
        [ [ App_Api_Spatiu_Remove::ERR_SPATIU_ID_INVALID ],
                [ 1, 'A1', 'A', 'nnn' ] ],
        // spatiu eliminat
        [ [ App_Api_Spatiu_Remove::ERR_SPATIU_ID_INVALID ],
                [ 1, 'A1', 'A', [ '2', ' 3 '] ] ],
        // spatiu apartinand altei asociatii
        [ [ App_Api_Spatiu_Remove::ERR_SPATIU_ID_INVALID ], 
                [ 2, 'B1', 'A', 1 ],
                    1 ],
    ];
    
    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );    
    }

    ////////////////////////////////////////////////////////////////////////////
   
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $spatiuId = $this->getSpatiuIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Spatiu_Remove::ARG_SPATIU_ID    => $spatiuId,
        ];
        
        return $inputValues;
    }

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * @dataProvider goodDataProvider
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'preProcess' => function( $inputValues, $credentials )
            {
                    $spatiuId = $inputValues[ App_Api_Spatiu_Remove::ARG_SPATIU_ID ];
                    $Spatiu = App_Component_Spatiu_Factory::getInstance()->find( $spatiuId );
                    $initialValues = $Spatiu->toArray();

                    $state = [
                        'spatiu_id'         => $spatiuId,
                        'initial_values'    => $initialValues,
                    ];
                    
                    return $state;
            },
            
            'returnedDataAssertions' => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Spatiu_Remove::ARG_SPATIU_ID => [ 'Equals', $inputValues[ App_Api_Spatiu_Remove::ARG_SPATIU_ID ] ],
                    ];

                    return $assertions;
            },
            
            'postProcess' => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $initialValues  = $preProcessState[ 'initial_values' ];
                    $spatiuId       = $preProcessState[ 'spatiu_id' ];

                    // verificare inregistrare in DB
                    $expectedProperties = Lib_Tools::filterArray( $initialValues, [
                        App_Component_Spatiu_Object::PROP_ASOCIATIE_ID,
                        App_Component_Spatiu_Object::PROP_BLOC_ID,
                        App_Component_Spatiu_Object::PROP_SCARA_ID,
                        App_Component_Spatiu_Object::PROP_ADDING_MONTH,
                        App_Component_Spatiu_Object::PROP_NUMAR,
                        App_Component_Spatiu_Object::PROP_ETAJ,
                    ]);
                    $expectedProperties [ App_Component_Spatiu_Object::PROP_STATUS ] = App_Component_Scara_Object::VAL_STATUS_DELETED;

                    $TestCase->assertObjectExistsAndHasProperties( 'Spatiu', $spatiuId, $expectedProperties );
            }
        ]);
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedObjects'  => function( $inputValues )
            {
                $objectConfig = [
                    [ 'Spatiu', $inputValues[ App_Component_Spatiu_Object::PROP_ID ] ],
                ];
                
                return $objectConfig;
            },
        ]);
    }
    
}