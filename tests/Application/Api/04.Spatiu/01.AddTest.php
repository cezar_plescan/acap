<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::addSpatiuAction
 * 
 */
class AddSpatiuTest extends SpatiuTestAbstract
{
    protected $_action = 'add-spatiu';
    
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, ADRESA_SCARA ]
        [ 1, 'A1', 'A', 'adresa SCARA A' ],
        [ 1, 'A1', 'B', 'adresa SCARA B' ],
        [ 2, 'B1', 'A', 'adresa A' ],
    ];
    
    protected function _getGoodDataProviderValues()
    {
        $values = [
            // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR, ETAJ, PROPRIETAR, NR_PERSOANE, SUPRAFATA ]
            [ 1, 'A1', 'A',     '1',    App_Component_Spatiu_Property_Etaj::VAL_DEMISOL,     'p1 ',      '0',    '25' ],
            [ 1, 'A1', 'A',     '2',    strtoupper( App_Component_Spatiu_Property_Etaj::VAL_PARTER ),      'p2 ',      '1',    '2' ],
            [ 1, 'A1', 'A',     'A3',   ' '.App_Component_Spatiu_Property_Etaj::VAL_MEZANIN.' ',     'p3 ',      '2',    ' 0770' ],
            [ 1, 'A1', 'A',     'A4',   1,      'p4 ',      '6',    '045.8888 ' ],
            [ 1, 'A1', 'A',     '5',    ' 1 ',   'P5 ',      '7',    '5' ],
            [ 1, 'A1', 'A',     '6',    ' 2',   ' p6 ',     '16',   80.78 ],

            [ 1, 'A1', 'B',     self::DATA_NUMAR_MAX_LENGTH, self::DATA_ETAJ_MAX, self::DATA_PROPRIETAR_MAX_LENGTH, self::DATA_PERSOANE_MAX, self::DATA_SUPRAFATA_MAX ],
            [ 1, 'A1', 'B',     '1',   4,   'p4 ',      '2',    '4.44400' ],
            [ 1, 'A1', 'B',     '2',   5,   'p4 ',      '0',    '005.8' ],

            [ 2, 'B1', 'A',     'A',    3,    'p1 ',      2,    '77' ],
            [ 2, 'B1', 'A',     'A1',   31,   'p1 ',      '12',   '11' ],
            [ 2, 'B1', 'A',     'A2',   32,   ' P1 ',     '3',    '22' ],
            [ 2, 'B1', 'A',     'A3',   App_Component_Spatiu_Property_Etaj::VAL_SUBSOL,   ' P1 ',     13,   '33' ],
            [ 2, 'B1', 'A',     'A4',   ' '.App_Component_Spatiu_Property_Etaj::VAL_MANSARDA,   ' P1 ',     13,   '33' ],
        ];
    
        return $values;
    }
    
    protected function getDataForRemoveAndAdd()
    { 
        $values = [
            // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR, ETAJ, PROPRIETAR, NR_PERSOANE, SUPRAFATA ]
            [ 1, 'A1', 'A',     'A3',   ' '.App_Component_Spatiu_Property_Etaj::VAL_PARTER.' ', 'p3 ',      '2',    ' 0770' ],
            [ 1, 'A1', 'A',     'A4',   ' 1',    'p4 ',      '6',    '045.8888 ' ],
            [ 2, 'B1', 'A',     'A1',   '31 ',   'p1 ',      '12',   '11' ],
        ];
        
        return $values;
    }
    
    protected function _getErrorsDataProviderValues()
    {
        $values = [
            /* [ [ expected_errors ], 
             *      [ input_values ] ]
             */
            [ [ App_Api_Spatiu_Add::ERR_SCARA_ID_INVALID ], 
                    [ 2, 'B1', 'B', '', '', '', '', '' ] ],
            [ [ App_Api_Spatiu_Add::ERR_SCARA_ID_INVALID ], 
                    [ 2, 'B1', 'A', 'A4', '33', ' P1 ', '13', '33' ],
                        1 ],

            [ [ App_Api_Spatiu_Add::ERR_ETAJ_REQUIRED, App_Api_Spatiu_Add::ERR_NUMAR_REQUIRED, App_Api_Spatiu_Add::ERR_PROPRIETAR_REQUIRED, App_Api_Spatiu_Add::ERR_NR_PERS_REQUIRED, App_Api_Spatiu_Add::ERR_SUPRAFATA_REQUIRED ],
                    [ 2, 'B1', 'A', '', ['', NULL, '  '], '', '', '' ] ],

            [ [ App_Api_Spatiu_Add::ERR_ETAJ_INVALID, App_Api_Spatiu_Add::ERR_NUMAR_DUPLICATE, App_Api_Spatiu_Add::ERR_PROPRIETAR_MAX_LENGTH, App_Api_Spatiu_Add::ERR_SUPRAFATA_REQUIRED ],
                    [ 2, 'B1', 'A', 'A3', ['-11', 'a', 0, self::DATA_ETAJ_MAX_1 ], self::DATA_PROPRIETAR_LONG_1, '13', '  ' ] ],

            [ [ App_Api_Spatiu_Add::ERR_NUMAR_MAX_LENGTH, App_Api_Spatiu_Add::ERR_PROPRIETAR_MAX_LENGTH, App_Api_Spatiu_Add::ERR_NR_PERS_REQUIRED, App_Api_Spatiu_Add::ERR_SUPRAFATA_REQUIRED ],
                    [ 2, 'B1', 'A', [ self::DATA_NUMAR_LONG_1, self::DATA_NUMAR_LONG_N ], '33', self::DATA_PROPRIETAR_LONG_N, NULL, NULL ] ],

            [ [ App_Api_Spatiu_Add::ERR_NUMAR_DUPLICATE, App_Api_Spatiu_Add::ERR_NR_PERS_INVALID, App_Api_Spatiu_Add::ERR_PROPRIETAR_REQUIRED ],
                    [ 2, 'B1', 'A', ' a1 ', '33', NULL, [ '-1', 'a', '3.4' ], '33' ] ],

            [ [ App_Api_Spatiu_Add::ERR_NUMAR_DUPLICATE, App_Api_Spatiu_Add::ERR_NR_PERS_MAX_VALUE, App_Api_Spatiu_Add::ERR_SUPRAFATA_INVALID ],
                    [ 2, 'B1', 'A', 'a', '33', 'proprietar', self::DATA_PERSOANE_MAX_1, [ 'a', '-1', '0010.110.00' ] ] ],

            [ [ App_Api_Spatiu_Add::ERR_SUPRAFATA_MAX_VALUE, App_Api_Spatiu_Add::ERR_NUMAR_REQUIRED, App_Api_Spatiu_Add::ERR_PROPRIETAR_REQUIRED, App_Api_Spatiu_Add::ERR_NR_PERS_REQUIRED ],
                    [ 2, 'B1', 'A', [ '  ', NULL ], '33', '   ', '  ', self::DATA_SUPRAFATA_MAX_1 ] ],

        ];
        
        return $values;
    }
    
    protected $scariData = [
        [ 1, 'A1', 'A' ],
    ];

    ////////////////////////////////////////////////////////////////////////////

    /**
     * Adaugare blocuri si scari
     */
    protected function setupDbFixture()
    {
        foreach ( $this->dbFixture as $dataRow )
        {
            $credentials = $this->getCredentialsFromTestData( $dataRow );
            $this->doLogin( $credentials );
            
            $Bloc = App_Component_Bloc_Factory::getInstance()->findActive([
                App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $dataRow[ 0 ],
                App_Component_Bloc_Object::PROP_DENUMIRE        => $dataRow[ 1 ],
            ]);
            
            if( !$Bloc )
            {
                $blocValues = [
                    App_Api_Bloc_Add::ARG_DENUMIRE => $dataRow[ 1 ],
                ];
                $this->doRequestWithToken( $this->getToken(), 'add-bloc', $blocValues );
                $this->assertResponseIsSuccessful();
                
                $blocId = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Api_Bloc_Add::ARG_BLOC_ID ];
            }
            else
            {
                $blocId = $Bloc->getId();
            }
            
            $scaraValues = [
                App_Api_Scara_Add::ARG_BLOC_ID      => $blocId,
                App_Api_Scara_Add::ARG_DENUMIRE     => $dataRow[ 2 ],
                App_Api_Scara_Add::ARG_ADRESA       => $dataRow[ 3 ],
            ];
            
            $this->doRequestWithToken( $this->getToken(), 'add-scara', $scaraValues );
            $this->assertResponseIsSuccessful();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////

    /**
     * 
     * @param array $testData [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, ETAJ, NUMAR, PROPRIETAR, NR_PERSOANE, SUPRAFATA ]

     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $scaraId = $this->getScaraIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Spatiu_Add::ARG_SCARA_ID    => $scaraId,
            App_Api_Spatiu_Add::ARG_NUMAR       => $testData[ 3 ],
            App_Api_Spatiu_Add::ARG_ETAJ        => $testData[ 4 ],
            App_Api_Spatiu_Add::ARG_PROPRIETAR  => $testData[ 5 ],
            App_Api_Spatiu_Add::ARG_NR_PERS     => $testData[ 6 ],
            App_Api_Spatiu_Add::ARG_SUPRAFATA   => $testData[ 7 ],
        ];
        
        return $inputValues;
    }
    
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @dataProvider goodDataProvider
     * 
     * @param Array $testData 
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'returnedDataAssertions' => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Spatiu_Add::ARG_SPATIU_ID   => 'NotEmpty',
                        App_Api_Spatiu_Add::ARG_SCARA_ID    => [ 'Equals', trim( $inputValues[ App_Api_Spatiu_Add::ARG_SCARA_ID ] ) ],
                        App_Api_Spatiu_Add::ARG_BLOC_ID     => [ 'Equals', App_Component_Scara_Factory::getInstance()->find( $inputValues[ App_Api_Spatiu_Add::ARG_SCARA_ID ] )->getBlocId() ],
                        App_Api_Spatiu_Add::ARG_ETAJ        => [ 'Equals', strtoupper( trim( $inputValues[ App_Api_Spatiu_Add::ARG_ETAJ ] ) ) ],
                        App_Api_Spatiu_Add::ARG_ETAJ_ORDER  => [ 'Equals', $this->getExpectedEtajOrderValue( $inputValues[ App_Api_Spatiu_Add::ARG_ETAJ ] ) ],
                        App_Api_Spatiu_Add::ARG_NUMAR       => [ 'Equals', trim( $inputValues[ App_Api_Spatiu_Add::ARG_NUMAR ] ) ],
                        App_Api_Spatiu_Add::ARG_PROPRIETAR  => [ 'Equals', trim( $inputValues[ App_Api_Spatiu_Add::ARG_PROPRIETAR ] ) ],
                        App_Api_Spatiu_Add::ARG_NR_PERS     => [ 'Equals', $this->getExpectedNrPersoaneValue( $inputValues[ App_Api_Spatiu_Add::ARG_NR_PERS ] ) ],
                        App_Api_Spatiu_Add::ARG_SUPRAFATA   => [ 'Equals', $this->getExpectedSuprafataValue( $inputValues[ App_Api_Spatiu_Add::ARG_SUPRAFATA ] ) ],
                    ];
                    
                    return $assertions;
            },
            
            'postProcess' => function( $preProcessState, $inputValues, $credentials, $responseData, Test_Application_Api_Abstract $TestCase )
            {
                    $Asociatie = $this->getAsociatieFromCredentials( $credentials );
                    $asociatieId = $Asociatie->getId();
                    $workingMonth = $Asociatie->getLunaCurenta();
                    
                    // retrieve spatiu_id from returned data
                    $returnedData = $responseData[ Lib_Api::RESPONSE_DATA ];
                    $spatiuId = $returnedData[ App_Api_Spatiu_Add::ARG_SPATIU_ID ];

                    $blocId = App_Component_Scara_Factory::getInstance()->find( $inputValues[ App_Api_Spatiu_Add::ARG_SCARA_ID ] )->getProperty( App_Component_Scara_Object::PROP_BLOC_ID );
                    
                    // verificare inregistrare in DB
                    $expectedProperties = [
                        App_Component_Spatiu_Object::PROP_ASOCIATIE_ID  => $asociatieId,
                        App_Component_Spatiu_Object::PROP_BLOC_ID       => $blocId,
                        App_Component_Spatiu_Object::PROP_SCARA_ID      => $inputValues[ App_Api_Spatiu_Add::ARG_SCARA_ID ],
                        App_Component_Spatiu_Object::PROP_NUMAR         => trim( $inputValues[ App_Api_Spatiu_Add::ARG_NUMAR ] ),
                        App_Component_Spatiu_Object::PROP_ETAJ          => $this->getExpectedEtajOrderValue( $inputValues[ App_Api_Spatiu_Add::ARG_ETAJ ] ),
                        App_Component_Spatiu_Object::PROP_ADDING_MONTH  => [ $workingMonth, true ],
                        App_Component_Spatiu_Object::PROP_STATUS        => App_Component_Spatiu_Object::VAL_STATUS_ACTIVE,
                    ];

                    $TestCase->assertObjectExistsAndHasProperties( 'Spatiu', $spatiuId, $expectedProperties );
                    
                    // verificare parametri
                    $listaParametri = [ App_Api_Spatiu_Add::ARG_PROPRIETAR, App_Api_Spatiu_Add::ARG_NR_PERS, App_Api_Spatiu_Add::ARG_SUPRAFATA ];
                    foreach( $listaParametri as $denumireParametru )
                    {
                        $inputValue = $inputValues[ $denumireParametru ];
                        
                        switch( $denumireParametru )
                        {
                            case App_Api_Spatiu_Add::ARG_SUPRAFATA:
                                $inputValue = $this->getExpectedSuprafataValue( $inputValue );
                            break;
                            case App_Api_Spatiu_Add::ARG_NR_PERS:
                                $inputValue = $this->getExpectedNrPersoaneValue( $inputValue );
                            break;
                        }
                        
                        $criteria = [
                            App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                            App_Component_SpatiuParametru_Object::PROP_PARAMETRU       => $denumireParametru,
                            App_Component_SpatiuParametru_Object::PROP_VALUE           => $inputValue,
                            App_Component_SpatiuParametru_Object::PROP_REF_ID          => $spatiuId,
                        ];
                        
                        $TestCase->assertEquals( 1, App_Component_SpatiuParametru_Factory::getInstance()->count( $criteria ), "Parametrul `$denumireParametru` nu a fost salvat sau este salvat incorect." );
                    }
            }
        ]);
    }

    /**
     * @depends test_GoodData
     * @dataProvider removeAndAddDataProvider
     */
    public function test_RemoveAndAdd( array $testData, array $credentials )
    {
        $spatiuIdToRemove = $this->getSpatiuIdFromTestData( $testData );
        App_Component_Spatiu_Factory::getInstance()->find( $spatiuIdToRemove )->remove();

        $inputValues = $this->generateInputValuesFromTestData( $testData );

        $this->performAndTestAction([
            'inputValues'   => $inputValues, 
            'credentials'   => $credentials,
        ]);
    }
    
    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedTables'   => [ 'Spatiu' ],
        ] );
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider scariDataProvider
     */
    public function test_Error_MaxEntries( array $testData, array $credentials )
    {
        $scaraId = $this->getScaraIdFromTestData( $testData );
        $this->deleteSpatiuEntries( $scaraId );
        
        // autentificare
        $this->doLogin( $credentials );
        $token = $this->getToken();
        
        // adaugare numar maxim de spatii
        for( $i = 0; $i < App_Component_Spatiu_Validator_Add::MAX_ENTRIES_PER_SCARA; $i++ )
        {
            $inputValues = [
                App_Component_Spatiu_Object::PROP_SCARA_ID  => $scaraId,
                App_Component_Spatiu_Object::PROP_ETAJ      => 1,
                App_Component_Spatiu_Object::PROP_NUMAR     => $i,
                App_Component_Spatiu_Object::PROP_PROPRIETAR => "proprietar_$i",
                App_Component_Spatiu_Object::PROP_NR_PERSOANE => 1,
                App_Component_Spatiu_Object::PROP_SUPRAFATA => 10,
            ];
            
            $this->doRequestWithToken( $token, $this->_action, $inputValues );
            $this->assertResponseIsSuccessful();
        }
        
        // se mai adauga un spatiu; trebuie sa se genereze eroare
        $inputValuesForError = $inputValues;
        $inputValuesForError[ App_Component_Spatiu_Object::PROP_NUMAR ] = $i;

        $this->doRequestWithToken( $token, $this->_action, $inputValuesForError );
        $this->assertResponseHasErrors( App_Api_Spatiu_Add::ERR_MAX_ENTRIES );
        
        // stergere ultima scara adaugata
        $patiuValuesForRemove = [
            App_Component_Spatiu_Object::PROP_NUMAR     => ( $i - 1 ),
            App_Component_Spatiu_Object::PROP_SCARA_ID  => $scaraId,
        ];
        $Spatiu = App_Component_Spatiu_Factory::getInstance()->findActive( $patiuValuesForRemove );
        $Spatiu->remove();
        
        // adaugare spatiu; operatia trebuie sa reuseasca
        $inputValuesForAdd = $inputValues;
        $inputValuesForAdd[ App_Component_Spatiu_Object::PROP_NUMAR ] = $i;

        $this->doRequestWithToken( $token, $this->_action, $inputValuesForAdd );
        $this->assertResponseIsSuccessful();
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider scariDataProvider
     */
    public function test_Error_MaxOperations( array $testData, array $credentials )
    {
        $scaraId = $this->getScaraIdFromTestData( $testData );
        $this->deleteSpatiuEntries( $scaraId );
        
        // autentificare
        $this->doLogin( $credentials );
        $token = $this->getToken();
        
        // executare numar maxim de operatii
        for( $i = 0; $i < App_Component_Spatiu_Validator_Add::MAX_OPERATIONS; $i++ )
        {
            $inputValues = [
                App_Component_Spatiu_Object::PROP_SCARA_ID  => $scaraId,
                App_Component_Spatiu_Object::PROP_ETAJ      => 1,
                App_Component_Spatiu_Object::PROP_NUMAR     => $i,
                App_Component_Spatiu_Object::PROP_PROPRIETAR => "proprietar_$i",
                App_Component_Spatiu_Object::PROP_NR_PERSOANE => 1,
                App_Component_Spatiu_Object::PROP_SUPRAFATA => 10,
            ];
            
            // adaugare
            $this->doRequestWithToken( $token, $this->_action, $inputValues );
            $this->assertResponseIsSuccessful();
            
            // stergere
            $spatiuId = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Api_Spatiu_Add::ARG_SPATIU_ID ];
            $Spatiu = App_Component_Spatiu_Factory::getInstance()->find( $spatiuId );
            $Spatiu->remove();
        }
        
        // se incearca adaugarea unui nou spatiu; trebuie sa se genereze eroare
        $inputValuesForError = $inputValues;
        $inputValuesForError[ App_Component_Spatiu_Object::PROP_NUMAR ] = $i;
        
        $this->doRequestWithToken( $token, $this->_action, $inputValuesForError );
        $this->assertResponseHasErrors( App_Api_Spatiu_Add::ERR_MAX_OPERATIONS );
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @return array of [ test_data, credentials ]
     */
    public function scariDataProvider()
    {
        $outputData = [];
        
        foreach( $this->scariData as $testData )
        {
            $credentials = $this->getCredentialsFromTestData( $testData );
            
            $this->addData( $outputData, $testData, $credentials );
        }
        
        return $outputData;
    }

    public function removeAndAddDataProvider()
    {
        return $this->generateOutputData( $this->getDataForRemoveAndAdd() );
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
}