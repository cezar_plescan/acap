<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * 
 */
class UnavailabilitySpatiuTest extends SpatiuTestAbstract
{
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR, ETAJ, PROPRIETAR, NR_PERSOANE, SUPRAFATA ]
        [ 1, 'A1', 'A', 'adresa SCARA A',     1,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN,   'p1 ',      '0',    '25' ],
        [ 1, 'A1', 'A', 'adresa SCARA A',     2,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN, 'p2 ',      '1',    '2' ],
        [ 1, 'A1', 'A', 'adresa SCARA A',     3,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN, 'p3 ',      '2',    ' 77' ],
        
        [ 1, 'A1', 'B', 'adresa SCARA B',     1,   '4',    'p4 ',      '2',    4.44 ],
        
        [ 2, 'B1', 'A', 'adresa SCARA A',     1,   '3',    'p1 ',      '2',    '77' ],
        [ 2, 'B1', 'A', 'adresa SCARA A',     2,   '31',   'p1 ',      '12',   '11' ],
    ];

    protected $scariToRemove = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA ]
        [ 2, 'B1', 'A' ],
    ];

    protected $blocuriToRemove = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'A1' ],
    ];

    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );    
    }

    ////////////////////////////////////////////////////////////////////////////
   
    protected function generateInputValuesFromTestData( array $testData ) {}

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * Stergere scara;
     * Verificare nedisponibilitate spatii componente.
     * 
     * @dataProvider scariToRemoveDataProvider
     */
    public function test_RemoveScara( array $testData, array $credentials )
    {
        $scaraId = $this->getScaraIdFromTestData( $testData );
        $spatiiIds = App_Component_Spatiu_Factory::getInstance()->getSpatiiIdsByScaraId( $scaraId );
        
        $this->doLogin( $credentials );
        
        $this->doRequestWithToken( $this->getToken(), 'remove-scara', [
            App_Api_Scara_Remove::ARG_SCARA_ID => $scaraId
        ]);
        $this->assertResponseIsSuccessful();
        
        foreach( $spatiiIds as $spatiuId )
        {
            $null = App_Component_Spatiu_Factory::getInstance()->findActive( $spatiuId );
            
            $this->assertNull( $null, "Spatiu cu ID-ul $spatiuId nu este dezactivat." );
        }
    }
       
    /**
     * Stergere bloc;
     * Verificare nedisponibilitate spatii componente.
     * 
     * @dataProvider blocuriToRemoveDataProvider
     */
    public function test_RemoveBloc( array $testData, array $credentials )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        $spatiiIds = App_Component_Spatiu_Factory::getInstance()->getSpatiiIdsByBlocId( $blocId );
        
        $this->doLogin( $credentials );
        
        $this->doRequestWithToken( $this->getToken(), 'remove-bloc', [
            App_Api_Bloc_Remove::ARG_BLOC_ID => $blocId
        ]);
        $this->assertResponseIsSuccessful();
        
        foreach( $spatiiIds as $spatiuId )
        {
            $null = App_Component_Spatiu_Factory::getInstance()->findActive( $spatiuId );
            
            $this->assertNull( $null, "Spatiu cu ID-ul $spatiuId nu este dezactivat." );
        }
    }
       
    ////////////////////////////////////////////////////////////////////////////

    public function scariToRemoveDataProvider()
    {
        return $this->generateOutputData( $this->scariToRemove );
    }
    
    public function blocuriToRemoveDataProvider()
    {
        return $this->generateOutputData( $this->blocuriToRemove );
    }
    
}