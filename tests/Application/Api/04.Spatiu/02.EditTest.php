<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::editSpatiuAction
 * 
 */
class EditSpatiuTest extends SpatiuTestAbstract
{
    protected $_action = 'edit-spatiu';
    
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR, ETAJ, PROPRIETAR, NR_PERSOANE, SUPRAFATA ]
        [ 1, 'A1', 'A', 'adresa SCARA A',     1,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN,   'p1 ',      '0',    '25' ],
        [ 1, 'A1', 'A', 'adresa SCARA A',     2,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN, 'p2 ',      '1',    '2' ],
        [ 1, 'A1', 'A', 'adresa SCARA A',     3,   App_Component_Spatiu_Property_Etaj::VAL_MEZANIN, 'p3 ',      '2',    ' 77' ],
        
        [ 1, 'A1', 'B', 'adresa SCARA B',     1,   '4',    'p4 ',      '2',    4.44 ],
        
        [ 2, 'B1', 'A', 'adresa SCARA A',     1,   '3',    'p1 ',      '2',    '77' ],
        [ 2, 'B1', 'A', 'adresa SCARA A',     2,   '31',   'p1 ',      '12',   '11' ],
    ];

    protected $_dataForAuthenticationErrors = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR_SPATIU, 
        //          NUMAR_NOU, ETAJ_NOU, PROPRIETAR_NOU, NR_PERSOANE_NOU, SUPRAFATA_NOUA ]
        [ 1, 'A1', 'B', '1', 
                '2', '3', 'p4 ', '2', '44' ],
        [ 2, 'B1', 'A', '1', 
                '1', '1', 'p1 ', '2', '77' ],
    ];
    
    protected $_goodData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR_SPATIU, 
        //          NUMAR_NOU, ETAJ_NOU, PROPRIETAR_NOU, NR_PERSOANE_NOU, SUPRAFATA_NOUA ]
        [ 1, 'A1', 'A', '1',
                NULL, NULL, ' proprietar nou ', NULL, NULL ], // editare proprietar
        [ 1, 'A1', 'A', '1',
                NULL, ['5', 'p'], NULL, NULL, NULL ], // editare etaj
        [ 1, 'A1', 'A', '1',
                10, '9', 'pppp ', '3', ' 0444 '], // editare toate campurile
        [ 1, 'A1', 'A', '10',
                11, NULL, NULL, '1', '4'], // editare spatiu anterior
        [ 1, 'A1', 'A', '2',
                self::DATA_NUMAR_MAX_LENGTH, self::DATA_ETAJ_MAX, self::DATA_PROPRIETAR_MAX_LENGTH, self::DATA_PERSOANE_MAX, self::DATA_SUPRAFATA_MAX], // editare toate campurile
        
        [ 1, 'A1', 'B', '1',
                ' 1 ', ' 4 ', ' p4 ', ' 2 ', 4.4400000 ], // editare cu aceleasi valori
        
        [ 1, 'A1', 'B', '1',
                2, ' 4 ', '  p4 ', ' 2 ', ' 0004.44000 '], // editare numar cu o valoare a unui spatiu de la o alta scara
        
    ];

    protected $_dataForErrors = [
        /* [ [ expected_errors ], 
        //      [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR_SPATIU, 
        //              NUMAR_NOU, ETAJ_NOU, PROPRIETAR_NOU, NR_PERSOANE_NOU, SUPRAFATA_NOUA ],
         *          asociatie_account_id ]
         */
        [ [ App_Api_Spatiu_Edit::ERR_SPATIU_ID_INVALID ], 
                [ 2, 'B1', 'A', 3,
                    '', '', '', '', '' ] ],
        [ [ App_Api_Spatiu_Edit::ERR_SPATIU_ID_INVALID ], 
                [ 2, 'B1', 'A', 1,
                    '', '', '', '', '' ],
                        1 ],
        
        [ [ App_Api_Spatiu_Edit::ERR_NO_ARGUMENTS ], 
                [ 2, 'B1', 'A', 1,
                    NULL, NULL, NULL, NULL, NULL ] ],
        
        [ [ App_Api_Spatiu_Edit::ERR_ETAJ_REQUIRED, App_Api_Spatiu_Edit::ERR_NUMAR_REQUIRED, App_Api_Spatiu_Edit::ERR_PROPRIETAR_REQUIRED, App_Api_Spatiu_Edit::ERR_NR_PERS_REQUIRED, App_Api_Spatiu_Edit::ERR_SUPRAFATA_REQUIRED ],
                [ 2, 'B1', 'A', 1,
                    '', ['', '  '], '  ', '', '  ' ] ],
        
        [ [ App_Api_Spatiu_Edit::ERR_ETAJ_INVALID, App_Api_Spatiu_Edit::ERR_NUMAR_DUPLICATE, App_Api_Spatiu_Edit::ERR_PROPRIETAR_MAX_LENGTH, App_Api_Spatiu_Edit::ERR_SUPRAFATA_REQUIRED ],
                [ 2, 'B1', 'A', 1,
                    2, ['-11', 'a', self::DATA_ETAJ_MAX_1 ], self::DATA_PROPRIETAR_LONG_1, NULL, '' ] ],
        
        [ [ App_Api_Spatiu_Edit::ERR_NUMAR_MAX_LENGTH, App_Api_Spatiu_Edit::ERR_PROPRIETAR_MAX_LENGTH, App_Api_Spatiu_Edit::ERR_NR_PERS_REQUIRED ],
                [ 2, 'B1', 'A', 1,
                    [ self::DATA_NUMAR_LONG_1, self::DATA_NUMAR_LONG_N ], NULL, self::DATA_PROPRIETAR_LONG_N, '  ', NULL ] ],
        
        [ [ App_Api_Spatiu_Edit::ERR_NUMAR_DUPLICATE, App_Api_Spatiu_Edit::ERR_NR_PERS_INVALID, App_Api_Spatiu_Edit::ERR_PROPRIETAR_REQUIRED ],
                [ 2, 'B1', 'A', 1,
                    ' 2 ', NULL, '', [ '-1', 'a', '3.4' ], NULL ] ],
        
        [ [ App_Api_Spatiu_Edit::ERR_NUMAR_DUPLICATE, App_Api_Spatiu_Edit::ERR_NR_PERS_MAX_VALUE, App_Api_Spatiu_Edit::ERR_SUPRAFATA_INVALID ],
                [ 2, 'B1', 'A', 1,
                    '2', NULL, NULL, self::DATA_PERSOANE_MAX_1, [ 'a', '-1', '0010.110.00' ] ] ],
        
        [ [ App_Api_Spatiu_Edit::ERR_SUPRAFATA_MAX_VALUE, App_Api_Spatiu_Edit::ERR_NUMAR_REQUIRED, App_Api_Spatiu_Edit::ERR_PROPRIETAR_REQUIRED, App_Api_Spatiu_Edit::ERR_NR_PERS_REQUIRED ],
                [ 2, 'B1', 'A', 1,
                    [ '  ', '' ], NULL, '', '  ', self::DATA_SUPRAFATA_MAX_1 ] ],
        
        [ [ App_Api_Spatiu_Edit::ERR_ETAJ_INVALID, App_Api_Spatiu_Edit::ERR_PROPRIETAR_MAX_LENGTH, App_Api_Spatiu_Edit::ERR_NR_PERS_INVALID, App_Api_Spatiu_Edit::ERR_SUPRAFATA_MAX_VALUE ],
                [ 2, 'B1', 'A', 1,
                    NULL, 'a', self::DATA_PROPRIETAR_LONG_N, 'qq', self::DATA_SUPRAFATA_MAX_1 ] ],
        
    ];
    
    protected $dataForRemoveAndEdit = [
        // eliminare spatiu | editare spatiu din aceeasi scara
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, NUMAR, ALT_NUMAR ]
        [ 2, 'B1 ', ' A', 1, 2 ],
    ];

    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );
    }
    
    ////////////////////////////////////////////////////////////////////////////
   
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $spatiuId = $this->getSpatiuIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Spatiu_Edit::ARG_SPATIU_ID   => $spatiuId,
            App_Api_Spatiu_Edit::ARG_NUMAR       => @$testData[ 4 ],
            App_Api_Spatiu_Edit::ARG_ETAJ        => @$testData[ 5 ],
            App_Api_Spatiu_Edit::ARG_PROPRIETAR  => @$testData[ 6 ],
            App_Api_Spatiu_Edit::ARG_NR_PERS     => @$testData[ 7 ],
            App_Api_Spatiu_Edit::ARG_SUPRAFATA   => @$testData[ 8 ],
        ];
        
        return $inputValues;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * @dataProvider goodDataProvider
     * @param Array $testData
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        /* @var $Spatiu App_Component_Spatiu_Object */
        
        $this->_test_GoodData( $testData, $credentials, [
            'preProcess' => function( $inputValues, $credentials )
            {
                    $spatiuId = $inputValues[ App_Api_Spatiu_Edit::ARG_SPATIU_ID ];
                    
                    /* @var $Spatiu App_Component_Spatiu_Object */
                    $Spatiu = App_Component_Spatiu_Factory::getInstance()->findActive( $spatiuId );
                    
                    $initialValues = $Spatiu->toArray();
                    $initialParametri = $Spatiu->getMainParametri();
                    
                    $state = [
                        'spatiu_id'         => $spatiuId,
                        'initial_values'    => $initialValues,
                        'initial_parametri' => $initialParametri,
                    ];
                    
                    return $state;
            },
            
            'returnedDataAssertions' => function( $returnedData, $inputValues )
            {
                    $idKey = App_Api_Spatiu_Edit::ARG_SPATIU_ID;
                    $assertions = [
                        $idKey => [ 'Equals', $inputValues[ $idKey ] ],
                    ];

                    $etajKey = App_Api_Spatiu_Edit::ARG_ETAJ;
                    if( isset( $inputValues[ $etajKey ] ) )
                    {
                        $assertions[ $etajKey ]         = [ 'Equals', strtoupper( trim( $inputValues[ $etajKey ] ) ) ];
                        $assertions[ App_Api_Spatiu_Edit::ARG_ETAJ_ORDER ] = [ 'Equals', $this->getExpectedEtajOrderValue( $inputValues[ $etajKey ] ) ];
                    }
                    
                    $numarKey = App_Api_Spatiu_Edit::ARG_NUMAR;
                    if( isset( $inputValues[ $numarKey ] ) )
                    {
                        $assertions[ $numarKey ]        = [ 'Equals', trim( $inputValues[ $numarKey ] ) ];
                    }
                    
                    $proprietarKey = App_Api_Spatiu_Edit::ARG_PROPRIETAR;
                    if( isset( $inputValues[ $proprietarKey ] ) )
                    {
                        $assertions[ $proprietarKey ]   = [ 'Same', trim( $inputValues[ $proprietarKey ] ) ];
                    }
                    
                    $persoaneKey = App_Api_Spatiu_Edit::ARG_NR_PERS;
                    if( isset( $inputValues[ $persoaneKey ] ) )
                    {
                        $assertions[ $persoaneKey ]     = [ 'Equals', $this->getExpectedNrPersoaneValue( $inputValues[ $persoaneKey ] ) ];
                    }
                    
                    $suprafataKey = App_Api_Spatiu_Edit::ARG_SUPRAFATA;
                    if( isset( $inputValues[ $suprafataKey ] ) )
                    {
                        $assertions[ $suprafataKey ]     = [ 'Equals', $this->getExpectedSuprafataValue( $inputValues[ $suprafataKey ] ) ];
                    }
                    
                    return $assertions;
            },
            
            'postProcess' => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $initialValues      = $preProcessState[ 'initial_values' ];
                    $initialParametri   = $preProcessState[ 'initial_parametri' ];
                    $spatiuId           = $preProcessState[ 'spatiu_id' ];

                    //// verificare inregistrare in DB
                    
                    $expectedProperties = Lib_Tools::filterArray( $initialValues, [
                        App_Component_Spatiu_Object::PROP_ASOCIATIE_ID,
                        App_Component_Spatiu_Object::PROP_BLOC_ID,
                        App_Component_Spatiu_Object::PROP_SCARA_ID,
                        App_Component_Spatiu_Object::PROP_ADDING_MONTH,
                        App_Component_Spatiu_Object::PROP_STATUS,
                    ]);
        
                    $etajKey = App_Api_Spatiu_Edit::ARG_ETAJ;
                    if( isset( $inputValues[ $etajKey ] ) )
                    {
                        $expectedProperties[ $etajKey ]         = [ $this->getExpectedEtajOrderValue( $inputValues[ $etajKey ] ), true ];
                    }
                    
                    $numarKey = App_Api_Spatiu_Edit::ARG_NUMAR;
                    if( isset( $inputValues[ $numarKey ] ) )
                    {
                        $expectedProperties[ $numarKey ]        = trim( $inputValues[ $numarKey ] );
                    }
                    
                    $this->assertObjectExistsAndHasProperties( 'Spatiu', $spatiuId, $expectedProperties );
                    
                    //// verificare parametri
                    
                    $expectedParametri = [];
                    
                    $proprietarKey = App_Api_Spatiu_Edit::ARG_PROPRIETAR;
                    $expectedParametri[ $proprietarKey ] = isset( $inputValues[ $proprietarKey ] ) ?
                        trim( $inputValues[ $proprietarKey ] ) :
                        $initialParametri[ $proprietarKey ];
                    
                    $persoaneKey = App_Api_Spatiu_Edit::ARG_NR_PERS;
                    $expectedParametri[ $persoaneKey ] = isset( $inputValues[ $persoaneKey ] ) ?
                        $this->getExpectedNrPersoaneValue( $inputValues[ $persoaneKey ] ) :
                        $initialParametri[ $persoaneKey ];
                    
                    
                    $suprafataKey = App_Api_Spatiu_Edit::ARG_SUPRAFATA;
                    $expectedParametri[ $suprafataKey ] = isset( $inputValues[ $suprafataKey ] ) ?
                        $this->getExpectedSuprafataValue( $inputValues[ $suprafataKey ] ) :
                        $initialParametri[ $suprafataKey ];

                    $asociatieId = $this->getAsociatieIdFromCredentials( $credentials );
                    
                    foreach( $expectedParametri as $denumireParametru => $valoareParametru )
                    {
                        $criteria = [
                            App_Component_SpatiuParametru_Object::PROP_ASOCIATIE_ID    => $asociatieId,
                            App_Component_SpatiuParametru_Object::PROP_PARAMETRU       => $denumireParametru,
                            App_Component_SpatiuParametru_Object::PROP_VALUE           => $valoareParametru,
                            App_Component_SpatiuParametru_Object::PROP_REF_ID          => $spatiuId,
                        ];
                        
                        $this->assertEquals( 1, App_Component_SpatiuParametru_Factory::getInstance()->count( $criteria ), "Parametrul `$denumireParametru` nu a fost salvat sau este salvat incorect." );
                    }
            }
        ]);
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedObjects'  => function( $inputValues )
            {
                $objectConfig = [
                    [ 'Spatiu', $inputValues[ App_Component_Scara_Object::PROP_ID ] ],
                ];
                
                return $objectConfig;
            },
            'unchangedTables'   => [ 'Parametru_Spatiu', 'Spatiu' ],
        ]);
    }
    
    /**
     * Scenariu:
     * - eliminare spatiu dintr-o scara
     * - editare numar alt spatiu din aceeasi scara cu numarul spatiului eliminat
     * 
     * @dataProvider removeAndEditDataProvider
     * @depends test_GoodData
     */
    public function test_RemoveAndEdit( array $spatiuToRemove, array $testData, array $credentials )
    {
        $spatiuIdToRemove = $this->getSpatiuIdFromTestData( $spatiuToRemove );
        App_Component_Spatiu_Factory::getInstance()->find( $spatiuIdToRemove )->remove();
        
        $inputValues = $this->generateInputValuesFromTestData( $testData );
        
        $this->performAndTestAction([
            'inputValues'       => $inputValues,
            'credentials'       => $credentials,
        ]);
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function removeAndEditDataProvider()
    {
        $outputData = [];
        
        foreach( $this->dataForRemoveAndEdit as $dataSet )
        {
            $testData = $dataSet;
            $spatiuToRemove = $dataSet;
            
            // spatiuToRemove
            array_splice( $spatiuToRemove, 4 );
            
            // testData
            $numarNou = $testData[ 3 ];
            $testData[ 3 ] = $testData[ 4 ];
            $testData[ 4 ] = $numarNou;
            
            $credentials = $this->getCredentialsFromTestData( $testData );
            
            $this->addData( $outputData, $spatiuToRemove, $testData, $credentials );
        }
        
        return $outputData;
        
    }
}