<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::addScaraAction
 * 
 * Scenariu valid:
 * - autentificare; adaugare scara
 * 
 * Scenarii pentru verificare erori:
 * - request neautentificat
 * - request cu token gresit
 * - adaugare scara existenta
 * - denumire prea lunga
 * - adresa prea lunga
 * - adresa inexistenta
 * - denumire inexistenta
 * - depasire limita numar de scari
 * - depasire limita numar de operatii
 * 
 * Scenarii pentru verificarea absentei erorilor:
 * - { denumire prea lunga } se foloseste o denumire cu lungimea maxima admisa
 * - { adresa prea lunga } se foloseste o adresa cu lungimea maxima admisa
 * - { denumire duplicata } se sterge o scara si se re-adauga
 *  
 */
class AddScaraTest extends ScaraTestAbstract
{
    protected $_action = 'add-scara';
    
    protected $blocuriData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'A1' ],
        [ 1, 'A2' ],
        [ 2, 'B1' ],
    ];
    
    protected $_goodData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE, ADRESA ]
        [ 1, 'A1', [ 'a', ' aAa ' ], ' adresa `a`' ],
        [ 1, 'A2', ' scara SCARA ', ' adresa SCARA' ],
        [ 2, 'B1', ' aAa ', ' alta adresa ' ],
        [ 2, 'B1', ' scara SCARA ', ' adresa SCARA' ],
        [ 2, 'B1', self::DATA_DENUMIRE_MAX_LENGTH, ' adresa SCARA' ],
        [ 2, 'B1', ' B ', self::DATA_ADRESA_MAX_LENGTH ],
    ];
    
    protected $dataForRemoveAndAdd = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE, ADRESA ]
        [ 1, 'A1', 'a', ' adresa `a`' ],
        [ 1, 'A2', ' scara SCARA ', ' adresa SCARA' ],
        [ 2, 'B1', ' aAa ', ' alta adresa ' ],
    ];
    
    protected $_dataForErrors = [
        /* [ [ expected_errors ], 
         *      [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE, ADRESA ] ]
         */
        [ [ App_Api_Scara_Add::ERR_INVALID_BLOC ], 
                [ 1, 'A3', 'denumire', 'adresa' ] ],
        [ [ App_Api_Scara_Add::ERR_INVALID_BLOC ], 
                [ 1, 'A1', 'denumire', 'adresa' ],
                    2 ],
        [ [ App_Api_Scara_Add::ERR_DENUMIRE_REQUIRED ], 
                [ 1, 'A1', [ NULL, '', ' ' ], ' adresa `a`' ] ],
        [ [ App_Api_Scara_Add::ERR_ADRESA_REQUIRED ], 
                [ 1, 'A1', 'b', [ NULL, '', ' ' ] ] ],
        [ [ App_Api_Scara_Add::ERR_DENUMIRE_MAX_LENGTH ], 
                [ 1, 'A1', [ self::DATA_DENUMIRE_LONG_1, self::DATA_DENUMIRE_LONG_N ], 'adresa' ] ],
        [ [ App_Api_Scara_Add::ERR_ADRESA_MAX_LENGTH ], 
                [ 1, 'A1', 'b', [ self::DATA_ADRESA_LONG_1, self::DATA_ADRESA_LONG_N ] ] ],
        [ [ App_Api_Scara_Add::ERR_DENUMIRE_DUPLICATE ], 
                [ 1, 'A1', ' A ', 'adresa' ] ],
        [ [ App_Api_Scara_Add::ERR_DENUMIRE_DUPLICATE ], 
                [ 2, 'B1', 'aaa', 'adresa' ] ],
        [ [ App_Api_Scara_Add::ERR_DENUMIRE_REQUIRED, App_Api_Scara_Add::ERR_ADRESA_MAX_LENGTH ], 
                [ 2, 'B1', '', self::DATA_ADRESA_LONG_N ] ],
        [ [ App_Api_Scara_Add::ERR_DENUMIRE_MAX_LENGTH, App_Api_Scara_Add::ERR_ADRESA_REQUIRED ], 
                [ 2, 'B1', self::DATA_DENUMIRE_LONG_N, '' ] ],
        [ [ App_Api_Scara_Add::ERR_DENUMIRE_DUPLICATE, App_Api_Scara_Add::ERR_ADRESA_REQUIRED ], 
                [ 2, 'B1', 'aAa', '' ] ],
    ];
    
    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $credentialsSet = self::getAsociatiiCredentials();
        
        foreach ( $this->blocuriData as $row )
        {
            $asociatieId = $row[ 0 ];
            $denumireBloc = $row[ 1 ];
            
            $this->doLogin( $credentialsSet[ $asociatieId ] );
            $this->doRequestWithToken( $this->getToken(), 'add-bloc', [ App_Api_Bloc_Add::ARG_DENUMIRE => $denumireBloc ] );
            $this->assertResponseIsSuccessful();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Scenariu valid:
     * - autentificare
     * - adaugare scara
     * Verificari:
     * - raspunsul sa contina exact cheile { App_Api_Scara_Add::ARG_DENUMIRE, App_Api_Scara_Add::ARG_ADRESA, App_Api_Scara_Add::ARG_SCARA_ID }
     * - inregistrare in DB (verificare campuri: asociatie_id, bloc_id, denumire, adresa, adding_month, status)
     * 
     * @dataProvider goodDataProvider
     * 
     * @param Array $testData [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, ADRESA_SCARA ]
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'returnedDataAssertions'    => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Scara_Add::ARG_SCARA_ID     => 'NotEmpty',
                        App_Api_Scara_Add::ARG_BLOC_ID      => [ 'Equals', trim( $inputValues[ App_Api_Scara_Add::ARG_BLOC_ID ] ) ],
                        App_Api_Scara_Add::ARG_DENUMIRE     => [ 'Equals', trim( $inputValues[ App_Api_Scara_Add::ARG_DENUMIRE ] ) ],
                        App_Api_Scara_Add::ARG_ADRESA       => [ 'Equals', trim( $inputValues[ App_Api_Scara_Add::ARG_ADRESA ] ) ],
                    ];
                    
                    return $assertions;
            },
            
            'postProcess'               => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $Asociatie = $this->getAsociatieFromCredentials( $credentials );

                    // retrieve scara_id from returned data
                    $returnedData = $responseData[ Lib_Api::RESPONSE_DATA ];
                    $scaraId = $returnedData[ App_Api_Scara_Add::ARG_SCARA_ID ];

                    // verificare inregistrare in DB
                    $expectedProperties = [
                        App_Component_Scara_Object::PROP_ASOCIATIE_ID   => $Asociatie->getId(),
                        App_Component_Scara_Object::PROP_BLOC_ID        => intval( $inputValues[ App_Component_Scara_Object::PROP_BLOC_ID ] ),
                        App_Component_Scara_Object::PROP_DENUMIRE       => trim( $inputValues[ App_Component_Scara_Object::PROP_DENUMIRE ] ),
                        App_Component_Scara_Object::PROP_ADRESA         => trim( $inputValues[ App_Component_Scara_Object::PROP_ADRESA ] ),
                        App_Component_Scara_Object::PROP_ADDING_MONTH   => [ $Asociatie->getLunaCurenta(), true ],
                        App_Component_Scara_Object::PROP_STATUS         => App_Component_Scara_Object::VAL_STATUS_ACTIVE,
                    ];

                    $TestCase->assertObjectExistsAndHasProperties( 'Scara', $scaraId, $expectedProperties );
            }
        ]);
    }

    /**
     * @depends test_GoodData
     * @dataProvider removeAndAddDataProvider
     */
    public function test_RemoveAndAdd( array $testData, array $credentials )
    {
        $scaraIdToRemove = $this->getScaraIdFromTestData( $testData );
        App_Component_Scara_Factory::getInstance()->find( $scaraIdToRemove )->remove();

        $inputValues = $this->generateInputValuesFromTestData( $testData );

        $this->performAndTestAction([
            'inputValues'   => $inputValues, 
            'credentials'   => $credentials,
        ]);
    }
    
    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedTables'   => [ 'Scara' ],
        ] );
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider blocuriDataProvider
     * 
     * Scenariu:
     * - se sterge continutul tabelei
     * - adaug numarul maxim de blocuri
     * - urmatoarea adaugare trebuie sa genereze eroare
     * - sterg o scara
     * - adaug un alt bloc; operatia trebuie sa se finalizeze cu succes
     */
    public function test_Error_MaxEntries( array $testData, array $credentials )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        self::deleteDbEntries( $blocId );
        
        // autentificare
        $this->doLogin( $credentials );
        $token = $this->getToken();
        
        // adaugare numar maxim de scari
        for( $i = 0; $i < App_Component_Scara_Validator_Add::MAX_ENTRIES; $i++ )
        {
            $denumire = 'scara_' . $i;
            $adresa = 'adresa_scara_' . $i;
            
            $inputValues = [
                App_Component_Scara_Object::PROP_DENUMIRE   => $denumire,
                App_Component_Scara_Object::PROP_ADRESA     => $adresa,
                App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
            ];
            
            $this->doRequestWithToken( $token, $this->_action, $inputValues );
            $this->assertResponseIsSuccessful();
        }
        
        // se mai adauga o scara; trebuie sa se genereze eroare
        $denumireForError = 'scara_' . $i;
        $inputValuesForError = $inputValues;
        $inputValuesForError[ App_Component_Scara_Object::PROP_DENUMIRE ] = $denumireForError;

        $this->doRequestWithToken( $token, $this->_action, $inputValuesForError );
        $this->assertResponseHasErrors( App_Api_Scara_Add::ERR_MAX_ENTRIES );
        
        // stergere ultima scara adaugata
        $denumireForRemove = 'scara_' . ( $i - 1 );
        $scaraValuesForRemove = [
            App_Component_Scara_Object::PROP_DENUMIRE   => $denumireForRemove,
            App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
        ];
        $Scara = App_Component_Scara_Factory::getInstance()->findActive( $scaraValuesForRemove );
        $Scara->remove();
        
        // adaugare scara; operatia trebuie sa reuseasca
        $inputValuesForAdd = $inputValues;
        $inputValuesForAdd[ App_Component_Scara_Object::PROP_DENUMIRE ] = $denumireForError;

        $this->doRequestWithToken( $token, $this->_action, $inputValuesForAdd );
        $this->assertResponseIsSuccessful();
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider blocuriDataProvider
     * 
     * Scenariu:
     * - adaugare, stergere - ciclul se repeta pana la limita
     * - adaugare => eroare
     */
    public function test_Error_MaxOperations( array $testData, array $credentials )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        self::deleteDbEntries( $blocId );
        
        // autentificare
        $this->doLogin( $credentials );
        $token = $this->getToken();
        
        // executare numar maxim de operatii
        for( $i = 0; $i < App_Component_Scara_Validator_Add::MAX_OPERATIONS; $i++ )
        {
            $denumire = 'scara_' . $i;
            $adresa = 'adresa_scara_' . $i;
            
            $inputValues = [
                App_Component_Scara_Object::PROP_DENUMIRE   => $denumire,
                App_Component_Scara_Object::PROP_ADRESA     => $adresa,
                App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
            ];
            
            // adaugare
            $this->doRequestWithToken( $token, $this->_action, $inputValues );
            $this->assertResponseIsSuccessful();
            
            // stergere
            $scaraId = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Component_Scara_Object::PROP_ID ];
            $Scara = App_Component_Scara_Factory::getInstance()->find( $scaraId );
            $Scara->remove();
        }
        
        // se incearca adaugarea unei noi scari; trebuie sa se genereze eroare
        $denumire = 'scara_' . $i;
        $inputValuesForError = [
            App_Component_Scara_Object::PROP_DENUMIRE   => $denumire,
            App_Component_Scara_Object::PROP_ADRESA     => $adresa,
            App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
        ];

        $this->doRequestWithToken( $token, $this->_action, $inputValuesForError );
        $this->assertResponseHasErrors( App_Api_Scara_Add::ERR_MAX_OPERATIONS );
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @return array of [ test_data, credentials ]
     */
    public function blocuriDataProvider()
    {
        $outputData = [];
        
        foreach( $this->blocuriData as $testData )
        {
            $credentials = $this->getCredentialsFromTestData( $testData );
            
            $this->addData( $outputData, $testData, $credentials );
        }
        
        return $outputData;
    }

    public function removeAndAddDataProvider()
    {
        return $this->generateOutputData( $this->dataForRemoveAndAdd );
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Scara_Add::ARG_BLOC_ID      => $blocId,
            App_Api_Scara_Add::ARG_DENUMIRE     => $testData[ 2 ],
            App_Api_Scara_Add::ARG_ADRESA       => $testData[ 3 ],
        ];
        
        return $inputValues;
    }
    
}