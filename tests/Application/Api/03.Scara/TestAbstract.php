<?php

abstract class ScaraTestAbstract extends Test_Application_Api_Abstract
{
    const DATA_DENUMIRE_LONG_1      = '**denumire_long_1**';
    const DATA_DENUMIRE_LONG_N      = '**denumire_long_N**';
    const DATA_DENUMIRE_MAX_LENGTH  = '**denumire_max_length**';
    const DATA_ADRESA_LONG_1        = '**adresa_long_1**';
    const DATA_ADRESA_LONG_N        = '**adresa_long_N**';
    const DATA_ADRESA_MAX_LENGTH    = '**adresa_max_length**';

    protected function cleanDb()
    {
        // truncate `bloc` table
        $blocDb = App_Component_Bloc_Db::getInstance();
        $blocDb->truncate();
        
        // truncate `scara` table
        $scaraDb = App_Component_Scara_Db::getInstance();
        $scaraDb->truncate();
    }

    protected function deleteDbEntries( $blocId )
    {
        $scaraDb = App_Component_Scara_Db::getInstance();
        
        $scaraDb->delete([
            App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
        ]);
    }
    
    protected function addContentIntoDb( array $dbContent )
    {
        foreach ( $dbContent as $dataRow )
        {
            $credentials = $this->getCredentialsFromTestData( $dataRow );
            $this->doLogin( $credentials );
            
            $Bloc = App_Component_Bloc_Factory::getInstance()->findActive([
                App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $dataRow[ 0 ],
                App_Component_Bloc_Object::PROP_DENUMIRE        => $dataRow[ 1 ],
            ]);
            
            if( !$Bloc )
            {
                $blocValues = [
                    App_Api_Bloc_Add::ARG_DENUMIRE => $dataRow[ 1 ],
                ];
                $this->doRequestWithToken( $this->getToken(), 'add-bloc', $blocValues );
                $this->assertResponseIsSuccessful();
                
                $blocId = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Api_Bloc_Add::ARG_BLOC_ID ];
            }
            else
            {
                $blocId = $Bloc->getId();
            }
            
            $scaraValues = [
                App_Api_Scara_Add::ARG_BLOC_ID      => $blocId,
                App_Api_Scara_Add::ARG_DENUMIRE     => $dataRow[ 2 ],
                App_Api_Scara_Add::ARG_ADRESA       => $dataRow[ 3 ],
            ];
            
            $this->doRequestWithToken( $this->getToken(), 'add-scara', $scaraValues );
            $this->assertResponseIsSuccessful();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////

    protected function defineDataValues()
    {
        return [
            self::DATA_ADRESA_MAX_LENGTH    => Lib_Tools::generateRandomString( App_Component_Scara_Validator_EntryAbstract::ADRESA_MAX_LENGTH ),
            self::DATA_ADRESA_LONG_1        => Lib_Tools::generateRandomString( App_Component_Scara_Validator_EntryAbstract::ADRESA_MAX_LENGTH + 1 ),
            self::DATA_ADRESA_LONG_N        => Lib_Tools::generateRandomString( App_Component_Scara_Validator_EntryAbstract::ADRESA_MAX_LENGTH + 1, App_Component_Scara_Validator_EntryAbstract::ADRESA_MAX_LENGTH * 10 ),
            self::DATA_DENUMIRE_MAX_LENGTH  => Lib_Tools::generateRandomString( App_Component_Scara_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH ),
            self::DATA_DENUMIRE_LONG_1      => Lib_Tools::generateRandomString( App_Component_Scara_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH + 1 ),
            self::DATA_DENUMIRE_LONG_N      => Lib_Tools::generateRandomString( App_Component_Scara_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH + 1, App_Component_Scara_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH * 10 ),
        ];
    }
    
    protected function getBlocIdFromTestData( array $testData )
    {
        $Bloc = App_Component_Bloc_Factory::getInstance()->findActive([
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $testData[ 0 ],
            App_Component_Bloc_Object::PROP_DENUMIRE        => $testData[ 1 ],
        ]);
        
        $blocId = $Bloc ? $Bloc->getId() : 0;
        
        return $blocId;
    }
    
    protected function getScaraIdFromTestData( array $testData )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        
        $Scara = App_Component_Scara_Factory::getInstance()->findActive([
            App_Component_Scara_Object::PROP_BLOC_ID    => $blocId,
            App_Component_Scara_Object::PROP_DENUMIRE   => $testData[ 2 ],
        ]);
        
        $scaraId = $Scara ? $Scara->getId() : 0;
        
        return $scaraId;
    }
    
}