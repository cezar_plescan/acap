<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::editScaraAction
 * 
 * Scenariu valid:
 * - autentificare; editare scara
 * 
 */
class EditScaraTest extends ScaraTestAbstract
{
    protected $_action = 'edit-scara';
    
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, ADRESA_SCARA ]
        [ 1, 'A1', 'A', 'adresa SCARA A' ],
        [ 1, 'A1', 'B', 'adresa SCARA B' ],
        
        [ 1, 'A2', 'A', 'adresa SCARA A' ],
        
        [ 2, 'B1', 'A', 'adresa A' ],
        [ 2, 'B1', 'B', 'adresa B' ],
    ];

    protected $_dataForAuthenticationErrors = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, DENUMIRE_NOUA, ADRESA_NOUA ]
        [ 1, 'A1 ', 'A', ' A* ', NULL ],
        [ 1, 'A1 ', 'B', ' A** ', NULL ],
        [ 1, 'A2', 'A', ' A*', ' adresa scara A * ' ],
        [ 2, 'B1', 'A', 'A*', ' addddddresa' ],
    ];
    
    protected $_goodData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, DENUMIRE_NOUA, ADRESA_NOUA ]
        [ 1, 'A1 ', ' A ', ' A* ', NULL ],
        [ 1, 'A1 ', ' A* ', 'A*', NULL ], // se editeaza aceeasi scara de 2 ori, cu aceeasi denumire
        [ 1, ' A1 ', ' B', NULL, ' adresa scara B *' ],
        [ 1, 'A2', 'A ', ' A*', ' adresa scara A * ' ], // se foloseste o denumire a unei scari de la alt bloc
        [ 2, 'B1', ' A', ' A * ', NULL ],
        [ 2, 'B1', ' B', self::DATA_DENUMIRE_MAX_LENGTH, self::DATA_ADRESA_MAX_LENGTH ],
    ];

    protected $_dataForErrors = [
        /* [ [ expected_errors ], 
         *      [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, DENUMIRE, ADRESA ],
         *          asociatie_credentials ]
         */
        [ [ App_Api_Scara_Edit::ERR_INVALID_SCARA ],
                [ 1, 'A1', 'scara_inexistenta', 'ddd', 'aaa' ] ],
        [ [ App_Api_Scara_Edit::ERR_INVALID_SCARA ], 
                [ 2, 'B1', 'A *', 'ddd', NULL ],
                    1 ],
        [ [ App_Api_Scara_Edit::ERR_DENUMIRE_REQUIRED ], 
                [ 1, 'A1', 'A*', [ '', ' ' ], NULL ] ],
        [ [ App_Api_Scara_Edit::ERR_ADRESA_REQUIRED ], 
                [ 1, 'A1', 'A*', NULL, [ '', ' ' ] ] ],
        [ [ App_Api_Scara_Edit::ERR_NO_ARGUMENTS ], 
                [ 1, 'A1', 'A*', NULL, NULL ] ],
        [ [ App_Api_Scara_Edit::ERR_DENUMIRE_MAX_LENGTH ], 
                [ 1, 'A1', 'a*', [ self::DATA_DENUMIRE_LONG_1, self::DATA_DENUMIRE_LONG_N ], 'adresa oarecare' ] ],
        [ [ App_Api_Scara_Edit::ERR_ADRESA_MAX_LENGTH ], 
                [ 1, 'A1', 'b', 'aaa', [ self::DATA_ADRESA_LONG_1, self::DATA_ADRESA_LONG_N ] ] ],
        [ [ App_Api_Scara_Edit::ERR_DENUMIRE_DUPLICATE ], 
                [ 1, 'A1', ' A* ', ' b ', NULL ] ],
        [ [ App_Api_Scara_Edit::ERR_DENUMIRE_DUPLICATE ], 
                [ 1, 'A1', 'a*', 'b', 'alta adresa' ] ],
        [ [ App_Api_Scara_Edit::ERR_DENUMIRE_REQUIRED, App_Api_Scara_Edit::ERR_ADRESA_MAX_LENGTH ], 
                [ 2, 'B1', 'A *', '', self::DATA_ADRESA_LONG_N ] ],
        [ [ App_Api_Scara_Edit::ERR_DENUMIRE_MAX_LENGTH, App_Api_Scara_Edit::ERR_ADRESA_REQUIRED ], 
                [ 2, 'B1', 'A *', self::DATA_DENUMIRE_LONG_N, '' ] ],
        [ [ App_Api_Scara_Edit::ERR_DENUMIRE_DUPLICATE, App_Api_Scara_Edit::ERR_ADRESA_REQUIRED ], 
                [ 1, 'A1', 'b', ' a*', '' ] ],
    ];
    
    protected $dataForRemoveAndEdit = [
        // eliminare scara | editare scara din acelasi bloc
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, DENUMIRE_ALTA_SCARA ]
        [ 1, 'A1 ', 'A*', 'B' ],
    ];

    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );
    }
    
    ////////////////////////////////////////////////////////////////////////////
   
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $scaraId = $this->getScaraIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Scara_Edit::ARG_SCARA_ID    => $scaraId,
            App_Api_Scara_Edit::ARG_DENUMIRE    => $testData[ 3 ],
            App_Api_Scara_Edit::ARG_ADRESA      => $testData[ 4 ],
        ];
        
        return $inputValues;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * Scenariu valid:
     * - autentificare
     * - editare scara
     * Verificari:
     * - raspunsul sa contina exact cheile { App_Api_Scara_Edit::ARG_DENUMIRE, App_Api_Scara_Edit::ARG_ADRESA, App_Api_Scara_Edit::ARG_SCARA_ID }
     * - inregistrare in DB (verificare campuri: id, asociatie_id, bloc_id, denumire, adresa, adding_month, status)
     * 
     * @dataProvider goodDataProvider
     * 
     * @param Array $testData [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, DENUMIRE_NOUA ]
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'preProcess'                => function( $inputValues, $credentials )
            {
                    // extragere informatii despre scara, pentru a fi folosite la verificarea de la pct. 2
                    $scaraId = $inputValues[ App_Api_Scara_Edit::ARG_SCARA_ID ];
                    $Scara = App_Component_Scara_Factory::getInstance()->findActive( $scaraId );
                    $initialValues = $Scara->toArray();

                    $state = [
                        'scara_id'          => $scaraId,
                        'initial_values'    => $initialValues,
                    ];
                    
                    return $state;
            },
            
            'returnedDataAssertions'    => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Scara_Edit::ARG_SCARA_ID    => [ 'Equals', $inputValues[ App_Api_Scara_Edit::ARG_SCARA_ID ] ],
                    ];

                    if( isset( $inputValues[ App_Api_Scara_Edit::ARG_DENUMIRE ] ) )
                    {
                        $assertions[ App_Api_Scara_Edit::ARG_DENUMIRE ] = [ 'Same', trim( $inputValues[ App_Api_Scara_Edit::ARG_DENUMIRE ] ) ];
                    }

                    if( isset( $inputValues[ App_Api_Scara_Edit::ARG_ADRESA ] ) )
                    {
                        $assertions[ App_Api_Scara_Edit::ARG_ADRESA ] = [ 'Same', trim( $inputValues[ App_Api_Scara_Edit::ARG_ADRESA ] ) ];
                    }
                    
                    return $assertions;
            },
            
            'postProcess'               => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $initialValues  = $preProcessState[ 'initial_values' ];
                    $scaraId        = $preProcessState[ 'scara_id' ];

                    // verificare inregistrare in DB
                    $expectedProperties = [
                        App_Component_Scara_Object::PROP_ASOCIATIE_ID   => $initialValues[ App_Component_Scara_Object::PROP_ASOCIATIE_ID ],
                        App_Component_Scara_Object::PROP_BLOC_ID        => $initialValues[ App_Component_Scara_Object::PROP_BLOC_ID ],
                        App_Component_Scara_Object::PROP_ADDING_MONTH   => $initialValues[ App_Component_Scara_Object::PROP_ADDING_MONTH ],
                        App_Component_Scara_Object::PROP_STATUS         => $initialValues[ App_Component_Scara_Object::PROP_STATUS ],
                    ];

                    if( isset( $inputValues[ App_Component_Scara_Object::PROP_DENUMIRE ] ) )
                    {
                        $expectedProperties[ App_Component_Scara_Object::PROP_DENUMIRE ] = trim( $inputValues[ App_Component_Scara_Object::PROP_DENUMIRE ] );
                    }

                    if( isset( $inputValues[ App_Component_Scara_Object::PROP_ADRESA ] ) )
                    {
                        $expectedProperties[ App_Component_Scara_Object::PROP_ADRESA ] = trim( $inputValues[ App_Component_Scara_Object::PROP_ADRESA ] );
                    }

                    $TestCase->assertObjectExistsAndHasProperties( 'Scara', $scaraId, $expectedProperties );
            }
        ]);
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedObjects'  => function( $inputValues )
            {
                $objectConfig = [
                    [ 'Scara', $inputValues[ App_Component_Scara_Object::PROP_ID ] ],
                ];
                
                return $objectConfig;
            },
        ]);
    }
    
    /**
     * Scenariu:
     * - eliminare scara dintr-un bloc
     * - editare alta scara din acelasi bloc cu denumirea celei eliminate
     * 
     * @dataProvider removeAndEditDataProvider
     * @depends test_GoodData
     */
    public function test_RemoveAndEdit( array $scaraToRemove, array $testData, array $credentials )
    {
        $scaraIdToRemove = $this->getScaraIdFromTestData( $scaraToRemove );
        App_Component_Scara_Factory::getInstance()->find( $scaraIdToRemove )->remove();
        
        $inputValues = $this->generateInputValuesFromTestData( $testData );
        
        $this->performAndTestAction([
            'inputValues'       => $inputValues,
            'credentials'       => $credentials,
        ]);
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function removeAndEditDataProvider()
    {
        $outputData = [];
        
        foreach( $this->dataForRemoveAndEdit as $dataSet )
        {
            $testData = $dataSet;
            $scaraToRemove = $dataSet;
            
            // scaraToRemove
            array_splice( $scaraToRemove, 3 );
            
            // testData
            $denumireNoua = $testData[ 2 ];
            $testData[ 2 ] = $testData[ 3 ];
            $testData[ 3 ] = $denumireNoua; // denumire
            $testData[ 4 ] = NULL; // adresa
            
            $credentials = $this->getCredentialsFromTestData( $testData );
            
            $this->addData( $outputData, $scaraToRemove, $testData, $credentials );
        }
        
        return $outputData;
        
    }
}