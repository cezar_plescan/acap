<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 */
class UnavailabilityScaraTest extends ScaraTestAbstract
{
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, ADRESA_SCARA ]
        [ 1, 'A1', 'A', 'adresa SCARA A' ],
        [ 1, 'A1', 'B', 'adresa SCARA B' ],
        
        [ 1, 'A2', 'A', 'adresa SCARA A' ],
        
        [ 2, 'B1', 'A', 'adresa A' ],
        [ 2, 'B1', 'B', 'adresa B' ],
    ];

    protected $blocuriToRemove = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'A1' ],
        [ 2, 'B1' ],
    ];

    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );    
    }

    ////////////////////////////////////////////////////////////////////////////
   
    protected function generateInputValuesFromTestData( array $testData ) {}

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * Stergere bloc;
     * Verificare nedisponibilitate
     * 
     * @dataProvider blocuriToRemoveDataProvider
     */
    public function test_RemoveBloc( array $testData, array $credentials )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        $scariIds = App_Component_Scara_Factory::getInstance()->getScariIdsByBlocId( $blocId );
        
        $this->doLogin( $credentials );
        
        $this->doRequestWithToken( $this->getToken(), 'remove-bloc', [
            App_Api_Bloc_Remove::ARG_BLOC_ID => $blocId
        ]);
        $this->assertResponseIsSuccessful();
        
        foreach( $scariIds as $scaraId )
        {
            $null = App_Component_Scara_Factory::getInstance()->findActive( $scaraId );
            
            $this->assertNull( $null, "Scara cu ID-ul $scaraId nu este dezactivata." );
        }
    }
       
    ////////////////////////////////////////////////////////////////////////////

    public function blocuriToRemoveDataProvider()
    {
        return $this->generateOutputData( $this->blocuriToRemove );
    }
    
}