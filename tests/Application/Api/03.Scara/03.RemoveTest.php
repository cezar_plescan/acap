<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::removeScaraAction
 * 
 */
class RemoveScaraTest extends ScaraTestAbstract
{
    protected $_action = 'remove-scara';
    
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA, ADRESA_SCARA ]
        [ 1, 'A1', 'A', 'adresa SCARA A' ],
        [ 1, 'A1', 'B', 'adresa SCARA B' ],
        
        [ 1, 'A2', 'A', 'adresa SCARA A' ],
        
        [ 2, 'B1', 'A', 'adresa A' ],
        [ 2, 'B1', 'B', 'adresa B' ],
    ];

    protected $_dataForAuthenticationErrors = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA ]
        [ 1, 'A1 ', 'A', ],
        [ 2, 'B1', 'B', ],
    ];
    
    protected $_goodData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA ]
        [ 1, 'A1 ', ' A ' ],
        [ 1, 'A2', 'A ' ],
        [ 2, 'B1', ' B' ],
    ];

    protected $_dataForErrors = [
        /* [ [ expected_errors ], 
         *      [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA ],
         *          asociatie_credentials ]
         */
        // scara inexistenta
        [ [ App_Api_Scara_Remove::ERR_INVALID_SCARA ],
                [ 1, 'A1', 'scara_inexistenta' ] ],
        // scara eliminata
        [ [ App_Api_Scara_Remove::ERR_INVALID_SCARA ],
                [ 1, 'A1', [ 'A', ' a '] ] ],
        // scara apartinand altei asociatii
        [ [ App_Api_Scara_Remove::ERR_INVALID_SCARA ], 
                [ 2, 'B1', 'A' ],
                    1 ],
    ];
    
    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );    
    }

    ////////////////////////////////////////////////////////////////////////////
   
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $scaraId = $this->getScaraIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Scara_Remove::ARG_SCARA_ID    => $scaraId,
        ];
        
        return $inputValues;
    }

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * @dataProvider goodDataProvider
     * 
     * @param Array $testData [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_SCARA ]
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'preProcess'                => function( $inputValues, $credentials )
            {
                    // extragere informatii despre scara, inainte de eliminare
                    $scaraId = $inputValues[ App_Api_Scara_Remove::ARG_SCARA_ID ];
                    $Scara = App_Component_Scara_Factory::getInstance()->find( $scaraId );
                    $initialValues = $Scara->toArray();

                    $state = [
                        'scara_id'          => $scaraId,
                        'initial_values'    => $initialValues,
                    ];
                    
                    return $state;
            },
            
            'returnedDataAssertions'    => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Scara_Remove::ARG_SCARA_ID    => [ 'Equals', $inputValues[ App_Api_Scara_Remove::ARG_SCARA_ID ] ],
                    ];

                    return $assertions;
            },
            
            'postProcess'               => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $initialValues  = $preProcessState[ 'initial_values' ];
                    $scaraId        = $preProcessState[ 'scara_id' ];

                    // verificare inregistrare in DB
                    $expectedProperties = [
                        App_Component_Scara_Object::PROP_ASOCIATIE_ID   => $initialValues[ App_Component_Scara_Object::PROP_ASOCIATIE_ID ],
                        App_Component_Scara_Object::PROP_BLOC_ID        => $initialValues[ App_Component_Scara_Object::PROP_BLOC_ID ],
                        App_Component_Scara_Object::PROP_ADDING_MONTH   => $initialValues[ App_Component_Scara_Object::PROP_ADDING_MONTH ],
                        App_Component_Scara_Object::PROP_DENUMIRE       => $initialValues[ App_Component_Scara_Object::PROP_DENUMIRE ],
                        App_Component_Scara_Object::PROP_ADRESA         => $initialValues[ App_Component_Scara_Object::PROP_ADRESA ],
                        App_Component_Scara_Object::PROP_STATUS         => App_Component_Scara_Object::VAL_STATUS_DELETED,
                    ];

                    $TestCase->assertObjectExistsAndHasProperties( 'Scara', $scaraId, $expectedProperties );
            }
        ]);
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedObjects'  => function( $inputValues )
            {
                $objectConfig = [
                    [ 'Scara', $inputValues[ App_Component_Scara_Object::PROP_ID ] ],
                ];
                
                return $objectConfig;
            },
        ]);
    }
    
}