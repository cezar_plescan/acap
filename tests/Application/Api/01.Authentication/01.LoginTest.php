<?php

/**
 * @covers ApiController::loginAction
 * @covers ApiController::logoutAction
 * @covers ApiController::getIdentityAction
 * @covers ApiController::checkTokenAction
 */
class LoginTest extends Test_Application_Api_Controller
{
    /**
     * @dataProvider goodDataProvider
     */
    public function test_GoodData( array $credentials )
    {
        // perform a login
        $returnData = $this->performAndTestLogin( $credentials );
        
        // request the identity
        $this->performAndTestGetIdentity( $returnData[ App_Api_Authenticate_Login::ARG_ASOCIATIE_ID ] );

        // verify the token
        $this->performAndTestCheckToken( $returnData[ App_Api_Authenticate_Login::ARG_TOKEN ] );

        // perform a logout
        $this->performAndTestLogout();

        // perform a logout (second time)
        $this->performAndTestLogout();
        
        // do a new login
        $this->performAndTestLogin( $credentials );
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider invalidCredentialsDataProvider
     * @param array $credentials
     */
    public function test_Login_Errors_InvalidCredentials( array $credentials )
    {
        $this->performAndTestLoginWithErrors( $credentials, App_Component_Authentication_Validator_Authenticate::ERR_INVALID );        
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider accountInactiveDataProvider
     * @param array $credentials
     */
    public function test_Login_Errors_AccountInactive( array $credentials )
    {
        $this->performAndTestLoginWithErrors( $credentials, App_Component_Authentication_Validator_Authenticate::ERR_INVALID );
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider accountBlockedDataProvider
     * @param array $credentials
     */
    public function test_Login_Errors_AccountBlocked( array $credentials )
    {
        $this->performAndTestLoginWithErrors( $credentials, App_Component_Authentication_Validator_Authenticate::ERR_INVALID );
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider accountDeletedDataProvider
     * @param array $credentials
     */
    public function test_Login_Errors_AccountDeleted( array $credentials )
    {
        $this->performAndTestLoginWithErrors( $credentials, App_Component_Authentication_Validator_Authenticate::ERR_INVALID );
    }
    
    /**
     * @depends test_GoodData
     * 
     */
    public function test_CheckToken_Errors_NotAuthenticated()
    {
        $token = 'aaa';
        
        /* scenario 1:
         * - initially there is no user authenticated
         * - make the request
         */
        $this->performAndTestCheckTokenWithErrors( $token, App_Component_Authentication_Validator_Token::ERR_NOT_AUTHENTICATED );
        
        /* scenario 2:
         * - login with valid credentials
         * - logout
         * - make the request
         */
        $goodCredentials = array_values( self::getAsociatiiCredentials() )[ 0 ];
        $this->doLogin( $goodCredentials );
        $this->doLogout();
        
        $this->performAndTestCheckTokenWithErrors( $token, App_Component_Authentication_Validator_Token::ERR_NOT_AUTHENTICATED );        
    }
    
    public function test_CheckToken_Errors_InvalidToken()
    {
        // 1. login
        $credentials_1 = array_values( self::getAsociatiiCredentials() )[ 0 ];
        $this->doLogin( $credentials_1 );
        $token_1 = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Api_Authenticate_Login::ARG_TOKEN ];
        
        // 2. use invalid token
        $this->performAndTestCheckTokenWithErrors( 'a' , App_Component_Authentication_Validator_Token::ERR_INVALID );
        
        // 3. login again with the same credentials
        $this->doLogin( $credentials_1 );
        $token_1_1 = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Api_Authenticate_Login::ARG_TOKEN ];

        // 4. use the previous token
        $this->performAndTestCheckTokenWithErrors( $token_1 , App_Component_Authentication_Validator_Token::ERR_INVALID );
        
        // 5. login again with other credentials
        $credentials_2 = array_values( self::getAsociatiiCredentials() )[ 1 ];
        $this->doLogin( $credentials_2 );

        // 6. use the previous token
        $this->performAndTestCheckTokenWithErrors( $token_1_1 , App_Component_Authentication_Validator_Token::ERR_INVALID );
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    protected function performAndTestLogin( array $credentials )
    {
        // perform a login
        $this->doLogin( $credentials );
        $this->checkLoginResponse( $credentials[ App_Api_Authenticate_Login::ARG_USERNAME ] );
        
        // check if the user identity is stored
        $this->checkIfIdentityIsStored( $credentials[ App_Api_Authenticate_Login::ARG_USERNAME ] );
        
        $returnData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        
        return $returnData;
    }
    
    /**
     * The response should return the ID and the token
     * 
     * @param string $username
     */
    protected function checkLoginResponse( $username )
    {
        $responseData = $this->assertResponseIsSuccessfulWithDataKey();
        $returnedData = $responseData[ Lib_Api::RESPONSE_DATA ];
        
        $Asociatie = $this->getAsociatieByUsername( $username );
        
        // check that the data returned is an array with both keys
        $this->assertArrayHasKey( App_Api_Authenticate_Login::ARG_ASOCIATIE_ID, $returnedData );
        $this->assertArrayHasKey( App_Api_Authenticate_Login::ARG_TOKEN, $returnedData );
        $this->assertCount( 2, $returnedData );
        
        // check the ID
        $this->assertEquals( $Asociatie->getId(), $returnedData[ App_Api_Authenticate_Login::ARG_ASOCIATIE_ID ], 'The ID is incorrect.' );
        
        // check the token (only if it exists and has a minimum length)
        $this->assertGreaterThanOrEqual( 32, strlen( $returnedData[ App_Api_Authenticate_Login::ARG_TOKEN ] ), 'Token too short or absent.' );
    }
    
    /**
     * Check if the user identity is stored
     * 
     * @param string $username
     * @return boolean
     */
    protected function checkIfIdentityIsStored( $username )
    {
        $Identity = Lib_Authentication_Manager::getWorker()->getIdentity();
        
        $this->assertInstanceOf( 'App_Component_Asociatie_Object', $Identity, 'No or incorrect identity stored.' );
        
        // check if the Identity is valid
        $Asociatie = $this->getAsociatieByUsername( $username );
        
        $expectedProperties = $Asociatie->toArray();
        unset( $expectedProperties[ App_Component_Asociatie_Object::PROP_TIMESTAMP ],
               $expectedProperties[ App_Component_Asociatie_Object::PROP_UPDATED_AT ] );
        
        $actualProperties = $Identity->toArray();
        unset( $actualProperties[ App_Component_Asociatie_Object::PROP_TIMESTAMP ],
               $actualProperties[ App_Component_Asociatie_Object::PROP_UPDATED_AT ] );
        
        $this->assertEquals( $expectedProperties, $actualProperties, 'The stored identity is invalid.' );
    }
    
    /**
     * Retrieves the current identity
     * 
     * @param int $asociatieId
     */
    protected function performAndTestGetIdentity( $asociatieId )
    {
        $this->doRequest( 'get-identity' );
        $this->assertResponseIsSuccessfulWithDataKey( 'Response does not contain the identity summary.' );
        
        $Asociatie = $this->getAsociatieById( $asociatieId );
        
        $expectedResponse = [
            App_Api_Authenticate_GetIdentity::ARG_ASOCIATIE_ID => $Asociatie->getId(),
            App_Api_Authenticate_GetIdentity::ARG_USERNAME     => $Asociatie->getUsername(),            
        ];
        
        $actualResponse = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        
        $this->assertEquals( $expectedResponse, $actualResponse, 'Identity is incorrect.' );
    }
    
    /**
     * 
     * @param string $token
     */
    protected function performAndTestCheckToken( $token )
    {
        $this->doRequestWithToken( $token, 'check-token' );
        $this->assertResponseIsSuccessfulWithoutDataKey();
    }
    
    protected function performAndTestLogout()
    {
        // perform a logout
        $this->doLogout();
        $this->assertResponseIsSuccessfulWithoutDataKey();
        
        // check if the user was de-authenticated
        $this->checkIfNoIdentity();
    }

    protected function checkIfNoIdentity()
    {
        $this->doRequest( 'get-identity' );
        $this->assertResponseIsNotSuccessful( 'Identity is still stored.' );
    }
    
    protected function performAndTestLoginWithErrors( array $credentials, $errorCode )
    {
        ////scenario 1:

        // perform a login with good credentials
        $goodCredentials = array_values( self::getAsociatiiCredentials() )[ 0 ];
        $this->doLogin( $goodCredentials );
        
        // perform a login with invalid credentials
        $this->performAndTestLoginWithInvalidData( $credentials, $errorCode );
        
        // check if the user is still logged in
        $this->checkIfIdentityIsStored( $goodCredentials[ App_Api_Authenticate_Login::ARG_USERNAME ] );
        
        ////scenario 2:
        
        // logout
        $this->doLogout();
        
        // perform a login with invalid credentials
        $this->performAndTestLoginWithInvalidData( $credentials, $errorCode );
        
        // check if there is no identity
        $this->checkIfNoIdentity();
        
    }
    
    protected function performAndTestLoginWithInvalidData( array $credentials, $errorCode )
    {
        $this->doLogin( $credentials );
        $this->assertResponseHasErrors( $errorCode );
    }
    
    protected function performAndTestCheckTokenWithErrors( $token, $errorCode )
    {
        $this->doRequestWithToken( $token , 'check-token' );
        $this->assertResponseHasErrors( $errorCode );
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function goodDataProvider()
    {
        $dataSet = [];
        
        foreach( self::getAsociatiiCredentials() as $credentials )
        {
            // store original values
            $c = $credentials;
            
            // 1. original values
            $this->addData( $dataSet, $credentials );
            
            // 2. pad username with spaces
            $credentials = $c;
            $credentials[ App_Api_Authenticate_Login::ARG_USERNAME ] = 
                Lib_Tools::generateRandomString( 1, 4, ' ' ) .
                $credentials[ App_Api_Authenticate_Login::ARG_USERNAME ] .
                Lib_Tools::generateRandomString( 1, 4, ' ' );
            
            $this->addData( $dataSet, $credentials );
            
            // 3. add unnecessary argument
            $credentials = $c;
            $credentials[ 'unnecessary_argument' ] = 'aaa';
            
            $this->addData( $dataSet, $credentials );
        }

        return $dataSet;
    }

    public function invalidCredentialsDataProvider()
    {
        $dataSet = [];
        
        // invalid username and password
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '',
        ]);
        
        // invalid username and password
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '11111',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '11111',
        ]);
        
        // invalid username and password
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '__123456__',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '123456',
        ]);
        
        // valid username, invalid password
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '123456',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '____',
        ]);
        
        // valid username, password empty
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '123456',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '',
        ]);
        
        // valid username, missing password
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '123456',
        ]);
        
        // missing username
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_PASSWORD     => '____',
        ]);
        
        // missing username and password
        $this->addData( $dataSet, []);
        
        // missing username and password, useless input
        $this->addData( $dataSet, [
            'aaaa'      => 'bbbb',
        ]);
        
        return $dataSet;
    }
    
    public function accountInactiveDataProvider()
    {
        $dataSet = [];
        
        // good credentials, but account inactive
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '25478354_1',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '25478354',
        ]);
        
        return $dataSet;
    }
    
    public function accountBlockedDataProvider()
    {
        $dataSet = [];
        
        // good credentials, but account blocked
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '25478354_2',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '25478354',
        ]);
        
        return $dataSet;
    }
    
    public function accountDeletedDataProvider()
    {
        $dataSet = [];
        
        // good credentials, but account deleted
        $this->addData( $dataSet, [
            App_Api_Authenticate_Login::ARG_USERNAME     => '25478354_3',
            App_Api_Authenticate_Login::ARG_PASSWORD     => '25478354',
        ]);
        
        return $dataSet;
    }
    
}