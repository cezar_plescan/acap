<?php

abstract class Test_Application_Api_Abstract extends Test_Application_Api_Controller
{
    protected $_goodData = [];
    protected $_dataForErrors = [];
    protected $_dataForAuthenticationErrors = [];
    
    protected $_dataValues = [];
    
    abstract protected function cleanDb();
    abstract protected function setupDbFixture();
    abstract protected function defineDataValues();
    abstract protected function generateInputValuesFromTestData( array $testData );

    protected function __runBeforeAllTests()
    {
        $this->cleanDb();
        $this->setupDbFixture();
    }

    ////////////////////////////////////////////////////////////////////////////
    
    protected function _test_GoodData( array $testData, array $credentials, array $testConfig = [] )
    {
        $inputValues = $this->generateInputValuesFromTestData( $testData );
        
        $this->performAndTestAction([
            'inputValues'       => $inputValues,
            'credentials'       => $credentials,
        ] + $testConfig );
    }

    protected function _test_Errors_Authentication( array $testData, array $credentials )
    {
        $inputValues = $this->generateInputValuesFromTestData( $testData );

        $this->performAndTestAction([
            'inputValues'       => $inputValues, 
            'expectedErrors'    => App_Api_Authenticate_Abstract::ERR_NOT_AUTHENTICATED,
            'token'             => function(){
                                        $tokens = [ '', NULL, 'any-token' ];
                                        return $tokens;
                                   },
        ]);
                                   
        $this->performAndTestAction([
            'inputValues'       => $inputValues, 
            'credentials'       => $credentials, 
            'expectedErrors'    => App_Api_Authenticate_Abstract::ERR_INVALID_TOKEN,
            'token'             => function( $goodToken ){
                                        $tokens = [ '', NULL, $goodToken . '_' ];
                                        return $tokens;
                                   },
        ]);
    }

    protected function _test_Errors( array $testData, array $expectedErrors, array $credentials, array $extraTestConfig = [] )
    {
        $inputValues = $this->generateInputValuesFromTestData( $testData );
        
        $this->performAndTestAction([
            'inputValues'       => $inputValues, 
            'credentials'       => $credentials, 
            'expectedErrors'    => $expectedErrors,
        ] + $extraTestConfig );
    }
    
    
    ////////////////////////////////////////////////////////////////////////////

    protected function performAndTestAction( array $config )
    {
        $inputValues            = (array)@$config[ 'inputValues' ];
        $credentials            = (array)@$config[ 'credentials' ];
        $expectedErrors         = @$config[ 'expectedErrors' ];
        $returnedDataAssertions = @$config[ 'returnedDataAssertions' ];
        $postProcess            = @$config[ 'postProcess' ];
        $preProcess             = @$config[ 'preProcess' ]; // should return an array
        $unchangedObjectsConfig = @$config[ 'unchangedObjects' ]; // should be an array or a function that returns an array
        $unchangedTablesConfig  = @$config[ 'unchangedTables' ]; // should be an array
        
        //// before request ////
        
        $preProcessState = is_callable( $preProcess ) ? 
                $preProcess( $inputValues, $credentials ) :
                [];
        
        $unchangedObjects = [];
        if( $unchangedObjectsConfig )
        {
            if( is_callable( $unchangedObjectsConfig ) )
            {
                $unchangedObjectsConfig = $unchangedObjectsConfig( $inputValues, $credentials );
            }
            
            if( !is_array( $unchangedObjectsConfig ) )
            {
                throw new Exception( '`unchangedObjects` config should be an array or a function that returns an array' );
            }
            
            foreach( $unchangedObjectsConfig as $unchangedObjectConfig )
            {
                $objectType = $unchangedObjectConfig[ 0 ];
                $objectId   = $unchangedObjectConfig[ 1 ];
                
                $Object = $this->getComponentObject( $objectType, $objectId );
                if( $Object )
                {
                    $unchangedObjects[ $objectType ][ $objectId ] = $Object->toArray();
                }
            }
        }
        
        $unchangedTables = [];
        if( $unchangedTablesConfig )
        {
            foreach( $unchangedTablesConfig as $componentName )
            {
                $snapshot = $this->getTableSnapshot( $componentName );
                
                $unchangedTables[ $componentName ] = $snapshot;
            }
        }
        
        if( $credentials )
        {
            // autentificare
            $this->doLogin( $credentials );
        }
        else
        {
            //$this->doLogout();
        }
        
        $token = $this->getToken();
                
        if( isset( $config[ 'token' ] ) )
        {
            $tokenOption = $config[ 'token' ];
            if( is_callable( $tokenOption ) )
            {
                $token = $tokenOption( $token );
            }
            else
            {
                $token = $tokenOption;
            }
        }

        // force token to be a list
        $token = (array)$token;
        foreach( $token as $t )
        {
            // trimitere request (token-ul poate sau nu sa existe)
            $this->doRequestWithToken( $t, $this->_action, $inputValues );
            $responseData = $this->getResponseData();
            
            if( $expectedErrors )
            {
                $this->assertResponseHasErrors( $expectedErrors );
            }
            elseif( $returnedDataAssertions )
            {
                $this->assertResponseIsSuccessfulWithDataKey();
                $this->assertReturnedDataCompliesWith( 
                        is_callable( $returnedDataAssertions ) ?
                            $returnedDataAssertions( $responseData[ Lib_Api::RESPONSE_DATA ], $inputValues ) :
                            $returnedDataAssertions
                );
            }
            else
            {
                $this->assertResponseIsSuccessful();
            }

            //// after request ////
            
            if( $unchangedTablesConfig )
            {
                foreach( $unchangedTablesConfig as $componentName )
                {
                    $snapshot = $this->getTableSnapshot( $componentName );

                    $this->assertSame( $unchangedTables[ $componentName ], $snapshot, "Tabela `$componentName` a fost modificată." );
                }
            }

            if( $unchangedObjects )
            {
                foreach( $unchangedObjects as $objectType => $typeGroup )
                {
                    foreach( $typeGroup as $objectId => $objectProperties )
                    {
                        $CurrentObject = $this->assertObjectExistsInDb( $objectType, $objectId );
                        $this->assertEquals( $objectProperties, $CurrentObject->toArray(), "Obiectul `$objectType` cu ID-ul $objectId a fost modificat." );
                    }
                }
            }
            
            if( is_callable( $postProcess ) )
            {
                $postProcess( $preProcessState, $inputValues, $credentials, $responseData, $this );
            }
        }
        
    }
    
    protected function assertReturnedDataCompliesWith( array $assertions )
    {
        $returnedData = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ];
        
        // valoarea returnata sa fie un array cu un anumit numar de elemente
        $this->assertInternalType( 'array', $returnedData );
        $this->assertCount( count( $assertions ), $returnedData );

        // perform assertions
        foreach( $assertions as $returnedDataKey => $terms )
        {
            $terms = (array)$terms;
            
            $assertionType  = $terms[ 0 ];
            $assertArguments = [ $returnedData[ $returnedDataKey ] ];
            
            if( count( $terms ) == 2 )
            {
                $expectedValue  = $terms[ 1 ];
                array_unshift( $assertArguments, $expectedValue );
            }

            // afisare mesaj
            $assertArguments[] = "Raspunsul nu contine parametrul `$returnedDataKey` sau acesta este gresit.";
            
            call_user_func_array( [ $this, 'assert'.$assertionType ], $assertArguments );
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * 
     * @param type $componentName
     * @param type $id
     * @return Lib_Component_ORM_Object
     */
    protected function assertObjectExistsInDb( $componentName, $id )
    {
        // read from DB the record having that ID
        $Object = $this->getComponentObject( $componentName, $id );
        $this->assertNotNull( $Object, 'Obiectul cu ID-ul ' . $id . ' nu exista.' );
        
        return $Object;
    }

    /**
     * 
     * @param Lib_Component_ORM_Object $Object
     * @param array $propertiesAssertions
     */
    protected function assertObjectHasProperties( Lib_Component_ORM_Object $Object, array $propertiesAssertions )
    {
        foreach( $propertiesAssertions as $property => $comparison )
        {
            $comparison = (array)$comparison;
            
            $expectedValue = $comparison[ 0 ];
            $assertType = empty( $comparison[ 1 ] ) ? 'Same' : 'Equals';
            
            $message = "Campul `$property` nu a fost salvat corect.";
            
            $this->{'assert'.$assertType}( $expectedValue, $Object->getProperty( $property ), $message );
        }
    }
    
    /**
     * 
     * @param type $componentName
     * @param type $id
     * @param array $properties
     */
    protected function assertObjectExistsAndHasProperties( $componentName, $id, array $properties)
    {
        $Object = $this->assertObjectExistsInDb( $componentName, $id );
        $this->assertObjectHasProperties( $Object, $properties );
    }

    ////////////////////////////////////////////////////////////////////////////

    protected function generateOutputData( $dataSet )
    {
        $outputData = [];
        
        foreach( $dataSet as $testDataRow )
        {
            $dataRows = $this->parseDataRow( $testDataRow );
            foreach( $dataRows as $dataRow )
            {
                $credentials = $this->getCredentialsFromTestData( $dataRow );
                $this->addData( $outputData, $dataRow, $credentials );
            }
        }
        
        return $outputData;
    }

    /**
     * Se foloseste pentru a defini variabila $_goodData
     * @overridable 
     * @return array
     */
    protected function _getGoodDataProviderValues()
    {
        return NULL;
    }
    
    private function populateGoodDataVariable()
    {
        $values = $this->_getGoodDataProviderValues();

        if( $values !== NULL )
        {
            if( !is_array( $values ) )
            {
                throw new UnexpectedValueException( "_getGoodDataProviderValues nu a returnat un Array." );
            }

            $this->_goodData = $values;
        }
    }

    /**
     * 
     * @return array of [ test_data, credentials ]
     */
    public function goodDataProvider()
    {
        $this->populateGoodDataVariable();
        
        return $this->generateOutputData( $this->_goodData );
    }

    /**
     * 
     * @return array of [ test_data, credentials ]
     */
    public function authenticationErrorsDataProvider()
    {
        if( empty( $this->_dataForAuthenticationErrors ) )
        {
            $this->populateGoodDataVariable();
        
            $this->_dataForAuthenticationErrors = $this->_goodData;
        }
        
        return $this->generateOutputData( $this->_dataForAuthenticationErrors );
    }
    
    /**
     * Se foloseste pentru a defini variabila $_dataForErrors
     * @overridable 
     * @return array
     */
    protected function _getErrorsDataProviderValues()
    {
        return NULL;
    }
    
    /**
     * 
     * @return array of [ test_data, error_codes, credentials ]
     */
    public function dataForErrorsProvider()
    {
        $values = $this->_getErrorsDataProviderValues();
        
        if( $values !== NULL )
        {
            if( !is_array( $values ) )
            {
                throw new UnexpectedValueException( "_getErrorsDataProviderValues nu a returnat un Array." );
            }
            
            $this->_dataForErrors = $values;
        }
        
        $outputData = [];
        
        foreach( $this->_dataForErrors as $dataSet )
        {
            $errors = (array)$dataSet[ 0 ];
            $testData = $dataSet[ 1 ];
            $authenticatedId = @$dataSet[ 2 ];
            
            $dataRows = $this->parseDataRow( $testData );
            foreach( $dataRows as $dataRow )
            {
                $credentials = isset( $authenticatedId ) ?
                        self::getAsociatiiCredentials()[ $authenticatedId ] :
                        $this->getCredentialsFromTestData( $dataRow );
                $this->addData( $outputData, $dataRow, $errors, $credentials );
            }
        }
        
        return $outputData;
    }

    /**
     * 
     * @return array of [ credentials ]
     */
    public function credentialsDataProvider()
    {
        $data = [];
        
        $credentials = array_values( self::getAsociatiiCredentials() );
        $this->addData( $data, $credentials );
                
        return $data;
    }
    
    /**
     * 
     * @return array of [ credentials ]
     */
    public function oneCredentialDataProvider()
    {
        $data = [];
        
        $credentials = array_values( self::getAsociatiiCredentials() )[ 0 ];
        $this->addData( $data, $credentials );
                
        return $data;
    }
    
    ////////////////////////////////////////////////////////////////////////////

    private function parseDataRow( array $dataRow )
    {
        $parsedData = [];
        
        // look up for multiple values (stored as array)
        $hasMultipleValues = false;
        foreach( $dataRow as $index => $value )
        {
            if( is_array( $value ) )
            {
                if( $hasMultipleValues )
                {
                    throw new Exception( 'Test data has more than one multiple value.' );
                }

                foreach( $value as $v )
                {
                    $dataRow[ $index ] = $v;

                    $testDataParsed = $this->parseTestValues( $dataRow );
                    $parsedData[] = $testDataParsed;
                }

                $hasMultipleValues = true;
            }
        }

        if( !$hasMultipleValues )
        {
            $testDataParsed = $this->parseTestValues( $dataRow );
            $parsedData[] = $testDataParsed;
        }
        
        return $parsedData;
    }
    
    private function parseTestValues( array $values )
    {
        foreach( $values as $index => $value )
        {
            $values[ $index ] = $this->parseTestValue( $value );
        }
        
        return $values;
    }
    
    private function parseTestValue( $value )
    {
        if( empty( $this->_dataValues ) )
        {
            $this->_dataValues = $this->defineDataValues();
        }
        
        $value = is_null( $value ) ? NULL : (string)$value;
        
        $parsedValue = array_key_exists( $value, $this->_dataValues ) ? $this->_dataValues[ $value ] : $value;
        
        return $parsedValue;
    }
    
    protected function getCredentialsFromTestData( array $testData )
    {
        $asociatieId = $testData[ 0 ];
        
        $credentials = self::getAsociatiiCredentials()[ $asociatieId ];
        
        return $credentials;
    }

    protected function getComponentObject( $componentName, $id )
    {
        // read from DB the record having that ID
        $factoryClass = "App_Component_{$componentName}_Factory";
        $Object = $factoryClass::getInstance()->find( $id );
        
        return $Object;
    }
    
    private function getTableSnapshot( $componentName )
    {
        $dbClassname = "App_Component_{$componentName}_Db";
        /* @var $db Lib_Component_ORM_Db */
        $db = $dbClassname::getInstance();
        
        $snapshot = $db->count();
        
        return $snapshot;
    }
    
}