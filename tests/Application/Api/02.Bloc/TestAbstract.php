<?php

abstract class BlocTestAbstract extends Test_Application_Api_Abstract
{
    const DATA_DENUMIRE_LONG_1      = '**denumire_long_1**';
    const DATA_DENUMIRE_LONG_N      = '**denumire_long_N**';
    const DATA_DENUMIRE_MAX_LENGTH  = '**denumire_max_length**';
    
    protected function cleanDb()
    {
        // truncate `bloc` table
        $blocBd = App_Component_Bloc_Db::getInstance();
        $blocBd->truncate();
    }

    protected function addContentIntoDb( array $dbContent )
    {
        foreach ( $dbContent as $dataRow )
        {
            $credentials = $this->getCredentialsFromTestData( $dataRow );
            $this->doLogin( $credentials );
            
            $blocValues = [
                App_Api_Bloc_Add::ARG_DENUMIRE => $dataRow[ 1 ],
            ];
            $this->doRequestWithToken( $this->getToken(), 'add-bloc', $blocValues );
            $this->assertResponseIsSuccessful();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    protected function defineDataValues()
    {
        return [
            self::DATA_DENUMIRE_MAX_LENGTH  => Lib_Tools::generateRandomString( App_Component_Bloc_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH ),
            self::DATA_DENUMIRE_LONG_1      => Lib_Tools::generateRandomString( App_Component_Bloc_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH + 1 ),
            self::DATA_DENUMIRE_LONG_N      => Lib_Tools::generateRandomString( App_Component_Bloc_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH + 1, App_Component_Bloc_Validator_EntryAbstract::DENUMIRE_MAX_LENGTH * 10 ),
        ];
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    protected function getBlocIdFromTestData( array $testData )
    {
        $Bloc = App_Component_Bloc_Factory::getInstance()->findActive([
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $testData[ 0 ],
            App_Component_Bloc_Object::PROP_DENUMIRE        => $testData[ 1 ],
        ]);
        
        $blocId = $Bloc ? $Bloc->getId() : 0;
        
        return $blocId;
    }
    
    
    
}