<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::removeBlocAction
 * 
 * Scenariu valid:
 * - autentificare
 * - eliminare bloc
 * 
 * Scenarii pentru verificare erori:
 * - request neautentificat
 * - request cu token gresit
 * - bloc invalid
 *
 */
class RemoveBlocTest extends BlocTestAbstract
{
    protected $_action = 'remove-bloc';

    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'A1' ], //
        [ 1, 'A_2' ], //
        [ 1, 'B 11' ],
        
        [ 2, 'A1' ],
        [ 2, 'A2' ],
        [ 2, 'A5' ],
        [ 2, 'B 11' ], //
    ];
    
    protected $_dataForAuthenticationErrors = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'A1 ', ],
        [ 2, 'B 11' ],
    ];
    
    protected $_goodData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'A1' ],
        [ 1, ' a_2' ],
        [ 2, ' b 11  ' ],
    ];

    protected $_dataForErrors = [
        /* [ [ expected_errors ], 
         *      [ ASOCIATIE_ID, DENUMIRE_BLOC ],
         *          asociatie_credentials ]
         */
        // scara inexistenta
        [ [ App_Api_Bloc_Remove::ERR_INVALID_BLOC_ID ],
                [ 1, 'bloc_inexistent' ] ],
        // bloc eliminata
        [ [ App_Api_Bloc_Remove::ERR_INVALID_BLOC_ID ],
                [ 1, [ ' a1 ', ' a_2 '] ] ],
        // bloc apartinand altei asociatii
        [ [ App_Api_Bloc_Remove::ERR_INVALID_BLOC_ID ], 
                [ 2, 'A5 ' ],
                    1 ],
    ];
    
    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );    
    }
    
    ////////////////////////////////////////////////////////////////////////////
   
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Bloc_Edit::ARG_BLOC_ID    => $blocId,
        ];
        
        return $inputValues;
    }

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * @dataProvider goodDataProvider
     * 
     * @param Array $testData [ ASOCIATIE_ID, DENUMIRE_BLOC ]
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'preProcess' => function( $inputValues, $credentials )
            {
                    // extragere informatii despre bloc, inainte de eliminare
                    $blocId = $inputValues[ App_Api_Bloc_Remove::ARG_BLOC_ID ];
                    $Bloc = App_Component_Bloc_Factory::getInstance()->find( $blocId );
                    $initialValues = $Bloc->toArray();

                    $state = [
                        'bloc_id'           => $blocId,
                        'initial_values'    => $initialValues,
                    ];
                    
                    return $state;
            },
            
            'returnedDataAssertions' => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Bloc_Remove::ARG_BLOC_ID    => [ 'Equals', $inputValues[ App_Api_Bloc_Remove::ARG_BLOC_ID ] ],
                    ];

                    return $assertions;
            },
            
            'postProcess' => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $initialValues  = $preProcessState[ 'initial_values' ];
                    $blocId         = $preProcessState[ 'bloc_id' ];

                    // verificare inregistrare in DB
                    $expectedProperties = [
                        App_Component_Bloc_Object::PROP_ASOCIATIE_ID   => $initialValues[ App_Component_Bloc_Object::PROP_ASOCIATIE_ID ],
                        App_Component_Bloc_Object::PROP_ADDING_MONTH   => $initialValues[ App_Component_Bloc_Object::PROP_ADDING_MONTH ],
                        App_Component_Bloc_Object::PROP_DENUMIRE       => $initialValues[ App_Component_Bloc_Object::PROP_DENUMIRE ],
                        App_Component_Bloc_Object::PROP_STATUS         => App_Component_Bloc_Object::VAL_STATUS_DELETED,
                    ];

                    $TestCase->assertObjectExistsAndHasProperties( 'Bloc', $blocId , $expectedProperties );
            }
        ]);
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedObjects'  => function( $inputValues )
            {
                $objectConfig = [
                    [ 'Bloc', $inputValues[ App_Component_Bloc_Object::PROP_ID ] ],
                ];
                
                return $objectConfig;
            },
        ]);
    }
    
    ////////////////////////////////////////////////////////////////////////////

    
    
}