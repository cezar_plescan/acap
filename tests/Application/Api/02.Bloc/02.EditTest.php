<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::editBlocAction
 * 
 * Scenariu valid:
 * - autentificare
 * - editare bloc
 * 
 * Scenarii pentru verificare erori:
 * - request neautentificat
 * - request cu token gresit
 * - denumire existenta
 * - denumire prea lunga
 * - denumire inexistenta
 * - bloc invalid
 *
 */
class EditBlocTest extends BlocTestAbstract
{
    protected $_action = 'edit-bloc';
    
    protected $dbFixture = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'A1' ],
        [ 1, 'A_2' ],
        [ 1, 'B 11' ],
        
        [ 2, 'A1' ],
        [ 2, 'A2' ],
        [ 2, 'A5' ],
        [ 2, 'B 11' ],
    ];
    
    protected $_dataForAuthenticationErrors = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_NOUA ]
        [ 1, 'A1', 'A1*' ],
        [ 1, 'A_2', 'A_2*' ],
        [ 2, 'A5', 'A5*' ],
        [ 2, 'B 11', 'B 11*' ],
    ];
    
    protected $_goodData = [
        // ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_NOUA
        [ 1, 'A1', ' A2 ' ],
        [ 1, 'A2', 'A2' ], // editare cu aceeasi denumire
        [ 1, 'A_2', ' A3 ' ],
        [ 1, 'B 11', 'B2' ],
        
        [ 2, 'A1', ' B2  ' ],
        [ 2, 'A5', 'A22' ],
        [ 2, 'B 11', self::DATA_DENUMIRE_MAX_LENGTH ],
    ];

    protected $_dataForErrors = [
        /* [ [ expected_errors ], 
         *      [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_NOUA ],
         *          asociatie_credentials ]
         */
        [ [ App_Api_Bloc_Edit::ERR_INVALID_BLOC_ID ],
                [ 1, 'bloc_inexistent', '_denumire_' ] ],
        [ [ App_Api_Bloc_Edit::ERR_INVALID_BLOC_ID ], 
                [ 2, 'B2', '_denumire_' ],
                    1 ],
        [ [ App_Api_Bloc_Edit::ERR_DENUMIRE_REQUIRED ], 
                [ 1, 'A2', [ '', ' ', NULL ] ] ],
        [ [ App_Api_Bloc_Edit::ERR_DENUMIRE_MAX_LENGTH ], 
                [ 1, 'A2', [ self::DATA_DENUMIRE_LONG_1, self::DATA_DENUMIRE_LONG_N ] ] ],
        [ [ App_Api_Bloc_Edit::ERR_DENUMIRE_DUPLICATE ], 
                [ 1, 'A2', ' a3 ' ] ],
        [ [ App_Api_Bloc_Edit::ERR_DENUMIRE_DUPLICATE ], 
                [ 2, ' a22 ', 'b2' ] ],
    ];
    
    protected $dataForRemoveAndEdit = [
        // eliminare bloc | editare alt bloc
        // [ ASOCIATIE_ID, DENUMIRE_BLOC, DENUMIRE_ALT_BLOC ]
        [ 1, 'A2 ', 'B2' ],
    ];

    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        $this->addContentIntoDb( $this->dbFixture );
    }
    
    ////////////////////////////////////////////////////////////////////////////
   
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $blocId = $this->getBlocIdFromTestData( $testData );
        
        $inputValues = [
            App_Api_Bloc_Edit::ARG_BLOC_ID  => $blocId,
            App_Api_Bloc_Edit::ARG_DENUMIRE => $testData[ 2 ],
        ];
        
        return $inputValues;
    }
    
    ////////////////////////////////////////////////////////////////////////////

    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * Scenariu valid:
     * - autentificare
     * - editare bloc
     * Verificari:
     * - raspunsul sa contina exact cheile { App_Api_Bloc_Edit::ARG_DENUMIRE, App_Api_Bloc_Edit::ARG_BLOC_ID }
     * - inserare inregistrare in DB (verificare campuri: asociatie_id, denumire, adding_month, status)
     * 
     * @dataProvider goodDataProvider
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'preProcess'                => function( $inputValues, $credentials )
            {
                    // extragere informatii despre bloc, pentru a fi folosite la verificarea de la pct. 2
                    $blocId = $inputValues[ App_Api_Bloc_Edit::ARG_BLOC_ID ];
                    $Bloc = App_Component_Bloc_Factory::getInstance()->findActive( $blocId );
                    $initialValues = $Bloc->toArray();

                    $state = [
                        'bloc_id'           => $blocId,
                        'initial_values'    => $initialValues,
                    ];
                    
                    return $state;
            },
            
            'returnedDataAssertions'    => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Bloc_Edit::ARG_BLOC_ID    => [ 'Equals', $inputValues[ App_Api_Bloc_Edit::ARG_BLOC_ID ] ],
                    ];

                    $assertions[ App_Api_Bloc_Edit::ARG_DENUMIRE ] = [ 'Same', trim( $inputValues[ App_Api_Bloc_Edit::ARG_DENUMIRE ] ) ];
                    
                    return $assertions;
            },
            
            'postProcess'               => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $initialValues  = $preProcessState[ 'initial_values' ];
                    $blocId         = $preProcessState[ 'bloc_id' ];

                    // verificare inregistrare in DB
                    $expectedProperties = [
                        App_Component_Bloc_Object::PROP_DENUMIRE        => trim( $inputValues[ App_Component_Bloc_Object::PROP_DENUMIRE ] ),
                        App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $initialValues[ App_Component_Bloc_Object::PROP_ASOCIATIE_ID ],
                        App_Component_Bloc_Object::PROP_ADDING_MONTH    => $initialValues[ App_Component_Bloc_Object::PROP_ADDING_MONTH ],
                        App_Component_Bloc_Object::PROP_STATUS          => $initialValues[ App_Component_Bloc_Object::PROP_STATUS ],
                    ];

                    $TestCase->assertObjectExistsAndHasProperties( 'Bloc', $blocId, $expectedProperties );
            }
        ]);
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedObjects'  => function( $inputValues )
            {
                $objectConfig = [
                    [ 'Scara', $inputValues[ App_Component_Scara_Object::PROP_ID ] ],
                ];
                
                return $objectConfig;
            },
        ]);
    }
    
    /**
     * Scenariu:
     * - stergere un bloc
     * - editare alt bloc cu denumirea blocului sters
     * Rezultat asteptat:
     * - operatie reusita
     * 
     * (acesta va fi ultimul test care se ruleaza din cauza ca aduce modificari asupra continutului tabelei)
     * 
     * @dataProvider removeAndEditDataProvider
     * @depends test_GoodData
     */
    public function test_RemoveAndEdit( array $blocToRemove, array $testData, array $credentials )
    {
        $blocIdToRemove = $this->getBlocIdFromTestData( $blocToRemove );
        App_Component_Bloc_Factory::getInstance()->find( $blocIdToRemove )->remove();
        
        $inputValues = $this->generateInputValuesFromTestData( $testData );
        
        $this->performAndTestAction([
            'inputValues'       => $inputValues,
            'credentials'       => $credentials,
        ]);
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function removeAndEditDataProvider()
    {
        $outputData = [];
        
        foreach( $this->dataForRemoveAndEdit as $dataSet )
        {
            $testData = $dataSet;
            $blocToRemove = $dataSet;
            
            // blocToRemove
            array_splice( $blocToRemove, 2 );
            
            // testData
            $denumireNoua = $testData[ 1 ];
            $testData[ 1 ] = $testData[ 2 ];
            $testData[ 2 ] = $denumireNoua;
            
            $credentials = $this->getCredentialsFromTestData( $testData );
            
            $this->addData( $outputData, $blocToRemove, $testData, $credentials );
        }
        
        return $outputData;
    }

}