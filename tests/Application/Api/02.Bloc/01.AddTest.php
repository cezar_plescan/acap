<?php

require_once 'TestAbstract.php';

/**
 * @group structura
 * 
 * @covers ApiController::addBlocAction
 * 
 * Scenariu valid:
 * - autentificare
 * - adaugare bloc
 * 
 * Scenarii pentru verificare erori:
 * - request neautentificat
 * - request cu token gresit
 * - adaugare bloc existent
 * - denumire prea lunga
 * - denumire inexistenta
 * - depasire limita numar de blocuri
 * - depasire limita numar de operatii
 *  */
class AddBlocTest extends BlocTestAbstract
{
    protected $_action = 'add-bloc';
    
    protected $_goodData = [
        // [ ASOCIATIE_ID, DENUMIRE_BLOC ]
        [ 1, 'a' ], 
        [ 1, ' aAa ' ], 
        [ 1, ' B ' ], 
        [ 2, ' B ' ],
        [ 2, self::DATA_DENUMIRE_MAX_LENGTH ],
    ];
    
    protected $_dataForErrors = [
        [  App_Api_Bloc_Add::ERR_DENUMIRE_REQUIRED, 
            [ 1, [ NULL, '  ', '' ] ] ],
        [  App_Api_Bloc_Add::ERR_DENUMIRE_DUPLICATE, 
            [ 1, [ ' A ', ' a ', ' AaA   ' ] ] ],
        [  App_Api_Bloc_Add::ERR_DENUMIRE_MAX_LENGTH, 
            [ 1, [ self::DATA_DENUMIRE_LONG_1, self::DATA_DENUMIRE_LONG_N ] ] ],
    ];

    protected $dataForRemoveAndAdd = [
        [ 1, ' aAa ' ], 
        [ 1, ' B ' ], 
        [ 2, ' B ' ],
    ];
    
    ////////////////////////////////////////////////////////////////////////////

    protected function setupDbFixture()
    {
        
    }

    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * 
     * @param array $testData
     * @return array
     */
    protected function generateInputValuesFromTestData( array $testData )
    {
        $inputValues = [
            App_Api_Scara_Add::ARG_DENUMIRE     => $testData[ 1 ],
        ];
        
        return $inputValues;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * Scenariu valid:
     * - autentificare
     * - adaugare bloc
     * Verificari:
     * - raspunsul sa contina exact cheile { App_Api_Bloc_Add::ARG_DENUMIRE, App_Api_Bloc_Add::ARG_BLOC_ID }
     * - inserare inregistrare in DB (verificare campuri: asociatie_id, denumire, adding_month, status)
     * 
     * @dataProvider goodDataProvider
     */
    public function test_GoodData( array $testData, array $credentials )
    {
        $this->_test_GoodData( $testData, $credentials, [
            'returnedDataAssertions'    => function( $returnedData, $inputValues )
            {
                    $assertions = [
                        App_Api_Bloc_Add::ARG_BLOC_ID   => 'NotEmpty',
                        App_Api_Bloc_Add::ARG_DENUMIRE  => [ 'Equals', trim( $inputValues[ App_Api_Bloc_Add::ARG_DENUMIRE ] ) ],
                    ];
                    
                    return $assertions;
            },
            
            'postProcess'               => function( $preProcessState, $inputValues, $credentials, $responseData, $TestCase )
            {
                    $Asociatie = $this->getAsociatieFromCredentials( $credentials );

                    // retrieve scara_id from returned data
                    $returnedData = $responseData[ Lib_Api::RESPONSE_DATA ];
                    $blocId = $returnedData[ App_Api_Bloc_Add::ARG_BLOC_ID ];

                    // verificare inregistrare in DB
                    $expectedProperties = [
                        App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $Asociatie->getId(),
                        App_Component_Bloc_Object::PROP_DENUMIRE        => trim( $inputValues[ App_Component_Bloc_Object::PROP_DENUMIRE ] ),
                        App_Component_Bloc_Object::PROP_ADDING_MONTH    => [ $Asociatie->getLunaCurenta(), true ],
                        App_Component_Bloc_Object::PROP_STATUS          => App_Component_Bloc_Object::VAL_STATUS_ACTIVE,
                    ];

                    $TestCase->assertObjectExistsAndHasProperties( 'Bloc', $blocId, $expectedProperties );
            }
        ]);
    }

    /**
     * @dataProvider authenticationErrorsDataProvider
     */
    public function test_Errors_Authentication( array $testData, array $credentials )
    {
        $this->_test_Errors_Authentication( $testData, $credentials );
    }
    
    /**
     * @dataProvider dataForErrorsProvider
     * @depends test_GoodData
     * 
     */
    public function test_Errors( array $testData, array $expectedErrors, array $credentials )
    {
        $this->_test_Errors( $testData, $expectedErrors, $credentials, [
            'unchangedTables'   => [ 'Bloc' ],
        ] );
    }
    
    /**
     * Stergere si re-adaugare bloc; nu trebuie sa apara eroare.
     * 
     * @dataProvider removeAndAddDataProvider
     * @depends test_GoodData
     */
    public function test_RemoveAndAdd( array $testData, array $credentials )
    {
        $blocIdToRemove = $this->getBlocIdFromTestData( $testData );
        App_Component_Bloc_Factory::getInstance()->find( $blocIdToRemove )->remove();

        $inputValues = $this->generateInputValuesFromTestData( $testData );

        $this->performAndTestAction([
            'inputValues'   => $inputValues, 
            'credentials'   => $credentials,
        ]);
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider oneCredentialDataProvider
     * 
     * Scenariu:
     * - se sterge continutul tabelei
     * - adaug numarul maxim de blocuri
     * - urmatoarea adaugare trebuie sa genereze eroare
     * - sterg un bloc
     * - adaug un alt bloc; operatia trebuie sa se finalizeze cu succes
     */
    public function test_Error_MaxEntries( array $credentials )
    {
        self::cleanDb();
        
        // autentificare
        $this->doLogin( $credentials );
        $token = $this->getToken();
        
        // adaugare numar maxim de blocuri
        for( $i = 0; $i < App_Component_Bloc_Validator_Add::MAX_ENTRIES; $i++ )
        {
            $denumire = 'bloc_' . $i;
            $blocValues = [ App_Component_Bloc_Object::PROP_DENUMIRE => $denumire ];
            
            $this->doRequestWithToken( $token, $this->_action, $blocValues );
            $this->assertResponseIsSuccessful();
        }
        
        // se mai adauga un bloc; trebuie sa se genereze eroare
        $denumireForError = 'bloc_' . $i;
        $blocValuesForError = [ App_Component_Bloc_Object::PROP_DENUMIRE => $denumireForError ];

        $this->doRequestWithToken( $token, $this->_action, $blocValuesForError );
        $this->assertResponseHasErrors( App_Api_Bloc_Add::ERR_MAX_ENTRIES );
        
        // stergere bloc (ultimul adaugat)
        $denumireForRemove = 'bloc_' . ( $i - 1 );
        $blocValuesForRemove = [
            App_Component_Bloc_Object::PROP_DENUMIRE        => $denumireForRemove,
            App_Component_Bloc_Object::PROP_ASOCIATIE_ID    => $this->getAsociatieByUsername( $credentials[ App_Component_Asociatie_Object::PROP_USERNAME ] )->getId(),
        ];

        $Bloc = App_Component_Bloc_Factory::getInstance()->findActive( $blocValuesForRemove );
        $Bloc->remove();
        
        // adaugare bloc; operatia trebuie sa reuseasca
        $blocValuesForAdd = [ App_Component_Bloc_Object::PROP_DENUMIRE => $denumireForError ];

        $this->doRequestWithToken( $token, $this->_action, $blocValuesForAdd );
        $this->assertResponseIsSuccessful();
        
    }
    
    /**
     * @depends test_GoodData
     * @dataProvider oneCredentialDataProvider
     * 
     * Scenariu:
     * - adaugare, stergere - ciclul se repeta pana la limita
     * - adaugare => eroare
     */
    public function test_Error_MaxOperations( array $credentials )
    {
        self::cleanDb();
        
        // autentificare
        $this->doLogin( $credentials );
        $token = $this->getToken();
        
        // executare numar maxim de operatii (adaugare urmata de stergere)
        for( $i = 0; $i < App_Component_Bloc_Validator_Add::MAX_OPERATIONS; $i++ )
        {
            $denumire = 'bloc_' . $i;
            $blocValues = [ App_Component_Bloc_Object::PROP_DENUMIRE => $denumire ];
            
            // adaugare
            $this->doRequestWithToken( $token, $this->_action, $blocValues );
            $this->assertResponseIsSuccessful();
            
            $blocId = $this->getResponseData()[ Lib_Api::RESPONSE_DATA ][ App_Component_Bloc_Object::PROP_ID ];
            
            // stergere
            $Bloc = App_Component_Bloc_Factory::getInstance()->find( $blocId );
            $Bloc->remove();
        }
        
        // se incearca adaugarea unui nou bloc; trebuie sa se genereze eroare
        $denumireForError = 'bloc_' . $i;
        $blocValuesForError = [ App_Component_Bloc_Object::PROP_DENUMIRE => $denumireForError ];

        $this->doRequestWithToken( $token, $this->_action, $blocValuesForError );
        $this->assertResponseHasErrors( App_Api_Bloc_Add::ERR_MAX_OPERATIONS );
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    public function removeAndAddDataProvider()
    {
        return $this->generateOutputData( $this->dataForRemoveAndAdd );
    }
    
}