<?php

class Lib_Controller_Api extends Lib_Controller_Abstract
{
    public function init()
    {
        $this->_helper->Layout->disableLayout();
    }

    /**
     * @param type $response
     */
    protected function sendResponse( $response )
    {
        $this->_helper->json( $response );
    }
    
    /**
     * 
     * @param Lib_Api $api
     */
    protected function sendApiResponse( Lib_Api $api )
    {
        $this->sendResponse( $api->getResponse() );
    }
    
    /**
     * 
     * @param type $apiClass
     * @param array $apiAguments
     */
    protected function linkToApi( $apiClass, array $apiAguments = [] )
    {
        $params = $this->getRequestParams();
        
        $reflectionClass = new ReflectionClass( $apiClass );
        $api = $reflectionClass->newInstanceArgs( $apiAguments );
        
        $api->run( $params );

        $this->sendApiResponse( $api );
    }
    
}

