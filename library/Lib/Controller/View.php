<?php

abstract class Lib_Controller_View extends Lib_Controller_Abstract
{
    public function init()
    {
        $this->_helper->Layout->setLayout( $this->getControllerName() );
    }
    
}
