<?php

abstract class Lib_Controller_Abstract extends Zend_Controller_Action
{
    /**
     * 
     * @return Array
     */
    protected function getRequestParams()
    {
        $request = $this->getRequest();

        $params = $request->getParams();

        // unset environment params
        unset(
            $params[$request->getModuleKey()],
            $params[$request->getControllerKey()],
            $params[$request->getActionKey()]
        );

        return $params;
    }
    
    protected function getControllerName()
    {
        $name = $this->getRequest()->getControllerName();
        
        return $name;
    }
    
}
