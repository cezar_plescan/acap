<?php

class Lib_Component_ORM_Db_Operator_Equal extends Lib_Component_ORM_Db_Operator
{
    protected
            $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getExpression()
    {
        $expression = ' = ' . $this->getDb()->quoteValue($this->value);

        return $expression;
    }

}
