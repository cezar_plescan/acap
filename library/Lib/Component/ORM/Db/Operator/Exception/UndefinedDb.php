<?php

class Lib_Component_ORM_Db_Operator_Exception_UndefinedDb extends UnexpectedValueException
{
    public function __construct()
    {
        $message = 'The database resource was not defined.';

        parent::__construct($message);
    }
}
