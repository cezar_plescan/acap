<?php

abstract class Lib_Component_ORM_Db_Operator
{
    abstract public function getExpression();

    protected
            $db;

    public function setDb(Lib_Component_ORM_Db $db)
    {
        $this->db = $db;
    }

    protected function getDb()
    {
        $db = $this->db;

        if (! ($db instanceof Lib_Component_ORM_Db))
        {
            throw new Lib_Component_ORM_Db_Operator_Exception_UndefinedDb();
        }

        return $db;
    }
}
