<?php

class Lib_Component_ORM_Db_Exception_UnsupportedMethod extends BadMethodCallException
{
    public function __construct($method)
    {
        $message = 'The method "' . $method . '" is not defined.';

        parent::__construct($message);
    }
}
