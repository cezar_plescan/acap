<?php

class Lib_Component_ORM_Factory extends Lib_Component_Factory
{
    static protected $instance;

    protected $_cache = [];
    
    /**
     * 
     * @param array $ctorArgs
     * @param type $objectType
     * @param type $objectNameSuffix
     * @return Lib_Component_ORM_Object
     */
    public function create(array $ctorArgs = null, $objectType = null, $objectNameSuffix = null)
    {
        return parent::create($ctorArgs, $objectType, $objectNameSuffix);
    }

    /**
     *
     * @param array $ctorArg The list of arguments passed to the constructor
     * @param type $objectType
     *
     * @return Lib_Component_ORM_Object
     */
    public function createAndSave(array $ctorArgs = NULL, $objectType = null)
    {
        $object = $this->create($ctorArgs, $objectType);
        $object->save();

        return $object;
    }

    /**
     *
     * @return Lib_Component_ORM_Db
     */
    protected function getDb()
    {
        return $this->getResource('Db');
    }

    public function count(array $where)
    {
        $count = $this->getDb()->count($where);

        return $count;
    }

    /**
     * Will find record in database/service and fills model with the data
     *
     * @param int|array $id id of record | column/value pairs of where clause
     * @param $object data object to assign the values to
     * 
     * @return Lib_Component_ORM_Object
     */
    public function find( $criteria )
    {
        if (!is_array($criteria))
        {
            $criteria = array(
                Lib_Component_ORM_Object::PROP_ID  => $criteria,
            );
        }

        $results = $this->getDb()->fetchAll( $criteria, null, 1 );
        $objects = $this->hydrateObjects( $results, true );

        $Object = reset( $objects );

        return $Object ? $Object : null;
    }

    /**
     * Find an active Object
     *
     * @param type $criteria
     */
    public function findActive( $criteria )
    {
        if (!is_array($criteria))
        {
            $criteria = array(
                Lib_Component_ORM_Object::PROP_ID  => $criteria,
            );
        }

        $objectClass = $this->getObjectClass();
        $criteria += $objectClass::getActiveStatusCriteria();

        $Object = $this->find( $criteria );

        return $Object;
    }

    public function countActive( array $criteria )
    {
        $objectClass = $this->getObjectClass();
        $criteria += $objectClass::getActiveStatusCriteria();

        $count = $this->count($criteria);

        return $count;
    }

    /**
     * Gets all of objects in database/service
     *
     * @return array    Array of Lib_Component_ORM_Object objects
     */
    public function fetchAll($where = null, $order = null, $count = null, $offset = null)
    {
        $results = $this->getDb()->fetchAll($where, $order, $count, $offset);

        $Objects = $this->hydrateObjects($results);

        return $Objects;
    }
    
    public function retrieveData( array $columns, $conditions = NULL )
    {
        $DB     = $this->getDb();
        $rows   = $DB->fetchData( $columns, $conditions );
        
        return $rows;
    }
    
    public function retrieveValue( $id, $column )
    {
        $rows = $this->retrieveData( [ $column ], [ Lib_Component_ORM_Object::PROP_ID => $id ] );
        
        $value = Lib_Tools::checkInputDataKeys( $rows, [ 0, $column ] );
        
        return $value;
    }
    
    protected function hasCache( $cacheId )
    {
        return isset( $this->_cache[ $cacheId ] );
    }

    protected function notCache( $cacheId )
    {
        return !isset( $this->_cache[ $cacheId ] );
    }

    protected function writeCache( $cacheId, $value )
    {
        $this->_cache[ $cacheId ] = $value;
    }

    protected function readCache( $cacheId )
    {
        return $this->_cache[ $cacheId ];
    }

}
