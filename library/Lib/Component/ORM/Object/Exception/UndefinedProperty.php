<?php

class Lib_Component_ORM_Object_Exception_UndefinedProperty extends UnexpectedValueException
{
    public function __construct($property)
    {
        $message = 'The property "' . $property . '" is not defined.';

        parent::__construct($message);
    }
}
