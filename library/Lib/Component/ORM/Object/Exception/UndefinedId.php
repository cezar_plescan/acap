<?php

class Lib_Component_ORM_Object_Exception_UndefinedId extends UnexpectedValueException
{
    public function __construct()
    {
        $message = 'The Object does not have the "ID" property';

        parent::__construct($message);
    }
}
