<?php

abstract class Lib_Component_ORM_Object extends Lib_Component_Object
{
    const PROP_ID            = 'id';
    const PROP_TIMESTAMP     = 'timestamp';
    const PROP_CREATED_AT    = 'created_at';
    const PROP_UPDATED_AT    = 'updated_at';

    protected
        $_cache = [],
        $_properties = [],
        $_additionalProperties = [],
        $_originalProperties = [],
        $_modifiedProperties = [];

    public function __construct(array $data = [])
    {
        $this->populateProperties( $data );

        $this->init();

        // check if the ID property exists
        if ( !$this->propertyExists( self::PROP_ID ) )
        {
            throw new Lib_Component_ORM_Object_Exception_UndefinedId();
        }
    }

    protected function init(){}

    static public function getActiveStatusCriteria()
    {
        return [];
    }

    protected function populateProperties( array $data )
    {
        foreach( $data as $property => $value )
        {
            $this->_originalProperties[ $property ] = $value;
            $this->_properties[ $property ] = $value;
        }
    }

    public function getId()
    {
        return $this->getProperty( self::PROP_ID );
    }

    /**
     *
     * @return Lib_Component_ORM_Db
     */
    protected function getDb()
    {
        return $this->getResource('Db');
    }

    /**
     * Check if the ID property is set
     */
    public function isNew()
    {
        return $this->getProperty( self::PROP_ID ) === null;
    }

    /**
     * Check if the object is populated
     */
    public function isEmpty()
    {
        foreach ($this->_properties as $value)
        {
            if (null !== $value)
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Get all accessible properties as array
     * @return array of accessible properties
     */
    public function toArray()
    {
        return $this->_properties;
    }

    /**
     * Clears (sets to null) all properties of the object
     * @return Lib_Component_ORM_Object
     */
    public function clear()
    {
        foreach ($this->_properties as $key => $value)
        {
            $this->setProperty( $key, null);
        }

        return $this;
    }

    /**
     * Saves the model to database/service
     *
     * @return int The id of the record or throws exception
     */
    public function save()
    {
        $db = $this->getDb();

        $this->_preSave();

        $dateTime = Lib_Date::getInstance()->now( 'Y-m-d H:i:s' );
        
        // if model doesn't have an ID property, then insert as new record, else update existing record
        if ( $this->isNew() )
        {
            $this->_preInsert();

            if( $this->propertyExists( self::PROP_CREATED_AT ) )
            {
                $this->setProperty( self::PROP_CREATED_AT, $dateTime );
            }
            
            if( $this->propertyExists( self::PROP_UPDATED_AT ) )
            {
                $this->setProperty( self::PROP_UPDATED_AT, $dateTime );
            }
            
            $tableData = $this->toArray();
            unset( $tableData[ self::PROP_ID ] );

            // save data to ref table
            $id = $db->insert( $tableData );

            // assign new id to object
            $this->setProperty( self::PROP_ID, $id );
        }
        else
        {
            $this->_preUpdate();
            
            $id = $this->getProperty( self::PROP_ID );

            $updateData = $this->getModifiedProperties();

            if ( $updateData )
            {
                if( $this->propertyExists( self::PROP_UPDATED_AT ) )
                {
                    $this->setProperty( self::PROP_UPDATED_AT, $dateTime );
                    $updateData[ self::PROP_UPDATED_AT ] = $dateTime;
                }

                // update existing record
                $db->update( $updateData, $db->whereEqual($id, self::PROP_ID) );
            }
        }

        $this->resetModifiedProperties();

        return $this;
    }

    protected function _preSave() {}

    protected function _preUpdate() {}

    protected function _preInsert() {}

    protected function getModifiedProperties()
    {
        $modifiedProperties = [];

        foreach ( $this->_modifiedProperties as $property => $previousValue )
        {
            $currentValue = $this->_properties[ $property ];
            if ( strcmp( $this->_originalProperties[ $property ], $currentValue ) )
            {
                $modifiedProperties[ $property ] = $currentValue;
            }
        }

        return $modifiedProperties;
    }

    protected function resetModifiedProperties()
    {
        $this->_originalProperties = $this->_properties;
        $this->_modifiedProperties = [];
    }

    /**
     * Delete the object
     *
     * @return Lib_Component_ORM_Object
     */
    public function delete()
    {
        $where = array(
            self::PROP_ID    => $this->getId(),
        );

        $this->getDb()->delete($where);

        $this->clear();
        $this->resetModifiedProperties();

        return $this;
    }

//    public function updateAndSave(array $properties)
//    {
//        $this->updateProperties($properties);
//        $result = $this->save();
//
//        return $result;
//    }
//
    /**
     * Checks if a property exists
     *
     * @param string $property
     * @return mixed
     */
    public function propertyExists( $property )
    {
        return array_key_exists( $property, $this->_properties );
    }

//    /**
//     * ??
//     * @param array $properties
//     * @return \Lib_Component_ORM_Object
//     */
//    public function addProperties(array $properties)
//    {
//        foreach ($properties as $property => $value)
//        {
//            $this->_properties[ $property ] = $value;
//        }
//
//        return $this;
//    }
//
    public function getProperty( $property )
    {
        if ( $this->propertyExists( $property ) )
        {
            return $this->_properties[ $property ];
        }
        elseif( array_key_exists( $property, $this->_additionalProperties ) )
        {
            return $this->_additionalProperties[ $property ];
        }
        else
        {
            throw new Lib_Component_ORM_Object_Exception_UndefinedProperty( $property );
        }
    }

    public function setProperty($property, $value)
    {
        if ( $this->propertyExists( $property ) )
        {
            // store the previous value
            $this->_modifiedProperties[ $property ] = $this->_properties[ $property ];
            $this->_properties[ $property ] = $value;
        }
        else
        {
            throw new Lib_Component_ORM_Object_Exception_UndefinedProperty($property);
        }

        return $this;
    }

    public function setPropertyIfKeyExists( $key, array $properties )
    {
        if( array_key_exists( $key, $properties ) )
        {
            $this->setProperty( $key, $properties[ $key ] );
        }
        
        return $this;
    }
    
    public function update(array $properties)
    {
        foreach( $properties as $property => $value )
        {
            $this->setProperty( $property, $value );
        }

        return $this;
    }

    public function updateAndSave(array $properties)
    {
        $this->update( $properties );
        $this->save();
        
        return $this;
    }

}
