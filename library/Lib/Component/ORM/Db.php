<?php

abstract class Lib_Component_ORM_Db
{
    const MAX_BIGINT = '18446744073709551615';

    protected 
            $_name,
            $Adapter = null,
            $Table = null,
            $tableColumns = [];

    static protected $instance;

    /**
     *
     * @return static
     */
    static public function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
        }

        return static::$instance;
    }
    
    private function __construct()
    {
        if ( !defined('static::TABLE') )
        {
            throw new Lib_Component_ORM_Db_Exception_TableNameNotDefined();
        }

        $this->_name = static::TABLE;
        $config = [ Zend_Db_Table::NAME => static::TABLE ];
        
        $this->Table = new Zend_Db_Table( $config );
        $this->Adapter = $this->Table->getAdapter();
        
        $this->tableColumns = $this->Table->info( Zend_Db_Table::COLS );
    }

    /**
     * Forward methods to the adapter
     *
     * @param type $name
     * @param type $arguments
     */
    public function __call($name, $arguments)
    {
        $adapter = $this->Adapter;

        if (method_exists($adapter, $name))
        {
            call_user_func_array(array($adapter, $name), $arguments);
        }
        else
        {
            throw new Lib_Component_ORM_Db_Exception_UnsupportedMethod($name);
        }
    }

    public function transaction(callable $callback)
    {
        $this->beginTransaction();

        try
        {
            $return = $callback();

            $this->commitTransaction();
        }
        catch (Exception $exception)
        {
            $this->resetTransaction();

            Lib_Logger_Exception::getInstance()->log($exception);

            $return = OPERATION_ERROR;
        }

        return $return;
    }

    public function beginTransaction()
    {
        $this->Adapter->beginTransaction();
    }

    public function commitTransaction()
    {
        $this->Adapter->commit();
    }

    public function resetTransaction()
    {
        $this->Adapter->rollBack();
    }

    protected function quoteColumns($columns, $table = null)
    {
        $columns = (array)$columns;

        if (!count($columns))
        {
            throw new UnexpectedValueException('No columns specified');
        }

        if ($table === false)
        {
            $quotedTable = '';
        }
        else
        {
            $quotedTable = $this->quoteTableName($table) . '.';
        }

        foreach($columns as $key => $value)
        {
            $quotedColumn = $quotedTable;

            if (!is_numeric($key))
            {
                $quotedColumn .= $this->quoteIdentifier($key) . ' AS ' . $this->quoteIdentifier($value);
            }
            else
            {
                if (trim($value) != '*')
                {
                    $value = $this->quoteIdentifier($value);
                }

                $quotedColumn .= $value;
            }

            $columns[$key] = $quotedColumn;
        }

        if (count($columns))
        {
            $sql = implode(', ', $columns);
        }

        return $sql;
    }

    protected function quoteTableName($tableName = null)
    {
        if ( null === $tableName )
        {
            $tableName = $this->_name;
        }

        $quotedTableName = $this->Adapter->quoteTableAs($tableName);

        return $quotedTableName;
    }

    public function quoteValue($value)
    {
        $value = $this->Adapter->quote( $value );

        return $value;
    }

    protected function quoteIdentifier($value)
    {
        $value = $this->Adapter->quoteIdentifier($value);

        return $value;
    }

    public function query($sql)
    {
        $query = $this->Adapter->query($sql);

        return $query;
    }

    public function queryAndFetchAll($sql)
    {
        $result = $this->query($sql)->fetchAll();

        return $result;
    }

    public function queryAndFetchColumn($sql)
    {
        $result = $this->query($sql)->fetchColumn();

        return $result;
    }

    public function fetchAll(array $where = array(), $order = null, $count = null, $offset = null)
    {
        $whereStmt = $this->whereConditions( $where );

        $sql = 'SELECT * ' .
                $this->fromClause() .
                $this->where( $whereStmt ) .
                $this->orderBy((array)$order) .
                $this->limit($count, $offset);

        $results = $this->queryAndFetchAll($sql);

        return $results;
    }
    
    public function fetchData( array $columns, $conditions = NULL )
    {
        $sql = $this->selectStmt( $columns ) . $this->where( $conditions );
        
        $results = $this->queryAndFetchAll( $sql );
        
        return $results;
    }

    /**
    * Deletes existing rows.
    *
    * @param  array WHERE clause(s).
    * @return int   The number of rows deleted.
    */
    public function delete( array $where = [] )
    {
        $where = $this->whereConditions( $where );

        $result = $this->Table->delete( $where );

        return $result;
    }

    /**
     *
     * @param string $columns Columns should be already prepared!
     * @param string $table
     * @return string
     */
    protected function selectStmt($columns, $table = null)
    {
        $columns = (array)$columns;
        $columns = implode(', ', $columns);

        $sql = 'SELECT ' . $columns .
                $this->fromClause($table);

        return $sql;
    }

    /**
     * Creates a FROM clause
     *
     * @param type $table
     *
     * return string
     */
    protected function fromClause($table = null)
    {
        $sql = ' FROM ' . $this->quoteTableName($table);

        return $sql;
    }

    protected function join($type, $table1, $column1, $column2, $table2 = null, $condition = null)
    {
        $sql = ' ' . $type. ' JOIN ' . $this->quoteTableName($table1) . '
            ON ' . $this->quoteColumns($column1, $table1) . ' = ' . $this->quoteColumns($column2, $table2);

        if (null !== $condition)
        {
            $sql .= ' AND ' . $condition;
        }

        return $sql;
    }

    /**
     * Creates an INNER JOIN clause
     *
     * @param type $table1 The Table to be joined
     * @param type $column1 The Column from table1
     * @param type $column2 The Column from table2
     * @param type $table2 If not specified, current table will be assumed
     *
     * return string
     */
    protected function innerJoin($table1, $column1, $column2, $table2 = null, $condition = null)
    {
        $sql = $this->join('INNER', $table1, $column1, $column2, $table2, $condition);

        return $sql;
    }

    /**
     * Creates a LEFT JOIN clause
     *
     * @param type $table1 The Table to be joined
     * @param type $column1 The Column from table1
     * @param type $column2 The Column from table2
     * @param type $table2 If not specified, current table will be assumed
     *
     * return string
     */
    protected function leftJoin($table1, $column1, $column2, $table2 = null, $condition = null)
    {
        $sql = $this->join('LEFT', $table1, $column1, $column2, $table2, $condition);

        return $sql;
    }

    /**
     * Creates a RIGHT JOIN clause
     *
     * @param type $table1 The Table to be joined
     * @param type $column1 The Column from table1
     * @param type $column2 The Column from table2
     * @param type $table2 If not specified, current table will be assumed
     *
     * return string
     */
    protected function rightJoin($table1, $column1, $column2, $table2 = null, $condition = null)
    {
        $sql = $this->join('RIGHT', $table1, $column1, $column2, $table2, $condition);

        return $sql;
    }

    /**
     *
     * @param type $conditionSql
     * @return string
     */
    protected function where( $conditions )
    {
        $sql = '';
        
        if( is_array( $conditions) )
        {
            $conditionSql = $this->whereConditions( $conditions );
        }
        else
        {
            $conditionSql = trim( $conditions );
        }

        if( strlen( $conditionSql ) > 0 )
        {
            $sql = ' WHERE ' . $conditionSql;
        }

        return $sql;
    }

    /**
     * Generates a partial WHERE statement.
     * It needs to be used together with the where() method.
     * It does NOT escape values!
     *
     * @param array $conditions
     *
     * @return string
     */
    public function whereAnd( array $conditions )
    {
        foreach( $conditions as $key => $condition )
        {
            $condition = trim( $condition );
            if( strlen( $condition ) )
            {
                $conditions[ $key ] = '(' . $condition . ')';
            }
            else
            {
                unset( $conditions[ $key ] );
            }
        }

        $conditionsSql = implode(' AND ', $conditions);

        return $conditionsSql;
    }

    /**
     * Generates a partial WHERE statement.
     * It needs to be used together with the where() method.
     * It does NOT escape values!
     *
     * @param array $conditions
     *
     * @return string
     */
    public function whereOr( array $conditions )
    {
        foreach( $conditions as $key => $condition )
        {
            $condition = trim( $condition );
            if( strlen( $condition ) )
            {
                $conditions[ $key ] = '(' . $condition . ')';
            }
            else
            {
                unset( $conditions[ $key ] );
            }
        }

        $conditionsSql = implode(' OR ', $conditions);

        return $conditionsSql;
    }

    public function whereEqual($value, $column, $table = null)
    {
        if (null === $value)
        {
            $sql = $this->whereNull($column, $table);
        }
        elseif ( is_array($value) )
        {
            $sql = $this->whereIn($value, $column, $table);
        }
        else
        {
            $sql = $this->whereOperator('=', $value, $column, $table);
        }

        return $sql;
    }

    public function whereNotEqual($value, $column, $table = null)
    {
        $sql = $this->whereOperator('!=', $value, $column, $table);

        return $sql;
    }

    public function whereLessOrEqualThan($value, $column, $table = null)
    {
        $sql = $this->whereOperator('<=', $value, $column, $table);

        return $sql;
    }

    public function whereLessThan($value, $column, $table = null)
    {
        $sql = $this->whereOperator('<', $value, $column, $table);

        return $sql;
    }

    public function whereGreaterOrEqualThan($value, $column, $table = null)
    {
        $sql = $this->whereOperator('>=', $value, $column, $table);

        return $sql;
    }

    public function whereGreaterThan($value, $column, $table = null)
    {
        $sql = $this->whereOperator('>', $value, $column, $table);

        return $sql;
    }

    public function whereNull($column, $table = null)
    {
        $quotedColumn = $this->quoteColumns($column, $table);

        $sql = $quotedColumn . ' IS NULL ';

        return $sql;
    }

    protected function whereOperator($operator, $value, $column, $table = null)
    {
        $quotedColumn = $this->quoteColumns($column, $table);

        $sql = $quotedColumn . ' ' . $operator . ' ' . $this->quoteValue( trim($value) );

        return $sql;
    }

    public function whereIn( array $values, $column, $table = null )
    {
        if (count($values))
        {
            foreach ($values as $index => $value)
            {
                $values[$index] = $this->quoteValue($value);
            }

            $query = $this->quoteColumns($column, $table) . ' IN (' . implode(', ', $values) . ')';
        }
        else
        {
            $query = '0';
        }

        return $query;
    }

    /**
     * Build a custom WHERE condition
     *
     * @param type $expression
     * @param type $column
     * @param type $table
     * @return type
     */
    public function whereCustom($expression, $column = null, $table = null)
    {
        $sql = '';
        if (null !== $column)
        {
            $quotedColumn = $this->quoteColumns($column, $table);
            $sql .= $quotedColumn . ' ';
        }

        $sql .= $expression;

        return $sql;
    }

    public function whereConditions( array $conditions )
    {
        $whereConditions = [];

        foreach( $conditions as $column => $value )
        {
            if( $value instanceof Lib_Component_ORM_Db_Operator )
            {
                $value->setDb( $this );
                $whereConditions[] = $this->whereCustom( $value->getExpression(), $column );
            }
            else if( is_numeric( $column ) )
            {
                $whereConditions[] = $value;
            }
            else
            {
                if( is_array( $value ) )
                {
                    $whereConditions[] = $this->whereIn($value, $column);
                }
                else
                {
                    $whereConditions[] = $this->whereEqual($value, $column);
                }
            }
        }

        $where = $this->whereAnd( $whereConditions );

        return $where;
    }

    /**
     * Generates an ORDER BY clause
     *
     * @param mixed
     * The order criteria is specified by one or more arguments
     * Each criterion (argument) can be represented as:
     *  - a scalar value - an ORDER BY clause that will put in the query as it is
     *  - an array - the first value is the column, the second (optional) is the sorting order and the third (optional) is the name of the table which the column belong to.
     *
     * @return string
     */
    protected function orderBy()
    {
        $sql = '';

        $columns = func_get_args();
        $orders = array();

        foreach ($columns as $index => $orderBy)
        {
            if ( is_array($orderBy) )
            {
                $orderBy = (array)$orderBy;
                if (!$orderBy) continue;

                // get column name
                $column = $orderBy[0];

                // get order
                if (isset($orderBy[1]))
                {
                    $order = $orderBy[1];
                }
                else
                {
                    $order = 'ASC';
                }

                // get table
                $table = @$orderBy[2];

                $orders[] = $this->quoteColumns($column, $table) . ' ' . $order;
            }
            else
            {
                $orders[] = $orderBy;
            }
        }

        if ($orders)
        {
            $sql = ' ORDER BY ' . implode(', ', $orders);
        }

        return $sql;
    }

    protected function limit($count, $offset = null)
    {
        if (null === $count && null === $offset)
        {
            return '';
        }

        $sql = ' LIMIT ';

        if (null !== $offset)
        {
            $offset = (int)$offset;
            $sql .= $offset . ', ' . (null === $count ? self::MAX_BIGINT : (int)$count);
        }
        else
        {
            $sql .= (int)$count;
        }

        return $sql;
    }

    public function count( $where = NULL )
    {
        $sql = 'SELECT COUNT(*) '.
                $this->fromClause() .
                $this->where( $this->whereConditions((array)$where) );


        $query = $this->query($sql);
        $count = $query->fetchColumn();

        return $count;
    }

    public function truncate()
    {
        $sql = 'TRUNCATE TABLE ' . $this->quoteTableName();

        $this->query($sql);
    }

    public function update( array $data, $where )
    {
        $whereSql = is_array( $where ) ? $this->whereConditions( $where ) : $where;
        
        return $this->Table->update( $data, $whereSql );
    }
    
    public function insert( array $data )
    {
        if( in_array( Lib_Component_ORM_Object::PROP_CREATED_AT, $this->tableColumns ) )
        {
            $currentUTCDatetime = Lib_Date::getInstance()->getUTCDateTime();
            $data[ Lib_Component_ORM_Object::PROP_CREATED_AT ] = $currentUTCDatetime;
            
            if( in_array( Lib_Component_ORM_Object::PROP_UPDATED_AT, $this->tableColumns ) )
            {
                $data[ Lib_Component_ORM_Object::PROP_UPDATED_AT ] = $currentUTCDatetime;
            }
        }
        
        $id = (int)$this->Table->insert( $data );
        
        return $id;
    }
    
    
}
