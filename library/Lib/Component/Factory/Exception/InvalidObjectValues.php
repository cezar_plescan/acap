<?php

class Lib_Component_Factory_Exception_InvalidObjectValues extends UnexpectedValueException
{
    public function __construct($value)
    {
        $message = 'The provided value should be an array : ' . "\n" . print_r($value, true);

        parent::__construct($message);
    }
}
