<?php

class Lib_Component_Factory_Exception_ObjectNotFound extends UnexpectedValueException
{
    public function __construct($object)
    {
        $message = 'The object "' . $object . '" is not defined.';

        parent::__construct($message);
    }
}
