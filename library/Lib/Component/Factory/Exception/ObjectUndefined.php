<?php

class Lib_Component_Factory_Exception_ObjectUndefined extends UnexpectedValueException
{
    public function __construct($objectType)
    {
        $message = 'The object type "' . $objectType . '" is not defined.';

        parent::__construct($message);
    }
}
