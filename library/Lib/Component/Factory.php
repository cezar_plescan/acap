<?php

class Lib_Component_Factory extends Lib_Component_Abstract
{
    static protected $instance;

    const OPT_PATH      = 'path';
    const OPT_CTOR_ARGS = 'args';
    const OPT_LEVELS_UP = 'lu';
    
    protected function __construct()
    {
        ;
    }

    /**
     *
     * @return static
     */
    static public function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
        }

        return static::$instance;
    }

    static protected $objectMap = array();

    /**
     *
     * @param array $ctorArg The list of arguments passed to the constructor
     * @param type $objectType
     *
     * @return Lib_Component_Object
     * @throws Lib_Component_Factory_Exception_ObjectNotFound
     */
    public function create(array $ctorArgs = NULL, $objectType = null, $objectNameSuffix = null)
    {
        $objectName = $this->getObjectName($objectType, $objectNameSuffix);

        $object = $this->getNewResource($objectName, $ctorArgs);

        return $object;
    }

    public function createObject( $objectName, array $options = [] )
    {
        if( !is_array( $objectName ) )
        {
            $objectName = (array)$objectName;
        }
        $objectParts = array_reverse( $objectName );

        $fragments = array_map( 
                function( $part ) 
                {
                    return Zend_Filter::filterStatic( $part, 'Word_UnderscoreToCamelCase');
                }, 
                $objectParts );

        $resourceName = implode( '_', $fragments );
        
        if( isset( $options[ self::OPT_PATH ] ) )
        {
            $path = str_replace( DIRECTORY_SEPARATOR, '_', $options[ self::OPT_PATH ] );
            
            $resourceName = $path . ( substr( $path, -1 ) == '_' ? '' : '_' ) . $resourceName;
        }
        
        $ctorArgs = isset( $options[ self::OPT_CTOR_ARGS ] ) ?
                (array)$options[ self::OPT_CTOR_ARGS ] :
                [];
        
        $levelsUp = isset( $options[ self::OPT_LEVELS_UP ] ) ? $options[ self::OPT_LEVELS_UP ] : NULL;
        
        $object = $this->getNewResource( $resourceName, $ctorArgs, $levelsUp );

        return $object;
    }
    
    protected function getObjectName($objectType = null, $objectNameSuffix = null)
    {
        $prefix = '';

        if (null !== $objectType)
        {
            $prefix = Zend_Filter::filterStatic($objectType, 'Word_UnderscoreToCamelCase');
        }

        if ( null === $objectNameSuffix )
        {
            $objectNameSuffix = 'Object';
        }

        $objectName = $prefix;
        
        $objectNameSuffix = trim( $objectNameSuffix );
        if( strlen( $objectNameSuffix ) )
        {
            if( strlen( $objectName ) )
            {
                $objectName .= '_';
            }
            
            $objectName .= $objectNameSuffix;
        }
        
        return $objectName;
    }

    public function getObjectClass($objectType = null)
    {
        $objectName = $this->getObjectName($objectType);

        $class = $this->getResourceClass($objectName);

        return $class;
    }

    public function getObjectIds()
    {
        return array_keys(static::$objectMap);
    }

    /**
     * @overridable
     *
     * @param array $objectProperties
     * @return mixed
     */
    protected function getObjectType(array $objectProperties)
    {
        return null;
    }

    /**
     * Create objects from an input array data
     *
     * @param array $data
     * @param bool $baseObjects Whether or not to create base objects or try to detect their types
     *
     * @return Lib_Component_Object[]
     * @throws UnexpectedValueException\
     */
    protected function hydrateObjects(array $data, $baseObjects = false)
    {
        $Objects = array();

        $hasId = false;
        if ($data && isset(reset($data)[Lib_Component_ORM_Object::PROP_ID]))
        {
            $hasId = true;
        }

        foreach ($data as $values)
        {
            if (!is_array($values))
            {
                throw new Lib_Component_Factory_Exception_InvalidObjectValues($values);
            }

            $objectType = $baseObjects ? null : $this->getObjectType( $values );
            $Object = $this->create( [ $values ], $objectType );

            if ($hasId)
            {
                $id = $values[Lib_Component_ORM_Object::PROP_ID];
                $Objects[ $id ] = $Object;
            }
            else
            {
                $Objects[] = $Object;
            }
        }

        return $Objects;
    }
}
