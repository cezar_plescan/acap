<?php

abstract class Lib_Component_Abstract
{
    protected
            $_cache = [],
            $resources = [],
            $resourceClasses = [];

    public function clearCache()
    {
        $this->_cache = [];
    }

    protected function getResourceClass( $resourceName, $levelsUp = 0 )
    {
        if( !isset( $this->resourceClasses[ $resourceName ] ) )
        {
            $locator = new Lib_Locator_Class();
            $className = get_called_class();

            $resourceClass = $locator->generateClassName( $className, $resourceName, true, $levelsUp );

            $this->resourceClasses[ $resourceName ] = $resourceClass;
        }

        return $this->resourceClasses[ $resourceName ];
    }

    protected function getResource( $resourceName, array $ctorArgs = [] )
    {
        if( !isset( $this->resources[ $resourceName ] ) )
        {
            $this->resources[ $resourceName ] = $this->getNewResource( $resourceName, $ctorArgs );
        }

        return $this->resources[ $resourceName ];
    }

    protected function getNewResource( $resourceName, array $ctorArgs = [], $levelsUp = 0 )
    {
        $resourceClass = $this->getResourceClass( $resourceName, $levelsUp );

        $reflectionClass = new ReflectionClass( $resourceClass );
        if( $reflectionClass->hasMethod( 'getInstance' ) )
        {
            $resource = $resourceClass::getInstance();
        }
        else
        {
            $resource = $reflectionClass->newInstanceArgs( $ctorArgs );
        }

        return $resource;
    }

}
