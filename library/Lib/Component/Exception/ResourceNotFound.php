<?php

class Lib_Component_Exception_ResourceNotFound extends UnexpectedValueException
{
    public function __construct($resource)
    {
        $message = 'The resource "' . $resource . '" is not defined.';

        parent::__construct($message);
    }
}
