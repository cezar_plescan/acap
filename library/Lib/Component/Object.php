<?php

abstract class Lib_Component_Object extends Lib_Component_Abstract
{
    /**
     *
     * @return Lib_Component_ORM_Factory
     */
    protected function getFactory()
    {
        return $this->getResource('Factory');
    }


}
