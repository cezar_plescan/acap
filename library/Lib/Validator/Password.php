<?php

/**
 * Validator for password and confirm password fields
 */
class Lib_Validator_Password extends Lib_Validator_String
{
    const VAL_CONFIRM       = 'confirm_password';

    const ERR_CONFIRM       = 'confirm_password';

    public function __construct($maxLength = null, $minLength = null)
    {
        $minLength = Lib_Application::getInstance()->getConfig(array('validators', 'password', 'min_length'));
        $maxLength = Lib_Application::getInstance()->getConfig(array('validators', 'password', 'max_length'));

        parent::__construct($maxLength, $minLength);
    }

    protected function init()
    {
        parent::init();

        // the password should not be trimmed
        $this->setOption(self::OPT_TRIM, false);

        $this->addErrorTemplates(array(
            self::ERR_CONFIRM   => 'Reintroduceţi parola dorită',
        ));

    }

    /**
    * Check if the passowrd is valid
    * @param type $value
    */
    public function _isValid($value)
    {
        $value = parent::_isValid($value);

        // check confirm password
        $confirmPassword = $this->getAdditionalValue(self::VAL_CONFIRM);
        if (0 != strcmp($value, $confirmPassword))
        {
            $this->_setAdditionalError(self::VAL_CONFIRM, self::ERR_CONFIRM);
        }

        return $value;
    }
}
