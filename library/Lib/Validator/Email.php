<?php

class Lib_Validator_Email extends Lib_Validator_String
{
    const MAX_LENGTH        = 100;

    /**
    * Check if the email is valid
    * @param type $email
    */
    public function _isValid($email)
    {
        $email = parent::_isValid($email);

        $validator = new Zend_Validate_EmailAddress();
        if (!$validator->isValid($email))
        {
            $this->_setError(self::ERR_INVALID);
        }

        return $email;
    }
}
