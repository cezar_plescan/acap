<?php

class Lib_Validator_Ignore extends Lib_Validator_Abstract
{
    protected function init()
    {
        $this->setOption(self::OPT_REQUIRED, false);
        $this->setUnsetValue();
    }

    protected function _isValid($value)
    {
        return null;
    }

}
