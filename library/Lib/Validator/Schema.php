<?php

class Lib_Validator_Schema implements Lib_Validator_Interface
{
    protected
        $validators     = [],
        $errors         = [],
        $filteredValues = [];

    public function __construct(array $validators)
    {
        $this->_setValidators($validators);
    }

    public function isValid($values)
    {
        if (!is_array($values))
        {
            throw new Lib_Validator_Schema_Exception_InvalidValues($values);
        }

        $errors = array();

        /* @var $validator Lib_Validator_Abstract */
        foreach ($this->validators as $key => $validator)
        {
            if ($validator instanceof Lib_Validator_Abstract)
            {
                $dependencyValidator = $validator->getDependency();
                if (null !== $dependencyValidator && !$dependencyValidator->wasValid())
                {
                    continue;
                }

                if (!$validator->isValid(@$values[$key]))
                {
                    $validatorError = $validator->getError();
                    if (null !== $validatorError)
                    {
                        $errors[$key] = $validatorError;
                    }

                    $additionalValuesErrors = $validator->getAdditionalErrors();
                    $errors = array_merge($errors, $additionalValuesErrors);

                    if ($validator->isValidationStopped())
                    {
                        break;
                    }
                }
                else
                {
                    // retrieve filtered values
                    $filteredValue = $validator->getFilteredValue();
                    if ( !$validator->isUnsetValue() && ($filteredValue !== null || !$validator->isUnsetIfEmpty()) )
                    {
                        $this->setFilteredValue($key, $filteredValue);
                    }

                    $filteredAdditionalValues = $validator->getFilteredAdditionalValue();
                    if ( count($filteredAdditionalValues) || !$validator->isUnsetIfEmpty() )
                    {
                        $this->addFilteredValues($filteredAdditionalValues);
                    }
                }
            }
            elseif (is_array($validator))
            {
                $validatorSchema = new self($validator);
                if ($validatorSchema->isValid($values[$key]))
                {
                    $filteredValues = $validatorSchema->getFilteredValue();
                    $this->setFilteredValue($key, $filteredValues);
                }
                else
                {
                    $validatorSchemaErrors = $validatorSchema->getErrors();
                    $errors[$key] = $validatorSchemaErrors;
                }
            }
        }

        if (count($errors))
        {
            $this->setErrors($errors);

            return false;
        }
        else
        {
            return true;
        }

    }

    protected function _validate(array $validators)
    {

    }

    protected function _setValidators(array $validators)
    {
        $this->validators = $validators;
    }

    protected function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function setFilteredValue($key, $value)
    {
        $this->filteredValues[$key] = $value;

        return $this;
    }

    protected function addFilteredValues(array $values)
    {
        foreach ($values as $key => $value)
        {
            $this->setFilteredValue($key, $value);
        }

        return $this;
    }

    public function getFilteredValue()
    {
        return $this->filteredValues;
    }

    public function appendValidator( $key, Lib_Validator_Abstract $validator )
    {
        $this->validators[ $key ] = $validator;
    }

    public function prependValidator( $key, Lib_Validator_Abstract $validator )
    {
        $this->validators = [ $key => $validator ] + $this->validators;
    }

}
