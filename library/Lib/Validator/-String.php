<?php

/**
 *
 * max_length value can be speficied in many ways:
 * - (re)define the class constant MAX_LENGTH
 * - pass the value as a constructor argument
 * - define the OPT_MAX_LENGTH option
 *
 * The same applies for min_length
 */
class Lib_Validator_String extends Lib_Validator_Abstract
{
    const MAX_LENGTH = 32;
    const MIN_LENGTH = 0;

    const OPT_MAX_LENGTH = 'max_length';
    const OPT_MIN_LENGTH = 'min_length';

    const ERR_MAX_LENGTH = 'max_length';
    const ERR_MIN_LENGTH = 'min_length';

    public function __construct( $maxLength = null, $minLength = null )
    {
        if (null === $maxLength)
        {
            $maxLength = static::MAX_LENGTH;
        }
        $this->setOption(self::OPT_MAX_LENGTH, $maxLength);

        if (null === $minLength)
        {
            $minLength = static::MIN_LENGTH;
        }
        $this->setOption(self::OPT_MIN_LENGTH, $minLength);
        
        parent::__construct();
    }

    protected function init()
    {
        $this->addErrorTemplates(array(
            self::ERR_MAX_LENGTH    => 'Valoarea trebuie să conţină cel mult ' . $this->getOption(self::OPT_MAX_LENGTH) . ' caractere',
            self::ERR_MIN_LENGTH    => 'Valoarea trebuie să conţină cel puţin ' . $this->getOption(self::OPT_MIN_LENGTH) . ' caractere',
        ));
    }

    protected function _isValid($value)
    {
        $length = strlen($value);

        if ($length > $this->getOption(self::OPT_MAX_LENGTH))
        {
            $this->_setError(self::ERR_MAX_LENGTH);
        }

        if ($length < $this->getOption(self::OPT_MIN_LENGTH))
        {
            $this->_setError(self::ERR_MIN_LENGTH);
        }

        return $value;
    }

}
