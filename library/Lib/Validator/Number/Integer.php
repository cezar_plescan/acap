<?php

class Lib_Validator_Number_Integer extends Lib_Validator_Number
{

    protected function init()
    {
        $this->addErrorTemplates([
        ]);
    }

    protected function _isValid($value)
    {
        // the value should contain only digits, period or minus sign
        if (!preg_match('/^[0-9\-]+$/', $value))
        {
            $this->_setError( self::ERR_INVALID );
        }
        elseif (is_numeric($value) )
        {
            // remove zeros from the end
            $value = floatval($value);

            $pattern = '/^[-+]?[0-9]{1,9}$/';

            if (!preg_match($pattern, $value))
            {
                $this->_setError( self::ERR_INVALID );
            }

            $value = (int)$value;

            $value = parent::_isValid( $value );
        }
        else
        {
            $this->_setError( self::ERR_INVALID );
        }

        return $value;
    }

}
