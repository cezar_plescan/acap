<?php

abstract class _Lib_Validator_Abstract extends Lib_Component_Abstract implements Lib_Validator_Interface
{
    const CODE      = 'code';
    const MESSAGE   = 'message';

    const OPT_REQUIRED          = 'required';
    const OPT_STOP_VALIDATION   = 'stop_validation';
    const OPT_TRIM              = 'trim';
    const OPT_UNSET_IF_EMPTY    = 'unset_if_empty';
    const OPT_UNSET_VALUE       = 'unset_value';
    const OPT_SKIP_VALIDATION   = 'skip_validation';

    const ERR_REQUIRED          = 'required';
    const ERR_INVALID           = 'invalid';

    protected
        $error = null,
        $additionalErrors = array(),
        $value = null,
        $filteredValue = null,
        $additionalValues = array(),
        $filteredAdditionalValues = array(),
        $dependency = null,
        $dependencyOption = null,
        $errorTemplates = array(
            self::ERR_INVALID   => 'Valoarea este incorectă',
            self::ERR_REQUIRED  => 'Valoarea este obligatorie',
        );


    // default options
    protected $options = array(
        self::OPT_REQUIRED          => true,
        self::OPT_STOP_VALIDATION   => false,
        self::OPT_TRIM              => true,
        self::OPT_UNSET_IF_EMPTY    => false,
        self::OPT_UNSET_VALUE       => false,
        self::OPT_SKIP_VALIDATION   => true, // skip validation if value is empty and not required
    );

    public function __construct()
    {
        $this->init();
    }

    protected function init(){}

    protected function beforeValidation(){}

    protected function reset()
    {
        $this->error = null;
        $this->additionalErrors = array();
        $this->value = null;
        $this->filteredValue = null;
        $this->additionalValues = array();
        $this->filteredAdditionalValues = array();
        $this->dependency = null;
        $this->dependencyOption = null;

    }

    public function setOption($name, $value)
    {
        $this->options[$name] = $value;

        return $this;
    }

    public function getOption($name, $defaultValue = null)
    {
        if ($name == $this->dependencyOption && $this->dependency instanceof Lib_Validator_Abstract)
        {
            return $this->dependency->getFilteredValue();
        }

        if (isset($this->options[$name]))
        {
            return $this->options[$name];
        }
        else if (null !== $defaultValue)
        {
            return $defaultValue;
        }
        else
        {
            throw new UnexpectedValueException('The option "' . $name . '" is not defined');
        }

        return null;
    }

    protected function addOption($option, $defaultValue)
    {
        $this->options[$option] = $defaultValue;

        return $this;
    }

    /**
     * Adds error templates
     *
     * @param array $errorTemplates
     * @return \Lib_Validator_Abstract
     */
    protected function addErrorTemplates(array $errorTemplates)
    {
        $this->errorTemplates = array_merge($this->errorTemplates, $errorTemplates);

        return $this;
    }

    /**
     * Over-writes existing error templates
     *
     * @param array $errorTemplates
     * @return \Lib_Validator_Abstract
     */
    public function setErrorTemplates(array $errorTemplates)
    {
        foreach ($errorTemplates as $errorCode => $template)
        {
            if (array_key_exists($errorCode, $this->errorTemplates))
            {
                $this->errorTemplates[$errorCode] = $template;
            }
        }

        return $this;
    }

    public function addAdditionalValue($key, $value, $value_key = null)
    {
        $this->additionalValues[$key] = array(
            'value' => $value,
            'key'   => $value_key,
        );

        return $this;
    }

    protected function updateAdditionalValue($key, $value)
    {
        $this->additionalValues[$key]['value'] = $value;

        return $this;
    }

    protected function getAdditionalValue($key, $defaultValue = null)
    {
        if (isset($this->additionalValues[$key]))
        {
            return $this->additionalValues[$key]['value'];
        }
        elseif (null !== $defaultValue)
        {
            return $defaultValue;
        }

        throw new UnexpectedValueException('The additional value "' . $key . '" is not defined');
    }

    protected function getAdditionalValueKey($key)
    {
        if (isset($this->additionalValues[$key]))
        {
            return $this->additionalValues[$key]['key'];
        }

        return null;
    }

    protected function _resetError()
    {
        $this->error = null;
    }

    protected function isEmpty($value)
    {
        $empty = false;
        if (is_array($value))
        {
            $empty = !count($value);
        }
        elseif (is_scalar($value))
        {
            $empty = !strlen($value);
        }
        else
        {
            $empty = empty($value);
        }

        return $empty;
    }

    public function isValid($value)
    {
        try
        {
            $this->error = null;
            $this->additionalErrors = [];
            $this->value = null;
            $this->filteredValue = null;
            $this->filteredAdditionalValues = [];

            if ($this->getOption(self::OPT_TRIM) && is_scalar($value))
            {
                $value = trim($value);
            }

            $this->beforeValidation();

            if ($this->isEmpty($value))
            {
                if ($this->getOption(self::OPT_REQUIRED))
                {
                    $this->_setError(self::ERR_REQUIRED);
                }
                elseif (!$this->isValidationSkipped())
                {
                    // validare
                    $value = $this->doValidate($value);
                }
            }
            else
            {
                // validare
                $value = $this->doValidate($value);
            }

            return true;
        }
        catch (Lib_Validator_Error $e)
        {
            return false;
        }
    }

    protected function doValidate($value)
    {
        // validare
        $value = $this->_isValid($value);
        $this->_setFilteredValue($value);

        return $value;
    }

    public function wasValid()
    {
        return ($this->error === null);
    }

    /**
     * This method performs the validation.
     * It should return the filtered value.
     *
     * @param mixed $value Value to be validated
     * @return mixed The filtered value
     */
    abstract protected function _isValid($value);

    protected function _setFilteredValue($value)
    {
        $this->filteredValue = $value;

        return $this;
    }

    protected function _setFilteredAdditionalValue($key, $value)
    {
        $this->filteredAdditionalValues[$key] = $value;

        return $this;
    }

    protected function _setValue($value)
    {
        return $this->value = $value;
    }

    public function getFilteredValue()
    {
        return $this->filteredValue;
    }

    public function getFilteredAdditionalValue()
    {
        return $this->filteredAdditionalValues;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setRequired( $required = true )
    {
        $this->setOption(self::OPT_REQUIRED, (bool)$required);

        return $this;
    }

    public function isRequired()
    {
        return (bool)$this->getOption(self::OPT_REQUIRED);
    }

    public function setStopValidation( $stop_validation = true )
    {
        $this->setOption(self::OPT_STOP_VALIDATION, (bool)$stop_validation);

        return $this;
    }

    public function setTrim( $trim = true )
    {
        $this->setOption(self::OPT_TRIM, (bool)$trim);

        return $this;
    }

    public function setSkipValidation( $skip = true )
    {
        $this->setOption(self::OPT_SKIP_VALIDATION, (bool)$skip);

        return $this;
    }

    public function isValidationSkipped()
    {
        return $this->getOption(self::OPT_SKIP_VALIDATION);
    }

    public function setUnsetIfEmpty( $flag = true )
    {
        $this->setOption(self::OPT_UNSET_IF_EMPTY, (bool)$flag);

        return $this;
    }

    public function isUnsetIfEmpty()
    {
        return $this->getOption(self::OPT_UNSET_IF_EMPTY);
    }

    public function setUnsetValue($flag = true)
    {
        $this->setOption(self::OPT_UNSET_VALUE, (bool)$flag);

        return $this;
    }

    public function isUnsetValue()
    {
        return $this->getOption(self::OPT_UNSET_VALUE);
    }

    public function isValidationStopped()
    {
        return (bool)$this->getOption(self::OPT_STOP_VALIDATION);
    }

    protected function setError($error, $key = null)
    {
        if ($key === null)
        {
            $this->error = $error;
        }
        else
        {
            $this->additionalErrors[$key] = $error;
        }
        return $this;
    }

    public function getAdditionalErrors()
    {
        return $this->additionalErrors;
    }

    public function getError()
    {
        return $this->error;
    }

    public function getErrors()
    {
        return $this->error;
    }

    public function getErrorMessage()
    {
        return ($this->error !== null ? $this->error[ self::MESSAGE ] : null);
    }

    public function getErrorCode()
    {
        return ($this->error !== null ? $this->error[ self::CODE ] : null);
    }

    protected function createError($code, $message)
    {
        $error = [
            self::CODE      => $code,
            self::MESSAGE   => $message,
        ];

        return $error;
    }

    protected function _getError($errorCode, $params = array())
    {
        if (!isset($this->errorTemplates[$errorCode]))
        {
            throw new Exception('Error code "' . $errorCode . '" is not defined in ' . get_called_class());
        }

        $message = $this->_translateError($this->errorTemplates[$errorCode], $params);
        $error = $this->createError($errorCode, $message);

        return $error;
    }

    protected function _translateError($errorTemplate, array $params)
    {
        $error = $errorTemplate;

        foreach ($params as $placeholder => $value)
        {
            $error = str_replace('%' . $placeholder . '%' , $value, $error);
        }

        return $error;
    }

    protected function _setError($errorCode, $params = array(), $key = null)
    {
        $error = $this->_getError($errorCode, $params);
        $value_key = $this->getAdditionalValueKey($key);
        $this->setError($error, $value_key);

        throw new Lib_Validator_Error();
    }

    protected function _setCustomError($errorCode, $errorMessage, $key = null)
    {
        $error = $this->createError($errorCode, $errorMessage);
        $value_key = $this->getAdditionalValueKey($key);
        $this->setError($error, $value_key);

        throw new Lib_Validator_Error();
    }

    protected function _setAdditionalError($key, $errorCode, $params = array())
    {
        $this->_setError($errorCode, $params, $key);
    }

    public function dependsOn(Lib_Validator_Abstract $validator, $option = null)
    {
        $this->dependency = $validator;
        $this->dependencyOption = $option;

        return $this;
    }

    public function getDependency()
    {
        return $this->dependency;
    }

}
