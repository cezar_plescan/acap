<?php

class Lib_Validator_Number extends Lib_Validator_Abstract
{
    const MAX_VALUE = null;
    const MIN_VALUE = null;

    const ERR_MAX = 'max';
    const ERR_MIN = 'min';

    protected function init()
    {
        $this->addErrorTemplates([
            self::ERR_MAX   => 'Valoarea nu poate fi mai mare decât ' . static::MAX_VALUE,
            self::ERR_MIM   => 'Valoarea nu poate fi mai mică decât ' . static::MIN_VALUE,
        ]);
    }

    protected function validate( $value )
    {
        if( !is_numeric( $value ) )
        {
            $this->_setError( self::ERR_INVALID );
        }

        if( static::MAX_VALUE !== null && $value > static::MAX_VALUE )
        {
            $this->_setError( self::ERR_MAX );
        }

        if( static::MIN_VALUE !== null && $value < static::MIN_VALUE )
        {
            $this->_setError( self::ERR_MIN );
        }

        return $value;
    }

}
