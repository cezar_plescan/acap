<?php

/**
 * Validates a date and converts it to be saved in the DB
 * 
 */
class Lib_Validator_Date extends Lib_Validator_Abstract
{
    const OPT_TIMESTAMP = 'timestamp';
    
    protected function init()
    {
        // specify if the validator will consider also numeric values (timestamps)
        $this->addOption(self::OPT_TIMESTAMP, false);
        
        $this->addErrorTemplates(array(
            self::ERR_INVALID   => 'Invalid date',
        ));
    }
    
    protected function _isValid($value)
    {
        if (!$this->getOption(self::OPT_TIMESTAMP) && is_numeric($value))
        {
            $this->_setError(self::ERR_INVALID);
        }
        
        $DateUtility = Lib_Date::getInstance();
        
        $formattedValue = $DateUtility->format($value, null, false);
        if (false === $formattedValue)
        {
            $this->_setError(self::ERR_INVALID);
        }
        
        return $formattedValue;
    }
    
}
