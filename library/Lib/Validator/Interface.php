<?php

interface Lib_Validator_Interface
{
    public function isValid($value);
}
