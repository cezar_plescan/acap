<?php

abstract class Lib_Validator_Abstract
{
    const ERR_INVALID_FORMAT = 'invalid_format';
    
    const OPT_FLOAT_PRECISION   = 'float_precision';
    const FLOAT_PRECISION       = 4;
    
    const ARG_OPTIONS       = 'options';
    
    protected
        $errors,
        $value,
        $filteredValue,
        $options = [],
        $messageTemplates = [],
        $callback;

    public function __construct( array $arguments = [] )
    {
        $this->setOption( self::OPT_FLOAT_PRECISION, self::FLOAT_PRECISION );
        
        if( isset( $arguments[ self::ARG_OPTIONS ] ) )
        {
            $this->setOption( $arguments[ self::ARG_OPTIONS ] );
        }
        
        $this->addMessageTemplates([
            self::ERR_INVALID_FORMAT => 'Invalid value',
        ]);
        
        $this->init();
    }

    /**
     * - valideaza valoarea/valorile de intrare
     * - returneaza valoarea/valorile filtrate
     */
    abstract protected function validate( $value );

    /**
     * @hook
     */
    protected function _beforeValidate(){}
    
    final public function isValid( $value = null )
    {
        try
        {
            $this->reset();
            
            $this->value = $value;
            
            $this->_beforeValidate();
            
            $filteredValue = call_user_func_array( [ $this, 'validate' ], func_num_args() ? func_get_args() : [ NULL ] );
            
            $this->stopValidationIfErrors();
            
            $this->filteredValue = $filteredValue;
            
            if( is_callable( $this->callback ) )
            {
                call_user_func( $this->callback, $this->filteredValue );
            }
            
            return TRUE;
        }
        catch ( Lib_Validator_Exception_Stop $e )
        {
            return FALSE;
        }
    }
    
    /**
     * @overridable
     */
    protected function init(){}

    protected function reset()
    {
        $this->errors = [];
        $this->value = null;
        $this->filteredValue = null;
    }

    public function getFilteredValue()
    {
        return $this->filteredValue;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function clearErrors()
    {
        $this->errors = [];
    }

    protected function setErrors( array $errors )
    {
        $this->errors = $errors;
        
        $this->stopValidation();
    }

    protected function hasErrors()
    {
        return (bool)$this->errors;
    }

    public function setOption( $name, $value = NULL )
    {
        if( is_array( $name ) )
        {
            foreach( $name as $opt => $v )
            {
                $this->options[ $opt ] = $v;
            }
        }
        else
        {
            $this->options[ $name ] = $value;
        }

        return $this;
    }

    public function getOption( $name, $defaultValue = null )
    {
        $value = NULL;
        
        if( isset( $this->options[ $name ] ) )
        {
            $value = $this->options[ $name ];
        }
        elseif( null !== $defaultValue )
        {
            $value = $defaultValue;
        }
        
        return $value;
    }
    
    protected function stopValidation()
    {
        throw new Lib_Validator_Exception_Stop();
    }
    
    protected function stopValidationIfErrors()
    {
        if ( $this->hasErrors() )
        {
            throw new Lib_Validator_Exception_Stop();
        }
    }
    
    protected function addError( $code, array $messageParams = null, $index = null )
    {
        if( $code instanceof self )
        {
            $this->errors = array_merge( $this->errors, $code->getErrors() );
        }
        else
        {
            $this->errors[] = $this->buildError( $code, $messageParams, $index );
        }
    }
    
    protected function setError( $code, array $messageParams = null )
    {
        $this->clearErrors();
        
        if( $code instanceof self )
        {
            $this->setErrors( $code->getErrors() );
        }
        else
        {
            $this->addError( $code, $messageParams );
            
            $this->stopValidation();
        }
    }

    protected function buildError( $code, array $messageParams = null, $index = null )
    {
        $error = Lib_Api::buildError( $code, $this->getErrorMessage( $code, $messageParams ), $index );
        
        return $error;
    }
    
    protected function addMessageTemplates( array $messages )
    {
        $this->messageTemplates = array_merge( $this->messageTemplates, $messages );
    }
    
    protected function getErrorMessage( $code, array $params = null )
    {
        $params = (array)$params;
        
        if( !isset( $this->messageTemplates[ $code ] ) )
        {
            throw new OutOfRangeException( "Error message not defined for code '$code'" );
        }
        
        $template = $this->messageTemplates[ $code ];
        
        $search = array_map(
                function( $value ) {
                    return '%'.$value.'%';
                }, 
                array_keys( $params )
        );
        
        $replacement = array_values( $params );
                
        $message = str_replace( $search, $replacement, $template );
        
        return $message;
    }
    
    public function setIsValidCallback( callable $callback )
    {
        $this->callback = $callback;
    }
    
    protected function ifConditionThenError( $condition, $errorCode, $setError = TRUE )
    {
        if( $condition )
        {
            $params = NULL;
            
            if( is_array( $errorCode ) )
            {
                $params = isset( $errorCode[ 1 ] ) ? $errorCode[ 1 ] : NULL;
                $errorCode = $errorCode[ 0 ];
            }
            
            if( $setError )
            {
                $this->setError( $errorCode, $params );
            }
            else
            {
                $this->addError( $errorCode, $params );
            }
        }
        
        return (bool)$condition;
    }
    
    //==========================================================================
    
    protected function attachValidator( self $Validator, $value )
    {
        if( $Validator->isValid( $value ) )
        {
            return $Validator->getFilteredValue();
        }
        else
        {
            $this->addError( $Validator );
            
            $this->stopValidation();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    protected function ifEmpty( &$value, $errorCode = null, $setError = TRUE )
    {
        $condition = FALSE;
        
        if( is_string( $value ) )
        {
            $value = trim( $value );
            
            $condition = ( strlen( $value ) == 0 );
        }
        elseif( !is_numeric( $value) && !is_bool( $value ) )
        {
            $condition = empty( $value );
        }
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifZero( $value, $errorCode = null, $setError = TRUE )
    {
        $condition = is_scalar( $value ) && ( trim( $value ) === '0' );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifFalse( $value, $errorCode = null, $setError = TRUE )
    {
        $condition = ( $value == FALSE );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifTrue( $value, $errorCode = null, $setError = TRUE )
    {
        $condition = ( $value == TRUE );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotArray( $value, $errorCode = null, $setError = TRUE )
    {
        $condition = !is_array( $value );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotInArray( $value, array $array, $errorCode = null, $setError = TRUE )
    {
        $condition = TRUE;
        $stringValue = strval( $value );
        
        foreach( $array as $element )
        {
            if( strval( $element ) === $stringValue )
            {
                $condition = FALSE;
                break;
            }
        }
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifInArray( $value, array $array, $errorCode = null, $setError = TRUE )
    {
        $condition = FALSE;
        $stringValue = strval( $value );
        
        foreach( $array as $element )
        {
            if( strval( $element ) === $stringValue )
            {
                $condition = TRUE;
                break;
            }
        }
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotScalar( $value, $errorCode = null, $setError = TRUE )
    {
        $condition = !is_scalar( $value );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotInteger( &$value, $errorCode = null, $setError = TRUE )
    {
        $pattern = '/^[-+]?[0-9]+$/';
        
        $condition = !is_scalar( $value ) || !preg_match( $pattern, trim( $value ) );
        
        if( !$condition )
        {
            $value = intval( $value );
        }
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    /**
     * Separatorul de zecimale este virgula sau punctul
     */
    protected function ifNotFloat( &$value, $errorCode = null, $setError = TRUE )
    {
        $condition = !is_scalar( $value );
        
        if( !$condition )
        {
            $value_string = trim( $value );

            $precision = $this->getOption( self::OPT_FLOAT_PRECISION );
            $pattern = '/^[-+]?[0-9]+([,\.][0-9]+)?$/';

            $value_number = str_replace(',', '.', $value_string );

            $value_float = floatval( $value_number );

            $condition = !preg_match( $pattern, $value_string ) || $value_float != round( $value_float, $precision );
        }
        
        if( !$condition )
        {
            $value = round( $value_float, $precision );
        }
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifLongerThan( $value, $length, $errorCode = null, $setError = TRUE )
    {
        $condition = ( strlen( $value ) > $length );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifGreaterThan( $value, $target, $errorCode = null, $setError = TRUE )
    {
        $condition = ( $value > $target );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifLessThan( $value, $target, $errorCode = null, $setError = TRUE )
    {
        $condition = ( $value < $target );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotDate( &$value, $errorCode = null, $setError = TRUE )
    {
        try {
            $value = new DateTime( $value );
            $condition = false;
        }
        catch( Exception $e ){
            $condition = true;
        }
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifKeyNotSet( array $inputValues, $key, $errorCode = NULL, $setError = TRUE )
    {
        $condition = !isset( $inputValues[ $key ] );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifEqual( $value1, $value2, $errorCode = NULL, $setError = TRUE )
    {
        $condition = ( $value1 == $value2 );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotEqual( $value1, $value2, $errorCode = NULL, $setError = TRUE )
    {
        $condition = ( $value1 != $value2 );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifStrictEqual( $value1, $value2, $errorCode = NULL, $setError = TRUE )
    {
        $condition = ( $value1 === $value2 );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotStrictEqual( $value1, $value2, $errorCode = NULL, $setError = TRUE )
    {
        $condition = ( $value1 !== $value2 );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    protected function ifNotIncludedIn( array $source, array $target, $errorCode = NULL, $setError = TRUE )
    {
        $diff = array_diff( $source, $target );
        $condition = ( count( $diff ) != 0 );
        
        return ( $errorCode === NULL ? $condition : $this->ifConditionThenError( $condition, $errorCode, $setError ) );
    }
    
    //==========================================================================
    
    protected function validateInputFormat( $value )
    {
        $this->ifNotArray( $value, self::ERR_INVALID_FORMAT, true );
    }
    
    protected function getValueAtKey( array $array, $key, $default = NULL )
    {
        $value = isset( $array[ $key ] ) ? $array[ $key ] : $default;
        
        return $value;
    }
    
    protected function makeArray( &$value )
    {
        if( !is_array( $value ) )
        {
            $value = [];
        }
    }
    
}
