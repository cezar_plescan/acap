<?php

class Lib_Validator_Callback extends Lib_Validator_Abstract
{
    protected function validate( $callback )
    {
        if( !is_callable( $callback) )
        {
            trigger_error( 'Argument 1 is not a callable.' , E_USER_ERROR );
        }
        
        $arguments = [];
        
        if( func_num_args() > 1 )
        {
            $arguments = func_get_arg( 1 );
            
            if( !is_array( $arguments) )
            {
                trigger_error( 'Argument 2 is not an array.' , E_USER_ERROR );
            }
        }
        
        $db = Lib_Application::getInstance()->getDbAdapter();
        
        $returnValue = NULL;
            
        try
        {
            $db->beginTransaction();

            $returnValue = call_user_func_array( $callback, $arguments );

            $db->commit();
        }
        catch( Exception $e )
        {
            $db->rollBack();

            throw $e;
            //Lib_Logger_Exception::getInstance()->log( $e );
            //$this->_setError(self::ERR_OPERATION);
        }
        
        return $returnValue;
    }

}
