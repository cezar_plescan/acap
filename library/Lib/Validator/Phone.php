<?php

/**
 *
 */
class Lib_Validator_Phone extends Lib_Validator_String
{
    const MAX_LENGTH = 10;

    protected
            $pattern = '/^[0-9]*$/';

    protected function _isValid($value)
    {
        $value = parent::_isValid($value);

        if (!preg_match($this->pattern, $value))
        {
            $this->_setError(self::ERR_INVALID);
        }

        return $value;
    }

}
