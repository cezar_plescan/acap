<?php

class Lib_Validator_Schema_Exception_InvalidValues extends UnexpectedValueException
{
    public function __construct($values)
    {
        $message = 'Provides values should be an array :' . "\n" . print_r($values, TRUE);

        parent::__construct($message);
    }
}
