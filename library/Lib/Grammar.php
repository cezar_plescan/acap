<?php

class Lib_Grammar
{
    static protected $pluralForms = [
        'caracter'      => 'caractere',
        'ascensor'      => 'ascensoare',
    ];
    
    /**
     * pluralize( 3, 'caracter', 'e' )
     * -> '3 caractere' 
     * 
     * pluralize( 24, 'caracter', 'e' )
     * -> '24 de caractere' 
     * 
     * pluralize( 24, 'floare', NULL, 'flori' )
     * -> '24 de flori' 
     * 
     * @param int $counter
     * @param string $word Forma de singular a cuvantului
     * @param string $pluralSuffix Terminatia care se adauga la sfarsit, pentru a forma pluralul
     * @param string $pluralForm Aceasta este forma intreaga de plural pentru un cuvant la care nu se poate folosi terminatia de plural
     * 
     * @return string
     */
    static public function pluralize( $counter, $word, $pluralForm = NULL )
    {
        $expression = $counter . ' ';
        $counter = abs( $counter );

        if( $counter === 1 )
        {
            $expression .= $word;
        }
        else
        {
            if( ( $counter % 100 ) >= 20 || ( $counter % 100 ) == 0 ) 
            {
                $expression .= 'de ';
            }
            
            if( isset( self::$pluralForms[ $word ] ) )
            {
                $pluralForm = self::$pluralForms[ $word ];
            }
            elseif( !isset( $pluralForm ) )
            {
                $pluralForm = $word;
            }
            
            $expression .= $pluralForm;
        }
        
        return $expression;
    }
    
    static public function numeralOrdinal( $count, $genFeminin = false )
    {
        $text = '';

        if( $count == 1 )
        {
            $text = $genFeminin ? 'prima' : 'primul';
        }
        else if( $count > 1 )
        {
            $text = $genFeminin ? 
                    ( 'a ' . $count . '-a' ) : 
                    ( 'al ' . $count . '-lea' );
        }

        return $text;
    }
    
}
