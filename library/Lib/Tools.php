<?php
/**
 * Useful independent functions
 */
class Lib_Tools
{
    const FILTER_KEYS_STRICT  = 0;
    const FILTER_ALL_KEYS     = 1;
    const FILTER_COMMON_KEYS  = 2;
    
    /**
     * 
     * @param array $inputData
     * @param array $keys
     * @param int $filterType
     * @return array
     * @throws InvalidArgumentException
     */
    static public function filterArray( $inputData, array $keys, $filterType = self::FILTER_KEYS_STRICT )
    {
        $filteredData = [];

        if( $inputData === null )
        {
            $inputData = [];
        }
        elseif( !is_array( $inputData ) )
        {
            throw new InvalidArgumentException('Input data should be an array or null');
        }

        switch( $filterType )
        {
            case self::FILTER_KEYS_STRICT:
                
                foreach( $keys as $key )
                {
                    $filteredData[ $key ] = @$inputData[ $key ];
                }
                
                break;
            
            case self::FILTER_COMMON_KEYS:
                
                foreach( $keys as $key )
                {
                    if( array_key_exists( $key, $inputData ) )
                    {
                        $filteredData[ $key ] = $inputData[ $key ];
                    }
                }
                
                break;
            
            case self::FILTER_ALL_KEYS:
                
                $filteredData = $inputData;
                foreach( $keys as $key )
                {
                    if( !array_key_exists( $key, $filteredData ) )
                    {
                        $filteredData[ $key ] = NULL;
                    }
                }
                
                break;
        }

        return $filteredData;
    }

    static public function generateCode()
    {
        $code = str_shuffle(sha1(rand(0, 9999999)));

        return $code;
    }

    static public function sortByKey(&$array, $key)
    {
        usort(
                $array,
                function( $a, $b) use ($key)
                {
                    return strnatcasecmp($a[$key], $b[$key]);
                }
        );
    }

    static function getHtmlView( $filePath, array $vars = [] )
    {
        $View = new Zend_View();
        
        $View->assign( $vars );
        
        $dirPath = dirname( $filePath );
        $View->addScriptPath( $dirPath );
        
        $filename = basename( $filePath );
        $html = $View->render( $filename );
        
        return $html;
    }
    
    static function generateRandomString( $min, $maxLength = null, $char = null )
    {
        if( $maxLength === null )
        {
            $maxLength = $min;
        }
        
        $domain = 'abcdefghijklmnopqrstuvwxyz'
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789!@#$%^&*()';
        
        $string = '';
        
        $length = rand($min, $maxLength);
        for( $i = 0; $i < $length; $i++ )
        {
            $string .= ( $char !== null ? $char : $domain[ rand( 0, strlen( $domain ) - 1 ) ]);
        }
        
        return $string;
    }
    
    /**
     * Separatorul de zecimale este virgula (,).
     * Delimitatorul pentru grupurile de 3 cifre este punctul (.).
     * 
     * @param type $number
     * @param type $precision
     * @return type
     */
    static public function formatNumber( $number, $precision = 2 )
    {
        $format = number_format( $number, $precision, ',' , '.' );
        
        return $format;
    }
    
    static public function checkInputDataKeys( $inputData, $keys, $throwException = TRUE )
    {
        if( !is_array( $inputData ) )
        {
            throw new UnexpectedValueException( 'Value is not an array.' );
        }
        
        if( !is_array( $keys ) )
        {
            $keys = (array)$keys;
        }
        
        $pointer = $inputData;
        $keysEnum = [];

        foreach( $keys as $key )
        {
            $keysEnum[] = $key;

            if( !isset( $pointer[ $key ] ) )
            {
                $keysText = implode('/', $keysEnum );

                if( $throwException )
                {
                    throw new UnexpectedValueException( 'Missing key: "'. $keysText .'"' );
                }
                else
                {
                    return NULL;
                }
            }
            
            $pointer = $pointer[ $key ];
        }
        
        return $pointer;
    }
    
}
