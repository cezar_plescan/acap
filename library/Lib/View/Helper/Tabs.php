<?php

class Lib_View_Helper_Tabs extends Zend_View_Helper_Abstract
{
    public function tabs( array $tabs, array $options = [] )
    {
        $idAttr = isset( $options[ 'id_attr' ] ) ? 
                'id="'.$options[ 'id_attr' ].'"' :
                '';
                
        $outerTemplate = '<div '. $idAttr .' class="'. @$options[ 'css_class' ] .' tabs-container">%s</div>';
        $innerTemplate = '<div class="tab-item-wrapper" data-id="%s">%s</div>';
        $content = '';
        
        $this->translateInput( $tabs );
        
        foreach( $tabs as $tab )
        {
            $tabId      = $tab[ 'id' ];
            $tabLabel   = $tab[ 'label' ];
                    
            $tabWrapper = sprintf( $innerTemplate, $tabId, $tabLabel );
            
            $content .= $tabWrapper . "\n";
        }
        
        $output = sprintf( $outerTemplate, $content );
        
        return $output;
    }
    
    protected function translateInput( array &$input )
    {
        $firstValue = reset( $input );
        $firstKey = key( $input );
        
        $translation = [];
        if( !is_numeric( $firstKey ) )
        {
            foreach( $input as $tabId => $tabLabel )
            {
                $translation[] = [
                    'id'        => $tabId,
                    'label'     => $tabLabel,
                ];
            }
            
            $input = $translation;
        }
        
    }
    
}

