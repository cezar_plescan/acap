<?php

class Lib_View_Helper_IncludeView extends Zend_View_Helper_Partial
{
    static protected $views = [];
    
    public function includeView( $relativePath, array $options = [] )
    {
        $scriptPath = $this->getScriptPath();
        $dirName    = dirname( $scriptPath );
        $viewPath   = realpath( $dirName  . DIRECTORY_SEPARATOR . $relativePath );

        $option_includeOnce = !empty( $options[ 'once' ] );
        
        if( $option_includeOnce && !empty( self::$views[ $viewPath ] ) )
        {
            $output = '';
        }
        else
        {
            $option_params = isset( $options[ 'params' ] ) && is_array( $options[ 'params' ] ) ?
                    $options[ 'params' ] :
                    [];
            
            $option_relativePath = isset( $options[ 'relative' ] ) ?
                    (bool)$options[ 'relative' ] :
                    ( $relativePath[ 0 ] == '/' ? false : true );

            $viewParams = array_merge( $this->view->getVars(), $option_params );

            if( $option_relativePath )
            {
                if( $viewPath === false )
                {
                    throw new UnexpectedValueException( "View $relativePath not found in $dirName" );
                }

                $viewDir = dirname( $viewPath );
                $viewName = basename( $viewPath );

                /* @var $View Zend_View */
                $View = $this->cloneView();
                $View->addScriptPath( $viewDir );

                $View->assign( $viewParams );
                $output = $View->render( $viewName );
            }
            else
            {
                $output = $this->partial( $relativePath, null, $viewParams );
            }

            self::$views[ $viewPath ] = true;
        }
        
        return $output;
    }
    
    protected function getScriptPath()
    {
        $trace = debug_backtrace();
        $view = null;
        $file = null;
        
        for( $i = 1; $i < count( $trace ); $i++ )
        {
            if( @$trace[ $i ][ 'object' ] instanceof Zend_View )
            {
                $view = $trace[ $i ][ 'object' ];
                
                break;
            }
        }
        
        if( $view )
        {
            $reflectionObject = new ReflectionClass( 'Zend_View_Abstract' );

            $reflectionProperty = $reflectionObject->getProperty('_file');
            $reflectionProperty->setAccessible(true);
            $file = realpath( $reflectionProperty->getValue( $view ) );
        }
        else
        {
            throw new RuntimeException( 'View Object not found' );
        }
        
        return $file;
    }
    
}

