<?php

class Lib_View_Helper_Panes extends Zend_View_Helper_Abstract
{
    public function panes( array $panes, array $options = [] )
    {
        $idAttr = isset( $options[ 'id_attr' ] ) ? 
                'id="'.$options[ 'id_attr' ].'"' :
                '';
                
        $outerTemplate = '<div '. $idAttr .' class="'. @$options[ 'css_class' ] .' panes-container">%s</div>';
        $innerTemplate = '<div class="pane-wrapper" data-id="%s">%s</div>';
        $content = '';
        
        $this->translateInput( $panes );
        
        foreach( $panes as $pane )
        {
            $paneId = $pane[ 'id' ];
            $paneHtml = $this->view->includeView( $pane[ 'file_path' ], [ 'relative' => @$pane[ 'relative' ] ] );
                    
            $paneWrapper = sprintf( $innerTemplate, $paneId, $paneHtml );
            
            $content .= $paneWrapper . "\n";
        }
        
        $output = sprintf( $outerTemplate, $content );
        
        return $output;
    }
    
    protected function translateInput( array &$input )
    {
        $firstValue = reset( $input );
        $firstKey = key( $input );
        
        $translation = [];
//        if( !is_numeric( $firstKey ) && !is_array( $firstValue ) )
        if( !is_array( $firstValue ) )
        {
            foreach( $input as $paneId => $filePath )
            {
                $translation[] = [
                    'id'        => $paneId,
                    'file_path' => $filePath,
                ];
            }
            
            $input = $translation;
        }
        
    }
    
}

