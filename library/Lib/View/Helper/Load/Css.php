<?php

class Lib_View_Helper_Load_Css extends Lib_View_Helper_Load_Resources
{
    protected
            $resourceDirectory = 'css',
            $htmlTemplate = '<link rel="stylesheet" href="/%resource_directory%/%resource%" />';

    public function load_css($module)
    {
        $script = $this->loadResources($module);

        return $script;
    }
}

