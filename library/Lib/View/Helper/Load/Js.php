<?php

class Lib_View_Helper_Load_Js extends Lib_View_Helper_Load_Resources
{
    protected
            $resourceDirectory = 'js',
            $htmlTemplate = '<script type="text/javascript" src="/%resource_directory%/%resource%"></script>';

    public function load_js($module)
    {
        $script = $this->loadResources($module);

        return $script;
    }
}

