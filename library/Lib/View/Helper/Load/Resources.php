<?php

abstract class Lib_View_Helper_Load_Resources extends Zend_View_Helper_Abstract
{
    protected
            /* @var $resourceDirectory string Public directory where resources(files) are located */
            $resourceDirectory,
            $htmlTemplate;

    /**
     * Reads and sorts files from a public directory.
     *
     * @param string $moduleDir    Optionally, resources can be located also in another directory
     *
     * @return array            The sorted list of file paths
     */
    protected function loadResources( $moduleDir = null )
    {
        $publicDirectoryPath = Lib_Application::getInstance()->getConfig( [ 'paths', 'public' ] );
        $baseDirPath = realpath( $publicDirectoryPath . DIRECTORY_SEPARATOR . $this->resourceDirectory );

        $resources = @scandir( $baseDirPath );
        if ($resources)
        {
            // keep only children files
            foreach( $resources as $i => $resource )
            {
                if( !$this->isValid( $resource, $baseDirPath ) )
                {
                    unset( $resources[ $i ] );
                }
            }

            // load files from $moduleDir
            if( null !== $moduleDir )
            {
                $moduleDirPath = realpath( $baseDirPath );
                $moduleDirParts = explode( DIRECTORY_SEPARATOR , $moduleDir );
                $currentRelativePath = '';
                
                foreach( $moduleDirParts as $directoryName )
                {
                    $currentRelativePath .= ( $directoryName . DIRECTORY_SEPARATOR );
                    $moduleDirPath .= ( DIRECTORY_SEPARATOR . $directoryName );
                    
                    $mResources = @scandir( $moduleDirPath );
                    if( $mResources )
                    {
                        foreach( $mResources as $resource )
                        {
                            if( $this->isValid( $resource, $moduleDirPath ) )
                            {
                                $resources[] = $currentRelativePath . $resource;
                            }
                        }
                    }
                }
            }

            $script = $this->buildHtml( $resources );

            return $script;
        }


        return '';
    }

    /**
     * Check if a filename will be ignored or not
     *
     * @param string $filename
     */
    protected function isValid($filename, $path)
    {
        // only regular files are kept
        if (!is_file($path. DIRECTORY_SEPARATOR . $filename))
        {
            return FALSE;
        }

        // if a file begins with '-' it will be ignored
        if ($filename[0] == '-')
        {
            return false;
        }

        return true;
    }

    protected function buildHtml($resources)
    {
        $script = '';
        if ($resources)
        {
            foreach ($resources as $resource)
            {
                $html = str_replace(
                    array(
                        '%resource_directory%',
                        '%resource%',
                    ),
                    array(
                        $this->resourceDirectory,
                        $resource,
                    ),
                    $this->htmlTemplate
                );
                $script .= $html . "\n";
            }
        }

        return $script;
    }
}

