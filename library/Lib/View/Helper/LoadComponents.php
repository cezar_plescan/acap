<?php

class Lib_View_Helper_LoadComponents extends Zend_View_Helper_Abstract
{
    protected
            $resourceDirectory = 'css',
            $htmlTemplate = '<link rel="stylesheet" href="/%resource_directory%/%resource%" />';

    public function loadComponents()
    {
        $script = $this->loadResources($module);

        return $script;
    }
    
}

