<?php

class Lib_View_Helper_LoadJs extends Lib_View_Helper_Load_Resources
{
    protected
            $resourceDirectory = 'js',
            $htmlTemplate = '<script type="text/javascript" src="/%resource_directory%/%resource%"></script>';

    public function loadJs($module)
    {
        $script = $this->loadResources($module);

        return $script;
    }
}

