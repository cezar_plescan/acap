<?php

class Lib_Session
{
    protected
            $namespace = 'app_session',
            $sessionInstance;

    static protected $instance = null;

    protected function __construct()
    {
        $this->sessionInstance = new Zend_Session_Namespace(self::$_namespace);

    }

    /**
     *
     * @return Lib_Session
     */
    static public function getInstance()
    {
        if (static::$instance == null)
        {
            static::$instance = new static;
        }

        return static::$instance;
    }


    public function set($key, $value)
    {
        $this->sessionInstance->$key = $value;
    }

    public function get($key)
    {
        $value = $this->sessionInstance->$key;

        return $value;
    }

    public function getOnce($key)
    {
        $value = $this->sessionInstance->$key;
        $this->delete($key);

        return $value;
    }

    public function delete($key)
    {
        unset($this->sessionInstance->$key);
    }

}
