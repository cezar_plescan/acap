<?php

/**
 * Singleton class
 */
class Lib_Logger_Exception extends Lib_Logger
{
    protected static $instance;

    public function log($exception)
    {
        if (! ($exception instanceof Exception) ) return;

        if ( Lib_Application::getInstance()->getConfig(['resources', 'frontController', 'params', 'displayExceptions']) )
        {
            throw $exception;
        }

        ///////////////////////////////////////////////////////////////
        $content = $exception->getMessage();
        $content .= "\n\n" . print_r( $exception->getTrace(), true );

        $this->write( $content );
    }

    protected function write($content)
    {
        $path       = Lib_Application::getInstance()->getConfig(['paths', 'log']);
        $path       = realpath($path);
        $filename   = Lib_Application::getInstance()->getConfig(['logger', 'exception', 'file']);
        $file       = $path . DIRECTORY_SEPARATOR . $filename;

        // add timestamp
        $timestampFormat = Lib_Application::getInstance()->getConfig(['settings', 'db_datetime_format']);
        $timestamp = '[' . Lib_Date::getInstance()->now($timestampFormat) . ']' . "\n";
        $content = $timestamp . "\n" . $content . "\n\n-------------------------------\n";

        file_put_contents($file, $content, FILE_APPEND);
    }
}
