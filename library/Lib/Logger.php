<?php

/**
 * Singleton class
 */
abstract class Lib_Logger
{
    protected function __construct() {}

    /**
     *
     * @return Lib_Logger
     */
    static public function getInstance()
    {
        if (static::$instance == null)
        {
            static::$instance = new static;
        }

        return static::$instance;
    }

    abstract public function log($event);
}
