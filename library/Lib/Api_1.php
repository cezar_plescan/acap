<?php

abstract class Lib_Api
{
    protected
            $errors,
            $data,
            $isSuccessful = false,
            $arguments = [],
            $validator,
            $mainValidatorSchema,
            $postValidatorSchema;

    public function __construct()
    {

    }

    protected function reset()
    {
        $this->errors = null;
        $this->data = null;
        $this->isSuccessful = false;
        $this->arguments = [];
        $this->validatorSchema = null;
        $this->mainValidatorSchema = null;
        $this->postValidatorSchema = null;
    }

    public function onSuccess(array $filteredValues) {}

    /**
     * @overridable
     */
    protected function initValidatorSchemas() {}
    
    protected function _defineMainValidators()
    {
        return array();
    }

    protected function _defineValidators()
    {
        return array();
    }

    protected function _definePostValidators()
    {
        return array();
    }

    protected function getArguments()
    {
        return $this->arguments;
    }

    protected function getArgument($argument)
    {
        return @$this->arguments[$argument];
    }

    /**
     *
     * @return Lib_Validator_Schema
     */
    protected function getValidatorSchema(array $filteredMainValues)
    {
        if ( null == $this->validatorSchema )
        {
            $validators = $this->_defineValidators( $filteredMainValues );

            $this->validatorSchema = new Lib_Validator_Schema($validators);
        }

        return $this->validatorSchema;
    }

    /**
     *
     * @return Lib_Validator_Schema
     */
    protected function getMainValidatorSchema()
    {
        if ( null == $this->mainValidatorSchema )
        {
            $validators = $this->_defineMainValidators();

            $this->mainValidatorSchema = new Lib_Validator_Schema($validators);
        }

        return $this->mainValidatorSchema;
    }

    /**
     *
     * @return Lib_Validator_Schema
     */
    protected function getPostValidatorSchema()
    {
        if ( null == $this->postValidatorSchema )
        {
            $validators = $this->_definePostValidators();

            $this->postValidatorSchema = new Lib_Validator_Schema($validators);
        }

        return $this->postValidatorSchema;
    }

    protected function setData($data)
    {
        $this->data = $data;
    }

    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Called before validating the values
     * 
     * @overridable
     */
    protected function beforeRun(){}

    /**
     * @overridable
     * @return {array} The processed arguments
     */
    protected function preProcessArguments( array $arguments )
    {
        return $arguments;
    }

    public function run(array $arguments)
    {
//        try ????
//        {
            $this->reset();
            
            $this->initValidatorSchemas();
            
            $arguments = $this->preProcessArguments( $arguments );
            
            $this->arguments = $arguments;

            $this->beforeRun();

            $mainValidatorSchema = $this->getMainValidatorSchema();

            // apply main validators
            if ($mainValidatorSchema->isValid($arguments))
            {
                $filteredMainValues = $mainValidatorSchema->getFilteredValue();

                // apply validators
                $validatorSchema = $this->getValidatorSchema( $filteredMainValues );

                if ($validatorSchema->isValid($arguments))
                {
                    // apply post validators
                    $postValidatorSchema = $this->getPostValidatorSchema();

                    if ($postValidatorSchema->isValid($arguments))
                    {
                        // retrieve filtered values
                        $filteredValues = $validatorSchema->getFilteredValue();
                        $filteredValues += $filteredMainValues;

                        $operationValidator = new Lib_Validator_Callback();
                        if ($operationValidator->isValid(array($this, 'onSuccess'), [$filteredValues]))
                        {
                            $this->setSuccessful();
                            $this->setData($operationValidator->getCallbackReturn());
                        }
                        else
                        {
                            $this->setErrors([$operationValidator->getError()]);
                        }
                    }
                    else
                    {
                        $this->setErrors($postValidatorSchema->getErrors());
                    }

                }
                else
                {
                    $this->setErrors($validatorSchema->getErrors());
                }
            }
            else
            {
                $this->setErrors($mainValidatorSchema->getErrors());
            }
//        }
//        catch( Exception $e )
//        {
//            throw $e;
//        }
    }

    protected function setSuccessful($success = true)
    {
        $this->isSuccessful = (bool)$success;
    }

    public function isSuccessful()
    {
        return $this->isSuccessful;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function checkResponse($response, $returnResponse = false)
    {
        if ( OPERATION_ERROR === $response )
        {
            return OPERATION_ERROR;
        }

        if ( $returnResponse )
        {
            return $response;
        }

        return NULL;
    }

}
