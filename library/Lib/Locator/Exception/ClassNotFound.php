<?php

class Lib_Locator_Exception_ClassNotFound extends UnexpectedValueException
{
    public function __construct($objectClass, $originalObjectClass)
    {
        $message = 'The Object "'. $objectClass .'" was not found near the "' . $originalObjectClass .'" object';

        parent::__construct($message);
    }
    
}
