<?php

/**
 * Determines specific class names for models
 *
 */
class Lib_Locator_Class extends Lib_Locator_Abstract
{
    const DELIMITER = '_';

    /**
     * Removes the last part from a class name
     *
     * @param int $count
     */
    public function removeSuffix( $className, $count = 1 )
    {
        $parts = explode( self::DELIMITER, $className );
        array_splice( $parts, - $count );

        $newClassName = implode( self::DELIMITER, $parts );

        return $newClassName;
    }

    /**
     * Generates a new class name that is a sibling with another class
     * It replaces the filename from the base class name with the name of the object class
     *
     * @param string $currentClassName The current class
     * @param string $newClassName The name of the object
     *
     * @return string
     */
    public function generateClassName( $currentClassName, $newClassName, $searchParents = false, $levelsUp = 0 )
    {
        $continue = false;
        $classExists = false;

        $originalClassName = $currentClassName;

        if( $levelsUp > 0 )
        {
            $currentClassName = $this->removeSuffix( $currentClassName, $levelsUp );
        }
        
        do
        {
            $parts = explode( self::DELIMITER, $currentClassName );
            $parts[ count( $parts ) - 1 ] = $newClassName;
            $objectClass = implode( self::DELIMITER, $parts );

            $classExists = @class_exists( $objectClass );

            if ( !$classExists )
            {
                if ( $searchParents )
                {
                    $currentClassName = get_parent_class( $currentClassName );

                    $continue = (bool)$currentClassName;
                }
                else
                {
                    $continue = false;
                }
            }
            elseif ( $classExists )
            {
                $continue = false;
            }
        }
        while ( $continue );

        if ( !$classExists )
        {
            throw new Lib_Locator_Exception_ClassNotFound( $newClassName, $originalClassName );
        }

        return $objectClass;
    }

}
