<?php

/**
 * Singleton class
 */
class Lib_Date
{
    static protected $instance = null;

    protected
            $now,
            $dbDateFormat,
            $dbDateTimeFormat;

    static protected $daysOfWeek = [
        0   => 'Duminică',
        1   => 'Luni',
        2   => 'Marţi',
        3   => 'Miercuri',
        4   => 'Joi',
        5   => 'Vineri',
        6   => 'Sâmbătă',
        7   => 'Duminică',
    ];

    static protected $months = [
        1   => 'Ianuarie',
        2   => 'Februarie',
        3   => 'Martie',
        4   => 'Aprilie',
        5   => 'Mai',
        6   => 'Iunie',
        7   => 'Iulie',
        8   => 'August',
        9   => 'Septembrie',
        10   => 'Octombrie',
        11   => 'Noiembrie',
        12   => 'Decembrie',
    ];

    static protected $seconds = array(
        'minute'    => 60,
        'hour'      => 3600,
        'day'       => 86400,
    );

    protected function __construct()
    {
        $this->dbDateFormat = Lib_Application::getInstance()->getConfig( [ 'settings', 'db_date_format' ] );
        $this->dbDateTimeFormat = Lib_Application::getInstance()->getConfig( [ 'settings', 'db_datetime_format' ] );
    }

    /**
     *
     * @return Lib_Date
     */
    static public function getInstance()
    {
        if (static::$instance == null)
        {
            static::$instance = new static;
        }

        return static::$instance;
    }

    static public function getDayOfWeekLabel( $day )
    {
        return self::$daysOfWeek[ $day ];
    }

    static public function getMonthLabel( $month )
    {
        return self::$months[ $month ];
    }

    //--------------------------------------------------------------------------
    
    public function getUTCDateTime()
    {
        $format = $this->dbDateTimeFormat;
        $dateTime = gmdate( $format );
        
        return $dateTime;
    }
    
    //--------------------------------------------------------------------------
    
    public function getMonths()
    {
        return self::$months;
    }
    
    public function now( $format = null )
    {
        if (null === $format)
        {
            $format = $this->dbDateFormat;
        }

        $now = date( $format, $this->now ? $this->now : time() );

        return $now;
    }

    protected function getTimestamp()
    {
        return $this->now ? $this->now : time();
    }

    public function getCurrentMonthId()
    {
        $dates = getdate( $this->getTimestamp() );

        $year = $dates['year'];
        $month = $dates['mon'];

        $monthId = $year . str_pad($month, 2, '0', STR_PAD_LEFT);

        return $monthId;
    }

    public function addMonths( $monthId, $add )
    {
        $parts = str_split( $monthId, 4 );

        $year = $parts[0];
        $month = (int)$parts[1];

        $monthAdd = $month + $add;

        $month = $monthAdd % 12;
        if ( $month < 0 )
        {
            $month += 12;
        }
        if ( $month == 0 )
        {
            $month = 12;
        }

        $year  += floor( ($monthAdd - 1) / 12 );

        $newMonthId = $year . str_pad($month, 2, '0', STR_PAD_LEFT);

        return $newMonthId;
    }

    public function getCurrentDayOfWeek()
    {
        $day = $this->now('N');

        return $day;
    }

    public function getCurrentDay()
    {
        $day = $this->now('j');

        return $day;
    }

    public function getCurrentYear()
    {
        $day = $this->now('Y');

        return $day;
    }

    public function setDate($date)
    {
        if (!is_numeric($date))
        {
            $date = strtotime($date);
        }

        $this->now = $date;
    }

    public function format($date, $format = null, $throwException = true)
    {
        if (null === $format)
        {
            $format = $this->dbDateFormat;
        }

        if (is_numeric($date))
        {
            $timestamp = $date;
        }
        else
        {
            $timestamp = strtotime($date);
        }

        if (!$timestamp)
        {
            if ($throwException)
            {
                throw new InvalidArgumentException('Invalid date: "' . $date . '"');
            }
            else
            {
                return false;
            }
        }
        else
        {
            $date = date($format, $timestamp);

            return $date;
        }

    }

    protected function compare($date1, $date2, $format)
    {
        $date1 = $this->format($date1, $format);
        $date2 = $this->format($date2, $format);

        $compare = strcmp($date1, $date2);

        return $compare;
    }

    public function compareDays($date1, $date2)
    {
        $format = 'Ymd';

        $compare = $this->compare($date1, $date2, $format);

        return $compare;
    }

    public function daysDiff($date1, $date2)
    {
        $date1 = strtotime($this->format($date1, 'Y-m-d'));
        $date2 = strtotime($this->format($date2, 'Y-m-d'));

        $diff = round(($date1 - $date2) / self::$seconds['day']);

        return $diff;
    }

    public function addDays($date, $days, $format = null)
    {
        $newDate = strtotime($date . ' +' . $days . 'days');
        $newDate = $this->format($newDate, $format);

        return $newDate;
    }

}
