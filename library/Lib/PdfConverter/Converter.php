<?php

class Lib_PdfConverter_Converter
{
    protected
            /** @var PdfCrowd */
            $worker,
            $output;


    public function __construct()
    {
        require_once 'pdfcrowd.php';

        $credentials = Lib_Application::getInstance()->getConfig( 'pdfcrowd' );
        $this->worker = new PdfCrowd( $credentials[ 'username' ], $credentials[ 'key' ] );

        // A3 size, portrait
        $this->worker->setPageWidth( '297mm' );
        $this->worker->setPageHeight( '420mm' );

        $this->worker->setHtmlZoom( 800 );
        $this->worker->setPdfScalingFactor( 2 );

    }

    public function convert( $html )
    {
        $this->output = $this->worker->convertHtml( $html );
    }

    public function getOutput()
    {
        return $this->output;
    }

}
