<?php

abstract class Lib_Api
{
    const RESPONSE_DATA     = 'data';
    const RESPONSE_ERRORS   = 'errors';
    const RESPONSE_SUCCESS  = 'success';
    
    const ERROR_CODE        = 'code';
    const ERROR_MESSAGE     = 'message';
    const ERROR_INDEX       = 'index';
    
    const ERROR_INDEX_DELIMITER = ';';
    
    protected
            $errors,
            $data,
            $isSuccessful = false,
            $arguments = [],
            $validator;

    abstract public function onSuccess( array $filteredValues = NULL );

    /**
     * @return Lib_Validator_Api
     */
    abstract protected function initValidator();
    
    public function __construct()
    {
        $this->init();
    }

    protected function init(){}
    
    protected function reset()
    {
        $this->errors = null;
        $this->data = null;
        $this->isSuccessful = false;
        $this->arguments = [];
    }

    protected function getArguments()
    {
        return $this->arguments;
    }

    protected function getArgument($argument)
    {
        return @$this->arguments[$argument];
    }

    protected function setData($data)
    {
        $this->data = $data;
    }

    protected function setErrors( $errors )
    {
        $this->errors = $errors;
    }

    /**
     * Called before validating the values
     * 
     * @overridable
     */
    protected function beforeRun(){}

    public function run( array $arguments )
    {
        $this->reset();

        $this->arguments = $arguments;

        if( $this->beforeRun() === FALSE )
        {
            return;
        }

        $this->loadValidator();
        $Validator = $this->validator;
        
        if ( $Validator->isValid( $arguments ) )
        {
            // retrieve filtered values
            $filteredValue = $Validator->getFilteredValue();

            $operationValidator = new Lib_Validator_Callback();
            if ( $operationValidator->isValid( [ $this, 'onSuccess' ], [ $filteredValue ] ) )
            {
                $this->setSuccessful();
                $this->setData( $operationValidator->getFilteredValue() );
            }
            else
            {
                $this->setErrors( $operationValidator->getErrors() );
            }
        }
        else
        {
            $this->setErrors( $Validator->getErrors() );
        }
        
    }

    protected function loadValidator()
    {
        $this->validator = $this->initValidator();
        if( $this->validator === NULL )
        {
            $this->validator = new Lib_Validator_Pass();
        }
        
        if( !( $this->validator instanceof Lib_Validator_Abstract ) )
        {
            throw new Lib_Api_Exception_InvalidValidator();
        }
    }
    
    protected function setSuccessful($success = true)
    {
        $this->isSuccessful = (bool)$success;
    }

    public function isSuccessful()
    {
        return $this->isSuccessful;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getResponse()
    {
        $response = array(
            self::RESPONSE_SUCCESS      => $this->isSuccessful(),
        );

        $data = $this->getData();
        // old code - ???
//        if (null !== $data && (!is_array($data) || 0 != count($data)))
//        {
//            $response[ self::RESPONSE_DATA ] = $data;
//        }

        if( $data !== NULL )
        {
            $response[ self::RESPONSE_DATA ] = $data;
        }
        
        $errors = $this->getErrors();
        if (null !== $errors && (!is_array($errors) || 0 != count($errors)))
        {
            $response[ self::RESPONSE_ERRORS ] = $errors;
        }

        return $response;
    }
    
    static public function buildSuccessfulResponse( $data = NULL )
    {
        $response = [
            self::RESPONSE_SUCCESS  => 1,
        ];

        if( $data !== NULL )
        {
            $response[ self::RESPONSE_DATA ] = $data;
        }
        
        return $response;
    }
    
    static public function buildErrorResponse( $errorCode, $errorMessage = null, $errorIndex = null )
    {
        $response = [
            self::RESPONSE_SUCCESS  => 0,
        ];

        $response[ self::RESPONSE_ERRORS ] = 
            is_array( $errorCode ) ?
                ( 
                    isset( $errorCode[ self::ERROR_CODE ] ) ?
                        [ $errorCode ] :
                        $errorCode 
                ) :
                [ self::buildError( $errorCode, $errorMessage, $errorIndex ) ];
        
        return $response;
    }
    
    static public function buildError( $code, $message, $index = null )
    {
        $error = [
            self::ERROR_CODE        => $code,
            self::ERROR_MESSAGE     => $message,
        ];
        
        if ( $index !== null )
        {
            $error[ self::ERROR_INDEX ] = self::buildErrorIndex( $index );
        }
        
        return $error;
    }

    static protected function buildErrorIndex( $index )
    {
        if ( is_array( $index ) )
        {
            $index = implode( self::ERROR_INDEX_DELIMITER, $index );
        }
        
        return $index;
    }
    

    // ? not sure if it is useful
    protected function checkResponse($response, $returnResponse = false)
    {
        if ( OPERATION_ERROR === $response )
        {
            return OPERATION_ERROR;
        }

        if ( $returnResponse )
        {
            return $response;
        }

        return NULL;
    }

    protected function populateDataIfKeyExists( array &$returnData, $key, array $from, $value )
    {
        if( array_key_exists( $key, $from ) )
        {
            $valueToPopulate = ( is_callable( $value ) ? $value() : $value );
            
            if( is_array( $valueToPopulate ) )
            {
                $returnData = array_merge( $returnData, $valueToPopulate );
            }
            else
            {
                $returnData[ $key ] = $valueToPopulate;
            }
        }
    }
    
}
