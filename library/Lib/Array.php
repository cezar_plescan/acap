<?php

class Lib_Array
{
    
    static public function removeElement( array &$array, $key )
    {
        array_splice( $array, $key, 1 );
    }
    
    static public function merge( array &$array, array $newArray )
    {
        $array = array_merge( $array, $newArray );
    }
    
}
