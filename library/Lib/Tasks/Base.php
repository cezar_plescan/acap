<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../../application'));

require_once '../Application.php';

// create the Application Instance
Lib_Application::getInstance();

