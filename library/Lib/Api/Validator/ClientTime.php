<?php

class Lib_Api_Validator_ClientTime extends Lib_Validator_Abstract
{
    const ERR_REQUIRED  = 'required';
    const ERR_INVALID   = 'invalid';
    const ERR_SYNC      = 'sync';
    
    const SYNC_TOLERANCE    = 5; // 5 minutes
    
    protected function init()
    {
        $this->addMessageTemplates([
            self::ERR_REQUIRED          => 'The client time is missing.',
            self::ERR_INVALID           => 'The client time is invalid.',
            self::ERR_SYNC              => 'Data sau ora dispozitivului nu sunt corecte.',
        ]);
    }

    /**
     * 
     */
    protected function validate( $time )
    {
        if( !isset( $time ) )
        {
            $this->setError( self::ERR_REQUIRED );
        }
        else
        {
            if( !$this->ifNotDate( $time, self::ERR_INVALID ) )
            {
                $this->validateTimeSync( $time );
            }
        }
        
        return $time;
    }
    
    protected function validateTimeSync( DateTime $clientTime )
    {
        $serverTime = new DateTime();
        
        $serverTimestamp = $serverTime->getTimestamp();
        $clientTimestamp = $clientTime->getTimestamp();
        
        if( abs( $serverTimestamp - $clientTimestamp ) > $this->getSyncTolerance() )
        {
            $this->setError( self::ERR_SYNC );
        }
    }
    
    private function getSyncTolerance()
    {
        $tolerance = 60 * Lib_Application::getInstance()->getConfig( [ 'settings', 'client_time_tolerance' ], self::SYNC_TOLERANCE );
        
        return $tolerance;
    }
    
}
