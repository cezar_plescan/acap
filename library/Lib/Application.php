<?php

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

require_once 'Zend/Session.php';
require_once 'Zend/Application.php';
require_once 'Zend/Config/Ini.php';

/**
 * Singleton Class
 */
class Lib_Application extends Zend_Application
{
    const APPLICATION_DEFAULT_SECTION = 'application';

    protected
            $local_config_dir,
            $config,
            $User;

    static protected $instance = null;

    /**
     *
     * @return Lib_Application
     */
    static public function getInstance()
    {
        if (static::$instance == null)
        {
            static::$instance = new static;
        }

        return static::$instance;
    }

    public function __construct()
    {
        $this->defineConstants();

        date_default_timezone_set( 'Europe/Bucharest' );

        $this->local_config_dir = realpath(APPLICATION_PATH . '/../local_config/');

        $environment = $this->_getEnvironment();

        $config = $this->getConfig();

        parent::__construct($environment, $config);

        // Ensure all communications are managed by sessions.
        Zend_Session::start();

        $this->bootstrap();
        
        $this->loadModuleBootstrap();
    }

    static public function close()
    {
        static::$instance = NULL;
        
        Zend_Db_Table::getDefaultAdapter()->closeConnection();
    }
    
    protected function defineConstants()
    {
        defined('OPERATION_ERROR') || define('OPERATION_ERROR', FALSE);
        defined('PHP_INT_MIN') || define('PHP_INT_MIN', ~PHP_INT_MAX);
    }
    
    protected function _getEnvironment()
    {
        // Define application environment
        // - read from local config
        // - read from env variable

        if( defined('APPLICATION_ENV') )
        {
            return APPLICATION_ENV;
        }

        $localEnvFile = $this->local_config_dir . DIRECTORY_SEPARATOR . 'environment';
        if (file_exists($localEnvFile))
        {
            $environment = file_get_contents($localEnvFile);
        }
        else
        {
            $environment = getenv('APPLICATION_ENV');
        }

        $environment = trim($environment);
        if ($environment === '')
        {
            $environment = self::APPLICATION_DEFAULT_SECTION;
        }

        define('APPLICATION_ENV', $environment);

        return $environment;
    }

    /**
     * Returns application configuration values
     *
     * @param string|array $key A configuration key or a path to the configuration key
     * @param mixed $default The default value for a non-existing configuration value
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getConfig($key = null, $default = null)
    {
        if (null === $this->config)
        {
            $this->config = $this->readConfig();
        }

        if (null === $key)
        {
            return $this->config;
        }

        $value = $this->config;
        $keys = (array)$key;

        foreach ($keys as $key)
        {
            if (is_array($value) && isset($value[$key]))
            {
                $value = $value[$key];
            }
            else if (null !== $default)
            {
                return $default;
            }
            else
            {
                throw new Lib_Application_Exception_InvalidConfigKey('Invalid config key: "' . implode('.', $keys) . '"');
            }
        }

        return $value;
    }

    protected function readConfig()
    {
        $appConfigFile = APPLICATION_PATH . '/configs/application.ini';

        $config = new Zend_Config_Ini(
                $appConfigFile,
                self::APPLICATION_DEFAULT_SECTION,
                array('allowModifications' => true)
        );

        $localConfigFile = $this->local_config_dir . DIRECTORY_SEPARATOR . 'config.ini';

        $localConfig = new Zend_Config_Ini(
                $localConfigFile,
                APPLICATION_ENV
        );
        $config->merge($localConfig);

        $config = $config->toArray();

        return $config;
    }
    
    /**
     *
     * @return Zend_Db_Adapter_Abstract
     */
    public function getDbAdapter()
    {
        return Zend_Db_Table::getDefaultAdapter();
    }

    protected function loadModuleBootstrap()
    {
        $plugin = new Lib_Module_Bootstrap_Loader();
        Zend_Controller_Front::getInstance()->registerPlugin( $plugin, 1 );
    }
    
}
