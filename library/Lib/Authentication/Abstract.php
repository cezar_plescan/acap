<?php

abstract class Lib_Authentication_Abstract
{
    public function __construct()
    {
        
    }
    
    public function storeIdentity( $identity, $rememberMe = true )
    {
        // store identity in session
        Zend_Auth::getInstance()->getStorage()->write( $identity );
        
        if( $rememberMe )
        {
            $duration = Lib_Application::getInstance()->getConfig( [ 'authentication', 'rememberme' ] );
            Zend_Session::RememberMe( $duration * 3600 );
        }
        else
        {
            Zend_Session::ForgetMe();
        }
        
    }
    
    /**
     * 
     * @return mixed
     */
    public function getIdentity()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        
        return $identity;
    }
    
    /**
     * 
     * @return bool
     */
    public function hasIdentity()
    {
        $hasIdentity = Zend_Auth::getInstance()->hasIdentity();
        
        return $hasIdentity;
    }
    
    /**
     * 
     */
    public function clearIdentity()
    {
        Zend_Auth::getInstance()->clearIdentity();
    }
    
}
