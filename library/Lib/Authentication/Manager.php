<?php

final class Lib_Authentication_Manager
{
    static protected $instance = null;

    /**
     *
     * @return Lib_Authentication_Abstract
     */
    static public function getWorker()
    {
        if (self::$instance == null)
        {
            self::$instance = self::createWorker();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        
    }

    /**
     * @return Lib_Authentication_Abstract
     */
    static protected function createWorker()
    {
        $authenticationClass = Lib_Application::getInstance()->getConfig( [ 'authentication', 'class' ] );
        if( !is_subclass_of( $authenticationClass, 'Lib_Authentication_Abstract' ) )
        {
            throw new Lib_Authentication_Exception_InvalidConfigClass( $authenticationClass );
        }
        
        $Worker = new $authenticationClass();
        
        return $Worker;
    }
    
}
