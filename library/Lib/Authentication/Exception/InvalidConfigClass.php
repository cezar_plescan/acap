<?php

class Lib_Authentication_Exception_InvalidConfigClass extends UnexpectedValueException
{
    public function __construct( $authenticationClass = NULL, $code = NULL, $previous = NULL )
    {
        $message = ( strlen( $authenticationClass) == 0 ) ?
                'Authentication class is not defined.' :
                'Invalid authentication class: "' . $authenticationClass . '".';
        
        parent::__construct( $message, $code, $previous );
    }
    
}
