<?php

class Lib_Module_Bootstrap_Abstract
{
    protected $moduleName;
    
    public function __construct( $moduleName )
    {
        $this->moduleName = $moduleName;
    }
    
    final public function run()
    {
        $this->executeResources();
    }
    
    private function executeResources()
    {
        $classMethods = get_class_methods( $this );
        foreach( $classMethods as $methodName )
        {
            if( $this->isResourceMethod( $methodName ) )
            {
                $this->$methodName();
            }
        }
    }
    
    private function isResourceMethod( $methodName )
    {
        $isResource = preg_match( '/^_init[A-Z]./', $methodName );
        
        return $isResource;
    }
    
    /**
     * 
     * @return Lib_Application
     */
    protected function getApplication()
    {
        $Application = Lib_Application::getInstance();
        
        return $Application;
    }
    
    /**
     * 
     * @return Zend_Application_Bootstrap_BootstrapAbstract
     */
    protected function getBootstrap()
    {
        $Bootstrap = $this->getApplication()->getBootstrap();
        
        return $Bootstrap;
    }
    
    protected function getModuleName()
    {
        return $this->moduleName;
    }
    
    /**
     * 
     * @return Zend_Controller_Front
     */
    protected function getFrontController()
    {
        $FrontController = Zend_Controller_Front::getInstance();
        
        return $FrontController;
    }
    
    protected function getControllerDirectory()
    {
        $directory = $this->getFrontController()->getControllerDirectory( $this->getModuleName() );
        
        return $directory;
    }
    
}
