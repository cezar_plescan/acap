<?php

class Lib_Module_Bootstrap_Loader extends Zend_Controller_Plugin_Abstract
{
    public function routeShutdown( Zend_Controller_Request_Abstract $request )
    {
        // load bootstrap for the current module 
        
        $module = $request->getModuleName();
        $moduleDirectory = Zend_Controller_Front::getInstance()->getModuleDirectory( $module );
        
        require_once $moduleDirectory . '/Bootstrap.php';
        
        $className = ucfirst( $module ) . '_Bootstrap';
        
        /* @var $ModuleBootstrap Lib_Module_Bootstrap_Abstract */
        $ModuleBootstrap = new $className( $module );
        $ModuleBootstrap->run();
    }
    
}
