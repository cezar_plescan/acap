<?php

/**
 * @todo To be reviewed !
 */
class Lib_Mail
{

    /**
     * Send mail.
     * All arguments are mandatory.
     *
     * @param string $email The email address
     * @param string $recipient_name The name of the recipient
     * @param string $body The email content in HTML format
     * @param string $subject The email subject
     */
    public function send($email, $recipient_name, $body, $subject)
    {
//        $settings = array(
//            'ssl'       => 'ssl',
//            'port'      => 465,
//            'auth'      => 'login',
//            'username'  => 'wordxpress.noreply@gmail.com',
//            'password'  => 'wordxpresstesting');
//
//        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $settings);
//
//        $email_from = "wordxpress.noreply@gmail.com";
//        $name_from = "WordXpress";
//        $email_to = $email;
//        $name_to = $recipient_name;
//
//        $mail = new Zend_Mail ();
//        $mail->setReplyTo($email_from, $name_from);
//        $mail->setFrom ($email_from, $name_from);
//        $mail->addTo ($email_to, $name_to);
//        $mail->setSubject ($subject);
//        $mail->setBodyHtml($body);
//
//        $mail->send($transport);



        $tr = new Zend_Mail_Transport_Smtp();
        Zend_Mail::setDefaultTransport($tr);

        $mail = new Zend_Mail();

        $fromEmail = Lib_Application::getInstance()->getConfig(array('settings', 'email'));

        $mail
                ->setBodyHtml($body)
                ->setFrom($fromEmail, 'WordXpress')
                ->addTo($email, $recipient_name)
                ->setSubject($subject)
                ->setReplyTo($fromEmail, 'WordXpress')
                ->addHeader('MIME-Version', '1.0')
                ->addHeader('Content-Transfer-Encoding', '8bit')
                ->addHeader('X-Mailer:', 'PHP/' . phpversion());
        ;

        $noErrors = true;

        $sendEmail = Lib_Application::getInstance()->getConfig(array('settings', 'sendMail'));
        if ($sendEmail)
        {
            try
            {
                $mail->send();
            }
            catch (Zend_Mail_Exception $e)
            {
                //the mail did not get sent
                $noErrors = false;
            }
        }

        return $noErrors;
    }

}
